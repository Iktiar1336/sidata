-- Adminer 4.8.1 PostgreSQL 14.4 (Debian 14.4-1.pgdg110+1) dump

DROP TABLE IF EXISTS "activity_log";
DROP SEQUENCE IF EXISTS activity_log_id_seq;
CREATE SEQUENCE activity_log_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."activity_log" (
    "id" bigint DEFAULT nextval('activity_log_id_seq') NOT NULL,
    "log_name" character varying(255),
    "description" text NOT NULL,
    "subject_type" character varying(255),
    "subject_id" bigint,
    "causer_type" character varying(255),
    "causer_id" bigint,
    "properties" json,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "activity_log_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "activity_log_log_name_index" ON "public"."activity_log" USING btree ("log_name");

CREATE INDEX "causer" ON "public"."activity_log" USING btree ("causer_type", "causer_id");

CREATE INDEX "subject" ON "public"."activity_log" USING btree ("subject_type", "subject_id");

INSERT INTO "activity_log" ("id", "log_name", "description", "subject_type", "subject_id", "causer_type", "causer_id", "properties", "created_at", "updated_at") VALUES
(1,	'Login',	'Super Admin Login Sebagai Super Admin',	'App\Models\User',	1,	'App\Models\User',	1,	'[]',	'2022-12-23 15:05:41',	'2022-12-23 15:05:41'),
(2,	'Login',	'Super Admin Login Sebagai Super Admin',	'App\Models\User',	1,	'App\Models\User',	1,	'[]',	'2022-12-23 20:21:06',	'2022-12-23 20:21:06'),
(3,	'Menambahkan Berita Baru',	'Super Admin Menambahkan Berita Baru Dengan Judul : Gerakan Tertib Arsip dan Sejarah Desa Resmi Digaungkan',	'App\Models\News',	1,	'App\Models\User',	1,	'[]',	'2022-12-23 20:22:39',	'2022-12-23 20:22:39'),
(4,	'Mengupdate Berita',	'Super Admin Mengupdate Berita Yang Berjudul : Gerakan Tertib Arsip dan Sejarah Desa Resmi Digaungkan',	'App\Models\News',	1,	'App\Models\User',	1,	'[]',	'2022-12-23 20:23:14',	'2022-12-23 20:23:14'),
(5,	'Login',	'Super Admin Login Sebagai Super Admin',	'App\Models\User',	1,	'App\Models\User',	1,	'[]',	'2022-12-24 12:02:51',	'2022-12-24 12:02:51'),
(6,	'Mengupdate Berita',	'Super Admin Mengupdate Berita Yang Berjudul : Gerakan Tertib Arsip dan Sejarah Desa Resmi Digaungkan',	'App\Models\News',	1,	'App\Models\User',	1,	'[]',	'2022-12-24 12:06:28',	'2022-12-24 12:06:28'),
(7,	'Menambahkan Infografis',	'Super Admin Menambahkan Infografis Dengan Judul :',	NULL,	NULL,	'App\Models\User',	1,	'[]',	'2022-12-24 12:21:32',	'2022-12-24 12:21:32'),
(8,	'Mengupdate Infografis',	'Super Admin Mengupdate Infografis Yang Berjudul :',	NULL,	NULL,	'App\Models\User',	1,	'[]',	'2022-12-24 12:22:01',	'2022-12-24 12:22:01'),
(9,	'Mengupdate Infografis',	'Super Admin Mengupdate Infografis Yang Berjudul :',	NULL,	NULL,	'App\Models\User',	1,	'[]',	'2022-12-24 12:29:36',	'2022-12-24 12:29:36'),
(10,	'Mengupdate Infografis',	'Super Admin Mengupdate Infografis Yang Berjudul :',	NULL,	NULL,	'App\Models\User',	1,	'[]',	'2022-12-24 12:30:25',	'2022-12-24 12:30:25'),
(11,	'Login',	'Super Admin Login Sebagai Super Admin',	'App\Models\User',	1,	'App\Models\User',	1,	'[]',	'2022-12-26 11:22:36',	'2022-12-26 11:22:36');

DROP TABLE IF EXISTS "app_setting";
DROP SEQUENCE IF EXISTS app_setting_id_seq;
CREATE SEQUENCE app_setting_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."app_setting" (
    "id" bigint DEFAULT nextval('app_setting_id_seq') NOT NULL,
    "nama_aplikasi" text,
    "deskripsi" text,
    "logo" text,
    "alamat" text,
    "email" text,
    "no_telp" text,
    "no_fax" text,
    "instagram" text,
    "facebook" text,
    "twitter" text,
    "youtube" text,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "app_setting_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "arsip_digital";
DROP SEQUENCE IF EXISTS arsip_digital_id_seq;
CREATE SEQUENCE arsip_digital_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."arsip_digital" (
    "id" bigint DEFAULT nextval('arsip_digital_id_seq') NOT NULL,
    "arsipkolektor_id" bigint NOT NULL,
    "user_id" bigint NOT NULL,
    "type" text,
    "folder" text,
    "file" text,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "arsip_digital_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "arsip_kolektor";
DROP SEQUENCE IF EXISTS arsip_kolektor_id_seq;
CREATE SEQUENCE arsip_kolektor_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."arsip_kolektor" (
    "id" bigint DEFAULT nextval('arsip_kolektor_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "instansi_id" bigint NOT NULL,
    "jenisarsip_id" bigint NOT NULL,
    "uraianitem" text NOT NULL,
    "nomor_surat" text NOT NULL,
    "tanggal_surat" text NOT NULL,
    "kode_klasifikasi" text NOT NULL,
    "tingkat_perkembangan" text NOT NULL,
    "media_arsip" text NOT NULL,
    "kondisi" text NOT NULL,
    "jumlah" text NOT NULL,
    "keterangan" text NOT NULL,
    "status" boolean DEFAULT true NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "arsip_kolektor_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "capaian";
DROP SEQUENCE IF EXISTS capaian_id_seq;
CREATE SEQUENCE capaian_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."capaian" (
    "id" bigint DEFAULT nextval('capaian_id_seq') NOT NULL,
    "judul" character varying(255) NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "deskripsi" text NOT NULL,
    "status" boolean DEFAULT true NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "capaian_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "capaian_kinerja";
DROP SEQUENCE IF EXISTS capaian_kinerja_id_seq;
CREATE SEQUENCE capaian_kinerja_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."capaian_kinerja" (
    "id" bigint DEFAULT nextval('capaian_kinerja_id_seq') NOT NULL,
    "sasaranstrategis_id" bigint NOT NULL,
    "realisasi" character varying(255) NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "persentase" character varying(255) NOT NULL,
    "status" boolean DEFAULT true NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "capaian_kinerja_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "capaian_modelkinerja";
DROP SEQUENCE IF EXISTS capaian_modelkinerja_id_seq;
CREATE SEQUENCE capaian_modelkinerja_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."capaian_modelkinerja" (
    "id" bigint DEFAULT nextval('capaian_modelkinerja_id_seq') NOT NULL,
    "capaian_id" bigint NOT NULL,
    "modelkinerja_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "capaian_modelkinerja_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "capaian_spbe";
DROP SEQUENCE IF EXISTS capaian_spbe_id_seq;
CREATE SEQUENCE capaian_spbe_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."capaian_spbe" (
    "id" bigint DEFAULT nextval('capaian_spbe_id_seq') NOT NULL,
    "judul" character varying(255) NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "indeks" character varying(255) NOT NULL,
    "predikat" character varying(255) NOT NULL,
    "deskripsi" text NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "capaian_spbe_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "capaian_spbe_tahun_unique" UNIQUE ("tahun")
) WITH (oids = false);


DROP TABLE IF EXISTS "capaiankinerja_modelkinerja";
DROP SEQUENCE IF EXISTS capaiankinerja_modelkinerja_id_seq;
CREATE SEQUENCE capaiankinerja_modelkinerja_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."capaiankinerja_modelkinerja" (
    "id" bigint DEFAULT nextval('capaiankinerja_modelkinerja_id_seq') NOT NULL,
    "capaiankinerja_id" bigint NOT NULL,
    "modelkinerja_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "capaiankinerja_modelkinerja_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "categories";
DROP SEQUENCE IF EXISTS categories_id_seq;
CREATE SEQUENCE categories_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."categories" (
    "id" bigint DEFAULT nextval('categories_id_seq') NOT NULL,
    "parent_id" integer,
    "workunit_id" bigint,
    "name" character varying(255) NOT NULL,
    "description" character varying(255),
    "access" character varying(255),
    "status" integer,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "categories_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "categories" ("id", "parent_id", "workunit_id", "name", "description", "access", "status", "created_at", "updated_at") VALUES
(1,	NULL,	3,	'Data Pegawai ANRI',	'Data Pegawai ANRI',	'Public',	1,	'2022-12-23 11:08:44',	'2022-12-23 11:08:44'),
(2,	1,	3,	'Berdasarkan Pendidikan',	'Berdasarkan Pendidikan',	NULL,	1,	'2022-12-23 11:08:45',	'2022-12-23 11:08:45'),
(3,	1,	3,	'Berdasarkan Jenis Kelamin',	'Berdasarkan Jenis Kelamin',	NULL,	1,	'2022-12-23 11:08:45',	'2022-12-23 11:08:45'),
(4,	1,	3,	'Berdasarkan Golongan',	'Berdasarkan Golongan',	NULL,	1,	'2022-12-23 11:08:46',	'2022-12-23 11:08:46'),
(5,	1,	3,	'Berdasarkan Tahun',	'Berdasarkan Tahun',	NULL,	1,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47');

DROP TABLE IF EXISTS "chats";
DROP SEQUENCE IF EXISTS chats_id_seq;
CREATE SEQUENCE chats_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."chats" (
    "id" bigint DEFAULT nextval('chats_id_seq') NOT NULL,
    "topic" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "chats_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "email_settings";
DROP SEQUENCE IF EXISTS email_settings_id_seq;
CREATE SEQUENCE email_settings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."email_settings" (
    "id" bigint DEFAULT nextval('email_settings_id_seq') NOT NULL,
    "driver" character varying(255) NOT NULL,
    "host" character varying(255) NOT NULL,
    "port" character varying(255) NOT NULL,
    "username" character varying(255) NOT NULL,
    "password" character varying(255) NOT NULL,
    "encryption" character varying(255) NOT NULL,
    "from_address" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "email_settings_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "email_settings" ("id", "driver", "host", "port", "username", "password", "encryption", "from_address", "created_at", "updated_at") VALUES
(1,	'smtp',	'smtp.gmail.com',	'587',	'sidataanri@gmail.com',	'eyJpdiI6IkxveWhVakQ3L3NVQmY5RC9oVExCV0E9PSIsInZhbHVlIjoic0EwN3JHaUZFbWxGcUlkVjc3UXBkd0F1cjg5Ly9GakRKQzgvUVpqUVR0ST0iLCJtYWMiOiI2YjczZjdhMzgwNmE4ZDNhYTY5ZDcwYzBkYWY2YTkyZDg2NTFjNTRmYTAwMDVlYWJhMmY5YTVhZDNiMDFjZDY1IiwidGFnIjoiIn0=',	'tls',	'sidataanri@gmail.com',	'2022-12-23 11:08:43',	'2022-12-23 11:08:43');

DROP TABLE IF EXISTS "failed_jobs";
DROP SEQUENCE IF EXISTS failed_jobs_id_seq;
CREATE SEQUENCE failed_jobs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."failed_jobs" (
    "id" bigint DEFAULT nextval('failed_jobs_id_seq') NOT NULL,
    "uuid" character varying(255) NOT NULL,
    "connection" text NOT NULL,
    "queue" text NOT NULL,
    "payload" text NOT NULL,
    "exception" text NOT NULL,
    "failed_at" timestamp(0) DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "failed_jobs_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "failed_jobs_uuid_unique" UNIQUE ("uuid")
) WITH (oids = false);


DROP TABLE IF EXISTS "file_uploads";
DROP SEQUENCE IF EXISTS file_uploads_id_seq;
CREATE SEQUENCE file_uploads_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."file_uploads" (
    "id" bigint DEFAULT nextval('file_uploads_id_seq') NOT NULL,
    "category_id" bigint NOT NULL,
    "filename" text NOT NULL,
    "year" integer NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "file_uploads_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "glosarium_metadata";
DROP SEQUENCE IF EXISTS glosarium_metadata_id_seq;
CREATE SEQUENCE glosarium_metadata_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."glosarium_metadata" (
    "id" bigint DEFAULT nextval('glosarium_metadata_id_seq') NOT NULL,
    "metadata" character varying(255) NOT NULL,
    "istilah" text NOT NULL,
    "definisi" text NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "glosarium_metadata_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "indikator_kinerja";
DROP SEQUENCE IF EXISTS indikator_kinerja_id_seq;
CREATE SEQUENCE indikator_kinerja_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."indikator_kinerja" (
    "id" bigint DEFAULT nextval('indikator_kinerja_id_seq') NOT NULL,
    "content" text NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "indikator_kinerja_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "infografis";
DROP SEQUENCE IF EXISTS infografis_id_seq;
CREATE SEQUENCE infografis_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."infografis" (
    "id" bigint DEFAULT nextval('infografis_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "title" character varying(255) NOT NULL,
    "slug" character varying(255) NOT NULL,
    "content" text NOT NULL,
    "image" character varying(255),
    "status" boolean DEFAULT true NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "infografis_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "infografis" ("id", "user_id", "title", "slug", "content", "image", "status", "created_at", "updated_at") VALUES
(1,	1,	'Data Biro Perencanaan dan Humas Tahun 2015 s.d. 2018',	'data-biro-perencanaan-dan-humas-tahun-2015-sd-2018',	'<p><strong>Data Biro Perencanaan dan Humas ini berisi Data DIPA ANRI, Data Agenda Pimpinan, Data Layanan Studi Banding dan Data Layanan Diorama tahun 2015 s.d. 2018</strong></p>',	'Biro_Perencanaan_dan_Humas_(DIPA).png',	't',	'2022-12-24 12:21:32',	'2022-12-24 12:30:25');

DROP TABLE IF EXISTS "instansi";
DROP SEQUENCE IF EXISTS instansi_id_seq;
CREATE SEQUENCE instansi_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."instansi" (
    "id" bigint DEFAULT nextval('instansi_id_seq') NOT NULL,
    "code" character varying(255) NOT NULL,
    "parent_id" bigint,
    "nama_instansi" character varying(255) NOT NULL,
    "nomenklatur_unit" character varying(255) NOT NULL,
    "alamat" character varying(255) NOT NULL,
    "telp" character varying(255),
    "email" character varying(255),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "instansi_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "jawaban";
DROP SEQUENCE IF EXISTS jawaban_id_seq;
CREATE SEQUENCE jawaban_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."jawaban" (
    "id" bigint DEFAULT nextval('jawaban_id_seq') NOT NULL,
    "surveyarsip_id" bigint NOT NULL,
    "user_id" bigint NOT NULL,
    "workunit_id" bigint,
    "unitkerja" character varying(255),
    "instansi" character varying(255),
    "instansi_id" bigint,
    "submitted_at" timestamp(0),
    "kelembagaan_tk1_a1" text,
    "kelembagaan_tk1_a2" text,
    "kelembagaan_tk1_b1" text,
    "kelembagaan_tk1_b2" text,
    "kelembagaan_tk1_c1" text,
    "kelembagaan_tk1_c2" text,
    "kelembagaan_tk1_d1" text,
    "kelembagaan_tk1_d2" text,
    "kelembagaan_tk1_e1" text,
    "kelembagaan_tk1_e2" text,
    "kelembagaan_tk1_f1" text,
    "kelembagaan_tk1_f2" text,
    "kelembagaan_tk2_a1" text,
    "kelembagaan_tk2_a2" text,
    "kelembagaan_tk2_b1" text,
    "kelembagaan_tk2_b2" text,
    "kelembagaan_tk2_c1" text,
    "kelembagaan_tk2_c2" text,
    "kelembagaan_tk2_d1" text,
    "kelembagaan_tk2_d2" text,
    "kelembagaan_tk2_e1" text,
    "kelembagaan_tk2_e2" text,
    "kelembagaan_tk2_f1" text,
    "kelembagaan_tk2_f2" text,
    "kelembagaan_tk3_a1" text,
    "kelembagaan_tk3_a2" text,
    "kelembagaan_tk3_b1" text,
    "kelembagaan_tk3_b2" text,
    "kelembagaan_tk3_c1" text,
    "kelembagaan_tk3_c2" text,
    "kelembagaan_tk3_d1" text,
    "kelembagaan_tk3_d2" text,
    "kelembagaan_tk3_e1" text,
    "kelembagaan_tk3_e2" text,
    "kelembagaan_tk3_f1" text,
    "kelembagaan_tk3_f2" text,
    "kelembagaan_tk4_a1" text,
    "kelembagaan_tk4_a2" text,
    "kelembagaan_tk4_b1" text,
    "kelembagaan_tk4_b2" text,
    "kelembagaan_tk4_c1" text,
    "kelembagaan_tk4_c2" text,
    "kelembagaan_tk4_d1" text,
    "kelembagaan_tk4_d2" text,
    "kelembagaan_tk4_e1" text,
    "kelembagaan_tk4_e2" text,
    "kelembagaan_tk4_f1" text,
    "kelembagaan_tk4_f2" text,
    "kebijakanpengelolaanarsip_tk1_a1" text,
    "kebijakanpengelolaanarsip_tk1_a2" text,
    "kebijakanpengelolaanarsip_tk1_b1" text,
    "kebijakanpengelolaanarsip_tk1_b2" text,
    "kebijakanpengelolaanarsip_tk1_c1" text,
    "kebijakanpengelolaanarsip_tk1_c2" text,
    "kebijakanpengelolaanarsip_tk1_d1" text,
    "kebijakanpengelolaanarsip_tk1_d2" text,
    "kebijakanpengelolaanarsip_tk1_e1" text,
    "kebijakanpengelolaanarsip_tk1_e2" text,
    "kebijakanpengelolaanarsip_tk2_a1" text,
    "kebijakanpengelolaanarsip_tk2_a2" text,
    "kebijakanpengelolaanarsip_tk2_b1" text,
    "kebijakanpengelolaanarsip_tk2_b2" text,
    "kebijakanpengelolaanarsip_tk2_c1" text,
    "kebijakanpengelolaanarsip_tk2_c2" text,
    "kebijakanpengelolaanarsip_tk2_d1" text,
    "kebijakanpengelolaanarsip_tk2_d2" text,
    "kebijakanpengelolaanarsip_tk2_e1" text,
    "kebijakanpengelolaanarsip_tk2_e2" text,
    "kebijakanpengelolaanarsip_tk3_a1" text,
    "kebijakanpengelolaanarsip_tk3_a2" text,
    "kebijakanpengelolaanarsip_tk3_b1" text,
    "kebijakanpengelolaanarsip_tk3_b2" text,
    "kebijakanpengelolaanarsip_tk3_c1" text,
    "kebijakanpengelolaanarsip_tk3_c2" text,
    "kebijakanpengelolaanarsip_tk3_d1" text,
    "kebijakanpengelolaanarsip_tk3_d2" text,
    "kebijakanpengelolaanarsip_tk3_e1" text,
    "kebijakanpengelolaanarsip_tk3_e2" text,
    "kebijakanpengelolaanarsip_tk4_a1" text,
    "kebijakanpengelolaanarsip_tk4_a2" text,
    "kebijakanpengelolaanarsip_tk4_b1" text,
    "kebijakanpengelolaanarsip_tk4_b2" text,
    "kebijakanpengelolaanarsip_tk4_c1" text,
    "kebijakanpengelolaanarsip_tk4_c2" text,
    "kebijakanpengelolaanarsip_tk4_d1" text,
    "kebijakanpengelolaanarsip_tk4_d2" text,
    "kebijakanpengelolaanarsip_tk4_e1" text,
    "kebijakanpengelolaanarsip_tk4_e2" text,
    "sdmkearsipan_tk1_a1" text,
    "sdmkearsipan_tk1_a2" text,
    "sdmkearsipan_tk1_a3" text,
    "sdmkearsipan_tk1_a4" text,
    "sdmkearsipan_tk1_a5" text,
    "sdmkearsipan_tk1_a6" text,
    "sdmkearsipan_tk1_b1" text,
    "sdmkearsipan_tk1_b2" text,
    "sdmkearsipan_tk1_b3" text,
    "sdmkearsipan_tk1_b4" text,
    "sdmkearsipan_tk1_b5" text,
    "sdmkearsipan_tk1_b6" text,
    "sdmkearsipan_tk1_c1" text,
    "sdmkearsipan_tk1_c2" text,
    "sdmkearsipan_tk1_c3" text,
    "sdmkearsipan_tk1_c4" text,
    "sdmkearsipan_tk1_c5" text,
    "sdmkearsipan_tk1_c6" text,
    "sdmkearsipan_tk1_d1" text,
    "sdmkearsipan_tk1_d2" text,
    "sdmkearsipan_tk1_d3" text,
    "sdmkearsipan_tk1_d4" text,
    "sdmkearsipan_tk1_d5" text,
    "sdmkearsipan_tk1_d6" text,
    "sdmkearsipan_tk1_e1" text,
    "sdmkearsipan_tk1_e2" text,
    "sdmkearsipan_tk1_e3" text,
    "sdmkearsipan_tk1_e4" text,
    "sdmkearsipan_tk1_e5" text,
    "sdmkearsipan_tk1_e6" text,
    "sdmkearsipan_tk1_f1" text,
    "sdmkearsipan_tk1_f2" text,
    "sdmkearsipan_tk1_f3" text,
    "sdmkearsipan_tk1_f4" text,
    "sdmkearsipan_tk1_f5" text,
    "sdmkearsipan_tk1_f6" text,
    "sdmkearsipan_tk1_g1" text,
    "sdmkearsipan_tk1_g2" text,
    "sdmkearsipan_tk1_g3" text,
    "sdmkearsipan_tk1_g4" text,
    "sdmkearsipan_tk1_g5" text,
    "sdmkearsipan_tk1_g6" text,
    "sdmkearsipan_tk1_h1" text,
    "sdmkearsipan_tk1_h2" text,
    "sdmkearsipan_tk1_h3" text,
    "sdmkearsipan_tk1_h4" text,
    "sdmkearsipan_tk1_h5" text,
    "sdmkearsipan_tk1_h6" text,
    "sdmkearsipan_tk2_a1" text,
    "sdmkearsipan_tk2_a2" text,
    "sdmkearsipan_tk2_a3" text,
    "sdmkearsipan_tk2_a4" text,
    "sdmkearsipan_tk2_a5" text,
    "sdmkearsipan_tk2_a6" text,
    "sdmkearsipan_tk2_b1" text,
    "sdmkearsipan_tk2_b2" text,
    "sdmkearsipan_tk2_b3" text,
    "sdmkearsipan_tk2_b4" text,
    "sdmkearsipan_tk2_b5" text,
    "sdmkearsipan_tk2_b6" text,
    "sdmkearsipan_tk2_c1" text,
    "sdmkearsipan_tk2_c2" text,
    "sdmkearsipan_tk2_c3" text,
    "sdmkearsipan_tk2_c4" text,
    "sdmkearsipan_tk2_c5" text,
    "sdmkearsipan_tk2_c6" text,
    "sdmkearsipan_tk2_d1" text,
    "sdmkearsipan_tk2_d2" text,
    "sdmkearsipan_tk2_d3" text,
    "sdmkearsipan_tk2_d4" text,
    "sdmkearsipan_tk2_d5" text,
    "sdmkearsipan_tk2_d6" text,
    "sdmkearsipan_tk2_e1" text,
    "sdmkearsipan_tk2_e2" text,
    "sdmkearsipan_tk2_e3" text,
    "sdmkearsipan_tk2_e4" text,
    "sdmkearsipan_tk2_e5" text,
    "sdmkearsipan_tk2_e6" text,
    "sdmkearsipan_tk2_f1" text,
    "sdmkearsipan_tk2_f2" text,
    "sdmkearsipan_tk2_f3" text,
    "sdmkearsipan_tk2_f4" text,
    "sdmkearsipan_tk2_f5" text,
    "sdmkearsipan_tk2_f6" text,
    "sdmkearsipan_tk2_g1" text,
    "sdmkearsipan_tk2_g2" text,
    "sdmkearsipan_tk2_g3" text,
    "sdmkearsipan_tk2_g4" text,
    "sdmkearsipan_tk2_g5" text,
    "sdmkearsipan_tk2_g6" text,
    "sdmkearsipan_tk2_h1" text,
    "sdmkearsipan_tk2_h2" text,
    "sdmkearsipan_tk2_h3" text,
    "sdmkearsipan_tk2_h4" text,
    "sdmkearsipan_tk2_h5" text,
    "sdmkearsipan_tk2_h6" text,
    "sdmkearsipan_tk3_a1" text,
    "sdmkearsipan_tk3_a2" text,
    "sdmkearsipan_tk3_a3" text,
    "sdmkearsipan_tk3_a4" text,
    "sdmkearsipan_tk3_a5" text,
    "sdmkearsipan_tk3_a6" text,
    "sdmkearsipan_tk3_b1" text,
    "sdmkearsipan_tk3_b2" text,
    "sdmkearsipan_tk3_b3" text,
    "sdmkearsipan_tk3_b4" text,
    "sdmkearsipan_tk3_b5" text,
    "sdmkearsipan_tk3_b6" text,
    "sdmkearsipan_tk3_c1" text,
    "sdmkearsipan_tk3_c2" text,
    "sdmkearsipan_tk3_c3" text,
    "sdmkearsipan_tk3_c4" text,
    "sdmkearsipan_tk3_c5" text,
    "sdmkearsipan_tk3_c6" text,
    "sdmkearsipan_tk3_d1" text,
    "sdmkearsipan_tk3_d2" text,
    "sdmkearsipan_tk3_d3" text,
    "sdmkearsipan_tk3_d4" text,
    "sdmkearsipan_tk3_d5" text,
    "sdmkearsipan_tk3_d6" text,
    "sdmkearsipan_tk3_e1" text,
    "sdmkearsipan_tk3_e2" text,
    "sdmkearsipan_tk3_e3" text,
    "sdmkearsipan_tk3_e4" text,
    "sdmkearsipan_tk3_e5" text,
    "sdmkearsipan_tk3_e6" text,
    "sdmkearsipan_tk3_f1" text,
    "sdmkearsipan_tk3_f2" text,
    "sdmkearsipan_tk3_f3" text,
    "sdmkearsipan_tk3_f4" text,
    "sdmkearsipan_tk3_f5" text,
    "sdmkearsipan_tk3_f6" text,
    "sdmkearsipan_tk3_g1" text,
    "sdmkearsipan_tk3_g2" text,
    "sdmkearsipan_tk3_g3" text,
    "sdmkearsipan_tk3_g4" text,
    "sdmkearsipan_tk3_g5" text,
    "sdmkearsipan_tk3_g6" text,
    "sdmkearsipan_tk3_h1" text,
    "sdmkearsipan_tk3_h2" text,
    "sdmkearsipan_tk3_h3" text,
    "sdmkearsipan_tk3_h4" text,
    "sdmkearsipan_tk3_h5" text,
    "sdmkearsipan_tk3_h6" text,
    "sdmkearsipan_tk4_a1" text,
    "sdmkearsipan_tk4_a2" text,
    "sdmkearsipan_tk4_a3" text,
    "sdmkearsipan_tk4_a4" text,
    "sdmkearsipan_tk4_a5" text,
    "sdmkearsipan_tk4_a6" text,
    "sdmkearsipan_tk4_b1" text,
    "sdmkearsipan_tk4_b2" text,
    "sdmkearsipan_tk4_b3" text,
    "sdmkearsipan_tk4_b4" text,
    "sdmkearsipan_tk4_b5" text,
    "sdmkearsipan_tk4_b6" text,
    "sdmkearsipan_tk4_c1" text,
    "sdmkearsipan_tk4_c2" text,
    "sdmkearsipan_tk4_c3" text,
    "sdmkearsipan_tk4_c4" text,
    "sdmkearsipan_tk4_c5" text,
    "sdmkearsipan_tk4_c6" text,
    "sdmkearsipan_tk4_d1" text,
    "sdmkearsipan_tk4_d2" text,
    "sdmkearsipan_tk4_d3" text,
    "sdmkearsipan_tk4_d4" text,
    "sdmkearsipan_tk4_d5" text,
    "sdmkearsipan_tk4_d6" text,
    "sdmkearsipan_tk4_e1" text,
    "sdmkearsipan_tk4_e2" text,
    "sdmkearsipan_tk4_e3" text,
    "sdmkearsipan_tk4_e4" text,
    "sdmkearsipan_tk4_e5" text,
    "sdmkearsipan_tk4_e6" text,
    "sdmkearsipan_tk4_f1" text,
    "sdmkearsipan_tk4_f2" text,
    "sdmkearsipan_tk4_f3" text,
    "sdmkearsipan_tk4_f4" text,
    "sdmkearsipan_tk4_f5" text,
    "sdmkearsipan_tk4_f6" text,
    "sdmkearsipan_tk4_g1" text,
    "sdmkearsipan_tk4_g2" text,
    "sdmkearsipan_tk4_g3" text,
    "sdmkearsipan_tk4_g4" text,
    "sdmkearsipan_tk4_g5" text,
    "sdmkearsipan_tk4_g6" text,
    "sdmkearsipan_tk4_h1" text,
    "sdmkearsipan_tk4_h2" text,
    "sdmkearsipan_tk4_h3" text,
    "sdmkearsipan_tk4_h4" text,
    "sdmkearsipan_tk4_h5" text,
    "sdmkearsipan_tk4_h6" text,
    "anggaranbidangkearsipan_a1" text,
    "anggaranbidangkearsipan_a2" text,
    "anggaranbidangkearsipan_a3" text,
    "anggaranbidangkearsipan_a4" text,
    "anggaranbidangkearsipan_b1" text,
    "anggaranbidangkearsipan_b2" text,
    "anggaranbidangkearsipan_b3" text,
    "anggaranbidangkearsipan_b4" text,
    "khasanaharsip_a1a" text,
    "khasanaharsip_a1b" text,
    "khasanaharsip_a1c" text,
    "khasanaharsip_a1d" text,
    "khasanaharsip_a1e" text,
    "khasanaharsip_a1f" text,
    "khasanaharsip_a1g" text,
    "khasanaharsip_a1h" text,
    "khasanaharsip_a2a" text,
    "khasanaharsip_a2b" text,
    "khasanaharsip_a2c" text,
    "khasanaharsip_a2d" text,
    "khasanaharsip_a2e" text,
    "khasanaharsip_a2f" text,
    "khasanaharsip_a2g" text,
    "khasanaharsip_a2h" text,
    "khasanaharsip_a3a" text,
    "khasanaharsip_a3b" text,
    "khasanaharsip_a3c" text,
    "khasanaharsip_a3d" text,
    "khasanaharsip_a3e" text,
    "khasanaharsip_a3f" text,
    "khasanaharsip_a3g" text,
    "khasanaharsip_a3h" text,
    "khasanaharsip_a4a" text,
    "khasanaharsip_a4b" text,
    "khasanaharsip_a4c" text,
    "khasanaharsip_a4d" text,
    "khasanaharsip_a4e" text,
    "khasanaharsip_a4f" text,
    "khasanaharsip_a4g" text,
    "khasanaharsip_a4h" text,
    "khasanaharsip_a5a" text,
    "khasanaharsip_a5b" text,
    "khasanaharsip_a5c" text,
    "khasanaharsip_a5d" text,
    "khasanaharsip_a5e" text,
    "khasanaharsip_a5f" text,
    "khasanaharsip_a5g" text,
    "khasanaharsip_a5h" text,
    "khasanaharsip_b1a" text,
    "khasanaharsip_b1b" text,
    "khasanaharsip_b1c" text,
    "khasanaharsip_b1d" text,
    "khasanaharsip_b1e" text,
    "khasanaharsip_b1f" text,
    "khasanaharsip_b1g" text,
    "khasanaharsip_b2a" text,
    "khasanaharsip_b2b" text,
    "khasanaharsip_b2c" text,
    "khasanaharsip_b2d" text,
    "khasanaharsip_b2e" text,
    "khasanaharsip_b2f" text,
    "khasanaharsip_b2g" text,
    "khasanaharsip_b3a" text,
    "khasanaharsip_b3b" text,
    "khasanaharsip_b3c" text,
    "khasanaharsip_b3d" text,
    "khasanaharsip_b3e" text,
    "khasanaharsip_b3f" text,
    "khasanaharsip_b3g" text,
    "khasanaharsip_b4a" text,
    "khasanaharsip_b4b" text,
    "khasanaharsip_b4c" text,
    "khasanaharsip_b4d" text,
    "khasanaharsip_b4e" text,
    "khasanaharsip_b4f" text,
    "khasanaharsip_b4g" text,
    "khasanaharsip_b5a" text,
    "khasanaharsip_b5b" text,
    "khasanaharsip_b5c" text,
    "khasanaharsip_b5d" text,
    "khasanaharsip_b5e" text,
    "khasanaharsip_b5f" text,
    "khasanaharsip_b5g" text,
    "khasanaharsip_c1a" text,
    "khasanaharsip_c1b" text,
    "khasanaharsip_c1c" text,
    "khasanaharsip_c1d" text,
    "khasanaharsip_c1e" text,
    "khasanaharsip_c1f" text,
    "khasanaharsip_c1g" text,
    "khasanaharsip_c2a" text,
    "khasanaharsip_c2b" text,
    "khasanaharsip_c2c" text,
    "khasanaharsip_c2d" text,
    "khasanaharsip_c2e" text,
    "khasanaharsip_c2f" text,
    "khasanaharsip_c2g" text,
    "khasanaharsip_c3a" text,
    "khasanaharsip_c3b" text,
    "khasanaharsip_c3c" text,
    "khasanaharsip_c3d" text,
    "khasanaharsip_c3e" text,
    "khasanaharsip_c3f" text,
    "khasanaharsip_c3g" text,
    "khasanaharsip_c4a" text,
    "khasanaharsip_c4b" text,
    "khasanaharsip_c4c" text,
    "khasanaharsip_c4d" text,
    "khasanaharsip_c4e" text,
    "khasanaharsip_c4f" text,
    "khasanaharsip_c4g" text,
    "khasanaharsip_c5a" text,
    "khasanaharsip_c5b" text,
    "khasanaharsip_c5c" text,
    "khasanaharsip_c5d" text,
    "khasanaharsip_c5e" text,
    "khasanaharsip_c5f" text,
    "khasanaharsip_c5g" text,
    "khasanaharsip_d1a" text,
    "khasanaharsip_d1b" text,
    "khasanaharsip_d1c" text,
    "khasanaharsip_d1d" text,
    "khasanaharsip_d1e" text,
    "khasanaharsip_d1f" text,
    "khasanaharsip_d1g" text,
    "khasanaharsip_d2a" text,
    "khasanaharsip_d2b" text,
    "khasanaharsip_d2c" text,
    "khasanaharsip_d2d" text,
    "khasanaharsip_d2e" text,
    "khasanaharsip_d2f" text,
    "khasanaharsip_d2g" text,
    "khasanaharsip_d3a" text,
    "khasanaharsip_d3b" text,
    "khasanaharsip_d3c" text,
    "khasanaharsip_d3d" text,
    "khasanaharsip_d3e" text,
    "khasanaharsip_d3f" text,
    "khasanaharsip_d3g" text,
    "khasanaharsip_d4a" text,
    "khasanaharsip_d4b" text,
    "khasanaharsip_d4c" text,
    "khasanaharsip_d4d" text,
    "khasanaharsip_d4e" text,
    "khasanaharsip_d4f" text,
    "khasanaharsip_d4g" text,
    "khasanaharsip_d5a" text,
    "khasanaharsip_d5b" text,
    "khasanaharsip_d5c" text,
    "khasanaharsip_d5d" text,
    "khasanaharsip_d5e" text,
    "khasanaharsip_d5f" text,
    "khasanaharsip_d5g" text,
    "khasanaharsip_e1a" text,
    "khasanaharsip_e1b" text,
    "khasanaharsip_e1c" text,
    "khasanaharsip_e1d" text,
    "khasanaharsip_e1e" text,
    "khasanaharsip_e1f" text,
    "khasanaharsip_e1g" text,
    "khasanaharsip_e2a" text,
    "khasanaharsip_e2b" text,
    "khasanaharsip_e2c" text,
    "khasanaharsip_e2d" text,
    "khasanaharsip_e2e" text,
    "khasanaharsip_e2f" text,
    "khasanaharsip_e2g" text,
    "khasanaharsip_e3a" text,
    "khasanaharsip_e3b" text,
    "khasanaharsip_e3c" text,
    "khasanaharsip_e3d" text,
    "khasanaharsip_e3e" text,
    "khasanaharsip_e3f" text,
    "khasanaharsip_e3g" text,
    "khasanaharsip_e4a" text,
    "khasanaharsip_e4b" text,
    "khasanaharsip_e4c" text,
    "khasanaharsip_e4d" text,
    "khasanaharsip_e4e" text,
    "khasanaharsip_e4f" text,
    "khasanaharsip_e4g" text,
    "khasanaharsip_e5a" text,
    "khasanaharsip_e5b" text,
    "khasanaharsip_e5c" text,
    "khasanaharsip_e5d" text,
    "khasanaharsip_e5e" text,
    "khasanaharsip_e5f" text,
    "khasanaharsip_e5g" text,
    "khasanaharsip_f1a" text,
    "khasanaharsip_f1b" text,
    "khasanaharsip_f1c" text,
    "khasanaharsip_f1d" text,
    "khasanaharsip_f1e" text,
    "khasanaharsip_f1f" text,
    "khasanaharsip_f1g" text,
    "khasanaharsip_f2a" text,
    "khasanaharsip_f2b" text,
    "khasanaharsip_f2c" text,
    "khasanaharsip_f2d" text,
    "khasanaharsip_f2e" text,
    "khasanaharsip_f2f" text,
    "khasanaharsip_f2g" text,
    "khasanaharsip_f3a" text,
    "khasanaharsip_f3b" text,
    "khasanaharsip_f3c" text,
    "khasanaharsip_f3d" text,
    "khasanaharsip_f3e" text,
    "khasanaharsip_f3f" text,
    "khasanaharsip_f3g" text,
    "khasanaharsip_f4a" text,
    "khasanaharsip_f4b" text,
    "khasanaharsip_f4c" text,
    "khasanaharsip_f4d" text,
    "khasanaharsip_f4e" text,
    "khasanaharsip_f4f" text,
    "khasanaharsip_f4g" text,
    "khasanaharsip_f5a" text,
    "khasanaharsip_f5b" text,
    "khasanaharsip_f5c" text,
    "khasanaharsip_f5d" text,
    "khasanaharsip_f5e" text,
    "khasanaharsip_f5f" text,
    "khasanaharsip_f5g" text,
    "khasanaharsip_g1a" text,
    "khasanaharsip_g1b" text,
    "khasanaharsip_g1c" text,
    "khasanaharsip_g1d" text,
    "khasanaharsip_g1e" text,
    "khasanaharsip_g1f" text,
    "khasanaharsip_g1g" text,
    "khasanaharsip_g2a" text,
    "khasanaharsip_g2b" text,
    "khasanaharsip_g2c" text,
    "khasanaharsip_g2d" text,
    "khasanaharsip_g2e" text,
    "khasanaharsip_g2f" text,
    "khasanaharsip_g2g" text,
    "khasanaharsip_g3a" text,
    "khasanaharsip_g3b" text,
    "khasanaharsip_g3c" text,
    "khasanaharsip_g3d" text,
    "khasanaharsip_g3e" text,
    "khasanaharsip_g3f" text,
    "khasanaharsip_g3g" text,
    "khasanaharsip_g4a" text,
    "khasanaharsip_g4b" text,
    "khasanaharsip_g4c" text,
    "khasanaharsip_g4d" text,
    "khasanaharsip_g4e" text,
    "khasanaharsip_g4f" text,
    "khasanaharsip_g4g" text,
    "khasanaharsip_g5a" text,
    "khasanaharsip_g5b" text,
    "khasanaharsip_g5c" text,
    "khasanaharsip_g5d" text,
    "khasanaharsip_g5e" text,
    "khasanaharsip_g5f" text,
    "khasanaharsip_g5g" text,
    "khasanaharsip_h1a" text,
    "khasanaharsip_h1b" text,
    "khasanaharsip_h1c" text,
    "khasanaharsip_h1d" text,
    "khasanaharsip_h1e" text,
    "khasanaharsip_h1f" text,
    "khasanaharsip_h1g" text,
    "khasanaharsip_h2a" text,
    "khasanaharsip_h2b" text,
    "khasanaharsip_h2c" text,
    "khasanaharsip_h2d" text,
    "khasanaharsip_h2e" text,
    "khasanaharsip_h2f" text,
    "khasanaharsip_h2g" text,
    "khasanaharsip_h3a" text,
    "khasanaharsip_h3b" text,
    "khasanaharsip_h3c" text,
    "khasanaharsip_h3d" text,
    "khasanaharsip_h3e" text,
    "khasanaharsip_h3f" text,
    "khasanaharsip_h3g" text,
    "khasanaharsip_h4a" text,
    "khasanaharsip_h4b" text,
    "khasanaharsip_h4c" text,
    "khasanaharsip_h4d" text,
    "khasanaharsip_h4e" text,
    "khasanaharsip_h4f" text,
    "khasanaharsip_h4g" text,
    "khasanaharsip_h5a" text,
    "khasanaharsip_h5b" text,
    "khasanaharsip_h5c" text,
    "khasanaharsip_h5d" text,
    "khasanaharsip_h5e" text,
    "khasanaharsip_h5f" text,
    "khasanaharsip_h5g" text,
    "khasanaharsip_i1a" text,
    "khasanaharsip_i2a" text,
    "khasanaharsip_i2b" text,
    "khasanaharsip_i2c" text,
    "khasanaharsip_i2d" text,
    "khasanaharsip_i2e" text,
    "khasanaharsip_i2f" text,
    "khasanaharsip_i2g" text,
    "khasanaharsip_i3a" text,
    "khasanaharsip_i3b" text,
    "khasanaharsip_i3c" text,
    "khasanaharsip_i3d" text,
    "khasanaharsip_i3e" text,
    "khasanaharsip_i3f" text,
    "khasanaharsip_i3g" text,
    "khasanaharsip_i4a" text,
    "khasanaharsip_i4b" text,
    "khasanaharsip_i4c" text,
    "khasanaharsip_i4d" text,
    "khasanaharsip_i4e" text,
    "khasanaharsip_i4f" text,
    "khasanaharsip_i4g" text,
    "khasanaharsip_i5a" text,
    "khasanaharsip_i5b" text,
    "khasanaharsip_i5c" text,
    "khasanaharsip_i5d" text,
    "khasanaharsip_i5e" text,
    "khasanaharsip_i5f" text,
    "khasanaharsip_i5g" text,
    "khasanaharsip_i6a" text,
    "khasanaharsip_i6b" text,
    "khasanaharsip_i6c" text,
    "khasanaharsip_i6d" text,
    "khasanaharsip_i6e" text,
    "khasanaharsip_i6f" text,
    "khasanaharsip_i6g" text,
    "sistempengelolaanarsip_1a" text,
    "sistempengelolaanarsip_1b" text,
    "sistempengelolaanarsip_1c" text,
    "sistempengelolaanarsip_2a" text,
    "sistempengelolaanarsip_2b" text,
    "sistempengelolaanarsip_2c" text,
    "sistempengelolaanarsip_3a" text,
    "sistempengelolaanarsip_3b" text,
    "sistempengelolaanarsip_3c" text,
    "saranaprasarana_a1" text,
    "saranaprasarana_a2" text,
    "saranaprasarana_a3" text,
    "saranaprasarana_b1" text,
    "saranaprasarana_b2" text,
    "saranaprasarana_b3" text,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "jawaban_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "jenis_arsip";
DROP SEQUENCE IF EXISTS jenis_arsip_id_seq;
CREATE SEQUENCE jenis_arsip_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."jenis_arsip" (
    "id" bigint DEFAULT nextval('jenis_arsip_id_seq') NOT NULL,
    "nama" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "jenis_arsip_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "kinerja";
DROP SEQUENCE IF EXISTS kinerja_id_seq;
CREATE SEQUENCE kinerja_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."kinerja" (
    "id" bigint DEFAULT nextval('kinerja_id_seq') NOT NULL,
    "workunit_id" bigint NOT NULL,
    "category_id" bigint NOT NULL,
    "sub_category_id" bigint NOT NULL,
    "year" integer NOT NULL,
    "total" integer NOT NULL,
    "description" character varying(255),
    "status" integer DEFAULT '1' NOT NULL,
    "satuan" character varying(255),
    "produsendata_id" bigint NOT NULL,
    "masterdata_id" bigint,
    "chat_id" bigint,
    "pengolahdata_id" bigint,
    "fileupload_id" bigint,
    "save_at" timestamp(0),
    "process_at" timestamp(0),
    "send_at" timestamp(0),
    "verified_at" timestamp(0),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "kinerja_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "kinerja" ("id", "workunit_id", "category_id", "sub_category_id", "year", "total", "description", "status", "satuan", "produsendata_id", "masterdata_id", "chat_id", "pengolahdata_id", "fileupload_id", "save_at", "process_at", "send_at", "verified_at", "created_at", "updated_at") VALUES
(1,	3,	1,	2,	2019,	100,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47'),
(2,	3,	1,	3,	2019,	62,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:48',	'2022-12-23 11:08:48'),
(3,	3,	1,	4,	2019,	60,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:48',	'2022-12-23 11:08:48'),
(4,	3,	1,	5,	2019,	33,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:49',	'2022-12-23 11:08:49'),
(5,	3,	1,	2,	2020,	41,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:50',	'2022-12-23 11:08:50'),
(6,	3,	1,	3,	2020,	84,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:50',	'2022-12-23 11:08:50'),
(7,	3,	1,	4,	2020,	47,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:51',	'2022-12-23 11:08:51'),
(8,	3,	1,	5,	2020,	73,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:51',	'2022-12-23 11:08:51'),
(9,	3,	1,	2,	2021,	23,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:52',	'2022-12-23 11:08:52'),
(10,	3,	1,	3,	2021,	97,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:53',	'2022-12-23 11:08:53'),
(11,	3,	1,	4,	2021,	46,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:53',	'2022-12-23 11:08:53'),
(12,	3,	1,	5,	2021,	13,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:54',	'2022-12-23 11:08:54'),
(13,	3,	1,	2,	2022,	53,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:54',	'2022-12-23 11:08:54'),
(14,	3,	1,	3,	2022,	25,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:55',	'2022-12-23 11:08:55'),
(15,	3,	1,	4,	2022,	42,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:56',	'2022-12-23 11:08:56'),
(16,	3,	1,	5,	2022,	95,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:56',	'2022-12-23 11:08:56'),
(17,	3,	1,	2,	2023,	83,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:57',	'2022-12-23 11:08:57'),
(18,	3,	1,	3,	2023,	21,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:57',	'2022-12-23 11:08:57'),
(19,	3,	1,	4,	2023,	46,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:58',	'2022-12-23 11:08:58'),
(20,	3,	1,	5,	2023,	35,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:59',	'2022-12-23 11:08:59'),
(21,	3,	1,	2,	2024,	73,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:59',	'2022-12-23 11:08:59'),
(22,	3,	1,	3,	2024,	29,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:09:00',	'2022-12-23 11:09:00'),
(23,	3,	1,	4,	2024,	91,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:09:00',	'2022-12-23 11:09:00'),
(24,	3,	1,	5,	2024,	79,	NULL,	4,	'orang',	4,	5,	NULL,	3,	NULL,	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:08:47',	'2022-12-23 11:09:01',	'2022-12-23 11:09:01');

DROP TABLE IF EXISTS "messages";
DROP SEQUENCE IF EXISTS messages_id_seq;
CREATE SEQUENCE messages_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."messages" (
    "id" bigint DEFAULT nextval('messages_id_seq') NOT NULL,
    "chat_id" bigint NOT NULL,
    "sender_id" bigint NOT NULL,
    "receiver_id" bigint NOT NULL,
    "message" text NOT NULL,
    "read_at" timestamp(0),
    "sent_at" timestamp(0),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "messages_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "migrations";
DROP SEQUENCE IF EXISTS migrations_id_seq;
CREATE SEQUENCE migrations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."migrations" (
    "id" integer DEFAULT nextval('migrations_id_seq') NOT NULL,
    "migration" character varying(255) NOT NULL,
    "batch" integer NOT NULL,
    CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "migrations" ("id", "migration", "batch") VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2016_06_01_000001_create_oauth_auth_codes_table',	1),
(4,	'2016_06_01_000002_create_oauth_access_tokens_table',	1),
(5,	'2016_06_01_000003_create_oauth_refresh_tokens_table',	1),
(6,	'2016_06_01_000004_create_oauth_clients_table',	1),
(7,	'2016_06_01_000005_create_oauth_personal_access_clients_table',	1),
(8,	'2019_08_19_000000_create_failed_jobs_table',	1),
(9,	'2019_12_14_000001_create_personal_access_tokens_table',	1),
(10,	'2022_07_27_143118_create_permission_tables',	1),
(11,	'2022_07_30_220017_create_activity_log_table',	1),
(12,	'2022_08_22_102222_create_categories_table',	1),
(13,	'2022_08_23_092419_create_work_units_table',	1),
(14,	'2022_08_26_102004_create_kinerjas_table',	1),
(15,	'2022_08_30_113622_create_file_uploads_table',	1),
(16,	'2022_08_31_214031_create_news_table',	1),
(17,	'2022_08_31_214113_create_infografis_table',	1),
(18,	'2022_08_31_215059_create_views_table',	1),
(19,	'2022_09_24_223520_create_notifications_table',	1),
(20,	'2022_10_17_151027_create_chats_table',	1),
(21,	'2022_10_17_155543_create_messages_table',	1),
(22,	'2022_11_01_094426_create_instansis_table',	1),
(23,	'2022_11_02_101647_create_sasaran_strategis_table',	1),
(24,	'2022_11_02_142507_create_arsip_kolektors_table',	1),
(25,	'2022_11_09_132931_create_email_settings_table',	1),
(26,	'2022_11_09_133721_create_app_settings_table',	1),
(27,	'2022_11_09_133753_create_telegram_settings_table',	1),
(28,	'2022_11_20_122431_create_tujuans_table',	1),
(29,	'2022_11_20_210208_create_realisasi_anggarans_table',	1),
(30,	'2022_11_20_232438_create_capaians_table',	1),
(31,	'2022_11_21_110328_create_indikator_kinerjas_table',	1),
(32,	'2022_11_21_111048_create_target_kinerjas_table',	1),
(33,	'2022_11_21_114026_create_perjanjian_kinerjas_table',	1),
(34,	'2022_11_24_093900_create_peran_strategis_table',	1),
(35,	'2022_11_24_094848_create_survey_arsips_table',	1),
(36,	'2022_11_24_140556_create_rencana_strategis_table',	1),
(37,	'2022_11_24_141629_create_capaian_kinerjas_table',	1),
(38,	'2022_11_26_212710_create_rencanastrategis_tujuan_table',	1),
(39,	'2022_11_26_213124_create_rencanastrategis_sasaranstrategis_table',	1),
(40,	'2022_11_27_144909_create_perjanjiankinerja_tujuan_table',	1),
(41,	'2022_11_27_144930_create_perjanjiankinerja_sasaranstrategis_table',	1),
(42,	'2022_11_29_101507_create_visi_misis_table',	1),
(43,	'2022_11_29_101757_create_tugas_fungsis_table',	1),
(44,	'2022_11_29_102414_create_struktur_organisasis_table',	1),
(45,	'2022_11_30_091529_create_model_kinerjas_table',	1),
(46,	'2022_11_30_093602_create_capaiankinerja_modelkinerja_table',	1),
(47,	'2022_11_30_093627_create_capaian_modelkinerja_table',	1),
(48,	'2022_11_30_093926_create_modelkinerja_realisasianggaran_table',	1),
(49,	'2022_12_04_145737_create_capaian_s_p_b_e_s_table',	1),
(50,	'2022_12_06_152737_create_jawabans_table',	1),
(51,	'2022_12_12_145902_create_jenis_arsips_table',	1),
(52,	'2022_12_12_152320_create_glosarium_metadata_table',	1),
(53,	'2022_12_13_092747_create_arsip_digitals_table',	1);

DROP TABLE IF EXISTS "model_has_permissions";
CREATE TABLE "public"."model_has_permissions" (
    "permission_id" bigint NOT NULL,
    "model_type" character varying(255) NOT NULL,
    "model_id" bigint NOT NULL,
    CONSTRAINT "model_has_permissions_pkey" PRIMARY KEY ("permission_id", "model_id", "model_type")
) WITH (oids = false);

CREATE INDEX "model_has_permissions_model_id_model_type_index" ON "public"."model_has_permissions" USING btree ("model_id", "model_type");


DROP TABLE IF EXISTS "model_has_roles";
CREATE TABLE "public"."model_has_roles" (
    "role_id" bigint NOT NULL,
    "model_type" character varying(255) NOT NULL,
    "model_id" bigint NOT NULL,
    CONSTRAINT "model_has_roles_pkey" PRIMARY KEY ("role_id", "model_id", "model_type")
) WITH (oids = false);

CREATE INDEX "model_has_roles_model_id_model_type_index" ON "public"."model_has_roles" USING btree ("model_id", "model_type");

INSERT INTO "model_has_roles" ("role_id", "model_type", "model_id") VALUES
(1,	'App\Models\User',	1),
(4,	'App\Models\User',	2),
(6,	'App\Models\User',	3),
(5,	'App\Models\User',	4),
(7,	'App\Models\User',	5);

DROP TABLE IF EXISTS "model_kinerja";
DROP SEQUENCE IF EXISTS model_kinerja_id_seq;
CREATE SEQUENCE model_kinerja_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."model_kinerja" (
    "id" bigint DEFAULT nextval('model_kinerja_id_seq') NOT NULL,
    "judul" character varying(255) NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "pengantar" text NOT NULL,
    "user_id" bigint NOT NULL,
    "visimisi_id" bigint NOT NULL,
    "core_values_asn" text NOT NULL,
    "nilai_nilai_anri" text NOT NULL,
    "latar_belakang" text NOT NULL,
    "tugasfungsi_id" bigint NOT NULL,
    "strukturorganisasi_id" bigint NOT NULL,
    "peranstrategis_id" bigint NOT NULL,
    "sistematika_penyajian" text NOT NULL,
    "rencanastrategis_id" bigint NOT NULL,
    "perjanjiankinerja_id" bigint NOT NULL,
    "capaianspbe_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "model_kinerja_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "modelkinerja_realisasianggaran";
DROP SEQUENCE IF EXISTS modelkinerja_realisasianggaran_id_seq;
CREATE SEQUENCE modelkinerja_realisasianggaran_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."modelkinerja_realisasianggaran" (
    "id" bigint DEFAULT nextval('modelkinerja_realisasianggaran_id_seq') NOT NULL,
    "modelkinerja_id" bigint NOT NULL,
    "realisasianggaran_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "modelkinerja_realisasianggaran_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "news";
DROP SEQUENCE IF EXISTS news_id_seq;
CREATE SEQUENCE news_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."news" (
    "id" bigint DEFAULT nextval('news_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "slug" character varying(255) NOT NULL,
    "title" character varying(255) NOT NULL,
    "content" text NOT NULL,
    "image" character varying(255),
    "status" boolean DEFAULT true NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "news_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "news" ("id", "user_id", "slug", "title", "content", "image", "status", "created_at", "updated_at") VALUES
(1,	1,	'gerakan-tertib-arsip-dan-sejarah-desa-resmi-digaungkan',	'Gerakan Tertib Arsip dan Sejarah Desa Resmi Digaungkan',	'<p style="text-align:justify"><span style="font-size:13px"><span style="color:#777777"><span style="background-color:#ffffff"><span style="font-family:Arial,sans-serif">Jakarta &ndash; (22/12/2022), Sebagai sebuah upaya dalam menciptakan pengelolaan arsip desa dan sejarah desa yang andal, Arsip Nasional Republik Indonesia (ANRI) dan Kementerian Desa, Pembangunan Daerah Tertinggal dan Transmigrasi (KDPDTT) mengeluarkan kebijakan Gerakan Tertib Arsip dan Sejarah Desa dalam bentuk Surat Edaran Bersama Kepala ANRI dan Menteri DPDTT Nomor 3 Tahun 2022. Kebijakan tersebut dikupas dalam webinar yang diselenggarakan ANRI dan KDPDTT.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:13px"><span style="color:#777777"><span style="background-color:#ffffff"><span style="font-family:Arial,sans-serif">Menurut Kepala ANRI, Imam Gunarto saat membuka acara, Gerakan Tertib Arsip dan Sejarah Desa memiliki beberapa urgensi, di antaranya karena desa merupakan ujung tombak pembangunan dan layanan publik. Negara hadir untuk rakyatnya dimulai pada penyelenggaraan layanan publik di desa, sehingga arsip yang tercipta dari kegiatan pemerintahan, pembangunan dan pemberdayaan di desa perlu dikelola dengan baik. &ldquo;Gerakan ini juga tidak hanya digencarkan oleh ANRI dan KDPDTT, tetapi dapat menjadi program bersama dengan Lembaga Kearsipan Daerah (LKD), baik tingkat provinsi maupun kabupaten/kota,&rdquo; terang Imam.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:13px"><span style="color:#777777"><span style="background-color:#ffffff"><span style="font-family:Arial,sans-serif">Ditambahkan olehnya, dengan adanya desa yang tertib dalam pengelolaan arsip, dapat tercipta memori desa, sehingga dapat memperanjang ingatan dan sejarah desa. Selain itu, desa tangguh bencana yang sudah memiliki kesadaran yang tinggi dalam penyelamatan pelindungan arsip dari bencana juga dapat tercipta,&rdquo; tambahnya. Menutup sambutannya, Imam juga menyampaikan agar pada 2023 ANRI bekerja sama dengan LKD provinsi dan kabupaten/kota dapat menggelar pemilihan desa dengan pengelolaan arsip terbaik di tingkat provinsi ataupun nasional.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:13px"><span style="color:#777777"><span style="background-color:#ffffff"><span style="font-family:Arial,sans-serif">Sementara itu, Sekretaris Jenderal KDPDTT, Taufik Madjid mengemukakan bahwa pengelolaan arsip yang logis dan teratur menjadi perhatian KDPDTT. Kementerian Desa PDTT juga melangkah menggerakkan perbaikan arsip di desa-desa. &ldquo;Sejak tahun 2021, Kementerian Desa, PDTT telah menyediakan loker desa, agar desa-desa dapat menyimpan dokumen penting secara daring. Dokumen desa bisa disimpan, tanpa bisa dihapus, agar arsip desa tertata, tersimpan, dan tidak hilang meski berganti kepempimpinan. Penyimpanan elektronik membuka peluang laporan dana desa disusun secara sederhana dan ringkas, karena lampirannya tersedia di loker desa,&rdquo; jelas Taufik.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:13px"><span style="color:#777777"><span style="background-color:#ffffff"><span style="font-family:Arial,sans-serif">Ditambahkan olehnya, dalam konteks pencapaian tujuan SDGs Desa, penyelenggaraan kearsipan, menjadi bagian dari pencapaian sasaran-sasaran SDGs Desa Tujuan ke-16 Desa Damai Berkeadilan, dan tujuan ke-18 Kelembagaan Desa Dinamis dan Budaya Desa Adaptif.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:13px"><span style="color:#777777"><span style="background-color:#ffffff"><span style="font-family:Arial,sans-serif">Pada webinar ini, turut hadir pula sebagai pembicara utama, Duta Arsip Indonesia, Rieke Dyah Pitaloka yang menyampaikan pentingnya tertib arsip di desa untuk mempermudah mencari dan meekam data atas pembangunan yang dilaksanakan di desa. Menurutnya, pembangunan dimulai dari desa, desa bukan pinggiran, desa adalah awal perjuangan dan pusat peradaban. Pembangunan desa yang demokratis salah satunya berbasis pada data yang akurat. Data ini bersumber dari arsip desa. &ldquo;Berbicara arsip juga berbicara data. Menyelamatkan arsip berarti juga menyelamatkan data. Oleh karenanya ini tidak hanya tugas dari ANRI atau KDPDTT, tetapi juga butuh dukungan dari berbagai pihak,&rdquo; tambah Rieke.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:13px"><span style="color:#777777"><span style="background-color:#ffffff"><span style="font-family:Arial,sans-serif">Selain itu, turut hadir pula narasumber pada sesi diskusi, yaitu</span>&nbsp;<span style="font-family:Arial,sans-serif">Deputi Bidang Pembinaan Kearsipan ANRI, Desi Pratiwi; Staf Ahli Menteri Bidang Pembangunan dan Kemasyarakatan KDPDTT, Bito Wikantosa; Dosen Sejarah Universitas Gadjah Mada, Sri Margana; Pegiat Literasi, Muhidin M. Dahlan, dan Budayawan, Taufik Rhazen serta moderator Arsiparis Madya ANRI, Widhi Setyo Putro.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:13px"><span style="color:#777777"><span style="background-color:#ffffff"><span style="font-family:Arial,sans-serif">Sebagai informasi, Surat Edaran Bersama Kepala ANRI dan Menteri DPDTT&nbsp;Nomor 3 Tahun 2022 tentang Gerakan Tertib Arsip dan Sejarah Desa telah ditetapkan pada 10 Oktober 2022. Surat Edaran Bersama ini sebagai bagian dari tindak lanjut implementasi Undang-Undang Nomor 6 Tahun 2014 tentang Desa dan Undang-Undang Nomor 43 Tahun 2009 tentang Kearsipan.</span></span></span></span></p>',	'arsip-nasional-republik-indonesia-1671069949-y2fjg.jpeg',	't',	'2022-12-23 20:22:39',	'2022-12-24 12:06:28');

DROP TABLE IF EXISTS "notifications";
DROP SEQUENCE IF EXISTS notifications_id_seq;
CREATE SEQUENCE notifications_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."notifications" (
    "id" bigint DEFAULT nextval('notifications_id_seq') NOT NULL,
    "title" character varying(255) NOT NULL,
    "message" character varying(255) NOT NULL,
    "user_id" bigint,
    "read_at" timestamp(0),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "notifications_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "oauth_access_tokens";
CREATE TABLE "public"."oauth_access_tokens" (
    "id" character varying(100) NOT NULL,
    "user_id" bigint,
    "client_id" bigint NOT NULL,
    "name" character varying(255),
    "scopes" text,
    "revoked" boolean NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "expires_at" timestamp(0),
    CONSTRAINT "oauth_access_tokens_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "oauth_access_tokens_user_id_index" ON "public"."oauth_access_tokens" USING btree ("user_id");


DROP TABLE IF EXISTS "oauth_auth_codes";
CREATE TABLE "public"."oauth_auth_codes" (
    "id" character varying(100) NOT NULL,
    "user_id" bigint NOT NULL,
    "client_id" bigint NOT NULL,
    "scopes" text,
    "revoked" boolean NOT NULL,
    "expires_at" timestamp(0),
    CONSTRAINT "oauth_auth_codes_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "oauth_auth_codes_user_id_index" ON "public"."oauth_auth_codes" USING btree ("user_id");


DROP TABLE IF EXISTS "oauth_clients";
DROP SEQUENCE IF EXISTS oauth_clients_id_seq;
CREATE SEQUENCE oauth_clients_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."oauth_clients" (
    "id" bigint DEFAULT nextval('oauth_clients_id_seq') NOT NULL,
    "user_id" bigint,
    "name" character varying(255) NOT NULL,
    "secret" character varying(100),
    "provider" character varying(255),
    "redirect" text NOT NULL,
    "personal_access_client" boolean NOT NULL,
    "password_client" boolean NOT NULL,
    "revoked" boolean NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "oauth_clients_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "oauth_clients_user_id_index" ON "public"."oauth_clients" USING btree ("user_id");


DROP TABLE IF EXISTS "oauth_personal_access_clients";
DROP SEQUENCE IF EXISTS oauth_personal_access_clients_id_seq;
CREATE SEQUENCE oauth_personal_access_clients_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."oauth_personal_access_clients" (
    "id" bigint DEFAULT nextval('oauth_personal_access_clients_id_seq') NOT NULL,
    "client_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "oauth_personal_access_clients_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "oauth_refresh_tokens";
CREATE TABLE "public"."oauth_refresh_tokens" (
    "id" character varying(100) NOT NULL,
    "access_token_id" character varying(100) NOT NULL,
    "revoked" boolean NOT NULL,
    "expires_at" timestamp(0),
    CONSTRAINT "oauth_refresh_tokens_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "oauth_refresh_tokens_access_token_id_index" ON "public"."oauth_refresh_tokens" USING btree ("access_token_id");


DROP TABLE IF EXISTS "password_resets";
CREATE TABLE "public"."password_resets" (
    "email" character varying(255) NOT NULL,
    "token" character varying(255) NOT NULL,
    "created_at" timestamp(0)
) WITH (oids = false);

CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree ("email");


DROP TABLE IF EXISTS "peran_strategis";
DROP SEQUENCE IF EXISTS peran_strategis_id_seq;
CREATE SEQUENCE peran_strategis_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."peran_strategis" (
    "id" bigint DEFAULT nextval('peran_strategis_id_seq') NOT NULL,
    "peran_strategis" text NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "peran_strategis_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "perjanjian_kinerja";
DROP SEQUENCE IF EXISTS perjanjian_kinerja_id_seq;
CREATE SEQUENCE perjanjian_kinerja_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."perjanjian_kinerja" (
    "id" bigint DEFAULT nextval('perjanjian_kinerja_id_seq') NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "perjanjian_kinerja_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "perjanjian_kinerja_tahun_unique" UNIQUE ("tahun")
) WITH (oids = false);


DROP TABLE IF EXISTS "perjanjiankinerja_sasaranstrategis";
DROP SEQUENCE IF EXISTS perjanjiankinerja_sasaranstrategis_id_seq;
CREATE SEQUENCE perjanjiankinerja_sasaranstrategis_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."perjanjiankinerja_sasaranstrategis" (
    "id" bigint DEFAULT nextval('perjanjiankinerja_sasaranstrategis_id_seq') NOT NULL,
    "perjanjiankinerja_id" bigint NOT NULL,
    "sasaranstrategis_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "perjanjiankinerja_sasaranstrategis_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "perjanjiankinerja_tujuan";
DROP SEQUENCE IF EXISTS perjanjiankinerja_tujuan_id_seq;
CREATE SEQUENCE perjanjiankinerja_tujuan_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."perjanjiankinerja_tujuan" (
    "id" bigint DEFAULT nextval('perjanjiankinerja_tujuan_id_seq') NOT NULL,
    "perjanjiankinerja_id" bigint NOT NULL,
    "tujuan_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "perjanjiankinerja_tujuan_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "permissions";
DROP SEQUENCE IF EXISTS permissions_id_seq;
CREATE SEQUENCE permissions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."permissions" (
    "id" bigint DEFAULT nextval('permissions_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "module" character varying(255) NOT NULL,
    "guard_name" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "permissions_name_guard_name_unique" UNIQUE ("name", "guard_name"),
    CONSTRAINT "permissions_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "permissions" ("id", "name", "module", "guard_name", "created_at", "updated_at") VALUES
(1,	'Dashboard Admin',	'Dashboard',	'web',	'2022-12-23 11:03:06',	'2022-12-23 11:03:06'),
(2,	'Dashboard Eksekutif',	'Dashboard',	'web',	'2022-12-23 11:03:08',	'2022-12-23 11:03:08'),
(3,	'Dashboard',	'Dashboard',	'web',	'2022-12-23 11:03:09',	'2022-12-23 11:03:09'),
(4,	'Statistik Produsen Data',	'Dashboard',	'web',	'2022-12-23 11:03:11',	'2022-12-23 11:03:11'),
(5,	'Statistik Master Data',	'Dashboard',	'web',	'2022-12-23 11:03:13',	'2022-12-23 11:03:13'),
(6,	'Statistik Pengolah Data',	'Dashboard',	'web',	'2022-12-23 11:03:15',	'2022-12-23 11:03:15'),
(7,	'Menu Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:17',	'2022-12-23 11:03:17'),
(8,	'List Survey Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:19',	'2022-12-23 11:03:19'),
(9,	'List Responden Survey Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:21',	'2022-12-23 11:03:21'),
(10,	'Isi Survey Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:22',	'2022-12-23 11:03:22'),
(11,	'Edit Jawaban Survey Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:24',	'2022-12-23 11:03:24'),
(12,	'Detail Jawaban Survey Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:26',	'2022-12-23 11:03:26'),
(13,	'Import Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:28',	'2022-12-23 11:03:28'),
(14,	'List Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:30',	'2022-12-23 11:03:30'),
(15,	'Edit Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:32',	'2022-12-23 11:03:32'),
(16,	'Detail Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:34',	'2022-12-23 11:03:34'),
(17,	'Delete Arsip Kolektor',	'Arsip Kolektor',	'web',	'2022-12-23 11:03:35',	'2022-12-23 11:03:35'),
(18,	'Menu Metadata Management',	'Metadata Management',	'web',	'2022-12-23 11:03:37',	'2022-12-23 11:03:37'),
(19,	'List Metadata Management',	'Metadata Management',	'web',	'2022-12-23 11:03:39',	'2022-12-23 11:03:39'),
(20,	'Tambah Metadata Management',	'Metadata Management',	'web',	'2022-12-23 11:03:41',	'2022-12-23 11:03:41'),
(21,	'Edit Metadata Management',	'Metadata Management',	'web',	'2022-12-23 11:03:43',	'2022-12-23 11:03:43'),
(22,	'Hapus Metadata Management',	'Metadata Management',	'web',	'2022-12-23 11:03:45',	'2022-12-23 11:03:45'),
(23,	'Menu Identifikasi Arsip',	'Identifikasi Arsip',	'web',	'2022-12-23 11:03:46',	'2022-12-23 11:03:46'),
(24,	'List Identifikasi Arsip',	'Identifikasi Arsip',	'web',	'2022-12-23 11:03:48',	'2022-12-23 11:03:48'),
(25,	'Tambah Identifikasi Arsip',	'Identifikasi Arsip',	'web',	'2022-12-23 11:03:50',	'2022-12-23 11:03:50'),
(26,	'Edit Identifikasi Arsip',	'Identifikasi Arsip',	'web',	'2022-12-23 11:03:52',	'2022-12-23 11:03:52'),
(27,	'Hapus Identifikasi Arsip',	'Identifikasi Arsip',	'web',	'2022-12-23 11:03:54',	'2022-12-23 11:03:54'),
(28,	'Menu Master Data',	'Master Data',	'web',	'2022-12-23 11:03:56',	'2022-12-23 11:03:56'),
(29,	'List Unit Kerja',	'Master Data',	'web',	'2022-12-23 11:03:57',	'2022-12-23 11:03:57'),
(30,	'Tambah Unit Kerja',	'Master Data',	'web',	'2022-12-23 11:03:59',	'2022-12-23 11:03:59'),
(31,	'Edit Unit Kerja',	'Master Data',	'web',	'2022-12-23 11:04:01',	'2022-12-23 11:04:01'),
(32,	'Hapus Unit Kerja',	'Master Data',	'web',	'2022-12-23 11:04:03',	'2022-12-23 11:04:03'),
(33,	'List Survey',	'Master Data',	'web',	'2022-12-23 11:04:05',	'2022-12-23 11:04:05'),
(34,	'Tambah Survey',	'Master Data',	'web',	'2022-12-23 11:04:07',	'2022-12-23 11:04:07'),
(35,	'Edit Survey',	'Master Data',	'web',	'2022-12-23 11:04:09',	'2022-12-23 11:04:09'),
(36,	'Delete Survey',	'Master Data',	'web',	'2022-12-23 11:04:11',	'2022-12-23 11:04:11'),
(37,	'List Jenis Arsip',	'Master Data',	'web',	'2022-12-23 11:04:12',	'2022-12-23 11:04:12'),
(38,	'Tambah Jenis Arsip',	'Master Data',	'web',	'2022-12-23 11:04:14',	'2022-12-23 11:04:14'),
(39,	'Edit Jenis Arsip',	'Master Data',	'web',	'2022-12-23 11:04:16',	'2022-12-23 11:04:16'),
(40,	'Delete Jenis Arsip',	'Master Data',	'web',	'2022-12-23 11:04:18',	'2022-12-23 11:04:18'),
(41,	'Menu Modul Data Kinerja',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:20',	'2022-12-23 11:04:20'),
(42,	'Grafik Data Kinerja',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:22',	'2022-12-23 11:04:22'),
(43,	'List Kategori',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:24',	'2022-12-23 11:04:24'),
(44,	'Tambah Kategori',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:26',	'2022-12-23 11:04:26'),
(45,	'Edit Kategori',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:27',	'2022-12-23 11:04:27'),
(46,	'Hapus Kategori',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:29',	'2022-12-23 11:04:29'),
(47,	'List Sub Kategori',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:31',	'2022-12-23 11:04:31'),
(48,	'Tambah Sub Kategori',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:33',	'2022-12-23 11:04:33'),
(49,	'Edit Sub Kategori',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:35',	'2022-12-23 11:04:35'),
(50,	'Hapus Sub Kategori',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:37',	'2022-12-23 11:04:37'),
(51,	'List Data Kinerja',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:39',	'2022-12-23 11:04:39'),
(52,	'Tambah Data Kinerja',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:40',	'2022-12-23 11:04:40'),
(53,	'Detail Data Kinerja',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:42',	'2022-12-23 11:04:42'),
(54,	'Edit Data Kinerja',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:44',	'2022-12-23 11:04:44'),
(55,	'Hapus Data Kinerja',	'Modul Data Kinerja',	'web',	'2022-12-23 11:04:46',	'2022-12-23 11:04:46'),
(56,	'Kotak Data Masuk Master Data',	'Kotak Masuk',	'web',	'2022-12-23 11:04:48',	'2022-12-23 11:04:48'),
(57,	'Kotak Data Masuk Pengolah Data',	'Kotak Masuk',	'web',	'2022-12-23 11:04:50',	'2022-12-23 11:04:50'),
(58,	'List Visi & Misi',	'Master Data Kinerja',	'web',	'2022-12-23 11:04:52',	'2022-12-23 11:04:52'),
(59,	'Tambah Visi & Misi',	'Master Data Kinerja',	'web',	'2022-12-23 11:04:54',	'2022-12-23 11:04:54'),
(60,	'Edit Visi & Misi',	'Master Data Kinerja',	'web',	'2022-12-23 11:04:55',	'2022-12-23 11:04:55'),
(61,	'Hapus Visi & Misi',	'Master Data Kinerja',	'web',	'2022-12-23 11:04:57',	'2022-12-23 11:04:57'),
(62,	'List Tugas & Fungsi',	'Master Data Kinerja',	'web',	'2022-12-23 11:04:59',	'2022-12-23 11:04:59'),
(63,	'Tambah Tugas & Fungsi',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:01',	'2022-12-23 11:05:01'),
(64,	'Edit Tugas & Fungsi',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:03',	'2022-12-23 11:05:03'),
(65,	'Hapus Tugas & Fungsi',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:05',	'2022-12-23 11:05:05'),
(66,	'List Struktur Organisasi',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:07',	'2022-12-23 11:05:07'),
(67,	'Tambah Struktur Organisasi',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:09',	'2022-12-23 11:05:09'),
(68,	'Edit Struktur Organisasi',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:11',	'2022-12-23 11:05:11'),
(69,	'Hapus Struktur Organisasi',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:13',	'2022-12-23 11:05:13'),
(70,	'List Indikator Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:15',	'2022-12-23 11:05:15'),
(71,	'Tambah Indikator Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:16',	'2022-12-23 11:05:16'),
(72,	'Edit Indikator Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:18',	'2022-12-23 11:05:18'),
(73,	'Hapus Indikator Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:20',	'2022-12-23 11:05:20'),
(74,	'List Target Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:22',	'2022-12-23 11:05:22'),
(75,	'Tambah Target Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:24',	'2022-12-23 11:05:24'),
(76,	'Edit Target Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:26',	'2022-12-23 11:05:26'),
(77,	'Hapus Target Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:28',	'2022-12-23 11:05:28'),
(78,	'List Peran Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:30',	'2022-12-23 11:05:30'),
(79,	'Tambah Peran Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:31',	'2022-12-23 11:05:31'),
(80,	'Edit Peran Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:33',	'2022-12-23 11:05:33'),
(81,	'Hapus Peran Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:35',	'2022-12-23 11:05:35'),
(82,	'List Sasaran Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:37',	'2022-12-23 11:05:37'),
(83,	'Tambah Sasaran Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:39',	'2022-12-23 11:05:39'),
(84,	'Edit Sasaran Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:41',	'2022-12-23 11:05:41'),
(85,	'Hapus Sasaran Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:43',	'2022-12-23 11:05:43'),
(86,	'List Rencana Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:45',	'2022-12-23 11:05:45'),
(87,	'Tambah Rencana Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:47',	'2022-12-23 11:05:47'),
(88,	'Edit Rencana Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:49',	'2022-12-23 11:05:49'),
(89,	'Detail Rencana Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:51',	'2022-12-23 11:05:51'),
(90,	'Hapus Rencana Strategis',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:53',	'2022-12-23 11:05:53'),
(91,	'List Tujuan',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:55',	'2022-12-23 11:05:55'),
(92,	'Tambah Tujuan',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:57',	'2022-12-23 11:05:57'),
(93,	'Edit Tujuan',	'Master Data Kinerja',	'web',	'2022-12-23 11:05:59',	'2022-12-23 11:05:59'),
(94,	'Hapus Tujuan',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:01',	'2022-12-23 11:06:01'),
(95,	'List Perjanjian Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:03',	'2022-12-23 11:06:03'),
(96,	'Tambah Perjanjian Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:05',	'2022-12-23 11:06:05'),
(97,	'Edit Perjanjian Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:07',	'2022-12-23 11:06:07'),
(98,	'Detail Perjanjian Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:08',	'2022-12-23 11:06:08'),
(99,	'Hapus Perjanjian Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:11',	'2022-12-23 11:06:11'),
(100,	'List Capaian & Realisasi Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:13',	'2022-12-23 11:06:13'),
(101,	'Tambah Capaian & Realisasi Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:14',	'2022-12-23 11:06:14'),
(102,	'Edit Capaian & Realisasi Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:16',	'2022-12-23 11:06:16'),
(103,	'Hapus Capaian & Realisasi Kinerja',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:18',	'2022-12-23 11:06:18'),
(104,	'List Capaian SPBE',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:20',	'2022-12-23 11:06:20'),
(105,	'Tambah Capaian SPBE',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:23',	'2022-12-23 11:06:23'),
(106,	'Edit Capaian SPBE',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:25',	'2022-12-23 11:06:25'),
(107,	'Hapus Capaian SPBE',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:28',	'2022-12-23 11:06:28'),
(108,	'List Capaian Lainnya',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:30',	'2022-12-23 11:06:30'),
(109,	'Tambah Capaian Lainnya',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:31',	'2022-12-23 11:06:31'),
(110,	'Edit Capaian Lainnya',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:33',	'2022-12-23 11:06:33'),
(111,	'Hapus Capaian Lainnya',	'Master Data Kinerja',	'web',	'2022-12-23 11:06:35',	'2022-12-23 11:06:35'),
(112,	'List Model Kinerja',	'Model Kinerja',	'web',	'2022-12-23 11:06:37',	'2022-12-23 11:06:37'),
(113,	'Tambah Model Kinerja',	'Model Kinerja',	'web',	'2022-12-23 11:06:39',	'2022-12-23 11:06:39'),
(114,	'Edit Model Kinerja',	'Model Kinerja',	'web',	'2022-12-23 11:06:41',	'2022-12-23 11:06:41'),
(115,	'Export Model Kinerja',	'Model Kinerja',	'web',	'2022-12-23 11:06:43',	'2022-12-23 11:06:43'),
(116,	'Detail Model Kinerja',	'Model Kinerja',	'web',	'2022-12-23 11:06:45',	'2022-12-23 11:06:45'),
(117,	'Hapus Model Kinerja',	'Model Kinerja',	'web',	'2022-12-23 11:06:47',	'2022-12-23 11:06:47'),
(118,	'Menu Informasi Publik',	'Informasi Publik',	'web',	'2022-12-23 11:06:49',	'2022-12-23 11:06:49'),
(119,	'List Berita',	'Informasi Publik',	'web',	'2022-12-23 11:06:51',	'2022-12-23 11:06:51'),
(120,	'Tambah Berita',	'Informasi Publik',	'web',	'2022-12-23 11:06:53',	'2022-12-23 11:06:53'),
(121,	'Edit Berita',	'Informasi Publik',	'web',	'2022-12-23 11:06:54',	'2022-12-23 11:06:54'),
(122,	'Hapus Berita',	'Informasi Publik',	'web',	'2022-12-23 11:06:56',	'2022-12-23 11:06:56'),
(123,	'List Infografis',	'Informasi Publik',	'web',	'2022-12-23 11:06:58',	'2022-12-23 11:06:58'),
(124,	'Tambah Infografis',	'Informasi Publik',	'web',	'2022-12-23 11:07:00',	'2022-12-23 11:07:00'),
(125,	'Edit Infografis',	'Informasi Publik',	'web',	'2022-12-23 11:07:02',	'2022-12-23 11:07:02'),
(126,	'Hapus Infografis',	'Informasi Publik',	'web',	'2022-12-23 11:07:05',	'2022-12-23 11:07:05'),
(127,	'Menu User Management',	'User Management',	'web',	'2022-12-23 11:07:07',	'2022-12-23 11:07:07'),
(128,	'List User',	'User Management',	'web',	'2022-12-23 11:07:08',	'2022-12-23 11:07:08'),
(129,	'Tambah User',	'User Management',	'web',	'2022-12-23 11:07:10',	'2022-12-23 11:07:10'),
(130,	'Edit User',	'User Management',	'web',	'2022-12-23 11:07:12',	'2022-12-23 11:07:12'),
(131,	'Hapus User',	'User Management',	'web',	'2022-12-23 11:07:14',	'2022-12-23 11:07:14'),
(132,	'Detail User',	'User Management',	'web',	'2022-12-23 11:07:16',	'2022-12-23 11:07:16'),
(133,	'List Role',	'User Management',	'web',	'2022-12-23 11:07:18',	'2022-12-23 11:07:18'),
(134,	'Tambah Role',	'User Management',	'web',	'2022-12-23 11:07:20',	'2022-12-23 11:07:20'),
(135,	'Edit Role',	'User Management',	'web',	'2022-12-23 11:07:22',	'2022-12-23 11:07:22'),
(136,	'Hapus Role',	'User Management',	'web',	'2022-12-23 11:07:24',	'2022-12-23 11:07:24'),
(137,	'List Hak Akses',	'User Management',	'web',	'2022-12-23 11:07:26',	'2022-12-23 11:07:26'),
(138,	'List Log Aktivitas',	'User Management',	'web',	'2022-12-23 11:07:28',	'2022-12-23 11:07:28'),
(139,	'User Profile',	'User Management',	'web',	'2022-12-23 11:07:30',	'2022-12-23 11:07:30'),
(140,	'Notifikasi Produsen Data',	'Notifikasi',	'web',	'2022-12-23 11:07:32',	'2022-12-23 11:07:32'),
(141,	'Notifikasi Master Data',	'Notifikasi',	'web',	'2022-12-23 11:07:34',	'2022-12-23 11:07:34'),
(142,	'Notifikasi Pengolah Data',	'Notifikasi',	'web',	'2022-12-23 11:07:36',	'2022-12-23 11:07:36'),
(143,	'List Instansi',	'Master Data',	'web',	'2022-12-23 11:07:38',	'2022-12-23 11:07:38'),
(144,	'Tambah Instansi',	'Master Data',	'web',	'2022-12-23 11:07:40',	'2022-12-23 11:07:40'),
(145,	'Edit Instansi',	'Master Data',	'web',	'2022-12-23 11:07:41',	'2022-12-23 11:07:41'),
(146,	'Hapus Instansi',	'Master Data',	'web',	'2022-12-23 11:07:43',	'2022-12-23 11:07:43'),
(147,	'List Sub Instansi',	'Master Data',	'web',	'2022-12-23 11:07:45',	'2022-12-23 11:07:45'),
(148,	'Tambah Sub Instansi',	'Master Data',	'web',	'2022-12-23 11:07:47',	'2022-12-23 11:07:47'),
(149,	'Edit Sub Instansi',	'Master Data',	'web',	'2022-12-23 11:07:49',	'2022-12-23 11:07:49'),
(150,	'Hapus Sub Instansi',	'Master Data',	'web',	'2022-12-23 11:07:51',	'2022-12-23 11:07:51');

DROP TABLE IF EXISTS "personal_access_tokens";
DROP SEQUENCE IF EXISTS personal_access_tokens_id_seq;
CREATE SEQUENCE personal_access_tokens_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."personal_access_tokens" (
    "id" bigint DEFAULT nextval('personal_access_tokens_id_seq') NOT NULL,
    "tokenable_type" character varying(255) NOT NULL,
    "tokenable_id" bigint NOT NULL,
    "name" character varying(255) NOT NULL,
    "token" character varying(64) NOT NULL,
    "abilities" text,
    "last_used_at" timestamp(0),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "personal_access_tokens_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "personal_access_tokens_token_unique" UNIQUE ("token")
) WITH (oids = false);

CREATE INDEX "personal_access_tokens_tokenable_type_tokenable_id_index" ON "public"."personal_access_tokens" USING btree ("tokenable_type", "tokenable_id");


DROP TABLE IF EXISTS "realisasi_anggaran";
DROP SEQUENCE IF EXISTS realisasi_anggaran_id_seq;
CREATE SEQUENCE realisasi_anggaran_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."realisasi_anggaran" (
    "id" bigint DEFAULT nextval('realisasi_anggaran_id_seq') NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "sasaranstrategis_id" bigint NOT NULL,
    "alokasi" character varying(255) NOT NULL,
    "realisasi" character varying(255) NOT NULL,
    "persentase" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "realisasi_anggaran_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "rencana_strategis";
DROP SEQUENCE IF EXISTS rencana_strategis_id_seq;
CREATE SEQUENCE rencana_strategis_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."rencana_strategis" (
    "id" bigint DEFAULT nextval('rencana_strategis_id_seq') NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "rencana_strategis_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "rencana_strategis_tahun_unique" UNIQUE ("tahun")
) WITH (oids = false);


DROP TABLE IF EXISTS "rencanastrategis_sasaranstrategis";
DROP SEQUENCE IF EXISTS rencanastrategis_sasaranstrategis_id_seq;
CREATE SEQUENCE rencanastrategis_sasaranstrategis_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."rencanastrategis_sasaranstrategis" (
    "id" bigint DEFAULT nextval('rencanastrategis_sasaranstrategis_id_seq') NOT NULL,
    "sasaranstrategis_id" bigint NOT NULL,
    "rencanastrategis_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "rencanastrategis_sasaranstrategis_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "rencanastrategis_tujuan";
DROP SEQUENCE IF EXISTS rencanastrategis_tujuan_id_seq;
CREATE SEQUENCE rencanastrategis_tujuan_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."rencanastrategis_tujuan" (
    "id" bigint DEFAULT nextval('rencanastrategis_tujuan_id_seq') NOT NULL,
    "rencanastrategis_id" bigint NOT NULL,
    "tujuan_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "rencanastrategis_tujuan_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "role_has_permissions";
CREATE TABLE "public"."role_has_permissions" (
    "permission_id" bigint NOT NULL,
    "role_id" bigint NOT NULL,
    CONSTRAINT "role_has_permissions_pkey" PRIMARY KEY ("permission_id", "role_id")
) WITH (oids = false);

INSERT INTO "role_has_permissions" ("permission_id", "role_id") VALUES
(139,	4),
(127,	4),
(1,	4),
(139,	6),
(6,	6),
(127,	6),
(139,	5),
(127,	5),
(4,	5),
(139,	7),
(127,	7),
(5,	7);

DROP TABLE IF EXISTS "roles";
DROP SEQUENCE IF EXISTS roles_id_seq;
CREATE SEQUENCE roles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."roles" (
    "id" bigint DEFAULT nextval('roles_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "description" text,
    "guard_name" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "roles_name_guard_name_unique" UNIQUE ("name", "guard_name"),
    CONSTRAINT "roles_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "roles" ("id", "name", "description", "guard_name", "created_at", "updated_at") VALUES
(1,	'super-admin',	'Super Admin',	'web',	'2022-12-23 11:07:53',	'2022-12-23 11:07:53'),
(2,	'Instansi',	'Instansi',	'web',	'2022-12-23 11:07:56',	'2022-12-23 11:07:56'),
(3,	'Eksekutif',	'Eksekutif',	'web',	'2022-12-23 11:07:58',	'2022-12-23 11:07:58'),
(4,	'admin',	'Administrator',	'web',	'2022-12-23 11:07:59',	'2022-12-23 11:07:59'),
(5,	'produsen-data',	'Produsen Data',	'web',	'2022-12-23 11:08:13',	'2022-12-23 11:08:13'),
(6,	'pengolah-data',	'Pengolah Data',	'web',	'2022-12-23 11:08:15',	'2022-12-23 11:08:15'),
(7,	'master-data',	'Master Data',	'web',	'2022-12-23 11:08:16',	'2022-12-23 11:08:16');

DROP TABLE IF EXISTS "sasaran_strategis";
DROP SEQUENCE IF EXISTS sasaran_strategis_id_seq;
CREATE SEQUENCE sasaran_strategis_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."sasaran_strategis" (
    "id" bigint DEFAULT nextval('sasaran_strategis_id_seq') NOT NULL,
    "sasaran_strategis" text NOT NULL,
    "indikatorkinerja_id" bigint NOT NULL,
    "targetkinerja_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "sasaran_strategis_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "struktur_organisasi";
DROP SEQUENCE IF EXISTS struktur_organisasi_id_seq;
CREATE SEQUENCE struktur_organisasi_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."struktur_organisasi" (
    "id" bigint DEFAULT nextval('struktur_organisasi_id_seq') NOT NULL,
    "file" character varying(255) NOT NULL,
    "status" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "struktur_organisasi_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "survey_arsip";
DROP SEQUENCE IF EXISTS survey_arsip_id_seq;
CREATE SEQUENCE survey_arsip_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."survey_arsip" (
    "id" bigint DEFAULT nextval('survey_arsip_id_seq') NOT NULL,
    "nama" character varying(255) NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "status" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "survey_arsip_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "target_kinerja";
DROP SEQUENCE IF EXISTS target_kinerja_id_seq;
CREATE SEQUENCE target_kinerja_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."target_kinerja" (
    "id" bigint DEFAULT nextval('target_kinerja_id_seq') NOT NULL,
    "indikatorkinerja_id" bigint NOT NULL,
    "tahun" character varying(255) NOT NULL,
    "target" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "target_kinerja_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "telegram_settings";
DROP SEQUENCE IF EXISTS telegram_settings_id_seq;
CREATE SEQUENCE telegram_settings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."telegram_settings" (
    "id" bigint DEFAULT nextval('telegram_settings_id_seq') NOT NULL,
    "telegram_bot_token" text NOT NULL,
    "username_bot" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "telegram_settings_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "telegram_settings" ("id", "telegram_bot_token", "username_bot", "created_at", "updated_at") VALUES
(1,	'eyJpdiI6ImNBbFQ2dEFmNmVGRDdFWUpXQUZZaGc9PSIsInZhbHVlIjoidm9VWFJ1a1huRFc1MFl5QnNrdHIzazF1V1lNc3hBRkZoMC9lMU91OFJaVU1SQlF1N0lhdjkvbXRTOFUwZk1YVCIsIm1hYyI6ImI3ZDQzY2ExNTRhM2ZjOWU5NjljYjlmMTZlZDdhNWQwNzc1OWZlY2NkYzkwOTBhMjY1OTk5OGUwMmZhNDM3YWEiLCJ0YWciOiIifQ==',	'sidata_anri',	'2022-12-23 11:08:44',	'2022-12-23 11:08:44');

DROP TABLE IF EXISTS "tugas_fungsi";
DROP SEQUENCE IF EXISTS tugas_fungsi_id_seq;
CREATE SEQUENCE tugas_fungsi_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."tugas_fungsi" (
    "id" bigint DEFAULT nextval('tugas_fungsi_id_seq') NOT NULL,
    "tugas" text NOT NULL,
    "fungsi" text NOT NULL,
    "kewenangan" text NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "tugas_fungsi_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "tujuan";
DROP SEQUENCE IF EXISTS tujuan_id_seq;
CREATE SEQUENCE tujuan_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."tujuan" (
    "id" bigint DEFAULT nextval('tujuan_id_seq') NOT NULL,
    "indikatorkinerja_id" bigint NOT NULL,
    "targetkinerja_id" bigint NOT NULL,
    "tujuan" text NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "tujuan_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."users" (
    "id" bigint DEFAULT nextval('users_id_seq') NOT NULL,
    "username" text NOT NULL,
    "name" text NOT NULL,
    "email" text NOT NULL,
    "phone" text,
    "jabatan" text,
    "workunit_id" text,
    "instansi_id" text,
    "email_verified_at" timestamp(0),
    "password" text NOT NULL,
    "last_seen" timestamp(0),
    "avatar" character varying(255),
    "telegram_id" text,
    "remember_token" character varying(100),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "users_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "users_username_unique" UNIQUE ("username")
) WITH (oids = false);

INSERT INTO "users" ("id", "username", "name", "email", "phone", "jabatan", "workunit_id", "instansi_id", "email_verified_at", "password", "last_seen", "avatar", "telegram_id", "remember_token", "created_at", "updated_at") VALUES
(2,	'admin',	'Admin',	'admin@gmail.com',	NULL,	NULL,	'1',	NULL,	'2022-12-23 11:08:00',	'$2y$10$jOhBYyHfPhxjHfzjoeAOJOjWB89dcbxcO01oLuJPna/TgIdvqEO2e',	NULL,	NULL,	NULL,	'4QmwMMoQew',	'2022-12-23 11:08:00',	'2022-12-23 11:08:00'),
(3,	'pengolah-data',	'Pengolah Data',	'pengolahdata@gmail.com',	NULL,	NULL,	'15',	NULL,	'2022-12-23 11:08:01',	'$2y$10$7O9BQUFEYHT4bDHE0zQFGuKgWdROCpARYRzxVzZRhv3XXN/F9VVV2',	NULL,	NULL,	'1442552516',	'6XbINUgnaa',	'2022-12-23 11:08:01',	'2022-12-23 11:08:01'),
(4,	'produsen-data',	'Produsen Data',	'produsendata@gmail.com',	NULL,	NULL,	'3',	NULL,	'2022-12-23 11:08:01',	'$2y$10$VmYNZvMhxVvlAfVJkNQY8upAUG9HTX7AWPWoMdY4efxRp817QEo.y',	NULL,	NULL,	'1442552516',	'4Mk8gDeOtq',	'2022-12-23 11:08:01',	'2022-12-23 11:08:01'),
(5,	'master-data',	'Master Data',	'masterdata@gmail.com',	NULL,	NULL,	'15',	NULL,	'2022-12-23 11:08:02',	'$2y$10$DMMjVzmV9D4TJ8/6eWT5deU.qo./EuLh7q0..EOxfCvDogDkZHOg2',	NULL,	NULL,	'1442552516',	'nwtPs1ID4O',	'2022-12-23 11:08:02',	'2022-12-23 11:08:02'),
(1,	'super-admin',	'Super Admin',	'superadmin@gmail.com',	NULL,	NULL,	'1',	NULL,	'2022-12-23 11:07:53',	'$2y$10$qrxSLjENnx4StZkDD/2PyOKbKWALkqtLFs.wzFyTKd07h7jtAt6vW',	'2022-12-26 12:32:20',	NULL,	NULL,	'jvijR3aTBm',	'2022-12-23 11:07:53',	'2022-12-26 12:32:20');

DROP TABLE IF EXISTS "views";
DROP SEQUENCE IF EXISTS views_id_seq;
CREATE SEQUENCE views_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."views" (
    "id" bigint DEFAULT nextval('views_id_seq') NOT NULL,
    "viewable_type" character varying(255) NOT NULL,
    "viewable_id" bigint NOT NULL,
    "visitor" text,
    "collection" character varying(255),
    "viewed_at" timestamp(0) DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "views_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "views_viewable_type_viewable_id_index" ON "public"."views" USING btree ("viewable_type", "viewable_id");

INSERT INTO "views" ("id", "viewable_type", "viewable_id", "visitor", "collection", "viewed_at") VALUES
(1,	'App\Models\News',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 11:49:13'),
(2,	'App\Models\News',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 11:49:51'),
(3,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:24:13'),
(4,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:28:05'),
(5,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:29:20'),
(6,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:29:46'),
(7,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:29:48'),
(8,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:29:51'),
(9,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:30:32'),
(10,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:31:44'),
(11,	'App\Models\Infografis',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:31:56'),
(12,	'App\Models\News',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:32:45'),
(13,	'App\Models\News',	1,	'8F3pG7yh08HFPFZDW8E6uSdWPOUyGbCbvvFNwk2ayWx4Tqt3sRAynNlBnoUIZjZUAjnGIqaaCsmhvqRF',	NULL,	'2022-12-24 12:33:16');

DROP TABLE IF EXISTS "visi_misi";
DROP SEQUENCE IF EXISTS visi_misi_id_seq;
CREATE SEQUENCE visi_misi_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."visi_misi" (
    "id" bigint DEFAULT nextval('visi_misi_id_seq') NOT NULL,
    "visi" text NOT NULL,
    "misi" text NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "visi_misi_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "work_units";
DROP SEQUENCE IF EXISTS work_units_id_seq;
CREATE SEQUENCE work_units_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."work_units" (
    "id" bigint DEFAULT nextval('work_units_id_seq') NOT NULL,
    "code" character varying(255) NOT NULL,
    "name" character varying(255) NOT NULL,
    "description" character varying(255) NOT NULL,
    "status" integer DEFAULT '1' NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "work_units_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "work_units" ("id", "code", "name", "description", "status", "created_at", "updated_at") VALUES
(1,	'001',	'Administrator',	'Administrator ',	1,	'2022-12-23 11:08:31',	'2022-12-23 11:08:31'),
(2,	'002',	'Biro Perencanaan dan Hubungan Masyarakat',	'Biro Perencanaan dan Hubungan Masyarakat ',	1,	'2022-12-23 11:08:31',	'2022-12-23 11:08:31'),
(3,	'003',	'Biro Organisasi, Kepegawaian dan Hukum ',	'Biro Organisasi, Kepegawaian dan Hukum ',	1,	'2022-12-23 11:08:32',	'2022-12-23 11:08:32'),
(4,	'004',	'Biro Umum ',	'Biro Umum  ',	1,	'2022-12-23 11:08:33',	'2022-12-23 11:08:33'),
(5,	'005',	'Direktorat Kearsipan Pusat ',	'Direktorat Kearsipan Pusat ',	1,	'2022-12-23 11:08:33',	'2022-12-23 11:08:33'),
(6,	'006',	'Direktorat Kearsipan Daerah I ',	'Direktorat Kearsipan Daerah I  ',	1,	'2022-12-23 11:08:34',	'2022-12-23 11:08:34'),
(7,	'007',	'Direktorat Kearsipan Daerah II',	'Direktorat Kearsipan Daerah II ',	1,	'2022-12-23 11:08:34',	'2022-12-23 11:08:34'),
(8,	'008',	'Direktorat SDM Kearsipan dan Sertifikasi ',	'Direktorat SDM Kearsipan dan Sertifikasi  ',	1,	'2022-12-23 11:08:35',	'2022-12-23 11:08:35'),
(9,	'009',	'Pusat Pendidikan dan Pelatihan Kearsipan ',	'Pusat Pendidikan dan Pelatihan Kearsipan  ',	1,	'2022-12-23 11:08:36',	'2022-12-23 11:08:36'),
(10,	'010',	'Direktorat Akuisisi',	'Direktorat Akuisisi ',	1,	'2022-12-23 11:08:36',	'2022-12-23 11:08:36'),
(11,	'011',	'Direktorat Pengolahan ',	'Direktorat Pengolahan   ',	1,	'2022-12-23 11:08:37',	'2022-12-23 11:08:37'),
(12,	'012',	'Direktorat Preservasi ',	'Direktorat Preservasi  ',	1,	'2022-12-23 11:08:37',	'2022-12-23 11:08:37'),
(13,	'013',	'Direktorat Layanan dan Pemanfaatan',	'Direktorat Layanan dan Pemanfaatan ',	1,	'2022-12-23 11:08:38',	'2022-12-23 11:08:38'),
(14,	'014',	'Pusat Sistem dan Jaringan Informasi Kearsipan Nasional ',	'Pusat Sistem dan Jaringan Informasi Kearsipan Nasional  ',	1,	'2022-12-23 11:08:39',	'2022-12-23 11:08:39'),
(15,	'015',	'Pusat Data dan Informasi ',	'Pusat Data dan Informasi  ',	1,	'2022-12-23 11:08:39',	'2022-12-23 11:08:39'),
(16,	'016',	'Pusat Pengkajian dan Pengembangan Sistem Kearsipan ',	'Pusat Pengkajian dan Pengembangan Sistem Kearsipan  ',	1,	'2022-12-23 11:08:40',	'2022-12-23 11:08:40'),
(17,	'017',	'Pusat Jasa Kearsipan',	'Pusat Jasa Kearsipan ',	1,	'2022-12-23 11:08:40',	'2022-12-23 11:08:40'),
(18,	'018',	'Inspektorat ',	'Inspektorat  ',	1,	'2022-12-23 11:08:41',	'2022-12-23 11:08:41'),
(19,	'019',	'Pusat Akreditasi Kearsipan ',	'Pusat Akreditasi Kearsipan  ',	1,	'2022-12-23 11:08:42',	'2022-12-23 11:08:42'),
(20,	'020',	'Balai Arsip Statis dan Tsunami',	'Balai Arsip Statis dan Tsunami ',	1,	'2022-12-23 11:08:42',	'2022-12-23 11:08:42');

ALTER TABLE ONLY "public"."capaian_kinerja" ADD CONSTRAINT "capaian_kinerja_sasaranstrategis_id_foreign" FOREIGN KEY (sasaranstrategis_id) REFERENCES sasaran_strategis(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."file_uploads" ADD CONSTRAINT "file_uploads_category_id_foreign" FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."infografis" ADD CONSTRAINT "infografis_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."instansi" ADD CONSTRAINT "instansi_parent_id_foreign" FOREIGN KEY (parent_id) REFERENCES instansi(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."kinerja" ADD CONSTRAINT "kinerja_category_id_foreign" FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."kinerja" ADD CONSTRAINT "kinerja_workunit_id_foreign" FOREIGN KEY (workunit_id) REFERENCES work_units(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."messages" ADD CONSTRAINT "messages_chat_id_foreign" FOREIGN KEY (chat_id) REFERENCES chats(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."messages" ADD CONSTRAINT "messages_receiver_id_foreign" FOREIGN KEY (receiver_id) REFERENCES users(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."messages" ADD CONSTRAINT "messages_sender_id_foreign" FOREIGN KEY (sender_id) REFERENCES users(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."model_has_permissions" ADD CONSTRAINT "model_has_permissions_permission_id_foreign" FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."model_has_roles" ADD CONSTRAINT "model_has_roles_role_id_foreign" FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."news" ADD CONSTRAINT "news_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."notifications" ADD CONSTRAINT "notifications_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."perjanjiankinerja_sasaranstrategis" ADD CONSTRAINT "perjanjiankinerja_sasaranstrategis_perjanjiankinerja_id_foreign" FOREIGN KEY (perjanjiankinerja_id) REFERENCES perjanjian_kinerja(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."perjanjiankinerja_sasaranstrategis" ADD CONSTRAINT "perjanjiankinerja_sasaranstrategis_sasaranstrategis_id_foreign" FOREIGN KEY (sasaranstrategis_id) REFERENCES sasaran_strategis(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."perjanjiankinerja_tujuan" ADD CONSTRAINT "perjanjiankinerja_tujuan_perjanjiankinerja_id_foreign" FOREIGN KEY (perjanjiankinerja_id) REFERENCES perjanjian_kinerja(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."perjanjiankinerja_tujuan" ADD CONSTRAINT "perjanjiankinerja_tujuan_tujuan_id_foreign" FOREIGN KEY (tujuan_id) REFERENCES tujuan(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."realisasi_anggaran" ADD CONSTRAINT "realisasi_anggaran_sasaranstrategis_id_foreign" FOREIGN KEY (sasaranstrategis_id) REFERENCES sasaran_strategis(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."rencanastrategis_sasaranstrategis" ADD CONSTRAINT "rencanastrategis_sasaranstrategis_rencanastrategis_id_foreign" FOREIGN KEY (rencanastrategis_id) REFERENCES rencana_strategis(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."rencanastrategis_sasaranstrategis" ADD CONSTRAINT "rencanastrategis_sasaranstrategis_sasaranstrategis_id_foreign" FOREIGN KEY (sasaranstrategis_id) REFERENCES sasaran_strategis(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."rencanastrategis_tujuan" ADD CONSTRAINT "rencanastrategis_tujuan_rencanastrategis_id_foreign" FOREIGN KEY (rencanastrategis_id) REFERENCES rencana_strategis(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."rencanastrategis_tujuan" ADD CONSTRAINT "rencanastrategis_tujuan_tujuan_id_foreign" FOREIGN KEY (tujuan_id) REFERENCES tujuan(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."role_has_permissions" ADD CONSTRAINT "role_has_permissions_permission_id_foreign" FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."role_has_permissions" ADD CONSTRAINT "role_has_permissions_role_id_foreign" FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."target_kinerja" ADD CONSTRAINT "target_kinerja_indikatorkinerja_id_foreign" FOREIGN KEY (indikatorkinerja_id) REFERENCES indikator_kinerja(id) NOT DEFERRABLE;

-- 2022-12-26 06:00:32.934705+00
