<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TelegramSetting;
use Illuminate\Support\Facades\Crypt;

class TelegramSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TelegramSetting::create([
            'telegram_bot_token' => Crypt::encryptString("5664006393:AAEL0aBSc72l7-EDrkwK2AatrddkAoDnNxM"),
            'username_bot' => 'sidata_anri',
        ]);
    }
}
