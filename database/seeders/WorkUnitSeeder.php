<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\WorkUnit;

class WorkUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workunit = [
            [
                'code' => '001',
                'name' => 'Administrator',
                'description' => 'Administrator ',
                'status' => 1,
            ],
            [
                'code' => '002',
                'name' => 'Biro Perencanaan dan Hubungan Masyarakat',
                'description' => 'Biro Perencanaan dan Hubungan Masyarakat ',
                'status' => 1,
            ],
            [
                'code' => '003',
                'name' => 'Biro Organisasi, Kepegawaian dan Hukum ',
                'description' => 'Biro Organisasi, Kepegawaian dan Hukum ',
                'status' => 1,
            ],
            [
                'code' => '004',
                'name' => 'Biro Umum ',
                'description' => 'Biro Umum  ',
                'status' => 1,
            ],
            [
                'code' => '005',
                'name' => 'Direktorat Kearsipan Pusat ',
                'description' => 'Direktorat Kearsipan Pusat ',
                'status' => 1,
            ],
            [
                'code' => '006',
                'name' => 'Direktorat Kearsipan Daerah I ',
                'description' => 'Direktorat Kearsipan Daerah I  ',
                'status' => 1,
            ],
            [
                'code' => '007',
                'name' => 'Direktorat Kearsipan Daerah II',
                'description' => 'Direktorat Kearsipan Daerah II ',
                'status' => 1,
            ],
            [
                'code' => '008',
                'name' => 'Direktorat SDM Kearsipan dan Sertifikasi ',
                'description' => 'Direktorat SDM Kearsipan dan Sertifikasi  ',
                'status' => 1,
            ],
            [
                'code' => '009',
                'name' => 'Pusat Pendidikan dan Pelatihan Kearsipan ',
                'description' => 'Pusat Pendidikan dan Pelatihan Kearsipan  ',
                'status' => 1,
            ],
            [
                'code' => '010',
                'name' => 'Direktorat Akuisisi',
                'description' => 'Direktorat Akuisisi ',
                'status' => 1,
            ],
            [
                'code' => '011',
                'name' => 'Direktorat Pengolahan ',
                'description' => 'Direktorat Pengolahan   ',
                'status' => 1,
            ],
            [
                'code' => '012',
                'name' => 'Direktorat Preservasi ',
                'description' => 'Direktorat Preservasi  ',
                'status' => 1,
            ],
            [
                'code' => '013',
                'name' => 'Direktorat Layanan dan Pemanfaatan',
                'description' => 'Direktorat Layanan dan Pemanfaatan ',
                'status' => 1,
            ],
            [
                'code' => '014',
                'name' => 'Pusat Sistem dan Jaringan Informasi Kearsipan Nasional ',
                'description' => 'Pusat Sistem dan Jaringan Informasi Kearsipan Nasional  ',
                'status' => 1,
            ],
            [
                'code' => '015',
                'name' => 'Pusat Data dan Informasi ',
                'description' => 'Pusat Data dan Informasi  ',
                'status' => 1,
            ],
            [
                'code' => '016',
                'name' => 'Pusat Pengkajian dan Pengembangan Sistem Kearsipan ',
                'description' => 'Pusat Pengkajian dan Pengembangan Sistem Kearsipan  ',
                'status' => 1,
            ],
            [
                'code' => '017',
                'name' => 'Pusat Jasa Kearsipan',
                'description' => 'Pusat Jasa Kearsipan ',
                'status' => 1,
            ],
            [
                'code' => '018',
                'name' => 'Inspektorat ',
                'description' => 'Inspektorat  ',
                'status' => 1,
            ],
            [
                'code' => '019',
                'name' => 'Pusat Akreditasi Kearsipan ',
                'description' => 'Pusat Akreditasi Kearsipan  ',
                'status' => 1,
            ],
            [
                'code' => '020',
                'name' => 'Balai Arsip Statis dan Tsunami',
                'description' => 'Balai Arsip Statis dan Tsunami ',
                'status' => 1,
            ]
        ];

        foreach ($workunit as $key => $value) {
            WorkUnit::create($value);
        }
    }
}
