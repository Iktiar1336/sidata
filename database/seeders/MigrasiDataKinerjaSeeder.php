<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\MigrasiDataKategoriImport;
use App\Models\Category;
use App\Models\FileUpload;
use App\Models\Kinerja;
use App\Models\Chat;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class MigrasiDataKinerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $file = public_path('/format/master_katdata.csv');
        // Excel::import(new MigrasiDataKategoriImport, $file);
        $datatransdata = DB::table('trans_data')->get();
        $datamasterkatdata = DB::table('master_katdata')->get();
        $datamastersubdata = DB::table('master_subdata')->get();
        $datatransfile = DB::table('trans_files')->get();
        $datapengeloladata = DB::table('pengelola_data')->get();
        $datapeople = DB::table('people')->get();

        foreach ($datatransdata as $value) {

            foreach ($datamasterkatdata as $kat) {
                if ($kat->idkatdata == $value->idkatdata) {
                    $kategori = Category::where('name', $kat->dataclass)->first();
                }
            }

            foreach ($datamastersubdata as $sub) {
                if ($sub->subdataid == $value->subdataid) {
                    $subkategori = Category::where('name', $sub->subclassdata)->first();
                }
            }

            $datafile = DB::table('trans_files')->where('idkatdata', $value->idkatdata)->where('tahun', $value->tahun)->first();
            if($datafile != null) {
                $path = $datafile->path;
                $filependukung = Str::remove('./fileupload/', $path);
                $storefileupload = FileUpload::create([
                    'filename' => $filependukung,
                    'category_id' => $kategori->id,
                    'year' => $value->tahun,
                ]);

                $fileuploadid = $storefileupload->id;
            } else {
                $filependukung = null;
                $fileuploadid = null;
            }
            // foreach ($datatransfile as $file) {
            //     if ($file->idkatdata == $value->idkatdata && $file->tahun == $value->tahun) {
            //         if($file->path != null || $file->path != '' || $file->path != '-') {
            //             $path = $file->path;
            //             $filependukung = Str::remove('./fileupload/', $path);
            //             $storefileupload = FileUpload::create([
            //                 'filename' => $filependukung,
            //                 'category_id' => $kategori->id,
            //                 'year' => $value->tahun,
            //             ]);

            //             $fileuploadid = $storefileupload->id;
            //         } else {
            //             $filependukung = null;
            //             $fileuploadid = null;
            //         }
            //     } else {
            //         $filependukung = null;
            //         $fileuploadid = null;
            //     }
            // }

            if ($value->verifyed == 0) {

                $status = 1;
                $pengelolaid = null;
                $masterdataid = null;

            } elseif($value->verifyed == 1) {
                $status = 2;
                $pengelolaid = null;
                $masterdataid = null;
            } elseif($value->verifyed == 2) {
                $status = 3;
                // $datapengolahdata = DB::table('pengelola_data')->where('idkatdata', $value->idkatdata)->first();
                // $people = DB::table('people')->where('peopleid', $datapengolahdata->peopleid)->first();
                // $user = User::where('name', $people->peoplename)->where('workunit_id', $people->roleid)->where('username', $people->username)->first();
                $pengelolaid = 14;
                $masterdataid = 12;
            } elseif($value->verifyed == 3) {
                $status = 4;
                $datapengolahdata = DB::table('pengelola_data')->where('idkatdata', $value->idkatdata)->first();
                $people = DB::table('people')->where('peopleid', $datapengolahdata->peopleid)->first();
                $user = User::where('name', $people->peoplename)->where('workunit_id', $people->roleid)->where('username', $people->username)->first();
                $pengelolaid = $datapengolahdata->peopleid;
                $masterdataid = 12;
            } elseif($value->verifyed == 4) {
                $status = 5;
                $pengelolaid = null;
                $masterdataid = null;
            }

            //dd($pengelolaid);

            $storechat = Chat::create([
                'topic' => 'Kinerja' . $kategori->name . 'Tahun' . $value->tahun,
            ]);

            $produsendata = User::where('workunit_id', $value->roleid)->first();

            $storekinerja = Kinerja::create([
                'category_id' => $kategori->id,
                'sub_category_id' => $subkategori->id,
                'workunit_id' => $value->roleid,
                'fileupload_id' => $fileuploadid,
                'pengolahdata_id' => $pengelolaid,
                'year' => $value->tahun,
                'total' => $value->jumlah,
                'description' => $value->keterangan,
                'status' => $status,
                'satuan' => $value->satuan,
                'masterdata_id' => $masterdataid,
                'save_at' => $value->dt_save,
                'send_at' => $value->dt_send,
                'chat_id' => $storechat->id,
                'process_at' => $value->dt_proses,
                'verified_at' => $value->dt_verifyed,
                'produsendata_id' => $produsendata->id,
            ]);

            activity('Menambahkan Data Kinerja Baru')
                ->performedOn($storekinerja)
                ->causedBy($produsendata)
                ->log('Menambahkan Data Kinerja');
        }
    }
}
