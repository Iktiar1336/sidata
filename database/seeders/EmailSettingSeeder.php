<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmailSetting;
use Illuminate\Support\Facades\Crypt;

class EmailSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmailSetting::create([
            'driver' => 'smtp',
            'host' => 'smtp.gmail.com',
            'port' => '587',
            'username' => 'sidataanri@gmail.com',
            'password' => Crypt::encryptString("arxcwxaqcoyzugqw"),
            'encryption' => 'tls',
            'from_address' => 'sidataanri@gmail.com',
        ]);
    }
}
