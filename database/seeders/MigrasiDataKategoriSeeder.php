<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class MigrasiDataKategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori = [
            [
                'workunit_id' => 3,
                'name' => 'Data Pegawai per Tahun', 
                'description' => 'Data Pegawai per Tahun',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' => 3,
                'name' => 'Data Pegawai per Pendidikan',
                'description' => 'Data Pegawai per Pendidikan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' => 3,
                'name' => 'Data Pegawai per Jenis Kelamin',
                'description' => 'Data Pegawai per Jenis Kelamin',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' => 3,
                'name' => 'Data Pegawai per Golongan',
                'description' => 'Data Pegawai per Golongan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' => 2,
                'name' => 'Data Bagian Perencanaan',
                'description' => 'Data Bagian Perencanaan yang diserahkan terdiri dari DIPA ANRI, Data Pagu Indikatif, Data Usulan Tambahan, Data Rencana Kerja ANRI (RKL). Data tersebut diserahkan ke Pusat Data dan Informasi dalam bentuk PDF. Untuk mempermudah pengolahan data oleh Pusat Data dan Informasi, data tersebut telah diolah dan dikonversikan ke bentuk Ms.Excel. Adapun data tambahan yang diserahkan pada tanggal 1 Juli 2019 yaitu Data IKU ANRI, Renstra, Perjanjian Kinerja dan Pagu Alokasi Anggaran.',
                'status' => 1,
                'access' => 'Publik'
            ],
            [   
                'workunit_id' => 2,
                'name' => 'Data Kerjasama dan Evaluasi', 
                'description' => 'Data terdiri dari subbagian evaluasi dan subbagian kerjasama',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  2,
                'name' => 'Data Bagian Humas', 
                'description' => 'Data terdiri data subbagian publikasi dan dokumentasi, subbagian HAL dan protokol dan subbagian TU Pimpinan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  12,
                'name' => 'Subdirektorat Penyimpanan Arsip',
                'description' => 'Data subdirektorat penyimpanan arsip dari tahun 2015 s.d. 2018 yang terdiri dari Penyimpanan Arsip, Peminjaman Arsip dan Pengembalian Arsip',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  12,
                'name' => 'Subdirektorat Restorasi Arsip',
                'description' => 'Data subdirektorat restorasi arsip tahun 2015 s.d. 2018 yang terdiri dari Data Arsip Rusak dan Data Arsip yang telah direstorasi',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  12,
                'name' => 'Subdirektorat Laboratorium dan Autentikasi Arsip',
                'description' => 'Data subdirektorat laboratorium dan autentikasi arsip tahun 2015 s.d. 2018 yang terdiri dari Pengujian Arsip',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  12,
                'name' => 'Subdirektorat Digitalisasi dan Reproduksi  Arsip',
                'description' => 'Data subdirektorat digitalisasi dan reproduksi arsip',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  8,
                'name' => 'Subdirektorat SDM Kearsipan',
                'description' => 'Data kinerja unit kerja subdirektorat SDM Kearsipan yang terdiri dari data sdm kearsipan berdasarkan jenjang jabatan, sdm kearsipan yang telah mendapatkan pembinaan, pengembangan dan pemberdayaan, data sdm kearsipan yang dibedakan menjadi beberapa kategori yaitu, Pemerintah Daerah, Kementerian Lembaga Negara dan Perguruan Tinggi Negeri.',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  8,
                'name' => 'Subdirektorat Sertifikasi',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  10,
                'name' => 'Subdirektorat Akuisisi Arsip I',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  17,
                'name' => 'Bidang Jasa Sistem dan Penataan Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                
                'workunit_id' =>  13, 
                'name' => 'Subdirektorat Layanan Arsip', 
                'description' => 'Data kinerja unit kerja subdirektorat layanan arsip yang terdiri dari data pengunjung, data peminjaman, data arsip yang dikembalikan, data penggandaan arsip konvensional, data penggandaan arsip media baru, data kepuasan pengguna, dan data konsultasi.',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                
                'workunit_id' =>  13, 
                'name' => 'Subdirektorat Pemanfaatan Arsip', 
                'description' => 'Data kinerja unit kerja subdirektorat pemanfaatan arsip yang terdiri dari data arsip tematik, data pameran tetap, data terbitan citra daerah, citra nusantara dan penerbitan lainnya.',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  10,
                'name' => 'Subdirektorat Akuisisi Arsip II', 
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  10,
                'name' => 'Subdirektorat Akuisisi Arsip III',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  15,
                'name' => 'Bidang Pengelolaan Perangkat TIK dan Sistem Informasi',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  15,
                'name' => 'Bidang Pengelolaan Data dan Informasi',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' => 17,
                'name' => 'Bidang Jasa Penyimpanan dan Perawatan Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  14,
                'name' => 'Bidang Pengembangan Sistem dan Jaringan Informasi Kearsipan Nasional',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  14,
                'name' => 'Bidang Pengembangan Simpul Jaringan',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  16,
                'name' => 'Bidang Sistem Kearsipan Dinamis',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  16,
                'name' => 'Bidang Sistem Kearsipan Statis',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  20,
                'name' => 'Akuisisi Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  20,
                'name' => 'Pengolahan Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  19,
                'name' => 'Data lembaga Pusat yang telah diawasi',
                'description' => 'Data lembaga Pusat yang telah diawasi Berdasarkan fungsi Pengawasan dari Bidang Akreditasi Pusat',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  3,
                'name' => 'Data Mutasi Jabatan Pegawai Internal',
                'description' => 'Data Mutasi Jabatan Pegawai Internal',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  3,
                'name' => 'Penerimaan CPNS',
                'description' => 'Penerimaan Calon Pegawai Negeri Sipil',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  3,
                'name' => 'Data Penerbitan Produk Hukum',
                'description' => 'Data Penerbitan Produk Hukum',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  3,
                'name' => 'Penyusunan SOP AP unit kerja',
                'description' => 'Penyusunan dan atau penyempurnaan SOP AP unit kerja',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  19,
                'name' => 'Data lembaga Pusat yang telah diakreditasi',
                'description' => 'Berisikan Data lembaga Pusat yang telah diakreditasi oleh Pusat Akreditasi Kearsipan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  11,
                'name' => 'Daftar Arsip Statis', 
                'description' => '-',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  11,
                'name' => 'Inventaris Arsip Statis',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  11,
                'name' => 'Guide Arsip Statis',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  11,
                'name' => 'Transkripsi Arsip Statis',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  11,
                'name' => 'Peningkatan Mutu',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  3, 
                'name' => 'Reformasi Birokrasi ANRI',
                'description' => 'Reformasi Birokrasi ANRI',
                'status' => 1,
                'access' => 'Publik'  
            ],
            [
                'workunit_id' =>  19,
                'name' => 'Data Lembaga Kearsipan Daerah yang telah diawasi',
                'description' => 'Berisikan Data lembaga kearsipan daerah yang telah diawasi oleh Pusat Akreditasi Kearsipan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  19,
                'name' => 'Data Lembaga Kearsipan Daerah yang telah diakreditasi',
                'description' => 'Berisikan Data lembaga kearsipan daerah yang telah diakreditasi oleh Pusat Akreditasi Kearsipan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  9,
                'name' => 'Bidang Perencanaan dan Evaluasi',
                'description' => 'Bertugas melaksanakan penyusunan rencana dan program, serta pemantauan, evaluasi, dan pelaporan pelaksanaan pendidikan dan pelatihan kearsipan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  9,
                'name' => 'Bidang Pelaksanaan dan Kerjasama',
                'description' => 'Bertugas melaksanakan penyelenggaraan pendidikan dan pelatihan kearsipan serta kerja sama',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  4,
                'name' => 'Bagian Keuangan',
                'description' => 'Bertugas melaksanakan penyiapan pembinaan dan pengelolaan urusan keuangan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  4,
                'name' => 'Bagian Perlengkapan dan Rumah Tangga',
                'description' => 'Bertugas melaksanakan pengelolaan urusan perlengkapan, barang milik negara, dan rumah tangga, serta pengamanan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  4,
                'name' => 'Bagian Arsip',
                'description' => 'Bertugas melaksanakan pengelolaan urusan kearsipan',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  5,
                'name' => 'Subdirektorat Pusat I',
                'description' => 'Data subdirektorat Pusat I dengan lokus lembaga negara. Datanya antara lain :\r\n1. Data Lembaga yang sudah mendapatkan bimbingan\r\n2. Data lembaga yang sudah mendapatkan supervisi\r\n3. Data lembaga yang sudah memiliki Perman/Perka lembaga tentang kearsipan\r\n4. Data lembaga yang telah memiliki Tata Naskah Dinas\r\n5. Data Lembaga yang telah memiliki Klasifikasi Arsip\r\n6. Data Lembaga yang telah memiliki JRA Fasilitatif dan JRA Substantif\r\n7. Data lembaga yang telah memiliki Klasifikasi Keamanan dan Akses Arsip\r\n8. Data Lembaga yang memiliki central file\r\n9. Data lembaga yang memiliki record center\r\n10. Data lembaga yang mendapatkan kemampuan teknis pengelolaan arsip aset sesyai dengan peraturan perundang - undangan\r\n11. Data lembaga yang sudah mencanangkan GNSTA\r\n12. Data lembaga yang sudah menerima Aplikasi SIKD\r\n13. Data lembaga yang sudah menerima dan mengimplementasikan Aplikasi SIKD',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  5,
                'name' => 'Subdirektorat Pusat II',
                'description' => 'Data subdirektorat Pusat II dengan lokus BUMN dan Perusahaan Swasta',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  5,
                'name' => 'Subdirektorat Pusat III',
                'description' => 'Data subdirektorat Pusat I dengan lokus Perguruan Tinggi Negeri (PTN), Organisasi Politik, Organisasi Kemasyarakatan dan Perseorangan. \r\nDatanya antara lain : 1. Data Lembaga yang sudah mendapatkan bimbingan 2. Data lembaga yang sudah mendapatkan supervisi 3. Data lembaga yang sudah memiliki Perman/Perka lembaga tentang kearsipan 4. Data lembaga yang telah memiliki Tata Naskah Dinas 5. Data Lembaga yang telah memiliki Klasifikasi Arsip 6. Data Lembaga yang telah memiliki JRA Fasilitatif dan JRA Substantif 7. Data lembaga yang telah memiliki Klasifikasi Keamanan dan Akses Arsip 8. Data Lembaga yang memiliki central file 9. Data lembaga yang memiliki record center 10. Data lembaga yang mendapatkan kemampuan teknis pengelolaan arsip aset sesyai dengan peraturan perundang - undangan 11. Data lembaga yang sudah mencanangkan GNSTA 12. Data lembaga yang sudah menerima Aplikasi SIKD 13. Data lembaga yang sudah menerima dan mengimplementasikan Aplikasi SIKD',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  6,
                'name' => 'Subdirektorat Daerah I A',
                'description'  => 'Data subdirektorat daerah I A pada lembaga kearsipan pemerintah daerah provinsi, kabupaten/kota, dan BUMD di wilayah I A antara lain Provinsi Bali, Kalimantan Barat, Kalimantan Tengah, Kalimantan Selatan, Kalimantan Timur dan Kalimantan Utara.\r\nDatanya antara lain :\r\n1. Data lembaga kearsipan daerah (LKD) yang berdiri sendiri\r\n2. Data lembaga kearsipan daerah (LKD) yang bergabung dengan perpustakaan\r\n3. Data lembaga yang sudah mendapatkan bimbingan dan konsultasi kearsipan\r\n4. Data lembaga yang berkonsultasi kearsipan ke ANRI\r\n5. Data lembaga yang sudah mendapatkan supervisi\r\n6. Data lembaga yang memiliki Perda/Perkada tentang kearsipan\r\n7. Data lembaga yang telah memiliki Tata Naskah Dinas\r\n8. Data lembaga yang telah memiliki klasifikasi arsip\r\n9. Data lembaga yang telah memiliki JRA Fasilitatif\r\n10. Data lembaga yang telah memiliki JRA Substantif\r\n11. Data lembaga yang telah memiliki klasifikasi keamanan dan akses arsip\r\n12. Data lembaga yang memiliki central file\r\n13. Data lembaga yang memiliki record center\r\n14. Data lembaga yang sudah mencanangkan GNSTA\r\n15. Data lembaga yang sudah menerima aplikasi SIKD\r\n16. Data lembaga yang sudah mengimplementasikan aplikasi SIKD\r\n17. Data yang memiliki Depo Arsip\r\n18. Data pemerintah daerah yang mendapatkan kemampuan teknis pengelolaan arsip asset sesuai dengan peraturan perundang - undangan\r\n19. Data lembaga yang sudah menerima aplikasi SIKS\r\n20. Data lembaga yang sudah mengimplementasikan SIKS',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  6,
                'name' => 'Subdirektorat Daerah I B',
                'description' => 'Data subdirektorat daerah I B pada lembaga kearsipan pemerintah daerah provinsi, kabupaten/kota, dan BUMD di wilayah I B antara lain Provinsi Nusa Tenggara Barat, Sulawesi Utara, Sulawesi Tengah, Sulawesi Selatan, Sulawesi Tenggara, Gorontalo dan Sulawesi Barat.\r\nDatanya antara lain :\r\n1. Data lembaga kearsipan daerah (LKD) yang berdiri sendiri\r\n2. Data lembaga kearsipan daerah (LKD) yang bergabung dengan perpustakaan\r\n3. Data lembaga yang sudah mendapatkan bimbingan dan konsultasi kearsipan\r\n4. Data lembaga yang berkonsultasi kearsipan ke ANRI\r\n5. Data lembaga yang sudah mendapatkan supervisi\r\n6. Data lembaga yang memiliki Perda/Perkada tentang kearsipan\r\n7. Data lembaga yang telah memiliki Tata Naskah Dinas\r\n8. Data lembaga yang telah memiliki klasifikasi arsip\r\n9. Data lembaga yang telah memiliki JRA Fasilitatif\r\n10. Data lembaga yang telah memiliki JRA Substantif\r\n11. Data lembaga yang telah memiliki klasifikasi keamanan dan akses arsip\r\n12. Data lembaga yang memiliki central file\r\n13. Data lembaga yang memiliki record center\r\n14. Data lembaga yang sudah mencanangkan GNSTA\r\n15. Data lembaga yang sudah menerima aplikasi SIKD\r\n16. Data lembaga yang sudah mengimplementasikan aplikasi SIKD\r\n17. Data yang memiliki Depo Arsip\r\n18. Data pemerintah daerah yang mendapatkan kemampuan teknis pengelolaan arsip asset sesuai dengan peraturan perundang - undangan\r\n19. Data lembaga yang sudah menerima aplikasi SIKS\r\n20. Data lembaga yang sudah mengimplementasikan SIKS',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  6,
                'name' => 'Subdirektorat Daerah I C',
                'description' => 'Data subdirektorat daerah I C pada lembaga kearsipan pemerintah daerah provinsi, kabupaten/kota, dan BUMD di wilayah I C antara lain Provinsi Nusa Tenggara Timur, Maluku, Maluku Utara, Papua dan Papua Barat\r\nDatanya antara lain :\r\n1. Data lembaga kearsipan daerah (LKD) yang berdiri sendiri\r\n2. Data lembaga kearsipan daerah (LKD) yang bergabung dengan perpustakaan\r\n3. Data lembaga yang sudah mendapatkan bimbingan dan konsultasi kearsipan\r\n4. Data lembaga yang berkonsultasi kearsipan ke ANRI\r\n5. Data lembaga yang sudah mendapatkan supervisi\r\n6. Data lembaga yang memiliki Perda/Perkada tentang kearsipan\r\n7. Data lembaga yang telah memiliki Tata Naskah Dinas\r\n8. Data lembaga yang telah memiliki klasifikasi arsip\r\n9. Data lembaga yang telah memiliki JRA Fasilitatif\r\n10. Data lembaga yang telah memiliki JRA Substantif\r\n11. Data lembaga yang telah memiliki klasifikasi keamanan dan akses arsip\r\n12. Data lembaga yang memiliki central file\r\n13. Data lembaga yang memiliki record center\r\n14. Data lembaga yang sudah mencanangkan GNSTA\r\n15. Data lembaga yang sudah menerima aplikasi SIKD\r\n16. Data lembaga yang sudah mengimplementasikan aplikasi SIKD\r\n17. Data yang memiliki Depo Arsip\r\n18. Data pemerintah daerah yang mendapatkan kemampuan teknis pengelolaan arsip asset sesuai dengan peraturan perundang - undangan\r\n19. Data lembaga yang sudah menerima aplikasi SIKS\r\n20. Data lembaga yang sudah mengimplementasikan SIKS',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  20,
                'name' => 'Penyimpanan Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  20,
                'name' => 'Restorasi Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  20,
                'name' => 'Alih media arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  20,
                'name' => 'Layanan Kearsipan',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  18,
                'name' => 'Audit',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  19,
                'name' => 'Nilai Hasil Pengawasan 34 Kementerian Periode Tahun 2016',
                'description' => 'Data yang berisi tentang nilai hasil pengawasan dari 34 Kementerian pada Periode Tahun 2016',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  18,
                'name' => 'Review',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  18,
                'name' => 'Evaluasi',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  18,
                'name' => 'Pemantauan/Monitoring',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  19,
                'name' => 'Nilai Hasil Pengawasan pada 4 BUMN di Periode Tahun 2016',
                'description' => 'Merupakan nilai hasil pengawasan pada 4 BUMN di Periode Tahun 2016',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  18,
                'name' => 'Pengawasan Lain',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  19,
                'name' => 'Data Nilai Hasil Pengawasan pada 34 Pemerintahan Daerah Provinsi Periode Tahun 2016',
                'description' => 'Data Nilai Hasil Pengawasan pada 34 Pemerintahan Daerah Provinsi Periode Tahun 2016',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  7,
                'name' => 'Program Implementasi SIKD ANRI',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  7,
                'name' => 'Instansi Yang memiliki JRA',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  7,
                'name' => 'Intsansi yang memiliki Organisasi Kearsipan',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  7,
                'name' => 'Instansi yang sudah memiliki Instrumen Kearsipan',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  7,
                'name' => 'Status Kelembagaan',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  7,
                'name' => 'Program Bimkos Kearsipan dan Supervisi ANRI',
                'description' => 'Lembaga yang mendapatkan program pembinaan kearsipan ANRI',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  12,
                'name' => 'Reproduksi dan Digitalisasi Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  12,
                'name' => 'Pengujian Laboratorium',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  12,
                'name' => 'Penyimpanan Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  12,
                'name' => 'Restorasi Arsip',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  9,
                'name' => 'Pendidikan dan Pelatihan Kearsipan PNBP',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  9,
                'name' => 'Pendidikan dan Pelatihan Kearsipan Non PNBP',
                'description' => '',
                'status' => 1,
                'access' => 'Publik'
            ],
            [
                'workunit_id' =>  9, 
                'name' => 'Training on Records and Archives Management',
                'description' => 'Pendidikan dan Pelatihan Kearsipan khusus peserta diklat Palestine dan Timor Leste',
                'status' => 1,
                'access' => 'Publik']
        ];

        foreach ($kategori as $key => $value) {
            Category::create($value);
        }
    }
}
