<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kinerja;
use App\Models\Category;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class DataKinerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::create([
            'name' => 'Data Pegawai ANRI',
            'workunit_id' => 3,
            'description' => 'Data Pegawai ANRI',
            'access' => 'Public',
            'status' => 1,
        ]);

        $subcategories = [
            [
                'name' => 'Berdasarkan Pendidikan',
                'workunit_id' => 3,
                'description' => 'Berdasarkan Pendidikan',
                'status' => 1,
                'parent_id' => $categories->id,
            ],
            [
                'name' => 'Berdasarkan Jenis Kelamin',
                'workunit_id' => 3,
                'description' => 'Berdasarkan Jenis Kelamin',
                'status' => 1,
                'parent_id' => $categories->id,
            ],
            [
                'name' => 'Berdasarkan Golongan',
                'workunit_id' => 3,
                'description' => 'Berdasarkan Golongan',
                'status' => 1,
                'parent_id' => $categories->id,
            ],
            [
                'name' => 'Berdasarkan Tahun',
                'workunit_id' => 3,
                'description' => 'Berdasarkan Tahun',
                'status' => 1,
                'parent_id' => $categories->id,
            ],
        ];

        foreach ($subcategories as $subcategory) {
            Category::create($subcategory);
        }

        $kinerjas = [
            [
                'workunit_id' => 3,
                'category_id' => 1,
                'sub_category_id' => 2,
                'year' => 2016,
                'total' => 543,
                'status' => 4,
                'satuan' => 'orang',
                'produsendata_id' => 4,
                'masterdata_id' => 5,
                'pengolahdata_id' => 3,
                'save_at' => Carbon::now(),
                'send_at' => Carbon::now(),
                'process_at' => Carbon::now(),
                'verified_at' => Carbon::now(),
            ],
            [
                'workunit_id' => 3,
                'category_id' => 1,
                'sub_category_id' => 2,
                'year' => 2017,
                'total' => 531,
                'status' => 4,
                'satuan' => 'orang',
                'produsendata_id' => 4,
                'masterdata_id' => 5,
                'pengolahdata_id' => 3,
                'save_at' => Carbon::now(),
                'send_at' => Carbon::now(),
                'process_at' => Carbon::now(),
                'verified_at' => Carbon::now(),
            ],
            [
                'workunit_id' => 3,
                'category_id' => 1,
                'sub_category_id' => 2,
                'year' => 2018,
                'total' => 574,
                'status' => 4,
                'satuan' => 'orang',
                'produsendata_id' => 4,
                'masterdata_id' => 5,
                'pengolahdata_id' => 3,
                'save_at' => Carbon::now(),
                'send_at' => Carbon::now(),
                'process_at' => Carbon::now(),
                'verified_at' => Carbon::now(),
            ],
            [
                'workunit_id' => 3,
                'category_id' => 1,
                'sub_category_id' => 2,
                'year' => 2019,
                'total' => 645,
                'status' => 4,
                'satuan' => 'orang',
                'produsendata_id' => 4,
                'masterdata_id' => 5,
                'pengolahdata_id' => 3,
                'save_at' => Carbon::now(),
                'send_at' => Carbon::now(),
                'process_at' => Carbon::now(),
                'verified_at' => Carbon::now(),
            ],
            [
                'workunit_id' => 3,
                'category_id' => 1,
                'sub_category_id' => 2,
                'year' => 2020,
                'total' => 635,
                'status' => 4,
                'satuan' => 'orang',
                'produsendata_id' => 4,
                'masterdata_id' => 5,
                'pengolahdata_id' => 3,
                'save_at' => Carbon::now(),
                'send_at' => Carbon::now(),
                'process_at' => Carbon::now(),
                'verified_at' => Carbon::now(),
            ],
            [
                'workunit_id' => 3,
                'category_id' => 1,
                'sub_category_id' => 2,
                'year' => 2021,
                'total' => 667,
                'status' => 4,
                'satuan' => 'orang',
                'produsendata_id' => 4,
                'masterdata_id' => 5,
                'pengolahdata_id' => 3,
                'save_at' => Carbon::now(),
                'send_at' => Carbon::now(),
                'process_at' => Carbon::now(),
                'verified_at' => Carbon::now(),
            ],
        ];
        

        foreach ($kinerjas as $kinerja) {
            Kinerja::create($kinerja);
        }
    }
}
