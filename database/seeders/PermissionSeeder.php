<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $modulDashboard = [
            [
                'name' => 'Dashboard Admin',
                'module' => 'Dashboard',
            ],
            [
                'name' => 'Dashboard Eksekutif',
                'module' => 'Dashboard',
            ],
            [
                'name' => 'Dashboard',
                'module' => 'Dashboard',
            ],
            [
                'name' => 'Statistik Produsen Data',
                'module' => 'Dashboard',
            ],
            [
                'name' => 'Statistik Master Data',
                'module' => 'Dashboard',
            ],
            [
                'name' => 'Statistik Pengolah Data',
                'module' => 'Dashboard',
            ]
        ];

        foreach ($modulDashboard as $key => $value) {
            Permission::create($value);
        }

        $modulArsipKolektor = [
            [
                'name' => 'Menu Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'List Survey Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'List Responden Survey Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'Isi Survey Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'Edit Jawaban Survey Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'Detail Jawaban Survey Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'Import Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'List Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ], 
            [
                'name' => 'Edit Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'Detail Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ],
            [
                'name' => 'Delete Arsip Kolektor',
                'module' => 'Arsip Kolektor',
            ]
        ];

        foreach ($modulArsipKolektor as $key => $value) {
            Permission::create($value);
        }

        $modulMetadataManagement = [
            [
                'name' => 'Menu Metadata Management',
                'module' => 'Metadata Management',
            ],
            [
                'name' => 'List Metadata Management',
                'module' => 'Metadata Management',
            ],
            [
                'name' => 'Tambah Metadata Management',
                'module' => 'Metadata Management',
            ],
            [
                'name' => 'Edit Metadata Management',
                'module' => 'Metadata Management',
            ],
            [
                'name' => 'Hapus Metadata Management',
                'module' => 'Metadata Management',
            ]
        ];

        foreach ($modulMetadataManagement as $key => $value) {
            Permission::create($value);
        }

        $modulIdentifikasiArsip = [
            [
                'name' => 'Menu Identifikasi Arsip',
                'module' => 'Identifikasi Arsip',
            ],
            [
                'name' => 'List Identifikasi Arsip',
                'module' => 'Identifikasi Arsip',
            ],
        ];

        foreach ($modulIdentifikasiArsip as $key => $value) {
            Permission::create($value);
        }

        $modulMasterUnitKerja = [
            [
                'name' => 'Menu Master Data',
                'module' => 'Master Data',
            ],
            [
                'name' => 'List Unit Kerja',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Tambah Unit Kerja',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Edit Unit Kerja',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Hapus Unit Kerja',
                'module' => 'Master Data',
            ],
            [
                'name' => 'List Survey',
                'module' => 'Master Data',
            ], 
            [
                'name' => 'Tambah Survey',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Edit Survey',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Delete Survey',
                'module' => 'Master Data',
            ],
            [
                'name' => 'List Jenis Arsip',
                'module' => 'Master Data',
            ], 
            [
                'name' => 'Tambah Jenis Arsip',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Edit Jenis Arsip',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Delete Jenis Arsip',
                'module' => 'Master Data',
            ]
        ];

        foreach ($modulMasterUnitKerja as $key => $value) {
            Permission::create($value);
        }

        $moduldatakinerja = [
            [
                'name' => 'Menu Modul Data Kinerja',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Grafik Data Kinerja',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'List Kategori',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Tambah Kategori',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Edit Kategori',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Hapus Kategori',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'List Sub Kategori',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Tambah Sub Kategori',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Edit Sub Kategori',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Hapus Sub Kategori',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'List Data Kinerja',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Tambah Data Kinerja',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Detail Data Kinerja',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Edit Data Kinerja',
                'module' => 'Modul Data Kinerja',
            ],
            [
                'name' => 'Hapus Data Kinerja',
                'module' => 'Modul Data Kinerja',
            ],

        ];

        foreach ($moduldatakinerja as $key => $value) {
            Permission::create($value);
        }

        $modulKotakDataMasuk = [
            [
                'name' => 'Kotak Data Masuk Master Data',
                'module' => 'Kotak Masuk',
            ],
            [
                'name' => 'Kotak Data Masuk Pengolah Data',
                'module' => 'Kotak Masuk',
            ],
        ];

        foreach ($modulKotakDataMasuk as $key => $value) {
            Permission::create($value);
        }

        $modulMasterDataKinerja = [
            [
                'name' => 'List Visi & Misi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Visi & Misi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Visi & Misi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Visi & Misi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Tugas & Fungsi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Tugas & Fungsi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Tugas & Fungsi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Tugas & Fungsi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Struktur Organisasi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Struktur Organisasi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Struktur Organisasi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Struktur Organisasi',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Indikator Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Indikator Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Indikator Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Indikator Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Target Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Target Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Target Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Target Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Peran Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Peran Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Peran Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Peran Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Sasaran Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Sasaran Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Sasaran Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Sasaran Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Rencana Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Rencana Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Rencana Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Detail Rencana Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Rencana Strategis',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Tujuan',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Tujuan',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Tujuan',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Tujuan',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Perjanjian Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Perjanjian Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Perjanjian Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Detail Perjanjian Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Perjanjian Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Capaian & Realisasi Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Capaian & Realisasi Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Capaian & Realisasi Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Capaian & Realisasi Kinerja',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Capaian SPBE',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Capaian SPBE',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Capaian SPBE',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Capaian SPBE',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'List Capaian Lainnya',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Tambah Capaian Lainnya',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Edit Capaian Lainnya',
                'module' => 'Master Data Kinerja',
            ],
            [
                'name' => 'Hapus Capaian Lainnya',
                'module' => 'Master Data Kinerja',
            ],
            
        ];

        foreach ($modulMasterDataKinerja as $key => $value) {
            Permission::create($value);
        }

        $modulModelkinerja = [
            [
                'name' => 'List Model Kinerja',
                'module' => 'Model Kinerja',
            ],
            [
                'name' => 'Tambah Model Kinerja',
                'module' => 'Model Kinerja',
            ],
            [
                'name' => 'Edit Model Kinerja',
                'module' => 'Model Kinerja',
            ],
            [
                'name' => 'Export Model Kinerja',
                'module' => 'Model Kinerja',
            ],
            [
                'name' => 'Detail Model Kinerja',
                'module' => 'Model Kinerja',
            ],
            [
                'name' => 'Hapus Model Kinerja',
                'module' => 'Model Kinerja',
            ],
            
        ];

        foreach ($modulModelkinerja as $key => $value) {
            Permission::create($value);
        }

        $modulInformasiPublik = [
            [
                'name' => 'Menu Informasi Publik',
                'module' => 'Informasi Publik',
            ],
            [
                'name' => 'List Berita',
                'module' => 'Informasi Publik',
            ],
            [
                'name' => 'Tambah Berita',
                'module' => 'Informasi Publik',
            ],
            [
                'name' => 'Edit Berita',
                'module' => 'Informasi Publik',
            ],
            [
                'name' => 'Hapus Berita',
                'module' => 'Informasi Publik',
            ],
            [
                'name' => 'List Infografis',
                'module' => 'Informasi Publik',
            ],
            [
                'name' => 'Tambah Infografis',
                'module' => 'Informasi Publik',
            ],
            [
                'name' => 'Edit Infografis',
                'module' => 'Informasi Publik',
            ],
            [
                'name' => 'Hapus Infografis',
                'module' => 'Informasi Publik',
            ],

        ];

        foreach ($modulInformasiPublik as $key => $value) {
            Permission::create($value);
        }

        $modulUserManagement = [
            [
                'name' => 'Menu User Management',
                'module' => 'User Management',
            ],
            [
                'name' => 'List User',
                'module' => 'User Management',
            ],
            [
                'name' => 'Tambah User',
                'module' => 'User Management',
            ],
            [
                'name' => 'Edit User',
                'module' => 'User Management',
            ],
            [
                'name' => 'Hapus User',
                'module' => 'User Management',
            ],
            [
                'name' => 'Detail User',
                'module' => 'User Management',
            ],
            [
                'name' => 'List Role',
                'module' => 'User Management',
            ],
            [
                'name' => 'Tambah Role',
                'module' => 'User Management',
            ],
            [
                'name' => 'Edit Role',
                'module' => 'User Management',
            ],
            [
                'name' => 'Hapus Role',
                'module' => 'User Management',
            ],
            [
                'name' => 'List Hak Akses',
                'module' => 'User Management',
            ],
            [
                'name' => 'List Log Aktivitas',
                'module' => 'User Management',
            ],
            [
                'name' => 'User Profile',
                'module' => 'User Management',
            ],
        ];

        foreach ($modulUserManagement as $key => $value) {
            Permission::create($value);
        }

        $modulNotifikasi = [
            [
                'name' => 'Notifikasi Produsen Data',
                'module' => 'Notifikasi',
            ],
            [
                'name' => 'Notifikasi Master Data',
                'module' => 'Notifikasi',
            ],
            [
                'name' => 'Notifikasi Pengolah Data',
                'module' => 'Notifikasi',
            ],
        ];

        foreach ($modulNotifikasi as $key => $value) {
            Permission::create($value);
        }

        $modulinstansi = [
            [
                'name' => 'List Instansi',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Tambah Instansi',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Edit Instansi',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Hapus Instansi',
                'module' => 'Master Data',
            ],
            [
                'name' => 'List Sub Instansi',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Tambah Sub Instansi',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Edit Sub Instansi',
                'module' => 'Master Data',
            ],
            [
                'name' => 'Hapus Sub Instansi',
                'module' => 'Master Data',
            ],
        ];

        foreach ($modulinstansi as $key => $value) {
            Permission::create($value);
        }


        $superAdmin = Role::create(['name' => 'super-admin', 'description' => 'Super Admin']);

        $usersuperadmin = User::factory()->create([
            'username' => 'super-admin',
            'workunit_id' => 1,
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('secret123'),
        ]);

        $usersuperadmin->assignRole($superAdmin);

        $instansi = Role::create(['name' => 'Instansi', 'description' => 'Instansi']);
        $eksekutif = Role::create(['name' => 'Eksekutif', 'description' => 'Eksekutif']);

        $admin = Role::create(['name' => 'admin','description' => 'Administrator']);

        $useradmin = User::factory()->create([
            'username' => 'admin',
            'workunit_id' => 1,
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret123'),
        ]);

        $userpengolahdata = User::factory()->create([
            'username' => 'pengolah-data',
            'workunit_id' => 15,
            'name' => 'Pengolah Data',
            'email' => 'pengolahdata@gmail.com',
            'telegram_id' => '1442552516',
            'password' => bcrypt('secret123'),
        ]);

        $userprodusendata = User::factory()->create([
            'username' => 'produsen-data',
            'workunit_id' => 3,
            'name' => 'Produsen Data',
            'email' => 'produsendata@gmail.com',
            'telegram_id' => '1442552516',
            'password' => bcrypt('secret123'),
        ]);

        $usermasterdata = User::factory()->create([
            'username' => 'master-data',
            'workunit_id' => 15,
            'name' => 'Master Data',
            'email' => 'masterdata@gmail.com',
            'telegram_id' => '1442552516',
            'password' => bcrypt('secret123'),
        ]);

        $userprofile = Permission::where('name', 'User Profile')->get()->first();
        $menuUserManagement = Permission::where('name', 'Menu User Management')->get()->first();
        $viewDashboardAdmin = Permission::where('name', 'Dashboard Admin')->get()->first();
        $viewStatistikMasterData = Permission::where('name', 'Statistik Master Data')->get()->first();
        $viewStatistikProdusenData = Permission::where('name', 'Statistik Produsen Data')->get()->first();
        $viewStatistikPengolahData = Permission::where('name', 'Statistik Pengolah Data')->get()->first();
        $viewDashboard = Permission::where('name', ' Dashboard')->get()->first();
        $menuDataUnitKerja = Permission::where('name', 'Menu Data Unit Kerja')->get()->first();
        $useradmin->assignRole($admin);
        $admin->givePermissionTo($userprofile, $menuUserManagement, $viewDashboardAdmin);
        $produsendata = Role::create(['name' => 'produsen-data','description' => 'Produsen Data']);
        $pengolahdata = Role::create(['name' => 'pengolah-data','description' => 'Pengolah Data']);
        $masterdata = Role::create(['name' => 'master-data','description' => 'Master Data']);
        $userpengolahdata->assignRole($pengolahdata);
        $pengolahdata->givePermissionTo($userprofile, $viewDashboard, $viewStatistikPengolahData, $menuUserManagement);
        $userprodusendata->assignRole($produsendata);
        $produsendata->givePermissionTo($userprofile, $menuUserManagement, $viewDashboard, $viewStatistikProdusenData);
        $usermasterdata->assignRole($masterdata);
        $masterdata->givePermissionTo($userprofile, $menuUserManagement, $viewDashboard, $viewStatistikMasterData);

    }
}
