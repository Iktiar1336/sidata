<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AppSetting;

class AppSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AppSetting::create([
            'nama_aplikasi' => 'SIDATA',
            'deskripsi_aplikasi' => 'Sistem Informasi Pengumpulan Data',
            'nama_instansi' => 'Arsip Nasional Republik Indonesia',
            'alamat' => 'Jl. Ampera Raya No. 7 Jakarta 12560',
            'no_telp' => '+62 21 7805851',
            'email' => 'info@anri.go.id',
            'no_fax' => '+62 21 7810280 - 7805812',
            'instagram' => 'https://www.instagram.com/arsipnasionalri/',
            'facebook' => 'https://web.facebook.com/ArsipNasionalRI',
            'twitter' => 'https://twitter.com/arsipnasionalri',
            'youtube' => 'https://www.youtube.com/channel/UCA-t7dKXcI8s-KciGOC9byA',
        ]);
    }
}
