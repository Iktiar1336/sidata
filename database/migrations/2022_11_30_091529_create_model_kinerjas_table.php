<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelKinerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_kinerja', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->string('tahun');
            $table->text('pengantar');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('visimisi_id');
            $table->text('core_values_asn');
            $table->text('nilai_nilai_anri');
            $table->text('latar_belakang');
            $table->unsignedBigInteger('tugasfungsi_id');
            $table->unsignedBigInteger('strukturorganisasi_id');
            $table->unsignedBigInteger('peranstrategis_id');
            $table->text('sistematika_penyajian');
            $table->unsignedBigInteger('rencanastrategis_id');
            $table->unsignedBigInteger('perjanjiankinerja_id');
            $table->unsignedBigInteger('capaianspbe_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_kinerjas');
    }
}
