<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapaiankinerjaModelkinerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capaiankinerja_modelkinerja', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('capaiankinerja_id');
            $table->unsignedBigInteger('modelkinerja_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capaiankinerja_modelkinerja');
    }
}
