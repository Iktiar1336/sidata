<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerjanjiankinerjaSasaranstrategisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perjanjiankinerja_sasaranstrategis', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perjanjiankinerja_id');
            $table->foreign('perjanjiankinerja_id')->references('id')->on('perjanjian_kinerja');
            $table->unsignedBigInteger('sasaranstrategis_id');
            $table->foreign('sasaranstrategis_id')->references('id')->on('sasaran_strategis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perjanjiankinerja_sasaranstrategis');
    }
}
