<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelkinerjaRealisasianggaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelkinerja_realisasianggaran', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('modelkinerja_id');
            $table->unsignedBigInteger('realisasianggaran_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelkinerja_realisasianggaran');
    }
}
