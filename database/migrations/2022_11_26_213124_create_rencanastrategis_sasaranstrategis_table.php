<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRencanastrategisSasaranstrategisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rencanastrategis_sasaranstrategis', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sasaranstrategis_id');
            $table->foreign('sasaranstrategis_id')->references('id')->on('sasaran_strategis');
            $table->unsignedBigInteger('rencanastrategis_id');
            $table->foreign('rencanastrategis_id')->references('id')->on('rencana_strategis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rencanastrategis_sasaranstrategis');
    }
}
