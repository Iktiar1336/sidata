<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetKinerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_kinerja', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('indikatorkinerja_id');
            $table->foreign('indikatorkinerja_id')->references('id')->on('indikator_kinerja');
            $table->string('tahun');
            $table->string('type');
            $table->string('target');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_kinerjas');
    }
}
