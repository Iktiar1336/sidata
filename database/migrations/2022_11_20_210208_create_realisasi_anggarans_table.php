<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealisasiAnggaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realisasi_anggaran', function (Blueprint $table) {
            $table->id();
            $table->string('tahun');
            $table->unsignedBigInteger('sasaranstrategis_id');
            $table->foreign('sasaranstrategis_id')->references('id')->on('sasaran_strategis')->onDelete('cascade');
            $table->string('alokasi');
            $table->string('realisasi');
            $table->string('persentase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realisasi_anggarans');
    }
}
