<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapaianKinerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capaian_kinerja', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sasaranstrategis_id');
            $table->foreign('sasaranstrategis_id')->references('id')->on('sasaran_strategis');
            $table->string('realisasi');
            $table->string('tahun');
            $table->string('persentase');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capaian_kinerjas');
    }
}
