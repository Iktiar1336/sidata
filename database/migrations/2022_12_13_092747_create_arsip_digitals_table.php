<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArsipDigitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arsip_digital', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('arsipkolektor_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('instansi_id')->nullable();
            $table->text('type')->nullable();
            $table->text('folder')->nullable();
            $table->text('file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arsip_digitals');
    }
}
