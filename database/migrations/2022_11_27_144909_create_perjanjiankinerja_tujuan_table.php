<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerjanjiankinerjaTujuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perjanjiankinerja_tujuan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perjanjiankinerja_id');
            $table->foreign('perjanjiankinerja_id')->references('id')->on('perjanjian_kinerja');
            $table->unsignedBigInteger('tujuan_id');
            $table->foreign('tujuan_id')->references('id')->on('tujuan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perjanjiankinerja_tujuan');
    }
}
