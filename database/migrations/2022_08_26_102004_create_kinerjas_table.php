<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKinerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kinerja', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('workunit_id');
            $table->foreign('workunit_id')->references('id')->on('work_units')->onDelete('cascade');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('sub_category_id');
            $table->integer('year');
            $table->string('total');
            $table->text('description')->nullable();
            $table->integer('status')->default(1);
            $table->string('satuan')->nullable();
            $table->unsignedBigInteger('produsendata_id');
            $table->unsignedBigInteger('masterdata_id')->nullable();
            $table->unsignedBigInteger('chat_id')->nullable();
            $table->unsignedBigInteger('pengolahdata_id')->nullable();
            $table->unsignedBigInteger('fileupload_id')->nullable();
            $table->timestamp('save_at')->nullable();
            $table->timestamp('process_at')->nullable();
            $table->timestamp('send_at')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kinerjas');
    }
}
