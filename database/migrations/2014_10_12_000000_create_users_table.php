<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->text('username')->unique();
            $table->text('name');
            $table->text('email')->nullable();
            $table->text('phone')->nullable();
            $table->text('jabatan')->nullable();
            $table->text('workunit_id')->nullable();
            $table->text('instansi_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('password');
            $table->timestamp('last_seen')->nullable();
            $table->string('avatar')->nullable();
            $table->text('telegram_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
