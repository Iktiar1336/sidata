<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArsipKolektorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arsip_kolektor', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('instansi_id');
            $table->unsignedBigInteger('jenisarsip_id');
            $table->text('kode_klasifikasi');
            $table->text('nomor_berkas');
            $table->string('type');
            $table->text('uraian_informasi');
            $table->text('kurun_waktu');
            $table->text('jumlah');
            $table->text('media');
            $table->text('satuan');
            $table->text('kondisi');
            $table->text('tingkat_perkembangan');
            $table->text('folder');
            $table->text('boks');
            $table->text('keterangan');
            $table->boolean('status')->default(1);
            // $table->text('uraianitem');
            // $table->text('nomor_surat');
            // $table->text('tanggal_surat');
            // $table->text('kode_klasifikasi');
            // $table->text('tingkat_perkembangan');
            // $table->text('media_arsip');
            // $table->text('kondisi');
            // $table->text('jumlah');
            // $table->text('keterangan'); 
            // $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arsip_kolektors');
    }
}
