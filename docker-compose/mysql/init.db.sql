-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2021 at 10:45 AM
-- Server version: 10.4.8-MariaDB-log
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-spip`
--
CREATE DATABASE IF NOT EXISTS `e-spip` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `e-spip`;

-- --------------------------------------------------------

--
-- Table structure for table `area_dampak`
--

CREATE TABLE `area_dampak` (
  `id` int(11) NOT NULL,
  `nama_area_dampak` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `area_dampak`
--

INSERT INTO `area_dampak` (`id`, `nama_area_dampak`, `created_at`, `updated_at`) VALUES
(1, 'Kerugian Negara', '2021-12-05 22:03:22', '2021-12-05 22:03:22'),
(2, 'Peraturan', '2021-12-05 22:03:44', '2021-12-05 22:03:44');

-- --------------------------------------------------------

--
-- Table structure for table `entitas`
--

CREATE TABLE `entitas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_unit_kerja` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_unit_kerja` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pemilik_risiko` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `akun_sekunder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entitas`
--

INSERT INTO `entitas` (`id`, `nama_unit_kerja`, `kode_unit_kerja`, `pemilik_risiko`, `akun_sekunder`, `created_at`, `updated_at`) VALUES
(1, 'Kuasa Anggaran', 'KA', '1', NULL, NULL, '2021-11-21 23:18:44'),
(2, 'Sekretariat Utama', 'SU', '1', NULL, '2021-12-05 14:25:56', '2021-12-05 14:25:56');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `level_entitas`
--

CREATE TABLE `level_entitas` (
  `id` int(11) NOT NULL,
  `no_level` int(11) NOT NULL,
  `uraian` varchar(255) NOT NULL,
  `tahun` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level_entitas`
--

INSERT INTO `level_entitas` (`id`, `no_level`, `uraian`, `tahun`, `created_at`, `updated_at`) VALUES
(1, 1, 'Penjelasan', 2021, '2021-12-05 22:54:25', '2021-12-05 22:54:25');

-- --------------------------------------------------------

--
-- Table structure for table `level_unit_kerja`
--

CREATE TABLE `level_unit_kerja` (
  `id` int(11) NOT NULL,
  `no_level` int(11) NOT NULL,
  `uraian` text NOT NULL,
  `id_unit_kerja` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level_unit_kerja`
--

INSERT INTO `level_unit_kerja` (`id`, `no_level`, `uraian`, `id_unit_kerja`, `tahun`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', 2, 2021, '2021-12-05 23:02:34', '2021-12-05 23:02:34');

-- --------------------------------------------------------

--
-- Table structure for table `loggers`
--

CREATE TABLE `loggers` (
  `id` int(10) UNSIGNED NOT NULL,
  `method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `controller` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parameter` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remote_ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loggers`
--

INSERT INTO `loggers` (`id`, `method`, `controller`, `action`, `parameter`, `user`, `remote_ip`, `created_at`, `updated_at`) VALUES
(1, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', NULL, '127.0.0.1', '2021-12-05 16:20:08', '2021-12-05 16:20:08'),
(2, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', NULL, '127.0.0.1', '2021-12-05 16:21:28', '2021-12-05 16:21:28'),
(3, 'GET', 'App\\Http\\Controllers\\LogController', 'index', '[]', NULL, '127.0.0.1', '2021-12-05 16:21:30', '2021-12-05 16:21:30'),
(4, 'GET', 'App\\Http\\Controllers\\LogController', 'index', '[]', NULL, '127.0.0.1', '2021-12-05 16:21:44', '2021-12-05 16:21:44'),
(5, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-05 16:22:27', '2021-12-05 16:22:27'),
(6, 'GET', 'App\\Http\\Controllers\\LogController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-05 16:22:29', '2021-12-05 16:22:29'),
(7, 'GET', 'App\\Http\\Controllers\\LogController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-05 16:25:04', '2021-12-05 16:25:04'),
(8, 'GET', 'App\\Http\\Controllers\\PelaporanController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-05 16:25:22', '2021-12-05 16:25:22'),
(9, 'GET', 'App\\Http\\Controllers\\PelaporanController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-05 16:25:42', '2021-12-05 16:25:42'),
(10, 'GET', 'App\\Http\\Controllers\\VerifikasiRtpController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-05 16:29:12', '2021-12-05 16:29:12'),
(11, 'GET', 'App\\Http\\Controllers\\VerifikasiRtpController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-05 16:29:31', '2021-12-05 16:29:31'),
(12, 'GET', 'App\\Http\\Controllers\\HomeController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:01:53', '2021-12-16 17:01:53'),
(13, 'GET', 'App\\Http\\Controllers\\HomeController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:02:03', '2021-12-16 17:02:03'),
(14, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:02:24', '2021-12-16 17:02:24'),
(15, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:03:41', '2021-12-16 17:03:41'),
(16, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:04:09', '2021-12-16 17:04:09'),
(17, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:04:12', '2021-12-16 17:04:12'),
(18, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:04:22', '2021-12-16 17:04:22'),
(19, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:05:15', '2021-12-16 17:05:15'),
(20, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:05:29', '2021-12-16 17:05:29'),
(21, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:05:33', '2021-12-16 17:05:33'),
(22, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:05:54', '2021-12-16 17:05:54'),
(23, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'addSk', '[]', 'admin', '127.0.0.1', '2021-12-16 17:06:08', '2021-12-16 17:06:08'),
(24, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'storeSk', '{\"_token\":\"O0K1MKfmRo4Ga0a9JfSjGaNzF330WV1HAhiP9uZF\",\"deskripsi\":\"test\"}', 'admin', '127.0.0.1', '2021-12-16 17:06:11', '2021-12-16 17:06:11'),
(25, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:06:12', '2021-12-16 17:06:12'),
(26, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'addSk', '[]', 'admin', '127.0.0.1', '2021-12-16 17:06:47', '2021-12-16 17:06:47'),
(27, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'storeSk', '{\"_token\":\"O0K1MKfmRo4Ga0a9JfSjGaNzF330WV1HAhiP9uZF\",\"deskripsi\":\"test2\"}', 'admin', '127.0.0.1', '2021-12-16 17:06:50', '2021-12-16 17:06:50'),
(28, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:06:50', '2021-12-16 17:06:50'),
(29, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:07:17', '2021-12-16 17:07:17'),
(30, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:07:25', '2021-12-16 17:07:25'),
(31, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:07:31', '2021-12-16 17:07:31'),
(32, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:09:29', '2021-12-16 17:09:29'),
(33, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:09:38', '2021-12-16 17:09:38'),
(34, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'editSk', '[]', 'admin', '127.0.0.1', '2021-12-16 17:09:40', '2021-12-16 17:09:40'),
(35, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'editSk', '[]', 'admin', '127.0.0.1', '2021-12-16 17:09:47', '2021-12-16 17:09:47'),
(36, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'editSk', '[]', 'admin', '127.0.0.1', '2021-12-16 17:10:32', '2021-12-16 17:10:32'),
(37, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'updateSk', '{\"_token\":\"O0K1MKfmRo4Ga0a9JfSjGaNzF330WV1HAhiP9uZF\",\"id\":\"2\",\"deskripsi\":\"ab\"}', 'admin', '127.0.0.1', '2021-12-16 17:10:34', '2021-12-16 17:10:34'),
(38, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:10:34', '2021-12-16 17:10:34'),
(39, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'addSd', '[]', 'admin', '127.0.0.1', '2021-12-16 17:10:53', '2021-12-16 17:10:53'),
(40, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'storeSd', '{\"_token\":\"O0K1MKfmRo4Ga0a9JfSjGaNzF330WV1HAhiP9uZF\",\"deskripsi_1\":\"test\",\"deskripsi_2\":\"test2\"}', 'admin', '127.0.0.1', '2021-12-16 17:10:57', '2021-12-16 17:10:57'),
(41, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:10:57', '2021-12-16 17:10:57'),
(42, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-16 17:12:20', '2021-12-16 17:12:20'),
(43, 'GET', 'App\\Http\\Controllers\\HomeController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:38:31', '2021-12-19 01:38:31'),
(44, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:38:34', '2021-12-19 01:38:34'),
(45, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:39:07', '2021-12-19 01:39:07'),
(46, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:39:27', '2021-12-19 01:39:27'),
(47, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:39:30', '2021-12-19 01:39:30'),
(48, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:39:39', '2021-12-19 01:39:39'),
(49, 'GET', 'App\\Http\\Controllers\\EntitasController', 'addEntitas', '[]', 'admin', '127.0.0.1', '2021-12-19 01:39:42', '2021-12-19 01:39:42'),
(50, 'GET', 'App\\Http\\Controllers\\EntitasController', 'addEntitas', '[]', 'admin', '127.0.0.1', '2021-12-19 01:40:18', '2021-12-19 01:40:18'),
(51, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:40:21', '2021-12-19 01:40:21'),
(52, 'GET', 'App\\Http\\Controllers\\EntitasController', 'editEntitas', '[]', 'admin', '127.0.0.1', '2021-12-19 01:40:26', '2021-12-19 01:40:26'),
(53, 'GET', 'App\\Http\\Controllers\\EntitasController', 'editEntitas', '[]', 'admin', '127.0.0.1', '2021-12-19 01:40:59', '2021-12-19 01:40:59'),
(54, 'GET', 'App\\Http\\Controllers\\EntitasController', 'editEntitas', '[]', 'admin', '127.0.0.1', '2021-12-19 01:41:31', '2021-12-19 01:41:31'),
(55, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:41:39', '2021-12-19 01:41:39'),
(56, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:43:24', '2021-12-19 01:43:24'),
(57, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:43:30', '2021-12-19 01:43:30'),
(58, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:44:41', '2021-12-19 01:44:41'),
(59, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:45:10', '2021-12-19 01:45:10'),
(60, 'GET', 'App\\Http\\Controllers\\EntitasController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:45:48', '2021-12-19 01:45:48'),
(61, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:46:09', '2021-12-19 01:46:09'),
(62, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'deleteSk', '{\"_token\":\"LxzjQRyHYSwzmMTa8ZwUDLXiEfUsvL04z6d6ckxo\"}', 'admin', '127.0.0.1', '2021-12-19 01:46:14', '2021-12-19 01:46:14'),
(63, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:46:14', '2021-12-19 01:46:14'),
(64, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'deleteSk', '{\"_token\":\"LxzjQRyHYSwzmMTa8ZwUDLXiEfUsvL04z6d6ckxo\"}', 'admin', '127.0.0.1', '2021-12-19 01:46:17', '2021-12-19 01:46:17'),
(65, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:46:17', '2021-12-19 01:46:17'),
(66, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'deleteSk', '{\"_token\":\"LxzjQRyHYSwzmMTa8ZwUDLXiEfUsvL04z6d6ckxo\"}', 'admin', '127.0.0.1', '2021-12-19 01:46:19', '2021-12-19 01:46:19'),
(67, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:46:20', '2021-12-19 01:46:20'),
(68, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'addSk', '[]', 'admin', '127.0.0.1', '2021-12-19 01:46:22', '2021-12-19 01:46:22'),
(69, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'storeSk', '{\"_token\":\"LxzjQRyHYSwzmMTa8ZwUDLXiEfUsvL04z6d6ckxo\",\"deskripsi\":\"test\"}', 'admin', '127.0.0.1', '2021-12-19 01:46:25', '2021-12-19 01:46:25'),
(70, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:46:25', '2021-12-19 01:46:25'),
(71, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'deleteSk', '{\"_token\":\"LxzjQRyHYSwzmMTa8ZwUDLXiEfUsvL04z6d6ckxo\"}', 'admin', '127.0.0.1', '2021-12-19 01:46:28', '2021-12-19 01:46:28'),
(72, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:46:28', '2021-12-19 01:46:28'),
(73, 'POST', 'App\\Http\\Controllers\\ParameterRisikoController', 'deleteSd', '{\"_token\":\"LxzjQRyHYSwzmMTa8ZwUDLXiEfUsvL04z6d6ckxo\"}', 'admin', '127.0.0.1', '2021-12-19 01:46:53', '2021-12-19 01:46:53'),
(74, 'GET', 'App\\Http\\Controllers\\ParameterRisikoController', 'index', '[]', 'admin', '127.0.0.1', '2021-12-19 01:46:54', '2021-12-19 01:46:54');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_11_22_042822_create_skala_kemungkinans_table', 2),
(6, '2021_11_22_051952_create_logs_table', 3),
(7, '2021_11_22_052218_create_entitas_table', 3),
(8, '2021_12_05_231238_create_loggers_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `selera_risiko`
--

CREATE TABLE `selera_risiko` (
  `id` int(11) NOT NULL,
  `id_unit_kerja` int(11) NOT NULL,
  `selera_risiko` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `skala_dampak`
--

CREATE TABLE `skala_dampak` (
  `id` int(11) NOT NULL,
  `no_skala` int(11) NOT NULL,
  `id_area_dampak` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `skala_kemungkinan`
--

CREATE TABLE `skala_kemungkinan` (
  `id` int(11) NOT NULL,
  `no_skala` int(11) NOT NULL,
  `nama_skala` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `skala_kemungkinan`
--

INSERT INTO `skala_kemungkinan` (`id`, `no_skala`, `nama_skala`, `created_at`, `updated_at`) VALUES
(2, 1, 'ab', '2021-12-05 21:57:54', '2021-12-17 00:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `tingkat_risiko`
--

CREATE TABLE `tingkat_risiko` (
  `id` int(11) NOT NULL,
  `id_skala_kemungkinan` int(11) NOT NULL,
  `skala_dampak` int(11) NOT NULL,
  `tingkat_risiko` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tingkat_risiko`
--

INSERT INTO `tingkat_risiko` (`id`, `id_skala_kemungkinan`, `skala_dampak`, `tingkat_risiko`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 5, '2021-12-05 22:29:47', '2021-12-05 22:29:47'),
(3, 1, 2, 1, '2021-12-05 22:30:03', '2021-12-05 22:30:03'),
(4, 2, 1, 3, '2021-12-05 22:31:50', '2021-12-05 22:33:44'),
(5, 2, 2, 5, '2021-12-05 22:32:09', '2021-12-05 22:32:09'),
(6, 3, 1, 5, '2021-12-05 22:33:47', '2021-12-05 22:33:47'),
(7, 3, 2, 1, '2021-12-05 22:33:52', '2021-12-05 22:34:22');

-- --------------------------------------------------------

--
-- Table structure for table `unit_kerja`
--

CREATE TABLE `unit_kerja` (
  `id` int(11) NOT NULL,
  `kode_unit_kerja` varchar(100) NOT NULL,
  `nama_unit_kerja` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$3rr/HKhj2s6sk1IIPXdpWOubPM9TEE6qfYsoMI8ag4YLhjsN33tEK', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_unit_kerja`
--

CREATE TABLE `user_unit_kerja` (
  `id` int(11) NOT NULL,
  `id_unit_kerja` int(11) NOT NULL,
  `id_user` varchar(100) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_unit_kerja`
--

INSERT INTO `user_unit_kerja` (`id`, `id_unit_kerja`, `id_user`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 2, '1', 2, '2021-12-05 21:39:38', '2021-12-05 21:39:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area_dampak`
--
ALTER TABLE `area_dampak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entitas`
--
ALTER TABLE `entitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `level_entitas`
--
ALTER TABLE `level_entitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level_unit_kerja`
--
ALTER TABLE `level_unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loggers`
--
ALTER TABLE `loggers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `selera_risiko`
--
ALTER TABLE `selera_risiko`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skala_dampak`
--
ALTER TABLE `skala_dampak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skala_kemungkinan`
--
ALTER TABLE `skala_kemungkinan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tingkat_risiko`
--
ALTER TABLE `tingkat_risiko`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_kerja`
--
ALTER TABLE `unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_unit_kerja`
--
ALTER TABLE `user_unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area_dampak`
--
ALTER TABLE `area_dampak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `entitas`
--
ALTER TABLE `entitas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level_entitas`
--
ALTER TABLE `level_entitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `level_unit_kerja`
--
ALTER TABLE `level_unit_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `loggers`
--
ALTER TABLE `loggers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `selera_risiko`
--
ALTER TABLE `selera_risiko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skala_dampak`
--
ALTER TABLE `skala_dampak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `skala_kemungkinan`
--
ALTER TABLE `skala_kemungkinan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tingkat_risiko`
--
ALTER TABLE `tingkat_risiko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `unit_kerja`
--
ALTER TABLE `unit_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_unit_kerja`
--
ALTER TABLE `user_unit_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
