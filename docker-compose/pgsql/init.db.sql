--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 14.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: activity_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activity_log (
    id bigint NOT NULL,
    log_name character varying(255),
    description text NOT NULL,
    subject_type character varying(255),
    subject_id bigint,
    causer_type character varying(255),
    causer_id bigint,
    properties json,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.activity_log OWNER TO postgres;

--
-- Name: activity_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.activity_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activity_log_id_seq OWNER TO postgres;

--
-- Name: activity_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.activity_log_id_seq OWNED BY public.activity_log.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories (
    id bigint NOT NULL,
    parent_id integer,
    workunit_id bigint,
    name character varying(255) NOT NULL,
    description character varying(255),
    access character varying(255),
    status integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: file_uploads; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.file_uploads (
    id bigint NOT NULL,
    category_id bigint NOT NULL,
    filename text NOT NULL,
    year integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.file_uploads OWNER TO postgres;

--
-- Name: file_uploads_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.file_uploads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.file_uploads_id_seq OWNER TO postgres;

--
-- Name: file_uploads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.file_uploads_id_seq OWNED BY public.file_uploads.id;


--
-- Name: infografis; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.infografis (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    content text NOT NULL,
    image character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.infografis OWNER TO postgres;

--
-- Name: infografis_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.infografis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.infografis_id_seq OWNER TO postgres;

--
-- Name: infografis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.infografis_id_seq OWNED BY public.infografis.id;


--
-- Name: kinerja; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kinerja (
    id bigint NOT NULL,
    workunit_id bigint NOT NULL,
    category_id bigint NOT NULL,
    sub_category_id bigint NOT NULL,
    year integer NOT NULL,
    total integer NOT NULL,
    description character varying(255),
    status integer DEFAULT 1 NOT NULL,
    satuan character varying(255),
    save_at timestamp(0) without time zone,
    process_at timestamp(0) without time zone,
    send_at timestamp(0) without time zone,
    verified_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.kinerja OWNER TO postgres;

--
-- Name: kinerja_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kinerja_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kinerja_id_seq OWNER TO postgres;

--
-- Name: kinerja_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kinerja_id_seq OWNED BY public.kinerja.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_permissions OWNER TO postgres;

--
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_roles OWNER TO postgres;

--
-- Name: news; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    slug character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    content text NOT NULL,
    image character varying(255),
    status boolean DEFAULT true NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.news OWNER TO postgres;

--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_id_seq OWNER TO postgres;

--
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_id_seq OWNED BY public.news.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: performances; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.performances (
    id bigint NOT NULL,
    visi text,
    misi text,
    tugas text,
    fungsi text,
    struktur_organisasi text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.performances OWNER TO postgres;

--
-- Name: performances_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.performances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.performances_id_seq OWNER TO postgres;

--
-- Name: performances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.performances_id_seq OWNED BY public.performances.id;


--
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    module character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: personal_access_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.personal_access_tokens OWNER TO postgres;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_access_tokens_id_seq OWNER TO postgres;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.personal_access_tokens_id_seq OWNED BY public.personal_access_tokens.id;


--
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.role_has_permissions OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    username character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    workunit_id bigint,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    last_seen timestamp(0) without time zone,
    avatar character varying(255),
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: views; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.views (
    id bigint NOT NULL,
    viewable_type character varying(255) NOT NULL,
    viewable_id bigint NOT NULL,
    visitor text,
    collection character varying(255),
    viewed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.views OWNER TO postgres;

--
-- Name: views_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.views_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.views_id_seq OWNER TO postgres;

--
-- Name: views_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.views_id_seq OWNED BY public.views.id;


--
-- Name: work_units; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.work_units (
    id bigint NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.work_units OWNER TO postgres;

--
-- Name: work_units_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.work_units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.work_units_id_seq OWNER TO postgres;

--
-- Name: work_units_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.work_units_id_seq OWNED BY public.work_units.id;


--
-- Name: activity_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_log ALTER COLUMN id SET DEFAULT nextval('public.activity_log_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: file_uploads id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_uploads ALTER COLUMN id SET DEFAULT nextval('public.file_uploads_id_seq'::regclass);


--
-- Name: infografis id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.infografis ALTER COLUMN id SET DEFAULT nextval('public.infografis_id_seq'::regclass);


--
-- Name: kinerja id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kinerja ALTER COLUMN id SET DEFAULT nextval('public.kinerja_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: news id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news ALTER COLUMN id SET DEFAULT nextval('public.news_id_seq'::regclass);


--
-- Name: performances id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.performances ALTER COLUMN id SET DEFAULT nextval('public.performances_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: personal_access_tokens id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.personal_access_tokens_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: views id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.views ALTER COLUMN id SET DEFAULT nextval('public.views_id_seq'::regclass);


--
-- Name: work_units id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work_units ALTER COLUMN id SET DEFAULT nextval('public.work_units_id_seq'::regclass);


--
-- Data for Name: activity_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.activity_log (id, log_name, description, subject_type, subject_id, causer_type, causer_id, properties, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories (id, parent_id, workunit_id, name, description, access, status, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: file_uploads; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.file_uploads (id, category_id, filename, year, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: infografis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.infografis (id, title, content, image, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: kinerja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kinerja (id, workunit_id, category_id, sub_category_id, year, total, description, status, satuan, save_at, process_at, send_at, verified_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2019_12_14_000001_create_personal_access_tokens_table	1
5	2022_07_27_143118_create_permission_tables	1
6	2022_07_30_220017_create_activity_log_table	1
7	2022_08_05_095704_create_performances_table	1
8	2022_08_22_102222_create_categories_table	1
9	2022_08_23_092419_create_work_units_table	1
10	2022_08_26_102004_create_kinerjas_table	1
11	2022_08_30_113622_create_file_uploads_table	1
12	2022_08_31_214031_create_news_table	1
13	2022_08_31_214113_create_infografis_table	1
14	2022_08_31_215059_create_views_table	1
\.


--
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
1	App\\Models\\User	1
2	App\\Models\\User	2
4	App\\Models\\User	3
3	App\\Models\\User	4
5	App\\Models\\User	5
\.


--
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news (id, user_id, slug, title, content, image, status, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: performances; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.performances (id, visi, misi, tugas, fungsi, struktur_organisasi, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permissions (id, name, module, guard_name, created_at, updated_at) FROM stdin;
1	menu-data-unit-kerja	Data Unit Kerja	web	2022-09-05 09:51:47	2022-09-05 09:51:47
2	menu-data-kinerja	Data Kinerja	web	2022-09-05 09:51:47	2022-09-05 09:51:47
3	menu-model-kinerja	Model Kinerja	web	2022-09-05 09:51:47	2022-09-05 09:51:47
4	menu-user-management	User Management	web	2022-09-05 09:51:47	2022-09-05 09:51:47
5	menu-archive-identification	Archive Identification	web	2022-09-05 09:51:47	2022-09-05 09:51:47
6	menu-kotak-data-masuk	Kotak Data Masuk	web	2022-09-05 09:51:47	2022-09-05 09:51:47
7	menu-metadata	Metadata	web	2022-09-05 09:51:47	2022-09-05 09:51:47
8	menu-informasi-publik	Informasi Publik	web	2022-09-05 09:51:47	2022-09-05 09:51:47
9	menu-archive-collector	Archive Collector	web	2022-09-05 09:51:47	2022-09-05 09:51:47
10	view-dashboard-admin	Dashboard Admin	web	2022-09-05 09:51:47	2022-09-05 09:51:47
11	view-dashboard	Dashboard	web	2022-09-05 09:51:47	2022-09-05 09:51:47
12	view-dashboard-eksekutif	Dashboard Eksekutif	web	2022-09-05 09:51:47	2022-09-05 09:51:47
13	view-statistik-produsen-data	Statistik Produsen Data	web	2022-09-05 09:51:48	2022-09-05 09:51:48
14	view-statistik-master-data	Statistik Master Data	web	2022-09-05 09:51:48	2022-09-05 09:51:48
15	view-statistik-pengolah-data	Statistik Pengolah Data	web	2022-09-05 09:51:48	2022-09-05 09:51:48
16	show-notif-activity	Notifikasi Aktivitas	web	2022-09-05 09:51:48	2022-09-05 09:51:48
17	show-notification-master-data	Notifikasi Master Data	web	2022-09-05 09:51:48	2022-09-05 09:51:48
18	show-notification-produsen-data	Notifikasi Produsen Data	web	2022-09-05 09:51:48	2022-09-05 09:51:48
19	show-notification-pengolah-data	Notifikasi Pengolah Data	web	2022-09-05 09:51:48	2022-09-05 09:51:48
20	list-berita	Informasi Publik	web	2022-09-05 09:51:48	2022-09-05 09:51:48
21	create-berita	Informasi Publik	web	2022-09-05 09:51:48	2022-09-05 09:51:48
22	edit-berita	Informasi Publik	web	2022-09-05 09:51:48	2022-09-05 09:51:48
23	delete-berita	Informasi Publik	web	2022-09-05 09:51:48	2022-09-05 09:51:48
24	list-infografis	Informasi Publik	web	2022-09-05 09:51:48	2022-09-05 09:51:48
25	create-infografis	Informasi Publik	web	2022-09-05 09:51:48	2022-09-05 09:51:48
26	edit-infografis	Informasi Publik	web	2022-09-05 09:51:48	2022-09-05 09:51:48
27	delete-infografis	Informasi Publik	web	2022-09-05 09:51:48	2022-09-05 09:51:48
28	user-profile	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
29	user-list	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
30	user-create	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
31	user-edit	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
32	user-delete	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
33	user-show	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
34	role-list	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
35	role-create	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
36	role-edit	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
37	role-delete	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
38	role-show	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
39	permission-list	User Management	web	2022-09-05 09:51:48	2022-09-05 09:51:48
40	category-list	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
41	category-create	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
42	category-edit	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
43	category-delete	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
44	sub-category-list	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
45	sub-category-create	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
46	sub-category-edit	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
47	sub-category-delete	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
48	list-data-kinerja	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
49	create-data-kinerja	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
50	edit-data-kinerja	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
51	delete-data-kinerja	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
52	send-data-kinerja	Data Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
53	workunit-list	Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
54	workunit-create	Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
55	workunit-edit	Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
56	workunit-delete	Unit Kerja	web	2022-09-05 09:51:48	2022-09-05 09:51:48
\.


--
-- Data for Name: personal_access_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
28	2
4	2
10	2
28	4
1	4
11	4
15	4
4	4
28	3
4	3
11	3
1	3
13	3
28	5
4	5
11	5
1	5
14	5
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, description, guard_name, created_at, updated_at) FROM stdin;
1	super-admin	Super Admin	web	2022-09-05 09:51:48	2022-09-05 09:51:48
2	admin	Administrator	web	2022-09-05 09:51:49	2022-09-05 09:51:49
3	produsen-data	Produsen Data	web	2022-09-05 09:51:49	2022-09-05 09:51:49
4	pengolah-data	Pengolah Data	web	2022-09-05 09:51:49	2022-09-05 09:51:49
5	master-data	Master Data	web	2022-09-05 09:51:49	2022-09-05 09:51:49
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, username, name, email, workunit_id, email_verified_at, password, last_seen, avatar, remember_token, created_at, updated_at) FROM stdin;
2	admin	Admin	admin@gmail.com	1	2022-09-05 09:51:49	$2y$10$eCBJtkQ8tNi3zJ3aFhhSPeQLcRdbIW.DSwwSx8Ne5jtwPXFvbrjaK	\N	\N	1JZNdangzD	2022-09-05 09:51:49	2022-09-05 09:51:49
3	pengolah-data	Pengolah Data	pengolahdata@gmail.com	15	2022-09-05 09:51:49	$2y$10$s5BxQB9jPT54/6dRE5zrP.oTdlcr3G9ZQcjCotR5Sh546HvNW3ium	\N	\N	n6musvy4J7	2022-09-05 09:51:49	2022-09-05 09:51:49
4	produsen-data	Produsen Data	produsendata@gmail.com	3	2022-09-05 09:51:49	$2y$10$xn.L.0xcxTRlgsy/aVHUO.3Z.8J3SUyq9SEe9h2vy81H8TAQ3/186	\N	\N	H6Sht7M2PD	2022-09-05 09:51:49	2022-09-05 09:51:49
5	master-data	Master Data	masterdata@gmail.com	15	2022-09-05 09:51:49	$2y$10$5B1MuWdytYmlEM6J.sT8DO.PSxNMKxADyqt1Gl/aYYmxyrzvH6vJ2	\N	\N	ptbIX9YI4G	2022-09-05 09:51:49	2022-09-05 09:51:49
1	super-admin	Super Admin	superadmin@gmail.com	1	2022-09-05 09:51:49	$2y$10$XvHbnhGRovHCFh3bmN9PTOuizx9FU7NKU4f9ZZFuYvwRfkHhVwa/C	2022-09-05 09:52:31	\N	xt1AHkApGS	2022-09-05 09:51:49	2022-09-05 09:52:31
\.


--
-- Data for Name: views; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.views (id, viewable_type, viewable_id, visitor, collection, viewed_at) FROM stdin;
\.


--
-- Data for Name: work_units; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.work_units (id, code, name, description, status, created_at, updated_at) FROM stdin;
1	001	Administrator	Administrator 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
2	002	Biro Perencanaan dan Hubungan Masyarakat	Biro Perencanaan dan Hubungan Masyarakat 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
3	003	Biro Organisasi, Kepegawaian dan Hukum 	Biro Organisasi, Kepegawaian dan Hukum 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
4	004	Biro Umum 	Biro Umum  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
5	005	Direktorat Kearsipan Pusat 	Direktorat Kearsipan Pusat 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
6	006	Direktorat Kearsipan Daerah I 	Direktorat Kearsipan Daerah I  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
7	007	Direktorat Kearsipan Daerah II	Direktorat Kearsipan Daerah II 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
8	008	Direktorat SDM Kearsipan dan Sertifikasi 	Direktorat SDM Kearsipan dan Sertifikasi  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
9	009	Pusat Pendidikan dan Pelatihan Kearsipan 	Pusat Pendidikan dan Pelatihan Kearsipan  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
10	010	Direktorat Akuisisi	Direktorat Akuisisi 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
11	011	Direktorat Pengolahan 	Direktorat Pengolahan   	1	2022-09-05 09:51:49	2022-09-05 09:51:49
12	012	Direktorat Preservasi 	Direktorat Preservasi  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
13	013	Direktorat Layanan dan Pemanfaatan	Direktorat Layanan dan Pemanfaatan 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
14	014	Pusat Sistem dan Jaringan Informasi Kearsipan Nasional 	Pusat Sistem dan Jaringan Informasi Kearsipan Nasional  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
15	015	Pusat Data dan Informasi 	Pusat Data dan Informasi  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
16	016	Pusat Pengkajian dan Pengembangan Sistem Kearsipan 	Pusat Pengkajian dan Pengembangan Sistem Kearsipan  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
17	017	Pusat Jasa Kearsipan	Pusat Jasa Kearsipan 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
18	018	Inspektorat 	Inspektorat  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
19	019	Pusat Akreditasi Kearsipan 	Pusat Akreditasi Kearsipan  	1	2022-09-05 09:51:49	2022-09-05 09:51:49
20	020	Balai Arsip Statis dan Tsunami	Balai Arsip Statis dan Tsunami 	1	2022-09-05 09:51:49	2022-09-05 09:51:49
\.


--
-- Name: activity_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.activity_log_id_seq', 1, false);


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_id_seq', 1, false);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: file_uploads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.file_uploads_id_seq', 1, false);


--
-- Name: infografis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.infografis_id_seq', 1, false);


--
-- Name: kinerja_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kinerja_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 14, true);


--
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_id_seq', 1, false);


--
-- Name: performances_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.performances_id_seq', 1, false);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permissions_id_seq', 56, true);


--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.personal_access_tokens_id_seq', 1, false);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 5, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 5, true);


--
-- Name: views_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.views_id_seq', 1, false);


--
-- Name: work_units_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.work_units_id_seq', 20, true);


--
-- Name: activity_log activity_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_log
    ADD CONSTRAINT activity_log_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: file_uploads file_uploads_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_uploads
    ADD CONSTRAINT file_uploads_pkey PRIMARY KEY (id);


--
-- Name: infografis infografis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.infografis
    ADD CONSTRAINT infografis_pkey PRIMARY KEY (id);


--
-- Name: kinerja kinerja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kinerja
    ADD CONSTRAINT kinerja_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- Name: news news_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: performances performances_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.performances
    ADD CONSTRAINT performances_pkey PRIMARY KEY (id);


--
-- Name: permissions permissions_name_guard_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_name_guard_name_unique UNIQUE (name, guard_name);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_token_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);


--
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: roles roles_name_guard_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_guard_name_unique UNIQUE (name, guard_name);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_unique UNIQUE (username);


--
-- Name: views views_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.views
    ADD CONSTRAINT views_pkey PRIMARY KEY (id);


--
-- Name: work_units work_units_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work_units
    ADD CONSTRAINT work_units_pkey PRIMARY KEY (id);


--
-- Name: activity_log_log_name_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX activity_log_log_name_index ON public.activity_log USING btree (log_name);


--
-- Name: causer; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX causer ON public.activity_log USING btree (causer_type, causer_id);


--
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: personal_access_tokens_tokenable_type_tokenable_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);


--
-- Name: subject; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX subject ON public.activity_log USING btree (subject_type, subject_id);


--
-- Name: views_viewable_type_viewable_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX views_viewable_type_viewable_id_index ON public.views USING btree (viewable_type, viewable_id);


--
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

