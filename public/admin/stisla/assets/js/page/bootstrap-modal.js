"use strict";

$("#modal-1").fireModal({body: 'Modal body text goes here.'});
$("#modal-2").fireModal({body: 'Modal body text goes here.', center: true});

let modal_3_body = '<p>Object to create a button on the modal.</p><pre class="language-javascript"><code>';
modal_3_body += '[\n';
modal_3_body += ' {\n';
modal_3_body += "   text: 'Login',\n";
modal_3_body += "   submit: true,\n";
modal_3_body += "   class: 'btn btn-primary btn-shadow',\n";
modal_3_body += "   handler: function(modal) {\n";
modal_3_body += "     alert('Hello, you clicked me!');\n"
modal_3_body += "   }\n"
modal_3_body += ' }\n';
modal_3_body += ']';
modal_3_body += '</code></pre>';
$("#modal-3").fireModal({
  title: 'Modal with Buttons',
  body: modal_3_body,
  buttons: [
    {
      text: 'Click, me!',
      class: 'btn btn-primary btn-shadow',
      handler: function(modal) {
        alert('Hello, you clicked me!');
      }
    }
  ]
});

$("#modal-4").fireModal({
  footerClass: 'bg-whitesmoke',
  body: 'Add the <code>bg-whitesmoke</code> class to the <code>footerClass</code> option.',
  buttons: [
    {
      text: 'No Action!',
      class: 'btn btn-primary btn-shadow',
      handler: function(modal) {
      }
    }
  ]
});

$("#modal-5").fireModal({
  title: 'Test Email',
  bodyclass: 'modal-lg',
  body: $("#modal-email-part"),
  autoFocus: true,
});

$("#modal-indikator-kinerja").fireModal({
  title: 'Tambah Indikator Kinerja',
  bodyclass: 'modal-lg',
  body: $("#modal-indikator-kinerja-part"),
  autoFocus: true,
});

$("#modal-capaian-kinerja").fireModal({
  title: 'Tambah Capaian Kinerja',
  bodyclass: 'modal-lg',
  body: $("#modal-capaian-kinerja-part"),
  autoFocus: true,
  onFormSubmit: function(modal) {
    var tahun = $("#tahun").val();
    var sasaranstrategis = $("#sasaran_strategis").val();
    var realisasi = $("#realisasi").val();
    if (tahun == '' || sasaranstrategis == '' || realisasi == '') {
      alert('Semua data harus diisi!');
    }
  }
});

$("#modal-add-expense").fireModal({
    title: 'Add Expense',
    footerClass: 'bg-whitesmoke',
    body: $("#modal-add-expense-part"),
    autoFocus: false,
    onFormSubmit: function (modal) {
        var ex_date = $('#add_ex_date').val();
        var ex_item = $('#add_ex_item').val();
        var ex_amt = $('#add_ex_amt').val();
        if (ex_date == '') {
            alert('Please Selet date!');
            return false;
        } else if (ex_item == '') {
            alert('Please provide Expense Item!');
            return false;
        } else if (ex_amt == '') {
            alert('Please provide Expense Amount!');
            return false;
        }
    },
});

$("#modal-perjanjian-kinerja").fireModal({
  title: 'Tambah Perjanjian Kinerja',
  bodyclass: 'modal-lg',
  body: $("#modal-perjanjian-kinerja-part"),
  autoFocus: true,
});

$("#modal-target-kinerja").fireModal({
  title: 'Tambah Target Kinerja',
  bodyclass: 'modal-lg',
  body: $("#modal-target-kinerja-part"),
  autoFocus: true,
});

$("#modal-tujuan").fireModal({
  title: 'Tambah Tujuan',
  bodyclass: 'modal-lg',
  body: $("#modal-tujuan-part"), 
  autoFocus: true,
});

$("#modal-sasaran-strategis").fireModal({
  title: 'Tambah Sasaran Strategis',
  bodyclass: 'modal-lg',
  body: $("#modal-sasaran-strategis-part"),
  autoFocus: true,
});

$("#modal-rencana-strategis").fireModal({
  title: 'Tambah Rencana Strategis',
  bodyclass: 'modal-lg',
  body: $("#modal-rencana-strategis-part"),
  autoFocus: true,
});

$("#modal-peran-strategis").fireModal({
  title: 'Tambah Peran Strategis',
  bodyclass: 'modal-lg',
  body: $("#modal-peran-strategis-part"),
  autoFocus: true,
});

$("#modal-13").fireModal({
  title: 'Tambah Instansi',
  bodyclass: 'modal-lg',
  body: $("#modal-instansi-part"),
  autoFocus: true,
});

$("#modal-14").fireModal({
  title: 'Tambah Sub Instansi',
  bodyclass: 'modal-lg',
  body: $("#modal-subinstansi-part"),
  autoFocus: true,
});


$("#modal-6").fireModal({
  title: 'Tambah Visi & Misi',
  bodyclass: 'modal-lg',
  body: $("#modal-visi-misi-part"),
  autoFocus: true,
});

$("#modal-7").fireModal({
  title: 'Tambah Tugas & Fungsi',
  bodyclass: 'modal-lg',
  body: $("#modal-tugas-fungsi-part"),
  autoFocus: true,
});

$("#modal-8").fireModal({
  title: 'Tambah Struktur Organisasi',
  bodyclass: 'modal-lg',
  body: $("#modal-struktur-organisasi-part"),
  autoFocus: true,
});

$("#modal-category").fireModal({
  title: 'Tambah Kategori Data',
  bodyclass: 'modal-lg',
  body: $("#modal-kategori-data-part"),
  autoFocus: true,
});

$("#modal-settingemail").fireModal({
  title: 'Test Email',
  bodyclass: 'modal-lg',
  body: $("#modal-settingemail-part"),
  autoFocus: true,
});

$("#modal-arsips").fireModal({
  title: 'Tambah Arsip',
  bodyclass: 'modal-lg',
  body: $("#modal-arsip-kolektor-part"),
  autoFocus: true,
});

$("#modal-user").fireModal({
  title: 'Tambah User',
  bodyclass: 'modal-lg',
  body: $("#modal-user-part"),
  autoFocus: true,
});

$("#modal-workunit").fireModal({
  title: 'Tambah Unit Kerja',
  bodyclass: 'modal-lg',
  body: $("#modal-workunit-part"),
  autoFocus: true,
});

$("#modal-sub-category").fireModal({
  title: 'Tambah Sub Kategori Data',
  bodyclass: 'modal-lg',
  body: $("#modal-sub-kategori-data-part"),
  autoFocus: true,
});


// $("#modal-6").fireModal({
//   body: '<p>Now you can see something on the left side of the footer.</p>',
//   created: function(modal) {
//     modal.find('.modal-footer').prepend('<div class="mr-auto"><a href="#">I\'m a hyperlink!</a></div>');
//   },
//   buttons: [
//     {
//       text: 'No Action',
//       submit: true,
//       class: 'btn btn-primary btn-shadow',
//       handler: function(modal) {
//       }
//     }
//   ]
// });

$('.oh-my-modal').fireModal({
  title: 'My Modal',
  body: 'This is cool plugin!'
});