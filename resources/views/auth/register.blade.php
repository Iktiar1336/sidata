@extends('layouts.auth')

@section('title')
    Sign Up
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-xl-11 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-6 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-6">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Sign Up!</h1>
                            </div>
                            <form method="POST" action="{{ route('register') }}" class="user">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input id="name" type="name" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" tabindex="1" value="{{ old('name') }}"  required autocomplete="name" autofocus placeholder="Masukan nama anda">
                                        @error('name')
                                            <span class="invalid-feedback">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="email" type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" tabindex="1" value="{{ old('email') }}"  required autocomplete="email" autofocus placeholder="Masukan email anda">
                                        @error('email')
                                            <span class="invalid-feedback">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input id="password" type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" tabindex="2" required autocomplete="new-password" placeholder="Masukan password anda">
                                    @error('password')
                                        <span class="invalid-feedback">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                </div>
                    
                                <div class="form-group">
                                    <input id="password-confirm" type="password" class="form-control form-control-user" name="password_confirmation" tabindex="3" required autocomplete="new-password" placeholder="Masukan konfirmasi password anda">
                                </div>
                    
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me" {{ old('remember') ? 'checked' : '' }} >
                                        <label class="custom-control-label" for="remember-me">Remember Me</label>
                                    </div>
                                </div>
                    
                                <div class="form-group">
                                    <button type="submit" class="btn btn-vivid-cerulean btn-user btn-block" tabindex="4">
                                        Sign Up
                                    </button>
                                </div>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="{{ route('password.request') }}">Lupa password ?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="{{ route('login') }}">Sudah punya akun ? Sign In!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="login-brand">
    <img src="{{ asset('admin/stisla/assets/img/stisla-fill.svg') }}" alt="logo" width="100" class="shadow-light rounded-circle">
</div>

<div class="card card-primary">
    <div class="card-header justify-content-center">
        <h4>Sign Up Page</h4>
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route('register') }}" class="needs-validation" novalidate="">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" tabindex="1" value="{{ old('name') }}"  required autocomplete="name" autofocus>
                <div class="invalid-feedback">
                    Please fill in your name
                </div>
                @error('name')
                    <span class="invalid-feedback">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" tabindex="1" value="{{ old('email') }}"  required autocomplete="email" autofocus>
                <div class="invalid-feedback">
                    Please fill in your email
                </div>
                @error('email')
                    <span class="invalid-feedback">
                        {{ $message }}
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <div class="d-block">
                    <label for="password" class="control-label">Password</label>
                </div>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" tabindex="2" required autocomplete="new-password">
                <div class="invalid-feedback">
                    please fill in your password
                </div>
                @error('password')
                    <span class="invalid-feedback">
                        {{ $message }}
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <div class="d-block">
                    <label for="password-confirm" class="control-label">Password Confirm</label>
                </div>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" tabindex="3" required autocomplete="new-password">
                <div class="invalid-feedback">
                    please fill in your password confirmation
                </div>
            </div>

            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me" {{ old('remember') ? 'checked' : '' }} >
                    <label class="custom-control-label" for="remember-me">Remember Me</label>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                    Sign Up
                </button>
            </div>
        </form>
    </div>
</div>
<div class="mt-5 text-muted text-center">
    Already have an account? <a href="{{ route('login') }}">Sign In</a>
</div> --}}
@endsection
