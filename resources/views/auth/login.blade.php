@extends('layouts.auth')

@section('title')
Sign In
@endsection

@section('css')
<style>
    .my-6 {
        margin-top: 4rem !important;
    }

    .field-icon {
        float: right;
        margin-top: -33px;
        margin-right: 16px;
        position: relative;
        color: #099CF4;
        z-index: 2;
    }

    .g-recaptcha div {
        width: 350px !important;
    }
</style>
@endsection

@section('content')

<!-- Outer Row -->
<div class="row justify-content-center">

    <div class="col-xl-12 col-lg-12 col-md-11">

        <div class="card o-hidden border-0 shadow-lg my-6">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                    <div class="col-lg-6">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Log in!</h1>
                            </div>
                            <form method="POST" action="{{ route('login') }}" class="user">
                                @csrf
                                <div class="form-group">
                                    <input id="email" type="text"
                                        class="form-control form-control-user @error('username') is-invalid @enderror"
                                        name="username" tabindex="1" value="{{ old('username') }}" required
                                        autocomplete="username" autofocus placeholder="Masukan Username atau Email Anda">
                                    @error('username')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input id="password" type="password"
                                        class="form-control form-control-user @error('password') is-invalid @enderror"
                                        name="password" tabindex="2" required autocomplete="current-password"
                                        placeholder="Masukan Password Anda">

                                    <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>

                                    @error('password') <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <center>
                                        {!! NoCaptcha::display() !!}
                                        {!! NoCaptcha::renderJs() !!}
                                    </center>

                                    @error('g-recaptcha-response')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember" class="custom-control-input" tabindex="3"
                                            id="remember-me" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="remember-me">Ingat Saya</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-vivid-cerulean btn-user btn-block"
                                        tabindex="4">
                                        Login
                                    </button>
                                </div>
                            </form>
                            <div class="text-center">
                                <a class="small" href="{{ route('password.request') }}">Lupa password ?</a>
                                <br>
                                <a class="small" href="/"> Kembali ke halaman utama</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection