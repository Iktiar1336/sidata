<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <title>Verifikasi Email!</title>

    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body {
            background-color: #f9f9f9;
            overflow-x: hidden;
        }

        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 120vh;
            margin: auto;
        }

        .img-logo {
            width: 200px;
            height: 72px;
            display: block;
            margin: auto;
            margin-bottom: 20px;
        }

        .card{
            width: 35rem;
            display: block;
            margin: auto;
        }

        .card-header {
            background: #7289DA url(https://cdn.discordapp.com/email_assets/f0a4cc6d7aaa7bdf2a3c15a193c6d224.png) top center / cover no-repeat;
            height: 130px;
            display: flex;
            justify-content: center;
            align-items: center;
            color: white;
            font-weight: bold;
            font-size: 30px;
            text-transform: capitalize;
        }

        .card-body{
            padding-left: 40px;
            padding-right: 40px;
        }

        .img-banner{
            width: 300px;
            height: auto;
            display: block;
            margin-top: -30px !important;
            margin: auto;
        }

        .card-text{
            text-align: justify;
        }

        @media only screen and (max-width: 600px) {
            .card{
                width: 27rem;
            }

            .card-header{
                font-size: 26px;
            }
        }
    </style>
</head>

<body>
    @php
        $first = true;
    @endphp
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="{{ asset('assets/img/logo-sidata.png') }}" alt="Logo" class="img-logo">
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Selamat Datang di {{ config('app.name') }}
                    </div>
                    <div class="card-body">
                        <img src="{{ asset('img-email-verify.png') }}" alt="img-banner" class="img-banner">
                        <h5 class="card-title">
                            Hallo {{ Auth::user()->name }},
                        </h5>
                        <p class="card-text">
                            Selamat akun anda telah berhasil di buat oleh Admin {{ config('app.name') }}. sebelum melanjutkan, anda harus memverifikasi email anda terlebih dahulu.
                            <br>
                        </p>
                        <span id="span">Klik tombol dibawah ini untuk memverifikasi email anda.</span>
                        <center>
                            <form method="POST" action="{{ route('verification.resend') }}">
                                @csrf
                                @if($first == true)
                                    <button type="submit" class="btn btn-primary" style="margin-top: 20px;">Verifikasi Email</button>
                                @else
                                    <button type="submit" class="btn btn-primary" style="margin-top: 20px;">Kirim Ulang Link Verifikasi</button>
                                @endif
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
        integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    @if (session('resent'))
        <script>
            $('.card-text').empty();
            $('.card-text').append(
                `
                Link verifikasi telah dikirim ke email anda. <br>
                Silahkan cek email anda untuk memverifikasi email anda. <br>
                Jika anda tidak menerima email, silahkan klik tombol dibawah ini untuk mengirim ulang link verifikasi.
                `
            );
            $('#span').remove();
            $('.btn-primary').text('Kirim Ulang Link Verifikasi');
            swal({
                title: "Berhasil!",
                text: "Link verifikasi telah dikirim ke email anda.",
                icon: "success",
            });
        </script>
    @endif
</body>

</html>

{{-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Verifikasi Email</title>

    <style>

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
            display: block;
            margin: 13px 0;
        }

        @media only screen and (max-width:480px) {
            @-ms-viewport {
                width: 320px;
            }

            @viewport {
                width: 320px;
            }
        }

        @media only screen and (max-width:600px) {
            #card{
                width: 100%;
                margin-left: 10px !important;
                margin-right: 10px !important;
            }
        }
    </style>
</head>

<body>
    <div style="background-color:#F9F9F9;">
        <!--[if mso | IE]>
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="width:640px;">
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
        <div style="margin:0px auto;max-width:640px;background:transparent;">
            <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:transparent;" align="center" border="0">
                <tbody>
                    <tr>
                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:40px 0px;">

                            <div aria-labelledby="mj-column-per-100" class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                                <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="word-break:break-word;font-size:0px;padding:0px;" align="center">
                                                <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width:200px;"><a href="/"><img alt="" title="" src="{{ asset('assets/img/logo-sidata.png') }}"
style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:70px;"></a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</div>
<div id="card" style="max-width:640px;margin:0 auto;box-shadow:0px 1px 5px rgba(0,0,0,0.1);border-radius:4px;overflow:hidden">
    <div style="margin:0px auto;width:100%;background:#7289DA url(https://cdn.discordapp.com/email_assets/f0a4cc6d7aaa7bdf2a3c15a193c6d224.png) top center / cover no-repeat;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#7289DA url(https://cdn.discordapp.com/email_assets/f0a4cc6d7aaa7bdf2a3c15a193c6d224.png) top center / cover no-repeat;" align="center" border="0" background="https://cdn.discordapp.com/email_assets/f0a4cc6d7aaa7bdf2a3c15a193c6d224.png">
            <tbody>
                <tr>
                    <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:57px;">
                        <div style="cursor:auto;color:white;font-family:Whitney, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-size:36px;font-weight:600;line-height:36px;text-align:center;">
                            Selamat Datang di {{ env('APP_NAME') }}
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin:0px auto;max-width:640px;background:#ffffff;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0">
            <tbody>
                <tr>
                    <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:40px 70px;">
                        <div aria-labelledby="mj-column-per-100" class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                            <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="word-break:break-word;font-size:0px;padding:0px 0px 20px;" align="left">
                                            <div style="cursor:auto;color:#737F8D;font-family:Whitney, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-size:16px;line-height:24px;text-align:left;">
                                                <p><img src="{{ asset('img-email-verify.png') }}" alt="Party Wumpus" title="None" width="500" style="height: auto;"></p>

                                                <h2 style="font-family: Whitney, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-weight: 500;font-size: 20px;color: #4F545C;letter-spacing: 0.27px;">Hallo {{ Auth::user()->name }},</h2>
                                                <p style="text-align: justify">
                                                    Selamat akun anda telah berhasil di buat oleh Admin {{ env('APP_NAME') }}. Sebelum melanjutkan, anda harus memverifikasi email anda terlebih dahulu.
                                                </p>
                                                <p>
                                                    Klik tombol dibawah ini untuk memverifikasi email anda.
                                                </p>

                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center">
                                            <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;">
                                                <tbody>
                                                    <tr>
                                                        <td style="border:none;border-radius:3px;color:white;cursor:auto;padding:15px 19px;" align="center" valign="middle" bgcolor="#7289DA">
                                                            <form method="POST" action="{{ route('verification.resend') }}">
                                                                @csrf
                                                                <button type="submit" style="text-decoration:none;line-height:100%;background:#7289DA;color:white;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:15px;font-weight:normal;text-transform:none;margin:0px;border: 0px;">{{ __('Klik untuk memverifikasi') }}</button>.
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div style="margin:0px auto;max-width:640px;background:transparent;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:transparent;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px;">
                    <div aria-labelledby="mj-column-per-100" class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-break:break-word;font-size:0px;">
                                        <div style="font-size:1px;line-height:12px;">&nbsp;</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div style="margin:0px auto;max-width:640px;background:transparent;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:transparent;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;">
                    <div aria-labelledby="mj-column-per-100" class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-break:break-word;font-size:0px;padding:0px;" align="center">
                                        <div style="cursor:auto;color:#99AAB5;font-family:Whitney, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-size:12px;line-height:24px;text-align:center;">
                                            &copy; Copyright 2022. Sistem Informasi Pengumpulan Data All Rights Reserved
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
</div>
</body>

</html> --}}

{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verifikasi alamat email Anda') }}</div>

<div class="card-body">
    @if (session('resent'))
    <div class="alert alert-success" role="alert">
        {{ __('Tautan verifikasi baru telah dikirim ke alamat email Anda.') }}
    </div>
    @endif

    {{ __('Sebelum melanjutkan, silahkan lakukan verifikasi email terlebih dahulu') }},
    {{ __('Jika anda tidak menerima email') }},
    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
        @csrf
        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Klik untuk memverifikasi') }}</button>.
    </form>
</div>
</div>
</div>
</div>
</div>
@endsection --}}
