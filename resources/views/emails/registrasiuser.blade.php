@extends('layouts.emails.layout')

@section('content')
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 21px; font-weight: bold; margin-top: 0; text-align: left;">Halo, {{ $user->name }}
</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
    Selamat datang di aplikasi <b>SIDATA</b> ( Sistem Informasi Pengumpulan Data ). Kami telah mendaftarkan akun anda, berikut adalah detail akun anda :
</p>
<table>
    <tr>
        <td>Nama</td>
        <td>&nbsp;&nbsp;&nbsp;:</td>
        <td>{{ $user->name }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>&nbsp;&nbsp;&nbsp;:</td>
        <td>{{ $user->email }}</td>
    </tr>
    <tr>
        <td>Username</td>
        <td>&nbsp;&nbsp;&nbsp;:</td>
        <td>{{ $user->username }}</td>
    </tr>
    <tr>
        <td>Jenis User</td>
        <td>&nbsp;&nbsp;&nbsp;:</td>
        <td>{{ $user->roles()->first()->description }}</td>
    </tr>
    @if ($user->workunit_id != null)
    <tr>
        <td>Unit Kerja</td>
        <td>&nbsp;&nbsp;&nbsp;:</td>
        <td>{{ $user->workunit->name }}</td>
    </tr>
    @elseif($user->instansi_id != null)
    <tr>
        <td>Intansi</td>
        <td>&nbsp;&nbsp;&nbsp;:</td>
        <td>{{ $user->instansi->nama_instansi }}</td>
    </tr>
    @endif
    <tr>
        <td>Password</td>
        <td>&nbsp;&nbsp;&nbsp;:</td>
        <td>{{ $password }}</td>
    </tr>
</table>
<br>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
    Silahkan login dan lakukan verifikasi akun anda pada aplikasi SIDATA.

</p>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Terima Kasih,<br><br>
    Pusat Data & Informasi <br> Arsip Nasional Republik Indonesia
</p>

<table class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation"
    style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; border-top: 1px solid #e8e5ef; margin-top: 25px; padding-top: 25px;">
    <tr>
        <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
            <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; line-height: 1.5em; margin-top: 0; text-align: center; font-size: 14px;color:#FF2D00;">
                Mohon untuk menjaga kerahasiaan akun anda, dan jangan memberikan informasi akun anda kepada orang lain.
                <span class="break-all" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; word-break: break-all;">
                    
                </span>
            </p>

        </td>
    </tr>
</table>
@endsection