@extends('layouts.front')

@section('title')
{{ $infografis->title }}
@endsection

@section('header')
@include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>{{ $infografis->title }}</h1>
                <ul class="breadcrumb">
                    <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="#">Informasi Publik</a></li>
                    <li><a href="/berita">Infografis</a></li>
                    <li class="active">Detail Infografis</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="blog-area single full-blog right-sidebar full-blog default-padding">
    <div class="container">
        <div class="blog-items">
            <div class="row justify-content-center ">
                <div class="blog-content col-lg-8 col-md-12">
                    <div class="item">
                        <div class="blog-item-box">
                            <!-- Start Post Thumb -->
                            <div class="thumb">
                                <div class="banner">
                                    <center>
                                        <img src="{{ asset('images/infografis/' . $infografis->image) }}" alt="Thumb" class="img-fluid justify-content-center">
                                    </center>
                                    <div class="date">
                                        @php
                                            $tanggal = \Carbon\Carbon::parse($infografis->created_at);
                                            $waktu = $tanggal->format('H:i:s');
                                        @endphp
                                        <span>
                                            {{ $tanggal->isoFormat('D MMMM Y') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- Start Post Thumb -->

                            <div class="info">
                                <div class="meta mt-3">
                                    <ul>
                                        <li>
                                            <i class="fas fa-history"></i>
                                        </li>
                                        <li>
                                            @php
                                                $tanggal = \Carbon\Carbon::parse($infografis->created_at);
                                                $waktu = $tanggal->format('H:i:s');
                                            @endphp
                                            <span>
                                                {{ $tanggal->isoFormat('D MMMM Y') }}
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fas fa-eye"></i><span> {{ views($infografis)->count() }}</span>
                                        </li>
                                        <li>
                                            <i class="fas fa-user"></i><span> {{ $infografis->user->name }}</span>
                                        </li>
                                    </ul>
                                </div>
                                
                                <h4>{{ $infografis->title }}</h4>
                                {!! $infografis->content !!}

                                <!-- Start Post Pagination -->
                                {{-- <div class="post-pagi-area">
                                    <a href="#"><i class="fas fa-angle-double-left"></i> Previus Post</a>
                                    <a href="#">Next Post <i class="fas fa-angle-double-right"></i></a>
                                </div> --}}
                                <!-- End Post Pagination -->
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection