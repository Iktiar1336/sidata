<!-- Header 
    ============================================= -->
<header id="home">

    <!-- Start Navigation -->
    <nav class="navbar navbar-default inc-top-bar attr-border dark navbar-fixed no-background bootsnav">

        <div class="container">

            <!-- Start Atribute Navigation -->
            <div class="attr-nav">
                <ul>
                    @guest
                    @if (Route::has('login'))
                    <li>
                        <a class="btn circle btn-md btn-gradient" href="{{ route('login') }}" style="font-size: 14px !important;padding-left: 40px !important;padding-right: 40px !important;">Sign
                            In</a>
                    </li>
                    @endif
                    @endguest
                </ul>
            </div>
            <!-- End Atribute Navigation -->

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="assets/img/logo-sidata.png" class="logo" alt="Logo" height="50px">
                </a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <li>
                        <a href="/" class="{{ Route::is('welcome') ? 'active' : '' }}">Home</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle {{ Route::is('visi-misi') ? 'active' : '' }} {{ Route::is('tugas-fungsi') ? 'active' : '' }} {{ Route::is('struktur-organisasi') ? 'active' : '' }}" data-toggle="dropdown">Profil</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('visi-misi') }}">Visi & Misi</a>
                            </li>
                            <li>
                                <a href="{{ route('tugas-fungsi') }}">Tugas & Fungsi</a>
                            </li>
                            <li>
                                <a href="{{ route('struktur-organisasi') }}">Struktur Organisasi</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle {{ Route::is('infografis') ? 'active' : '' }} {{ Route::is('news') ? 'active' : '' }}" data-toggle="dropdown">Informasi Publik</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('infografis') }}">Infografis</a>
                            </li>
                            <li>
                                <a href="{{ route('news') }}">Berita</a>
                            </li>
                            {{-- <li>
                                <a href="#">Arsip Publik</a>
                            </li> --}}
                        </ul>
                    </li>
                    <li>
                        <a href=" {{ route('unit-kerja') }}">Unit Kerja</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown">Capaian</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('capaian-spbe') }}">Capaian SPBE</a>
                            </li>
                            <li>
                                <a href="{{ route('capaian-kinerja') }}">Capaian Kinerja</a>
                            </li>
                            <li>
                                <a href="{{ route('capaian-lainnya') }}">Capaian Lainnya</a>
                            </li>
                        </ul>
                    </li>
                    @guest
                    @if (Route::has('login'))
                    <li>
                        <a href="{{ route('login') }}" style="visibility: hidden" id="nav-login">Sign
                            In</a>
                    </li>
                    @endif
                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown">Hi
                            {{ auth()->user()->name }}</a>
                        <ul class="dropdown-menu">
                            <li>
                                @if (auth()->user()->roles->first()->name == 'admin' ||
                                auth()->user()->roles->first()->name == 'super-admin')
                                <a href="{{ route('dashboard.admin') }}">Dashboard</a>
                                @else
                                <a href="{{ route('home') }}">Dashboard</a>
                                @endif
                            </li>
                            <li>
                                <a class="text-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">Logout</a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endguest
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>

    </nav>
    <!-- End Navigation -->

</header>
<!-- End Header -->