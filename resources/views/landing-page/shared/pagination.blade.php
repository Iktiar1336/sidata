@if ($paginator->hasPages())
<nav aria-label="navigation">
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            <li class="page-item"><a href="" class="page-link disabled"><i class="fas fa-angle-double-left"></i></a></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="fas fa-angle-double-left"></i></li>
        @endif
        @foreach ($elements as $element)
           
            @if (is_string($element))
                <li class="page-item active"><a href="" class="page-link disabled">{{ $element }}</a></li>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><a href="#" class="page-link disabled">{{ $page }}</a></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        @if ($paginator->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="fas fa-angle-double-right"></i></a></li>
        @else
            <li class="page-item"><a href="" class="page-link disabled"><i class="fas fa-angle-double-right"></i></a></li>
        @endif
    </ul>
</nav>
@endif 