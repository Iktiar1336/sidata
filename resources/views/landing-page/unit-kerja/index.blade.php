@extends('layouts.front')

@section('title')
Data Unit Kerja
@endsection

@section('css')
<style>
    .btn-eye {
        width: 50px;
        height: 50px;
        border-radius: 100px !important;
    }

    .card {
        border: 0px !important;
        background: transparent !important;
    }

    .card-body {
        padding-left: 0px !important;
    }
</style>
@endsection

@section('header')
@include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>Data Unit Kerja</h1>
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
                    <li class="active">Unit Kera</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="about-area mt-5">
    <div class="container">
        <div class="table-responsive mt-4 pr-3 pt-3">
            <table id="example" class="table table-hover" style="border: 0px !important">
                <thead>
                    <tr>
                        <th>Nama Unit Kerja</th>
                        <th>Jumlah Kategori Data</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($unitkerja as $item)
                    <tr>
                        <td>
                            <div class="card mb-3">
                                <div class="row no-gutters">
                                    <div class="col-md-2">
                                        <img src="{{ asset('admin/stisla/assets/img/bg-unit-kerja.png') }}"
                                            alt="Unit Kerja" class="img-fluid">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <a href="{{ route('detail-unit-kerja', Crypt::encrypt($item->id)) }}" class="card-title"><b>{{ $item->name }}</b></a>
                                            <p class="card-text"><small class="text-muted"><i class="fas fa-eye"></i>
                                                    {{ views($item)->count(); }}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- {{ $item->name }} --}}
                        </td>
                        <td>
                            <span class="badge badge-success" style="font-size: 14px">
                                @php
                                    $count = 0;
                                @endphp
                                @foreach ($item->category as $kategori)
                                    @php
                                        $kategori->kinerja->count() > 0 ? $count++ : '';
                                    @endphp
                                @endforeach

                                {{ $count }}
                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/js/dataTable.min.js') }}"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function () {
    $('#example').DataTable();
  });
</script>
@endsection