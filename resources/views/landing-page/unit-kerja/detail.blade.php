@extends('layouts.front')

@section('title')
Data Unit Kerja
@endsection

@section('css')
    <style>
        .content-chart {
            max-height: 600px;
            overflow-y: auto;
        }
    </style>
@endsection

@section('header')
    @include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h3>Data Unit Kerja {{ $unitkerja->name  }}</h3>
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
                    <li class="active">Unit Kera</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="about-area mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Pilih Kategori :</label>
                                <select class="form-control" id="category">
                                    <option disabled selected>Pilih Kategori</option>
                                    @forelse ($collectioncategory as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @empty
                                        <option value="" disabled>Tidak ada data</option>
                                    @endforelse
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="sub-category">Pilih Sub Kategori</label>
                                <select class="form-control" id="subcategory" disabled>

                                </select>
                              </div>
                              <div id="form-subcategory">
                                
                              </div>
                              <div class="form-group">
                                <label for="sub-category">Tahun</label>
                                <select class="form-control" id="tahun" disabled>
                                    <option value="" disabled>Tidak ada data</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="sub-category">S.d</label>
                                <select class="form-control" id="sd-tahun" disabled>
                                    <option value="" disabled>Tidak ada data</option>
                                </select>
                                <small id="emailHelp" class="form-text text-danger">
                                    *Maksimal 5 tahun dari tahun awal
                                </small>
                              </div>
                              <div class="form-group">
                                <label for="">Jenis Grafik</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="mychart" id="column" value="column" checked onclick="chartfunc()">
                                    <label class="form-check-label" for="column">
                                      Column
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="mychart" id="bar" value="bar" onclick="chartfunc()">
                                    <label class="form-check-label" for="bar">
                                      Bar
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="mychart" id="areaspline" value="areaspline" onclick="chartfunc()" >
                                    <label class="form-check-label" for="areaspline">
                                        Area Spline
                                    </label>
                                  </div>
                              </div>
                              <div class="form-group" id="btn-reset">

                              </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body content-chart">
                        <div id="chart"></div>
                        <div id="table"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script>
        $(document).ready(function() {
            var subcategory = $('#subcategory');
            $('#category').on('change', function() {
                var id = $(this).val();
                $.ajax({
                    url: "{{ route('getsubCategory') }}",
                    type: 'POST',
                    data: {
                        category_id: id,
                        _token: "{{ csrf_token() }}"
                    },
                    dataType: 'json',
                    success: function(data) {
                        subcategory.prop('disabled', false);
                        subcategory.empty();
                        subcategory.append('<option value="" disabled selected>Pilih Sub Kategori</option>');
                        $.each(data.subcategories, function(key, value) {
                            subcategory.append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                });
            });
        });

        $('#subcategory').on('change', function() {
            var id = $(this).val();
            $('#tahun').empty();
            $('#sd-tahun').empty();
            $('#tahun').append('<option value="" disabled selected>Mohon Menunggu</option>');
            $('#sd-tahun').append('<option value="" disabled selected>Mohon Menunggu</option>');
            $.ajax({
                url: "{{ route('getYear') }}",
                type: 'POST',
                data: {
                    subcategory_id: id,
                    category_id: $('#category').val(),
                    _token: "{{ csrf_token() }}"
                },
                dataType: 'json',
                success: function(data) {
                    $('#tahun').empty();
                    $('#sd-tahun').empty();
                    $('#tahun').append('<option value="" disabled selected>Pilih Tahun</option>');
                    $('#sd-tahun').append('<option value="" disabled selected>Pilih Tahun</option>');
                    $.each(data.year, function(key, value) {
                        $('#tahun').prop('disabled', false);
                        $('#tahun').append('<option value="'+key+'">'+key+'</option>');
                        $('#sd-tahun').append('<option value="'+key+'">'+key+'</option>');
                    });
                }
            });
        });

        $('#tahun').on('change', function() {
            $('#sd-tahun').val('');
            $('#sd-tahun').prop('disabled', false);
        })

        $('#sd-tahun').on('change', function() {
            var tahun = $('#tahun').val();
            var sd_tahun = $(this).val();
            var selisih = sd_tahun - tahun;
            if(selisih > 4) {
                swal("Tahun akhir tidak boleh lebih dari 5 tahun dari tahun awal", {
                    icon: "error",
                    title: "Oops! Terjadi Kesalahan",
                });
                $('#sd-tahun').val('');
            } else {
                if(sd_tahun < tahun) {
                    swal("Tahun Akhir Tidak Boleh Kurang Dari Tahun Awal", {
                        icon: "error",
                        title: "Oops! Terjadi Kesalahan",
                    });
                    $('#sd-tahun').val('');
                } else if(sd_tahun == '') {
                    alert('Tahun tidak boleh lebih kosong');
                    
                } else {
                    var category = $('#category').val();
                    var subcategory = $('#subcategory').val();
                    var workunitid = '{{ $unitkerja->id }}'
                    $.ajax({
                        url: "{{ route('getKinerja') }}",
                        type: 'GET',
                        data: {
                            start_date: tahun,
                            end_date: sd_tahun,
                            category: category,
                            subcategory: subcategory,
                            workunitid : workunitid,
                            _token: "{{ csrf_token() }}"
                        },
                        dataType: 'json',
                        success: function(data) {
                            $('#btn-reset').html('<button type="button" class="btn btn-danger" onclick="resetbtn()">Reset</button>');
                            var chart = Highcharts.chart('chart', {
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: 'Grafik ' + data.category
                                },
                                subtitle: { 
                                    text: data.subcategory + ' | Sumber : ' + data.sumber 
                                },
                                xAxis: {
                                    categories: data.categories,
                                    crosshair: true
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'Jumlah Kinerja'
                                    }
                                },
                                credits: {
                                    enabled: false
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
                                        '<td style="padding:0"><b> {point.y:1f} </b></td></tr>',
                                    footerFormat: '</table>',
                                    shared: true,
                                    useHTML: true
                                },
                                plotOptions: {
                                    column: {
                                        pointPadding: 0.2,
                                        borderWidth: 0
                                    }
                                },
                            });
                            $.each(data.series, function (key, value){
                                chart.addSeries({
                                name: key,
                                data: value
                                });
                            });


                            $('#table').html(data.table);
                            $('[data-toggle="popover"]').popover();   
                            chartfunc = function () {
                                var chartType = $('input[name=mychart]:checked').val();
                                if (chartType == 'column') {
                                    chart.update({
                                        chart: {
                                            type: 'column'
                                        }
                                    });
                                } else if (chartType == 'bar') {
                                    chart.update({
                                        chart: {
                                            type: 'bar'
                                        }
                                    });
                                } else if (chartType == 'areaspline') {
                                    chart.update({
                                        chart: {
                                            type: 'areaspline'
                                        },
                                        plotOptions: {
                                            areaspline: {
                                                fillOpacity: 0.5
                                            }
                                        },
                                    });
                                }
                            }
                        }
                    });
                }
            }
        });

        function resetbtn() {
            $('#category').val('');
            $('#subcategory').prop('disabled', true);
            $('#subcategory').val('');
            $('#tahun').prop('disabled', true);
            $('#tahun').val('');
            $('#sd-tahun').prop('disabled', true);
            $('#sd-tahun').val('');
            $('#chart').html('');
            $('#table').empty();
        }
    </script>
@endsection