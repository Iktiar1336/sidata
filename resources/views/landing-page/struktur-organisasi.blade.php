@extends('layouts.front')

@section('title')
    Struktur Organisasi
@endsection

@section('header')
    @include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>Struktur Organisasi</h1>
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="#">Profil</a></li>
                    <li class="active">Struktur Organisasi</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="about-area mt-5"> 
    <div class="container">
        <div class="row">
            @forelse ($strukturorganisasi as $item)
                <div class="col-lg-12">
                    <img src="{{ asset('/uploads/strukturorganisasi/'.$item->file) }}" alt=""
                        class="img-fluid">
                </div>
            @empty
                <div class="col-lg-12">
                    <h2 class="text-center text-danger font-weight-bold">Belum ada data</h2>
                </div>
            @endforelse
        </div>
    </div>
</div>
@endsection