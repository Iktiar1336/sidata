@extends('layouts.front')

@section('title')
Visi & Misi
@endsection

@section('header')
@include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>Visi & Misi</h1>
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="#">Profil</a></li>
                    <li class="active">Visi & Misi</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="about-area mt-5">
    <div class="container">
        <div class="row">
            @forelse ($visimisi as $item)
            <div class="col-lg-12">
                <div class="media">
                    <div class="media-body">
                        <h2 class="font-weight-bold">Visi</h2>
                        {!! $item->visi !!}
                        <hr>
                        <h2 class="font-weight-bold">Misi</h2>
                        <div class="misi" style="margin-left: 30px">
                            {!! $item->misi !!}
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <div class="col-lg-12">
                <h2 class="text-center text-danger font-weight-bold">Belum ada data</h2>
            </div>
            @endforelse
        </div>
    </div>
</div>
@endsection