@extends('layouts.front')

@section('title')
Capaian Kinerja
@endsection

@section('css')
<style>
    .btn-eye {
        width: 50px;
        height: 50px;
        border-radius: 100px !important;
    }

    .card {
        border: 0px !important;
        background: transparent !important;
    }

    .card-body {
        padding-left: 0px !important;
    }
</style>
@endsection

@section('header')
@include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>Capaian Kinerja</h1>
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="#"><i class="fas fa-home"></i> Capaian</a></li>
                    <li class="active">Capaian Kinerja</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="about-area mt-5">
    <div class="container">
        <div class="table-responsive mt-4 pr-3 pt-3">
            <table id="example" class="table table-hover" style="border: 0px !important">
                <thead>
                    <tr>
                        <th>Sasaran Strategis</th>
                        <th>Tahun</th>
                        <th>Target</th>
                        <th>Realisasi</th>
                        <th>Persentase</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($capaiankinerja as $item)
                    <tr>
                        <td>{!! $item->sasaranstrategis->sasaran_strategis !!}</td>
                        <td>{{ $item->tahun }}</td>
                        <td>
                            @php
                                if($item->sasaranstrategis->targetkinerja->type == 'nilai'){
                                    echo $item->sasaranstrategis->targetkinerja->target;
                                }else{
                                    if($item->sasaranstrategis->targetkinerja->target >= 90 && $item->sasaranstrategis->targetkinerja->target <= 100){
                                            echo '<span class="badge badge-success p-2">AA</span>';
                                        }elseif($item->sasaranstrategis->targetkinerja->target >= 80 && $item->sasaranstrategis->targetkinerja->target <= 89){
                                            echo '<span class="badge badge-success p-2">A</span>';
                                        }elseif($item->sasaranstrategis->targetkinerja->target >= 70 && $item->sasaranstrategis->targetkinerja->target <= 79){
                                            echo '<span class="badge badge-success p-2">BB</span>';
                                        }elseif($item->sasaranstrategis->targetkinerja->target >= 60 && $item->sasaranstrategis->targetkinerja->target <= 69){
                                            echo '<span class="badge badge-warning p-2">B</span>';
                                        }elseif($item->sasaranstrategis->targetkinerja->target >= 50 && $item->sasaranstrategis->targetkinerja->target <= 59){
                                            echo '<span class="badge badge-warning p-2">CC</span>';
                                        }elseif($item->sasaranstrategis->targetkinerja->target >= 30 && $item->sasaranstrategis->targetkinerja->target <= 49){
                                            echo '<span class="badge badge-danger p-2">C</span>';
                                        }elseif($item->sasaranstrategis->targetkinerja->target >= 0 && $item->sasaranstrategis->targetkinerja->target <= 29){
                                            echo '<span class="badge badge-danger p-2">D</span>';
                                        }
                                }
                            @endphp
                        </td>
                        <td>
                            @php
                                if($item->sasaranstrategis->targetkinerja->type == 'nilai'){
                                    echo $item->realisasi;
                                }else{
                                    if($item->realisasi >= 90 && $item->realisasi <= 100){
                                        echo '<span class="badge badge-success p-2">AA</span>';
                                    }elseif($item->realisasi >= 80 && $item->realisasi <= 89){
                                        echo '<span class="badge badge-success p-2">A</span>';
                                    }elseif($item->realisasi >= 70 && $item->realisasi <= 79){
                                        echo '<span class="badge badge-success p-2">BB</span>';
                                    }elseif($item->realisasi >= 60 && $item->realisasi <= 69){
                                        echo '<span class="badge badge-warning p-2">B</span>';
                                    }elseif($item->realisasi >= 50 && $item->realisasi <= 59){
                                        echo '<span class="badge badge-warning p-2">CC</span>';
                                    }elseif($item->realisasi >= 30 && $item->realisasi <= 49){
                                        echo '<span class="badge badge-danger p-2">C</span>';
                                    }elseif($item->realisasi >= 0 && $item->realisasi <= 29){
                                        echo '<span class="badge badge-danger p-2">D</span>';
                                    }
                                }
                            @endphp
                        </td>
                        <td>
                            @php
                                if($item->sasaranstrategis->targetkinerja->target >= $item->realisasi){
                                    echo '<span class="badge badge-danger p-2"><i class="fas fa-arrow-down"></i> '.$item->persentase.'</span>';
                                }else{
                                    echo '<span class="badge badge-success p-2"><i class="fas fa-arrow-up"></i> '.$item->persentase.'</span>';
                                }
                            @endphp
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/js/dataTable.min.js') }}"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function () {
    $('#example').DataTable();
  });
</script>
@endsection