@extends('layouts.front')

@section('title')
{{ $capaianspbe->judul }}
@endsection

@section('css')
    <style>
        .highcharts-figure{
            min-width: 310px;
            max-width: 700px;
            margin: 1em auto;
        }
    </style>
@endsection

@section('header')
@include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>{{ $capaianspbe->judul }} Tahun {{ $capaianspbe->tahun }}</h1>
                <ul class="breadcrumb">
                    <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="#">Capaian</a></li>
                    <li><a href="/berita">Capaian SPBE</a></li>
                    <li class="active">Detail Capaian SPBE</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="blog-area single full-blog right-sidebar full-blog default-padding mb-5">
    <div class="container">
        <div class="blog-items">
            <div class="row">
                <div class="blog-content col-lg-12 col-md-12 mb-5">
                    <div class="item">

                        <div class="blog-item-box">
                            <!-- Start Post Thumb -->
                            {{-- <div class="thumb">
                                <div class="banner">
                                    <img src="{{ asset('images/news/' . $capaianspbe->image) }}" alt="Thumb" class="img-fluid">
                            <div class="date">
                                @php
                                $tanggal = \Carbon\Carbon::parse($capaianspbe->created_at);
                                $waktu = $tanggal->format('H:i:s');
                                @endphp
                                <span>
                                    {{ $tanggal->isoFormat('D MMMM Y') }}
                                </span>
                            </div>
                        </div>
                    </div> --}}
                    <!-- Start Post Thumb -->

                    <div class="info mt-4">
                        <div class="meta">
                            <ul>
                                <li>
                                    <i class="fas fa-history"></i>
                                </li>
                                <li>
                                    @php
                                    $tanggal = \Carbon\Carbon::parse($capaianspbe->created_at);
                                    $waktu = $tanggal->format('H:i:s');
                                    @endphp
                                    <span>
                                        {{ $tanggal->isoFormat('D MMMM Y') }}
                                    </span>
                                </li>
                                <li>
                                    <i class="fas fa-eye"></i><span> {{ views($capaianspbe)->count() }}</span>
                                </li>
                            </ul>
                        </div>

                        <h4 class="text-center">{{ $capaianspbe->judul }} Tahun {{ $capaianspbe->tahun }}</h4>

                        <figure class="highcharts-figure">
                            <div id="container"></div>
                        </figure>

                        {!! $capaianspbe->deskripsi !!}

                        <!-- Start Post Pagination -->
                        <div class="post-pagi-area">
                            <a href="{{ route('capaian-spbe') }}" style="float: left"><i class="fas fa-angle-double-left"></i> Kembali</a>

                        </div>
                        <!-- End Post Pagination -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection

@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
    Highcharts.chart('container', {

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            height: '80%'
        },

        title: {
            text: 'Nilai Indeks SPBE',
            
        },

        subtitle: {
            text: 'Sumber : Kementerian Pendayagunaan Aparatur Negara dan Reformasi Birokrasi'
        },

        credits: {
            enabled: false
        },

        pane: {
            startAngle: -90,
            endAngle: 89.9,
            background: null,
            center: ['50%', '75%'],
            size: '110%'
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 5.0,
            tickPixelInterval: 72,
            tickPosition: 'inside',
            tickColor: Highcharts.defaultOptions.chart.backgroundColor || '#FFFFFF',
            tickLength: 20,
            tickWidth: 2,
            minorTickInterval: null,
            labels: {
                distance: 20,
                style: {
                    fontSize: '14px'
                }
            },
            plotBands: [{
                from: 0,
                to: 1.8,
                color: '#E74C3C',
                thickness: 20
            },{
                from: 1.8,
                to: 2.6,
                color: '#FCF3CF',
                thickness: 20
            }, {
                from: 2.6,
                to: 3.5,
                color: '#D1F2EB',
                thickness: 20
            }, {
                from: 3.5,
                to: 4.2,
                color: '#A3E4D7',
                thickness: 20
            },{
                from: 4.2,
                to: 5.0,
                color: '#58D68D',
                thickness: 20
            }]
        },

        series: [{
            name: 'Indeks',
            data: [{{ $capaianspbe->indeks }}],
            tooltip: {
                valueSuffix: ' {{ $capaianspbe->predikat }}'
            },
            dataLabels: {
                format: '{y} {{ $capaianspbe->predikat }}',
                borderWidth: 0,
                color: (
                    Highcharts.defaultOptions.title &&
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || '#333333',
                style: {
                    fontSize: '16px'
                }
            },
            dial: {
                radius: '80%',
                backgroundColor: 'gray',
                baseWidth: 12,
                baseLength: '0%',
                rearLength: '0%'
            },
            pivot: {
                backgroundColor: 'gray',
                radius: 6
            }

        }]

    });
</script>

@endsection