@extends('layouts.front')

@section('title')
{{ $capaian->judul }}
@endsection

@section('header')
@include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>{{ $capaian->judul }}</h1>
                <ul class="breadcrumb">
                    <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="#">Capaian</a></li>
                    <li><a href="/berita">Capaian Lainnya</a></li>
                    <li class="active">Detail Capaian</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="blog-area single full-blog right-sidebar full-blog default-padding mb-5">
    <div class="container">
        <div class="blog-items">
            <div class="row">
                <div class="blog-content col-lg-12 col-md-12 mb-5">
                    <div class="item">

                        <div class="blog-item-box">
                            <!-- Start Post Thumb -->
                            {{-- <div class="thumb">
                                <div class="banner">
                                    <img src="{{ asset('images/news/' . $capaian->image) }}" alt="Thumb" class="img-fluid">
                                    <div class="date">
                                        @php
                                            $tanggal = \Carbon\Carbon::parse($capaian->created_at);
                                            $waktu = $tanggal->format('H:i:s');
                                        @endphp
                                        <span>
                                            {{ $tanggal->isoFormat('D MMMM Y') }}
                                        </span>
                                    </div>
                                </div>
                            </div> --}}
                            <!-- Start Post Thumb -->

                            <div class="info mt-4">
                                <div class="meta">
                                    <ul>
                                        <li>
                                            <i class="fas fa-history"></i>
                                        </li>
                                        <li>
                                            @php
                                                $tanggal = \Carbon\Carbon::parse($capaian->created_at);
                                                $waktu = $tanggal->format('H:i:s');
                                            @endphp
                                            <span>
                                                {{ $tanggal->isoFormat('D MMMM Y') }}
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fas fa-eye"></i><span> {{ views($capaian)->count() }}</span>
                                        </li>
                                    </ul>
                                </div>
                                
                                <h4>{{ $capaian->judul }}</h4>
                                {!! $capaian->deskripsi !!}

                                <!-- Start Post Pagination -->
                                <div class="post-pagi-area">
                                    <a href="{{ route('capaian-lainnya') }}" style="float: left"><i class="fas fa-angle-double-left"></i> Kembali</a>
                                    
                                </div>
                                <!-- End Post Pagination -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection