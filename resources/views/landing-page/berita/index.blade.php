@extends('layouts.front')

@section('title')
Berita
@endsection

@section('css')
<style>
    .search-form {
        left: 200px;
        top: 200px;
        float: right;
        margin-bottom: 30px;
    }

    .search-field {
        background-color: transparent;
        background-image: url(https://wp-themes.com/wp-content/themes/twentythirteen/images/search-icon.png);
        background-position: 5px center;
        background-repeat: no-repeat;
        background-size: 24px 24px;
        border: none;
        cursor: pointer;
        height: 50px;
        margin: 3px 0;
        padding: 0 0 0 34px;
        position: relative;
        -webkit-transition: width 400ms ease, background 400ms ease;
        transition: width 400ms ease, background 400ms ease;
        width: 0px;
        cursor: pointer;
    }

    .search-field:focus {
        background-color: #fff;
        border: 2px solid #c3c0ab;
        cursor: text;
        outline: 0;
        width: 350px;
        color: #000;
        border-radius: 50px;
    }

    .search-form .search-submit {
        display: none;
    }
</style>
@endsection

@section('header')
@include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>Berita</h1>
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="#">Informasi Publik</a></li>
                    <li class="active">Berita</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding">
    <div class="container">
        <div class="blog-items">
            <div class="blog-content">
                <div class="blog-item-box">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <form role="search" method="GET" class="search-form" action="{{ route('news') }}">
                                <label>
                                    <input type="search" class="search-field" placeholder="Search …" value="" name="search" title="Search for:" />
                                </label>
                                <input type="submit" class="search-submit" value="search" />
                            </form>
                        </div>
                        @forelse ($berita as $item)
                        <!-- Single Item -->
                        <div class="col-lg-4 col-md-6 single-item">
                            <div class="item">
                                <div class="thumb">
                                    <div>
                                        <img src="{{ asset('images/news/' . $item->image) }}" alt="Thumb" width="100%"
                                            height="250px">
                                    </div>
                                </div>
                                <div class="info">
                                    <div class="meta">
                                        <ul>
                                            <li>
                                                <i class="fas fa-history"></i>
                                            </li>
                                            <li>
                                                @php
                                                    $tanggal = \Carbon\Carbon::parse($item->created_at);
                                                    $waktu = $tanggal->format('H:i:s');
                                                @endphp
                                                {{ $tanggal->isoFormat('D MMMM Y') }}
                                            </li>
                                            <li>
                                                <i class="fas fa-eye"></i> {{ views($item)->count() }}
                                            </li>
                                        </ul>
                                    </div>
                                    <h4>
                                        <a href="{{ route('detail-news', $item->slug) }}">{{ Illuminate\Support\Str::limit($item->title, 20) }}</a>
                                    </h4>
                                    <p>
                                        @php
                                            $item->content = strip_tags($item->content);
                                            echo Illuminate\Support\Str::limit($item->content, 100);
                                        @endphp
                                    </p>
                                    <a class="btn-simple" href="{{ route('detail-news', $item->slug) }}"><i class="fas fa-angle-right"></i> Lihat Detail</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Item -->
                        @empty
                        <div class="col-lg-12 col-md-12">
                            <h2 class="text-danger text-center font-weight-bold">
                                <i class="fas fa-exclamation-triangle"></i> Data Tidak Ditemukan
                            </h2>
                        </div>
                        @endforelse
                    </div>

                    <!-- Pagination -->
                    <div class="row">
                        <div class="col-md-12 pagi-area text-center">
                            {!! $berita->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection