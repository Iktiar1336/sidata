@extends('layouts.front')

@section('title')
{{ $berita->title }}
@endsection

@section('header')
@include('landing-page.shared.navbar-sticky')
@endsection

@section('content')
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>{{ $berita->title }}</h1>
                <ul class="breadcrumb">
                    <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="#">Informasi Publik</a></li>
                    <li><a href="/berita">Berita</a></li>
                    <li class="active">Detail Berita</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="blog-area single full-blog right-sidebar full-blog default-padding">
    <div class="container">
        <div class="blog-items">
            <div class="row">
                <div class="blog-content col-lg-12 col-md-12">
                    <div class="item">

                        <div class="blog-item-box">
                            <!-- Start Post Thumb -->
                            <div class="thumb">
                                <div class="banner">
                                    <img src="{{ asset('images/news/' . $berita->image) }}" alt="Thumb" class="img-fluid">
                                    <div class="date">
                                        @php
                                            $tanggal = \Carbon\Carbon::parse($berita->created_at);
                                            $waktu = $tanggal->format('H:i:s');
                                        @endphp
                                        <span>
                                            {{ $tanggal->isoFormat('D MMMM Y') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- Start Post Thumb -->

                            <div class="info mt-4">
                                <div class="meta">
                                    <ul>
                                        <li>
                                            <i class="fas fa-history"></i>
                                        </li>
                                        <li>
                                            @php
                                                $tanggal = \Carbon\Carbon::parse($berita->created_at);
                                                $waktu = $tanggal->format('H:i:s');
                                            @endphp
                                            <span>
                                                {{ $tanggal->isoFormat('D MMMM Y') }}
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fas fa-eye"></i><span> {{ views($berita)->count() }}</span>
                                        </li>
                                        <li>
                                            <i class="fas fa-user"></i><span> {{ $berita->user->name }}</span>
                                        </li>
                                    </ul>
                                </div>
                                
                                <h4>{{ $berita->title }}</h4>
                                {!! $berita->content !!}

                                <!-- Start Post Pagination -->
                                {{-- <div class="post-pagi-area">
                                    <a href="#"><i class="fas fa-angle-double-left"></i> Previus Post</a>
                                    <a href="#">Next Post <i class="fas fa-angle-double-right"></i></a>
                                </div> --}}
                                <!-- End Post Pagination -->
                            </div>
                        </div>
                    </div>

                    {{-- <!-- Start Post Tag s-->
                    <div class="post-tags share">
                        <div class="tags">
                            <a href="#">Analysis</a>
                            <a href="#">Process</a>
                        </div>
                        <div class="social">
                            <ul>
                                <li class="facebook">
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li class="twitter">
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="pinterest">
                                    <a href="#">
                                        <i class="fab fa-pinterest-p"></i>
                                    </a>
                                </li>
                                <li class="linkedin">
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Post Tags -->

                    <!-- Start Blog Comment -->
                    <div class="blog-comments">
                        <div class="comments-area">
                            <div class="comments-title">
                                <h4>
                                    5 comments
                                </h4>
                                <div class="comments-list">
                                    <div class="commen-item">
                                        <div class="avatar">
                                            <img src="assets/img/team/1.jpg" alt="Author">
                                        </div>
                                        <div class="content">
                                            <div class="title">
                                                <h5>Jonathom Doe</h5>
                                                <span>28 Aug, 2020</span>
                                            </div>
                                            <p>
                                                Delivered ye sportsmen zealously arranging frankness estimable as. Nay any article enabled musical shyness yet sixteen yet blushes. Entire its the did figure wonder off. 
                                            </p>
                                            <div class="comments-info">
                                                <a href="#"><i class="fa fa-reply"></i>Reply</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="commen-item reply">
                                        <div class="avatar">
                                            <img src="assets/img/team/2.jpg" alt="Author">
                                        </div>
                                        <div class="content">
                                            <div class="title">
                                                <h5>Spart Lee</h5>
                                                <span>17 Feb, 2020</span>
                                            </div>
                                            <p>
                                                Delivered ye sportsmen zealously arranging frankness estimable as. Nay any article enabled musical shyness yet sixteen yet blushes. Entire its the did figure wonder off. 
                                            </p>
                                            <div class="comments-info">
                                                <a href="#"><i class="fa fa-reply"></i>Reply</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comments-form">
                                <div class="title">
                                    <h4>Leave a comments</h4>
                                </div>
                                <form action="#" class="contact-comments">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <!-- Name -->
                                                <input name="name" class="form-control" placeholder="Name *" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <!-- Email -->
                                                <input name="email" class="form-control" placeholder="Email *" type="email">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group comments">
                                                <!-- Comment -->
                                                <textarea class="form-control" placeholder="Comment"></textarea>
                                            </div>
                                            <div class="form-group full-width submit">
                                                <button type="submit">
                                                    Post Comments
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End Comments Form --> --}}
                </div>
                <!-- Start Sidebar -->
                {{-- <div class="sidebar col-lg-4 col-md-12">
                    <aside>
                        <div class="sidebar-item search">
                            <div class="sidebar-info">
                                <form>
                                    <input type="text" name="text" class="form-control">
                                    <button type="submit"><i class="fas fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="sidebar-item recent-post">
                            <div class="title">
                                <h4>Recent Post</h4>
                            </div>
                            <ul>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/case/11.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Participate in staff meetingness manage dedicated</a>
                                        <div class="meta-title">
                                            <span class="post-date"><i class="fas fa-clock"></i> 12 Feb, 2020</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/case/22.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Future Plan & Strategy for Consutruction </a>
                                        <div class="meta-title">
                                            <span class="post-date"><i class="fas fa-clock"></i> 05 Jul, 2019</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/case/33.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Melancholy particular devonshire alteration</a>
                                        <div class="meta-title">
                                            <span class="post-date"><i class="fas fa-clock"></i> 29 Aug, 2020</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-item category">
                            <div class="title">
                                <h4>category list</h4>
                            </div>
                            <div class="sidebar-info">
                                <ul>
                                    <li>
                                        <a href="#">national <span>69</span></a>
                                    </li>
                                    <li>
                                        <a href="#">national <span>25</span></a>
                                    </li>
                                    <li>
                                        <a href="#">sports <span>18</span></a>
                                    </li>
                                    <li>
                                        <a href="#">megazine <span>37</span></a>
                                    </li>
                                    <li>
                                        <a href="#">health <span>12</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar-item gallery">
                            <div class="title">
                                <h4>Gallery</h4>
                            </div>
                            <div class="sidebar-info">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <img src="assets/img/case/66.jpg" alt="thumb">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/img/case/55.jpg" alt="thumb">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/img/case/44.jpg" alt="thumb">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/img/case/33.jpg" alt="thumb">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/img/case/22.jpg" alt="thumb">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/img/case/11.jpg" alt="thumb">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar-item archives">
                            <div class="title">
                                <h4>Archives</h4>
                            </div>
                            <div class="sidebar-info">
                                <ul>
                                    <li><a href="#">Aug 2020</a></li>
                                    <li><a href="#">Sept 2020</a></li>
                                    <li><a href="#">Nov 2020</a></li>
                                    <li><a href="#">Dec 2020</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar-item social-sidebar">
                            <div class="title">
                                <h4>follow us</h4>
                            </div>
                            <div class="sidebar-info">
                                <ul>
                                    <li class="facebook">
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li class="twitter">
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="pinterest">
                                        <a href="#">
                                            <i class="fab fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li class="g-plus">
                                        <a href="#">
                                            <i class="fab fa-google-plus-g"></i>
                                        </a>
                                    </li>
                                    <li class="linkedin">
                                        <a href="#">
                                            <i class="fab fa-linkedin-in"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar-item tags">
                            <div class="title">
                                <h4>tags</h4>
                            </div>
                            <div class="sidebar-info">
                                <ul>
                                    <li><a href="#">Fashion</a>
                                    </li>
                                    <li><a href="#">Education</a>
                                    </li>
                                    <li><a href="#">nation</a>
                                    </li>
                                    <li><a href="#">study</a>
                                    </li>
                                    <li><a href="#">health</a>
                                    </li>
                                    <li><a href="#">food</a>
                                    </li>
                                    <li><a href="#">travel</a>
                                    </li>
                                    <li><a href="#">science</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div> --}}
                <!-- End Start Sidebar -->
            </div>
        </div>
    </div>
</div>
@endsection