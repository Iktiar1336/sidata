@extends('layouts.dashboard')

@section('title')
Pertanyaan
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Pertanyaan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data</div>
            <div class="breadcrumb-item">Pertanyaan</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Pertanyaan</h2>
        <p class="section-lead">Halaman untuk mengelola data Pertanyaan.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Pertanyaan</h4>
                <div class="card-header-action">
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-target-kinerja">Tambah Pertanyaan</button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-kolom-pertanyaan">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Kategori</th>
                                <th>Nama</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('pertanyaan.store') }}" method="POST" class="modal-part" id="modal-target-kinerja-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="kategori_pertanyaan">Pilih Kategori Pertanyaan</label>
        <select class="form-control" name="kategori_pertanyaan_id" id="kategori_pertanyaan">
            <option value="" disabled selected>Pilih Kategori Pertanyaan</option>
            @foreach ($kategoriPertanyaan as $item)
                
                <option value="{{ $item->id }}">{{ $item->judul }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group d-none" id="kolompertanyaan">
        @php
            
        @endphp
        <label for="kolom_pertanyaan">Pilih Kolom Pertanyaan</label>
        <select class="form-control" name="kolom_pertanyaan_id" id="kolom_pertanyaan">
            <option value="" disabled selected>Pilih Kolom Pertanyaan</option>
        </select>
    </div>

    <div class="form-group" id="is_rowspan">
        <label for="rowspan">Rowspan</label>
        <select class="form-control" name="is_rowspan" id="rowspan">
            <option value="" disabled selected>Pilih Rowspan</option>
            <option value="1">Ya</option>
            <option value="0">Tidak</option>
        </select>
    </div>

    <div class="form-group" id="jumlah_rowspan">

    </div>

    <div class="form-group">
        <label for="type">Type Pertanyaan</label>
        <select class="selectpicker" name="type" id="type">
            <option value="" disabled selected>Pilih Type Pertanyaan</option>
            <option value="text">Text</option>
            <option value="input">Input</option>
        </select>
    </div>

    <div class="form-group" id="text_pertanyaan">

    </div>

    <div class="form-group" id="input_type">
        <label for="input_type">Input Type</label>
        <select class="selectpicker" name="input_type" id="input_type">
            <option value="" disabled selected>Pilih Input Type</option>
            <option value="text">Text</option>
            <option value="number">Number</option>
            <option value="file">File</option>
            <option value="textarea">Textarea</option>
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
            Simpan
        </button>
    </div>
</form>
@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('#is_rowspan').hide();
        $('.selecpicker').selectpicker();
        $('#input_type').hide();
        $('#text_pertanyaan').hide();

        $('#table-kolom-pertanyaan').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/pertanyaan/",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'kolom', name: 'kolom'},
                {data: 'type', name: 'type'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#kategori_pertanyaan').on('change', function() {
            var id = $(this).val();
            $.ajax({
                url: "{{ route('get-kolom-pertanyaan') }}",
                method: "POST",
                data: {
                    kategoripertanyaan_id: id,
                    _token: "{{ csrf_token() }}"
                },
                success: function(data) {
                    $('#kolom_pertanyaan').empty();
                    $('#is_rowspan').hide();
                    $('#jumlah_rowspan').empty();
                    $('#kolom_pertanyaan').append('<option value="" disabled selected>Pilih Kolom Pertanyaan</option>');
                    
                    $.each(data.kolomPertanyaan, function(key, value) {
                        
                        if(value.disabled == true) {
                            disabled = 'disabled';
                        } else {
                            disabled = '';
                        }

                        if(data.firstkolom.id == value.id){
                            $('#kolom_pertanyaan').append('<option value="'+value.id+'" class="first"'+disabled+'>'+value.nama_kolom+'</option>');
                        } else {
                            $('#kolom_pertanyaan').append('<option value="'+value.id+'"'+disabled+'>'+value.nama_kolom+'</option>');
                        }
                    });
                    $('#kolompertanyaan').removeClass('d-none');
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        });

        $('#kolom_pertanyaan').on('change', function() {
            var classname = $('select[name=kolom_pertanyaan_id] :selected').attr('class');
            if (classname == "first") {
                $('#is_rowspan').show();
                $('#jumlah_rowspan').empty();
            } else {
                $('#is_rowspan').hide();
                $('#jumlah_rowspan').empty();
            } 
        });

        $('#rowspan').on('change', function() {
            var is_rowspan = $(this).val();
            if (is_rowspan == 1) {
                $('#jumlah_rowspan').append('<label for="jumlah_rowspan">Jumlah Rowspan</label><input type="number" name="jumlah_rowspan" id="jumlah_rowspan" class="form-control">');
            } else {
                $('#jumlah_rowspan').empty();
            }
        });

        $('#type').on('change', function() {
            var type = $(this).val();
            if (type == "input") {
                $('#input_type').show();
                $('#text_pertanyaan').hide();
                $('#text_pertanyaan').empty();
            } else if (type == "text") {
                $('#input_type').hide();
                $('#text_pertanyaan').show();
                $('#text_pertanyaan').append('<label for="text_pertanyaan">Text Pertanyaan</label><input type="text" name="text_pertanyaan" id="text_pertanyaan" class="form-control">');
            } 
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Pertanyaan berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Pertanyaan berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Pertanyaan gagal dihapus! Karena sudah terikat dengan data lain.", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Pertanyaan sudah ada, silahkan pilih tahun yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Pertanyaan berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif


@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection