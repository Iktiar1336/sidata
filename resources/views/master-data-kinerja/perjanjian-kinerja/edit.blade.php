@extends('layouts.dashboard')

@section('title')
Edit Perjanjian Kinerja
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Perjanjian Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Edit Perjanjian Kinerja</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Perjanjian Kinerja Tahun : {{ $perjanjiankinerja->tahun }}</h2>
        <p class="section-lead">Halaman untuk mengelola data Perjanjian Kinerja.</p>

        <div class="card">
            <div class="card-header">
                <h4>Edit Perjanjian Kinerja Tahun : {{ $perjanjiankinerja->tahun }}</h4>
                <div class="card-header-action">
                    <a href="{{ route('perjanjian-kinerja.index') }}" class="btn btn-primary">Kembali</a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('perjanjian-kinerja.update', Crypt::encrypt($perjanjiankinerja->id)) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        @php
                            $tahun = date('Y');
                        @endphp
                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true">
                            <option value="" disabled selected>Pilih Tahun</option>
                            @for ($i = 0; $i <= 4; $i++)
                                <option value="{{ $tahun+$i }}" {{ $tahun+$i == $perjanjiankinerja->tahun ? 'selected' : '' }}>{{ $tahun+$i }}</option>
                            @endfor
                        </select>
                        
                        @error('tahun')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>    
                
                    <div class="form-group">
                        <label for="tujuan_id">Tujuan</label>
                        <select class="selecpicker @error('tujuan_id') is-invalid @enderror" name="tujuan_id[]" id="tujuan_id" data-show-subtext="true" data-live-search="true" multiple>
                            <option value="" disabled>Pilih Tujuan</option>
                            @foreach ($tujuan as $item)
                                @if(in_array($item->id, $tujuan_id))
                                    <option value="{{ $item->id }}" selected>{!! $item->tujuan !!}</option>
                                @else
                                    <option value="{{ $item->id }}">{!! $item->tujuan !!}</option>
                                @endif
                            @endforeach 
                        </select>
                        @error('tujuan_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <label for="sasaran_strategis">Sasaran Strategis</label>
                        <select class="selecpicker @error('sasaran_strategis_id') is-invalid @enderror" name="sasaran_strategis_id[]" id="sasaran_strategis" data-show-subtext="true" data-live-search="true" multiple>
                            <option value="" disabled>Pilih Sasaran Strategis</option>
                            @foreach ($sasaranstrategis as $item)
                                @if(in_array($item->id, $sasaran_strategis_id))
                                    <option value="{{ $item->id }}" selected>{!! $item->sasaran_strategis !!}</option>
                                @else
                                    <option value="{{ $item->id }}">{!! $item->sasaran_strategis !!}</option>
                                @endif
                            @endforeach 
                        </select>
                        @error('sasaran_strategis_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();
    });
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection