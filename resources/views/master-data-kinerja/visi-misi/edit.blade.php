@extends('layouts.dashboard')

@section('title')
    Update Visi & Misi
@endsection

@section('css')
    
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Update Visi & Misi</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
      <div class="breadcrumb-item">Update Visi & Misi</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Update Visi dan Misi</h2>

    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4>Update Visi dan Misi</h4>
          </div>
          <div class="card-body">
            <form action="{{ route('visi-misi.update', Crypt::encrypt($visimisi->id)) }}" method="POST">
              @csrf
              @method('PUT')
              <div class="form-group">
                  <label>Visi :</label>
                  <textarea name="visi" class="form-control @error('visi') is-invalid @enderror" required id="visi">
                    {!! $visimisi->visi !!}
                  </textarea>
                  @error('visi')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
              <div class="form-group">
                  <label>Misi :</label>
                  <textarea name="misi" class="form-control @error('misi') is-invalid @enderror" required id="misi">
                    {!! $visimisi->misi !!}
                  </textarea>
                  @error('misi')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-success btn-block">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js')
<script>
  var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
  };
  CKEDITOR.replace('visi', options);
  CKEDITOR.replace('misi', options);
</script>
@endsection