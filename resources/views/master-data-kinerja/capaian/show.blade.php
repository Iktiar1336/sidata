@extends('layouts.dashboard')

@section('title')
Detail Capaian
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Detail Capaian</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Detail Capaian</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Detail Capaian</h2>
        <p class="section-lead">Halaman untuk mengelola data Capaian.</p>

        <div class="card">
            <div class="card-header">
                <h4>Detail Capaian</h4>
                <div class="card-header-action">
                    <a href="{{ route('capaian.index') }}" class="btn btn-primary">Kembali</a>
                </div>
            </div>
            <div class="card-body">
                <div class="media">
                    <div class="media-body">
                      <h5 class="mt-0">Judul</h5>
                      {!! $capaian->judul !!}
                      <hr>
                      <h5 class="mt-0">Deskripsi</h5>
                      {!! $capaian->deskripsi !!}
                    </div>
                  </div>
            </div>
        </div>
    </div>
</section>

@endsection 