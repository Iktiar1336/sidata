@extends('layouts.dashboard')

@section('title')
Capaian
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Capaian</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Capaian</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Capaian</h2>
        <p class="section-lead">Halaman untuk mengelola data Capaian.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Capaian</h4>
                <div class="card-header-action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Tambah Capaian
                      </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table-capaian">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Judul</th>
                                <th>Tahun</th>
                                <th>Status</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Capaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('capaian.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Judul Capaian</label>
                        <textarea class="form-control @error('judul') is-invalid @enderror" name="judul" placeholder="Judul Capaian" id="judul"></textarea>
                
                        @error('judul')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        @php
                            $tahun = date('Y');
                        @endphp
                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required>
                            <option value="" disabled selected>Pilih Tahun</option>
                            @for ($i = 0; $i <= 4; $i++)
                                <option value="{{ $tahun+$i }}" >{{ $tahun+$i }}</option>
                            @endfor
                        </select>
                        
                        @error('tahun')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>    

                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" placeholder="Capaian" id="deskripsi"></textarea>
                
                        @error('deskripsi')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="">Status</label>
                        <div class="form-check">
                            <input class="form-check-input" value="1" type="radio" name="status" id="status1"
                                checked="">
                            <label class="form-check-label" for="status1">
                                Publish
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" value="0" type="radio" name="status" id="status2"
                                checked="">
                            <label class="form-check-label" for="status2">
                                Draft
                            </label>
                        </div>
                        @error('status')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        $('.selecpicker').selectpicker();
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
        };
        CKEDITOR.replace('deskripsi', options);

        $('#table-capaian').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/capaian",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'judul', name: 'judul'},
                {data: 'tahun', name: 'tahun'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Capaian berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Capaian berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Capaian Tidak Dapat Dihapus!, Karena Sudah Terikat Dengan Data Lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Capaian berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection