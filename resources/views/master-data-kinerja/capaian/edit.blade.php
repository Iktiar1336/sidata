@extends('layouts.dashboard')

@section('title')
Update Capaian
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Update Capaian</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Update Capaian</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Update Capaian</h2>
        <p class="section-lead">Halaman untuk mengelola data Capaian.</p>

        <div class="card">
            <div class="card-header">
                <h4>Update Capaian</h4>
                <div class="card-header-action">
                    <a href="{{ route('capaian.index') }}" class="btn btn-primary">Kembali</a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('capaian.update', Crypt::encrypt($capaian->id)) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="judul">Judul Capaian</label>
                        <textarea class="form-control @error('judul') is-invalid @enderror" name="judul" placeholder="Judul Capaian" id="judul">{!! $capaian->judul !!}
                        </textarea>
                
                        @error('judul')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        @php
                            $tahun = date('Y');
                        @endphp
                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required>
                            <option value="" disabled selected>Pilih Tahun</option>
                            @for ($i = 0; $i <= 4; $i++)
                                <option value="{{ $tahun+$i }}" {{ $capaian->tahun == $tahun+$i ? 'selected' : '' }}>{{ $tahun+$i }}</option>
                            @endfor
                        </select>
                        
                        @error('tahun')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>    

                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" placeholder="Capaian" id="indikatorkinerja">
                            {!! $capaian->deskripsi !!}
                        </textarea>
                
                        @error('deskripsi')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="status1" value="1"
                                {{ $capaian->status == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status1">Publish</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="status2" value="0"
                                {{ $capaian->status == 0 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status2">Draft </label>
                        </div>
                        
                        @error('status')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.selecpicker').selectpicker();

            var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
        };
        CKEDITOR.replace('deskripsi', options);
        });
    </script>
@endsection