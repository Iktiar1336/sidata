@extends('layouts.dashboard')

@section('title')
Alokasi Anggaran
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Alokasi Anggaran</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Alokasi Anggaran</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Alokasi Anggaran</h2>
        <p class="section-lead">Halaman untuk mengelola data Alokasi Anggaran.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Alokasi Anggaran</h4>
                <div class="card-header-action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Tambah Alokasi Anggaran
                    </button>
                    {{-- <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-capaian-kinerja"></button> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-capaiankinerja">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Sasaran Strategis</th>
                                <th>Alokasi</th>
                                <th>Realisasi</th>
                                <th>Tahun</th>
                                <th>Persentase</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Alokasi Anggaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('realisasi-anggaran.store') }}" method="POST" id="form">
                @csrf
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    @php
                        $tahun = date('Y');
                    @endphp
                    <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required>
                        <option value="" disabled selected>Pilih Tahun</option>
                        @for ($i = 0; $i <= 4; $i++)
                            <option value="{{ $tahun+$i }}" >{{ $tahun+$i }}</option>
                        @endfor
                    </select>
                    
                    @error('tahun')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>    
            
                <div class="form-group">
                    <label for="sasaran_strategis">Sasaran Strategis</label>
                    <select class="selecpicker @error('sasaran_strategis_id') is-invalid @enderror" name="sasaran_strategis_id" id="sasaran_strategis" data-show-subtext="true" data-live-search="true" required>
                        <option value="" selected disabled>Pilih Sasaran Strategis</option>
                        @foreach ($sasaranstrategis as $item)
                            <option value="{{ $item->id }}">{!! $item->sasaran_strategis !!} | Tahun : {{ $item->targetkinerja->tahun }} | Target : {{ $item->targetkinerja->target }}</option>
                        @endforeach 
                    </select>
                    @error('sasaran_strategis_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div> 
                    @enderror
                </div>

                <div class="form-group">
                    <label for="alokasi_anggaran">Alokasi</label>
                    <input type="number" min="0" class="form-control @error('alokasi_anggaran') is-invalid @enderror" id="alokasi_anggaran" name="alokasi_anggaran" placeholder="Alokasi Anggaran" required step="any">
                    <small class="text-danger">Masukkan angka tanpa titik atau koma</small>
                    @error('alokasi_anggaran')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            
                <div class="form-group">
                    <label for="realisasi">Realisasi</label>
                    <input type="number" min="0" class="form-control @error('realisasi') is-invalid @enderror" id="realisasi" name="realisasi" placeholder="Realisasi Kinerja" required step="any">
                    <small class="text-danger">Masukkan angka tanpa titik atau koma</small>
                    @error('realisasi')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        $('#form').submit(function(e) {
            e.preventDefault();
            $('#btn-submit-indikator-kinerja').html('Menyimpan...');
            $('#form').unbind('submit').submit();

        })

        $('#table-capaiankinerja').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/realisasi-anggaran",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'sasaranstrategis', name: 'sasaranstrategis'}, 
                {data: 'alokasi', name: 'alokasi'}, 
                {data: 'realisasi', name: 'realisasi'}, 
                {data: 'tahun', name: 'tahun'},
                {data: 'persentase', name: 'persentase'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Alokasi Anggaran berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Alokasi Anggaran sudah ada, silahkan pilih data yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Alokasi Anggaran berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('error-insert'))
<script>
    swal("Data yang anda masukkan tidak valid!, silahkan coba lagi", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif

@endsection