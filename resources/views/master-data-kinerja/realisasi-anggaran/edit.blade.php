@extends('layouts.dashboard')

@section('title')
Alokasi Anggaran
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Alokasi Anggaran</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Alokasi Anggaran</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Alokasi Anggaran</h2>
        <p class="section-lead">Halaman untuk mengelola data Alokasi Anggaran.</p>

        <div class="card">
            <div class="card-header">
                <h4>Update Alokasi Anggaran</h4>
                <div class="card-header-action">
                    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Tambah Alokasi Anggaran
                    </button> --}} 
                    {{-- <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-capaian-kinerja"></button> --}}
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('realisasi-anggaran.update', Crypt::encrypt($realisasianggaran->id)) }}" method="POST" id="form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        @php
                        $tahun = date('Y');
                        @endphp
                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required>
                            <option value="" disabled selected>Pilih Tahun</option>
                            @for ($i = 0; $i <= 4; $i++) 
                                <option value="{{ $tahun+$i }}" {{ $realisasianggaran->tahun == $tahun+$i ? 'selected' : '' }}>{{ $tahun+$i }}</option>
                            @endfor
                        </select>

                        @error('tahun')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="sasaran_strategis">Sasaran Strategis</label>
                        <select class="selecpicker @error('sasaran_strategis_id') is-invalid @enderror" name="sasaran_strategis_id" id="sasaran_strategis" data-show-subtext="true" data-live-search="true" required>
                            <option value="" selected disabled>Pilih Sasaran Strategis</option>
                            @foreach ($sasaranstrategis as $item)
                            <option value="{{ $item->id }}" {{ $realisasianggaran->sasaranstrategis_id == $item->id ? 'selected' : '' }}>{!! $item->sasaran_strategis !!} | Tahun : {{ $item->targetkinerja->tahun }} | Target : {{ $item->targetkinerja->target }}</option>
                            @endforeach
                        </select>
                        @error('sasaran_strategis_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="alokasi_anggaran">Alokasi</label>
                        <input type="text" class="form-control @error('alokasi_anggaran') is-invalid @enderror" id="alokasi_anggaran" name="alokasi_anggaran" placeholder="Alokasi Anggaran" required step="any" value="{{ $realisasianggaran->alokasi }}">
                        <small class="text-danger">Masukkan angka tanpa titik atau koma</small>
                        @error('alokasi_anggaran')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="realisasi">Realisasi</label>
                        <input type="text" class="form-control @error('realisasi') is-invalid @enderror" id="realisasi" name="realisasi" placeholder="Realisasi Kinerja" required step="any" value="{{ $realisasianggaran->realisasi }}">
                        <small class="text-danger">Masukkan angka tanpa titik atau koma</small>
                        @error('realisasi')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        $('#form').submit(function(e) {
            e.preventDefault();
            $('#btn-submit-indikator-kinerja').html('Menyimpan...');
            $('#form').unbind('submit').submit();

        })
    });
</script>

@if (session('error-insert'))
<script>
    swal("Data yang anda masukkan tidak valid!, silahkan coba lagi", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection