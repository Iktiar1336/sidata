@extends('layouts.dashboard')

@section('title')
    Indikator Kinerja
@endsection

@section('css')
    
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Indikator Kinerja</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Master Data Kinerja</div>
                <div class="breadcrumb-item">Indikator Kinerja</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">
                Update Indikator Kinerja
            </h2>

            <div class="card">
                <div class="card-header">
                    <h4>Update Indikator Kinerja</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('indikator-kinerja.update', Crypt::encrypt($indikator_kinerja->id)) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="indikator_kinerja">Indikator Kinerja</label>
                            <textarea class="form-control @error('indikator_kinerja') is-invalid @enderror" name="indikator_kinerja" placeholder="Indikator Kinerja">{!! $indikator_kinerja->content !!}
                            </textarea>
                    
                            @error('indikator_kinerja')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                                Simpan
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            CKEDITOR.replace('indikator_kinerja');
        });
    </script>
@endsection