@extends('layouts.dashboard')

@section('title')
Indikator Kinerja
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Indikator Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Indikator Kinerja</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Indikator Kinerja</h2>
        <p class="section-lead">Halaman untuk mengelola data indikator kinerja.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Indikator Kinerja</h4>
                <div class="card-header-action">
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-indikator-kinerja">Tambah Indikator Kinerja</button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table-indikator-kinerja">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Indikator Kinerja</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('indikator-kinerja.store') }}" method="POST" class="modal-part" id="modal-indikator-kinerja-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="indikator_kinerja">Indikator Kinerja</label>
        <textarea class="form-control @error('indikator_kinerja') is-invalid @enderror" name="indikator_kinerja" placeholder="Indikator Kinerja" id="indikatorkinerja"></textarea>

        @error('indikator_kinerja')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
            Simpan
        </button>
    </div>
</form>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('indikatorkinerja');

        $('#table-indikator-kinerja').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/indikator-kinerja",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'indikator_kinerja', name: 'indikator_kinerja'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Indikator kinerja berhasil di tambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Indikator Kinerja berhasil di update!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Indikator kinerja tidak dapat di hapus!, karena sudah terikat dengan data lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Indikator kinerja berhasil di hapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection