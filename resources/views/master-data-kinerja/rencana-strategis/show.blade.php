@extends('layouts.dashboard')

@section('title')
Detail Rencana Strategis
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Rencana Strategis</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Detail Rencana Strategis</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Detail Rencana Strategis Tahun : {{ $rencanastrategis->tahun }}</h2>
        <p class="section-lead">Halaman untuk mengelola data Rencana Strategis.</p>

        <div class="card">
            <div class="card-header">
                <h4>Detail Rencana Strategis Tahun : {{ $rencanastrategis->tahun }}</h4>
                <div class="card-header-action">
                    <a href="{{ route('rencana-strategis.index') }}" class="btn btn-primary">Kembali</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-rencana-strategis">
                        <thead>
                            <tr>
                                <th>
                                    Tujuan dan Sasaran Strategis
                                </th>
                                <th>Indikator Kinerja</th>
                                <th>Target Kinerja {{ $rencanastrategis->tahun }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($rencanastrategis->tujuan as $tujuan)
                                <tr>
                                    <td>
                                        <b>Tujuan ANRI :</b>
                                        {!! $tujuan->tujuan !!}
                                    </td>
                                    <td>{!! $tujuan->indikatorkinerja->content !!}</td>
                                    <td>{!! $tujuan->targetkinerja->target !!}</td>
                                </tr>
                            @endforeach
                            @foreach ($rencanastrategis->sasaranstrategis as $sasaran)
                                <tr>
                                    <td>
                                        <b>Sasaran Strategis {{ $no++ }} :</b>
                                        {!! $sasaran->sasaran_strategis !!}
                                    </td>
                                    <td>{!! $sasaran->indikatorkinerja->content !!}</td>
                                    <td>{!! $sasaran->targetkinerja->target !!}</td>
                                </tr>
                            @endforeach
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>

@if (session('insert'))
<script>
    swal("Rencana Strategis berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Rencana Strategis berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Rencana Strategis Tidak Dapat Dihapus!, Karena Sudah Terikat Dengan Data Lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Rencana Strategis berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection