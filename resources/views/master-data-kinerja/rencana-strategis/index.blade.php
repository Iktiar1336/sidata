@extends('layouts.dashboard')

@section('title')
Rencana Strategis
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Rencana Strategis</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Rencana Strategis</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Rencana Strategis</h2>
        <p class="section-lead">Halaman untuk mengelola data Rencana Strategis.</p>

        <div class="card">
            <div class="card-header">
                <h4>Rencana Strategis</h4>
                <div class="card-header-action">
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-rencana-strategis">Tambah Rencana Strategis</button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-rencana-strategis">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Tahun</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('rencana-strategis.store') }}" method="POST" class="modal-part" id="modal-rencana-strategis-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="tahun">Tahun</label>
        @php
            $tahun = date('Y');
        @endphp
        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true">
            <option value="" disabled selected>Pilih Tahun</option>
            @for ($i = 0; $i <= 4; $i++)
                <option value="{{ $tahun+$i }}" >{{ $tahun+$i }}</option>
            @endfor
        </select>
        
        @error('tahun')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>    

    <div class="form-group">
        <label for="tujuan_id">Tujuan</label>
        <select class="selecpicker @error('tujuan_id') is-invalid @enderror" name="tujuan_id[]" id="tujuan_id" data-show-subtext="true" data-live-search="true" multiple>
            <option value="" disabled>Pilih Tujuan</option>
            @foreach ($tujuan as $item)
                <option value="{{ $item->id }}" >{!! $item->tujuan !!} | Tahun : {{ $item->targetkinerja->tahun }}</option>
            @endforeach 
        </select>
        @error('tujuan_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="sasaran_strategis">Sasaran Strategis</label>
        <select class="selecpicker @error('sasaran_strategis_id') is-invalid @enderror" name="sasaran_strategis_id[]" id="sasaran_strategis" data-show-subtext="true" data-live-search="true" multiple>
            <option value="" disabled>Pilih Sasaran Strategis</option>
            @foreach ($sasaranstrategis as $item)
                <option value="{{ $item->id }}" >{!! $item->sasaran_strategis !!} | Tahun : {{ $item->targetkinerja->tahun }}</option>
            @endforeach 
        </select>
        @error('sasaran_strategis_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>


    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
            Simpan
        </button>
    </div>
</form>

<div class="modal fade" data-backdrop="static" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {

        var table = $('#table-rencana-strategis').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/rencana-strategis",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'tahun', name: 'tahun'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('.selecpicker').selectpicker();
    });
</script>

@if (session('insert'))
<script>
    swal("Rencana Strategis berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Rencana Strategis berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Rencana Strategis Tidak Dapat Dihapus!, Karena Sudah Terikat Dengan Data Lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete'))
<script>
    swal("Rencana Strategis berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection