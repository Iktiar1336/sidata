@extends('layouts.dashboard')

@section('title')
Sasaran Strategis
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Sasaran Strategis</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Sasaran Strategis</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Sasaran Strategis</h2>
        <p class="section-lead">Halaman untuk mengelola data Sasaran Strategis.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Sasaran Strategis</h4>
                <div class="card-header-action">
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-sasaran-strategis">Tambah Sasaran Strategis</button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-sasaran-strategis">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Sasaran Strategis</th>
                                <th>Indikator Kinerja</th>
                                <th>Target Kinerja</th>
                                <th>Tahun</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('sasaran-strategis.store') }}" method="POST" class="modal-part" id="modal-sasaran-strategis-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="indikatorkinerja">Pilih Indikator Kinerja</label>
        <select class="selecpicker" name="indikatorkinerja_id" id="indikatorkinerja">
            <option value="" disabled selected>Pilih Indikator Kinerja</option>
            @foreach ($indikatorkinerja as $item)
                <option value="{{ $item->id }}">{!! $item->content !!}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="targetkinerja">Pilih Target Kinerja</label>
        <select class="form-control" name="targetkinerja_id" id="targetkinerja" disabled>
            <option value="" disabled selected>Pilih Target Kinerja</option>
            
        </select>
    </div>
    <div class="form-group">
        <label for="sasaran_strategis">Sasaran Strategis</label>
        <textarea class="form-control @error('sasaran_strategis') is-invalid @enderror" name="sasaran_strategis" placeholder="Sasaran Strategis" id="sasaranstrategis"></textarea>

        @error('sasaran_strategis')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
            Simpan
        </button>
    </div>
</form>
@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        CKEDITOR.replace('sasaranstrategis');

        $('#table-sasaran-strategis').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/sasaran-strategis",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'sasaran_strategis', name: 'sasaran_strategis'},
                {data: 'indikatorkinerja', name: 'indikatorkinerja'},
                {data: 'targetkinerja', name: 'targetkinerja'},
                {data: 'tahun', name: 'tahun'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#indikatorkinerja').on('change', function() {
            var id = $(this).val();
            $('#targetkinerja').empty();
            $('#targetkinerja').addClass('form-control');
            $('#targetkinerja').removeClass('selecpicker');
            $('#targetkinerja').append('<option>loading...</option>');
            $.ajax({
                url: '{{ route('sasaran-strategis.create') }}',
                type: 'GET',
                data: {
                    indikatorkinerja_id: id,
                    csrf_token: '{{ csrf_token() }}',
                },
                success: function(data) {
                    $('#targetkinerja').prop('disabled', false);
                    $('#targetkinerja').empty();
                    $('#targetkinerja').removeClass('form-control');
                    $('#targetkinerja').addClass('selecpicker');
                    if(data.length > 0) {
                        $('#targetkinerja').append('<option value="" disabled selected>Pilih Target Kinerja</option>');
                        $.each(data, function(key, value) {
                            $('#targetkinerja').append('<option value="'+value.id+'"> Target : '+value.target+' Tahun : '+value.tahun+'</option>');
                            
                        });
                    } else {
                        $('#targetkinerja').append('<option value="" disabled selected>Tidak ada data</option>');
                    }

                    $('.selecpicker').selectpicker('refresh');
                }
            });
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Sasaran strategis berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Sasaran strategis sudah ada, silahkan pilih data yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Sasaran strategis berhasil di update!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete'))
<script>
    swal("Sasaran strategis berhasil di hapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection