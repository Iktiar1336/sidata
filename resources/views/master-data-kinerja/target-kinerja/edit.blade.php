@extends('layouts.dashboard')

@section('title')
    Target Kinerja
@endsection

@section('css')
    
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Target Kinerja</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Master Data Kinerja</div>
                <div class="breadcrumb-item">Target Kinerja</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">
                Update Target Kinerja
            </h2>

            <div class="card">
                <div class="card-header">
                    <h4>Update Target Kinerja</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('target-kinerja.update', Crypt::encrypt($targetkinerja->id)) }}" method="POST" >
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="indikatorkinerja">Pilih Indikator Kinerja</label>
                            <select class="selecpicker" name="indikatorkinerja_id" id="indikatorkinerja">
                                <option value="" disabled selected>Pilih Indikator Kinerja</option>
                                @foreach ($indikatorkinerja as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $targetkinerja->indikatorkinerja_id ? 'selected' : '' }}>{!! $item->content !!}</option>
                                @endforeach
                            </select>
                    
                        </div>
                        <div class="form-group">
                            <label for="tahun">Tahun</label>
                            @php
                                $tahun = date('Y');
                            @endphp
                            <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun">
                                <option value="" disabled selected>Pilih Tahun</option>
                                @for ($i = 0; $i <= 5; $i++)
                                    <option value="{{ $tahun+$i }}" {{ $tahun+$i == $targetkinerja->tahun ? 'selected' : '' }}>{{ $tahun+$i }}</option>
                                @endfor
                            </select>
                            {{-- <input type="text" class="form-control @error('tahun') is-invalid @enderror" id="tahun" name="tahun" placeholder="Tahun"> --}}
                            
                            @error('tahun')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="type">Type Target</label>
                            <select class="selecpicker @error('type') is-invalid @enderror" name="type" id="type">
                                <option value="" disabled selected>Pilih Type Target</option>
                                <option value="nilai" {{ $targetkinerja->type == 'nilai' ? 'selected' : '' }}>Kuantitas</option>
                                <option value="kategori" {{ $targetkinerja->type == 'kategori' ? 'selected' : '' }}>Kualitas</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="target_kinerja">Target Kinerja</label>
                            <input type="number" min="0" class="form-control @error('target_kinerja') is-invalid @enderror" id="target_kinerja" value="{{ $targetkinerja->target }}" name="target_kinerja" placeholder="Target Kinerja">
                    
                            @error('target_kinerja')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.selecpicker').selectpicker();
        });
    </script>


@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif


@endsection