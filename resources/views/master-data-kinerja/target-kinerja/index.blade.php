@extends('layouts.dashboard')

@section('title')
Target Kinerja
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Target Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Target Kinerja</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Target Kinerja</h2>
        <p class="section-lead">Halaman untuk mengelola data Target kinerja.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Target Kinerja</h4>
                <div class="card-header-action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#targetkinerjamodal">
                        Tambah Target Kinerja
                    </button>
                    {{-- <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-target-kinerja">Tambah Target Kinerja</button> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-target-kinerja">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Tahun</th>
                                <th>Indikator Kinerja</th>
                                <th>Target</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="targetkinerjamodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Target Kinerja</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('target-kinerja.store') }}" method="POST" >
                @csrf
                <div class="form-group">
                    <label for="indikatorkinerja">Pilih Indikator Kinerja</label>
                    <select class="selecpicker" name="indikatorkinerja_id" id="indikatorkinerja" required>
                        <option value="" disabled selected>Pilih Indikator Kinerja</option>
                        @foreach ($indikatorkinerja as $item)
                            <option value="{{ $item->id }}">{!! $item->content !!}</option>
                        @endforeach
                    </select>
            
                </div>
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    @php
                        $tahun = date('Y');
                    @endphp
                    <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required>
                        <option value="" disabled selected>Pilih Tahun</option>
                        @for ($i = 0; $i <= 4; $i++)
                            <option value="{{ $tahun+$i }}" >{{ $tahun+$i }}</option>
                        @endfor
                    </select>
                    
                    @error('tahun')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>    
                <div class="form-group">
                    <label for="type">Type Nilai Target</label>
                    <select class="selecpicker @error('type') is-invalid @enderror" name="type" id="type" data-show-subtext="true" data-live-search="true" required>
                        <option value="" disabled selected>Pilih Type Nilai Target</option>
                        <option value="nilai" >Nilai</option>
                        <option value="kategori" >Kategori</option>
                    </select>

                    @error('type')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="target_kinerja">Target Kinerja</label>
                    <input type="number" class="form-control @error('target_kinerja') is-invalid @enderror" id="target_kinerja" name="target_kinerja" min="0" placeholder="Target Kinerja" required>
            
                    @error('target_kinerja')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        $('#table-target-kinerja').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/target-kinerja",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'tahun', name: 'tahun'},
                {data: 'indikator_kinerja', name: 'indikator_kinerja'},
                {data: 'target', name: 'target'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#type').on('change', function() {
            if (this.value == 'nilai') {
                $('#target_kinerja').attr('type', 'number');
            } else {
                $('#target_kinerja').attr('type', 'text');
            }
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Target kinerja berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Target kinerja berhasil di update!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Target kinerja gagal dihapus! Karena sudah terikat dengan data lain.", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Target kinerja sudah ada, silahkan pilih tahun yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Target kinerja berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif


@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection