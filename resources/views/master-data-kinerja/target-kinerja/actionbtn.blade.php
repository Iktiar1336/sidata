<form action="{{ route('target-kinerja.destroy', Crypt::encrypt($row->id)) }}" method="POST">
    @csrf
    @method('DELETE')
    <a href="{{ route('target-kinerja.edit', Crypt::encrypt($row->id)) }}" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
    <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i></button>
</form>

<script>
    $('.btn-delete').on('click', function(e) {
        e.preventDefault();
        var form = $(this).parents('form');
        swal({
            title: 'Apakah Anda Yakin?',
            text: "Target kinerja akan dihapus secara permanen!",
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            } else {
                Swal.fire("Target kinerja tidak jadi dihapus!");
            }
        });
            
    });
</script>