@extends('layouts.dashboard')

@section('title')
Tugas & Fungsi
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Tugas & Fungsi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
            <div class="breadcrumb-item">Tugas & Fungsi</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Tugas dan Fungsi</h2>
        <p class="section-lead">
            Berikut adalah tugas , fungsi dan kewenangan Arsip Nasional Republik Indonesia
        </p>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Tugas dan Fungsi</h4>
                    </div>
                    <div class="card-body">
                        @forelse ($tugasfungsi as $item)
                            <div class="media">
                                <div class="media-body">
                                    <h5 class="mt-0">Tugas</h5>
                                    {!! $item->tugas !!}
                                    <hr>
                                    <h5 class="mt-0">Fungsi</h5>
                                    {!! $item->fungsi !!}
                                    <h5 class="mt-0">Kewenangan</h5>
                                    {!! $item->kewenangan !!}
                                </div>
                            </div>
                            <a href="{{ route('tugas-fungsi.edit', Crypt::encrypt($item->id)) }}" class="btn btn-warning btn-block">Edit</a>
                        @empty
                          <h5 class="text-danger">
                              Maaf Tugas dan Fungsi saat ini belum tersedia
                          </h5>
                          <p class="mt-2">
                              Silahkan Tambahkan Tugas dan Fungsi
                          </p>

                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tugasfungsi">
                              Tambahkan
                          </button>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="tugasfungsi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Tugas , Fungsi & Kewenangan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form action="{{ route('tugas-fungsi.store') }}" method="POST">
                  @csrf
                  <div class="form-group">
                      <label>Tugas :</label>
                      <textarea name="tugas" class="form-control @error('tugas') is-invalid @enderror" required id="tugas"></textarea>
                      @error('tugas')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                  </div>
                  <div class="form-group">
                      <label>Fungsi :</label>
                      <textarea name="fungsi" class="form-control @error('fungsi') is-invalid @enderror" required id="fungsi"></textarea>
                      @error('fungsi')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                  </div>
                  <div class="form-group">
                      <label>Kewenangan :</label>
                      <textarea name="kewenangan" class="form-control @error('kewenangan') is-invalid @enderror" required id="kewenangan"></textarea>
                      @error('kewenangan')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-success btn-block">Simpan</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>

@endsection

@section('js')
<script>
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
    };
    CKEDITOR.replace('tugas', options);
    CKEDITOR.replace('fungsi', options);
    CKEDITOR.replace('kewenangan', options);
</script>

@if (session('insert'))
    <script>
        swal("Tugas , fungsi & kewenangan berhasil ditambahkan", {
            icon: "success",
            title: "Berhasil",
        });
    </script>
@endif

@if (session('update'))
    <script>
        swal("Tugas , fungsi & kewenangan berhasil di update", {
            icon: "success",
            title: "Berhasil",
        });
    </script>
@endif


@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection
