@extends('layouts.dashboard')

@section('title')
Capaian SPBE
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Capaian SPBE</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Capaian SPBE</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Capaian SPBE</h2>
        <p class="section-lead">Halaman untuk mengelola data Capaian SPBE.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Capaian SPBE</h4>
                <div class="card-header-action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Tambah Capaian SPBE
                    </button>
                    {{-- <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-capaian-kinerja"></button> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-capaianspbe">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Judul</th>
                                <th>Tahun</th>
                                <th>Indeks</th>
                                <th>Predikat</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Capaian SPBE</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('capaian-spbe.store') }}" method="POST" id="form">
                @csrf
                <div class="form-group">
                    <label for="judul">Judul Capaian</label>
                    <textarea class="form-control @error('judul') is-invalid @enderror" name="judul" placeholder="Judul Capaian" id="judul"></textarea>
            
                    @error('judul')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    @php
                        $tahun = date('Y');
                    @endphp
                    <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required>
                        <option value="" disabled selected>Pilih Tahun</option>
                        @for ($i = 0; $i <= 4; $i++)
                            <option value="{{ $tahun+$i }}" >{{ $tahun+$i }}</option>
                        @endfor
                    </select>
                    
                    @error('tahun')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>    

                <div class="form-group">
                    <label for="indeks">Indeks</label>
                    <input type="number" class="form-control @error('indeks') is-invalid @enderror" id="indeks" step="any" name="indeks" min="0" max="5" placeholder="Indeks">
            
                    @error('indeks')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <textarea class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" placeholder="Deskripsi Capaian" id="deskripsi"></textarea>
            
                    @error('deskripsi')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        CKEDITOR.replace('deskripsi');

        $('#form').submit(function(e) {
            e.preventDefault();
            $('#btn-submit-indikator-kinerja').html('Menyimpan...');
            $('#form').unbind('submit').submit();

        })

        $('#table-capaianspbe').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/capaian-spbe",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'judul', name: 'sasaranstrategis'}, 
                {data: 'tahun', name: 'tahun'},
                {data: 'indeks', name: 'indeks'}, 
                {data: 'predikat', name: 'realisasi'}, 
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Capaian SPBE berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Capaian SPBE sudah ada, silahkan pilih data yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Capaian SPBE berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('error-insert'))
<script>
    swal("Data yang anda masukkan tidak valid!, silahkan coba lagi", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif

@endsection