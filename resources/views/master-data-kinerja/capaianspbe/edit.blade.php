@extends('layouts.dashboard')

@section('title')
Update Capaian SPBE
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Update Capaian SPBE</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Update Capaian SPBE</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Update Capaian SPBE</h2>
        <p class="section-lead">Halaman untuk mengelola data Capaian SPBE.</p>

        <div class="card">
            <div class="card-header">
                <h4>Update Capaian SPBE</h4>
                <div class="card-header-action">
                    <a href="{{ route('capaian-spbe.index') }}" class="btn btn-primary">Kembali</a>
                    {{-- <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-capaian-kinerja"></button> --}}
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('capaian-spbe.update', Crypt::encrypt($capaian_spbe->id)) }}" method="POST" id="form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="judul">Judul Capaian</label>
                        <textarea class="form-control @error('judul') is-invalid @enderror" name="judul" placeholder="Judul Capaian" id="judul">{{ $capaian_spbe->judul }}</textarea>
                
                        @error('judul')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        @php
                            $tahun = date('Y');
                        @endphp
                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required>
                            <option value="" disabled selected>Pilih Tahun</option>
                            @for ($i = 0; $i <= 4; $i++)
                                <option value="{{ $tahun+$i }}" {{ $capaian_spbe->tahun == $tahun+$i ? 'selected' : '' }}>{{ $tahun+$i }}</option>
                            @endfor
                        </select>
                        
                        @error('tahun')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>    
    
                    <div class="form-group">
                        <label for="indeks">Indeks</label>
                        <input type="number" class="form-control @error('indeks') is-invalid @enderror" id="indeks" step="any" name="indeks" min="0" max="5" placeholder="Indeks" value="{{ $capaian_spbe->indeks }}">
                
                        @error('indeks')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
    
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" placeholder="Deskripsi Capaian" id="deskripsi">{!! $capaian_spbe->deskripsi !!}</textarea>
                
                        @error('deskripsi')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
    
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        CKEDITOR.replace('deskripsi');

        $('#form').submit(function(e) {
            e.preventDefault();
            $('#btn-submit-indikator-kinerja').html('Menyimpan...');
            $('#form').unbind('submit').submit();

        })
    });
</script>

@if (session('insert'))
<script>
    swal("Capaian SPBE berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Capaian SPBE sudah ada, silahkan pilih data yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Capaian SPBE berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('error-insert'))
<script>
    swal("Data yang anda masukkan tidak valid!, silahkan coba lagi", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif

@endsection