@extends('layouts.dashboard')

@section('title')
Struktur Organisasi
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Struktur Organisasi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
            <div class="breadcrumb-item">Struktur Organisasi</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Struktur Organisasi</h2>
        <p class="section-lead">
        </p>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Struktur Organisasi</h4>
                    </div>
                    <div class="card-body">
                        @forelse ($strukturorganisasi as $item)
                        <img src="{{ asset('uploads/strukturorganisasi/'.$item->file) }}" alt="" class="img-fluid">
                        <a href="{{ route('struktur-organisasi.edit', Crypt::encrypt($item->id)) }}" class="btn btn-warning btn-block mt-3">Edit</a>

                        @empty
                        <h5 class="text-danger">
                            Maaf Struktur Organisasi Saat Ini Belum Tersedia
                        </h5>
                        <p class="mt-2">
                            Silahkan Tambahkan Struktur Organisasi Terlebih Dahulu
                        </p>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#strukturorganisasi">
                            Tambahkan
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="strukturorganisasi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Struktur Organisasi</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ route('struktur-organisasi.store') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group">
                                                <label>Image :</label>
                                                <input type="file" name="struktur-organisasi" class="form-control @error('struktur-organisasi') is-invalid @enderror" required>
                                                @error('struktur-organisasi')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            {{-- <div class="form-group">
                                        <label for="exampleFormControlSelect1">Status</label>
                                        <select class="form-control" name="status" id="exampleFormControlSelect1">
                                          <option value="1">Aktif</option>
                                          <option value="0">Tidak Aktif</option>
                                        </select>
                                      </div> --}}
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success btn-block">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endif

@if (Session::has('success'))
<script>
	swal("{{ Session::get('success') }}", {
		title: "Success",
		icon: "success",
	});
</script>
@endif
@endsection
