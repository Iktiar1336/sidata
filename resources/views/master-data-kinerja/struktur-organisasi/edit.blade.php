@extends('layouts.dashboard')

@section('title')
Update Struktur Organisasi
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Update Struktur Organisasi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
            <div class="breadcrumb-item">Update Struktur Organisasi</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Update Struktur Organisasi</h2>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Update Struktur Organisasi</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('struktur-organisasi.update', Crypt::encrypt($strukturorganisasi->id)) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="preview">Preview :</label>
                                <img id="imagePreview" src="{{ asset('uploads/strukturorganisasi/'.$strukturorganisasi->file) }}" alt="" class="img-fluid m-auto" width="100%">
                                <br>
                                <br>
                                <label>Image :</label>
                                <input id="imageUpload" type="file" name="struktur-organisasi" class="form-control @error('struktur-organisasi') is-invalid @enderror">
                                @error('struktur-organisasi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                    <label for="exampleFormControlSelect1">Status</label>
                    <select class="form-control" name="status" id="exampleFormControlSelect1">
                        <option value="1">Aktif</option>
                        <option value="0">Tidak Aktif</option>
                    </select>
                </div> --}}
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imageUpload").change(function () {
        readURL(this);
    });
</script>
@endsection