@extends('layouts.dashboard')

@section('title')
    Tujuan
@endsection

@section('css')
    
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Tujuan</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Master Data Kinerja</div>
                <div class="breadcrumb-item">Tujuan</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">
                Update Tujuan
            </h2>

            <div class="card">
                <div class="card-header">
                    <h4>Update Tujuan</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('tujuan.update', Crypt::encrypt($tujuan->id)) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="indikatorkinerja">Pilih Indikator Kinerja</label>
                            <select class="selecpicker" name="indikatorkinerja_id" id="indikatorkinerja">
                                <option value="" disabled selected>Pilih Indikator Kinerja</option>
                                @foreach ($indikatorkinerja as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $tujuan->indikatorkinerja_id ? 'selected' : '' }}>{!! $item->content !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="targetkinerja">Pilih Target Kinerja</label>
                            <select class="selecpicker" name="targetkinerja_id" id="targetkinerja">
                                <option value="" disabled selected>Pilih Target Kinerja</option>
                                @foreach ($targetkinerja as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $tujuan->targetkinerja_id ? 'selected' : '' }}>{{ 'Target : ' . $item->target . ' Untuk Tahun : ' . $item->tahun }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tujuan">Tujuan</label>
                            <textarea class="form-control @error('tujuan') is-invalid @enderror" name="tujuan" placeholder="Tujuan" id="sasaranstrategis">
                                {!! $tujuan->tujuan !!}
                            </textarea>
                    
                            @error('tujuan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('#indikatorkinerja').selectpicker();
            $('#targetkinerja').selectpicker();

            CKEDITOR.replace('sasaranstrategis');
        });
    </script>


    @if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif


@endsection