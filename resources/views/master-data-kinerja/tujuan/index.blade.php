@extends('layouts.dashboard')

@section('title')
Tujuan
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Tujuan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Tujuan</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Tujuan</h2>
        <p class="section-lead">Halaman untuk mengelola data Tujuan.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Tujuan</h4>
                <div class="card-header-action">
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-tujuan">Tambah Tujuan</button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table-tujuan">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Tujuan</th>
                                <th>Indikator Kinerja</th>
                                <th>Target Kinerja</th>
                                <th>Tahun</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('tujuan.store') }}" method="POST" class="modal-part" id="modal-tujuan-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="indikatorkinerja">Pilih Indikator Kinerja</label>
        <select class="selecpicker" name="indikatorkinerja_id" id="indikatorkinerja">
            <option value="" disabled selected>Pilih Indikator Kinerja</option>
            @foreach ($indikatorkinerja as $item)
                <option value="{{ $item->id }}">{!! $item->content !!}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="targetkinerja">Pilih Target Kinerja</label>
        <select class="selecpicker" name="targetkinerja_id" id="targetkinerja">
            <option value="" disabled selected>Pilih Target Kinerja</option>
            @foreach ($targetkinerja as $item)
                <option value="{{ $item->id }}">{{ 'Target : ' . $item->target . ' Untuk Tahun : ' . $item->tahun }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="tujuan">Tujuan</label>
        <textarea class="form-control @error('tujuan') is-invalid @enderror" name="tujuan" placeholder="Tujuan" id="tujuan"></textarea>

        @error('tujuan')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
            Simpan
        </button>
    </div>
</form>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {

        $('.selecpicker').selectpicker();

        CKEDITOR.replace('tujuan');

        $('#table-tujuan').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/tujuan",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'tujuan', name: 'tujuan'},
                {data: 'indikatorkinerja', name: 'indikatorkinerja'},
                {data: 'targetkinerja', name: 'targetkinerja'},
                {data: 'tahun', name: 'tahun'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Tujuan berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Tujuan berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Tujuan Tidak Dapat Dihapus!, Karena Sudah Terikat Dengan Data Lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Tujuan berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection