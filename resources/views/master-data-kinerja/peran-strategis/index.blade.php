@extends('layouts.dashboard')

@section('title')
Peran Strategis
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Peran Strategis</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Peran Strategis</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Peran Strategis</h2>
        <p class="section-lead">Halaman untuk mengelola data Peran Strategis.</p>

        <div class="card">
            <div class="card-header">
                <h4>Peran Strategis</h4>
                <div class="card-header-action">
                    
                </div>
            </div>
            <div class="card-body">
                @forelse ($peran_strategis as $item)
                    <div class="media">
                        <div class="media-body">
                            {!! $item->peran_strategis !!}
                            <hr>
                        </div>
                    </div>
                    <a href="{{ route('peran-strategis.edit', Crypt::encrypt($item->id)) }}" class="btn btn-warning btn-block">Edit</a>
                @empty
                    <h5 class="text-danger">
                        Maaf Peran Strategis saat ini belum tersedia
                    </h5>
                    <p class="mt-2">
                        Silahkan Tambahkan Peran Strategis
                    </p>
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-peran-strategis">Tambah Peran Strategis</button>
                @endforelse
            </div>
        </div>
    </div>
</section>

<form action="{{ route('peran-strategis.store') }}" method="POST" class="modal-part" id="modal-peran-strategis-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="peran_strategis">Peran Strategis</label>
        <textarea class="form-control @error('peran_strategis') is-invalid @enderror" name="peran_strategis" placeholder="Peran Strategis" id="peranstrategis"></textarea>

        @error('peran_strategis')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
            Simpan
        </button>
    </div>
</form>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('peranstrategis');

        
    });
</script>

@if (session('insert'))
<script>
    swal("Peran Strategis berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Peran strategis berhasil di update!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Peran strategis tidak dapat dihapus!, karena sudah terikat dengan data lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Peran Strategis berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection