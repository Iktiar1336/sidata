@extends('layouts.dashboard')

@section('title')
Update Peran Strategis
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Peran Strategis</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Peran Strategis</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">
            Update Peran Strategis
        </h2>

        <div class="card">
            <div class="card-header">
                <h4>Update Peran Strategis</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('peran-strategis.update', Crypt::encrypt($peran_strategis->id)) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="peran_strategis">Peran Strategis</label>
                        <textarea class="form-control @error('peran_strategis') is-invalid @enderror" name="peran_strategis" placeholder="Peran Strategis" id="peranstrategis">
                        {!! $peran_strategis->peran_strategis !!}
                        </textarea>
                
                        @error('peran_strategis')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            CKEDITOR.replace('peranstrategis');
        });
    </script>
@endsection