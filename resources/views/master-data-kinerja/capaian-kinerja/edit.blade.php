@extends('layouts.dashboard')

@section('title')
Update Capaian Kinerja
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Capaian Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Capaian Kinerja</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">
            Update Capaian Kinerja
        </h2>

        <div class="card">
            <div class="card-header">
                <h4>Update Capaian Kinerja</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('capaian-kinerja.update', Crypt::encrypt($capaiankinerja->id)) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        @php
                            $tahun = date('Y');
                        @endphp
                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required disabled>
                            <option value="" disabled selected>Pilih Tahun</option>
                            @for ($i = 0; $i <= 4; $i++)
                                <option value="{{ $tahun+$i }}" {{ $capaiankinerja->tahun == $tahun+$i ? 'selected' : '' }}>{{ $tahun+$i }}</option>
                            @endfor
                            <input type="hidden" name="tahun" value="{{ $capaiankinerja->tahun }}">
                        </select>
                        
                        @error('tahun')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>    
                
                    <div class="form-group">
                        <label for="sasaran_strategis">Sasaran Strategis</label>
                        <select class="selecpicker @error('sasaran_strategis_id') is-invalid @enderror" name="sasaran_strategis_id" id="sasaran_strategis" data-show-subtext="true" data-live-search="true" required disabled>
                            <option value="" selected disabled>Pilih Sasaran Strategis</option>
                            @foreach ($sasaranstrategis as $item)
                                <option value="{{ $item->id }}" {{ $capaiankinerja->sasaranstrategis_id == $item->id ? 'selected' : '' }}>{!! $item->sasaran_strategis !!} | Tahun : {{ $item->targetkinerja->tahun }} | Target : {{ $item->targetkinerja->target }}</option>
                            @endforeach 

                            <input type="hidden" name="sasaran_strategis_id" value="{{ $capaiankinerja->sasaranstrategis_id }}">
                        </select>
                        
                        @error('sasaran_strategis_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <label for="realisasi">Realisasi</label>
                        <input type="number" min="0" class="form-control @error('realisasi') is-invalid @enderror" id="realisasi" name="realisasi" placeholder="Realisasi Kinerja" required step=".01" value="{{ $capaiankinerja->realisasi }}">
                
                        @error('realisasi')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="status1" value="1"
                                {{ $capaiankinerja->status == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status1">Publish</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="status2" value="0"
                                {{ $capaiankinerja->status == 0 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status2">Draft </label>
                        </div>
                        
                        @error('status')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.selecpicker').selectpicker();
        }); 
    </script>
@endsection