@extends('layouts.dashboard')

@section('title')
Capaian Kinerja
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Capaian Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Capaian Kinerja</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Capaian kinerja</h2>
        <p class="section-lead">Halaman untuk mengelola data Capaian Kinerja.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Capaian Kinerja</h4>
                <div class="card-header-action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Tambah Capaian kinerja
                    </button>
                    {{-- <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-capaian-kinerja"></button> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-capaiankinerja">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Sasaran Strategis</th>
                                <th>Target</th>
                                <th>Realisasi</th>
                                <th>Tahun</th>
                                <th>Persentase</th>
                                <th>Status</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Capaian Kinerja</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('capaian-kinerja.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    @php
                        $tahun = date('Y');
                    @endphp
                    <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true" required>
                        <option value="" disabled selected>Pilih Tahun</option>
                        @for ($i = 0; $i <= 4; $i++)
                            <option value="{{ $tahun+$i }}" >{{ $tahun+$i }}</option>
                        @endfor
                    </select>
                    
                    @error('tahun')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>    
            
                <div class="form-group">
                    <label for="sasaran_strategis">Sasaran Strategis</label>
                    <select class="selecpicker @error('sasaran_strategis_id') is-invalid @enderror" name="sasaran_strategis_id" id="sasaran_strategis" data-show-subtext="true" data-live-search="true" required>
                        <option value="" selected disabled>Pilih Sasaran Strategis</option>
                        @foreach ($sasaranstrategis as $item)
                            <option value="{{ $item->id }}">{!! $item->sasaran_strategis !!} | Tahun : {{ $item->targetkinerja->tahun }} | Target : {{ $item->targetkinerja->target }}</option>
                        @endforeach 
                    </select>
                    @error('sasaran_strategis_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div> 
                    @enderror
                </div>
            
                <div class="form-group">
                    <label for="realisasi">Realisasi</label>
                    <input type="number" min="0" class="form-control @error('realisasi') is-invalid @enderror" id="realisasi" name="realisasi" placeholder="Realisasi Kinerja" required step=".01">
            
                    @error('realisasi')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="">Status</label>
                    <div class="form-check">
                        <input class="form-check-input" value="1" type="radio" name="status" id="status1"
                            checked="">
                        <label class="form-check-label" for="status1">
                            Publish
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" value="0" type="radio" name="status" id="status2"
                            checked="">
                        <label class="form-check-label" for="status2">
                            Draft
                        </label>
                    </div>
                    @error('status')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        $('#table-capaiankinerja').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-data-kinerja/capaian-kinerja",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'sasaranstrategis', name: 'sasaranstrategis'}, 
                {data: 'target', name: 'target'}, 
                {data: 'realisasi', name: 'realisasi'}, 
                {data: 'tahun', name: 'tahun'},
                {data: 'persentase', name: 'persentase'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Capaian kinerja berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Capaian kinerja sudah ada, silahkan pilih data yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Capaian kinerja berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif

@endsection