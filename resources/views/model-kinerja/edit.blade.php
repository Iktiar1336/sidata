@extends('layouts.dashboard')

@section('title')
Update Model Kinerja
@endsection

@section('css')
<style>
    .custom{
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 50px;
        overflow-y: auto;
    }
    .nav-pills{
        width: 100%;
    }
    .nav-pills li{
        overflow-y: auto;
        display: flex;
        justify-content: center;
    }

    .step-line {
        background: #6777ef;
        height: 2px;
        width: 65vw;
        position: absolute;
        top: 22px;
        z-index: 1;
    }

    .nav-pills.custom li {
        background: #fff;
        margin: 0 10px;
        z-index: 2;
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Update Model Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Update Model Kinerja</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Model Kinerja</h2>
        <p class="section-lead">Halaman untuk mengelola data Model kinerja.</p>

        <div class="card">
            <div class="card-header">
                <h4>Update Model Kinerja</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('model-kinerja.update', Crypt::encrypt($model_kinerja->id)) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col">
                            <ul class="nav nav-pills justify-content-center custom row"> 
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" href="#pills-judul" id="pills-judul-tab" data-toggle="pill" data-target="#pills-judul" type="button" role="tab" aria-controls="pills-judul" aria-selected="true">1
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-pengantar" id="pills-pengantar-tab" data-toggle="pill" data-target="#pills-pengantar" type="button" role="tab" aria-controls="pills-pengantar" aria-selected="true">2
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-visimisi" id="pills-visimisi-tab" data-toggle="pill" data-target="#pills-visimisi" type="button" role="tab" aria-controls="pills-visimisi" aria-selected="false">3</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-corevaluesasn" id="pills-corevaluesasn-tab" data-toggle="pill" data-target="#pills-corevaluesasn" type="button" role="tab" aria-controls="pills-corevaluesasn" aria-selected="false">4</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-nilainilaianri" id="pills-nilainilaianri-tab" data-toggle="pill" data-target="#pills-nilainilaianri" type="button" role="tab" aria-controls="pills-nilainilaianri" aria-selected="false">5</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-latarbelakang" id="pills-latarbelakang-tab" data-toggle="pill" data-target="#pills-latarbelakang" type="button" role="tab" aria-controls="pills-latarbelakang" aria-selected="false">6</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-tugasfungsi" id="pills-tugasfungsi-tab" data-toggle="pill" data-target="#pills-tugasfungsi" type="button" role="tab" aria-controls="pills-tugasfungsi" aria-selected="false">7
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-peranstrategis" id="pills-peranstrategis-tab" data-toggle="pill" data-target="#pills-peranstrategis" type="button" role="tab" aria-controls="pills-peranstrategis" aria-selected="false">8
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-sistematikapenyajian" id="pills-sistematikapenyajian-tab" data-toggle="pill" data-target="#pills-sistematikapenyajian" type="button" role="tab" aria-controls="pills-sistematikapenyajian" aria-selected="false">9
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-rencanastrategis" id="pills-rencanastrategis-tab" data-toggle="pill" data-target="#pills-rencanastrategis" type="button" role="tab" aria-controls="pills-rencanastrategis" aria-selected="false">10</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-perjanjiankinerja" id="pills-perjanjiankinerja-tab" data-toggle="pill" data-target="#pills-perjanjiankinerja" type="button" role="tab" aria-controls="pills-perjanjiankinerja" aria-selected="false">11</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-capaiankinerjaorganisasi" id="pills-capaiankinerjaorganisasi-tab" data-toggle="pill" data-target="#pills-capaiankinerjaorganisasi" type="button" role="tab" aria-controls="pills-capaiankinerjaorganisasi" aria-selected="false">12</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-realisasianggaran" id="pills-realisasianggaran-tab" data-toggle="pill" data-target="#pills-realisasianggaran" type="button" role="tab" aria-controls="pills-realisasianggaran" aria-selected="false">13</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-capaiankinerja" id="pills-capaiankinerja-tab" data-toggle="pill" data-target="#pills-capaiankinerja" type="button" role="tab" aria-controls="pills-capaiankinerja" aria-selected="false">14
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-judul" role="tabpanel" aria-labelledby="pills-judul-tab">
                                    <div class="form-group">
                                        <label for="judul">Judul</label>
                                        <input type="text" name="judul" id="judul" class="form-control" placeholder="Masukkan Judul Model Kinerja" required value="{{ $model_kinerja->judul }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tahun">Tahun</label>
                                        @php
                                            $tahun = date('Y');
                                        @endphp
                                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true">
                                            <option value="" disabled selected>Pilih Tahun</option>
                                            @for ($i = 0; $i <= 4; $i++)
                                                <option value="{{ $tahun+$i }}" {{ $model_kinerja->tahun == $tahun+$i ? 'selected' : '' }}>{{ $tahun+$i }}</option>
                                            @endfor
                                        </select>
                                        
                                        @error('tahun')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div> 
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-pengantar-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-pengantar" role="tabpanel" aria-labelledby="pills-pengantar-tab">
                                    <div class="form-group">
                                        <label for="pengantar">Pengantar</label>
                                        <textarea class="form-control @error('pengantar') is-invalid @enderror" name="pengantar" placeholder="Pengantar" id="pengantar">{!! $model_kinerja->pengantar !!}</textarea>
                                
                                        @error('pengantar')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-judul-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-visimisi-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-visimisi" role="tabpanel" aria-labelledby="pills-visimisi-tab">
                                    @foreach ($visi_misi as $visimisi)
                                        <div class="form-group">
                                            <label for="visi">Visi</label>
                                            <textarea class="form-control @error('visi') is-invalid @enderror" name="visi" placeholder="visi" id="visi" readonly>{!! $visimisi->visi !!}</textarea>
                                    
                                            @error('visi')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="misi">Misi</label>
                                            <textarea class="form-control @error('misi') is-invalid @enderror" name="misi" placeholder="misi" id="misi" readonly>{!! $visimisi->misi !!}</textarea>
                                    
                                            @error('misi')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    @endforeach
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-pengantar-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-corevaluesasn-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-corevaluesasn" role="tabpanel" aria-labelledby="pills-corevaluesasn-tab">
                                    <div class="form-group">
                                        <label for="corevaluesasn">CORE VALUES ASN</label>
                                        <textarea class="form-control @error('corevaluesasn') is-invalid @enderror" name="corevaluesasn" placeholder="Core Values Asn" id="corevaluesasn">{!! $model_kinerja->core_values_asn !!}</textarea>
                                
                                        @error('corevaluesasn')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-visimisi-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-nilainilaianri-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-nilainilaianri" role="tabpanel" aria-labelledby="pills-nilainilaianri-tab">
                                    <div class="form-group">
                                        <label for="nilainilaianri">Nilai Nilai ANRI</label>
                                        <textarea class="form-control @error('nilainilaianri') is-invalid @enderror" name="nilainilaianri" placeholder="Nilai Nilai ANRI" id="nilainilaianri">{!! $model_kinerja->nilai_nilai_anri !!}</textarea>
                                
                                        @error('nilainilaianri')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-corevaluesasn-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-latarbelakang-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-latarbelakang" role="tabpanel" aria-labelledby="pills-latarbelakang-tab">
                                    <div class="form-group">
                                        <label for="latarbelakang">Latar Belakang</label>
                                        <textarea class="form-control @error('latarbelakang') is-invalid @enderror" name="latarbelakang" placeholder="Latar Belakang" id="latarbelakang">{!! $model_kinerja->latar_belakang !!}</textarea>
                                
                                        @error('latarbelakang')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-nilainilaianri-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-tugasfungsi-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-tugasfungsi" role="tabpanel" aria-labelledby="pills-tugasfungsi-tab">
                                    @foreach ($tugas_fungsi as $tugasfungsi)
                                        <div class="form-group">
                                            <label for="tugas">Tugas</label>
                                            <textarea class="form-control @error('tugas') is-invalid @enderror" name="tugas" placeholder="Tugas" id="tugas" readonly>{!! $tugasfungsi->tugas !!}</textarea>
                                    
                                            @error('tugas')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="fungsi">Fungsi</label>
                                            <textarea class="form-control @error('fungsi') is-invalid @enderror" name="fungsi" placeholder="Fungsi" id="fungsi" readonly>{!! $tugasfungsi->fungsi !!}</textarea>
                                    
                                            @error('fungsi')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="kewenangan">Kewenangan</label>
                                            <textarea class="form-control @error('kewenangan') is-invalid @enderror" name="kewenangan" placeholder="Kewenangan" id="kewenangan" readonly>{!! $tugasfungsi->kewenangan !!}</textarea>
                                    
                                            @error('kewenangan')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    @endforeach
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-latarbelakang-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-peranstrategis-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-peranstrategis" role="tabpanel" aria-labelledby="pills-peranstrategis-tab">
                                    @foreach ($peranstrategis as $item)
                                        <div class="form-group">
                                            <label for="peranstrategis">Peran Strategis</label>
                                            <textarea class="form-control @error('peranstrategis') is-invalid @enderror" name="peranstrategis" placeholder="Latar Belakang" id="peranstrategis">{!! $item->peran_strategis !!}</textarea>
                                    
                                            @error('peranstrategis')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    @endforeach
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-tugasfungsi-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-sistematikapenyajian-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-sistematikapenyajian" role="tabpanel" aria-labelledby="pills-sistematikapenyajian-tab">
                                    <div class="form-group">
                                        <label for="sistematikapenyajian">Sistematika Penyajian</label>
                                        <textarea class="form-control @error('sistematikapenyajian') is-invalid @enderror" name="sistematikapenyajian" placeholder="Latar Belakang" id="sistematikapenyajian">{!! $model_kinerja->sistematika_penyajian !!}</textarea>
                                
                                        @error('sistematikapenyajian')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-peranstrategis-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-rencanastrategis-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-rencanastrategis" role="tabpanel" aria-labelledby="pills-rencanastrategis-tab">
                                    <div class="form-group">
                                        <label for="rencanastrategis">Rencana Strategis</label>
                                        <select class="selectpicker @error('rencanastrategis') is-invalid @enderror" name="rencanastrategis" id="rencanastrategis" data-show-subtext="true" data-live-search="true">
                                            <option value="" disabled selected>Pilih Rencana Strategis</option>
                                            @foreach ($rencanastrategis as $item)
                                                <option value="{{ $item->id }}" {{ $model_kinerja->rencanastrategis_id == $item->id ? 'selected' : '' }}>Rencana Strategis Tahun : {{ $item->tahun }}</option>
                                            @endforeach
                                        </select>
                                
                                        @error('rencanastrategis')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-sistematikapenyajian-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-perjanjiankinerja-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-perjanjiankinerja" role="tabpanel" aria-labelledby="pills-perjanjiankinerja-tab">
                                    <div class="form-group">
                                        <label for="perjanjiankinerja">Perjanjian Kinerja</label>
                                        <select class="selectpicker @error('perjanjiankinerja') is-invalid @enderror" name="perjanjiankinerja" id="perjanjiankinerja" data-show-subtext="true" data-live-search="true">
                                            <option value="" disabled selected>Pilih Perjanjian Kinerja</option>
                                            @foreach ($perjanjiankinerja as $item)
                                                <option value="{{ $item->id }}" {{ $model_kinerja->perjanjiankinerja_id == $item->id ? 'selected' : '' }}>Perjanjian Kinerja Tahun : {{ $item->tahun }}</option>
                                            @endforeach
                                        </select>
                                
                                        @error('perjanjiankinerja')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-rencanastrategis-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-capaiankinerjaorganisasi-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-capaiankinerjaorganisasi" role="tabpanel" aria-labelledby="pills-capaiankinerjaorganisasi-tab">
                                    <div class="form-group">
                                        <label for="capaiankinerjaorganisasi">Capaian Kinerja</label>
                                        <select class="selectpicker @error('capaiankinerjaorganisasi') is-invalid @enderror" name="capaiankinerjaorganisasi[]" id="capaiankinerjaorganisasi" data-show-subtext="true" data-live-search="true" multiple>
                                            <option value="" disabled>Pilih Capaian Kinerja</option>
                                            @foreach ($capaiankinerja as $item)
                                                @if (in_array($item->id, $capaiankinerja_id))
                                                    <option value="{{ $item->id }}" selected>{!! $item->sasaranstrategis->sasaran_strategis !!} | Target : {{ $item->sasaranstrategis->targetkinerja->target }} | Realisasi : {{ $item->realisasi }} | Tahun : {{ $item->tahun }}</option>
                                                @else
                                                    <option value="{{ $item->id }}">{!! $item->sasaranstrategis->sasaran_strategis !!} | Target : {{ $item->sasaranstrategis->targetkinerja->target }} | Realisasi : {{ $item->realisasi }} | Tahun : {{ $item->tahun }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                
                                        @error('capaiankinerjaorganisasi')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-perjanjiankinerja-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-realisasianggaran-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-realisasianggaran" role="tabpanel" aria-labelledby="pills-realisasianggaran-tab">
                                    <div class="form-group">
                                        <label for="realisasianggaran">Realisasi Anggaran</label>
                                        <select class="selectpicker @error('realisasianggaran') is-invalid @enderror" name="realisasianggaran" id="realisasianggaran" data-show-subtext="true" data-live-search="true" multiple>
                                            <option value="" disabled >Pilih Realisasi Anggaran</option>
                                            @foreach ($realisasianggaran as $item)
                                                @if(in_array($item->id, $realisasianggaran_id))
                                                    <option value="{{ $item->id }}" selected>{!! $item->sasaranstrategis->sasaran_strategis !!} | Alokasi : Rp. {{ number_format($item->alokasi, 0, ',', '.') }} | Realisasi : Rp. {{ number_format($item->realisasi, 0, ',', '.') }} | Tahun : {{ $item->tahun }}</option>
                                                @else
                                                    <option value="{{ $item->id }}">{!! $item->sasaranstrategis->sasaran_strategis !!} | Alokasi : Rp. {{ number_format($item->alokasi, 0, ',', '.') }} | Realisasi : Rp. {{ number_format($item->realisasi, 0, ',', '.') }} | Tahun : {{ $item->tahun }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                
                                        @error('realisasianggaran')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-capaiankinerjaorganisasi-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-capaiankinerja-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-capaiankinerja" role="tabpanel" aria-labelledby="pills-capaiankinerja-tab">
                                    <div class="form-group">
                                        <label for="capaianspbe">Capaian SPBE</label>
                                        <select class="selectpicker @error('capaianspbe') is-invalid @enderror" name="capaianspbe" id="capaianspbe" data-show-subtext="true" data-live-search="true">
                                            <option value="" disabled selected>Pilih Capaian SPBE</option>
                                            @foreach ($capaianspbe as $item)
                                                <option value="{{ $item->id }}" {{ $model_kinerja->capaianspbe_id == $item->id ? 'selected' : '' }}>Tahun : {{ $item->tahun }} | Indeks {{ $item->indeks }}</option>
                                            @endforeach
                                        </select>
                                
                                        @error('capaianspbe')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="capaiankinerja">Capaian Kinerja Lainnya</label>
                                        <select class="selectpicker @error('capaiankinerja') is-invalid @enderror" name="capaiankinerja[]" id="capaiankinerja" data-show-subtext="true" data-live-search="true" multiple>
                                            <option value="" disabled>Pilih Capaian Kinerja</option>
                                            @foreach ($capaiankinerja as $item)
                                                @if (in_array($item->id, $capaiankinerja_id))
                                                    <option value="{{ $item->id }}" selected>{!! $item->sasaranstrategis->sasaran_strategis !!} | Target : {{ $item->sasaranstrategis->targetkinerja->target }} | Realisasi : {{ $item->realisasi }} | Tahun : {{ $item->tahun }}</option>
                                                @else
                                                    <option value="{{ $item->id }}">{!! $item->sasaranstrategis->sasaran_strategis !!} | Target : {{ $item->sasaranstrategis->targetkinerja->target }} | Realisasi : {{ $item->realisasi }} | Tahun : {{ $item->tahun }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                
                                        @error('capaiankinerja')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="row">
                                        <button type="button" class="btn btn-primary btn-prev col mr-2" data-to="#pills-realisasianggaran-tab">Kembali</button>
                                        <button type="submit" class="btn btn-success col">Simpan</button>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </form>
                {{-- <form action="">
                    <div class="form-group">
                        <label for="pengantar">Pengantar :</label>
                        <textarea class="form-control @error('pengantar') is-invalid @enderror" name="pengantar" placeholder="Pengantar" id="pengantar"></textarea>
                
                        @error('pengantar')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </form> --}}
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')

<script>
    $(document).ready(function () {
        $('.selecpicker').selectpicker();

        CKEDITOR.replace('pengantar');
        CKEDITOR.replace('visi');
        CKEDITOR.replace('misi');
        CKEDITOR.replace('corevaluesasn');
        CKEDITOR.replace('nilainilaianri');
        CKEDITOR.replace('latarbelakang');
        CKEDITOR.replace('tugas');
        CKEDITOR.replace('fungsi');
        CKEDITOR.replace('kewenangan');
        CKEDITOR.replace('peranstrategis');
        CKEDITOR.replace('sistematikapenyajian');

        $('.btn-next').on('click', function () {
            const n = $(this).attr('data-to');
            $(n).trigger('click').delay(1000);
        });

        $('.btn-prev').on('click', function () {
            const n = $(this).attr('data-to');
            $(n).trigger('click');
        });
    });

</script>


@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection