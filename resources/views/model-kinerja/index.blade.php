@extends('layouts.dashboard')

@section('title')
Model Kinerja
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Model Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Model Kinerja</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Model Kinerja</h2>
        <p class="section-lead">Halaman untuk mengelola data Model kinerja.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Model Kinerja</h4>
                <div class="card-header-action">
                    <a href="{{ route('model-kinerja.create') }}" class="btn btn-primary">Tambah Model Kinerja</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table-model-kinerja">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Judul</th>
                                <th>Tahun</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('model-kinerja.store') }}" method="POST" class="modal-part" id="modal-Model-kinerja-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="Model_kinerja">Model Kinerja</label>
        <textarea class="form-control @error('Model_kinerja') is-invalid @enderror" name="Model_kinerja" placeholder="Model Kinerja" id="Modelkinerja"></textarea>

        @error('Model_kinerja')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-Model-kinerja">
            Simpan
        </button>
    </div>
</form>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('Modelkinerja');

        $('#table-model-kinerja').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/model-kinerja",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'judul', name: 'judul'},
                {data: 'tahun', name: 'tahun'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Model Kinerja berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Model Kinerja berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Model Kinerja Tidak Dapat Dihapus!, Karena Sudah Terikat Dengan Data Lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Model Kinerja berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection