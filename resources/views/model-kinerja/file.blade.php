<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ $model_kinerja->judul }}</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <style>
        /** Define the margins of your page **/
        @page {
            margin: 100px 40px;
        }

        .page-break {
            page-break-after: always;
        }

        main{
            margin-left: 60px;
            margin-right: 60px;
        }

        header {
            position: fixed;
            top: -70px;
            left: 0px;
            right: 0px;
            height: 40px;

            /** Extra personal styles **/
            color: #03a9f4;
            text-align: left;
            line-height: 25px;
            font-size: 14px;
            margin-bottom: 10px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            border-top: 1px solid #03a9f4;
            color: black;
            display: flex;
            justify-content: space-between;
        }

        .left {
            float: left;
        }

        .right {
            float: right;
        }

        .bg-blue {
            background-color: #03a9f4;
            color: white;
            padding: 25px 30px;
            display: flex;
            align-items: center;
            border-radius: 15px;
            justify-content: center;
            text-align: center
        }
    </style>
</head>

<body>
    <header>
        {{ $model_kinerja->judul }}
    </header>

    <main>
        <section class="pengantar">
            <h3 class="bg-blue">Pengantar</h3>
            <p>{!! $model_kinerja->pengantar !!}</p>
            <div class="page-break"></div>
        </section>

        <section class="visi-misi">
            <h3 class="bg-blue">Visi</h3>
            <p>{!! $model_kinerja->visimisi->visi !!}</p>
            <h3 class="bg-blue">Misi</h3>
            <p>{!! $model_kinerja->visimisi->misi !!}</p>
            <div class="page-break"></div>
        </section>

        <section class="corevalueasn">
            <h3 class="bg-blue">Core Value ASN</h3>
            <p>{!! $model_kinerja->core_values_asn !!}</p>
            <div class="page-break"></div>
        </section>

        <section class="nilainilaianri">
            <h3 class="bg-blue">Nilai Nilai ANRI</h3>
            <p>{!! $model_kinerja->nilai_nilai_anri !!}</p>
            <div class="page-break"></div>
        </section>

        <section class="latarbelakang">
            <h3 class="bg-blue">Latar Belakang</h3>
            <p>{!! $model_kinerja->latar_belakang !!}</p>
            <div class="page-break"></div>
        </section>

        <section class="tugasfungsi">
            <h3 class="bg-blue">Tugas, Fungsi dan Struktur Organisasi</h3>
            <p>{!! $model_kinerja->tugasfungsi->tugas !!}</p>
            <p>{!! $model_kinerja->tugasfungsi->fungsi !!}</p>
            <center>
                <img src="{{ asset('uploads/strukturorganisasi/'.$model_kinerja->strukturorganisasi->file) }}" alt="" width="500px">
            </center>
            <div class="page-break"></div>
        </section>

        <section class="peranstrategis">
            <h3 class="bg-blue">Peran Strategis</h3>
            <p>{!! $model_kinerja->peranstrategis->peran_strategis !!}</p>
            <div class="page-break"></div>
        </section>

        <section class="sistematikapenyajian">
            <h3 class="bg-blue">Sistematika Penyajian</h3>
            <p>{!! $model_kinerja->sistematika_penyajian !!}</p>
            <div class="page-break"></div>
        </section>

        <section class="rencanastrategis">
            <h3 class="bg-blue">Rencana Strategis</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 14px">
                            Tujuan dan Sasaran Strategis
                        </th>
                        <th style="font-size: 14px">Indikator Kinerja</th>
                        <th style="font-size: 14px" width="30%">Target Kinerja {{ $model_kinerja->rencanastrategis->tahun }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($model_kinerja->rencanastrategis->tujuan as $tujuan)
                        <tr>
                            <td>
                                <b style="font-size: 14px">Tujuan ANRI :</b>
                                {!! $tujuan->tujuan !!}
                            </td>
                            <td>{!! $tujuan->indikatorkinerja->content !!}</td>
                            <td>{!! $tujuan->targetkinerja->target !!}</td>
                        </tr>
                    @endforeach
                    @foreach ($model_kinerja->rencanastrategis->sasaranstrategis as $sasaran)
                        <tr>
                            <td>
                                <b style="font-size: 14px">Sasaran Strategis {{ $no++ }} :</b>
                                {!! $sasaran->sasaran_strategis !!}
                            </td>
                            <td>{!! $sasaran->indikatorkinerja->content !!}</td>
                            <td>{!! $sasaran->targetkinerja->target !!}</td>
                        </tr>
                    @endforeach
                <tbody>
                </tbody>
            </table>
            <div class="page-break"></div>
        </section>

        <section class="perjanjiankinerja">
            <h3 class="bg-blue">Perjanjian Kinerja</h3>
            <div class="table-responsive">
                <table class="table table-bordered" id="table-rencana-strategis">
                    <thead>
                        <tr>
                            <th style="font-size: 14px">
                                Tujuan dan Sasaran Strategis
                            </th style="font-size: 14px">
                            <th style="font-size: 14px">Indikator Kinerja</th>
                            <th style="font-size: 14px" width="30%">Target Kinerja {{ $model_kinerja->perjanjiankinerja->tahun }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($model_kinerja->perjanjiankinerja->tujuan as $tujuan)
                            <tr>
                                <td>
                                    <b>Tujuan ANRI :</b>
                                    {!! $tujuan->tujuan !!}
                                </td>
                                <td>{!! $tujuan->indikatorkinerja->content !!}</td>
                                <td>{!! $tujuan->targetkinerja->target !!}</td>
                            </tr>
                        @endforeach
                        @foreach ($model_kinerja->perjanjiankinerja->sasaranstrategis as $sasaran)
                            <tr>
                                <td>
                                    <b>Sasaran Strategis {{ $no++ }} :</b>
                                    {!! $sasaran->sasaran_strategis !!}
                                </td>
                                <td>{!! $sasaran->indikatorkinerja->content !!}</td>
                                <td>{!! $sasaran->targetkinerja->target !!}</td>
                            </tr>
                        @endforeach
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="page-break"></div>
        </section>

        <section class="capaiankinerjaorganisasi">
            <h3 class="bg-blue">Capaian Kinerja Organisasi</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 14px">
                            No
                        </th>
                        <th style="font-size: 14px">
                            Sasaran Strategis
                        </th>
                        <th style="font-size: 14px">Indikator Kinerja</th>
                        <th style="font-size: 14px">Target</th>
                        <th style="font-size: 14px">Realisasi</th>
                        <th style="font-size: 14px">Persentase</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($model_kinerja->capaiankinerja as $capaiankinerja)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>
                                {!! $capaiankinerja->sasaranstrategis->sasaran_strategis !!}
                            </td>
                            <td>{!! $capaiankinerja->sasaranstrategis->indikatorkinerja->content !!}</td>
                            <td>
                                {!! $capaiankinerja->sasaranstrategis->targetkinerja->target !!}
                            </td>
                            <td>
                                {!! $capaiankinerja->realisasi !!}
                            </td>
                            <td>
                                {!! $capaiankinerja->persentase !!}
                            </td>
                        </tr>
                    @endforeach
                <tbody>
                </tbody>
            </table>
            <div class="page-break"></div>
        </section>

        <section class="realisasianggaran">
            <h3 class="bg-blue">Realisasi Anggaran</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 14px">
                            No
                        </th>
                        <th style="font-size: 14px">
                            Sasaran Strategis
                        </th>
                        <th style="font-size: 14px">Indikator Kinerja</th>
                        <th style="font-size: 14px">Alokasi</th>
                        <th style="font-size: 14px">Realisasi</th>
                        <th style="font-size: 14px">Persentase</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($model_kinerja->realisasianggaran as $realisasianggaran)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>
                                {!! $realisasianggaran->sasaranstrategis->sasaran_strategis !!}
                            </td>
                            <td>{!! $realisasianggaran->sasaranstrategis->indikatorkinerja->content !!}</td>
                            <td>
                                {{ 'Rp. '.number_format($realisasianggaran->alokasi, 0, ',', '.') }}
                            </td>
                            <td>
                                {{ 'Rp. '.number_format($realisasianggaran->realisasi, 0, ',', '.') }}
                            </td>
                            <td>
                                {{ $realisasianggaran->persentase }}
                            </td>
                        </tr>
                    @endforeach
                <tbody>
                </tbody>
            </table>
            <div class="page-break"></div>
        </section>

        <section class="capaiankinerjalainnya">
            <h3 class="bg-blue">Capaian Kinerja Lainnya</h3>
            <ul>
                <li>
                    {{ $model_kinerja->capaianspbe->judul }}
                    <br><br>
                    {!! $model_kinerja->capaianspbe->deskripsi !!}
                </li>
                @foreach ($model_kinerja->capaian as $capaian)
                    <li>
                       {{ $capaian->judul }}
                       <br><br>
                          {!! $capaian->deskripsi !!}
                    </li>
                @endforeach
            </ul>
            <div class="page-break"></div>
        </section>
    </main>

    {{-- <script type="text/php">
        if (isset($pdf) ) {
            $font = $fontMetrics->getFont("helvetica", "bold");
            $pdf->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
            $pdf->page_text(500, 18, "Footer: {PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
        }
    </script> --}}
</body>

</html>