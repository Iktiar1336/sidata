@extends('layouts.front')

@section('title')
SIDATA - Sistem Informasi Pengumpulan Data
@endsection

@section('header')
    @include('landing-page.shared.navbar-fixed')
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/css/slick.min.css') }}"  />
<link rel="stylesheet" href="{{ asset('assets/css/slick-theme.min.css') }}" />
<style>
    .input-group{
        width: 100%;
        margin-top: 20px;
        border: 1px solid #ced4da;
        border-radius: 50px;
    }
    #input-search{
        border: none;
        height: 55px;
        border-top-left-radius: 50px;
        border-right: 1px solid #ced4da;
        border-bottom-left-radius: 50px;
        padding-left: 20px; 
    }
    #input-search:focus {
        
    }
    #input-search::placeholder{
        padding-left: 5px; 
    }
    #inputGroupSelect01{
        border-radius: 0;
        height: 55px;
    }
    #inputGroupSelect01:focus {
        outline: none;
        box-shadow: none;
    }
    #btn-search{
        border: none;
        height: 55px;
        border-top-right-radius: 50px;
        border-bottom-right-radius: 50px;
        background-color: #099cf4;
        padding-right: 30px;
        padding-left: 30px;
        justify-content: center;
        display: flex;
        align-items: center;
        color: #fff;
        font-size: 20px;
    }
    .nav-tabs{
        display: flex !important;
        justify-content: center;
        align-items: center;
        width: 100%;
    }

    .nav-tabs .nav-item{
        width: 50%;
    }

    .nav-tabs .nav-link{
        width: 100%;
    }

    .nav-tabs .nav-link.active{
        background-color: #099cf4 !important;
        color: #fff !important;
        border: 1px solid #099cf4 !important;
    }

    .btn-round{
        border-radius: 50px;
    }
</style>
@endsection

@section('content')
<!-- Preloader Start -->
{{-- <div class="se-pre-con"></div> --}}
<!-- Preloader Ends -->
<!-- Start Banner 
    ============================================= -->
<div class="banner-area thumb-pos pad-responsive text-combo shape-top">
    <div class="item">
        <div class="box-table">
            <div class="box-cell">
                <div class="container">
                    <div class="row">
                        <div class="double-items">
                            <div class="col-lg-6 info">
                                <h2 class="wow fadeInDown" data-wow-duration="1s" style="color: #099cf4;">
                                    <span>Selamat Datang Di </span>
                                    Sistem Informasi Pengumpulan Data 
                                </h2>
                                <p class="wow fadeInLeft" data-wow-duration="1.5s" style="text-transform: capitalize">
                                    Sebuah sistem informasi yang
                                    dibuat untuk memudahkan pengumpulan data di lingkungan Arsip Nasional Republik
                                    Indonesia.
                                </p>
                                <form action="{{ route('search') }}" method="POST">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search" placeholder="Masukan kata kunci anda" aria-label="search" aria-describedby="button-addon4" id="input-search" required>
                                        <div class="input-group-append" id="button-addon4">
                                            <select class="custom-select" id="inputGroupSelect01" name="category" required>
                                                <option value="infografis">Infografis</option>
                                                <option value="news">Berita</option>
                                                {{-- <option value="arsip">Arsip Publik</option> --}}
                                            </select>
                                            <button class="btn btn-outline-secondary" id="btn-search" type="submit">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                {{-- <div class="main-search-input">
                                    <div class="main-search-input-item">
                                        <input type="text" placeholder="Masukan kata kunci anda" value="">
                                    </div>
                                    <button class="button"
                                        onclick="window.location.href='listings-half-screen-map-list.html'">Search</button>
                                </div> --}}

                            </div>
                            <div class="col-lg-6 thumb">
                                <img class="wow fadeInRight" src="{{ asset('assets/img/banner.png') }}" width="550px" alt="Thumb">
                                {{-- <img class="absolute wow fadeInUp" data-wow-duration="2.5s"
                                    src="assets/img/illustration/17.png" alt="Thumb"> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Banner -->

<!-- Start Waves
    ============================================= -->
<div class="services-area text-center carousel-shadow wavesshape-bottom default-padding-top" style="margin-top: 60px !important;">
    <div class="waveshape">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
        </svg>
    </div>
</div>
<!-- End Waves -->

<!-- Start Statistic Data
    ============================================= -->
<div class="fun-factor-area bg-gray default-padding">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(assets/img/map.svg);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="client-items text-center">
            <div class="row">
                <div class="col-lg-3 col-md-6 item">
                    <div class="fun-fact">
                        <div class="timer" data-to="{{ DB::table('work_units')->count() }}" data-speed="5000">{{ DB::table('work_units')->count() }}</div>
                        <span class="medium">Unit Kerja</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 item">
                    <div class="fun-fact">
                        <div class="timer" data-to="{{ DB::table('infografis')->count() }}" data-speed="5000">{{ DB::table('infografis')->count() }}</div>
                        <span class="medium">Infografis</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 item">
                    <div class="fun-fact">
                        @php
                            $category = DB::table('categories')->whereNull('parent_id')->where('status', 1)->get();
                            $totalpenyajianinformasi = 0;
                            foreach ($category as $key => $value) {
                                $kinerja = DB::table('kinerja')->where('category_id', $value->id)->count();
                                $totalpenyajianinformasi += $kinerja;
                            }
                            $totalinfografis = DB::table('infografis')->count();
                            $totalpenyajianinformasi += $totalinfografis;
                            $totalberita = DB::table('news')->count();
                            $totalpenyajianinformasi += $totalberita;
                            $totalarsip = DB::table('arsip_kolektor')->count();
                            $totalpenyajianinformasi += $totalarsip;
                        @endphp
                        <div class="timer" data-to="{{ $totalpenyajianinformasi }}" data-speed="5000">
                            {{ $totalpenyajianinformasi }}
                        </div>
                        <span class="medium">Penyajian Informasi</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 item">
                    <div class="fun-fact">
                        <div class="timer" data-to="{{ DB::table('kinerja')->where('status', 4)->count() }}" data-speed="5000">{{ DB::table('kinerja')->where('status', 4)->count() }}</div>
                        <span class="medium">Data Terkumpul</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Statistic Data -->

<div class="work-process-area default-padding-bottom default-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="site-heading text-center">
                    <h4>Arsip Publik</h4>
                    <h2>
                        Arsip Publik Terbaru
                    </h2>
                </div>
            </div>
        </div>
        <div class="works-process-items text-center">
            <div class="row">
                @foreach ($arsipdigital as $arsip)
                    <div class="col-lg-4 single-item">
                        <div class="item">
                            <div class="icon">
                                <img src="{{ $arsip->GetimageByType($arsip->type) }}" alt="Thumb">
                            </div>
                            <div class="info mt-3">
                                <h4>{{ $arsip->file }}</h4>
                                <button class="btn-simple" type="button" id="btn-detail-arsip" data-id="{{ Crypt::encrypt($arsip->id) }}"><i class="fas fa-angle-right"></i> Lihat Detail</button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<!-- Start HighLight
    ============================================= -->
<div class="blog-area bg-gray default-padding bottom-less mt-5">
    <div class="container">
        <div class="heading-left">
            <div class="row">
                <div class="col-lg-5">
                    <h2>
                        Informasi Terbaru
                    </h2>
                </div>
                <div class="col-lg-6 offset-lg-1">
                    <p>
                        Informasi terbaru berisi informasi terbaru berupa berita dan infografis yang diunggah pada website ini.
                    </p>
                </div>
            </div>
        </div>
        <div class="blog-items">
            <div class="row" id="slide-highlight">
                @foreach ($news as $berita)
                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 single-item">
                        <div class="item">
                            <div class="thumb">
                                <div>
                                    <img src="{{ asset('images/news/' . $berita->image) }}" alt="Thumb" width="100%" height="250px">
                                </div>
                            </div>
                            <div class="info">
                                <div class="meta">
                                    <ul>
                                        <li>
                                            <i class="fas fa-history"></i>
                                        </li>
                                        <li>
                                            @php
                                            $tanggal = \Carbon\Carbon::parse($berita->created_at);
                                            $waktu = $tanggal->format('H:i:s');
                                            @endphp
                                            {{ $tanggal->isoFormat('D MMMM Y') }}
                                        </li>
                                        <li>
                                            <i class="fas fa-eye"></i> {{ views($berita)->count() }}
                                        </li>
                                        {{-- <li>
                                                <i class="fas fa-user"></i><a href="#"> {{ $berita->user->name }}</a>
                                        </li> --}}
                                    </ul>
                                </div>
                                <h4>
                                    <a href="{{ route('detail-news', $berita->slug) }}">{{ Illuminate\Support\Str::limit($berita->title, 20) }}</a>
                                </h4>
                                <p>
                                    @php
                                    $berita->content = strip_tags($berita->content);
                                    echo Illuminate\Support\Str::limit($berita->content, 30);
                                    @endphp
                                </p>
                                <a class="btn-simple" href="{{ route('detail-news', $berita->slug) }}"><i class="fas fa-angle-right"></i> Lihat Detail</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                @endforeach
                @foreach ($infografis as $item)
                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 single-item">
                        <div class="item">
                            <div class="thumb">
                                <center>
                                    <div class="justify-content-center">
                                        <img src="{{ asset('images/infografis/' . $item->image) }}" alt="Thumb" width="100%" height="250px">
                                    </div>
                                </center>
                            </div>
                            <div class="info">
                                <div class="meta">
                                    <ul>
                                        <li>
                                            <i class="fas fa-history"></i>
                                        </li>
                                        <li>
                                            @php
                                            $tanggal = \Carbon\Carbon::parse($item->created_at);
                                            $waktu = $tanggal->format('H:i:s');
                                            @endphp
                                            {{ $tanggal->isoFormat('D MMMM Y') }}
                                        </li>
                                        <li>
                                            <i class="fas fa-eye"></i> {{ views($item)->count() }}
                                        </li>
                                    </ul>
                                </div>
                                <h4>
                                    <a href="{{ route('detail-infografis', $item->slug) }}">{{ Illuminate\Support\Str::limit($item->title, 20) }}</a>
                                </h4>
                                <p>
                                    @php
                                        $item->content = strip_tags($item->content);
                                        echo Illuminate\Support\Str::limit($item->content, 30);
                                    @endphp
                                </p>
                                <a class="btn-simple" href="{{ route('detail-infografis', $item->slug) }}"><i class="fas fa-angle-right"></i> Lihat Detail</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- End HighLight -->

<div class="modal fade" id="modalDetailArsip" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalDetailArsipLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalDetailArsipLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/js/slick.min.js') }}"></script>

<script>

    $(document).ready(function () {
        $('#slide-highlight').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        $('#btn-detail-arsip').click(function () {
            var id = $(this).data('id');
            $.ajax({
                url: "/get-detail-arsip/"+id,
                type: "GET",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function (data) {
                    console.log(data);
                    $('.modal-body').empty();
                    var html = `
                    <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-detail-tab" data-toggle="pill" data-target="#pills-detail" type="button" role="tab" aria-controls="pills-detail" aria-selected="true">Detail</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-metadata-tab" data-toggle="pill" data-target="#pills-metadata" type="button" role="tab" aria-controls="pills-metadata" aria-selected="false">Metadata</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-detail" role="tabpanel" aria-labelledby="pills-detail-tab">
                            <table class="table">
                                <tr>
                                    <th>Nama File</th>
                                    <td> : </td>
                                    <td id="nama_file"></td>
                                </tr>
                                <tr>
                                    <th>Ukuran File</th>
                                    <td> : </td>
                                    <td id="ukuran_file"></td>
                                </tr>
                                <tr>
                                    <th>Format File</th>
                                    <td> : </td>
                                    <td id="format_file"></td>
                                </tr>
                                <tr>
                                    <th>Di Upload Pada</th>
                                    <td> : </td>
                                    <td id="tgl_upload"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="pills-metadata" role="tabpanel" aria-labelledby="pills-metadata-tab">
                            <table class="table">
                                <tr>
                                    <th>Nama Instansi</th>
                                    <td>:</td>
                                    <td id="namaInstansi"></td>
                                </tr>
                                </tr>
                                <tr>
                                    <th>Uraian Item</th>
                                    <td>:</td>
                                    <td id="uraianItem"></td>
                                </tr>
                                <tr>
                                    <th>No. Surat</th>
                                    <td>:</td>
                                    <td id="noSurat"></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Surat</th>
                                    <td>:</td>
                                    <td id="tanggalSurat"></td>
                                </tr>
                                <tr>
                                    <th>Kode Klasifikasi</th>
                                    <td>:</td>
                                    <td id="kodeKlasifikasi"></td>
                                </tr>
                                <tr>
                                    <th>Tingkat Perkembangan</th>
                                    <td>:</td>
                                    <td id="tingkatPerkembangan"></td>
                                </tr>
                                <tr>
                                    <th>Media Arsip</th>
                                    <td>:</td>
                                    <td id="mediaArsip"></td>
                                </tr>
                                <tr>
                                    <th>Kondisi</th>
                                    <td>:</td>
                                    <td id="kondisi"></td>
                                </tr>
                                <tr>
                                    <th>Jumlah</th>
                                    <td>:</td>
                                    <td id="jumlah"></td>
                                </tr>
                                <tr>
                                    <th>Keterangan</th>
                                    <td>:</td>
                                    <td id="keterangan"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    `;
                    $('.modal-body').append(html);
                    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                    var tanggal = new Date(data.arsipdigital.created_at);
                    var tanggal_surat = new Date(data.arsipkolektor.tanggal_surat);
                    var yhari = tanggal_surat.getDay();
                    var ytanggal = tanggal_surat.getDate();
                    var ybulan = tanggal_surat.getMonth();
                    var ytahun = tanggal_surat.getFullYear();
                    var tgl_surat = hari[yhari] + ', ' + ytanggal + ' ' + bulan[ybulan] + ' ' + ytahun;
                    var xhari = tanggal.getDay();
                    var xtanggal = tanggal.getDate();
                    var xbulan = tanggal.getMonth();
                    var xtahun = tanggal.getFullYear();
                    var jam = tanggal.getHours();
                    var menit = tanggal.getMinutes().toString().padStart(2, '0');
                    var detik = tanggal.getSeconds().toString().padStart(2, '0');
                    var tgl = hari[xhari] + ', ' + xtanggal + ' ' + bulan[xbulan] + ' ' + xtahun + ' ' + jam + ':' + menit + ':' + detik;
                    $('.modal-title').text(data.arsipdigital.file);
                    $('#nama_file').text(data.arsipdigital.file);
                    $('#ukuran_file').text(data.size);
                    $('#format_file').text(data.arsipdigital.type);
                    $('#namaInstansi').text(data.instansi);
                    $('#uraianItem').text(data.arsipkolektor.uraianitem);
                    $('#noSurat').text(data.arsipkolektor.nomor_surat);
                    $('#tanggalSurat').text(tgl_surat);
                    $('#kodeKlasifikasi').text(data.arsipkolektor.kode_klasifikasi);
                    $('#tingkatPerkembangan').text(data.arsipkolektor.tingkat_perkembangan);
                    $('#mediaArsip').text(data.arsipkolektor.media_arsip);
                    $('#kondisi').text(data.arsipkolektor.kondisi);
                    $('#jumlah').text(data.arsipkolektor.jumlah);
                    $('#keterangan').text(data.arsipkolektor.keterangan);
                    $('#tgl_upload').text(tgl);
                    $('#modalDetailArsip').modal('show');
                }
            });
        });
    });
</script>

@endsection