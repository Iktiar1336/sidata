@extends('layouts.dashboard')

@section('title')
Dashboard
@endsection

@section('css')
    <style>
        .alert-bg-random{
            background-image: url(https://source.unsplash.com/featured/900x80);
        }
    </style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Dashboard</h1>
    </div>
    <div class="section-body">
        <h2 class="section-title">Selamat Datang {{ auth()->user()->name }}</h2>
        <p class="section-lead">
            Anda login sebagai {{ auth()->user()->roles->first()->description }}
            @if (auth()->user()->workunit_id != null)
            dari Unit Kerja {{
        auth()->user()->workunit->name }}
            @elseif(auth()->user()->workunit_id == null && auth()->user()->instansi_id != null)
            dari Instansi {{ auth()->user()->instansi->nama_instansi }}
            @endif
        </p>
        <div class="row">
            <div class="col-12 mb-4">
                <div class="hero text-white hero-bg-image hero-bg-parallax" style="background-image: url('{{ asset('admin/stisla/assets/img/unsplash/andre-benz-1214056-unsplash.jpg') }}');">
                    <div class="hero-inner">
                        <h2>Selamat Datang , {{ auth()->user()->name }}!</h2>
                        <p class="lead">
                            Anda login sebagai {{ auth()->user()->roles->first()->description }}
                            @if (auth()->user()->workunit_id != null)
                            dari Unit Kerja {{
                auth()->user()->workunit->name }} <br>
                            Terima kasih anda telah membantu kami dalam mengumpulkan data unit kerja {{ auth()->user()->workunit->name
                }}
                            @elseif(auth()->user()->workunit_id == null && auth()->user()->instansi_id != null)
                            dari Instansi {{ auth()->user()->instansi->nama_instansi }}
                            @endif
                            <br>
                            ~ Pusat Data dan Informasi ~ <br>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        @can('Statistik Produsen Data')
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-database"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Data Terkumpul</h4>
                        </div>
                        <div class="card-body">
                            @php
                            $dataterkumpul = DB::table('kinerja')->where('produsendata_id',
                            auth()->user()->id)->where('workunit_id',
                            auth()->user()->workunit_id)->get()->groupBy(['category_id', 'year']);
                            @endphp
                            {{ $dataterkumpul->count() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-check-double"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Data Terverifikasi</h4>
                        </div>
                        <div class="card-body">
                            @php
                            $dataterverifikasi = DB::table('kinerja')->where('produsendata_id',
                            auth()->user()->id)->where('workunit_id',
                            auth()->user()->workunit_id)->where('status', 4)->get()->groupBy(['category_id', 'year']);
                            @endphp
                            {{ $dataterverifikasi->count() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-history"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Data Sedang Di Proses</h4>
                        </div>
                        <div class="card-body">
                            @php
                            $datasedangproses = DB::table('kinerja')->where('produsendata_id',
                            auth()->user()->id)->where('workunit_id',
                            auth()->user()->workunit_id)->where('status', 3)->get()->groupBy(['category_id', 'year']);
                            @endphp
                            {{ $dataterverifikasi->count() }}
                            {{-- {{ \DB::table('kinerja')->where('produsendata_id', auth()->user()->id)->where('workunit_id',
                auth()->user()->workunit_id)->where('status', 4)->count() }} --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-info">
                        <i class="fas fa-clipboard-list"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Aktivitas Input Data</h4>
                        </div>
                        <div class="card-body">
                            <div class="progress mt-3" style="height: 12px; border-radius: 50px">
                                @php
                                if(\DB::table('kinerja')->where('produsendata_id', auth()->user()->id)->where('workunit_id',
                                auth()->user()->workunit_id)->count() > 0){
                                $total = \DB::table('kinerja')->where('produsendata_id', auth()->user()->id)->where('workunit_id',
                                auth()->user()->workunit_id)->count() / \DB::table('kinerja')->where('workunit_id',
                                auth()->user()->workunit_id)->count() * 100;
                                } else {
                                $total = 0;
                                }
                                @endphp
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" data-width="{{ $total }}%" aria-valuenow="{{ $total }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $total }}%;">{{
                    $total }}%</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endcan

        @can('Statistik Master Data')
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-success">
                            <i class="fas fa-users"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total Penugasan</h4>
                            </div>
                            <div class="card-body">
                                @php
                                $data = \DB::table('kinerja')
                                ->where('status', 3)
                                ->where('masterdata_id', auth()->user()->id)
                                ->get()->groupBy(['category_id', 'year']);
                                @endphp
                                {{ $data->count() }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-warning">
                            <i class="fas fa-history"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total Pending Penugasan</h4>
                            </div>
                            <div class="card-body">
                                @php
                                $data = \DB::table('kinerja')
                                ->where('status', 2)
                                ->get()->groupBy(['category_id', 'year']);
                                @endphp
                                {{ $data->count() }}
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        @endcan

    @can('Statistik Pengolah Data')
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Penugasan</h4>
                    </div>
                    <div class="card-body">
                        @php
                        $data = \DB::table('kinerja')
                        ->where('status', 4)
                        ->where('pengolahdata_id', auth()->user()->id)
                        ->get()->groupBy(['category_id', 'year']);
                        @endphp
                        {{ $data->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-history"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Pending Penugasan</h4>
                    </div>
                    <div class="card-body">
                        @php
                        $data = \DB::table('kinerja')
                        ->where('status', 3)
                        ->where('pengolahdata_id', auth()->user()->id)
                        ->get()->groupBy(['category_id', 'year']);
                        @endphp
                        {{ $data->count() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan

    @if (auth()->user()->phone == null)
    <div class="row">
        <div class="col-12 mb-4">
            <div class="alert alert-danger alert-has-icon alert-dismissible show fade">
                <div class="alert-icon"><i class="fas fa-exclamation-triangle"></i></div>
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    <div class="alert-title">Perhatian !</div>
                    <p class="mb-0">
                        Silahkan Lengkapi Data Diri Anda Terlebih Dahulu Sebelum Melakukan Pengisian Data
                    </p>
                    <a href="{{ route('user.profile') }}" class="btn btn-outline-light mt-3 btn-lg btn-icon icon-left"><i class="fas fa-pen"></i> Lengkapi Data Diri</a>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if (auth()->user()->roles->first()->name == 'Instansi')
        @foreach ($survey as $sy)
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="hero text-white hero-bg-image hero-bg-parallax" style="background-image: url(https://source.unsplash.com/featured/900x201);">
                        <div class="hero-inner">
                            <h2>
                                Perhatian!
                            </h2>
                            <p class="lead text-capitalize">
                                Terdapat Instrumen Survey Baru Yang Harus Anda Isi, Dengan Judul {{ $sy->nama }} Tahun {{ $sy->tahun }} <br>
                                Silahkan Klik Tombol Dibawah Ini Untuk Melakukan Pengisian
                            </p>
                            <div class="mt-4">
                                <a href="{{ route('arsip-kolektor.show', Crypt::encrypt($sy->id)) }}" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="fas fa-pen"></i> Isi Survey</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
    </div>
</section>
@endsection

@section('js')

@endsection