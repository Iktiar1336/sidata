{{-- @extends('errors::minimal')

@section('title', __('Service Unavailable'))
@section('code', '503')
@section('message', __('Service Unavailable')) --}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WE ARE COMING SOON</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            background: #000;
        }

        * {
            box-sizing: border-box;
        }

        .maintenance {
            /* background-image: url(https://demo.wpbeaveraddons.com/wp-content/uploads/2018/02/main-1.jpg); */
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' width='1440' height='560' preserveAspectRatio='none' viewBox='0 0 1440 560'%3e%3cg mask='url(%26quot%3b%23SvgjsMask1034%26quot%3b)' fill='none'%3e%3crect width='1440' height='560' x='0' y='0' fill='rgba(9%2c 156%2c 244%2c 1)'%3e%3c/rect%3e%3cpath d='M0%2c529.896C97.668%2c526.17%2c174.668%2c452.029%2c252.547%2c392.971C324.913%2c338.094%2c399.905%2c282.457%2c436.123%2c199.171C472.019%2c116.625%2c480.624%2c20.29%2c451.675%2c-64.941C424.67%2c-144.451%2c335.466%2c-179.805%2c284.689%2c-246.684C228.904%2c-320.159%2c226.598%2c-444.809%2c139.716%2c-475.828C53.728%2c-506.528%2c-29.109%2c-422.08%2c-114.256%2c-389.119C-191.249%2c-359.315%2c-280.447%2c-352.199%2c-336.422%2c-291.512C-393.011%2c-230.16%2c-411.498%2c-143.295%2c-417.761%2c-60.065C-423.741%2c19.403%2c-395.577%2c93.636%2c-371.11%2c169.48C-343.4%2c255.377%2c-329.99%2c349.845%2c-265.243%2c412.726C-194.471%2c481.458%2c-98.583%2c533.657%2c0%2c529.896' fill='%230780c8'%3e%3c/path%3e%3cpath d='M1440 1126.212C1544.242 1134.604 1613.587 1015.971 1682.4569999999999 937.271 1736.846 875.12 1746.559 789.267 1794.013 721.672 1856.801 632.235 2001.442 588.109 2004.27 478.87 2006.9850000000001 374.031 1882.175 315.02 1806.42 242.49599999999998 1737.68 176.68900000000002 1669.646 113.02100000000002 1582.407 75.005 1485.907 32.952999999999975 1379.785-18.644999999999982 1279.329 12.803999999999974 1179.113 44.17700000000002 1112.282 141.904 1066.629 236.473 1027.066 318.426 1060.124 413.555 1042.7440000000001 502.883 1024.055 598.94 920.997 689.442 961.1320000000001 778.691 1001.2090000000001 867.81 1142.311 842.762 1221.396 900.155 1307.9859999999999 962.995 1333.356 1117.627 1440 1126.212' fill='%2333aef8'%3e%3c/path%3e%3c/g%3e%3cdefs%3e%3cmask id='SvgjsMask1034'%3e%3crect width='1440' height='560' fill='white'%3e%3c/rect%3e%3c/mask%3e%3c/defs%3e%3c/svg%3e");
            background-repeat: no-repeat;
            background-position: center center;
            background-attachment: scroll;
            background-size: cover;
        }

        .maintenance {
            width: 100%;
            height: 100%;
            min-height: 100vh;
        }

        .maintenance {
            display: flex;
            flex-flow: column nowrap;
            justify-content: center;
            align-items: center;
        }

        .maintenance_contain {
            display: flex;
            flex-direction: column;
            flex-wrap: nowrap;
            align-items: center;
            justify-content: center;
            width: 100%;
            padding: 15px;
        }

        .maintenance_contain img {
            width: auto;
            max-width: 100%;
        }

        .pp-infobox-title-prefix {
            font-weight: 500;
            font-size: 20px;
            color: #fff;
            margin-top: 30px;
            text-align: center;
        }

        .pp-infobox-title-prefix {
            font-family: sans-serif;
        }

        .pp-infobox-title {
            color: #fff;
            font-family: sans-serif;
            font-weight: 700;
            font-size: 40px;
            margin-top: 10px;
            margin-bottom: 10px;
            text-align: center;
            display: block;
            word-break: break-word;
        }

        .pp-infobox-description {
            color: #fff;
            font-family: "Poppins", sans-serif;
            font-weight: 400;
            font-size: 18px;
            margin-top: 0px;
            margin-bottom: 0px;
            text-align: center;
        }

        .pp-infobox-description p {
            margin: 0;
        }

        .title-text.pp-primary-title {
            color: #fff;
            padding-top: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            padding-right: 0px;
            font-family: sans-serif;
            font-weight: 500;
            font-size: 18px;
            line-height: 1.4;
            margin-top: 50px;
            margin-bottom: 0px;
        }

        .pp-social-icon {
            margin-left: 10px;
            margin-right: 10px;
            display: inline-block;
            line-height: 0;
            margin-bottom: 10px;
            margin-top: 10px;
            text-align: center;
        }

        .pp-social-icon a {
            display: inline-block;
            height: 40px;
            width: 40px;
        }

        .pp-social-icon a i {
            border-radius: 100px;
            font-size: 20px;
            height: 40px;
            width: 40px;
            line-height: 40px;
            text-align: center;
        }

        .pp-social-icon:nth-child(1) a i {
            color: #4b76bd;
        }

        .pp-social-icon:nth-child(1) a i {
            border: 2px solid #4b76bd;
        }

        .pp-social-icon:nth-child(2) a i {
            color: #00c6ff;
        }

        .pp-social-icon:nth-child(2) a i {
            border: 2px solid #00c6ff;
        }

        .pp-social-icon:nth-child(3) a i {
            color: #fb5245;
        }

        .pp-social-icon:nth-child(3) a i {
            border: 2px solid #fb5245;
        }

        .pp-social-icon:nth-child(4) a i {
            color: #158acb;
        }

        .pp-social-icon:nth-child(4) a i {
            border: 2px solid #158acb;
        }

        .pp-social-icons {
            display: flex;
            flex-flow: row wrap;
            align-items: center;
            justify-content: center;
        }

        @media (max-width: 767px) {
            .maintenance {
                background-size: cover;
            }

            .maintenance_contain img {
                width: 80%;
            }

            .pp-infobox-title-prefix {
                font-size: 18px;
            }

            .pp-infobox-title {
                font-size: 30px;
            }

            .pp-infobox-description {
                font-size: 16px;
            }

            .title-text.pp-primary-title {
                font-size: 16px;
            }
        }
    </style>
</head>

<body>
    <div class="maintenance">
        <div class="maintenance_contain">
            <lottie-player src="https://assets8.lottiefiles.com/private_files/lf30_y9czxcb9.json"
                background="transparent" speed="1"
                style="width: 500px; height: 500px;margin-bottom: -100px;margin-top:-100px;" loop autoplay>
            </lottie-player>
            <span class="pp-infobox-title-prefix">WE ARE COMING SOON</span>
            <div class="pp-infobox-title-wrapper">
                <h3 class="pp-infobox-title">The website under maintenance!</h3>
            </div>
            <div class="pp-infobox-description">
                <p>This website is under system maintenance, and we will be back in 24 hours</p>
            </div>
        </div>
    </div>
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
</body>

</html>