<form action="{{ route('infografis.destroy', Crypt::encrypt($row->id)) }}" method="post" class="btn-group">
    @csrf
    @method('delete')

    @can('Edit Infografis')
        <a href="{{ route('infografis.edit', Crypt::encrypt($row->id)) }}" class="btn btn-warning btn-sm mr-2"><i class="fas fa-pen"></i></a>
    @endcan

    @can('Hapus Infografis')
        <button class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i></button>
    @endcan
</form>

<script>
    $('.btn-delete').on('click', function(e) {
        e.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: 'Apakah anda yakin?',
            text: "Infografis ini akan dihapus secara permanen!",
            icon: 'warning',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            } else {
                swal("Infografis tidak jadi dihapus!");
            }
        });
    });
</script>