@extends('layouts.dashboard')

@section('title')
Edit Infografis
@endsection

@section('content')
<div class="section">
    <div class="section-header">
        <h1>Edit Infografis</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Infografis</a></div>
            <div class="breadcrumb-item">Edit Infografis</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Infografis</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('infografis.index') }}" class="btn btn-primary">Kembali</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('infografis.update', Crypt::encrypt($infografis->id)) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">Judul</label>
                                <input type="text" name="title"
                                    class="form-control @error('title') is-invalid @enderror"
                                    value="{{ $infografis->title }}" required>
                                @error('title')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Konten</label>
                                <textarea name="content" class="form-control @error('content') is-invalid @enderror"
                                    id="content" required>{!! $infografis->content !!}</textarea>
                                @error('content')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" id="status1" value="1"
                                        {{ $infografis->status == 1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="status1">Publish</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" id="status2" value="0"
                                        {{ $infografis->status == 0 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="status2">Draft </label>
                                </div>
                                
                                @error('status')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Gambar</label>
                                <input type="file" name="image"
                                    class="form-control @error('image') is-invalid @enderror">
                                <small class="form-text text-danger">Hanya diperbolehkan dengan format jpeg,png,jpg dan
                                    maksimal 2 MB</small>
                                @error('image')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success btn-block" type="submit">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
  };
  CKEDITOR.replace('content', options);
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
                title: "Failed",
                icon: "error",
            });
</script>
@endforeach
@endif
@endsection