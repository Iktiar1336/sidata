@extends('layouts.dashboard')

@section('title')
    List Infografis
@endsection

@section('content')
<section class="section">
  <div class="section-header">
      <h1>Infografis</h1>
      <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Informasi</a></div>
          <div class="breadcrumb-item">Infografis</div>
      </div>
  </div>

  <div class="section-body">
      <h2 class="section-title">List Infografis</h2>

      <div class="row">
          <div class="col-12">
              <div class="card">
                <div class="card-header">
                    <a href="{{ route('infografis.create') }}" class="btn btn-primary">Tambah Infografis</a>
                </div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table class="table dataTable" id="table-infografis" width="100%" cellspacing="0">
                              <thead>
                                  <tr>
                                      <th>Image</th>
                                      <th>Judul</th>
                                      <th>Deskripsi</th>
                                      <th>Status</th>
                                      <th>Opsi</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-infografis').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/informasi-publik/infografis",
            columns: [
                { data: 'image', name: 'image' },
                { data: 'title', name: 'title' },
                { data: 'content', name: 'content' },
                { data: 'status', name: 'status' },
                { data: 'action', name: 'action' },
            ]
        });
    });
</script>

@if(Session::has('success'))
<script>
    swal({
        title: "Berhasil",
        text: "{{ Session::get('success') }}",
        icon: "success",
        button: "OK",
    });
</script>
@endif
@endsection
