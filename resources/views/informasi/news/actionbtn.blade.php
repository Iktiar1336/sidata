<form action="{{ route('news.destroy', Crypt::encrypt($row->id)) }}" method="post" class="btn-group">
    @csrf
    @method('delete')

    @can('Edit Berita')
        <a href="{{ route('news.edit', Crypt::encrypt($row->id)) }}" class="btn btn-warning btn-sm mr-2"><i class="fas fa-pen"></i></a>
    @endcan

    @can('Hapus Berita')
        <button class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i></button>
    @endcan
</form>

<script>
    $('.btn-delete').on('click', function(e) {
        e.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: 'Apakah anda yakin?',
            text: "Berita ini akan dihapus secara permanen!",
            icon: 'warning',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            } else {
                swal("Berita tidak jadi dihapus!");
            }
        });
    });
</script>