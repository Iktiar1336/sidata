@extends('layouts.dashboard')

@section('title')
List Berita
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Berita</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Informasi Publik</a></div>
            <div class="breadcrumb-item">Berita</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Berita</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        @can('Tambah Berita')
                            <a href="{{ route('news.create') }}" class="btn btn-primary">Tambah Berita</a>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable" id="table-berita" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Judul</th>
                                        <th>Konten</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-berita').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/informasi-publik/news",
            columns: [
                {data: 'image', name: 'image'},
                {data: 'title', name: 'title'},
                {data: 'content', name: 'content'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if(Session::has('success'))
<script>
    swal({
        title: "Berhasil",
        text: "{{ Session::get('success') }}",
        icon: "success",
        button: "OK",
    });
</script>
@endif


@endsection