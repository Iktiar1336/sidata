@extends('layouts.dashboard')

@section('title')
User Profile
@endsection

@section('css')
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Profile</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Profile</div>
        </div>
    </div>
    <div class="section-body">
        <h2 class="section-title">Hi, {{ Auth()->user()->name }}!</h2>
        <p class="section-lead">
            Ini adalah halaman profile anda.
        </p>

        <div class="row mt-sm-4">
            <div class="col-12 col-md-12 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <div class="user-item">
                            <img alt="image" id="preview-img" src="{{ Auth::user()->getAvatar() }}" class="img-fluid" style="width: 240px; height:200px">
                            <div class="user-details">
                                <div class="user-name">{{ Auth()->user()->name }}</div>
                                <div class="user-cta">
                                    @if(Cache::has('user-is-online-' . auth()->user()->id))
                                        <span class="text-success">Online</span>
                                    @else
                                        <span class="text-danger">
                                            Offline
                                            @if(auth()->user()->last_seen != null)
                                                {{ \Carbon\Carbon::parse(auth()->user()->last_seen)->diffForHumans() }}
                                            @endif
                                        </span>
                                    @endif

                                    <br>

                                    <div class="table-responsive">
                                        <table class="table table-bordered mt-3">
                                            <tbody>
                                                <tr>
                                                    <td><i class="fas fa-user-tag"></i></td>
                                                    <td>{{ Auth()->user()->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-envelope"></i></td>
                                                    <td>{{ Auth()->user()->email }}</td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-phone"></i></td>
                                                    <td>{{ Auth()->user()->phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-user"></i></td>
                                                    <td>{{ Auth()->user()->username }}</td>
                                                </tr>
                                                @if (auth()->user()->roles()->first()->name != 'Instansi' && auth()->user()->roles()->first()->name != 'Eksekutif')
                                                    <tr>
                                                        <td><i class="fas fa-building"></i></td>
                                                        <td>{{ Auth()->user()->workunit->name }}</td>
                                                    </tr>
                                                @elseif(auth()->user()->roles()->first()->name == 'Instansi')
                                                    <tr>
                                                        <td><i class="fas fa-building"></i></td>
                                                        <td>{{ Auth()->user()->instansi->nama_instansi }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-briefcase"></i></td>
                                                        <td>{{ Auth()->user()->jabatan }}</td>
                                                    </tr>
                                                @else
                                                    
                                                @endif
                                                <tr>
                                                    <td><i class="fas fa-key"></i></td>
                                                    <td>
                                                        @if (auth()->user()->roles()->first()->description == 'user')
                                                            <span class="badge badge-info mt-3">{{ auth()->user()->roles()->first()->description }}</span>
                                                        @elseif (auth()->user()->roles()->first()->description == 'admin')
                                                            <span class="badge badge-primary mt-3">{{ auth()->user()->roles()->first()->description }}</span>
                                                        @else
                                                            <label class="badge badge-success mt-3">{{ auth()->user()->roles()->first()->description }}</label>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#update-profile" role="tab" aria-controls="pills-update-profile" aria-selected="false">Update Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#activity" role="tab" aria-controls="pills-activity" aria-selected="true">Aktivity Log</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#change-password" role="tab" aria-controls="pills-change-password" aria-selected="false">Update Password</a>
                            </li>
                        </ul>
                        <div class="tab-content mt-3">
                            <div class="tab-pane fade show active" id="update-profile" role="tabpanel" aria-labelledby="profile-tab">
                                <form method="post" action="{{ route('profile.update') }}" class="needs-validation" novalidate="" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-md-12 col-12">
                                            <label>Photo Profile</label>
                                            <input type="file" class="form-control @error('avatar') is-invalid @enderror" name="avatar" id="preview" accept="image/png, image/jpeg">
                                            @error('avatar')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        @if (auth()->user()->roles()->first()->name != 'Instansi' && auth()->user()->roles()->first()->name != 'Eksekutif' )
                                            <div class="form-group col-md-6 col-12">
                                                <label>Unit Kerja</label><span class="text-danger">*</span>
                                                <input type="text" class="form-control @error('workunit') is-invalid @enderror" name="workunit" value="{{ Auth::user()->workunit->name }}" readonly required="">
                                                @error('workunit')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        @elseif(auth()->user()->roles()->first()->name != 'Instansi' && auth()->user()->roles()->first()->name != 'Eksekutif' && auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin')
                                            <div class="form-group col-md-6 col-12">
                                                <label>ID Telegram</label><span class="text-danger">*</span>
                                                <input type="text" class="form-control @error('telegram_id') is-invalid @enderror" name="telegram_id" value="{{ Auth::user()->telegram_id }}" required="">
                                                @error('telegram_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        @elseif(auth()->user()->roles()->first()->name == 'Instansi')
                                            <div class="form-group col-md-6 col-12">
                                                <label>Instansi</label><span class="text-danger">*</span>
                                                <input type="text" class="form-control @error('instansi') is-invalid @enderror" name="instansi" value="{{ Auth::user()->instansi->nama_instansi }}" readonly required="">
                                                @error('instansi')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-6 col-12">
                                                <label>Jabatan</label><span class="text-danger">*</span>
                                                <input type="text" class="form-control @error('jabatan') is-invalid @enderror" name="jabatan" value="{{ Auth::user()->jabatan }}" required="">
                                                @error('jabatan')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        @endif
                                        <div class="form-group col-md-6 col-12">
                                            <label>Nama</label><span class="text-danger">*</span>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ Auth::user()->name }}" required="">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>No. Telp</label><span class="text-danger">*</span>
                                            <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ Auth::user()->phone }}" required="">
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Username</label><span class="text-danger">*</span>
                                            <input type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ Auth::user()->username }}" required="">
                                            @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Email</label><span class="text-danger">*</span>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required="">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success btn-block">Simpan</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="activity" role="tabpanel" aria-labelledby="activity-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped dataTable" id="table-1">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Aktivity</th>
                                                <th>Waktu</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no = 1;
                                            @endphp
                                            @foreach ($activity as $act)
                                            @if ($act->causer->id == auth()->user()->id)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $act->description }}</td>
                                                <td>{{ \Carbon\Carbon::parse($act->created_at)->diffForHumans() }}</td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="change-password" role="tabpanel" aria-labelledby="change-password-tab">

                                <form method="post" action="{{ route('change.password') }}" class="needs-validation" novalidate="">
                                    @csrf

                                    @foreach ($errors->all() as $error)
                                    <p class="text-danger">{{ $error }}</p>
                                    @endforeach

                                    <div class="row">
                                        <div class="form-group col-md-12 col-12">
                                            <label for="password">{{ __('Password Saat Ini') }}</label><span class="text-danger">*</span>
                                            <input id="password" type="password" class="form-control" name="password" autocomplete="current-password" placeholder="Masukan password anda saat ini">
                                        </div>
                                        <div class="form-group col-md-12 col-12">
                                            <label for="password">{{ __('Password Baru') }}</label><span class="text-danger">*</span>
                                            <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password" placeholder="Masukan password baru anda">
                                        </div>
                                        <div class="form-group col-md-12 col-12">
                                            <label for="password">{{ __('Konfirmasi Password Baru') }}</label><span class="text-danger">*</span>
                                            <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password" placeholder="Masukan konfirmasi password baru anda">
                                        </div>
                                        <div class="form-group col-md-12 col-12">
                                            <button class="btn btn-success btn-block">Simpan</button>
                                        </div>
                                    </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

@section('js')
@if (session('update-password-success'))
<script>
    swal("Password Berhasil Di Update", {
        title: "Sukses",
        icon: "success",
    });
</script>
@endif

@if (session('update-profile-success'))
<script>
    swal("Profile Berhasil Di Update", {
        title: "Sukses",
        icon: "success",
    });
</script>
@endif

@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach

<script>
    document.getElementById('preview').addEventListener('change', function () {
        if (this.files[0]) {
            var picture = new FileReader();
            picture.readAsDataURL(this.files[0]);
            picture.addEventListener('load', function (event) {
                document.getElementById('preview-img').setAttribute('src', event.target.result);
            });
        }
    });
</script>
@endsection