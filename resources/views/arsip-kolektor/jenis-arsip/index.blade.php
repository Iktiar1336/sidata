@extends('layouts.dashboard')

@section('title')
Jenis Arsip
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Jenis Arsip</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Jenis Arsip</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Jenis Arsip</h2>
        <p class="section-lead">Halaman untuk mengelola data Jenis Arsip.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Jenis Arsip</h4>
                <div class="card-header-action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Tambah Jenis Arsip
                      </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table-capaian">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Nama</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Jenis Arsip</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('jenis-arsip.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama </label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" placeholder="Nama Jenis Arsip" id="nama">
                
                        @error('Nama')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        $('.selecpicker').selectpicker();
        $('#table-capaian').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/arsip/jenis-arsip/",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'nama', name: 'nama'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Jenis arsip berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Jenis arsip berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Jenis arsip tidak dapat di hapus, karena sudah terikat dengan data lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Jenis arsip berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection