@extends('layouts.dashboard')

@section('title')
Update Jenis Arsip
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Jenis Arsip</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Jenis Arsip</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Update Jenis Arsip</h2>
        <p class="section-lead">Halaman untuk mengelola data Jenis Arsip.</p>

        <div class="card">
            <div class="card-header">
                <h4>Update Jenis Arsip</h4>
                <div class="card-header-action">
                    <a href="{{ route('jenis-arsip.index') }}" class="btn btn-primary">
                        Kembali
                      </a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('jenis-arsip.update', Crypt::encrypt($jenisarsip->id)) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="nama">Nama </label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" value="{{ $jenisarsip->nama }}" name="nama" placeholder="Nama Jenis Arsip" id="nama">
                
                        @error('Nama')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection