<div class="dropdown d-inline">
    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opsi
    </button>

    <div class="dropdown-menu">
        @if ($row->status == 1)
            @if (auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin')
                @if ($jawaban)
                    <a class="dropdown-item has-icon text-warning" href="{{ route('instrumen-survey.edit', Crypt::encrypt($row->id)) }}">
                        <i class="fas fa-pen mt-1"></i> 
                        Edit jawaban
                    </a>
                    <a class="dropdown-item has-icon text-primary" href="{{ route('import-arsip.index') }}">
                        <i class="fas fa-file-import mt-1"></i> 
                        Import Arsip
                    </a>
                    <form action="{{ route('arsip.detail', Crypt::encrypt($row->id)) }}" method="POST">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $jawaban->user_id }}">
                        <button type="submit" class="dropdown-item has-icon text-info">
                            <i class="fas fa-eye mt-1"></i> 
                            Lihat Jawaban
                        </button>
                    </form>
                @else
                    <a class="dropdown-item has-icon text-primary" href="{{ route('instrumen-survey.isisurvey', Crypt::encrypt($row->id)) }}">
                        <i class="fas fa-pen mt-1"></i> 
                        Isi Survey
                    </a>
                @endif
            @else
                <a class="dropdown-item has-icon" href="{{ route('instrumen-survey.listresponden', Crypt::encrypt($row->id)) }}">
                    <i class="fas fa-eye mt-1"></i> 
                    Detail
                </a>
                <a class="dropdown-item has-icon text-primary" href="{{ route('import-arsip.index') }}">
                    <i class="fas fa-file-import mt-1"></i> 
                    Import Arsip
                </a>
            @endif
        @else
            
        @endif
    </div>
</div>