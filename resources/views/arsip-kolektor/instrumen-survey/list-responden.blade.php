@extends('layouts.dashboard')

@section('title', 'List Responden')

@section('css')
    
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>List Responden</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">List Responden</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List List Responden</h2>
 
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered" id="tablearsip">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Responden</th>
                                        <th>Instansi</th>
                                        <th>
                                            Waktu Pengisian
                                        </th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')

@if (session('error'))
    <script>
        swal("Data Arsip Sudah Ada", {
            title: "Failed",
            icon: "error",
        });
    </script>
@endif

<script>

    $(document).ready(function () {
        $('.selectpicker').selectpicker();

        $('#tablearsip').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/arsip-kolektor/instrumen-survey/responden/" + "{{ $survey->id }}", 
            dataType: "json",
            autoWidth: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'nama', name: 'nama'},
                {data: 'instansi', name: 'instansi'},
                {data: 'waktu_pengisian', name: 'waktu_pengisian'},
                {data: 'action', name: 'action',orderable: false,searchable: false}
            ]
        });

        $('#instansi_id').on('change', function () {
            var instansi_id = $(this).val();
            if (instansi_id == '') {
                swal("Gagal", "Instansi tidak valid !", "error");
            }
        });
    });
</script>

@if (session('insert'))
    <script>
        swal("Data Arsip Berhasil Ditambahkan", {
            title: "Success",
            icon: "success",
        });
    </script>
@endif

@if (session('update'))
    <script>
        swal("Data Arsip Berhasil Diubah", {
            title: "Success",
            icon: "success",
        });
    </script>
@endif

@if (session('delete'))
    <script>
        swal("Data Arsip Berhasil Dihapus", {
            title: "Success",
            icon: "success",
        });
    </script>
@endif

@if (session('import-failed'))
    <script>
        swal("Data Yang Anda Masukkan Tidak Valid , Silahkan Cek Kembali", {
            title: "Failed",
            icon: "error",
        });
    </script>
@endif

@if (session('error'))
    <script>
        swal("Data Yang Anda Masukkan Sudah Ada, Silahkan Cek Kembali", {
            title: "Failed",
            icon: "error",
        });
    </script>
@endif
@endsection