@extends('layouts.dashboard')

@section('title', 'Instrumen Survey')

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Instrumen Survey</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Instrumen Survey</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Instrumen Survey</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered" id="tablearsip" style="width: 110%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Survey</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>

    $(document).ready(function () {
        $('.selectpicker').selectpicker();

        $('#tablearsip').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/arsip-kolektor/instrumen-survey", 
            dataType: "json",
            autoWidth: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'nama', name: 'nama',
                    render: function (data, type, row) {
                        return '<p class="text-uppercase">'+data+'</p>';
                    }},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action',orderable: false,searchable: false}
            ]
        });

        $('#instansi_id').on('change', function () {
            var instansi_id = $(this).val();
            if (instansi_id == '') {
                swal("Gagal", "Instansi tidak valid !", "error");
            }
        });
    });
</script>

@if (Session::has('success'))
<script>
	swal("{{ Session::get('success') }}", {
		title: "Success",
		icon: "success",
	});
</script>
@endif

@if (Session::has('error'))
<script>
    swal("{{ Session::get('error') }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

@endsection