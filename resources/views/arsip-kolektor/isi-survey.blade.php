@extends('layouts.dashboard')

@section('title')
Pengisian Survey
@endsection

@section('css')
<style>
    .table th {
        width: auto;
    }

    .col-tab {
        overflow-x: auto;
    }

    .col-tab #pills-tab {
        width: max-content;
    }
    .step-line {
        background: #6777ef;
        height: 2px;
        width: 90vw;
        position: absolute;
        top: 22px;
        z-index: 1;
    }

    .nav-pills li {
        background: #fff;
        margin: 0 10px;
        z-index: 2;
        border: 2px solid #6777ef;
        border-radius: 10px;
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Survey</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Survey</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Pengisian Survey</h2>
        <p class="section-lead">Pengisian Survey {{ $survey->nama }} Tahun {{ $survey->tahun }}</p>

        <div class="card">
            <div class="card-body">
                <form action="{{ route('arsip.jawab-survey', Crypt::encrypt($survey->id)) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-tab" id="col-tab">
                            <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                                <div class="step-line"></div>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" href="#pills-responden" id="pills-responden-tab" data-toggle="pill" data-target="#pills-responden" type="button" role="tab" aria-controls="pills-responden" aria-selected="true">Data Responden
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-kelembagaan" id="pills-kelembagaan-tab" data-toggle="pill" data-target="#pills-kelembagaan" type="button" role="tab" aria-controls="pills-kelembagaan" aria-selected="true">Kelembagaan
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-kebijakan" id="pills-kebijakan-tab" data-toggle="pill" data-target="#pills-kebijakan" type="button" role="tab" aria-controls="pills-kebijakan" aria-selected="true">Kebijakan Pengelolaan Arsip
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-sdmkearsipan" id="pills-sdmkearsipan-tab" data-toggle="pill" data-target="#pills-sdmkearsipan" type="button" role="tab" aria-controls="pills-sdmkearsipan" aria-selected="true">SDM Kearsipan
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-anggarankearsipan" id="pills-anggarankearsipan-tab" data-toggle="pill" data-target="#pills-anggarankearsipan" type="button" role="tab" aria-controls="pills-anggarankearsipan" aria-selected="true">Anggaran Bidang Kearsipan
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-khasanaharsip" id="pills-khasanaharsip-tab" data-toggle="pill" data-target="#pills-khasanaharsip" type="button" role="tab" aria-controls="pills-khasanaharsip" aria-selected="true">Khasanah Arsip
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-sistempengelolaanarsip" id="pills-sistempengelolaanarsip-tab" data-toggle="pill" data-target="#pills-sistempengelolaanarsip" type="button" role="tab" aria-controls="pills-sistempengelolaanarsip" aria-selected="true">Sistem Pengelolaan Arsip
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#pills-sarana-prasarana" id="pills-sarana-prasarana-tab" data-toggle="pill" data-target="#pills-sarana-prasarana" type="button" role="tab" aria-controls="pills-sarana-prasarana" aria-selected="true">Sarana Prasarana
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12">
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-responden" role="tabpanel" aria-labelledby="pills-responden-tab">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="nama">NAMA :</label>
                                            <input type="hidden" name="surveyarsip_id" value="{{ $survey->id }}"><span class="text-danger">*</span>
                                            <input type="hidden" name="user_id" id="nama" value="{{ auth()->user()->id }}" readonly class="form-control" placeholder="Masukkan nama anda" required>
                                            <input type="text" name="nama" id="nama" value="{{ auth()->user()->name }}" readonly class="form-control" placeholder="Masukkan nama anda" required>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="jabatan">JABATAN :</label><span class="text-danger">*</span>
                                            <input type="text" name="jabatan" id="jabatan" value="{{ auth()->user()->jabatan }}" readonly class="form-control" placeholder="Masukkan jabatan anda" required>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="unitkerja">UNIT KERJA :</label><span class="text-danger">*</span>
                                            <input type="text" name="unitkerja" id="unitkerja" class="form-control" placeholder="Masukkan unit kerja anda" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="instansi">KEMENTERIAN / INSTANSI :</label> <span class="text-danger">*</span>
                                            <input type="text" name="instansi" id="instansi" class="form-control" placeholder="Masukkan instansi anda" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="no_telp">NO. HANDPHONE</label><span class="text-danger">*</span>
                                            <input type="text" name="no_telp" id="no_telp" value="{{ Auth::user()->phone }}" class="form-control" placeholder="Masukkan no. handphone anda" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for=""></label>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-kelembagaan-tab">Berikutnya</button>
                                </div>
                                <div class="tab-pane fade" id="pills-kelembagaan" role="tabpanel" aria-labelledby="pills-kelembagaan-tab">


                                    {{-- @foreach ($kategoriPertanyaan as $item)
                                        <div class="section-{{ $item->judul }}">
                                            <div class="media">
                                                <div class="media-body">
                                                    <h5 class="mt-0 mb-1">A. {{ $item->judul }}</h5>
                                                    <p>{{ $item->deskripsi }}</p>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            @foreach($item->kolompertanyaan as $kolom)
                                                                <th>{{ $kolom->nama_kolom }}</th>
                                                            @endforeach
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            @foreach ($pertanyaan as $p)
                                                                @if($p->text !== null)
                                                                    <td>{{ $p->text }}</td>
                                                                @else
                                                                    <td>
                                                                    </td>
                                                                @endif 
                                                            @endforeach
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endforeach --}}


                                    <!-- A. KELEMBAGAAN -->
                                    <div class="section-kelembagaan">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-1">A. KELEMBAGAAN</h5>
                                                <p>Isilah sesuai dengan kondisi yang ada saat ini, disesuaikan dengan tingkat unit kearsipan</p>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <th>No</th>
                                                    <th>Unit Kerja</th>
                                                    <th>Uraian</th>
                                                    <th>Isian</th>
                                                    <th>Keterangan</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="6">1.</td>
                                                        <td rowspan="6">TK. 1</td>
                                                        <td>Nomenklatur Instansi Pusat</td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk1_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk1_a2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Nama Kepala Unit Kearsipan
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk1_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk1_b2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Alamat
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk1_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk1_c2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Email
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk1_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk1_d2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Telp / Fax
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk1_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk1_e2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Website Instansi Pusat
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk1_f1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk1_f2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="6">2.</td>
                                                        <td rowspan="6">TK. 2</td>
                                                        <td>Nomenklatur Instansi Pusat</td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk2_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk2_a2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Nama Kepala Unit Kearsipan
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk2_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk2_b2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Alamat
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk2_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk2_c2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Email
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk2_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk2_d2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Telp / Fax
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk2_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk2_e2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Website Instansi Pusat
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk2_f1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk2_f2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="6">3.</td>
                                                        <td rowspan="6">TK. 3</td>
                                                        <td>Nomenklatur Instansi Pusat</td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk3_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk3_a2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Nama Kepala Unit Kearsipan
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk3_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk3_b2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Alamat
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk3_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk3_c2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Email
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk3_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk3_d2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Telp / Fax
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk3_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk3_e2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Website Instansi Pusat
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk3_f1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk3_f2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="6">4.</td>
                                                        <td rowspan="6">TK. 4</td>
                                                        <td>Nomenklatur Instansi Pusat</td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk4_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk4_a2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Nama Kepala Unit Kearsipan
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk4_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk4_b2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Alamat
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk4_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk4_c2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Email
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk4_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk4_d2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Telp / Fax
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk4_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk4_e2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Website Instansi Pusat
                                                        </td>
                                                        <td>
                                                            <input type="text" name="kelembagaan_tk4_f1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kelembagaan_tk4_f2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-responden-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-kebijakan-tab">Berikutnya</button>
                                </div>

                                <div class="tab-pane fade" id="pills-kebijakan" role="tabpanel" aria-labelledby="pills-kebijakan-tab">

                                    <!-- B. KEBIJAKAN PENGELOLAAN ARSIP -->
                                    <div class="section-kebijakan mt-2">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-1">B. KEBIJAKAN PENGELOLAAN ARSIP</h5>
                                                <p>Apakah di instansi saudara memiliki peraturan pada poin tersebut dibawah ini, jika ada isikan nomor dan tahun kebijakan tersebut pada kolom Nama Kebijakan</p>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Unit Kerja</th>
                                                        <th>Uraian</th>
                                                        <th>Nama Kebijakan</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">1.</td>
                                                        <td rowspan="5">TK. 1</td>
                                                        <td>Kebijakan Tata Naskah Dinas</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk1_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk1_a2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Jadwal Retensi Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk1_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk1_b2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Klasifikasi Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk1_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk1_c2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Klasifikasi Keamanan dan Akses :</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk1_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk1_d2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pedoman Pengelolaan Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk1_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk1_e2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="5">2.</td>
                                                        <td rowspan="5">TK. 2</td>
                                                        <td>Kebijakan Tata Naskah Dinas</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk2_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk2_a2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Jadwal Retensi Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk2_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk2_b2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Klasifikasi Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk2_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk2_c2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Klasifikasi Keamanan dan Akses :</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk2_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk2_d2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pedoman Pengelolaan Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk2_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk2_e2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="5">3.</td>
                                                        <td rowspan="5">TK. 3</td>
                                                        <td>Kebijakan Tata Naskah Dinas</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk3_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk3_a2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Jadwal Retensi Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk3_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk3_b2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Klasifikasi Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk3_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk3_c2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Klasifikasi Keamanan dan Akses :</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk3_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk3_d2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pedoman Pengelolaan Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk3_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk3_e2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="5">4.</td>
                                                        <td rowspan="5">TK. 4</td>
                                                        <td>Kebijakan Tata Naskah Dinas</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk4_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk4_a2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Jadwal Retensi Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk4_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk4_b2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Klasifikasi Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk4_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk4_c2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kebijakan Klasifikasi Keamanan dan Akses :</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk4_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk4_d2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pedoman Pengelolaan Arsip</td>
                                                        <td>
                                                            <input type="text" name="kebijakanpengelolaanarsip_tk4_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="kebijakanpengelolaanarsip_tk4_e2" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-kelembagaan-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-sdmkearsipan-tab">Berikutnya</button>
                                </div>

                                <div class="tab-pane fade" id="pills-sdmkearsipan" role="tabpanel" aria-labelledby="pills-sdmkearsipan-tab">
                                    <!-- C.SDM KEARSIPAN -->
                                    <div class="section-sdmkearsipan mt-2">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-1">C. SDM KEARSIPAN</h5>
                                                <p>Apakah di lingkungan Instansi saudara memiliki SDM kearsipan, pada poin tersebut dibawah ini mohon diisikan jumlah orang SDM pada kolom jumlah sesuai dengan kondisi yang ada saat ini, berikut dengan status SDM</p>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-responsive" style="width: 205%;">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Unit Kerja</th>
                                                        <th>Uraian</th>
                                                        <th>Arsiparis / Pengelola LK</th>
                                                        <th>Arsiparis / Pengelola PR </th>
                                                        <th>Arsiparis / Pengelola Total</th>
                                                        <th>Keterangan ASN</th>
                                                        <th>Keterangan THL</th>
                                                        <th>Keterangan HNR</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="8">1.</td>
                                                        <td rowspan="8">TK. 1</td>
                                                        <td>Arsiparis Terampil</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_a2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_a3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_a4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_a5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_a6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Mahir</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_b2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_b3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_b4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_b5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_b6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Penyelia</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_c2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_c3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_c4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_c5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_c6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Pertama</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_d2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_d3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_d4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_d5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_d6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Muda</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_e2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_e3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_e4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_e5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_e6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Madya</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_f1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_f2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_f3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_f4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_f5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_f6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Utama</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_g1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_g2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_g3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_g4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_g5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_g6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pengelola Arsip / Non - Arsiparis</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_h1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_h2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_h3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_h4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_h5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk1_h6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="8">2.</td>
                                                        <td rowspan="8">TK. 2</td>
                                                        <td>Arsiparis Terampil</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_a2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_a3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_a4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_a5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_a6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Arsiparis Mahir</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_b2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_b3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_b4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_b5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_b6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Penyelia</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_c2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_c3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_c4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_c5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_c6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Pertama</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_d2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_d3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_d4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_d5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_d6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Muda</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_e2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_e3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_e4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_e5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_e6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Madya</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_f1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_f2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_f3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_f4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_f5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_f6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arsiparis Utama</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_g1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_g2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_g3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_g4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_g5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_g6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pengelola Arsip / Non - Arsiparis</td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_h1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_h2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_h3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_h4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_h5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk2_h6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="8">3.</td>
                                                        <td rowspan="8">TK. 3</td>
                                                        <td>
                                                            Arsiparis Terampil
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_a2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_a3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_a4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_a5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_a6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Mahir
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_b2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_b3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_b4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_b5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_b6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Penyelia
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_c2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_c3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_c4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_c5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_c6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Pertama
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_d2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_d3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_d4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_d5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_d6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Muda
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_e2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_e3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_e4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_e5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_e6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Madya
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_f1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_f2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_f3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_f4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_f5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_f6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Utama
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_g1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_g2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_g3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_g4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_g5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_g6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Pengelola Arsip / Non - Arsiparis
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_h1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_h2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_h3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_h4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_h5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk3_h6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="8">4.</td>
                                                        <td rowspan="8">TK .4</td>
                                                        <td>
                                                            Arsiparis Terampil
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_a2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_a3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_a4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_a5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_a6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Mahir
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_b2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_b3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_b4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_b5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_b6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Penyelia
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_c1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_c2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_c3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_c4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_c5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_c6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Pertama
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_d1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_d2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_d3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_d4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_d5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_d6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Muda
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_e1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_e2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_e3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_e4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_e5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_e6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Madya
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_f1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_f2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_f3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_f4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_f5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_f6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsiparis Utama
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_g1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_g2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_g3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_g4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_g5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_g6" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Pengelola Arsip / Non - Arsiparis
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_h1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_h2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_h3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_h4" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_h5" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="sdmkearsipan_tk4_h6" class="form-control" placeholder="">
                                                        </td>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-kebijakan-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-anggarankearsipan-tab">Berikutnya</button>
                                </div>

                                <div class="tab-pane fade" id="pills-anggarankearsipan" role="tabpanel" aria-labelledby="pills-anggarankearsipan-tab">
                                    <!-- D. Anggaran Bidang Kearsipan -->
                                    <div class="section-anggaranbidangkerarsipan mt-4">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-1">D. Anggaran Bidang Kearsipan</h5>
                                                <p>Isilah sesuai dengan anggaran tahun 2022 dan tahun 2023</p>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered" style="width: 140%">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Uraian</th>
                                                        @php
                                                        $thisYear = date('Y');
                                                        $nextYear = date('Y', strtotime('+1 year'));
                                                        @endphp
                                                        <th>{{$thisYear}} - Penataan Arsip</th>
                                                        <th>{{$thisYear}} - Ahli Media</th>
                                                        <th>{{ $nextYear }} - Penataan Arsip</th>
                                                        <th>{{ $nextYear }} - Ahli Media</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            1.
                                                        </td>
                                                        <td>
                                                            Anggaran Kearsipan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="anggaranbidangkearsipan_a1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="anggaranbidangkearsipan_a2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="anggaranbidangkearsipan_a3" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="anggaranbidangkearsipan_a4" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            2.
                                                        </td>
                                                        <td>
                                                            Pihak Ke 3 (Sebutkan Nama Perusahaan)
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="anggaranbidangkearsipan_b1" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="anggaranbidangkearsipan_b2" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="anggaranbidangkearsipan_b3" class="form-control" placeholder="" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="anggaranbidangkearsipan_b4" class="form-control" placeholder="" readonly>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-sdmkearsipan-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-khasanaharsip-tab">Berikutnya</button>
                                </div>

                                <div class="tab-pane fade" id="pills-khasanaharsip" role=" tabpanel" aria-labelledby="pills-khasanaharsip-tab">
                                    <!-- E. Khasanah Arsip -->
                                    <div class="section-khasanaharsip mt-4">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-1">E. Khasanah Arsip</h5>
                                                <p style="text-align: justify">
                                                    Apakah di instansi saudara memiliki khasanah arsip pada poin tersebut dibawah ini, jika ada agar berikan informasi jumlah yang tersedia pada kolom lembar/roll/bundle/amplop/cd/folder/box/MAP/kaset/computer yang disesuaikan dengan kondisi yang ada, informasi luas tempat penyimpanan, periode tahun
                                                    arsip yang disimpan (contoh: 20x0- 20x2), unit kerja yang menyimpan dan kondisi tempat penyimpanan (Baik, Cukup Memadai, Kurang Memadai)
                                                </p>
                                            </div>
                                        </div>

                                        <!-- Arsip Aktif -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered" style="width: 270%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis</th>
                                                        <th>Uraian</th>
                                                        <th>
                                                            Lembar / Roll
                                                        </th>
                                                        <th>
                                                            Bundle / Amplop / CD / Folder
                                                        </th>
                                                        <th>
                                                            Box / MAP / Kaset (CAN) / Computer (Hardisk)
                                                        </th>
                                                        <th>
                                                            Luas Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            Periode Tahun
                                                        </th>
                                                        <th>
                                                            Lokasi Simpan (Unit Kerja)
                                                        </th>
                                                        <th>
                                                            Kondisi Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            File
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">1.</td>
                                                        <td rowspan="5">
                                                            Arsip Aktif
                                                        </td>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a1a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a1c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a1d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a1e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a1f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_a1g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="file" name="khasanaharsip_a1h" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gambar / Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_a2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="file" name="khasanaharsip_a2h" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film / Video
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_a3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="file" name="khasanaharsip_a3h" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_a4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="file" name="khasanaharsip_a4h" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_a5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_a5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_a5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="file" name="khasanaharsip_a5h" class="form-control" placeholder="">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Arsip In Aktif -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-3" style="width: 270%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis</th>
                                                        <th>Uraian</th>
                                                        <th>
                                                            Lembar / Roll
                                                        </th>
                                                        <th>
                                                            Bundle / Amplop / CD / Folder
                                                        </th>
                                                        <th>
                                                            Box / MAP / Kaset (CAN) / Computer (Hardisk)
                                                        </th>
                                                        <th>
                                                            Luas Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            Periode Tahun
                                                        </th>
                                                        <th>
                                                            Lokasi Simpan (Unit Kerja)
                                                        </th>
                                                        <th>
                                                            Kondisi Tempat Penyimpanan
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">2.</td>
                                                        <td rowspan="5">Arsip In Aktif</td>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b1a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b1c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b1d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b1e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b1f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_b1g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gambar / Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_b2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film / Video
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_b3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_b4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_b5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_b5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_b5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Arsip Vital -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-3" style="width: 270%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis</th>
                                                        <th>Uraian</th>
                                                        <th>
                                                            Lembar / Roll
                                                        </th>
                                                        <th>
                                                            Bundle / Amplop / CD / Folder
                                                        </th>
                                                        <th>
                                                            Box / MAP / Kaset (CAN) / Computer (Hardisk)
                                                        </th>
                                                        <th>
                                                            Luas Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            Periode Tahun
                                                        </th>
                                                        <th>
                                                            Lokasi Simpan (Unit Kerja)
                                                        </th>
                                                        <th>
                                                            Kondisi Tempat Penyimpanan
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">3.</td>
                                                        <td rowspan="5">Arsip Vital</td>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c1a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c1c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c1d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c1e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c1f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_c1g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gambar / Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_c2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film / Video
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_c3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_c4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_c5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_c5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_c5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Daftar Arsip Usul Serah -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-3" style="width: 270%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis</th>
                                                        <th>Uraian</th>
                                                        <th>
                                                            Lembar / Roll
                                                        </th>
                                                        <th>
                                                            Bundle / Amplop / CD / Folder
                                                        </th>
                                                        <th>
                                                            Box / MAP / Kaset (CAN) / Computer (Hardisk)
                                                        </th>
                                                        <th>
                                                            Luas Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            Periode Tahun
                                                        </th>
                                                        <th>
                                                            Lokasi Simpan (Unit Kerja)
                                                        </th>
                                                        <th>
                                                            Kondisi Tempat Penyimpanan
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">4.</td>
                                                        <td rowspan="5">Daftar Arsip Usul Serah</td>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d1a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d1c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d1d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d1e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d1f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_d1g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gambar / Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_d2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film / Video
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_d3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_d4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_d5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_d5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_d5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Daftar Arsip Usul Musnah -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-3" style="width: 270%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis</th>
                                                        <th>Uraian</th>
                                                        <th>
                                                            Lembar / Roll
                                                        </th>
                                                        <th>
                                                            Bundle / Amplop / CD / Folder
                                                        </th>
                                                        <th>
                                                            Box / MAP / Kaset (CAN) / Computer (Hardisk)
                                                        </th>
                                                        <th>
                                                            Luas Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            Periode Tahun
                                                        </th>
                                                        <th>
                                                            Lokasi Simpan (Unit Kerja)
                                                        </th>
                                                        <th>
                                                            Kondisi Tempat Penyimpanan
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">5.</td>
                                                        <td rowspan="5">Daftar Arsip Usul Musnah</td>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e1a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e1c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e1d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e1e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e1f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_e1g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gambar / Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_e2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film / Video
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_e3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_e4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_e5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_e5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_e5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Arsip Asset -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-3" style="width: 270%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis</th>
                                                        <th>Uraian</th>
                                                        <th>
                                                            Lembar / Roll
                                                        </th>
                                                        <th>
                                                            Bundle / Amplop / CD / Folder
                                                        </th>
                                                        <th>
                                                            Box / MAP / Kaset (CAN) / Computer (Hardisk)
                                                        </th>
                                                        <th>
                                                            Luas Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            Periode Tahun
                                                        </th>
                                                        <th>
                                                            Lokasi Simpan (Unit Kerja)
                                                        </th>
                                                        <th>
                                                            Kondisi Tempat Penyimpanan
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">6.</td>
                                                        <td rowspan="5">Arsip Aset</td>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f1a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f1c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f1d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f1e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f1f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_f1g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gambar / Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_f2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film / Video
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_f3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_f4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_f5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_f5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_f5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Arsip Terjaga -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-3" style="width: 270%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis</th>
                                                        <th>Uraian</th>
                                                        <th>
                                                            Lembar / Roll
                                                        </th>
                                                        <th>
                                                            Bundle / Amplop / CD / Folder
                                                        </th>
                                                        <th>
                                                            Box / MAP / Kaset (CAN) / Computer (Hardisk)
                                                        </th>
                                                        <th>
                                                            Luas Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            Periode Tahun
                                                        </th>
                                                        <th>
                                                            Lokasi Simpan (Unit Kerja)
                                                        </th>
                                                        <th>
                                                            Kondisi Tempat Penyimpanan
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">7.</td>
                                                        <td rowspan="5">Arsip Terjaga</td>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g1a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g1c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g1d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g1e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g1f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_g1g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gambar / Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_g2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film / Video
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_g3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_g4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_g5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_g5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_g5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Arsip Alih Media -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-3" style="width: 270%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis</th>
                                                        <th>Uraian</th>
                                                        <th>
                                                            Lembar / Roll
                                                        </th>
                                                        <th>
                                                            Bundle / Amplop / CD / Folder
                                                        </th>
                                                        <th>
                                                            Box / MAP / Kaset (CAN) / Computer (Hardisk)
                                                        </th>
                                                        <th>
                                                            Luas Tempat Penyimpanan
                                                        </th>
                                                        <th>
                                                            Periode Tahun
                                                        </th>
                                                        <th>
                                                            Lokasi Simpan (Unit Kerja)
                                                        </th>
                                                        <th>
                                                            Kondisi Tempat Penyimpanan
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="5">8.</td>
                                                        <td rowspan="5">Arsip Alih Media</td>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h1a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h1c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h1d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h1e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h1f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_h1g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gambar / Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_h2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film / Video
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_h3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_h4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_h5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_h5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_h5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Opsional -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-4">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Uraian Pertanyaan</th>
                                                        <th>Ada</th>
                                                        <th>Tidak Ada</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>9</td>
                                                        <td>
                                                            Apakah di instansi Saudara masih ada arsip yang belum di daftar / diberkaskan?
                                                        </td>
                                                        <td>
                                                            <input type="radio" id="optionada" name="khasanaharsip_i1a" value="Ada">
                                                        </td>
                                                        <td>
                                                            <input type="radio" id="optiontidakada" name="khasanaharsip_i1a" value="Tidak Ada">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- Opsional -->
                                        <div class="table-responsive" id="form-optional">
                                            <table class="table table-bordered" style="width: 270%;">
                                                <thead>
                                                    <tr>
                                                        <th>Uraian</th>
                                                        <th>Folder</th>
                                                        <th>CD</th>
                                                        <th>Komputer / Hardisk</th>
                                                        <th>Luas Ruang Penyimpanan</th>
                                                        <th>Periode Tahun</th>
                                                        <th>Lokasi Simpan (Unit Kerja)</th>
                                                        <th>Lokasi Simpan (Kondisi)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            Kertas / Naskah / Dokumen
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i2a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i2c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i2d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i2e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i2f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_i2g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Foto
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i3a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i3c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i3d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i3e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i3f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_i3g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Film
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i4a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i4b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i4c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i4d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i4e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i4f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_i4g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Kartografi & Kearsitekturan
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i5a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i5b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i5c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i5d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i5e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i5f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_i5g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arsip dalam bentuk lain
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i6a" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i6b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="number" value="" min="0" name="khasanaharsip_i6c" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i6d" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i6e" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="khasanaharsip_i6f" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <select name="khasanaharsip_i6g" class="form-control">
                                                                <option value="Baik">Baik</option>
                                                                <option value="Cukup Memadai">Cukup Memadai</option>
                                                                <option value="Kurang Memadai">Kurang Memadai</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-anggarankearsipan-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-sistempengelolaanarsip-tab">Berikutnya</button>
                                </div>

                                <div class="tab-pane fade" id="pills-sistempengelolaanarsip" role="tabpanel" aria-labelledby="pills-sistempengelolaanarsip-tab">
                                    <!-- F. Sistem Pengelolaan Arsip -->
                                    <div class="section-sistempengelolaanarsip mt-4">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-1">F. Sistem Pengelolaan Arsip</h5>
                                                <p style="text-align: justify">
                                                    Apakah di instansi saudara menggunakan sistem informasi pengelolaan arsip pada poin tersebut dibawah ini (1 - 3) kalau ada berikan tanda cek (v) pada kolom Ada dan tahun dipergunakan pada kolom tahun implementasi sesuai dengan kondisi yang ada saat ini
                                                    untuk point 3, pada kolom keterangan isikan Nama-nama Sistem Informasi, Kodisi Penggunaan (masih digunakan/tidak) jumlah pengguna,dan kapasitas storage datanya
                                                </p>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Sistem Informasi</th>
                                                        <th>Ada</th>
                                                        <th>Tahun</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1.</td>
                                                        <td>
                                                            Srikandi
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="sistempengelolaanarsip_1a" value="Ada">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="sistempengelolaanarsip_1b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="sistempengelolaanarsip_1c" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2.</td>
                                                        <td>
                                                            SIKN - JIKN
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="sistempengelolaanarsip_2a" value="Ada">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="sistempengelolaanarsip_2b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="sistempengelolaanarsip_2c" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3.</td>
                                                        <td>
                                                            Sistem Pengelolaan Arsip lainnya, (mohon disebutkan nama sistemnya pada kolom dibawah)
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="sistempengelolaanarsip_3a" value="Ada">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="sistempengelolaanarsip_3b" class="form-control" placeholder="">
                                                        </td>
                                                        <td>
                                                            <textarea name="sistempengelolaanarsip_3c" class="form-control" placeholder=""></textarea>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-khasanaharsip-tab">Kembali</button>
                                    <button type="button" class="btn btn-primary btn-next" data-to="#pills-sarana-prasarana-tab">Berikutnya</button>
                                </div>

                                <div class="tab-pane fade" id="pills-sarana-prasarana" role="tabpanel" aria-labelledby="pills-sarana-prasarana-tab">
                                    <!-- G. Sarana Prasarana -->
                                    <div class="table responsive mt-3">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-1">G. Sarana Prasarana</h5>
                                                <p style="text-align: justify">
                                                    Apakah di instansi saudara memiliki sarana dan prasana kearsipan pada poin tersebut dibawah ini (1 - 2) kalau ada berikan tanda cek (v) pada kolom ada dan isikan jumlah sarana dan prasarana yangdimiliki pada kolom jumlah sesuai dengan kondisi yang ada saat ini
                                                </p>
                                                <ol>
                                                    <li>
                                                        untuk point 1, pada kolom keterangan isikan kapasitas yang masih tersedia atas ruangan dan rak arsip
                                                    </li>
                                                    <li>
                                                        untuk point 2, pada kolom keterangan isikan kapasitas yang masih tersedia dari repositori digital”
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Sistem Informasi</th>
                                                    <th>Ada</th>
                                                    <th>Jumlah</th>
                                                    <th>Keterangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1.</td>
                                                    <td>
                                                        Record Center
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="saranaprasarana_a1" value="Ada">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="saranaprasarana_a2" class="form-control" placeholder="">
                                                    </td>
                                                    <td>
                                                        <textarea name="saranaprasarana_a3" class="form-control" placeholder=""></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2.</td>
                                                    <td>
                                                        Record Digital
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="saranaprasarana_b1" value="Ada">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="saranaprasarana_b2" class="form-control" placeholder="">
                                                    </td>
                                                    <td>
                                                        <textarea name="saranaprasarana_b3" class="form-control" placeholder=""></textarea>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <button type="button" class="btn btn-primary btn-prev" data-to="#pills-sistempengelolaanarsip-tab">Kembali</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block">Simpan</button>
                    </div> --}}
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        $('.selecpicker').selectpicker();

        const element = document.querySelector("#col-tab");

        element.addEventListener('wheel', (event) => {
        event.preventDefault();

        element.scrollBy({
            left: event.deltaY < 0 ? -50 : 50,
            
        });
        });

        $('#form-optional').hide();

        $('.btn-next').on('click', function () {
            // Get value from data-to in button next
            const n = $(this).attr('data-to');

            // Action trigger click for tag a with id in value n
            $(n).trigger('click').delay(1000);
            $('html, body').animate({
                scrollTop: 0
            });
        });

        $('.btn-prev').on('click', function () {
            // Get value from data-to in button next
            const n = $(this).attr('data-to');

            // Action trigger click for tag a with id in value n
            $(n).trigger('click').delay(1000);
            $('html, body').animate({
                scrollTop: 0
            });
        });

        $('#optionada').on('click', function () {
            var val = $(this).val();
            if (val == 'Ada') {
                $('#form-optional').show();
            }
        });

        $('#optiontidakada').on('click', function () {
            var val = $(this).val();
            if (val == 'Tidak Ada') {
                $('#form-optional').hide();
            }
        });
    });
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection