@extends('layouts.dashboard')

@section('title', 'Arsip Kolektor')

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Arsip Kolektor</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Arsip Kolektor</div>
        </div>
    </div>
    
    <div class="section-body">
        <h2 class="section-title">Edit Arsip Kolektor</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('arsip-kolektor.update', \Crypt::encrypt($arsip_kolektor->id)) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="instansi">Instansi</label>
                                <input type="hidden" name="instansi_id" value="{{ $arsip_kolektor->instansi_id }}">   
                                <input type="hidden" name="user_id" value="{{ $arsip_kolektor->user_id }}">
                                <select name="instansi" id="instansi" class="form-control" disabled>
                                    <option value="">Pilih Instansi</option>
                                    @foreach ($instansi as $item)
                                        <option value="{{ $item->id }}" {{ $item->id == $arsip_kolektor->instansi_id ? 'selected' : '' }}>{{ $item->nama_instansi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="no_berkas">No Berkas</label>
                                <input type="text" name="no_berkas" id="no_berkas" class="form-control" value="{{ $arsip_kolektor->no_berkas }}">

                                @error('no_berkas')
                                    <div class="text-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="kode_klasifikasi">Kode Klasifikasi</label>
                                <input type="text" name="kode_klasifikasi" id="kode_klasifikasi" class="form-control" value="{{ $arsip_kolektor->kode_klasifikasi }}">

                                @error('kode_klasifikasi')
                                    <div class="text-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="uraian_informasi_berkas">Uraian Informasi Berkas</label>
                                <textarea name="uraian_informasi_berkas" id="uraian_informasi_berkas" class="form-control">{{ $arsip_kolektor->uraian_informasi_berkas }}</textarea>

                                @error('uraian_informasi_berkas')
                                    <div class="text-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="tanggal_arsip">Tanggal</label>
                                @php
                                    $tanggal = \Carbon\Carbon::parse($arsip_kolektor->tanggal)->isoFormat('YYYY-MM-DD');
                                @endphp

                                <input type="date" name="tanggal" id="tanggal_arsip" class="form-control" value="{{ $tanggal }}">

                                @error('tanggal_arsip')
                                    <div class="text-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="jumlah_berkas">Jumlah Berkas</label>
                                <input type="number" name="jumlah" id="jumlah_berkas" class="form-control" value="{{ $arsip_kolektor->jumlah }}">

                                @error('jumlah_berkas')
                                    <div class="text-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <textarea name="keterangan" id="keterangan" class="form-control">{{ $arsip_kolektor->keterangan }}</textarea>

                                @error('keterangan')
                                    <div class="text-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Pilih Status</option>
                                    <option value="1" {{ $arsip_kolektor->status == 1 ? 'selected' : '' }}>Aktif</option>
                                    <option value="0" {{ $arsip_kolektor->status == 0 ? 'selected' : '' }}>Tidak Aktif</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function() {
       CKEDITOR.replace('uraian_informasi_berkas');
       CKEDITOR.replace('keterangan');
    });
</script>
@endsection
        