@extends('layouts.dashboard')

@section('title')
Update Survey
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Survey</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Survey</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Update Survey</h2>
        <p class="section-lead">Halaman untuk mengelola data Survey.</p>

        <div class="card">
            <div class="card-header">
                <h4>Update Daftar Survey</h4>
                <div class="card-header-action">
                    
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('survey.update', Crypt::encrypt($survey->id)) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label for="nama">Nama Survey</label>
                        <input type="text" name="nama" id="nama" class="form-control" value="{{ $survey->nama }}" placeholder="Masukkan nama survey" required>
                    </div>

                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        @php
                            $tahun = date('Y');
                        @endphp
                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true">
                            <option value="" disabled selected>Pilih Tahun</option>
                            @for ($i = 0; $i <= 4; $i++)
                                <option value="{{ $tahun+$i }}" {{ $tahun+$i == $survey->tahun ? 'selected' : '' }}>{{ $tahun+$i }}</option>
                            @endfor
                        </select>
                        
                        @error('tahun')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div> 
                    <div class="form-group">
                        <label class="d-block">Status :</label>
                    
                        <div class="form-check form-check-inline">
                            <input class="form-check-input @error('status') is-invalid @enderror" name="status" required type="radio" id="inlineCheckbox1" value="1" {{ $survey->status == '1'
                            ? 'checked':'' }}>
                            <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input @error('status') is-invalid @enderror" name="status" required type="radio" id="inlineCheckbox2" value="0" {{ $survey->status == '0'
                            ? 'checked':'' }}>
                            <label class="form-check-label" for="inlineCheckbox2">Tidak Aktif</label>
                        </div>
                
                        @error('status')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        $('.selecpicker').selectpicker();
    });
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection