@extends('layouts.dashboard')

@section('title')
Survey
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Survey</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Survey</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Survey</h2>
        <p class="section-lead">Halaman untuk mengelola data Survey.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Survey</h4>
                <div class="card-header-action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visimisi">
                        Tambah Survey
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table-indikator-kinerja">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Nama</th>
                                <th>Tahun</th>
                                <th>Status</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="visimisi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Survey</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('survey.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama Survey</label>
                        <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan nama survey" required>
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        @php
                            $tahun = date('Y');
                        @endphp
                        <select class="selecpicker @error('tahun') is-invalid @enderror" name="tahun" id="tahun" data-show-subtext="true" data-live-search="true">
                            <option value="" disabled selected>Pilih Tahun</option>
                            @for ($i = 0; $i <= 4; $i++)
                                <option value="{{ $tahun+$i }}" >{{ $tahun+$i }}</option>
                            @endfor
                        </select>
                        
                        @error('tahun')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div> 
                    <div class="form-group">
                        <label class="d-block">Status :</label>
                    
                        <div class="form-check form-check-inline">
                            <input class="form-check-input @error('status') is-invalid @enderror" name="status" required type="radio" id="inlineCheckbox1" value="1" checked>
                            <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input @error('status') is-invalid @enderror" name="status" required type="radio" id="inlineCheckbox1" value="0">
                            <label class="form-check-label" for="inlineCheckbox1">Tidak Aktif</label>
                        </div>
                
                        @error('status')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        $('.selecpicker').selectpicker();
        $('#table-indikator-kinerja').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/arsip/survey",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'nama', name: 'nama'},
                {data: 'tahun', name: 'tahun'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Survey berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Survey tidak berhasil ditambahkan, karena sudah ada!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Survey berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Survey Tidak Dapat Dihapus!, Karena Sudah Terikat Dengan Data Lain", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Survey berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection