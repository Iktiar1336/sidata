@extends('layouts.dashboard')

@section('title', 'List Arsip')

@section('css')
    
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>List Arsip</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">List Arsip</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Arsip</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered" id="tablearsip" style="width: 150%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Uraian Item</th>
                                        <th>Nomor Surat</th>
                                        <th>Tanggal Surat</th>
                                        <th>Kode Klasifikasi</th>
                                        <th>
                                            Tingkat Perkembangan
                                        </th>
                                        <th>Media Arsip</th>
                                        <th>Kondisi</th>
                                        <th>Jumlah</th>
                                        <th>Keterangan</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')

@if (session('error'))
    <script>
        swal("Data Arsip Sudah Ada", {
            title: "Failed",
            icon: "error",
        });
    </script>
@endif

<script>

    $(document).ready(function () {
        $('.selectpicker').selectpicker();

        $('#tablearsip').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/arsip-kolektor/list-arsip/", 
            dataType: "json",
            autoWidth: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'uraianitem', name: 'uraianitem'},
                {data: 'nomor_surat', name: 'nomor_surat'},
                {data: 'tanggal_surat', name: 'tanggal_surat'},
                {data: 'kode_klasifikasi', name: 'kode_klasifikasi'},
                {data: 'tingkat_perkembangan', name: 'tingkat_perkembangan'},
                {data: 'media_arsip', name: 'media_arsip'},
                {data: 'kondisi', name: 'kondisi'},
                {data: 'jumlah', name: 'jumlah'},
                {data: 'keterangan', name: 'keterangan'},
                {data: 'action', name: 'action',orderable: false,searchable: false}
            ]
        });

        $('#instansi_id').on('change', function () {
            var instansi_id = $(this).val();
            if (instansi_id == '') {
                swal("Gagal", "Instansi tidak valid !", "error");
            }
        });
    });
</script>

@if ($message = Session::get('success'))
<script>
    swal("{{ $message }}", {
        title: "Berhasil",
        icon: "success",
    });
</script>
@endif

@if ($message = Session::get('error'))
<script>
    swal("{{ $message }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endif
@endsection