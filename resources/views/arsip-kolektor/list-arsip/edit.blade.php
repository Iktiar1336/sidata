@extends('layouts.dashboard')

@section('title', 'List Arsip')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/filepond/4.30.4/filepond.css" integrity="sha512-OwkTbucz29JjQUeii4ZRkjY/E+Xdg4AfffPZICCf98rYKWIHxX87AwwuIQ73rbVrev8goqrKmaXyu+VxyDqr1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet" />
<style>
    .filepond--drop-label {
        color: #4c4e53;
    }

    .filepond--label-action {
        text-decoration-color: #babdc0;
    }

    .filepond--panel-root {
        border-radius: 2em;
        background-color: #edf0f4;
        height: 1em;
    }

    .filepond--item-panel {
        background-color: #595e68;
    }

    .filepond--drip-blob {
        background-color: #7f8a9a;
    }

    .loading {
        left: 0;
        right: 0;
        top: 50%;
        width: 150px;
        color: #000;
        margin: auto;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .loading span {
        position: absolute;
        height: 10px;
        width: 84px;
        font-size: 14px;
        top: 50px;
        overflow: hidden;
    }

    .loading span>i {
        position: absolute;
        height: 4px;
        width: 4px;
        border-radius: 50%;
        -webkit-animation: wait 4s infinite;
        -moz-animation: wait 4s infinite;
        -o-animation: wait 4s infinite;
        animation: wait 4s infinite;
    }

    .loading span>i:nth-of-type(1) {
        left: -28px;
        background: yellow;
    }

    .loading span>i:nth-of-type(2) {
        left: -21px;
        -webkit-animation-delay: 0.8s;
        animation-delay: 0.8s;
        background: lightgreen;
    }

    @-webkit-keyframes wait {
        0% {
            left: -7px
        }

        30% {
            left: 52px
        }

        60% {
            left: 22px
        }

        100% {
            left: 100px
        }
    }

    @-moz-keyframes wait {
        0% {
            left: -7px
        }

        30% {
            left: 52px
        }

        60% {
            left: 22px
        }

        100% {
            left: 100px
        }
    }

    @-o-keyframes wait {
        0% {
            left: -7px
        }

        30% {
            left: 52px
        }

        60% {
            left: 22px
        }

        100% {
            left: 100px
        }
    }

    @keyframes wait {
        0% {
            left: -7px
        }

        30% {
            left: 52px
        }

        60% {
            left: 22px
        }

        100% {
            left: 100px
        }
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>List Arsip</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">List Arsip</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Arsip</h2>

        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h4>List Arsip Digital</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled list-unstyled-border" id="list-arsip-digital">
                            <div class="loading">
                                <p>Sedang Memuat...</p>
                                <span><i></i><i></i></span>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Input</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('list-arsip.update', Crypt::encrypt($arsip->id)) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="uraian_item">Uraian Item</label>
                                <input type="text" name="uraian_item" id="uraian_item" class="form-control" value="{{ $arsip->uraianitem }}">
                            </div>
                            <div class="form-group">
                                <label for="nomor_surat">Nomor Surat</label>
                                <input type="text" name="nomor_surat" id="nomor_surat" class="form-control" value="{{ $arsip->nomor_surat }}">
                            </div>
                            <div class="form-group">
                                <label for="tanggal_surat">Tanggal Surat</label>
                                <input type="date" name="tanggal_surat" id="tanggal_surat" class="form-control" value="{{ \Carbon\Carbon::parse($arsip->tanggal_surat)->format('Y-m-d') }}">
                            </div>
                            <div class="form-group">
                                <label for="kode_klasifikasi">Kode Klasifikasi</label>
                                <input type="text" name="kode_klasifikasi" id="kode_klasifikasi" class="form-control" value="{{ $arsip->kode_klasifikasi }}">
                            </div>
                            <div class="form-group">
                                <label for="tingkat_perkembangan">Tingkat Perkembangan</label>
                                <input type="text" name="tingkat_perkembangan" id="tingkat_perkembangan" class="form-control" value="{{ $arsip->tingkat_perkembangan }}">
                            </div>
                            <div class="form-group">
                                <label for="media_arsip">Media Arsip</label>
                                <input type="text" name="media_arsip" id="media_arsip" class="form-control" value="{{ $arsip->media_arsip }}">
                            </div>
                            <div class="form-group">
                                <label for="kondisi">Kondisi</label>
                                <input type="text" name="kondisi" id="kondisi" class="form-control" value="{{ $arsip->kondisi }}">
                            </div>
                            <div class="form-group">
                                <label for="jumlah">Jumlah</label>
                                <input type="text" name="jumlah" id="jumlah" class="form-control" value="{{ $arsip->jumlah }}">
                            </div>
                            <div class="form-group">
                                <label for="keterangan">keterangan</label>
                                <input type="text" name="keterangan" id="keterangan" class="form-control" value="{{ $arsip->keterangan }}">
                            </div>
                            <input type="file" class="filepond" name="file[]" multiple data-max-file-size="3000MB" data-max-files="20" />
                            <span class="text-danger">
                                Ukuran file maksimal 3GB dan maksimal 20 file dalam 1 kali upload <br>
                            </span>

                            <button type="submit" class="btn btn-success btn-block mt-3">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/filepond/4.30.4/filepond.min.js" integrity="sha512-l+50U3iKl0++46sldyNg5mOh27O0OWyWWsU2UnGfIVcxC+fEttAvao0Rns9KclIELHihYJppMWmM5sWof0M7uA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    function listArsipDigital() {
        $.ajax({
            url: "{{ route('list-arsip-digital.list', Crypt::encrypt($arsip->id)) }}",
            type: "GET",
            dataType: "json",
            success: function (data) {
                //console.log(data);
                if(data.length == 0){
                    $('#list-arsip-digital').empty();
                    $('#list-arsip-digital').append(
                        `<li><h5 class="text-center text-danger">Tidak ada data</h5></li>`
                    );
                } else {
                    $('#list-arsip-digital').empty();
                    $.each(data, function (key, value) {
                        var name = value.name;
                        var newname = name.slice(0, 30) + '...';
                        $('#list-arsip-digital').append(`
                            <li class="media">
                                <div>
                                    <img class="mr-3 rounded" width="50" src="`+value.img+`" alt="product">
                                </div>
                                <div class="media-body">
                                <div class="media-title">
                                    <a href="` + value.url + `">` + newname + `</a>
                                    <div class="float-right">
                                        <a href="#" data-url="`+ value.deleteUrl +`" id="delete-file"><i class="fas fa-trash"></i></a>
                                    </div>
                                </div>
                                <div class="text-muted text-small">Type : <a href="#">` + value.type + `</a> <div class="bullet"></div>Size : ` + value.size + `</div>
                                </div>
                            </li>
                        `);
                    });
                }
            }
        });
    }
    $(document).ready(function () {
        setTimeout(function () {
            listArsipDigital();
        }, 3000);

        $(document).on('click', '#delete-file', function (e) {
            //e.preventDefault();
            var url = $(this).data('url');
            $.ajax({
                url: url,
                type: "DELETE",
                dataType: "json",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                    swal({
                        title: "Berhasil!",
                        text: "File Berhasil Dihapus",
                        icon: "success",
                        button: "Ok",
                    });
                    listArsipDigital();
                }
            });
        });
    });
    document.addEventListener('DOMContentLoaded', function () {

        // Register any plugins
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileEncode,
            FilePondPluginFileValidateSize,
        );

        // Create FilePond object
        const inputElement = document.querySelector('input[type="file"]');
        const pond = FilePond.create(inputElement, {
            allowMultiple: true,
            allowReorder: true,
            allowFileEncode: true,
            allowFileValidateSize: true,
            maxFileSize: '3000MB',
            maxFiles: 20,
            labelIdle: 'Drag & Drop File Arsip Digital Anda Atau <span class="filepond--label-action">Pilih</span>',
            imagePreviewHeight: 170,
            imageCropAspectRatio: '1:1',
            imageResizeTargetWidth: 100,
            imageResizeTargetHeight: 100,
            credits: false,
        });

        FilePond.setOptions({
            server: {
                url: '/arsip/list-arsip-digital',
                process: '/upload/{{ $arsip->id }}',
                revert: '/delete/{{ $arsip->id }}',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }
        });


    });
</script>
@endsection