@extends('layouts.dashboard')

@section('title')
Import Arsip
@endsection

@section('content')

<section class="section">
    <div class="section-header">
        <h1>Import Arsip</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Arsip Kolektor</a></div>
            <div class="breadcrumb-item">Import Arsip</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Import Arsip</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="height: auto">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Jenis Arsip</th>
                                        <th>Format</th>
                                        <th>Import</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($jenisarsip as $item)
                                        <tr>
                                            <td>
                                                {{ $no++ }}
                                            </td>
                                            <td>
                                                {{ $item->nama }}
                                            </td>
                                            <td>
                                                <a href="{{ asset('/format/Template-Format-Import.xlsx') }}" class="btn btn-primary btn-sm"><i class="fas fa-file-download"></i></a>
                                            </td>
                                            <td>
                                                <form action="{{ route('import-arsip.store') }}" method="POST" class="" enctype="multipart/form-data">
                                                    @csrf
                                                    @if (auth()->user()->roles()->first()->name == 'admin' || auth()->user()->roles()->first()->name == 'super-admin')
                                                        <div class="form-group">
                                                            <label for="kolektor">Instansi</label>
                                                            <select name="instansi_id" id="instansi_id" class="selectpicker @error('instansi_id') is-invalid @enderror" data-show-subtext="true" data-live-search="true" required>
                                                                <option value="">Pilih Instansi</option>
                                                                @foreach ($intansi as $i)
                                                                    <option value="{{ $i->id }}">{{ $i->nama_instansi }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('instansi_id')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                        </div>
                                                    @else
                                                        <input type="hidden" name="instansi_id" value="{{ auth()->user()->instansi_id }}">
                                                    @endif
                                                    <div class="input-group">
                                                        <input type="hidden" name="jenisarsip_id" value="{{ $item->id }}">
                                                        <input type="hidden" name="jenis" value="{{ $item->nama }}">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file" name="file" id="arsip-aktif" required>
                                                            <label class="custom-file-label" for="arsip-aktif">
                                                                Pilih File Atau Browse File
                                                            </label>
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-primary" type="submit">Import</button>
                                                        </div>
                                                    </div> 
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-left">
                        <a href="{{ route('instrumen-survey.index') }}" class="btn btn-danger">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script> 
    $(document).ready(function() {
        $('.selectpicker').selectpicker();
        $('#arsip-aktif').on('change', function() {
            var fileName = $(this).val();
            $(this).next('.custom-file-label').html(fileName.replace('C:\\fakepath\\', ''));
        })
        $('#arsip-in-aktif').on('change', function() {
            var fileName = $(this).val();
            $(this).next('.custom-file-label').html(fileName.replace('C:\\fakepath\\', ''));
        })
        $('#arsip-terjaga').on('change', function() {
            var fileName = $(this).val();
            $(this).next('.custom-file-label').html(fileName.replace('C:\\fakepath\\', ''));
        })
        $('#arsip-vital').on('change', function() {
            var fileName = $(this).val();
            $(this).next('.custom-file-label').html(fileName. replace('C:\\fakepath\\', ''));
        })
    })
</script>
@if (session('success'))
<script>
    swal("Terima kasih, anda telah mengisi survey ini, selanjutnya silahkan import file arsip anda sesuai dengan format yang telah ditentukan", {
        title: "Jawaban anda telah tersimpan",
        icon: "success",
    });
</script>
@endif

@if ($message = Session::get('import-success'))
<script>
    swal("{{ $message }}", {
        title: "Sukses",
        icon: "success",
    });
</script>
@endif

@if ($message = Session::get('error'))
<script>
    swal("{{ $message }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endif

@if ($message = Session::get('import-failed'))
<script>
    swal("{{ $message }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endif

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endif

<script>
    // var options = {
    //     filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    //     filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
    //     filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    //     filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
    // };
    // CKEDITOR.replace('visi', options);
    // CKEDITOR.replace('misi', options);
    // CKEDITOR.replace('tujuan', options);
    // CKEDITOR.replace('fungsi', options);
    // CKEDITOR.replace('tugas', options);
</script>
@endsection