@extends('layouts.dashboard')

@section('title')
Kolom Pertanyaan
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Kolom Pertanyaan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data</div>
            <div class="breadcrumb-item">Kolom Pertanyaan</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Kolom Pertanyaan</h2>
        <p class="section-lead">Halaman untuk mengelola data Kolom Pertanyaan.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Kolom Pertanyaan</h4>
                <div class="card-header-action">
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-target-kinerja">Tambah Kolom Pertanyaan</button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-kolom-pertanyaan">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Kategori</th>
                                <th>Nama</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('kolom-pertanyaan.store') }}" method="POST" class="modal-part" id="modal-target-kinerja-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="kategori_pertanyaan">Pilih Kategori Pertanyaan</label>
        <select class="selecpicker" name="kategori_pertanyaan_id" id="kategori_pertanyaan">
            <option value="" disabled selected>Pilih Kategori Pertanyaan</option>
            @php
                $char = range('A', 'Z');
            @endphp

            @foreach ($kategoriPertanyaan as $item)
                <option value="{{ $item->id }}">{{ $char[$loop->index] }}. {{ $item->judul }}</option>
            @endforeach
            
        </select>

    </div>
    <div class="form-group">
        <label for="nama_kolom">Nama Kolom Pertanyaan</label>
        <input type="text" class="form-control @error('nama_kolom') is-invalid @enderror" id="nama_kolom" name="nama_kolom" min="0" placeholder="Kolom Pertanyaan">

        @error('nama_kolom')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
            Simpan
        </button>
    </div>
</form>
@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        $('#table-kolom-pertanyaan').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/kolom-pertanyaan/",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'kategori', name: 'kategori'},
                {data: 'nama_kolom', name: 'nama_kolom'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Kolom Pertanyaan berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Kolom Pertanyaan berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Kolom Pertanyaan gagal dihapus! Karena sudah terikat dengan data lain.", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Kolom Pertanyaan sudah ada, silahkan pilih tahun yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Kolom Pertanyaan berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif


@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection