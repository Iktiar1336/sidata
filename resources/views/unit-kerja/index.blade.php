@extends('layouts.dashboard')

@section('title')
    Unit Kerja
@endsection

@section('css')
    
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Unit Kerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Unit Kerja</a></div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Unit Kerja</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-workunit">Tambah Unit Kerja</button>
                  </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable" id="table-1" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Nama</th>
                                        <th>Tugas Pokok & Fungsi</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($workunit as $item)
                                        <tr>
                                          <td>{{ $item->code }}</td>
                                          <td>{{ $item->name }}</td>
                                          <td>{{ $item->description }}</td>
                                          <td>
                                              @if($item->status == 1)
                                                  <span class="badge badge-success">Aktif</span>
                                              @else
                                                  <span class="badge badge-danger">Tidak Aktif</span>
                                              @endif
                                          </td>
                                          <td>
                                              <a href="{{ route('work-units.edit', Crypt::encrypt($item->id)) }}" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                                              <form action="{{ route('work-units.destroy', Crypt::encrypt($item->id)) }}" method="POST" class="d-inline">
                                                  @csrf
                                                  @method('delete')
                                                  <button class="btn btn-danger btn-sm delete"><i class="fas fa-trash"></i></button>
                                              </form>
                                          </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('work-units.store') }}" method="POST" class="modal-part" id="modal-workunit-part" tabindex="-1">
  @csrf
  <div class="form-group">
    <label>Kode Unit:</label>
    <input type="text" class="form-control @error('code') is-invalid @enderror" placeholder="Kode Unit" value="{{ $kode }}" name="code" required readonly>
    @error('code')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
  <div class="form-group">
      <label>Nama Unit Kerja:</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Nama Unit Kerja" name="name" required>
      @error('name')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
  </div>
  <div class="form-group">
      <label>Deskripsi :</label>
      <textarea name="description" class="form-control @error('description') is-invalid @enderror" required id="description"></textarea>
      @error('description')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
  </div>
  <div class="form-group">
    <label class="d-block">Status :</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input @error('status') is-invalid @enderror" name="status" required type="radio"
            id="inlineCheckbox1" value="1" checked>
        <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input @error('status') is-invalid @enderror" name="status" required type="radio"
            id="inlineCheckbox1" value="0">
        <label class="form-check-label" for="inlineCheckbox1">Tidak Aktif</label>
    </div>
    @error('status')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
  <div class="form-group">
      <button type="submit" class="btn btn-success btn-block">Submit</button>
  </div>
</form>
@endsection

@section('js')

@if (session('insert-work-unit-success'))
<script>
    swal("Unit Kerja Berhasil Di Tambahkan", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('update-work-unit-success'))
<script>
    swal("Unit Kerja Berhasil Di Update", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-work-unit-success'))
<script>
    swal("Unit Kerja Berhasil Di Hapus", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-work-unit-failed'))
<script>
    swal("Unit Kerja Tidak Dapat Dihapus Karena Sudah Ter-Relasi Dengan Data Lain", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

<script>
    $('.delete').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Unit Kerja ini akan di hapus secara permanen!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('.selectpicker2').selectpicker();
    });
  </script>
  @if (count($errors) > 0)
  @foreach ($errors->all() as $error)
      <script>
          swal("{{ $error }}", {
              title: "Failed",
              icon: "error",
          });
      </script>
  @endforeach
@endif
@endsection