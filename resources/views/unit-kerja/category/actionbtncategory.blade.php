<div class="row">
    <a href="{{ route('categories.edit', Crypt::encrypt($row->id)) }}" class="btn btn-warning btn-sm mr-2"><i class="fas fa-pen"></i></a>
    <form action="{{ route('categories.destroy', Crypt::encrypt($row->id)) }}" method="POST">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger btn-sm delete"><i class="fas fa-trash"></i></button>
    </form>
</div>

<script>
    $('.delete').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Kategori ini akan di hapus secara permanen!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });
</script>
