<ul>
  @foreach ($subcategories as $sub)
  <li>
    {{ $sub->name }}
    @if (count($sub->subcategory))
      @include('unit-kerja.category.subcategory-treeview', ['subcategories' => $sub->subcategory])
    @endif
  </li>
  @endforeach
</ul> 