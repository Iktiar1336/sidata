@extends('layouts.dashboard')

@section('title')
Sub Kategori Data
@endsection

@section('css')
<style>
    .tree,
    .tree ul {
        margin: 0;
        padding: 0;
        list-style: none
    }

    .tree ul {
        margin-left: 1em;
        position: relative
    }

    .tree ul ul {
        margin-left: .7em
    }

    .tree ul:before {
        content: "";
        display: block;
        width: 0;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        border-left: 1px solid
    }

    .tree li {
        margin: 0;
        padding: 0 1em;
        line-height: 4em;
        font-weight: 700;
        position: relative;
        color: #6777ef;
    }

    .tree ul li:before {
        content: "";
        display: block;
        width: 10px;
        height: 0;
        border-top: 1px solid;
        margin-top: -1px;
        position: absolute;
        top: 1.9em;
        left: 0
    }

    .tree ul li:last-child:before {
        background: #fff;
        height: auto;
        top: 1.9em;
        bottom: 0
    }

    .indicator {
        margin-right: 5px;
        font-size: 17px
    }

    .tree li a {
        text-decoration: none;
        color: #369;
    }

    .tree li button,
    .tree li button:active,
    .tree li button:focus {
        text-decoration: none;
        color: #369;
        border: none;
        background: transparent;
        margin: 0px 0px 0px 0px;
        padding: 0px 0px 0px 0px;
        outline: 0;
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Sub Kategori Data</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Unit Kerja</a></div>
            <div class="breadcrumb-item">Sub Kategori Data</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Sub Kategori Data</h2>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <ul id="tree1" class="list-group">
                            @foreach ($workunit as $unitkerja)
                             @if (auth()->user()->roles->first()->name != 'super-admin' && $unitkerja->id == auth()->user()->workunit_id)
                                @if (count($unitkerja->category))
                                    <li class="list-group-item">
                                        {{ $unitkerja->name }}
                                        <ul>
                                            @foreach ($unitkerja->category as $cat)
                                                @if (auth()->user()->roles->first()->name != 'super-admin' && $cat->workunit_id == auth()->user()->workunit_id)
                                                    @if ($cat->status == 1 && $cat->parent_id == null)
                                                        <li>
                                                            {{ $cat->name }}
                                                            @if (count($cat->subcategory) > 0)
                                                                @include('unit-kerja.category.subcategory-treeview', ['subcategories' => $cat->subcategory])
                                                            @endif
                                                        </li>        
                                                    @endif
                                                @elseif(auth()->user()->roles->first()->name == 'super-admin' || auth()->user()->roles->first()->name == 'admin')
                                                    @if ($cat->status == 1 && $cat->parent_id == null)
                                                        <li>
                                                            {{ $cat->name }}
                                                            @if (count($cat->subcategory) > 0)
                                                                @include('unit-kerja.category.subcategory-treeview', ['subcategories' => $cat->subcategory])
                                                            @endif
                                                        </li>        
                                                    @endif
                                                @endif
                                        @endforeach
                                        </ul>
                                    </li>
                                @else 
                                    <li class="list-group-item">
                                        {{ $unitkerja->name }}
                                    </li>
                                @endif
                            @elseif(auth()->user()->roles->first()->name == 'super-admin' || auth()->user()->roles->first()->name == 'admin')
                                @if (count($unitkerja->category))
                                    <li class="list-group-item">
                                        {{ $unitkerja->name }}
                                        <ul>
                                            @foreach ($unitkerja->category as $cat)
                                                @if (auth()->user()->roles->first()->name != 'super-admin' && $cat->workunit_id == auth()->user()->workunit_id)
                                                    @if ($cat->status == 1 && $cat->parent_id == null)
                                                        <li>
                                                            {{ $cat->name }}
                                                            @if (count($cat->subcategory) > 0)
                                                                @include('unit-kerja.category.subcategory-treeview', ['subcategories' => $cat->subcategory])
                                                            @endif
                                                        </li>        
                                                    @endif
                                                @elseif(auth()->user()->roles->first()->name == 'super-admin' || auth()->user()->roles->first()->name == 'admin')
                                                    @if ($cat->status == 1 && $cat->parent_id == null)
                                                        <li>
                                                            {{ $cat->name }}
                                                            @if (count($cat->subcategory) > 0)
                                                                @include('unit-kerja.category.subcategory-treeview', ['subcategories' => $cat->subcategory])
                                                            @endif
                                                        </li>        
                                                    @endif
                                                @endif
                                        @endforeach
                                        </ul>
                                    </li>
                                @else 
                                    <li class="list-group-item">
                                        {{ $unitkerja->name }}
                                    </li>
                                @endif
                             @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List Sub Category Data</h4>
                        <div class="card-header-action">
                            <button class="btn btn-primary trigger--fire-modal-5 text-center"
                                id="modal-sub-category">Tambah
                                Sub Kategori Data</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable" id="table-1" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Kategori Data</th>
                                        <th>Sub Kategori Data</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $item)
                                        @if (count($item->subcategory)) 
                                            @foreach ($item->subcategory as $sub)
                                                @if (auth()->user()->roles->first()->name != 'super-admin' && $item->workunit_id == auth()->user()->workunit_id || $item->workunit_id == null)
                                                    <tr>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $sub->name }}</td>
                                                        <td>
                                                            <div class="row">
                                                                <a href="{{ route('categories.sub-category.edit', Crypt::encrypt($sub->id)) }}"
                                                                    class="btn btn-warning btn-sm mr-2"><i class="fas fa-pen"></i></a>
                                                                <form
                                                                    action="{{ route('categories.sub-category.delete', Crypt::encrypt($sub->id)) }}"
                                                                    method="POST" class="d-inline">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button class="btn btn-danger btn-sm delete"><i
                                                                            class="fas fa-trash"></i></button>
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @elseif(auth()->user()->roles->first()->name == 'super-admin' || auth()->user()->roles->first()->name == 'admin')
                                                    <tr>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $sub->name }}</td>
                                                        <td>
                                                            <div class="row">
                                                                <a href="{{ route('categories.sub-category.edit', Crypt::encrypt($sub->id)) }}"
                                                                    class="btn btn-warning btn-sm mr-2"><i class="fas fa-pen"></i></a>
                                                                <form
                                                                    action="{{ route('categories.sub-category.delete', Crypt::encrypt($sub->id)) }}"
                                                                    method="POST" class="d-inline">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button class="btn btn-danger btn-sm delete"><i
                                                                            class="fas fa-trash"></i></button>
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form action="{{ route('categories.store-subcategory') }}" method="POST" class="modal-part" id="modal-sub-kategori-data-part" tabindex="-1">
            @csrf
            <div class="form-group">
                <label>Pilih Kategori :</label>
                <select class="selectpicker2" data-show-subtext="true" data-live-search="true" name="parent_id">
                    @foreach ($categories as $item)
                        @if (auth()->user()->roles->first()->name != 'super-admin' && $item->workunit_id == auth()->user()->workunit_id || $item->workunit_id == null)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @elseif(auth()->user()->roles->first()->name == 'super-admin' || auth()->user()->roles->first()->name == 'admin')
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endif
                    @endforeach
                </select>

                @error('parent_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label>Nama Sub Kategori Data:</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror"
                    placeholder="Nama Sub Kategori Data" name="name" required>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong> 
                </span>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">Simpan</button>
            </div>
        </form>
    </div>
</section>

@endsection

@section('js')
@if (session('insert-sub-category-success'))
<script>
    swal("Sub Kategori Data Berhasil Di Tambahkan", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('update-sub-category-success'))
<script>
    swal("Sub Kategori Data Berhasil Di Update", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-sub-category-success'))
<script>
    swal("Sub Kategori Data Berhasil Di Hapus", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-sub-category-failed'))
<script>
    swal("Sub Kategori Tidak Bisa Di Hapus Karena Sudah Memiliki Sub Kategori Lagi", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

<script>
    $('.delete').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Sub Kategori ini akan di hapus secara permanen!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.selectpicker2').selectpicker();
    });
</script>
@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endif

<script>
    $.fn.extend({
        treed: function (o) {

            var openedClass = 'fa-folder-open';
            var closedClass = 'fa-folder';

            if (typeof o != 'undefined') {
                if (typeof o.openedClass != 'undefined') {
                    openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined') {
                    closedClass = o.closedClass;
                }
            };

            //initialize each of the top levels
            var tree = $(this);
            tree.addClass("tree");
            tree.find('li').has("ul").each(function () {
                var branch = $(this); //li with children ul
                branch.prepend("<i class='indicator far " + closedClass + "'></i>");
                branch.addClass('branch');
                branch.on('click', function (e) {
                    if (this == e.target) {
                        var icon = $(this).children('i:first');
                        icon.toggleClass(openedClass + " " + closedClass);
                        $(this).children().children().toggle();
                    }
                })
                branch.children().children().toggle();
            });
            //fire event from the dynamically added icon
            tree.find('.branch .indicator').each(function () {
                $(this).on('click', function () {
                    $(this).closest('li').click();
                });
            });
            //fire event to open branch if the li contains an anchor instead of text
            tree.find('.branch>a').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
            //fire event to open branch if the li contains a button instead of text
            tree.find('.branch>button').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });
    $('#tree1').treed({
        openedClass: 'fa-folder',
        closedClass: 'fa-folder-open'
    });
</script>
@endsection