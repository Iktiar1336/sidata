@extends('layouts.dashboard')

@section('title')
Kategori Data
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Kategori Data</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Unit Kerja</a></div>
            <div class="breadcrumb-item">Kategori Data</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Kategori Data</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-category">Tambah Kategori Data</button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable" id="table-category" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Unit Kerja</th>
                                        <th>Kategori Data</th>
                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Akses Informasi</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($category as $item)
                                        @if (auth()->user()->roles->first()->name != 'super-admin' && $item->workunit_id ==
                                        auth()->user()->workunit_id)
                                        <tr>
                                            <td>{{ $item->workunit->name }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>
                                                @if ($item->status == 1)
                                                <span class="badge badge-success">Aktif</span>
                                                @else
                                                <span class="badge badge-danger">Tidak Aktif</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->access == "Public")
                                                    <span class="badge badge-success">Publik</span>
                                                @else
                                                    <span class="badge badge-danger">Tidak Publik</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <a href="{{ route('categories.edit', Crypt::encrypt($item->id)) }}" class="btn btn-warning btn-sm mr-2"><i class="fas fa-pen"></i></a>
                                                    <form action="{{ route('categories.destroy', Crypt::encrypt($item->id)) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @else
                                        @if (auth()->user()->roles->first()->name == 'super-admin' ||
                                        auth()->user()->roles->first()->name == 'admin')
                                        <tr>
                                            <td>{{ $item->workunit->name }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>
                                                @if ($item->status == 1)
                                                <span class="badge badge-success">Aktif</span>
                                                @else
                                                <span class="badge badge-danger">Tidak Aktif</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->access == "Public")
                                                <span class="badge badge-success">Publik</span>
                                                @else
                                                <span class="badge badge-danger">Tidak Publik</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <a href="{{ route('categories.edit', Crypt::encrypt($item->id)) }}" class="btn btn-warning btn-sm mr-2"><i class="fas fa-pen"></i></a>
                                                    <form action="{{ route('categories.destroy', Crypt::encrypt($item->id)) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endif
                                        @endif
                                    @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('categories.store') }}" method="POST" class="modal-part" id="modal-kategori-data-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label>Pilih Unit Kerja :</label>
        <select class="selectpicker2 @error('workunit_id') is-invalid @enderror" data-show-subtext="true" name="workunit_id" data-live-search="true">
            @foreach ($workunit as $s)
            @if ($s->name != 'Administrator')
            @if ($s->id == auth()->user()->workunit_id && auth()->user()->roles->first()->name != 'super-admin')
            <option value="{{ $s->id }}">{{ $s->name }}</option>
            @elseif(auth()->user()->roles->first()->name == 'super-admin' || auth()->user()->roles->first()->name ==
            'admin')
            <option value="{{ $s->id }}">{{ $s->name }}</option>
            @endif
            @endif
            @endforeach
        </select>
        @error('workunit_id')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label>Nama Kategori Data:</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama Kategori Data" name="name" required>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label>Deskripsi :</label>
        <textarea class="form-control @error('description') is-invalid @enderror" name="description" rows="3"></textarea>
        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label>Tingkat Akses Informasi :</label>
        <select class="form-control form-control-sm @error('access') is-invalid @enderror" name="access">
            <option value="Public">Terbuka</option>
            <option value="Private">Tertutup</option>
        </select>
        @error('access')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label class="d-block">Status :</label>
    
        <div class="form-check form-check-inline">
            <input class="form-check-input @error('status') is-invalid @enderror" name="status" required type="radio" id="inlineCheckbox1" value="1" checked>
            <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input @error('status') is-invalid @enderror" name="status" required type="radio" id="inlineCheckbox1" value="0">
            <label class="form-check-label" for="inlineCheckbox1">Tidak Aktif</label>
        </div>

        @error('status')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block">Simpan</button>
    </div>
</form>
@endsection

@section('js')

@if (session('insert-category-success'))
<script>
    swal("Kategori Data Berhasil Di Tambahkan", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('update-category-success'))
<script>
    swal("Kategori Data Berhasil Di Update", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-category-success'))
<script>
    swal("Kategori Data Berhasil Di Hapus", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-category-failed'))
<script>
    swal("Kategori Data Tidak Bisa Hapus Dikarenakan Sudah Memiliki Sub Kategori", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

<script>
    $('.delete').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Kategori ini akan di hapus secara permanen!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.selectpicker2').selectpicker();
        $('#table-category').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('categories.index') }}",
            columns: [{
                    data: 'workunit',
                    name: 'workunit'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'access',
                    name: 'access'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ]
        });
    });
</script>
@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection