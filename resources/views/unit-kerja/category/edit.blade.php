@extends('layouts.dashboard')

@section('title')
Edit Kategori Data
@endsection

@section('content')

<section class="section">
    <div class="section-header">
        <h1>Edit Kategori</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Unit Kerja</a></div>
            <div class="breadcrumb-item">Edit Kategori</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Kategori</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('categories.index') }}" class="btn btn-primary">Kembali</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('categories.update', Crypt::encrypt($category->id)) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>Pilih Unit Kerja :</label>
                                <select class="selectpicker2" data-show-subtext="true" name="workunit_id"
                                    data-live-search="true">
                                    @foreach ($workunit as $s)
                                        @if ($category->workunit_id == $s->id)
                                            <option value="{{ $s->id }}" {{ $category->workunit_id == $s->id ? 'selected':''}}>{{ $s->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('workunit_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Nama Kategori Data:</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                    placeholder="Nama Kategori Data" name="name" required value="{{ $category->name }}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Deskripsi :</label>
                                <textarea class="form-control @error('description') is-invalid @enderror"
                                    name="description" rows="3">{{ $category->description }}</textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Tingkat Akses Informasi :</label>
                                <select class="form-control form-control-sm @error('access') is-invalid @enderror"
                                    name="access">
                                    <option value="Open" {{ $category->access == 'Open' ? 'selected':'' }}>Terbuka
                                    </option>
                                    <option value="Close" {{ $category->access == 'Close' ? 'selected':'' }}>Tertutup
                                    </option>
                                </select>
                                @error('access')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="d-block">Status :</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('status') is-invalid @enderror" name="status"
                                        required type="radio" id="inlineCheckbox1" value="1" {{ $category->status == '1'
                                    ? 'checked':'' }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('status') is-invalid @enderror" name="status"
                                        required type="radio" id="inlineCheckbox1" value="0" {{ $category->status == '0'
                                    ? 'checked':'' }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Tidak Aktif</label>
                                </div>
                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function () {
      $('.selectpicker2').selectpicker();
  });
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
              title: "Failed",
              icon: "error",
          });
</script>
@endforeach
@endif
@endsection