@extends('layouts.dashboard')

@section('title')
Edit Sub Kategori Data
@endsection

@section('content')

<section class="section">
    <div class="section-header">
        <h1>Edit Sub Kategori Data</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Unit Kerja</a></div>
            <div class="breadcrumb-item">Edit Sub Kategori</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Sub Kategori</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('categories.sub-category') }}" class="btn btn-primary">Kembali</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('categories.sub-category.update', Crypt::encrypt($subcategory->id)) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>Pilih Kategori :</label>
                                <select class="selectpicker2" data-show-subtext="true" data-live-search="true"
                                    name="parent_id">
                                    @foreach ($category as $c)
                                      @if ($c->id != $subcategory->id)
                                      <option value="{{ $c->id }}" {{ $subcategory->parent_id == $c->id ? 'selected':'' }}>{{ $c->name }}</option>
                                      @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Sub Kategori Data:</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                    placeholder="Nama Sub Kategori Data" name="name" required value="{{ $subcategory->name }}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        $('.selectpicker2').selectpicker();
    });
</script>
@endsection