@extends('layouts.dashboard')

@section('title')
List Kotak Data Masuk Kinerja
@endsection

@section('css')
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
    rel="stylesheet" />
@endsection

@can('Kotak Data Masuk Master Data')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Kotak Data Masuk Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Kotak Data Masuk</a></div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Kotak Data Masuk Kinerja</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable" id="table-1" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Unit Kerja</th>
                                        <th>Data Kinerja Unit</th>
                                        <th>Tahun</th>
                                        <th>Pengolah Data</th>
                                        <th>Verifikasi</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kinerja as $data)
                                    @foreach ($data as $item)
                                    <tr>
                                        <form action="{{ route('kinerja.send-penugasan') }}" method="POST"
                                            class="needs-validation was-validated">
                                            @csrf
                                            <td>{{ $item->first()->workunit->name }}</td>
                                            <td>
                                                @php
                                                    if($item->first()->category->parent_id != null){
                                                        $parentcategory = \DB::table('categories')->where('id', $item->first()->category->parent_id)->get()->first();
                                                    }
                                                @endphp
                                                @if ($item->first()->category->parent_id != null)
                                                    <div id="accordion">
                                                        <div class="accordion">
                                                            <div class="accordion-header" role="button"
                                                                data-toggle="collapse" data-target="#panel-body-{{ $item->first()->id }}"
                                                                aria-expanded="true">
                                                                <h4>{{ $item->first()->category->name }}</h4>
                                                            </div>
                                                            <div class="accordion-body collapse show" id="panel-body-{{ $item->first()->id }}"
                                                                data-parent="#accordion">
                                                                <ul>
                                                                    <li>
                                                                        {{ $parentcategory->name }}
                                                                        @include('unit-kerja.kotak-masuk.treeview', ['childs' => $parentcategory])
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    {{ $item->first()->category->name }}
                                                @endif
                                            </td>
                                            <td>{{ $item->first()->year }}</td>
                                            <td>
                                                @if ($item->first()->pengolahdata_id == null)
                                                <input type="hidden" name="kinerja_id"
                                                    value="{{ Crypt::encrypt($item->first()->id) }}">
                                                <input type="hidden" name="workunit_id"
                                                    value="{{ Crypt::encrypt($item->first()->workunit_id) }}">
                                                <input type="hidden" name="produsendata_id"
                                                    value="{{ Crypt::encrypt($item->first()->produsendata_id) }}">
                                                <input type="hidden" name="masterdata_id"
                                                    value="{{ Crypt::encrypt(auth()->user()->id) }}">
                                                <input type="hidden" name="category_id"
                                                    value="{{ Crypt::encrypt($item->first()->category_id) }}">
                                                <input type="hidden" name="year"
                                                    value="{{ Crypt::encrypt($item->first()->year) }}">
                                                <input type="hidden" name="pengolahdata_id" value="" id="pengolahdata">
                                                <select required name="pengolahdataid"
                                                    class="selectpicker @error('pengolahdata_id') is-invalid @enderror"
                                                    data-show-subtext="true" data-live-search="true"
                                                    id="pilihpengolahdata">
                                                    <option>Pilih Pengolah Data</option>
                                                    @foreach ($users as $user)
                                                    @if ($user->hasRole('pengolah-data'))
                                                    <option value="{{ Crypt::encrypt($user->id) }}">
                                                        {{$user->name }}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                                @else
                                                {{$item->first()->pengolahdata->name }}
                                                <br>
                                                <span style="font-size: 10px">Tanggal Penugasan :
                                                    @php
                                                        $process_at = \Carbon\Carbon::parse($item->first()->process_at);
                                                        $time = $process_at->format('H:i:s');
                                                    @endphp
                                                    {{$process_at->isoFormat('dddd, D MMMM Y') . ' ' . $time }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->first()->verified_at == null)
                                                -
                                                @else
                                                    @php
                                                        $verify_at = \Carbon\Carbon::parse($item->first()->verified_at);
                                                        $time = $verify_at->format('H:i:s');
                                                    @endphp
                                                    {{$verify_at->isoFormat('dddd, D MMMM Y') . ' ' . $time }}
                                                @endif
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <button type="button" class="btn btn-info btn-sm mr-2 btn-detail"
                                                        data-category-id="{{ $item->first()->category_id }}"
                                                        data-year="{{ $item->first()->year }}">
                                                        <i class="fas fa-eye"></i>
                                                    </button>
                                                    @if ($item->first()->status == 2)
                                                    <button type="submit" class="btn btn-primary btn-sm"><i
                                                            class="fas fa-paper-plane"></i></button>
                                                    @endif
                                                </div>
                                            </td>
                                        </form>
                                    </tr>
                                    @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="table-primary">
                                <th>Data Kinerja Yang Tercipta</th>
                                <th>Jumlah</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody id="detail-kinerja">

                        </tbody>
                    </table>
                    <div id="data-detail">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jqueryui-editable/js/jqueryui-editable.min.js"></script>
<script>
    $(document).ready(function () {
            $('.selectpicker').selectpicker();
        });

        $.each($('#pilihpengolahdata'), function (index, value) {
            $(value).attr('required', true);
        });

        $('#pilihpengolahdata').on('change', function () {
            var pengolahdata = $(this).val();
            if (pengolahdata != '') {
                $('#pengolahdata').val(pengolahdata);
            } else {
                $('#pengolahdata').val('');
                alert('Pilih Pengolah Data');
            }
        });
</script>

<script>
    $(document).ready(function () {
            $('.btn-detail').on('click', function () {
                var category_id = $(this).data('category-id');
                $('#detail-kinerja').empty();
                $('#data-detail').empty();
                var year = $(this).data('year');
                $.ajax({
                    url: "{{ route('kinerja.detail-kinerja') }}",
                    method: "POST",
                    data: {
                        category_id: category_id,
                        year: year,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        if(data.fileupload != null){
                            var file = "download-file-pendukung" + "/" + data.kinerja[0].fileupload.id;
                            var filename = data.kinerja[0].fileupload.filename;
                        } else {
                            var file = "#";
                            var filename = "-";
                        }

                        $('#staticBackdropLabel').text(data.kinerja[0].category.name + ' Tahun ' + data.kinerja[0].year);

                        $.each(data.kinerja, function (key, value) {
                            $('#detail-kinerja').append(
                                '<tr>' +
                                '<td>' + value.subcategory.name + '</td>' +
                                '<td>' + value.total + ' ' + value.satuan + '</td>' +
                                '<td>' + value.description + '</td>' +
                                '</tr>'
                            );
                        });

                        $('#data-detail').append(
                            '<span>File Pendukung : <a href="'+ file +'">' + filename +'</a></span>' +
                            '<br>' + '<span>Sumber Data : ' + data.kinerja[0].workunit.name + '</span>'
                        );

                        console.log(data);
                        $('#staticBackdrop').modal('show');
                    }
                });
            });
        });
</script>

@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
            title: "Gagal",
            icon: "error",
        });
</script>
@endforeach
@endsection
@endcan

@can('Kotak Data Masuk Pengolah Data')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Kotak Data Masuk Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Kotak Data Masuk</a></div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Kotak Data Masuk Kinerja</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable" id="table-kotak-masuk" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Unit Kerja</th>
                                        <th>Data Kinerja Unit</th>
                                        <th>Tahun</th>
                                        <th>Pengolah Data</th>
                                        <th>Verifikasi</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kinerja as $data)
                                        @foreach ($data as $item)
                                            @if ($item->first()->status >= 3 && $item->first()->pengolahdata_id == Auth::user()->id)
                                                <tr>
                                                    <form action="{{ route('kinerja.send-penugasan') }}" method="POST"
                                                        class="needs-validation was-validated">
                                                        @csrf
                                                        <td>{{ $item->first()->workunit->name }}</td>
                                                        <td>
                                                            @php
                                                                if($item->first()->category->parent_id != null){
                                                                    $parentcategory = \DB::table('categories')->where('id', $item->first()->category->parent_id)->get()->first();
                                                                }
                                                            @endphp
                                                            @if ($item->first()->category->parent_id != null)
                                                                <div id="accordion">
                                                                    <div class="accordion">
                                                                        <div class="accordion-header" role="button"
                                                                            data-toggle="collapse" data-target="#panel-body-{{ $item->first()->id }}"
                                                                            aria-expanded="true">
                                                                            <h4>{{ $item->first()->category->name }}</h4>
                                                                        </div>
                                                                        <div class="accordion-body collapse show" id="panel-body-{{ $item->first()->id }}"
                                                                            data-parent="#accordion">
                                                                            <ul>
                                                                                <li>
                                                                                    {{ $parentcategory->name }}
                                                                                    @include('unit-kerja.kotak-masuk.treeview', ['childs' => $parentcategory])
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @else
                                                                {{ $item->first()->category->name }}
                                                            @endif
                                                        </td>
                                                        <td>{{ $item->first()->year }}</td>
                                                        <td>
                                                            @if ($item->first()->pengolahdata_id == null)
                                                            <input type="hidden" name="kinerja_id"
                                                                value="{{ Crypt::encrypt($item->first()->id) }}">
                                                            <input type="hidden" name="workunit_id"
                                                                value="{{ Crypt::encrypt($item->first()->workunit_id) }}">
                                                            <input type="hidden" name="produsendata_id"
                                                                value="{{ Crypt::encrypt($item->first()->produsendata_id) }}">
                                                            <input type="hidden" name="masterdata_id"
                                                                value="{{ Crypt::encrypt(auth()->user()->id) }}">
                                                            <input type="hidden" name="category_id"
                                                                value="{{ Crypt::encrypt($item->first()->category_id) }}">
                                                            <input type="hidden" name="year"
                                                                value="{{ Crypt::encrypt($item->first()->year) }}">
                                                            <input type="hidden" name="pengolahdata_id" value="" id="pengolahdata">
                                                            <select required name="pengolahdataid"
                                                                class="selectpicker @error('pengolahdata_id') is-invalid @enderror"
                                                                data-show-subtext="true" data-live-search="true"
                                                                id="pilihpengolahdata">
                                                                <option>Pilih Pengolah Data</option>
                                                                @foreach ($users as $user)
                                                                @if ($user->hasRole('pengolah-data'))
                                                                <option value="{{ Crypt::encrypt($user->id) }}">
                                                                    {{$user->name }}</option>
                                                                @endif
                                                                @endforeach
                                                            </select>
                                                            @else
                                                            {{$item->first()->pengolahdata->name }}
                                                            <br>
                                                            <span style="font-size: 10px">Tanggal Penugasan :
                                                                @php
                                                                    $process_at = \Carbon\Carbon::parse($item->first()->process_at);
                                                                    $time = $process_at->format('H:i:s');
                                                                @endphp
                                                                {{$process_at->isoFormat('dddd, D MMMM Y') . ' ' . $time }}</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if ($item->first()->verified_at == null)
                                                            -
                                                            @else
                                                            @php
                                                                $verify_at = \Carbon\Carbon::parse($item->first()->verified_at);
                                                                $time = $verify_at->format('H:i:s');
                                                            @endphp
                                                            {{$verify_at->isoFormat('dddd, D MMMM Y') . ' ' . $time }}</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="row">
                                                                <button type="button" class="btn btn-info btn-sm mr-2 btn-detail"
                                                                    data-category-id="{{ $item->first()->category_id }}"
                                                                    data-year="{{ $item->first()->year }}">
                                                                    <i class="fas fa-eye"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-info btn-sm mr-2 btn-chat"
                                                                    data-category-name="{{ $item->first()->category->name }}" data-category-id="{{ $item->first()->category->id }}" data-year="{{ $item->first()->year }}" data-chatid="{{ $item->first()->chat_id }}" data-produsendata-id="{{ $item->first()->produsendata_id }}">
                                                                    <i class="fas fa-comment-alt"></i>
                                                                </button>
                                                                @if ($item->first()->status == 2)
                                                                <button type="submit" class="btn btn-primary btn-sm">
                                                                    <i class="fas fa-paper-plane"></i></button>
                                                                @endif
                                                            </div> 
                                                        </td>
                                                    </form>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('kinerja.verifikasi-kinerja') }}" method="POST" id="form">
                @csrf
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="table-primary">
                                    <th>Data Kinerja Yang Tercipta</th>
                                    <th>Jumlah</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody id="detail-kinerja">

                            </tbody>
                        </table>
                        <div id="data-detail">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="chat-modal" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="topic-chat" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content chat-box card-success" id="mychatbox2">
            <div class="modal-header">
                <h5 id="topic-chat" class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="chat-content pl-3 pr-3 mb-3">

                </div>
                <div class="chat-form">
                    <form id="chat-form2">
                        <input type="text" class="form-control" name="message" id="message" placeholder="Masukan Pesan Anda">
                        <button class="btn btn-primary" id="btn-send-chat">
                            <i class="far fa-paper-plane"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    "use strict";

    $(document).ready(function () {
        $('.selectpicker').selectpicker();

        $('#table-kotak-masuk').DataTable({
            "autoWidth": true,
        });
    });

    $.each($('#pilihpengolahdata'), function (index, value) {
        $(value).attr('required', true);
    });

    $('#pilihpengolahdata').on('change', function () {
        var pengolahdata = $(this).val();
        if (pengolahdata != '') {
            $('#pengolahdata').val(pengolahdata);
        } else {
            $('#pengolahdata').val('');
            alert('Pilih Pengolah Data');
        }
    });

    var chats = []; 

    $('.btn-detail').on('click', function () {
        var category_id = $(this).data('category-id');
        $('#detail-kinerja').empty();
        $('#data-detail').empty();
        $('.modal-footer').empty();
        $('.modal-footer').append('<button type="button" class="btn btn-danger btn-md" data-dismiss="modal">Close</button>');
        var year = $(this).data('year');
        $.ajax({
            url: "{{ route('kinerja.detail-kinerja') }}",
            method: "POST",
            data: {
                category_id: category_id,
                year: year,
                _token: "{{ csrf_token() }}"
            },
            success: function (data) {
                if (data.fileupload != null) {
                    var file = "download-file-pendukung" + "/" + data.kinerja[0].fileupload.id;
                    var filename = data.kinerja[0].fileupload.filename;
                } else {
                    var file = "#";
                    var filename = "-";
                }

                $('#staticBackdropLabel').text(data.kinerja[0].category.name + ' Tahun ' + data.kinerja[0].year);

                $.each(data.kinerja, function (key, value) {

                    if (value.verified_at == null) {
                        var opsiedit = '<i class="fas fa-pen" onclick="edit(' + value.id + ', ' + value.total + ')"></i>'
                    } else {
                        var opsiedit = '-'
                    }

                    if (value.satuan == null) {
                        var satuan = ' '
                    } else {
                        var satuan = value.satuan
                    }

                    $('#detail-kinerja').append(
                        '<tr>' +
                        '<td>' + value.subcategory.name + '</td>' +
                        '<td>' + value.total + ' ' + satuan + '</td>' +
                        '<td>' + value.description + '</td>' +
                        //'<td>' + opsiedit + '</td>' +
                        // '<td>' + value.total + '</td>' +
                        '</tr>'
                    );
                });

                if (data.kinerja[0].verified_at == null) {
                    $('#data-detail').append(
                        '<span>File Pendukung : <a href="' + file + '" >' + filename + '</a></span>' +
                        '<br>' + '<span>Sumber Data : ' + data.kinerja[0].workunit.name + '</span>'
                    );

                    $('#form').append(
                        '<input type="hidden" name="category_id" value="' + data.kinerja[0].category_id + '">' +
                        '<input type="hidden" name="year" value="' + data.kinerja[0].year + '">' +
                        '<input type="hidden" name="pengolahdata_id" value="' + data.kinerja[0].pengolahdata_id + '">'
                    );

                    if (data.kinerja[0].status == 5) {
                        var buttonsubmit = '<button type="submit" class="btn btn-primary btn-md disabled" disabled>Buka Akses Edit</button>';
                        var buttonverifikasi = '<button type="submit" class="btn btn-success btn-md disabled" disabled>Verifikasi</button>';
                    } else {
                        var buttonsubmit = '<button type="submit" class="btn btn-primary btn-md">Buka Akses Edit</button>';
                        var buttonverifikasi = '<button type="submit" class="btn btn-success btn-md">Verifikasi</button>';

                    };

                    $('.modal-footer').append(
                        buttonverifikasi
                    );

                    $('.modal-footer').append(
                        "<form action='{{ route('kinerja.openaccessedit-kinerja') }}' method='POST'>" +
                        '@csrf' +
                        '<input type="hidden" name="category_id" value="' + data.kinerja[0].category_id + '">' +
                        '<input type="hidden" name="year" value="' + data.kinerja[0].year + '">' +
                        '<input type="hidden" name="pengolahdata_id" value="' + data.kinerja[0].pengolahdata_id + '">' +
                        buttonsubmit +
                        "</form>"
                    );
                } else {
                    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                    var tanggal = new Date(data.kinerja[0].verified_at);
                    var xhari = tanggal.getDay();
                    var xtanggal = tanggal.getDate();
                    var xbulan = tanggal.getMonth();
                    var xtahun = tanggal.getFullYear();
                    var jam = tanggal.getHours();
                    var menit = tanggal.getMinutes().toString().padStart(2, '0');
                    var detik = tanggal.getSeconds().toString().padStart(2, '0');
                    var tgl = hari[xhari] + ', ' + xtanggal + ' ' + bulan[xbulan] + ' ' + xtahun + ' ' + jam + ':' + menit + ':' + detik;
                    $('#data-detail').append(
                        '<span>Sumber Data : ' + data.kinerja[0].workunit.name + '</span>' +
                        '<br>' + '<span><b>Tanggal Verifikasi : ' + tgl + '</b></span>'
                    );
                }
                $('#staticBackdrop').modal('show');
            }
        });
    });

    function edit(id, total) {
        var Jumlah = prompt("Masukkan Jumlah", total);
        if (Jumlah != null) {
            $.ajax({
                url: "{{ route('kinerja.update-kinerja') }}",
                method: "POST",
                data: {
                    id: id,
                    total: Jumlah,
                    _token: "{{ csrf_token() }}"
                },
                success: function (data) {
                    alert('Data Berhasil Diubah');
                    location.reload();
                }
            });
        }
    }

    $('.close').on('click', function () {
        $('.chat-content').empty();
        for (var a = 0; a < chats.length; a++) {
            delete chats[a];
            chats.length = 0;
        }
    });
    
    $('.btn-chat').on('click', function () {
        var categoryname = $(this).data('category-name');
        var year = $(this).data('year');
        var user_id = "{{ Auth::user()->id }}";
        var topic = 'Kinerja ' + categoryname + ' Tahun ' + year;
        var chatid = $(this).data('chatid');
        $.ajax({
            url: "{{ route('chat.get-chat') }}",
            method: "POST",
            data: {
                chatid: chatid,
                user_id: user_id,
                _token: "{{ csrf_token() }}"
            },
            success: function (data) {
                $.each(data.messages, function(key,value) {
                    var date = new Date(value.created_at);
                    var hours = date.getHours();
                    var minutes = "0" + date.getMinutes();
                    var seconds = "0" + date.getSeconds();

                    if(value.sender_id == {{ Auth::user()->id }}) {
                        var position = 'right';
                        var pic = value.sender.avatar;
                    } else {
                        var position = 'left';
                        var pic = value.receiver.avatar;
                    }

                    chats.push({
                        text: value.message,
                        position: position,
                        type: 'text',
                        day: value.created_at,
                        time: hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2),
                        picture: pic
                    });

                })
                for(var i = 0; i < chats.length; i++) {
                    var type = 'text';
                
                    if(chats[i].typing != undefined) type = 'typing';
                    $.chatCtrl('#mychatbox2', {
                        text: (chats[i].text != undefined ? chats[i].text : ''),
                        // picture: (chats[i].position == 'left' ? '../assets/img/avatar/avatar-5.png' : '../assets/img/avatar/avatar-2.png'),
                        picture: chats[i].picture,
                        position: 'chat-'+chats[i].position,
                        time: (chats[i].time != undefined ? chats[i].time : ''),
                        type: type
                    });
                }
                console.log(chats);
            }
        });
        $('#topic-chat').html(topic);
        $('#chat-modal').modal('show');
    });

    $('#chat-form2').submit(function() {
        var me = $(this);
        var chatid = $('.btn-chat').data('chatid');
        var receiver_id = $('.btn-chat').data('produsendata-id');
        var message = $('#message').val();
        $.ajax({
            url: "{{ route('chat.store') }}",
            method: "POST",
            data: {
                chatid: chatid,
                receiver_id: receiver_id,
                message: message,
                _token: "{{ csrf_token() }}"
            },
            success: function (data) {
                if(me.find('input').val().trim().length > 0) {
                    $.chatCtrl('#mychatbox2', {
                        text: me.find('input').val(),
                        picture: data.picture,
                    });
                    me.find('input').val('');
                    
                }
            }
        });
        return false;
    })
</script>

@foreach ($errors->all() as $error)
    <script>
        swal("{{ $error }}", {
                title: "Gagal",
                icon: "error",
            });
    </script>
@endforeach

@endsection
@endcan