@php
    if ($childs->parent_id != null) {
        $parent = \DB::table('categories')->where('id', $childs->parent_id)->get()->first();
    }
@endphp

<ul>
    @if ($childs->parent_id != null)
        <li>
            {{ $parent->name }}
            @include('unit-kerja.kotak-masuk.treeview', ['childs' => $parent])
        </li>
    @endif
</ul>