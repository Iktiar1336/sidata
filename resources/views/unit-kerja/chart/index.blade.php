@extends('layouts.dashboard')

@section('title')
Chart Data Kinerja
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Chart Data Kinerja</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">Unit Kerja</a></div>
      <div class="breadcrumb-item">Chart Data Kinerja</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Chart Data Kinerja</h2>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <div class="form-group">
              <select class="selectpicker2 @error('category_id') is-invalid @enderror" data-show-subtext="true"
                data-live-search="true" id="category" name="category_id" {{ $category->count() == 0 ? 'disabled':'' }}>
                <option value="">Pilih Kategori</option> 
                @forelse ($category as $item)
                  @if ($item->kinerja->count() > 0)
                      @forelse ($item->kinerja as $kinerja)
                        @if ($kinerja->status == 4)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                          @break; 
                        @endif
                      @empty
                        <option value="" disabled selected>Tidak Ada Kategori</option> 
                      @endforelse
                  @endif
                @empty
                  <option value="" disabled selected>Tidak Ada Kategori</option> 
                @endforelse
              </select>
            </div>
            <div id="chart"></div>
            <div id="table" class="table-responsive mt-5">

            </div>
            <div id="metadata"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script >
    $(document).ready(function () {
        $('.selectpicker2').selectpicker();

        $('#category').on('change', function () {
            var category_id = $(this).val();
            var startyear = <?php echo json_encode($year)?>;
            console.log(startyear);
            if (category_id) {
                $.ajax({
                    url: "{{ route('chart-kinerja.get') }}",
                    type: "GET",
                    dataType: "json",
                    data: {
                        category_id: category_id
                    },
                    success: function (data) {
                        console.log(data);
                        var chart = Highcharts.chart('chart', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Grafik ' + data.category 
                            },
                            subtitle: {
                                text: 'Source:' + data.sumber
                            },
                            xAxis: {
                                categories: data.categories,
                                crosshair: true
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Jumlah Kinerja'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
                                    '<td style="padding:0"><b> {point.y:1f} </b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                        });
                        $.each(data.series, function (key, value){
                          console.log(value);
                          chart.addSeries({
                            name: key,
                            data: value
                          });
                        });
                        $('#table').html(data.table);
                        $("[data-toggle=popover]").popover();
                    }
                });
            } else {
                alert('danger');
            }
        });
    }); 
</script>
@endsection