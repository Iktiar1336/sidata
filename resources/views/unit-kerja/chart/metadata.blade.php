@php
    if ($categories->parent_id != null) {
        $parent = \DB::table('categories')->where('id', $childs->parent_id)->get()->first();
    }
@endphp

<ul>
    @if ($categories->parent_id != null)
        <li>
            {{ $parent->name }}
            @include('unit-kerja.chart.metadata', ['categories' => $parent])
        </li>
    @endif
</ul>