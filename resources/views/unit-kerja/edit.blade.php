@extends('layouts.dashboard')

@section('title')
Edit Unit Kerja
@endsection

@section('content')

<section class="section">
    <div class="section-header">
        <h1>Edit Unit Kerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Unit Kerja</a></div>
            <div class="breadcrumb-item">Edit Unit Kerja</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Unit Kerja</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('work-units.index') }}" class="btn btn-primary">Kembali</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('work-units.update', Crypt::encrypt($workunit->id)) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>Kode Unit:</label>
                                <input type="text" class="form-control @error('code') is-invalid @enderror"
                                    placeholder="Kode Unit" value="{{ $workunit->code }}" name="code" required readonly>
                                @error('code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Nama Unit Kerja:</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                    value="{{ $workunit->name }}" placeholder="Nama Unit Kerja" name="name" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Deskripsi :</label>
                                <textarea name="description"
                                    class="form-control @error('description') is-invalid @enderror" required
                                    id="description">{{ $workunit->description }}</textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="d-block">Status :</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('status') is-invalid @enderror" name="status"
                                        required type="radio" id="inlineCheckbox1" value="1" {{ $workunit->status == '1' ? 'checked':'' }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('status') is-invalid @enderror" name="status"
                                        required type="radio" id="inlineCheckbox1" value="0" {{ $workunit->status == '0' ? 'checked':'' }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Tidak Aktif</label>
                                </div>
                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        $('.selectpicker2').selectpicker();
    });
</script>
@endsection