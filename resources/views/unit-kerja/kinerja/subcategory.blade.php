@foreach ($subcategories as $sub)
<option value="{{ Crypt::encrypt($sub->id) }}">{{ $sub->name }}</option>
@if (count($sub->subcategory))
@include('unit-kerja.kinerja.subcategory', ['subcategories' => $sub->subcategory])
@endif
@endforeach