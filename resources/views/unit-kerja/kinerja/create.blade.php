@extends('layouts.dashboard')

@section('title')
Tambah Data Kinerja
@endsection

@section('css')

@endsection

@section('content')
<div class="section">
  <div class="section-header">
    <h1>Tambah Data Kinerja</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
      <div class="breadcrumb-item">Tambah Data Kinerja</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Tambah Data Kinerja</h2>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <a href="{{ route('kinerja.index') }}" class="btn btn-primary">Kembali</a>
          </div>
          <div class="card-body">
            <form action="{{ route('kinerja.store') }}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <input type="hidden" name="workunit_id" value="{{ auth()->user()->workunit->id }}">
                <label>Pilih Kategori :</label>
                <select class="selectpicker2 @error('category_id') is-invalid @enderror" data-show-subtext="true"
                  data-live-search="true" id="category" name="category_id[]">
                  <option value="" selected>Pilih Kategori</option>
                  @foreach ($category as $c)
                  @if (auth()->user()->roles->first()->name != 'super-admin' && $c->workunit_id ==
                  auth()->user()->workunit_id)
                  <option value="{{ $c->id }}">{{ $c->name }}</option>
                  @elseif (auth()->user()->roles->first()->name == 'super-admin' || auth()->user()->roles->first()->name
                  == 'admin')
                  <option value="{{ $c->id }}">{{ $c->name }}</option>
                  @endif
                  @endforeach
                </select>
                @error('parent_id')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
              <div class="form-group mb-3" id="form-subcategory">
                <label>Pilih Sub Kategori :</label>
                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                <select id="subcategory" class="selectpicker @error('category_id') is-invalid @enderror"
                  name="category_id[]" data-show-subtext="true" data-live-search="true">
                </select>
              </div>
              <div id="form">

              </div>
              <div class="list-group" id="listdata">

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.20.0/basic/ckeditor.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function () {
      var formgroup = '<div class="form-group">';
      var formgroupclose = '</div>';
      var date = new Date();
      var year = date.getFullYear();
      var year = '<input type="number" class="form-control mb-4 @error('year ') is-invalid @enderror" name="year" value="'+year+'" id="year" placeholder="Tahun">';
      var button = '<button type="submit" class="btn btn-success mt-4" id="add">Simpan</button>';
      var inputfile = '<input type="file" name="file" class="form-control mt-4 @error('file') is-invalid @enderror" id="file">';
      inputfile += '<small id="emailHelp" class="form-text text-danger">Upload file bukti pendukung hanya diperbolehkan dengan format (xls, xlsx, csv) max : 2 MB</small>'
      $('.selectpicker2').selectpicker();
      $('#form-subcategory').hide();
      $('#category').on('change', function (e) {
          var category_id = e.target.value;
          if (category_id != '') {
            getSubCategory(category_id);
            $('#subcategory').empty();
          } else {
              $('#form-subcategory').hide();
              $('#subcategory').empty();
              $('#listdata').empty();
          }
      });

      function getSubCategory(id) {
        $.ajax({
              url: "{{ route('kinerja.fetchsubcategory') }}",
              type: "POST",
              data: {
                  category_id: id
              },
              success: function (result) {
                  var html = '';
                  var datasub = '';
                  $('#form-subcategory').hide();
                  $('#subcategory').empty();
                  $('#listdata').empty();
                  if (result.subcategories[0].subcategory.length > 0) {
                    $('#subcategory').append('<option value="">Pilih Sub Kategori</option>');
                    html += year
                    var idket = 1;
                    $.each(result.subcategories[0].subcategory, function (key, value) {
                        if (value.subcategory.length > 0) {
                          $('#form-subcategory').show();
                          $('#subcategory').append('<option value="' + value.id + '">' + value.name + '</option>');
                          $('.selectpicker').selectpicker('refresh');
                        } else {
                          // $('#subcategory').append('<option value="' + value.id + '">' + value.name + '</option>');
                          // $('.selectpicker').selectpicker('refresh');
                          var oneplus = idket+1;
                          html += '<input type="hidden" name="category_id[]" value="'+id+'">';
                          html += '<li class="list-group-item">' + value.name + '<input type="text" id="satuan" name="satuan[]" placeholder="Satuan Data" class="form-control text-small float-right col-md-3 @error('satuan') is-invalid @enderror" required> <input type="number" step="any" value="0" min="0" style="width:110px;" id="subdata" name="total[]" class="form-control text-small float-right col-md-3 @error('total ') is-invalid @enderror"></li>';
                          html += '<input type="hidden" id="idsub" name="subcategory_id[]" value="' + value.id + '" >';
                          html += '<li class="list-group-item">Keterangan : <textarea name="description[]" id="ket_'+oneplus+'" class="form-control mb-2 mt-3 @error('description') is-invalid @enderror keterangan"></textarea></li>';
                          $('#listdata').html(html);
                          idket = idket+1;
                          oneplus++;
                        }
                    });
                    html += inputfile;
                    html += button;
                    $('#listdata').html(html);
                    $('.keterangan').each(function () {
                      CKEDITOR.replace(this.id);
                    });
                  } else {
                    $('#form-subcategory').hide();
                    $('#subcategory').empty();
                    $('#listdata').empty();
                  }
              }
          });
      }

      $('#subcategory').on('change', function (a) {
          $('#form').empty();
          if ($(this).val() == '') {
              $('#listdata').html('');
          } else {
              var category_id = a.target.value;
              var html = '';
              var datasub = '';
              $('#listdata').html(html);
              $.ajax({
                  url: "{{ route('kinerja.getsubCategory') }}",
                  type: "POST",
                  data: {
                      category_id: category_id
                  },
                  success: function (data) {
                      if (data.subcategories[0].subcategory.length > 0) {
                          html += year;
                          var datasubcategory = [];
                          var idket = 1;
                          $.each(data.subcategories[0].subcategory, function (index,
                              subcategory) {
                              if (subcategory.subcategory.length > 0) {
                                  datasubcategory.push(subcategory);
                              } else {
                                var oneplus = idket+1;
                                html += '<li class="list-group-item">' + subcategory.name + '<input type="text" id="satuan" name="satuan[]" placeholder="Satuan Data" class="form-control text-small float-right col-md-3 @error('satuan') is-invalid @enderror"> <input type="number" step="any" value="0" min="0" style="width:110px;" id="subdata" name="total[]" class="form-control text-small float-right col-md-3 @error('total ') is-invalid @enderror"></li>';
                                html += '<input type="hidden" id="idsub" name="subcategory_id[]" value="' + subcategory.id + '" >';
                                html += '<li class="list-group-item">Keterangan : <textarea name="description[]" id="ket_'+oneplus+'" class="form-control mb-2 mt-3 @error('description') is-invalid @enderror keterangan"></textarea></li>';
                                $('#listdata').html(html);
                                idket = idket+1;
                                oneplus++;
                              }
                          });
                          if (datasubcategory.length > 0) {
                            getMultiSubCategory(datasubcategory);
                          } else {
                            html += inputfile;
                            html += button;
                            $('#listdata').html(html);
                            $('.keterangan').each(function () {
                              CKEDITOR.replace(this.id);
                            });
                          }
                          
                      } else {
                          html += '<li class="list-group-item text-danger">Tidak ada data <br> Silahkan Tambah Sub Kategori Terlebih Dahulu.</li>';
                          $('#listdata').html(html);
                      }
                  },
              });
          }
      });

      function getMultiSubCategory(data) {
        var id = 0;
        var multisubid = '';
        var element = `
            <div class="form-group">
              <label for="category">Pilih Sub Kategori</label>
              <select id="multi-sub" class="selectpicker subcategory @error('category_id') is-invalid @enderror" name="category_id[]" data-show-subtext="true" data-live-search="true">
                <option value="">Pilih Sub Kategori</option>
              </select>
            </div>
        `;
        $('#form').append(element);
        $('.subcategory').each(function(index, el){
          id++;
          var newid = 'subcategory-' + id;
          multisubid = newid;
          $(this).attr('id', newid);
        })
        $.each(data, function (index, subcategory) {
          var option = `
            <option value="${subcategory.id}">${subcategory.name}</option>
          `;
          $('#' + multisubid).append(option);
          $('.selectpicker').selectpicker('refresh');
        });
        $('#' + multisubid).on('change', function (a) {
          if ($(this).val() == '') {
              $('#listdata').html('');
          } else {
              var category_id = a.target.value;
              var html = '';
              var datasub = '';
              $('#listdata').html(html);
              $.ajax({
                  url: "{{ route('kinerja.getsubCategory') }}",
                  type: "POST",
                  data: {
                      category_id: category_id
                  },
                  success: function (data) {
                      if (data.subcategories[0].subcategory.length > 0) {
                          html += year;
                          var datasubcategory = [];
                          $.each(data.subcategories[0].subcategory, function (index,
                              subcategory) {
                              if (subcategory.subcategory.length > 0) {
                                datasubcategory.push(subcategory);
                              } else {
                                html += '<li class="list-group-item">' + subcategory.name + '<input type="text" id="satuan" name="satuan[]" placeholder="Satuan Data" class="form-control text-small float-right col-md-3 @error('satuan') is-invalid @enderror"> <input type="number" step="any" value="0" min="0" style="width:110px;" id="subdata" name="total[]" class="form-control text-small float-right col-md-3 @error('total ') is-invalid @enderror"></li>';
                                html += '<input type="hidden" id="idsub" name="subcategory_id[]" value="' + subcategory.id + '" >';
                                html += '<li class="list-group-item">Keterangan : <textarea name="description[]" id="ket" class="form-control mb-2 mt-3 @error('description') is-invalid @enderror"></textarea></li>';
                                $('#listdata').html(html);
                              }
                          });
                          if (datasubcategory.length > 0) {
                            $('#' + multisubid).attr('disabled', true);
                            getMultiSubCategory(datasubcategory);
                          } else {
                            html += inputfile;
                            html += button;
                            $('#listdata').html(html);
                          }
                      } else {
                          html += '<li class="list-group-item text-danger">Tidak ada data <br> Silahkan Tambah Sub Kategori Terlebih Dahulu.</li>';
                          $('#listdata').html(html);
                      }
                  },
              });
          }
        });
      }
    });
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
  swal("{{ $error }}", {
            title: "Failed",
            icon: "error",
        });
</script>
@endforeach
@endif

@if (session('error'))
<script>
  swal("{{ session('error') }}", {
          title: "Failed",
          icon: "error",
      });
</script>
@endif
@endsection