@extends('layouts.dashboard')

@section('title')
Data Kinerja
@endsection

@section('css')
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Data Kinerja</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Unit Kerja</a></div>
            <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Data Kinerja</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    @if (auth()->user()->roles->first()->name != 'super-admin')
                    <div class="card-header">
                        <a href="{{ route('kinerja.create') }}" class="btn btn-primary">Tambah Data Kinerja</a>
                    </div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable table-bordered" id="table-kinerja" width="150%" cellspasing="0">
                                <thead>
                                    <tr>
                                        <th>Kategori Data</th>
                                        <th>Sub Category Data</th>
                                        <th>Tahun</th>
                                        <th>Jumlah</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kinerja as $item)
                                        @foreach ($item as $a)
                                            <tr>
                                                <td>
                                                    @php
                                                        if($a->first()->category->parent_id != null){
                                                            $parentcategory = \DB::table('categories')->where('id', $a->first()->category->parent_id)->get()->first();
                                                        }
                                                    @endphp
                                                    @if ($a->first()->category->parent_id != null)
                                                        <div id="accordion">
                                                            <div class="accordion">
                                                                <div class="accordion-header" role="button"
                                                                    data-toggle="collapse" data-target="#panel-body-{{ $a->first()->category->id }}"
                                                                    aria-expanded="true">
                                                                    <h4>{{ $a->first()->category->name }}</h4>
                                                                </div>
                                                                <div class="accordion-body collapse show" id="panel-body-{{ $a->first()->category->id }}"
                                                                    data-parent="#accordion">
                                                                    <ul>
                                                                        <li>
                                                                            {{ $parentcategory->name }}
                                                                            @include('unit-kerja.kotak-masuk.treeview', ['childs' => $parentcategory])
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        {{ $a->first()->category->name }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <ul class="list-group list-group-flush">
                                                        @foreach ($a as $b)
                                                        <li class="list-group-item">{{ $b->subcategory->name }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group list-group-flush">
                                                        @foreach ($a as $b)
                                                        <li class="list-group-item">{{ $b->year }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group list-group-flush">
                                                        @foreach ($a as $b)
                                                        <li class="list-group-item">{{ $b->total . ' ' . $b->satuan }} </li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group list-group-flush">
                                                        @foreach ($a as $b)
                                                        <li class="list-group-item">
                                                            <button tabindex="0" type="button" class="btn btn-primary btn-sm" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom" data-html="true" data-content="{{  $b->description  }}"> <i class="fas fa-eye"></i> </button>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group list-group-flush">
                                                        @foreach ($a as $b)
                                                        <li class="list-group-item">
                                                            @if ($b->status == 1)
                                                            <i class="fas fa-star text-warning"></i>
                                                            @elseif ($b->status == 2)
                                                            <i class="fas fa-star text-warning"></i>
                                                            <i class="fas fa-star text-warning"></i>
                                                            @elseif ($b->status == 3 || $b->status == 5)
                                                            <i class="fas fa-star text-warning"></i>
                                                            <i class="fas fa-star text-warning"></i>
                                                            <i class="fas fa-star text-warning"></i>
                                                            @elseif($b->status == 4)
                                                            <i class="fas fa-star text-warning"></i>
                                                            <i class="fas fa-star text-warning"></i>
                                                            <i class="fas fa-star text-warning"></i>
                                                            <i class="fas fa-star text-warning"></i>
                                                            @endif
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    @if ($a->first()->produsendata_id == auth()->user()->id)
                                                        <div class="dropdown d-inline">
                                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Opsi
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                @if ($b->status == 1 || $b->status == 5)
                                                                    <a href="{{ route('kinerja.edit', Crypt::encrypt($b->id)) }}" class="dropdown-item has-icon text-warning {{ $b->status != 1 && $b->status != 5 ? 'btn-disabled':'' }}">
                                                                        <i class="fas fa-pen mt-1"></i>  Edit
                                                                    </a>
                                                                @endif
                                                                @can('Detail Data Kinerja')
                                                                    <button type="button" class="dropdown-item has-icon text-primary btn-detail" data-category-id="{{ $a->first()->category_id }}" data-year="{{ $a->first()->year }}">
                                                                        <i class="fas fa-eye mt-1"></i> Detail
                                                                    </button>
                                                                @endcan
                                                                @if ($b->status == 1)
                                                                    <form action="{{ route('kinerja.destroy', Crypt::encrypt($b->category_id)) }}" method="POST" class="d-inline">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <input type="hidden" name="year" value="{{ $b->year }}">
                                                                        <input type="hidden" name="kinerja_id" value="{{ $b->id }}">
                                                                        <button type="submit" class="dropdown-item has-icon text-danger" {{ $b->status != 1 ? 'disabled':'' }}> <i class="fas fa-trash mt-1"></i> Hapus</button>
                                                                    </form>
                                                                    <form action="{{ route('kinerja.send-kinerja', Crypt::encrypt($b->category_id)) }}" method="POST" class="d-inline">
                                                                        @csrf
                                                                        <input type="hidden" name="year" value="{{ $b->year }}">
                                                                        <input type="hidden" name="kinerja_id" value="{{ $b->id }}">
                                                                        <button type="submit" class="dropdown-item has-icon text-primary {{ $b->status != 1 ? 'disabled':'' }} send"> 
                                                                            <i class="fas fa-paper-plane mt-1"></i> Kirim
                                                                        </button>
                                                                    </form>
                                                                @endif
                                                                @if ($b->status == 3 || $b->status == 5)
                                                                    <button type="button" id="btn-chat" class="dropdown-item has-icon text-info btn-chat" data-toggle="modal" data-category-name="{{ $a->first()->category->name }}" data-category-id="{{ $a->first()->category->id }}" data-year="{{ $a->first()->year }}" data-chatid="{{ $a->first()->chat_id }}" data-produsendata-id="{{ $a->first()->produsendata_id }}" data-pengolahdata-id="{{ $a->first()->pengolahdata_id }}" data-produsendata-name="{{ $a->first()->produsendata->name }}" data-target="#exampleModal-chat" {{ $b->status != 3 ? 'disabled':'' }}>
                                                                        <i class="fas fa-comment-alt mt-1"></i> Chat
                                                                    </button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                            <p>
                                Keterangan : <br>
                                <i class="fas fa-star text-warning"></i> x 1 : <span class="text-lead"
                                    style="font-weight: 400">Tersimpan</span>
                                <i class="fas fa-star text-warning"></i> x 2 : <span class="text-lead"
                                    style="font-weight: 400">Terkirim</span>
                                <i class="fas fa-star text-warning"></i> x 3 : <span class="text-lead"
                                    style="font-weight: 400">Sedang Di Proses</span>
                                <i class="fas fa-star text-warning"></i> x 4 : <span class="text-lead"
                                    style="font-weight: 400">Diverifikasi</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('kinerja.verifikasi-kinerja') }}" method="POST" id="form">
                @csrf
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="table-primary">
                                    <th>Data Kinerja Yang Tercipta</th>
                                    <th>Jumlah</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody id="detail-kinerja">

                            </tbody>
                        </table>
                        <div id="data-detail">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="chat-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="topic-chat" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content chat-box card-success" id="mychatbox2">
            <div class="modal-header">
                <h5 id="topic-chat" class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="chat-content pl-3 pr-3 mb-3">

                </div>
                <div class="chat-form">
                    <form id="chat-form2">
                        <input type="text" class="form-control" name="message" id="message" placeholder="Masukan Pesan Anda">
                        <button class="btn btn-primary" id="btn-send-chat">
                            <i class="far fa-paper-plane"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@if (session('insert'))
<script>
    swal("Data kinerja berhasil di tambahkan", {
        title: "Berhasil",
        icon: "success",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Data kinerja berhasil di update", {
        title: "Berhasil",
        icon: "success",
    });
</script>
@endif

@if (session('delete'))
<script>
    swal("Data kinerja berhasil di hapus", {
        title: "Berhasil",
        icon: "success",
    });
</script>
@endif

@if (session('delete'))
<script>
    swal("Data kinerja tidak dapat dihapus karena sudah ter-relasi dengan data lain", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endif

@if (session('send-work-unit-success'))
<script>
    swal("Data kinerja berhasil di kirim, silahkan tunggu notifikasi selanjutnya", {
        title: "Berhasil",
        icon: "success",
    });
</script>
@endif

<script>
    $(document).ready(function() {
      $('.selectpicker2').selectpicker();
      $('.btn-disabled').removeAttr('href');
    });
</script>
@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
              title: "Gagal",
              icon: "error",
          });
</script>
@endforeach
@endif

<script>
    $(document).ready(function() {
        $('#table-kinerja').DataTable({
            autoWidth: false,
        });

    });

    var chats = []; 
    $('.close').on('click', function () {
        $('.chat-content').empty();
        for (var a = 0; a < chats.length; a++) {
            delete chats[a];
            chats.length = 0;
        }
    });

    $('.btn-detail').on('click', function () {
        var category_id = $(this).data('category-id');
        $('#detail-kinerja').empty();
        $('#data-detail').empty();
        $('.modal-footer').empty();
        $('.modal-footer').append('<button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Close</button>');
        var year = $(this).data('year');
        $.ajax({
            url: "{{ route('kinerja.detail-kinerja') }}",
            method: "POST",
            data: {
                category_id: category_id,
                year: year,
                _token: "{{ csrf_token() }}"
            },
            success: function (data) {
                console.log(data);
                if (data.fileupload != null) {
                    var file = "{{ asset('uploads/kinerja') }}" + "/" + data.kinerja[0].fileupload.filename;
                    var filename = data.kinerja[0].fileupload.filename;
                } else {
                    var file = "#";
                    var filename = "-";
                }

                $('#staticBackdropLabel').text(data.kinerja[0].category.name + ' Tahun ' + data.kinerja[0].year);

                $.each(data.kinerja, function (key, value) {

                    if (value.verified_at == null) {
                        var opsiedit = '<i class="fas fa-pen" onclick="edit(' + value.id + ', ' + value.total + ')"></i>'
                    } else {
                        var opsiedit = '-'
                    }

                    if (value.satuan == null) {
                        var satuan = ' '
                    } else {
                        var satuan = value.satuan
                    }

                    $('#detail-kinerja').append(
                        '<tr>' +
                        '<td>' + value.subcategory.name + '</td>' +
                        '<td>' + value.total + ' ' + satuan + '</td>' +
                        '<td>' + value.description + '</td>' +
                        //'<td>' + opsiedit + '</td>' +
                        // '<td>' + value.total + '</td>' +
                        '</tr>'
                    );
                });

                if (data.kinerja[0].verified_at == null) {
                    $('#data-detail').append(
                        '<span>File Pendukung : <a href="' + file + '" download>' + filename + '</a></span>' +
                        '<br>' + '<span>Sumber Data : ' + data.kinerja[0].workunit.name + '</span>'
                    );

                    $('#form').append(
                        '<input type="hidden" name="category_id" value="' + data.kinerja[0].category_id + '">' +
                        '<input type="hidden" name="year" value="' + data.kinerja[0].year + '">' +
                        '<input type="hidden" name="pengolahdata_id" value="' + data.kinerja[0].pengolahdata_id + '">'
                    );
                } else {
                    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                    var tanggal = new Date(data.kinerja[0].verified_at);
                    var xhari = tanggal.getDay();
                    var xtanggal = tanggal.getDate();
                    var xbulan = tanggal.getMonth();
                    var xtahun = tanggal.getFullYear();
                    var jam = tanggal.getHours();
                    var menit = tanggal.getMinutes().toString().padStart(2, '0');
                    var detik = tanggal.getSeconds().toString().padStart(2, '0');
                    var tgl = hari[xhari] + ', ' + xtanggal + ' ' + bulan[xbulan] + ' ' + xtahun + ' ' + jam + ':' + menit + ':' + detik;
                    $('#data-detail').append(
                        '<table>' +
                        '<tr>' +
                        '<td width="28%">Sumber Data</td>' +
                        '<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>' +
                        '<td>' + data.kinerja[0].workunit.name + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td width="28%">Tanggal Verifikasi</td>' +
                        '<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>' +
                        '<td>' + tgl + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td width="28%">Diverifikasi Oleh</td>' +
                        '<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>' +
                        '<td>' + data.kinerja[0].pengolahdata.name + ' ( '+ data.kinerja[0].pengolahdata.workunit.name +' )</td>' +
                        '</tr>' +
                        '</table>'
                        // '<span>Sumber Data : ' + data.kinerja[0].workunit.name + '</span>' +
                        // '<br>' + '<span><b>Tanggal Verifikasi : ' + tgl + '</b></span>' +
                        // '<br>' + '<span>Diverifikasi Oleh : ' + data.kinerja[0].pengolahdata.workunit.name + '</span>'

                    );
                }
                $('#staticBackdrop').modal('show');
            }
        });
    });

    $('.delete').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Data Kinerja ini akan di hapus secara permanen!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });

    $('.send').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Pastika Data Yang Anda Inputkan Sudah Benar! Data Kinerja Yang Sudah Dikirim Tidak Dapat Di Edit Kembali!",
            icon: "warning",
            buttons: true,
            dangerMode: false,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });

    $("[data-toggle=popover]").popover();
    
    $('#btn-chat').on('click', function () {
        var categoryname = $(this).data('category-name');
        var year = $(this).data('year');
        var user_id = "{{ Auth::user()->id }}";
        var topic = 'Kinerja ' + categoryname + ' Tahun ' + year;
        var chatid = $(this).data('chatid');
        $.ajax({
            url: "{{ route('chat.get-chat') }}",
            method: "POST",
            data: {
                chatid: chatid,
                user_id: user_id,
                _token: "{{ csrf_token() }}"
            },
            success: function (data) {
                $.each(data.messages, function(key,value) {
                    var date = new Date(value.created_at);
                    var hours = date.getHours();
                    var minutes = "0" + date.getMinutes();
                    var seconds = "0" + date.getSeconds();

                    if(value.sender_id == {{ Auth::user()->id }}) {
                        var position = 'right';
                        var picsender = "{{ Auth::user()->getAvatar() }}";
                        var picreceiver = value.receiver.avatar;
                    } else {
                        var position = 'left';
                        var picsender = value.sender.avatar;
                        var picreceiver = value.receiver.avatar;
                    }

                    chats.push({
                        text: value.message,
                        position: position,
                        type: 'text',
                        day: value.created_at,
                        time: hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2),
                        picturesender: picsender,
                        picturereceiver: picreceiver,
                    });

                })
                for(var i = 0; i < chats.length; i++) {
                    var type = 'text';
                
                    if(chats[i].typing != undefined) type = 'typing';
                    $.chatCtrl('#mychatbox2', {
                        text: (chats[i].text != undefined ? chats[i].text : ''),
                        // picture: (chats[i].position == 'left' ? '../assets/img/avatar/avatar-5.png' : '../assets/img/avatar/avatar-2.png'),
                        picture: chats[i].position == 'left' ? chats[i].picturereceiver : chats[i].picturesender,
                        position: 'chat-'+chats[i].position,
                        time: (chats[i].time != undefined ? chats[i].time : ''),
                        type: type
                    });
                }
                console.log(chats);
            }
        });
        $('#topic-chat').html(topic);
        $('#chat-modal').modal('show');
    });

    $('#chat-form2').submit(function() {
        var me = $(this);
        var chatid = $('#btn-chat').data('chatid');
        var receiver_id = $('.btn-chat').data('pengolahdata-id');
        var sender_id = $('#btn-chat').data('produsendata-id');
        var message = $('#message').val();
        $.ajax({
            url: "{{ route('chat.store') }}",
            method: "POST",
            data: {
                chatid: chatid,
                receiver_id: receiver_id,
                sender_id: sender_id,
                message: message,
                _token: "{{ csrf_token() }}"
            },
            success: function (data) {
                if(me.find('input').val().trim().length > 0) {
                    $.chatCtrl('#mychatbox2', {
                        text: me.find('input').val(),
                        picture: data.picture,
                    });
                    me.find('input').val('');
                    
                }
            }
        });
        return false;
    })
</script>
@endsection