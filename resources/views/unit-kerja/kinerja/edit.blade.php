@extends('layouts.dashboard')

@section('title')
Edit Data Kinerja
@endsection

@section('css')

@endsection

@section('content')
<div class="section">
  <div class="section-header">
    <h1>Edit Data Kinerja</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
      <div class="breadcrumb-item">Edit Data Kinerja</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Edit Data Kinerja</h2>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <a href="{{ route('kinerja.index') }}" class="btn btn-primary">Kembali</a>
          </div>
          <div class="card-body">
            <form action="{{ route('kinerja.update', Crypt::encrypt($datakinerja->category_id)) }}" method="post"
              enctype="multipart/form-data">
              @csrf
              @method('PUT')
              <div class="form-group">
                <input type="hidden" name="workunit_id" value="{{ $datakinerja->workunit_id }}">
                <label>Pilih Kategori :</label>
                <input type="hidden" name="user_id" value="{{ $datakinerja->produsendata_id }}">
                <select class="selectpicker2 @error('category_id') is-invalid @enderror" data-show-subtext="true"
                  data-live-search="true" id="category" name="category_id" disabled>
                  @foreach ($category as $c)
                  @if (auth()->user()->roles->first()->name != 'super-admin' && $c->workunit_id ==
                  auth()->user()->workunit_id)
                  <option value="{{ Crypt::encrypt($c->id) }}" {{ $kinerja[0]->category_id == $c->id ? 'selected':''
                    }}>{{ $c->name }}</option>
                  @elseif (auth()->user()->roles->first()->name == 'super-admin' || auth()->user()->roles->first()->name
                  == 'admin')
                  <option value="{{ Crypt::encrypt($c->id) }}" {{ $kinerja[0]->category_id == $c->id ? 'selected':''
                    }}>{{ $c->name }}</option>
                  @endif
                  @endforeach
                </select>
                @error('category_id')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
              <div class="list-group" id="listdata">
                <div class="form-group">
                  <input type="number" class="form-control @error('year') is-invalid @enderror" name="year" id="year"
                    value="{{ $kinerja[0]->year }}" placeholder="Tahun" readonly>
                  @error('year')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                @php
                    $i = 1;
                @endphp
                @foreach ($kinerja as $k)
                <li class="list-group-item">
                  {{ $k->subcategory->name }}
                  <input type="hidden" id="idsub" name="subcategory_id[]" value="{{ $k->subcategory->id }}">
                  <input type="text" id="satuan" name="satuan[]" placeholder="Satuan Data" value="{{ $k->satuan }}"
                    class="form-control text-small float-right col-md-3 @error('satuan') is-invalid @enderror"> <input
                    type="number" step="any" value="{{ $k->total }}" style="width:110px;" id="subdata" name="total[]"
                    class="form-control text-small float-right col-md-3 @error('satuan') is-invalid @enderror">
                </li>
                <li class="list-group-item">
                  Keterangan :
                  <textarea name="description[]" id="ket-{{ $i++ }}" class="form-control mb-2 mt-3 @error('description') is-invalid @enderror keterangan"> {{ $k->description }} </textarea>
                </li>
                @endforeach
                <div class="form-group">
                  <input type="file" name="file" class="form-control mt-4 @error('file') is-invalid @enderror"
                    id="file">
                  <small class="form-text text-danger">Upload file bukti pendukung hanya diperbolehkan dengan format
                    (xls, xlsx)</small>
                  @error('file')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-success btn-block {{ $k->status == 5 ? 'kirim':'' }}" id="add">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

@section('js')
<script>
  $(document).ready(function() {
    $('.selectpicker2').selectpicker();
  });
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
  swal("{{ $error }}", {
              title: "Failed",
              icon: "error",
          });
</script>
@endforeach
@endif
<script>
  $('.kirim').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Pastika Data Yang Anda Inputkan Saat Ini Sudah Benar!, Jangan Sampai Ada Kesalahan Input Data Kembali",
            icon: "warning",
            buttons: true,
            dangerMode: false,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });
    $('.keterangan').each(function () {
      CKEDITOR.replace(this.id);
    });
</script>
@endsection