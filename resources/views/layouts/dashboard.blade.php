<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="description" content="SIDATA - Sistem Informasi Pengumpulan Data">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="shortcut icon" href="{{ asset('admin/stisla/assets/img/faviconsidata.png') }}" type="image/x-icon">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jquery-editable/css/jquery-editable.css" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#099cf4" />
    <link rel="apple-touch-icon" href="{{ asset('admin/stisla/assets/img/faviconsidata.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <title>@yield('title')</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- CSS Libraries -->
    {{--
    <link rel="stylesheet" href="{{ asset('admin/stisla/plugins/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/plugins/summernote/dist/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/plugins/owl.carousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/plugins/owl.carousel/dist/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('admin/stisla/plugins/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('admin/stisla/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/assets/css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/assets/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/stisla/assets/css/bootstrap-select.min.css') }}">

    <!-- Custom CSS -->
    @yield('css')

    <style>
        .float {
            position: fixed;
            width: 50px;
            height: 50px;
            bottom: 25px;
            right: 25px;
            display: flex;
            justify-content: center;
            align-items: center;
            background: linear-gradient(90deg, #4628BA 0%, #35269C 56.77%, #0E2258 100%);
            color: #FFF;
            border-radius: 50px;
            text-align: center;
            z-index: 1000;
        }

        .my-float {
            margin-right: 6px;
            width: 30px;
            height: 30px;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            font-size: 16px
        }

        .float:hover {
            color: #FFF;
            text-decoration: none;
        }
    </style>
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            @include('admin.shared.navbar')
            <div class="main-sidebar">
                @include('admin.shared.sidebar')
            </div>

            <!-- Main Content -->
            <div class="main-content">
                @yield('content')

            </div>
            @include('admin.shared.footer')
        </div>
    </div>

    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('admin/stisla/assets/js/stisla.js') }}"></script>

    <!-- JS Libraies -->
    <script src="{{ asset('admin/stisla/plugins/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>
    <script src="{{ asset('admin/stisla/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('admin/stisla/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('admin/stisla/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/stisla/plugins/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/stisla/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/stisla/assets/js/page/modules-datatables.js') }}"></script>
    <script src="{{ asset('admin/stisla/assets/js/page/bootstrap-modal.js') }}"></script>
    <script src="{{ asset('admin/stisla/assets/js/bootstrap-select.min.js') }}"></script>

    <!-- Template JS File -->
    <script src="{{ asset('admin/stisla/assets/js/scripts.js') }}"></script>
    <script src="{{ asset('admin/stisla/assets/js/custom.js') }}"></script>

    <!-- Page Specific JS File -->
    {{-- <script src="{{ asset('admin/stisla/assets/js/page/index.js') }}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>
    <script>
        $(document).ready(function () {
            $.ajax({
                url: "{{ route('get-notification') }}",
                type: "GET",
                dataType: "json",
                dataSrc: "",
                success: function (data) {
                    if (data.success > 0) {
                        $('#notification').append(
                            '<span class="badge badge-danger" id="badge-notif" style="position: absolute;top: -10px;right: -5px;width: 20px;height: 24px;display:flex;justify-content:center;align-items:center">' +
                            data.success + '</span>');
                    }
                }
            });
        });

        $('#read-notif').click(function () {
            $.ajax({
                url: "{{ route('read-notification') }}",
                type: "GET",
                success: function (data) {
                    $('#badge-notif').hide();
                }
            });
        });
    </script>

    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function (reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
    <script src="https://cdn.ckeditor.com/4.20.0/full-all/ckeditor.js"></script>

    <!-- Custom JS -->
    @yield('js')

</body>

</html>