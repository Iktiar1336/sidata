<!DOCTYPE html>
<html lang="en">

<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SIDATA - Sistem Informasi Pengumpulan Data">
    <meta name="keywords" content="SIDATA, Sistem Informasi Pengumpulan Data, Arsip Nasional Republik Indonesia, ANRI">
    <meta name="author" content="Arsip Nasional Republik Indonesia">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <!-- ========== Page Title ========== -->
    <title>@yield('title')</title>

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="{{ asset('assets/img/faviconsidata.png') }}" type="image/x-icon">
    <meta name="theme-color" content="#099cf4" />
    <link rel="apple-touch-icon" href="{{ asset('admin/stisla/assets/img/faviconsidata.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <!-- ========== Start Stylesheet ========== -->

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/flaticon-set.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/magnific-popup.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/bootsnav.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <!-- ========== End Stylesheet ========== -->

    <!-- ========== Google Fonts ========== -->
    {{-- <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800&amp;display=swap"
        rel="stylesheet"> --}}

    @yield('css')

    <style>
        .float {
            position: fixed;
            width: 50px;
            height: 50px;
            bottom: 25px;
            right: 25px;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #099cf4;
            color: #FFF;
            border-radius: 50px;
            text-align: center;
            z-index: 1000;
        }

        .my-float {
            margin-right: 2px;
            width: 30px;
            height: 30px;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            font-size: 16px
        }

        .float:hover {
            color: #FFF;
            text-decoration: none;
        }

        .bullet {
            display: inline-block;
            vertical-align: middle;
            width: 5px;
            height: 5px;
            margin: 0 5px 5px 5px;
            background-color: #000;
            justify-content: center;
            border-radius: 50%;
        }

        @media (max-width: 767px) {
            #nav-login {
                visibility: visible !important;
            }

            .img-anri {
                float: right;
            }
        }
    </style>

</head>

<body>

    @yield('header')

    <!-- Start Main Content -->
    @yield('content')
    <!-- End Main Content -->

    <a href="https://t.me/sidataanri_bot" target="_BLANK" class="float" data-toggle="tooltip" data-placement="top"
        title="Bot Telegram">
        <i class="fas fa-paper-plane my-float"></i>
    </a>

    <!-- Star Footer
    ============================================= -->
    <footer>
        <div class="container">
            <div class="f-items default-padding">
                <div class="row">
                    <div class="equal-height col-lg-4 col-md-6 item">
                        <div class="f-item about">
                            <img src="{{ asset('assets/img/logo-sidata.png') }}" class="mr-5" alt="Logo" height="70px">
                            <img src="{{ asset('anri.png') }}" alt="Logo" style="width: 200px;">
                            <p>
                                Seluruh isi dalam website ini dilindungi oleh Undang-undang Republik Indonesia.
                            </p>
                        </div>
                    </div>

                    <div class="equal-height col-lg-2 col-md-6 item">
                        <div class="f-item link">
                            <h4 class="widget-title">Overview</h4>
                            <ul>
                                <li>
                                    <a href="#">Inforgrafis</a>
                                </li>
                                <li>
                                    <a href="#">Unit Kerja</a>
                                </li>
                                <li>
                                    <a href="#">Arsip Publik</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="equal-height col-lg-2 col-md-6 item">
                        <div class="f-item link">
                            <h4 class="widget-title">Privacy</h4>
                            <ul>
                                <li>
                                    <a href="#">Kebijakan Privasi</a>
                                </li>
                                <li>
                                    <a href="#">Ketentuan Pengguna</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="equal-height col-lg-4 col-md-6 item">
                        <div class="f-item contact">
                            <h4 class="widget-title">Contact Info</h4>
                            @php
                                $appsetting = \App\Models\AppSetting::first();
                            @endphp
                            <p>
                                {{ $appsetting->nama_instansi }}
                            </p>
                            <div class="address">
                                <ul>
                                    <li>
                                        <i class="fas fa-map-marker-alt"></i>
                                        <span>&nbsp; {{ $appsetting->alamat }}</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-phone"></i>
                                        <span>&nbsp; {{ $appsetting->no_telp }}</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-envelope"></i>
                                        <span>
                                            <a href="mailto:{{ $appsetting->email }}" style="font-weight: 500;">&nbsp;
                                                {{ $appsetting->email }}</a>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <ul class="social">
                                <li class="facebook">
                                    <a href="{{ $appsetting->facebook }}"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="twitter">
                                    <a href="{{ $appsetting->twitter }}"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="youtube">
                                    <a href="{{ $appsetting->youtube }}"><i class="fab fa-youtube"></i></a>
                                </li>
                                <li class="instagram">
                                    <a href="{{ $appsetting->instagram }}"><i class="fab fa-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p>Copyright &copy; 2022 <i class="bullet"></i> All Rights Reserved Sistem Informasi Pengumpulan Data | {{ $appsetting->nama_instansi }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shape -->
        <div class="footer-shape" style="background-image: url(assets/img/shape/1.svg);"></div>
        <!-- End Shape -->
    </footer>
    <!-- End Footer-->

    <!-- jQuery Frameworks
    ============================================= -->
    <script src="{{ asset('assets/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/equal-height.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/modernizr.custom.13711.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/count-to.js') }}"></script>
    <script src="{{ asset('assets/js/bootsnav.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
        integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
    </script> --}}

    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        window.onload = () => {
        'use strict';
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/sw.js');
        }
    }
    </script>
    @yield('js')

</body>

</html>