<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIDATA - @yield('title')</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('asset-login/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('asset-login/css/sb-admin-2.min.css') }}" rel="stylesheet">

    @yield('css')

    <style>
        .btn-vivid-cerulean {
            color: #fff;
            background-color: #099CF4;
            font-weight: bold;
            font-size: 14px !important;
            border: 2px solid #099CF4;
            letter-spacing: 1px;
        }

        .btn-vivid-cerulean:hover {
            color: #099CF4;
            background-color: transparent;
            transition: 0.3s;
            border: 2px solid #099CF4;
        }
    </style>

</head>

<body>

    <div class="container">

        @yield('content')

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('asset-login/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('asset-login/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('asset-login/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('asset-login/js/sb-admin-2.min.js') }}"></script>

    <script>
        $(".toggle-password").click(function () {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>

<script>
    window.onload = () => {
        'use strict';
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/sw.js');
        }
    }
</script>

</body>

</html>