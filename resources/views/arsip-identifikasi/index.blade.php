@extends('layouts.dashboard')

@section('title', 'Taxonomy Arsip')

@section('css')
<style>
    .tree,
    .tree ul {
        margin: 0;
        padding: 0;
        list-style: none
    }

    .tree ul {
        margin-left: 1em;
        position: relative
    }

    .tree ul ul {
        margin-left: .7em
    }

    .tree ul:before {
        content: "";
        display: block;
        width: 0;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        border-left: 1px solid
    }

    .tree li {
        margin: 0;
        padding: 0 1em;
        line-height: 4em;
        font-weight: 700;
        position: relative;
        color: #6777ef;
    }

    .tree ul li:before {
        content: "";
        display: block;
        width: 10px;
        height: 0;
        border-top: 1px solid;
        margin-top: -1px;
        position: absolute;
        top: 1.9em;
        left: 0
    }

    .tree ul li:last-child:before {
        background: #fff;
        height: auto;
        top: 1.9em;
        bottom: 0
    }

    .indicator {
        margin-right: 5px;
        font-size: 17px
    }

    .tree li a {
        text-decoration: none;
        color: #369;
    }

    .tree li button,
    .tree li button:active,
    .tree li button:focus {
        text-decoration: none;
        color: #369;
        border: none;
        background: transparent;
        margin: 0px 0px 0px 0px;
        padding: 0px 0px 0px 0px;
        outline: 0;
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Taxonomy Arsip</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data Kinerja</div>
            <div class="breadcrumb-item">Taxonomy Arsip</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Taxonomy Arsip</h2>
        <p class="section-lead">Halaman untuk mengelola data Taxonomy Arsip.</p>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Aturan Identifikasi</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @if (auth()->user()->roles()->first()->name == 'admin' || auth()->user()->roles()->first()->name == 'super-admin')
                                <div class="{{ auth()->user()->roles()->first()->name == 'admin' || auth()->user()->roles()->first()->name == 'super-admin' ? 'col-md-4' : '' }}">
                                    <div class="form-group">
                                        <label for="instansi_id">Pilih Instansi</label>
                                        <select class="selectpicker" name="instansi_id" id="instansi_id" data-show-subtext="true" data-live-search="true">
                                            <option selected disabled>Pilih Instansi</option>
                                            <option value="ALL">Pilih Semua</option>
                                            @foreach ($instansi as $item)
                                                <option value="{{ $item->id }}">{{ $item->nama_instansi }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="{{ auth()->user()->roles()->first()->name == 'admin' || auth()->user()->roles()->first()->name == 'super-admin' ? 'col-md-4' : 'col-md-6' }}">
                                <div class="form-group">
                                    <label for="filter1">Filter 1</label>
                                    <select class="form-control" name="filter_1" id="filter1" name="filter1">
                                        <option value="">Pilih</option>
                                        <option value="jenis_arsip">Jenis Arsip</option>
                                        <option value="media_arsip">Media Arsip</option>
                                        <option value="keterangan">Keterangan</option>
                                        <option value="kode_klasifikasi">Kode Klasifikasi</option>
                                        <option value="tingkat_perkembangan">Tingkat Perkembangan</option>
                                        <option value="kondisi">Kondisi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="{{ auth()->user()->roles()->first()->name == 'admin' || auth()->user()->roles()->first()->name == 'super-admin' ? 'col-md-4' : 'col-md-6' }}">
                                <div class="form-group">
                                    <label for="filter2">Filter 2</label>
                                    <select class="form-control" name="filter_2" id="filter2" disabled>
                                        <option value="">Pilih</option>
                                        <option value="jenis_arsip">Jenis Arsip</option>
                                        <option value="media_arsip">Media Arsip</option>
                                        <option value="keterangan">Keterangan</option>
                                        <option value="kode_klasifikasi">Kode Klasifikasi</option>
                                        <option value="tingkat_perkembangan">Tingkat Perkembangan</option>
                                        <option value="kondisi">Kondisi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="filter3">Filter 3</label>
                                    <select class="form-control" name="filter_3" id="filter3" disabled>
                                        <option value="">Pilih</option>
                                        <option value="jenis_arsip">Jenis Arsip</option>
                                        <option value="media_arsip">Media Arsip</option>
                                        <option value="keterangan">Keterangan</option>
                                        <option value="kode_klasifikasi">Kode Klasifikasi</option>
                                        <option value="tingkat_perkembangan">Tingkat Perkembangan</option>
                                        <option value="kondisi">Kondisi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="filter4">Filter 4</label>
                                    <select class="form-control" name="filter_4" id="filter4" disabled>
                                        <option value="">Pilih</option>
                                        <option value="jenis_arsip">Jenis Arsip</option>
                                        <option value="media_arsip">Media Arsip</option>
                                        <option value="keterangan">Keterangan</option>
                                        <option value="kode_klasifikasi">Kode Klasifikasi</option>
                                        <option value="tingkat_perkembangan">Tingkat Perkembangan</option>
                                        <option value="kondisi">Kondisi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="filter5">Filter 5</label>
                                    <select class="form-control" name="filter_5" id="filter5" disabled>
                                        <option value="">Pilih</option>
                                        <option value="jenis_arsip">Jenis Arsip</option>
                                        <option value="media_arsip">Media Arsip</option>
                                        <option value="keterangan">Keterangan</option>
                                        <option value="kode_klasifikasi">Kode Klasifikasi</option>
                                        <option value="tingkat_perkembangan">Tingkat Perkembangan</option>
                                        <option value="kondisi">Kondisi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="filter6">Filter 6</label>
                                    <select class="form-control" name="filter_6" id="filter6" disabled>
                                        <option value="">Pilih</option>
                                        <option value="jenis_arsip">Jenis Arsip</option>
                                        <option value="media_arsip">Media Arsip</option>
                                        <option value="keterangan">Keterangan</option>
                                        <option value="kode_klasifikasi">Kode Klasifikasi</option>
                                        <option value="tingkat_perkembangan">Tingkat Perkembangan</option>
                                        <option value="kondisi">Kondisi</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Taxonomy Arsip</h4>
                    </div>
                    <div class="card-body">
                        <div id="treview" width="100%">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).ready(function () {

        $('#instansi_id').change(function () {
            $('#filter1').val('');
            $('#filter2').val('');
            $('#filter3').val('');
            $('#filter4').val('');
            $('#filter5').val('');
            $('#filter6').val('');

            $('#filter2').prop('disabled', true);
            $('#filter3').prop('disabled', true);
            $('#filter4').prop('disabled', true);
            $('#filter5').prop('disabled', true);
            $('#filter6').prop('disabled', true);

            $('#treview').empty();

            $('#filter1').prop('disabled', false);
        });

        $('#filter1').change(function () {
            $('#filter2').val('');
            $('#filter3').val('');
            $('#filter4').val('');
            $('#filter5').val('');
            $('#filter6').val('');

            $('#filter3').prop('disabled', true);
            $('#filter4').prop('disabled', true);
            $('#filter5').prop('disabled', true);
            $('#filter6').prop('disabled', true);

            $('#treview').empty();

            $('#filter2').prop('disabled', false);

            $.each($('#filter2 option'), function (i, v) {
                $(v).prop('disabled', false);
                if ($(v).val() == $('#filter1').val()) {
                    $(v).prop('disabled', true);
                }
            });

        });

        $('#filter2').change(function () {
            $('#filter3').val('');
            $('#filter4').val('');
            $('#filter5').val('');
            $('#filter6').val('');

            $('#filter4').prop('disabled', true);
            $('#filter5').prop('disabled', true);
            $('#filter6').prop('disabled', true);

            $('#treview').empty();

            $('#filter3').prop('disabled', false);

            $.each($('#filter3 option'), function (i, v) {
                $(v).prop('disabled', false);
                if ($(v).val() == $('#filter1').val() || $(v).val() == $('#filter2').val()) {
                    $(v).prop('disabled', true);
                }
            });

        });

        $('#filter3').change(function () {
            $('#filter4').val('');
            $('#filter5').val('');
            $('#filter6').val('');

            $('#filter5').prop('disabled', true);
            $('#filter6').prop('disabled', true);

            $('#treview').empty();

            $('#filter4').prop('disabled', false);

            $.each($('#filter4 option'), function (i, v) {
                $(v).prop('disabled', false);
                if ($(v).val() == $('#filter1').val() || $(v).val() == $('#filter2').val() || $(v).val() == $('#filter3').val()) {
                    $(v).prop('disabled', true);
                }
            });

        });

        $('#filter4').change(function () {
            $('#filter5').val('');
            $('#filter6').val('');

            $('#filter6').prop('disabled', true);

            $('#filter5').prop('disabled', false);

            $('#treview').empty();

            $.each($('#filter5 option'), function (i, v) {
                $(v).prop('disabled', false);
                if ($(v).val() == $('#filter1').val() || $(v).val() == $('#filter2').val() || $(v).val() == $('#filter3').val() || $(v).val() == $('#filter4').val()) {
                    $(v).prop('disabled', true);
                }
            });

        });

        $('#filter5').change(function () {
            $('#filter6').val('');

            $('#filter6').prop('disabled', false);

            $('#treview').empty();

            $.each($('#filter6 option'), function (i, v) {
                $(v).prop('disabled', false);
                if ($(v).val() == $('#filter1').val() || $(v).val() == $('#filter2').val() || $(v).val() == $('#filter3').val() || $(v).val() == $('#filter4').val() || $(v).val() == $('#filter5').val()) {
                    $(v).prop('disabled', true);
                }
            });

        });

        $('#filter6').change(function () {
            $.each($('#filter6 option'), function (i, v) {
                $(v).prop('disabled', false);
                if ($(v).val() == $('#filter1').val() || $(v).val() == $('#filter2').val() || $(v).val() == $('#filter3').val() || $(v).val() == $('#filter4').val() || $(v).val() == $('#filter5').val()) {
                    $(v).prop('disabled', true);
                }
            });

            $.ajax({
                url: "/arsip-identifikasi/taxonomy",
                type: "GET",
                data: {
                    instansi_id : $('#instansi_id').val(),
                    filter_1: $('#filter1').val(),
                    filter_2: $('#filter2').val(),
                    filter_3: $('#filter3').val(),
                    filter_4: $('#filter4').val(),
                    filter_5: $('#filter5').val(),
                    filter_6: $('#filter6').val(),
                },
                success: function (data) {
                    $('#treview').empty();
                    console.log(data.subinstansi);
                    if(data.data.length > 0){
                        $.each(data.data, function (i, v) {
                            $('#treview').append(`
                                    <tr>
                                        <td></td>
                                        <td>
                                            <ul class="list-group" id="tree1">
                                                <li class="list-group-item">
                                                    ${v.key}
                                                    ${subtree(v.data)}
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                            `);

                            $.fn.extend({
                                treed: function (o) {

                                    var openedClass = 'fa-folder-open';
                                    var closedClass = 'fa-folder';

                                    if (typeof o != 'undefined') {
                                        if (typeof o.openedClass != 'undefined') {
                                            openedClass = o.openedClass;
                                        }
                                        if (typeof o.closedClass != 'undefined') {
                                            closedClass = o.closedClass;
                                        }
                                    };

                                    //initialize each of the top levels
                                    var tree = $(this);
                                    tree.addClass("tree");
                                    tree.find('li').has("ul").each(function () {
                                        var branch = $(this); //li with children ul
                                        branch.prepend("<i class='indicator far " + closedClass + "'></i>");
                                        branch.addClass('branch');
                                        branch.on('click', function (e) {
                                            if (this == e.target) {
                                                var icon = $(this).children('i:first');
                                                icon.toggleClass(openedClass + " " + closedClass);
                                                $(this).children().children().toggle();
                                            }
                                        })
                                        branch.children().children().toggle();
                                    });
                                    //fire event from the dynamically added icon
                                    tree.find('.branch .indicator').each(function () {
                                        $(this).on('click', function () {
                                            $(this).closest('li').click();
                                        });
                                    });
                                    //fire event to open branch if the li contains an anchor instead of text
                                    tree.find('.branch>a').each(function () {
                                        $(this).on('click', function (e) {
                                            $(this).closest('li').click();
                                            e.preventDefault();
                                        });
                                    });
                                    //fire event to open branch if the li contains a button instead of text
                                    tree.find('.branch>button').each(function () {
                                        $(this).on('click', function (e) {
                                            $(this).closest('li').click();
                                            e.preventDefault();
                                        });
                                    });
                                }
                            });
                            $('#tree1').treed({
                                openedClass: 'fa-folder',
                                closedClass: 'fa-folder-open'
                            });

                            $('.btn-detail-arsip').on('click', function () {
                                var id = $(this).data('id');
                                var instansi = $(this).data('instansi');
                                var jenis_arsip = $(this).data('jenis-arsip');
                                var jumlah = $(this).data('jumlah');
                                var keterangan = $(this).data('keterangan');
                                var kondisi = $(this).data('kondisi');
                                var kode_klasifikasi = $(this).data('kode-klasifikasi');
                                var media_arsip = $(this).data('media');
                                var tanggal_surat = $(this).data('tanggal-surat');
                                var nomor_surat = $(this).data('nomor-surat');
                                var type = $(this).data('type'); 
                                var tingkat_perkembangan = $(this).data('tingkat-perkembangan');
                                var uraian_item = $(this).data('uraian-item');
                                var berkas = $(this).data('berkas');
                                var modal = $('#modal-detail');
                                var xhari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                                var xbulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                var date = new Date(tanggal_surat);
                                var hari = date.getDay();
                                var tanggal = date.getDate();
                                var bulan = date.getMonth();
                                var tahun = date.getFullYear();
                                var jam = date.getHours();
                                var menit = date.getMinutes().toString().padStart(2, '0');
                                var detik = date.getSeconds().toString().padStart(2, '0');
                                var tanggal = xhari[hari] + ', ' + tanggal + ' ' + xbulan[bulan] + ' ' + tahun + ' ';
                                $('.modal-body').empty();
                                $('.modal-title').text('Detail Arsip' + ' ' + berkas);
                                $('.modal-body').append(
                                    `
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Instansi</th>
                                            <td>${instansi}</td>
                                        </tr>
                                        <tr>
                                            <th>Jenis Arsip</th>
                                            <td>${jenis_arsip}</td>
                                        </tr>
                                        <tr>
                                            <th>Uraian Item</th>
                                            <td>${uraian_item}</td>
                                        </tr>
                                        <tr>
                                            <th>Nomor Surat</th>
                                            <td>
                                                ${nomor_surat}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Surat</th>
                                            <td>${
                                                tanggal
                                            }</td>
                                        </tr>
                                        <tr>
                                            <th>Kode Klasifikasi</th>
                                            <td>${kode_klasifikasi}</td>
                                        </tr>
                                        <tr>
                                            <th>Tingkat Perkembangan</th>
                                            <td>${tingkat_perkembangan}</td>
                                        </tr>
                                        <tr>
                                            <th>Media Arsip</th>
                                            <td>${media_arsip}</td>
                                        </tr>
                                        <tr>
                                            <th>Kondisi</th>
                                            <td>${kondisi}</td>
                                        </tr>
                                        <tr>
                                            <th>Keterangan</th>
                                            <td>${keterangan}</td>
                                        </tr>
                                        <tr>
                                            <th>Berkas</th>
                                            <td>
                                                <a href="/arsip-kolektor/list-arsip-digital/download/${id}" target="_blank" class="btn btn-primary btn-sm">Lihat Berkas</a>
                                            </td>
                                        </tr>
                                                
                                    </table>
                                    `
                                );

                                modal.modal('show');
                            });
                        });
                    } else {
                        alert('Data tidak ditemukan');
                    }
                }
            });

            function subtree(data) {
                var html = '<ul>';
                var no = 1; 
                $.each(data, function (i, v) {
                    html += '<li>' 
                        console.log(v);
                    if(v.data){
                        html += v.key;
                        html += subtree(v.data);
                    } else {
                        html += `
                            <button type="button" class="btn btn-link btn-detail-arsip" data-id="${v.id}" data-instansi="${v.instansi}" data-jenis-arsip="${v.jenis_arsip}" data-jumlah="${v.jumlah_berkas}" data-kondisi="${v.kondisi}" data-kode-klasifikasi="${v.kode_klasifikasi}" data-media="${v.media_arsip}" data-nomor-surat="${v.nomor_surat}" data-tanggal-surat="${v.tanggal_surat}" data-type="${v.type}" data-tingkat-perkembangan="${v.tingkat_perkembangan}" data-uraian-item="${v.uraian_item}" data-berkas="${v.nama_file}" data-keterangan="${v.keterangan}">
                                ${v.nama_file}
                            </button>
                        `;
                    }
                    html += '</li>';
                });

                html += '</ul>';
                return html;
            }
        });

        function format(d) {
            // `d` is the original data object for the row
            $table = `
                    <table class="table table-bordered table-striped" id="grup1">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Grup</th>
                            </tr>
                        </thead>
                        <tbody>
                `;
            // $.each(d.data, function(i, v){
            //     $table += `
            //         <tr>
            //             <td class="details-control"><i class="fa fa-plus-square"></i></td>
            //             <td>`+v.key+`</td>
            //         </tr>
            //     `;
            // });

            $table += `
                        </tbody>
                    </table>
                `;
            return $table;
        }

        $('.btn-detail').on('click', function () {
            var id = $(this).data('details');
            alert(id);
        });

        $('#table-arsip tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = $('#table-arsip').DataTable().row(tr);
            console.log(row.data());


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });


    });
</script>
@endsection