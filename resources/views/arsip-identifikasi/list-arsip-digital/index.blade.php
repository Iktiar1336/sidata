@extends('layouts.dashboard')

@section('title', 'List Arsip')

@section('css')
    
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>List Arsip Digital</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">List Arsip Digital</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Arsip Digital</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered" id="tablearsip">
                                <thead>
                                    <tr>
                                        <th>Instansi</th>
                                        <th>File</th>
                                        <th>Type</th>
                                        <th>Size</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Instansi</th>
                                        <th>File</th>
                                        <th>Type</th>
                                        <th>Size</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')

<script>

    $(document).ready(function () {
        $('.selectpicker').selectpicker();

        $('#tablearsip').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('list-arsip-digital.index') }}", 
            dataType: "json",
            autoWidth: true,
            columns: [
                {data: 'instansi', name: 'instansi'},
                {data: 'file', name: 'file'},
                {data: 'type', name: 'type'},
                {data: 'size', name: 'size'},
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var select = $('<select class="selectpicker" data-show-subtext="true" data-live-search="true"><option value="">Pilih</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                        $('.selectpicker').selectpicker('refresh');
                    });
                });
            }
        });
    });
</script>
@endsection