@extends('layouts.dashboard')

@section('title')
Log Aktivitas
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Log Aktivitas</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">User</a></div>
            <div class="breadcrumb-item">Log Aktivitas</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Log Aktivitas</h2>
        <div class="row">
            <div class="col-md-3 mt-2">
                <div class="card">
                    <div class="card-header">
                        <h4>Filter</h4>
                    </div>
                    <div class="card-body">
                        <div class="list-group" id="list-tab" role="tablist">
                            @foreach ($roles as $item)
                                <a class="list-group-item list-group-item-action" id="list-{{ $item->name }}-list" data-toggle="list" href="#list-{{ $item->name }}" role="tab" aria-controls="{{ $item->name }}">{{ $item->description }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content" id="nav-tabContent">
                    @foreach ($roles as $role)
                    <div class="tab-pane fade" id="list-{{ $role->name }}" role="tabpanel" aria-labelledby="list-{{ $role->name }}-list">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ $role->description }}</h4>
                                <div class="card-header-action">
                                    <a href="{{ route('activities.export', $role->id) }}" class="btn btn-primary">
                                        <i class="fas fa-file-excel"></i> Export Excel</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="activity-{{ $role->name }}" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Subjek
                                                </th>
                                                <th>Deksripsi</th>
                                                <th>Waktu</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($users as $user)
                                                @if ($user->hasRole($role->name))
                                                    @foreach ($user->activity as $activity)
                                                        <tr>
                                                            <td>
                                                                <a href="#">{{ $activity->log_name }}</a>
                                                            </td>
                                                            <td>
                                                                {{ $activity->description }}
                                                            </td>
                                                            <td>
                                                                {{ \Carbon\Carbon::parse($activity->created_at)->isoFormat('dddd, D MMMM Y, H:ss') }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    
</script>
@endsection