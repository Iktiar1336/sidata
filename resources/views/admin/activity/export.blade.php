<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Export Excel</title>
</head>
<body>
  <table>
    <thead>
      <tr>
        <th>Subjek</th>
        <th>Deksripsi</th>
        <th>Waktu</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $user)
        @foreach ($user->activity as $activity)
          <tr>
            <td>{{ $activity->log_name }}</td>
            <td>{{ $activity->description }}</td>
            <td>{{ \Carbon\Carbon::parse($activity->created_at)->isoFormat('dddd, D MMMM Y, H:ss') }}</td>
          </tr>
        @endforeach
      @endforeach
    </tbody>
  </table>
</body>
</html>