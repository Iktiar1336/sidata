@extends('layouts.dashboard')

@section('title')
    Update Visi & Misi
@endsection

@section('content')

<section class="section">
    <div class="section-header">
        <h1>Edit Visi & Misi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
            <div class="breadcrumb-item">Visi & Misi</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Visi & Misi</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('visi-misi.index') }}" class="btn btn-primary">Back</a>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-has-icon">
                                <div class="alert-icon"><i class="fas fa-info-circle"></i></div>
                                <div class="alert-body">
                                    <div class="alert-title"><strong>Whoops!</strong> There were some problems with your input.</div>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        {!! Form::open(array('route' => ['visi-misi.update', $visi_misi->id], 'method' => 'PUT')) !!}
                        <div class="form-group">
                            <label>Visi :</label>
                            <textarea name="visi" class="form-control @error('visi') is-invalid @enderror" required id="visi">{!! $visi_misi->visi !!}</textarea>
                            @error('visi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Misi :</label>
                            <textarea name="misi" class="form-control @error('misi') is-invalid @enderror" required id="misi">{{ $visi_misi->misi }}</textarea>
                            @error('misi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Tujuan :</label>
                            <textarea name="tujuan" class="form-control @error('tujuan') is-invalid @enderror" required id="tujuan">{{ $visi_misi->tujuan }}</textarea>
                            @error('tujuan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Fungsi :</label>
                            <textarea name="fungsi" class="form-control @error('fungsi') is-invalid @enderror" required id="fungsi">{{ $visi_misi->fungsi }}</textarea>
                            @error('fungsi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Tugas :</label>
                            <textarea name="tugas" class="form-control @error('tugas') is-invalid @enderror" required id="tugas">{{ $visi_misi->tugas }}</textarea>
                            @error('tugas')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection

@section('js')
<script>
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
    };
    CKEDITOR.replace('visi', options);
    CKEDITOR.replace('misi', options);
    CKEDITOR.replace('tujuan', options);
    CKEDITOR.replace('fungsi', options);
    CKEDITOR.replace('tugas', options);
</script>
@endsection