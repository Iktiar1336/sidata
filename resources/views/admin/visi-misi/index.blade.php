@extends('layouts.dashboard')

@section('title')
  Visi & Misi
@endsection

@section('css')
    
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Misi & Misi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
            <div class="breadcrumb-item">Visi & Misi</div>
        </div>
    </div>

    <div class="section-body">
        @foreach ($visi_misi as $item)
            <div class="card">
                <div class="card-header">
                    <h4>Misi & Misi</h4>
                </div>
                <div class="card-body">
                    {!! $item->visi !!}
                </div>
                <div class="card-footer bg-whitesmoke">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('visi-misi.edit', $item->id) }}" class="btn btn-primary">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>
@endsection

@section('js')
    
@endsection