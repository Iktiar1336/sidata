@extends('layouts.dashboard')

@section('title')
    Hak Akses
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Hak Akses</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">User</a></div>
            <div class="breadcrumb-item">Hak Akses</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Hak Akses</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable table-striped table-hover table-bordered" id="table-permission" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Modul</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<form action="{{ route('permissions.store') }}" method="POST" class="modal-part" id="modal-permission-part">
    @csrf
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" placeholder="Name Permission" name="name" required>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block">Submit</button>
    </div>
</form>
@endsection

@section('js')

<script>
    $(document).ready(function () {
        var url = "/permissions";
        $('#table-permission').DataTable({
            processing: true,
            serverSide: true,
            ajax: url,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'module', name: 'module'},
            ]
        });
    });
</script>

@if (session('insert-permission-success'))
<script>
    swal("Permission berhasil di tambah", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('update-permission-success'))
<script>
    swal("Permission berhasil di update", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-permission-success'))
<script>
    swal("Permission berhasil di hapus", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-permission-failed'))
<script>
    swal("Permission ini tidak bisa dihapus dikarenakan sudah memiliki role", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

<script>
    $('.delete').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Permission ini akan dihapus secara permanen!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });
  </script>

@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endsection