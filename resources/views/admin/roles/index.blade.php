@extends('layouts.dashboard')

@section('css')

@endsection

@section('title')
Jenis User
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Jenis User</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">User</a></div>
            <div class="breadcrumb-item">Jenis User</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Jenis User</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('roles.create') }}" class="btn btn-primary">Tambah Jenis User</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable table-striped" id="tablerole" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Deskripsi</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')

@if (session('insert-role-success'))
    <script>
        swal("Role berhasil di tambahkan", {
            title: "Success",
            icon: "success",
        });
    </script>
@endif

@if (session('update-role-success'))
    <script>
        swal("Role berhasil di update", {
            title: "Success",
            icon: "success",
        });
    </script>
@endif

@if (session('delete-role-success'))
    <script>
        swal("Role berhasil di hapus", {
            title: "Success",
            icon: "success",
        });
    </script>
@endif

@if (session('delete-role-failed'))
    <script>
        swal("Role ini tidak bisa dihapus dikarenakan sudah memiliki user", {
            title: "Failed",
            icon: "error",
        });
    </script>
@endif

@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <script>
            swal("{{ $error }}", {
                title: "Failed",
                icon: "error",
            });
        </script>
    @endforeach
@endif

<script>
    $(document).ready(function () {
        $('#tablerole').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/roles", 
            dataType: 'json',
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,

                },
            ]
        });
    });

    $('.delete').on('click', function (event) {
        event.preventDefault();
        const form = $(this).closest('form');
        swal({
            title: "Apakah anda yakin?",
            text: "Role ini akan di hapus secara permanen!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });
</script>

@endsection