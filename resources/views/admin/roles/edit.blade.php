@extends('layouts.dashboard')

@section('title')
Edit Role
@endsection

@section('content')

<section class="section">
    <div class="section-header">
        <h1>Edit Role</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">User</a></div>
            <div class="breadcrumb-item">Edit Role</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Role</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('roles.index') }}" class="btn btn-primary">Kembali</a>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger alert-has-icon">
                            <div class="alert-icon"><i class="fas fa-info-circle"></i></div>
                            <div class="alert-body">
                                <div class="alert-title"><strong>Whoops!</strong> Ada yang salah dengan inputan anda.
                                </div>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif

                        {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update',
                        Crypt::encrypt($role->id)]]) !!}
                        <div class="form-group">
                            <label>Name :</label>
                            @if ($role->name == 'admin')
                            {!! Form::text('name', null, array('placeholder' => 'Nama Role','class' => 'form-control',
                            'required', 'readonly')) !!}
                            @else
                            {!! Form::text('name', null, array('placeholder' => 'Nama Role','class' => 'form-control',
                            'required', 'readonly')) !!}
                            @endif
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Deskripsi</label>
                            {!! Form::textarea('description', null, array('placeholder' => 'Deskripsi Role','class' =>
                            'form-control', 'required')) !!}
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12">Permissions :</label>
                            <div class="col-sm-12">
                                <div class="form-check">
                                    <input type="checkbox" name="checkall" id="checkall"
                                        onclick="check_uncheck_checkbox(this.checked);">
                                    <label for="checkall">Pilih Semua</label>
                                </div>
                            </div>
                            @foreach ($modules as $module)
                            <div class="col-md-6 mb-4">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <span class="font-weight-bold">{{ $module['module'] }}</span>
                                    </li>
                                    <div class="list-group-item">
                                        <div class="form-check">
                                            <input type="checkbox" name="checkall" id="checkall-{{ \Str::slug($module['module']) }}" onclick="check_uncheck(this.checked, '{{ \Str::slug($module['module']) }}');" data-name="{{ \Str::slug($module['module']) }}">
                                            <label for="checkall-{{ \Str::slug($module['module']) }}">Pilih Semua</label>
                                        </div>
                                    </div>
                                    @foreach (\Spatie\Permission\Models\Permission::where('module',
                                    $module['module'])->get() as $perm)
                                    <li class="list-group-item">
                                        <div class="form-check">
                                            <label>{{ Form::checkbox('permissions[]', $perm->name, in_array($perm->id,
                                                $rolePermissions) ? true : false, array('class' => ['permission', \Str::slug($module['module'])])) }}
                                                {{ $perm->name }}</label>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block">Simpan</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    function check_uncheck_checkbox(isChecked) {
        if (isChecked) {
            $('.permission').prop('checked', true);
        } else {
            $('.permission').prop('checked', false);
        }
    }

    function check_uncheck(isChecked, name) {
        if (isChecked) {
            $('.' + name).prop('checked', true);
        } else {
            $('.' + name).prop('checked', false);
        }
    }
</script>
@endsection