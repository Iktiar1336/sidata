<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="/">
            <img src="{{ asset('admin/stisla/assets/img/logo-sidata-light.png') }}" alt="logo" class="logo-default" style="width: 140px;">
        </a>
    </div>

    <div class="sidebar-brand sidebar-brand-sm">
        <a href="/">
            <img src="{{ asset('admin/stisla/assets/img/logo-sidata-mini.png') }}" alt="logo" class="logo-default" style="width: 40px;">
        </a>
    </div>

    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>

        @can('Dashboard')
            @if (auth()->user()->roles->first()->name != 'super-admin')
                <li class="{{ Request::is('home') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="fas fa-fire"></i>
                        <span>Dashboard </span>
                    </a>
                </li>
            @endif
        @endcan

        @can('Dashboard Admin')
            <li class="nav-item {{ Route::is('dashboard.admin') ? 'active' : '' }}">
                <a href="{{ route('dashboard.admin') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
        @endcan

        @can('Dashboard Eksekutif')
            <li class="nav-item {{ Route::is('dashboard.eksekutif') ? 'active' : '' }}">
                <a href="{{ route('dashboard.eksekutif') }}" class="nav-link"><i class="fas fa-chart-line"></i><span>Informasi Eksekutif</span></a>
            </li>
        @endcan

        <li class="menu-header">Navigasi</li>

        @can('Menu Arsip Kolektor')
            <li class="dropdown {{ Route::is('instrumen-survey.index') ? 'active' : '' }} {{ Route::is('list-arsip.index') ? 'active' : '' }} {{ Route::is('list-arsip.edit') ? 'active' : '' }} {{ Route::is('list-arsip.detail') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-archive"></i> 
                    <span>Arsip Kolektor</span>
                </a>
                <ul class="dropdown-menu">
                    @can('List Survey Arsip Kolektor')
                        <li class="{{ Route::is('instrumen-survey.index') ? 'active' : '' }}">
                            <a href="{{ route('instrumen-survey.index') }}" class="nav-link">Instrumen Survey</a>
                        </li>
                    @endcan

                    @can('List Arsip Kolektor')
                        <li class="{{ Route::is('list-arsip.index') ? 'active' : '' }} {{ Route::is('list-arsip.edit') ? 'active' : '' }} {{ Route::is('list-arsip.detail') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('list-arsip.index') }}">List Arsip</a>
                        </li>
                    @endcan
                </ul>
            </li>
            
        @endcan 

        @can('Menu Metadata Management')
            <li class="dropdown {{ Route::is('metadata.index') ? 'active' : '' }} {{ Route::is('glosarium.index') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-project-diagram"></i> 
                    <span>Metadata Management</span> 
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Route::is('metadata.index') ? 'active' : '' }}"><a class="nav-link" href="{{ route('metadata.index') }}">Formatting & Pencarian </a></li>
                    <li class="{{ Route::is('glosarium.index') ? 'active' : '' }}"><a class="nav-link" href="{{ route('glosarium.index') }}">Glosarium Metadata </a></li>
                </ul>
            </li>
        @endcan

        @can('Menu Identifikasi Arsip')
            <li class="dropdown {{ Route::is('taxonomy.index') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-archive"></i> 
                    <span>Identifikasi Arsip</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Route::is('taxonomy.index') ? 'active' : '' }}"><a class="nav-link" href="{{ route('taxonomy.index') }}">Taxonomy Arsip </a></li>
                    <li class="{{ Route::is('list-arsip-digital.index') ? 'active' : '' }}"><a class="nav-link" href="{{ route('list-arsip-digital.index') }}">List Arsip Digital</a></li>
                </ul>
            </li>
        @endcan

        @can('Kotak Data Masuk Master Data')
            @if (auth()->user()->roles->first()->name != 'super-admin' && auth()->user()->roles->first()->name != 'admin')
                <li class="dropdown {{ Route::is('kinerja.inbox') ? 'active' : '' }}">
                    @php
                        $inboxmasterdata = App\Models\Kinerja::where('status', '2')->get()->groupBy('category_id','year');
                    @endphp
                    <a class="nav-link" href="{{ route('kinerja.inbox') }}">
                        <i class="fas fa-envelope"></i> 
                        <span>
                            Kotak Data Masuk
                            @if ($inboxmasterdata->count() > 0)
                                <div class="badge badge-danger">{{ $inboxmasterdata->count() }}</div>
                            @endif
                        </span>
                    </a>
                </li>
            @endif
        @endcan

        @can('Kotak Data Masuk Pengolah Data')
            @if (auth()->user()->roles->first()->name != 'super-admin' && auth()->user()->roles->first()->name != 'admin')
                <li class="dropdown {{ Route::is('kinerja.inbox') ? 'active' : '' }}">
                    @php
                        $inboxpengolahdata = App\Models\Kinerja::where('status','3')->where('pengolahdata_id', auth()->user()->id)->get()->groupBy('category_id','year');
                    @endphp
                    <a class="nav-link" href="{{ route('kinerja.inbox') }}">
                        <i class="fas fa-envelope"></i> 
                        <span>Kotak Data Masuk
                            @if ($inboxpengolahdata->count() > 0)
                                <div class="badge badge-danger">{{ $inboxpengolahdata->count() }}</div>
                            @endif
                        </span>
                    </a>
                </li>
            @endif
        @endcan
        
        @can('Menu Master Data')
            <li class="dropdown {{ Route::is('instansi.index') ? 'active' : '' }} {{ Route::is('instansi.edit') ? 'active' : '' }} {{ Route::is('work-units.index') ? 'active' : '' }} {{ Route::is('work-units.edit') ? 'active' : '' }} {{ Route::is('survey.index') ? 'active' : '' }} {{ Route::is('survey.edit') ? 'active' : '' }} {{ Route::is('jenis-arsip.index') ? 'active' : '' }} {{ Route::is('jenis-arsip.edit') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown">
                    <i class="fas fa-th-large"></i>
                    <span>Master Data</span>
                </a>
                <ul class="dropdown-menu">
                    @can('List Instansi')
                        <li class="{{ Route::is('instansi.index') ? 'active' : '' }} {{ Route::is('instansi.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('instansi.index') }}">Instansi</a>
                        </li>
                    @endcan
                    @can('List Unit Kerja')
                        <li class="{{ Route::is('work-units.index') ? 'active' : '' }} {{ Route::is('work-units.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('work-units.index') }}">Unit Kerja</a>
                        </li>
                    @endcan
                    @can('List Survey')
                        <li class="{{ Route::is('survey.index') ? 'active' : '' }} {{ Route::is('survey.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('survey.index') }}">Survey</a>
                        </li>
                    @endcan
                    <li class="{{ Route::is('jenis-arsip.index') ? 'active' : '' }} {{ Route::is('jenis-arsip.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('jenis-arsip.index') }}">Jenis Arsip</a>
                    </li>
                </ul>
            </li>
        @endcan

        @can('Menu Modul Data Kinerja')
            @can('Grafik Data Kinerja')
                <li class="nav-item {{ Route::is('chart-kinerja') ? 'active' : '' }}">
                    <a href="{{ route('chart-kinerja') }}" class="nav-link"><i class="fas fa-chart-line"></i><span>Grafik Data Kinerja</span></a>
                </li>
            @endcan

            <li class="dropdown {{ Route::is('categories.index') ? 'active' : '' }} {{ Route::is('categories.sub-category') ? 'active' : '' }} {{ Route::is('categories.sub-category.edit') ? 'active' : '' }} {{ Route::is('categories.edit') ? 'active' : '' }} {{ Route::is('kinerja.index') ? 'active' : '' }} {{ Route::is('kinerja.create') ? 'active' : '' }} {{ Route::is('categories.edit') ? 'active' : '' }}">
                
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-chart-area"></i> 
                    <span>Modul Data Kinerja</span>
                </a>

                <ul class="dropdown-menu">
                    @can('List Data Kinerja')
                        <li class="{{ Route::is('kinerja.index') ? 'active' : '' }} {{ Route::is('kinerja.create') ? 'active' : '' }} {{ Route::is('kinerja.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('kinerja.index') }}">Data Kinerja</a>
                        </li>
                    @endcan

                    @can('List Kategori')
                        <li class="{{ Route::is('categories.index') ? 'active' : '' }} {{ Route::is('categories.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('categories.index') }}">Master Kategori</a>
                        </li>
                    @endcan

                    @can('List Sub Kategori')
                        <li class="{{ Route::is('categories.sub-category') ? 'active' : '' }} {{ Route::is('categories.sub-category.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('categories.sub-category') }}">Master Sub Kategori</a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan

        @can('Master Data Kinerja')
            <li class="dropdown {{ Route::is('visi-misi.index') ? 'active' : '' }} {{ Route::is('visi-misi.edit') ? 'active' : '' }} {{ Route::is('tugas-fungsi.index') ? 'active' : '' }} {{ Route::is('tugas-fungsi.edit') ? 'active' : '' }}  {{ Route::is('struktur-organisasi.index') ? 'active' : '' }} {{ Route::is('struktur-organisasi.edit') ? 'active' : '' }} {{ Route::is('indikator-kinerja.index') ? 'active' : '' }} {{ Route::is('indikator-kinerja.edit') ? 'active' : '' }} {{ Route::is('target-kinerja.index') ? 'active' : '' }} {{ Route::is('target-kinerja.edit') ? 'active' : '' }} {{ Route::is('sasaran-strategis.index') ? 'active' : '' }} {{ Route::is('sasaran-strategis.edit') ? 'active' : '' }} {{ Route::is('peran-strategis.index') ? 'active' : '' }} {{ Route::is('peran-strategis.edit') ? 'active' : '' }} {{ Route::is('tujuan.index') ? 'active' : '' }} {{ Route::is('tujuan.edit') ? 'active' : '' }} {{ Route::is('rencana-strategis.index') ? 'active' : '' }} {{ Route::is('rencana-strategis.edit') ? 'active' : '' }} {{ Route::is('perjanjian-kinerja.index') ? 'active' : '' }} {{ Route::is('perjanjian-kinerja.edit') ? 'active' : '' }} {{ Route::is('capaian-kinerja.index') ? 'active' : '' }} {{ Route::is('capaian-kinerja.edit') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-chart-bar"></i> 
                    <span>Master Data Kinerja</span>
                </a>

                <ul class="dropdown-menu">
                    @can('List Visi & Misi')
                        <li class="{{ Route::is('visi-misi.index') ? 'active' : '' }} {{ Route::is('visi-misi.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('visi-misi.index') }}">Visi & Misi </a>
                        </li>
                    @endcan
                    @can('List Tugas & Fungsi')
                        <li class="{{ Route::is('tugas-fungsi.index') ? 'active' : '' }} {{ Route::is('tugas-fungsi.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('tugas-fungsi.index') }}">Tugas & Fungsi </a>
                        </li>
                    @endcan
                    @can('List Struktur Organisasi')
                        <li class="{{ Route::is('struktur-organisasi.index') ? 'active' : '' }} {{ Route::is('struktur-organisasi.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('struktur-organisasi.index') }}">Struktur Organisasi</a>
                        </li>
                    @endcan
                    @can('List Indikator Kinerja')
                        <li class="{{ Route::is('indikator-kinerja.index') ? 'active' : '' }} {{ Route::is('indikator-kinerja.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('indikator-kinerja.index') }}">Indikator Kinerja</a>
                        </li>
                    @endcan
                    @can('List Target Kinerja')
                    <li class="{{ Route::is('target-kinerja.index') ? 'active' : '' }} {{ Route::is('target-kinerja.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('target-kinerja.index') }}">Target Kinerja</a>
                    </li>
                    @endcan
                    <li class="{{ Route::is('sasaran-strategis.index') ? 'active' : '' }} {{ Route::is('sasaran-strategis.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('sasaran-strategis.index') }}">Sasaran Strategis</a>
                    </li>
                    <li class="{{ Route::is('tujuan.index') ? 'active' : '' }} {{ Route::is('tujuan.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('tujuan.index') }}">Tujuan</a>
                    </li>
                    <li class="{{ Route::is('peran-strategis.index') ? 'active' : '' }} {{ Route::is('peran-strategis.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('peran-strategis.index') }}">Peran Strategis</a>
                    </li>
                    <li class="{{ Route::is('rencana-strategis.index') ? 'active' : '' }} {{ Route::is('rencana-strategis.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('rencana-strategis.index') }}">Rencana Strategis</a>
                    </li>
                    <li class="{{ Route::is('perjanjian-kinerja.index') ? 'active' : '' }} {{ Route::is('perjanjian-kinerja.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('perjanjian-kinerja.index') }}">Perjanjian Kinerja</a>
                    </li>
                    <li class="{{ Route::is('capaian-kinerja.index') ? 'active' : '' }} {{ Route::is('capaian-kinerja.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('capaian-kinerja.index') }}">Capaian & Realisasi Kinerja</a>
                    </li>
                    <li class="{{ Route::is('realisasi-anggaran.index') ? 'active' : '' }} {{ Route::is('realisasi-anggaran.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('realisasi-anggaran.index') }}">Realisasi Anggaran</a>
                    </li>
                    <li class="{{ Route::is('capaian-spbe.index') ? 'active' : '' }} {{ Route::is('capaian-spbe.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('capaian-spbe.index') }}">Capaian SPBE</a>
                    </li>
                    <li class="{{ Route::is('capaian.index') ? 'active' : '' }} {{ Route::is('capaian.edit') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('capaian.index') }}">Capaian Lainnya</a>
                    </li>
                </ul>
            </li>
        @endcan

        @can('List Model Kinerja')
            <li class="dropdown">
                <a href="{{ route('model-kinerja.index') }}" class="nav-link"><i class="fas fa-chart-pie"></i> <span>Model Kinerja </span></a>
            </li>
        @endcan

        @can('Menu Informasi Publik')
            <li class="dropdown {{ Route::is('news.index') ? 'active' : '' }} {{ Route::is('news.create') ? 'active' : '' }} {{ Route::is('news.edit') ? 'active' : '' }} {{ Route::is('infografis.index') ? 'active' : '' }} {{ Route::is('infografis.edit') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown">
                    <i class="fas fa-globe-asia"></i>
                    <span>Informasi Publik</span>
                </a>

                <ul class="dropdown-menu">
                    @can('List Berita')
                        <li class="{{ Route::is('news.index') ? 'active' : '' }} {{ Route::is('news.create') ? 'active' : '' }} {{ Route::is('news.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('news.index') }}">Berita</a>
                        </li>
                    @endcan

                    @can('List Infografis')
                        <li class="{{ Route::is('infografis.index') ? 'active' : '' }} {{ Route::is('infografis.edit') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('infografis.index') }}">Infografis</a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan

        @can('Menu User Management')
            <li class="dropdown {{ Route::is('user.profile') ? 'active' : '' }} {{ Route::is('users.index') ? 'active' : '' }} {{ Route::is('users.show') ? 'active' : '' }} {{ Route::is('activities.index') ? 'active' : '' }} {{ Route::is('roles.index') ? 'active' : '' }} {{ Route::is('roles.create') ? 'active' : '' }} {{ Route::is('roles.edit') ? 'active' : '' }} {{ Route::is('permissions.index') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown">
                    <i class="fas fa-users"></i>
                    <span>User Manajemen</span>
                </a>

                <ul class="dropdown-menu">
                    @can('List User')
                        <li class="{{ Route::is('users.index') ? 'active' : '' }} {{ Route::is('users.show') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('users.index') }}">Pengguna</a>
                        </li>
                    @endcan

                    @can('List Log Aktivitas')
                        <li class="{{ Route::is('activities.index') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('activities.index') }}">Log Aktifitas</a>
                        </li>
                    @endcan

                    @can('List Role')
                        <li class="{{ Route::is('roles.index') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('roles.index') }}">Jenis Pengguna</a>
                        </li>
                    @endcan
                    
                    @can('List Hak Akses')
                        <li class="{{ Route::is('permissions.index') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('permissions.index') }}">Hak Akses</a>
                        </li>
                    @endcan

                    @can('User Profile')
                        <li class="{{ Route::is('user.profile') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('user.profile') }}">Profile</a>
                        </li>
                    @endcan

                    <li>
                        <a class="nav-link text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    </li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </ul>
            </li>
        @endcan

        @can('Menu Pengaturan')
            <li class="nav-item {{ Route::is('setting') ? 'active' : '' }}">
                <a href="{{ route('setting') }}" class="nav-link">
                    <i class="fas fa-cog"></i>
                    <span>Pengaturan</span>
                </a>
            </li>
        @endcan
    </ul>
</aside>