<div class="navbar-bg">

</div>
<nav class="navbar navbar-expand-lg main-navbar">

    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li>
                <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg">
                    <i class="fas fa-bars"></i>
                </a>
            </li>
        </ul>
    </form>

    <ul class="navbar-nav navbar-right">
        @can('show-notif-activity')
            <li class="dropdown dropdown-list-toggle">
                <a href="#" data-toggle="dropdown" class="nav-link nav-link-lg message-toggle beep">
                    <i class="fas fa-history"></i>
                </a>
                
                <div class="dropdown-menu dropdown-list dropdown-menu-right">
                    <div class="dropdown-header">Aktivity Log
                    </div>
                    <div class="dropdown-list-content dropdown-list-message">
                        @foreach(Spatie\Activitylog\Models\Activity::orderBy('created_at', 'DESC')->take(7)->get() as $activitys)
                            <a href="#" class="dropdown-item">
                                <div class="dropdown-item-avatar">
                                    <img alt="image" src="{{ asset('admin/stisla/assets/img/avatar/avatar-2.png') }}" class="rounded-circle">
                                </div>
                                <div class="dropdown-item-desc">
                                    <b>{{ $activitys->log_name }}</b>
                                    <p>{{ $activitys->description }}</p>
                                    <div class="time">{{ \Carbon\Carbon::parse($activitys->created_at)->diffForHumans() }}</div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                    <div class="dropdown-footer text-center">
                        <a href="{{ route('activities.index') }}">Lihat Semua <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </li>
        @endcan

        @if (auth()->user()->roles()->first()->name != 'super-admin')
            @can('Notifikasi Produsen Data')
                <li class="dropdown dropdown-list-toggle">
                    <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg" id="notification"><i class="far fa-bell"></i>
                        @php
                            $notif =\DB::table('notifications')->where('user_id', Auth::user()->id)->orderBy('created_at','DESC')->get();
                        @endphp
                    </a>
                    <div class="dropdown-menu dropdown-list dropdown-menu-right">
                        <div class="dropdown-header">Notifikasi
                            <div class="float-right">
                                @if ($notif->where('read_at', null)->count() > 0)
                                    <a href="javascript:void(0)" id="read-notif">Tandai Semua Telah Di Baca</a>
                                @endif
                            </div>
                        </div>
                        <div class="dropdown-list-content dropdown-list-icons">
                            @foreach ($notif as $n)
                                @if ($n->read_at == null)
                                    <a href="#" class="dropdown-item dropdown-item-unread">
                                        <div class="dropdown-item-icon bg-info text-white">
                                            <i class="fas fa-bell"></i>
                                        </div>
                                        <div class="dropdown-item-desc">
                                            {{ $n->title }}
                                            <br>
                                            <span style="font-size: 10px">{{ $n->message }}</span>
                                            <div class="time text-primary">{{ \Carbon\Carbon::parse($n->created_at)->diffForHumans() }}
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href="#" class="dropdown-item">
                                        <div class="dropdown-item-icon bg-info text-white">
                                            <i class="fas fa-bell"></i>
                                        </div>
                                        <div class="dropdown-item-desc">
                                            {{ $n->title }}
                                            <br>
                                            <span style="font-size: 10px">{{ $n->message }}</span>
                                            <div class="time text-primary">{{ \Carbon\Carbon::parse($n->created_at)->diffForHumans() }}
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                        <div class="dropdown-footer text-center">
                            <a href="#">Lihat Semua <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </li>
            @endcan

            @can('Notifikasi Master Data')
                <li class="dropdown dropdown-list-toggle">
                    <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg" id="notification"><i class="far fa-bell"></i>
                        @php
                            $notif =\DB::table('notifications')->where('user_id', Auth::user()->id)->orderBy('created_at','DESC')->get();
                        @endphp
                    </a>
                    <div class="dropdown-menu dropdown-list dropdown-menu-right">
                        <div class="dropdown-header">Notifikasi
                            <div class="float-right">
                                @if ($notif->where('read_at', null)->count() > 0)
                                    <a href="javascript:void(0)" id="read-notif">Tandai Semua Telah Di Baca</a>
                                @endif
                            </div>
                        </div>
                        <div class="dropdown-list-content dropdown-list-icons">
                            @foreach ($notif as $n)
                                @if ($n->read_at == null)
                                    <a href="#" class="dropdown-item dropdown-item-unread">
                                        <div class="dropdown-item-icon bg-info text-white">
                                            <i class="fas fa-bell"></i>
                                        </div>
                                        <div class="dropdown-item-desc">
                                            {{ $n->title }}
                                            <br>
                                            <span style="font-size: 10px">{{ $n->message }}</span>
                                            <div class="time text-primary">{{ \Carbon\Carbon::parse($n->created_at)->diffForHumans() }}
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href="#" class="dropdown-item">
                                        <div class="dropdown-item-icon bg-info text-white">
                                            <i class="fas fa-bell"></i>
                                        </div>
                                        <div class="dropdown-item-desc">
                                            {{ $n->title }}
                                            <br>
                                            <span style="font-size: 10px">{{ $n->message }}</span>
                                            <div class="time text-primary">{{ \Carbon\Carbon::parse($n->created_at)->diffForHumans() }}
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                        <div class="dropdown-footer text-center">
                            <a href="#">Lihat Semua <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </li>
            @endcan

            @can('Notifikasi Pengolah Data')
                <li class="dropdown dropdown-list-toggle">
                    <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg" id="notification"><i class="far fa-bell"></i>
                        @php
                            $notif =\DB::table('notifications')->where('user_id', Auth::user()->id)->orderBy('created_at','DESC')->get();
                        @endphp
                    </a>
                    <div class="dropdown-menu dropdown-list dropdown-menu-right">
                        <div class="dropdown-header">Notifikasi
                            <div class="float-right">
                                @if ($notif->where('read_at', null)->count() > 0)
                                    <a href="javascript:void(0)" id="read-notif">Tandai Semua Telah Di Baca</a>
                                @endif
                            </div>
                        </div>
                        <div class="dropdown-list-content dropdown-list-icons">
                            @foreach ($notif as $n)
                                @if ($n->read_at == null)
                                    <a href="#" class="dropdown-item dropdown-item-unread">
                                        <div class="dropdown-item-icon bg-info text-white">
                                            <i class="fas fa-bell"></i>
                                        </div>
                                        <div class="dropdown-item-desc">
                                            {{ $n->title }}
                                            <br>
                                            <span style="font-size: 10px">{{ $n->message }}</span>
                                            <div class="time text-primary">{{ \Carbon\Carbon::parse($n->created_at)->diffForHumans() }}
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href="#" class="dropdown-item">
                                        <div class="dropdown-item-icon bg-info text-white">
                                            <i class="fas fa-bell"></i>
                                        </div>
                                        <div class="dropdown-item-desc">
                                            {{ $n->title }}
                                            <br>
                                            <span style="font-size: 10px">{{ $n->message }}</span>
                                            <div class="time text-primary">{{ \Carbon\Carbon::parse($n->created_at)->diffForHumans() }}
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                        <div class="dropdown-footer text-center">
                            <a href="#">Lihat Semua <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </li>
            @endcan
        @endif

        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="{{ Auth::user()->getAvatar() }}" class="rounded-circle mr-1" style="width: 40px; height: 40px">
                <div class="d-sm-none d-lg-inline-block">
                    Hi, {{ Auth::user()->name }}
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-title">
                    @if(Cache::has('user-is-online-' . auth()->user()->id))
                        <span class="text-success">Online</span>
                    @else
                        <span class="text-secondary">Offline</span>
                    @endif
                </div>

                <a href="{{ route('user.profile') }}" class="dropdown-item has-icon">
                    <i class="far fa-user mt-1"></i> Profile
                </a>

                @if (auth()->user()->roles()->first()->name == 'super-admin')
                    <a href="{{ route('activities.index') }}" class="dropdown-item has-icon">
                        <i class="fas fa-history mt-1"></i> Log Aktivitas
                    </a>

                    <a href="{{ route('setting') }}" class="dropdown-item has-icon">
                        <i class="fas fa-cog mt-1"></i>
                        Pengaturan
                    </a>
                @endif

                <div class="dropdown-divider"></div>

                <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>