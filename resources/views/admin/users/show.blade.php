@extends('layouts.dashboard')

@section('title')
Detail User
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Detail User</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">User Manajemen</a></div>
            <div class="breadcrumb-item">Detail User</div>
        </div>
    </div>
    <div class="section-body">
        <h2 class="section-title">Detail User {{ $user->name }}</h2>

        <div class="row mt-sm-4">
            <div class="col-12 col-md-12 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <div class="user-item">
                            <img alt="image" src="{{ $user->getAvatar() }}" class="img-fluid">
                            <div class="user-details">
                                <div class="user-name">{{ $user->name }}</div>
                                <div class="user-cta">
                                    @if(Cache::has('user-is-online-' . $user->id))
                                    <span class="text-success">Online</span>
                                    @else
                                    <span class="text-danger">
                                        Offline
                                        @if($user->last_seen != null)
                                        {{ \Carbon\Carbon::parse($user->last_seen)->diffForHumans() }}
                                        @endif
                                    </span>
                                    @endif
                                    <br>
                                    @if ($user->roles()->first()->description == 'Eksekutif')
                                    <span class="badge badge-info mt-3">{{ $user->roles()->first()->description
                                        }}</span>
                                    @elseif ($user->roles()->first()->description == 'Administrator')
                                    <span class="badge badge-primary mt-3">{{ $user->roles()->first()->description
                                        }}</span>
                                    @else
                                    <label class="badge badge-success mt-3">{{ $user->roles()->first()->description
                                        }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#activity" role="tab" aria-controls="pills-activity" aria-selected="true">Aktivitas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#update-profile" role="tab" aria-controls="pills-update-profile" aria-selected="false">Info Profile</a>
                            </li>
                        </ul>
                        <div class="tab-content mt-3">
                            <div class="tab-pane fade show active" id="activity" role="tabpanel" aria-labelledby="activity-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped dataTable" id="table-1">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Log</th>
                                                <th>Waktu</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no = 1;
                                            @endphp
                                            @foreach ($activity as $act)
                                            @if ($act->causer_id == $user->id)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $act->description }}</td>
                                                <td>{{ \Carbon\Carbon::parse($act->created_at)->diffForHumans() }}</td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="update-profile" role="tabpanel" aria-labelledby="profile-tab">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Nama Lengkap</th>
                                            <td>:</td>
                                            <td>{{ $user->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Username</th>
                                            <td>:</td>
                                            <td>{{ $user->username }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                        <tr>
                                            <th>Jenis User</th>
                                            <td>:</td>
                                            <td>
                                                @if ($user->roles()->first()->description == 'Eksekutif')
                                                    <span class="badge badge-info">{{ $user->roles()->first()->description }}</span>
                                                @elseif ($user->roles()->first()->description == 'Administrator')
                                                    <span class="badge badge-primary">{{ $user->roles()->first()->description }}</span>
                                                @else
                                                    <label class="badge badge-success">{{ $user->roles()->first()->description }}</label>
                                                @endif
                                            </td>
                                        </tr>

                                        @if ($user->roles()->first()->description == 'Produsen Data' || $user->roles()->first()->description == 'Master Data' || $user->roles()->first()->description == 'Pengolah Data' || $user->roles()->first()->description == 'Administrator')
                                            <tr>
                                                <th>Unit Kerja</th>
                                                <td>:</td>
                                                <td>{{ $user->workunit->name }}</td>
                                            </tr>
                                        @elseif($user->roles()->first()->description == 'Instansi')
                                            <tr>
                                                <th>Instansi</th>
                                                <td>:</td>
                                                <td>{{ $user->instansi->nama_instansi }}</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection