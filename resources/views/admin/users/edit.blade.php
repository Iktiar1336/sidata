@extends('layouts.dashboard')

@section('title')
Edit User
@endsection

@section('content')

<section class="section">
    <div class="section-header">
        <h1>Edit User</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">User</a></div>
            <div class="breadcrumb-item">Edit User</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit User</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('users.index') }}" class="btn btn-primary">Kembali</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('users.update', Crypt::encrypt($user->id)) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>Nama Lengkap:</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                    value="{{ $user->name }}" placeholder="Nama Lengkap" readonly name="name" required readonly>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Username :</label>
                                <input type="text" class="form-control @error('username') is-invalid @enderror"
                                    value="{{ $user->username }}" placeholder="Username" name="username" readonly required
                                    readonly>
                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group" id="input_unitkerja">
                                <label>Pilih Unit Kerja :</label>
                                <select class="selectpicker2" data-show-subtext="true" name="workunit_id"
                                    data-live-search="true">
                                    @foreach ($workunit as $s)
                                    <option value="{{ $s->id }}" {{ $user->workunit_id == $s->id ? 'selected':'' }}>{{
                                        $s->name }}</option>
                                    @endforeach
                                </select>
                                @error('workunit_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group" id="instansi">
                                <label>Pilih Instansi :</label><small class="text-danger">*</small>
                                <select class="selectpicker2" data-show-subtext="true" name="instansi_id" data-live-search="true">
                                    <option value="" disabled selected aria-required="true">Pilih Instansi</option>
                                    @foreach ($instansi as $s)
                                        <option value="{{ $s->id }}" {{ $user->instansi_id == $s->id ? 'selected' : '' }}>{{ $s->nama_instansi }}</option>
                                    @endforeach
                                </select>
                                @error('instansi_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Pilih Role :</label>
                                <select class="selectpicker2" data-show-subtext="true" name="role_id" id="role_id" data-live-search="true">
                                    @foreach ($role as $r)
                                        @if ($r->name != 'super-admin')
                                            <option value="{{ $r->id }}" {{ $userRole->id == $r->id ? 'selected':'' }}>{{ $r->description }}</option> 
                                        @endif
                                    @endforeach
                                </select>
                                @error('role_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                                <label>Email:</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                    value="{{ $user->email }}" placeholder="Email" name="email" required readonly>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div> --}}
                            <div class="form-group">
                                <label>Password :</label>
                                <input id="password" type="password"
                                    class="form-control form-control-user @error('password') is-invalid @enderror"
                                    value="{{ old('value') }}" name="password" tabindex="2"
                                    autocomplete="new-password" placeholder="Masukan password anda">
                                @error('password')
                                <span class="invalid-feedback">
                                    {{ $message }}
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>konfirmasi Password :</label>
                                <input id="password-confirm" type="password" class="form-control form-control-user"
                                    name="password_confirmation" tabindex="3" autocomplete="new-password"
                                    placeholder="Masukan konfirmasi password anda">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        $('.selectpicker2').selectpicker();
        var role_id = $('#role_id').val();
        if (role_id == 2) {
            $('#input_unitkerja').hide();
            $('#instansi').show();
        } else if (role_id == 3) {
            $('#input_unitkerja').hide();
            $('#instansi').hide();
        } else if (role_id == 4) {
            $('#input_unitkerja').show();
            $('#instansi').hide();
        } else {
            $('#input_unitkerja').show();
            $('#instansi').hide();
        }
        $('#role_id').on('change', function () {
            if ($(this).val() == 2) {
                $('#input_unitkerja').hide();
                $('#telegram').hide();
                $('#instansi').show();
            } else if ($(this).val() == 3) {
                $('#input_unitkerja').hide();
                $('#telegram').hide();
                $('#instansi').hide();
            } else if ($(this).val() == 4) {
                $('#input_unitkerja').show();
                $('#instansi').hide();
            } else {
                $('#input_unitkerja').show();
                $('#telegram').show();
                $('#instansi').hide();
            }
        });
    });
</script>
@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <script>
            swal("{{ $error }}", {
                        title: "Failed",
                        icon: "error",
                    });
        </script>
    @endforeach
@endif
@endsection