@can('user-edit')
    @if ($row->id != Auth::user()->id && $row->name != 'superadmin')
        <a href="{{ route('users.edit', Crypt::encrypt($row->id)) }}" class="btn btn-warning btn-sm"><i class="fa fa-pen"></i></a>

    @endif
@endcan
@can('user-show')
    <a class="btn btn-info btn-sm" href="{{ route('users.show', Crypt::encrypt($row->id)) }}"><i class="fa fa-eye"></i></a>
@endcan
@can('user-delete')
    @if ($row->name != 'super-admin' && auth()->user()->id != $row->id)
        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy',
        Crypt::encrypt($row->id)],'style'=>'display:inline']) !!}
        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class'
        => 'btn btn-danger btn-sm delete']) !!}
        {!! Form::close() !!}
    @endif
@endcan

<script>
    $('.delete').on('click', function (event) {
            event.preventDefault();
            const form = $(this).closest('form');
            swal({
                title: "Apakah anda yakin?",
                text: "User ini akan di hapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    form.submit();
                }
            });
    });
</script>