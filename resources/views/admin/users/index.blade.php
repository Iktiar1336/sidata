@extends('layouts.dashboard')

@section('title')
List Pengguna
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Pengguna</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">User Manajemen</a></div>
            <div class="breadcrumb-item">Pengguna</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Pengguna</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        @can('Tambah User')
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahuser">
                            Tambah Pengguna
                        </button>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table dataTable" id="table-user" width="120%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Unit Kerja / Instansi</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="tambahuser" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('users.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Pilih Role :</label><small class="text-danger">*</small>
                        <select class="selectpicker2" data-show-subtext="true" id="role" name="role_id" data-live-search="true">
                            <option value="" disabled selected>Pilih Role</option>
                            @foreach ($role as $r)
                                @if ($r->name != 'super-admin')
                                    <option value="{{ $r->id }}">{{ $r->description }}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('role_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group" id="instansi">
                        <label>Pilih Instansi :</label><small class="text-danger">*</small>
                        <select class="selectpicker2" data-show-subtext="true" name="instansi_id" data-live-search="true">
                            <option value="" disabled selected aria-required="true">Pilih Instansi</option>
                            @foreach ($instansi as $s)
                            <option value="{{ $s->id }}">{{ $s->nama_instansi }}</option>
                            @endforeach
                        </select>
                        @error('instansi_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group" id="unit-kerja">
                        <label>Pilih Unit Kerja :</label><small class="text-danger">*</small>
                        <select class="selectpicker2" data-show-subtext="true" name="workunit_id" data-live-search="true">
                            <option value="" disabled selected aria-required="true">Pilih Unit Kerja</option>
                            @foreach ($workunit as $s)
                            <option value="{{ $s->id }}">{{ $s->name }}</option>
                            @endforeach
                        </select>
                        @error('workunit_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap:</label><small class="text-danger">*</small>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('value') }}" placeholder="Nama Lengkap" name="name" required>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Username :</label><small class="text-danger">*</small>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" placeholder="Username" name="username" required>
                        @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group" id="telegram">
                        <label>Telegram ID :</label><small class="text-danger">*</small>
                        <input type="number" class="form-control @error('telegram_id') is-invalid @enderror" value="{{ old('telegram_id') }}" placeholder="Telegram ID" name="telegram_id">
                        <small class="text-success">*Telegram ID digunakan untuk mengirimkan notifikasi ke Telegram.</small>
                        @error('telegram_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Email:</label> <small class="text-danger">*</small>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Email" name="email" required>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@if (session('insert-user-success'))
<script>
    swal("User berhasil di tambahkan", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('update-user-success'))
<script>
    swal("User berhasil di update", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-user-success'))
<script>
    swal("User berhasil di hapus", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-user-failed'))
<script>
    swal("User tidak dapat dihapus karena masih memiliki data", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

@if (session('error'))
<script>
    swal("{{ session('error') }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

<script>
    $(document).ready(function () {
        $('.selectpicker2').selectpicker();
        $('.bg-random').css('background-color', function () {
            return '#' + Math.floor(Math.random() * 16777215).toString(16);
        });
        $('#table-user').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('users.index') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'username',
                    name: 'username'
                },
                {
                    data: 'unitkerja',
                    name: 'unitkerja'
                },
                {
                    data: 'role',
                    name: 'role'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ]
        });
        $('#unit-kerja').hide();
        $('#instansi').hide();
        $('#telegram').hide();
        $('#role').on('change', function () {
            if ($(this).val() == 2) {
                $('#unit-kerja').hide();
                $('#telegram').hide();
                $('#instansi').show();
            } else if ($(this).val() == 3) {
                $('#unit-kerja').hide();
                $('#telegram').hide();
                $('#instansi').hide();
            } else if ($(this).val() == 4) {
                $('#unit-kerja').show();
                $('#instansi').hide();
            } else {
                $('#unit-kerja').show();
                $('#telegram').show();
                $('#instansi').hide();
            }
        });
    });
</script>
@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection