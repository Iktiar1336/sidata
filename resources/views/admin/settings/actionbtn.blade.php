<form action="{{ route('delete-api-client', Crypt::encrypt($row->id)) }}" method="post" class="btn-group">
    @csrf
    @method('delete')

    <button class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i></button>
</form>