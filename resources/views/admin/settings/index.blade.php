@extends('layouts.dashboard')

@section('title')
Setting
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Settings</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Settings</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Overview</h2>
        <p class="section-lead">
            Organize and adjust all settings about this site.
        </p>

        <div class="row">
            <div class="col-md-3 mt-2">
                <div class="card">
                    <div class="card-header">
                        <h4>Filter</h4>
                    </div>
                    <div class="card-body">
                        <div class="list-group" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action active" id="list-app-setting-list" data-toggle="list" href="#list-app-setting" role="tab" aria-controls="app-setting"><i class="fas fa-cog"></i> &nbsp;&nbsp;Aplikasi</a>
                            <a class="list-group-item list-group-item-action" id="list-api-setting-list" data-toggle="list" href="#list-api-setting" role="tab" aria-controls="api-setting"><i class="fas fa-users-cog"></i> &nbsp;&nbsp;API Client</a>
                            <a class="list-group-item list-group-item-action" id="list-email-setting-list" data-toggle="list" href="#list-email-setting" role="tab" aria-controls="email-setting"><i class="fas fa-envelope"></i> &nbsp;&nbsp;Email</a>
                            <a class="list-group-item list-group-item-action" id="list-telegram-setting-list" data-toggle="list" href="#list-telegram-setting" role="tab" aria-controls="telegram-setting"><i class="fas fa-paper-plane"></i> &nbsp;&nbsp;Telegram</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-app-setting" role="tabpanel" aria-labelledby="list-app-setting-list">
                        <div class="card">
                            <div class="card-header">
                                <h4>Setting Aplikasi</h4>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-pills" id="myTab3" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="true">Aplikasi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#profile3" role="tab" aria-controls="profile" aria-selected="false">Sosial Media</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent2">
                                    <div class="tab-pane fade show active" id="home3" role="tabpanel" aria-labelledby="home-tab3">
                                        <form action="{{ route('setting.update.app') }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group">
                                                <label for="nama_aplikasi">Nama Aplikasi</label>
                                                <input type="text" class="form-control" id="nama_aplikasi" name="nama_aplikasi" value="{{ $settings->nama_aplikasi }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="deskripsi_aplikasi">Deskripsi Aplikasi</label>
                                                <textarea class="form-control" id="deskripsi_aplikasi" name="deskripsi_aplikasi" rows="3">{{ $settings->deskripsi_aplikasi }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_instansi">Nama Instansi</label>
                                                <input type="text" class="form-control" id="nama_instansi" name="nama_instansi" value="{{ $settings->nama_instansi }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat" class="d-block">Alamat</label>
                                                <textarea class="form-control" id="alamat" name="alamat" rows="3">{{ $settings->alamat }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="no_telp">No. Telp</label>
                                                <input type="text" class="form-control" id="no_telp" name="no_telp" value="{{ $settings->no_telp }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" id="email" name="email" value="{{ $settings->email }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="fax">Fax</label>
                                                <input type="text" class="form-control" id="fax" name="fax" value="{{ $settings->no_fax }}">
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="profile3" role="tabpanel" aria-labelledby="profile-tab3">
                                        Sed sed metus vel lacus hendrerit tempus. Sed efficitur velit tortor, ac efficitur est lobortis quis. Nullam lacinia metus erat, sed fermentum justo rutrum ultrices. Proin quis iaculis tellus. Etiam ac vehicula eros, pharetra consectetur dui.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-api-setting" role="tabpanel" aria-labelledby="list-api-setting-list">
                        <div class="card">
                            <div class="card-header">
                                <h4>Setting Aplikasi</h4>
                            </div>
                            <div class="card-body">
                                <button type="button" class="btn btn-primary mt-2 mb-4" data-toggle="modal" data-target="#apiclient">
                                    Tambah Client
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="apiclient" tabindex="-1" aria-labelledby="apiclientLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="apiclientLabel">Tambah Client</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{ route('create-api-client') }}" method="POST">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="name">Nama Client</label>
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Client" required>
                                                    </div>
                                                    {{-- <div class="form-group">
                                                        <label for="redirect">Redirect</label>
                                                        <input type="text" class="form-control" name="redirect" id="redirect" placeholder="Redirect">
                                                    </div> --}}
                                                    <button type="submit" class="btn btn-success btn-block">Simpan</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered" id="table-apiclient" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Client ID</th>
                                                <th>Nama</th>
                                                <th>Secret</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-email-setting" role="tabpanel" aria-labelledby="list-email-setting-list">
                        <div class="card">
                            <div class="card-header">
                                <h4>Pengaturan Email</h4>
                                <div class="card-header-action">
                                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-settingemail">Tes Email</button>
                                </div>

                                <form action="{{ route('email-test') }}" method="POST" class="modal-part" id="modal-settingemail-part" tabindex="-1">
                                    @csrf
                                    <div class="form-group">
                                        <label>Email Penerima :</label>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email Penerima" name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-block">Kirim</button>
                                    </div>
                                </form>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('email-setting.update', $emailSettings->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="email_driver">Driver</label>
                                        <select name="driver" id="email_driver" class="selecpicker">
                                            <option value="smtp" {{ $emailSettings->driver == 'smtp' ? 'selected' : '' }}>SMTP</option>
                                            <option value="mail" {{ $emailSettings->driver == 'mail' ? 'selected' : '' }}>Mail</option>
                                            <option value="sendmail" {{ $emailSettings->driver == 'sendmail' ? 'selected' : '' }}>Sendmail</option>
                                            <option value="mailgun" {{ $emailSettings->driver == 'mailgun' ? 'selected' : '' }}>Mailgun</option>
                                            <option value="ses" {{ $emailSettings->driver == 'ses' ? 'selected' : '' }}>SES</option>
                                            <option value="sparkpost" {{ $emailSettings->driver == 'sparkpost' ? 'selected' : '' }}>Sparkpost</option>
                                            <option value="log" {{ $emailSettings->driver == 'log' ? 'selected' : '' }}>Log</option>
                                            <option value="array" {{ $emailSettings->driver == 'array' ? 'selected' : '' }}>Array</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email_host">Host</label>
                                        <input type="text" name="host" id="email_host" class="form-control" value="{{ $emailSettings->host }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email_port">Port</label>
                                        <input type="text" name="port" id="email_port" class="form-control" value="{{ $emailSettings->port }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email_username">Username</label>
                                        <input type="text" name="username" id="email_username" class="form-control" value="{{ $emailSettings->username }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email_password">Password</label>
                                        <input type="text" name="password" id="email_password" class="form-control" value="{{ \Crypt::decryptString($emailSettings->password) }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email_encryption">Encryption</label>
                                        <select name="encryption" id="email_encryption" class="form-control">
                                            <option value="tls" {{ $emailSettings->encryption == 'tls' ? 'selected' : '' }}>TLS</option>
                                            <option value="ssl" {{ $emailSettings->encryption == 'ssl' ? 'selected' : '' }}>SSL</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email_from_address">Email Pengirim</label>
                                        <input type="text" name="email_pengirim" id="email_from_address" class="form-control" value="{{ $emailSettings->from_address }}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-block">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-telegram-setting" role="tabpanel" aria-labelledby="list-telegram-setting-list">
                        <div class="card">
                            <div class="card-header">
                                <h4>Pengaturan Telegram</h4>
                                <div class="card-header-action">

                                </div>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('telegram-setting.update', Crypt::encrypt($telegramSettings->id)) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="telegram_token">Token :</label>
                                        <input type="text" name="telegram_bot_token" id="telegram_token" class="form-control" value="{{ \Crypt::decryptString($telegramSettings->telegram_bot_token) }}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-block">Simpan</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection

@section('js')
<script>
    $(document).ready(function () {
        $('.selecpicker').selectpicker();

        $('#table-apiclient').DataTable({
            serverSide: true,
            processing: true,
            autoWidth: true,
            ajax: "/setting/api-client/get",
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { 
                    data: 'id', 
                    name : 'id',
                    render: function (data, type, row) {
                        return '<button class="btn btn-info btn-sm" onclick="copyToClipboard(`'+data+'`)"><i class="fas fa-copy"></i> Copy</button>';
                    }
                },
                { data: 'name', name: 'name' },
                { 
                    data: 'secret', 
                    name : 'secret',
                    render: function (data, type, row) {
                        return '<button class="btn btn-info btn-sm" onclick="copyToClipboard(`'+data+'`)"><i class="fas fa-copy"></i> Copy</button>';
                    }
                 },
                { data: 'action', name: 'action' },
            ]
        });
    });

    function copyToClipboard(text) {
        var dummy = document.createElement("textarea");
        document.body.appendChild(dummy);
        dummy.value = text;
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
        swal("Berhasil di copy", {
            title: "Success",
            icon: "success",
        });
    }
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endif

@if (session('setting-telegram-success'))
<script>
    swal("Setting Bot Telegram Berhasil Di Simpan", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('setting-email-success'))
<script>
    swal("Setting Email Berhasil Di Simpan", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('setting-telegram-failed'))
<script>
    swal("Setting Bot Telegram Gagal Di Simpan, Silahkan Cek Kembali Token Anda", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

@if (session('create-apiclient-success'))
<script>
    swal("API Client Berhasil Di Buat", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('delete-apiclient-success'))
<script>
    swal("API Client Berhasil Di Hapus", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('email-test-success'))
<script>
    swal("Email Berhasil Di Kirim", {
        title: "Success",
        icon: "success",
    });
</script>
@endif

@if (session('email-test-failed'))
<script>
    swal("Email Gagal Di Kirim, Silahkan Cek Kembali Pengaturan Email", {
        title: "Failed",
        icon: "error",
    });
</script>
@endif

@if(Session::has('success'))
<script>
    swal("{{ Session::get('success') }}", {
        title: "Success",
        icon: "success",
    });
</script>
@endif
@endsection