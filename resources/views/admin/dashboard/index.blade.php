@extends('layouts.dashboard')

@section('title')
Dashboard
@endsection

@section('css')
<style>
    .r-15 {
        border-radius: 15px;
    }

    .bg-light-blue {
        background-color: #CAF6F5;
    }

    .bg-light-orange {
        background-color: #FEEADF;
    }

    .bg-light-blue-dark {
        background-color: #E3EBFE;
    }

    #container {
        height: 318px !important;
        margin: 0 auto;
        border-radius: 10px;
        margin-top: 20px;
        margin-bottom: 20px
    }

    .title-card {
        margin-top: -10px
    }

    #listactivity {
        height: 350px;
        width: 100%;
        overflow-y: scroll;
    }

    .loading {
        left: 0;
        right: 0;
        top: 50%;
        width: 150px;
        color: #000;
        margin: auto;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .loading span {
        position: absolute;
        height: 10px;
        width: 84px;
        font-size: 14px;
        top: 50px;
        overflow: hidden;
    }

    .loading span>i {
        position: absolute;
        height: 4px;
        width: 4px;
        border-radius: 50%;
        -webkit-animation: wait 4s infinite;
        -moz-animation: wait 4s infinite;
        -o-animation: wait 4s infinite;
        animation: wait 4s infinite;
    }

    .loading span>i:nth-of-type(1) {
        left: -28px;
        background: yellow;
    }

    .loading span>i:nth-of-type(2) {
        left: -21px;
        -webkit-animation-delay: 0.8s;
        animation-delay: 0.8s;
        background: lightgreen;
    }

    @-webkit-keyframes wait {
        0% {
            left: -7px
        }

        30% {
            left: 52px
        }

        60% {
            left: 22px
        }

        100% {
            left: 100px
        }
    }

    @-moz-keyframes wait {
        0% {
            left: -7px
        }

        30% {
            left: 52px
        }

        60% {
            left: 22px
        }

        100% {
            left: 100px
        }
    }

    @-o-keyframes wait {
        0% {
            left: -7px
        }

        30% {
            left: 52px
        }

        60% {
            left: 22px
        }

        100% {
            left: 100px
        }
    }

    @keyframes wait {
        0% {
            left: -7px
        }

        30% {
            left: 52px
        }

        60% {
            left: 22px
        }

        100% {
            left: 100px
        }
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Dashboard</h1>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Pengguna</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('users')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-user-cog"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Jenis Pengguna</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('roles')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-key"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Hak Akses</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('permissions')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-archive"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Arsip</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('arsip_kolektor')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-archive"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Arsip Digital</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('arsip_digital')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-building"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Instansi</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('instansi')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-building"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Unit Kerja</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('work_units')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-chart-bar"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Data Kinerja</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('kinerja')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-chart-pie"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Infografis</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('infografis')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-newspaper"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Berita</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('news')->count() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Statistik Pengguna</h4>
                    <div class="card-header-action">
                        <div class="dropdown d-none" id="btnfilter">
                            <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown">Filter</a>
                            <div class="dropdown-menu dropdown-menu-right" id="dropdown-filter">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="loading">
                        <p>Sedang Memuat...</p>
                        <span><i></i><i></i></span>
                    </div>
                    <div id="chartuser">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Log Aktivitas Pengguna Terbaru</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled list-unstyled-border" id="listactivity">
                        @foreach (Spatie\Activitylog\Models\Activity::orderBy('created_at', 'DESC')->take(7)->get() as $activitys)
                        <li class="media">
                            <img class="mr-3 rounded-circle" width="50" src="{{ asset('admin/stisla/assets/img/avatar/avatar-2.png') }}" alt="avatar">
                            <div class="media-body">
                                <div class="float-right text-primary">{{ \Carbon\Carbon::parse($activitys->created_at)->diffForHumans() }}</div>
                                <div class="media-title">{{ $activitys->log_name }}</div>
                                <span class="text-small text-muted">{{ $activitys->description }}</span>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <div class="text-center pt-1 pb-1">
                        <a href="{{ route('activities.index') }}" class="btn btn-primary btn-lg btn-round">
                            Lihat Semua
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Total Jumlah Arsip Di Setiap Instansi</h4>
                </div>
                <div class="card-body">
                    <div class="summary">
                        <div class="summary-item">
                            <ul class="list-unstyled list-unstyled-border" id="list-jumlah-arsip-instansi">
                                @foreach ($instansi as $i)
                                <li class="media">
                                    <a href="#">
                                        <img class="mr-3 rounded" width="50" src="{{ asset('admin/stisla/assets/img/bg-unit-kerja.png') }}" alt="product">
                                    </a>
                                    <div class="media-body">
                                        <div class="media-right badge badge-light">
                                            {{ $i->arsipkolektor->count() }}
                                        </div>
                                        <div class="media-title">
                                            {{ $i->nama_instansi }}
                                        </div>
                                        <div class="text-muted text-small">Lembaga Daerah</div>
                                    </div>
                                </li>                                
                                @endforeach
                            </ul>
                            {{ $instansi->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection

@section('js')

<script src='https://api.mapbox.com/mapbox-gl-js/v2.9.1/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v2.9.1/mapbox-gl.css' rel='stylesheet' />
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v5.0.0/mapbox-gl-geocoder.min.js"></script>
<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v5.0.0/mapbox-gl-geocoder.css" type="text/css">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="https://cdn.ckeditor.com/4.19.1/standard/ckeditor.js"></script>

<script>
    $(document).ready(function () {
        var datauser = <?php echo $grouped;?>;
        var categories = <?php echo json_encode($categories); ?>;
        setTimeout(function () {
            $('.loading').hide();
            var chart = Highcharts.chart('chartuser', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: categories,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Jumlah Pengguna'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:1f} User</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                }
            });
            $.each(datauser, function (i, item) {
                chart.addSeries({
                    name: i,
                    data: item
                });
            });

            $('#btnfilter').removeClass('d-none');

            $('#dropdown-filter').append(`
                <div class="dropdown-item">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input mychart" type="radio" name="mychart" id="column" value="column" onclick="chartfunc()" checked>
                        <label class="form-check-label" for="column">Column</label>
                    </div>
                </div>
                <div class="dropdown-item">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input mychart" type="radio" name="mychart" id="bar" value="bar" onclick="chartfunc()">
                        <label class="form-check-label" for="bar">Bar</label>
                    </div>
                </div>
                <div class="dropdown-item">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input mychart" type="radio" name="mychart" id="areaspline" value="areaspline" onclick="chartfunc()">
                        <label class="form-check-label" for="areaspline">Areaspline</label>
                    </div>  
                </div>
            `);
            chartfunc = function () {
                var chartType = $('input[name=mychart]:checked').val();
                if (chartType == 'column') {
                    chart.update({
                        chart: {
                            type: 'column'
                        }
                    });
                } else if (chartType == 'bar') {
                    chart.update({
                        chart: {
                            type: 'bar'
                        }
                    });
                } else if (chartType == 'areaspline') {
                    chart.update({
                        chart: {
                            type: 'areaspline'
                        },
                        plotOptions: {
                            areaspline: {
                                fillOpacity: 0.5
                            }
                        },
                    });
                }
            }
        }, 3000);
    });
</script>
@endsection