@extends('layouts.dashboard')

@section('title')
Sistem Informasi Eksekutif
@endsection

@section('css')
<style>
    .table-responsive {
        max-height: 400px !important;
    }

    .r-15 {
        border-radius: 15px;
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Sistem Informasi Eksekutif</h1>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-archive"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Data Kinerja</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('kinerja')->where('status'  , '4')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-chart-pie"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Arsip</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('arsip_kolektor')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-chart-bar"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Arsip Digital</h4>
                    </div>
                    <div class="card-body">
                        {{ \DB::table('arsip_digital')->count() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @php
        $capaians = \DB::table('capaian_spbe')->orderBy('id', 'desc')->first();
        @endphp

        @if($capaians != null)
        <div class="col-lg-4 col-md-12 col-sm-6 col-12">
            <div class="card card-statistic-1 bg-primary r-15">
                <div class="card-wrap">
                    <div class="card-header">
                        <h4 class="text-white title-card">Indeks SPBE</h4>
                    </div>
                    <div class="card-body text-white">
                        {{ $capaians->indeks }}
                        <span class="badge badge-pill">{{ $capaians->predikat }}</span>

                        @php
                        $persentase = $capaians->indeks * 100 / 5.0;
                        $predikat = $capaians->predikat;

                        if($predikat == 'Memuaskan'){
                        $color = 'success';
                        }elseif($predikat == 'Sangat Baik'){
                        $color = 'info';
                        }elseif($predikat == 'Baik'){
                        $color = 'primary';
                        }elseif($predikat == 'Cukup'){
                        $color = 'warning';
                        }else{
                        $color = 'danger';
                        }
                        @endphp
                        <div class="progress mb-3 mt-2" data-height="5" style="height: 5px;">
                            <div class="progress-bar bg-warning" role="progressbar" data-width="{{ $persentase }}%" aria-valuenow="{{ $persentase }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $persentase }}%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @php
        $thistahun = date('Y');
        $capaiankinerja = \DB::table('capaian_kinerja')->where('tahun' , $thistahun)->first();
        @endphp
        @if ($capaiankinerja != null)
        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <div class="card card-statistic-1 bg-warning r-15">
                <div class="card-wrap">
                    <div class="card-header">
                        <h4 class="text-white title-card">Capaian Kinerja</h4>
                    </div>
                    <div class="card-body text-white">
                        {{ $capaiankinerja->persentase }}<span class="badge badge-pill">{{ $capaiankinerja->realisasi }}</span>
                        <div class="progress mb-3 mt-2" data-height="5" style="height: 5px;">
                            <div class="progress-bar bg-primary  " role="progressbar" data-width="{{ $capaiankinerja->persentase }}" aria-valuenow="{{ $capaiankinerja->persentase }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $capaiankinerja->persentase }};"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @php
        $thistahun = date('Y');
        $realisasianggaran = \DB::table('realisasi_anggaran')->where('tahun' , $thistahun)->first();
        @endphp
        @if ($realisasianggaran != null)
        @php
        $persentaseanggaran = $realisasianggaran->realisasi * 100 / $realisasianggaran->alokasi;
        @endphp
        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <div class="card card-statistic-1 bg-info r-15">
                <div class="card-wrap">
                    <div class="card-header">
                        <h4 class="text-white title-card">Realisasi Anggaran</h4>
                    </div>
                    <div class="card-body text-white">
                        {{ number_format($persentaseanggaran, 2, '.', '.') }}%
                        @if ($realisasianggaran->realisasi >= $realisasianggaran->alokasi)
                        <i class="fas fa-arrow-up text-success"></i>
                        @else
                        <i class="fas fa-arrow-down text-danger"></i>
                        @endif
                        <span class="badge badge-pill">
                            Rp. {{ number_format($realisasianggaran->realisasi, 0, ',', '.') }}
                        </span>
                        <div class="progress mb-3 mt-2" data-height="5" style="height: 5px;">
                            <div class="progress-bar bg-danger" role="progressbar" data-width="{{ number_format($persentaseanggaran, 2, '.', '.') }}%" aria-valuenow="{{ number_format($persentaseanggaran, 2, '.', '.') }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ number_format($persentaseanggaran, 2, '.', '.') }}%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        {{-- <div class="col-lg-4 col-md-6 col-12">
      <div class="card">
        <div class="card-header">
          <h4>Lembaga Kearsipan</h4>
        </div>
        <div class="card-body">
          <div class="summary">
            <div class="summary-item">
              <ul class="list-unstyled list-unstyled-border">
                <li class="media">
                  <a href="#">
                    <img class="mr-3 rounded" width="50" src="{{ asset('admin/stisla/assets/img/Dki-Jakarta.png') }}" alt="product">
        </a>
        <div class="media-body">
            <div class="media-right badge badge-light">4,3k</div>
            <div class="media-title"><a href="#">Provinsi DKI Jakarta</a></div>
            <div class="text-muted text-small">Lembaga Daerah</div>
        </div>
        </li>
        <li class="media">
            <a href="#">
                <img class="mr-3 rounded" width="50" src="{{ asset('admin/stisla/assets/img/Jawa-Barat.png') }}" alt="product">
            </a>
            <div class="media-body">
                <div class="media-right badge badge-light">3,2k</div>
                <div class="media-title"><a href="#">Provinsi Jawa Barat</a></div>
                <div class="text-muted text-small">Lembaga Daerah
                </div>
            </div>
        </li>
        <li class="media">
            <a href="#">
                <img class="mr-3 rounded" width="50" src="{{ asset('admin/stisla/assets/img/Jawa-Timur.png') }}" alt="product">
            </a>
            <div class="media-body">
                <div class="media-right badge badge-light">4,6k</div>
                <div class="media-title"><a href="#">Provinsi Jawa Timur</a></div>
                <div class="text-muted text-small">Lembaga Daerah
                </div>
            </div>
        </li>
        </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="col-lg-8 col-md-6 col-12 col-sm-12">
        <div class="card">
            <div class="card-body pt-2 pb-2">
                <div id="container">Please wait</div>
            </div>
        </div>
    </div> --}}
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
            <div class="card">
              <div class="card-header">
                <h4>Data Real Unit Kerja</h4>
              </div>
                <div class="card-body pt-3 pb-3">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Pilih Kategori</label>
                                <select class="selectpicker2 @error('category_id') is-invalid @enderror" data-show-subtext="true" data-live-search="true" id="category" name="category_id">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($category as $item)
                                    @if ($item->kinerja->count() > 0)
                                    @foreach ($item->kinerja as $kinerja)
                                    @if ($kinerja->status == 4)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @break;
                                    @endif
                                    @endforeach
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tahun-awal">Pilih Tahun Awal</label>
                                <select name="tahun" id="tahun-awal" class="form-control">
                                    <option value="">Pilih Tahun Awal</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tahun-akhir">Pilih Tahun Akhir</label>
                                <select name="tahun" id="tahun-akhir" class="form-control">
                                    <option value="">Pilih Tahun Akhir</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="chart">

                    </div>

                    <div id="table" class="table-responsive mt-3">

                    </div>

                    <div class="table-responsive" id="table-prediksi">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <h4>
                Data Prediksi Dengan Metode Forecasting Regresi Linear
              </h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Pilih Sub Kategori</label>
                        <select class="selectsubcategory form-control @error('category_id') is-invalid @enderror" data-show-subtext="true" data-live-search="true" id="subcategory" name="category_id">
                            <option value="">Pilih Sub Kategori</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="form">
                        <label for="">Pilih Tahun Prediksi</label>
                        <select name="tahun" id="tahun-prediksi" class="form-control">
                            <option value="">Pilih Tahun</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="mt-3 justify-content-center">
                <center>
                    <img src="{{ asset('/rumus-regresi-linier (1).PNG') }}" alt="Rumus Regresi Linear" class="img-fluid">
                </center>
            </div>
            <div class="mt-3" id="chart-prediksi">

            </div>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="https://cdn.ckeditor.com/4.19.1/standard/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        $('.selectpicker2').selectpicker();
        $('#tahun-prediksi').prop('disabled', true);
        $('#tahun-awal').prop('disabled', true);
        $('#tahun-akhir').prop('disabled', true);
        $('#subcategory').prop('disabled', true);
        $('#category').on('change', function () {
            var category_id = $(this).val();
            var lastyear = "{{ $year->last() }}";
            $('#table-prediksi').empty();
            $('#data-prediksi').empty();
            $('#tahun-prediksi').val('');
            $('#subcategory').val('');
            $('#data-prediksi').empty();
            $('#table').empty();
            $('#chart').empty();
            $('#chart-prediksi').empty();
            if (category_id) {
                $.ajax({
                    url: "{{ route('get-year') }}",
                    type: "GET",
                    dataType: "json",
                    data: {
                        category_id: category_id
                    },
                    success: function (data) {
                        $('#tahun-awal').empty();
                        $('#tahun-akhir').empty();
                        $('#tahun-awal').append('<option value="">Pilih Tahun Awal</option>');
                        $('#tahun-akhir').append('<option value="">Pilih Tahun Akhir</option>');
                        $.each(data, function (key, value) {
                            $('#tahun-awal').append('<option value="' + value + '">' + value + '</option>');
                            $('#tahun-akhir').append('<option value="' + value + '">' + value + '</option>');
                        });
                        $('#tahun-awal').prop('disabled', false);
                        $('#tahun-akhir').prop('disabled', false);
                    }
                });
            } else {
                $('#form-group-tahun').hide();
                $('#chart').html('');
                $('#table').html('');
                $('#subcategory').html('');
                alert('Silahkan Pilih Kategori');
            }
        });

        $('#tahun-awal').on('change', function () {
            $('#tahun-akhir').val('');
            $('#table').empty();
            $('#chart').empty();
            $('#chart-prediksi').empty();
            $('#tahun-prediksi').val('');
            $('#subcategory').val('');
        });
        $('#tahun-akhir').on('change', function () {
            var category_id = $('#category').val();
            var lastyear = $(this).val();
            var selisih = $('#tahun-akhir').val() - $('#tahun-awal').val();
            $('#table-prediksi').empty();
            $('#table').empty();
            $('#data-prediksi').empty();
            $('#chart-prediksi').empty();
            $('#tahun-prediksi').val('');
            $('#tahun-prediksi').empty();
            $('#subcategory').val('');
            $('#subcategory').empty();
            $('#chart').empty();
            $('#data-prediksi').empty();
            if ($('#tahun-awal').val() > $('#tahun-akhir').val()) {
                swal({
                    title: "Peringatan",
                    text: "Tahun Awal Tidak Boleh Lebih Besar Dari Tahun Akhir",
                    icon: "warning",
                    button: "OK",
                });
                $('#tahun-akhir').val('');
            } else if (selisih >= 10) {
                swal({
                    title: "Peringatan",
                    text: "Selisih Tahun Tidak Boleh Lebih Dari 10 Tahun",
                    icon: "warning",
                    button: "OK",
                });
                $('#tahun-akhir').val('');
            } else {
                $.ajax({
                    url: "{{ route('chart-kinerja-eksekutif') }}",
                    method: "GET",
                    data: {
                        category_id: category_id,
                        tahun_awal: $('#tahun-awal').val(),
                        tahun_akhir: $('#tahun-akhir').val(),
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        var chart = Highcharts.chart('chart', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Grafik ' + data.category
                            },
                            subtitle: {
                                text: 'Source:' + data.sumber
                            },
                            xAxis: {
                                categories: data.categories,
                                crosshair: true
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Jumlah Kinerja'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
                                    '<td style="padding:0"><b> {point.y:1f} </b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                        });
                        chart.showLoading();
                        
                        chart.hideLoading();
                        $.each(data.series, function (key, value) {
                            chart.addSeries({
                                name: key,
                                data: value
                            });
                        });
                        $('#chart').append(`
                        <center>
                          <div class="form-check form-check-inline">
                          <input class="form-check-input mychart" type="radio" name="mychart" id="column" value="column" onclick="chartfunc()" checked>
                          <label class="form-check-label" for="column">Column</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input mychart" type="radio" name="mychart" id="bar" value="bar" onclick="chartfunc()">
                          <label class="form-check-label" for="bar">Bar</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input mychart" type="radio" name="mychart" id="areaspline" value="areaspline" onclick="chartfunc()">
                          <label class="form-check-label" for="areaspline">Areaspline</label>
                        </div>  
                        </center>
                        `);
                        chartfunc = function () {
                            var chartType = $('input[name=mychart]:checked').val();
                            if (chartType == 'column') {
                                chart.update({
                                    chart: {
                                        type: 'column'
                                    }
                                });
                            } else if (chartType == 'bar') {
                                chart.update({
                                    chart: {
                                        type: 'bar'
                                    }
                                });
                            } else if (chartType == 'areaspline') {
                                chart.update({
                                    chart: {
                                        type: 'areaspline'
                                    },
                                    plotOptions: {
                                        areaspline: {
                                            fillOpacity: 0.5
                                        }
                                    },
                                });
                            }
                        }
                        $('#table').html(data.table);
                        $("[data-toggle=popover]").popover();
                        $('#tahun-prediksi').prop('disabled', false);
                        // $('#tahun-prediksi').removeClass('form-control');

                        $date = new Date();
                        $year = $date.getFullYear();

                        var start_year = "{{ \Carbon\Carbon::now()->format('Y') }}";

                        $('#tahun-prediksi').append('<option value="">Pilih Tahun Prediksi</option>');

                        for (var j = 1; j <= 10; j++) {
                            var tahun = parseInt(lastyear) + j;
                            $('#tahun-prediksi').append('<option value="' + j + '">' + tahun + ' (' + j + ' Tahun Kedepan ) </option>');
                        }

                        $('#subcategory').removeClass('form-control');
                        $('#subcategory').prop('disabled', false);
                        $('#subcategory').append('<option value="">Pilih Sub Kategori</option>');

                        $.each(data.groupcategoryid, function (key, value) {
                            $('#subcategory').append(
                                '<option value="' + value[0].id + '">' + value[0].name + '</option>'
                            );
                        });

                        $('.selectsubcategory').selectpicker();
                        $('.selectpickeryear').selectpicker();
                    }
                });
            }
        });

        $('#subcategory').on('change', function () {
            $('#table-prediksi').empty();
            $('#data-prediksi').empty();
            $('#data-prediksi').empty();
            $('#tahun-prediksi').val('');
            $('#chart-prediksi').empty();
        });

        $('#tahun-prediksi').on('change', function () {
            var subcategory_id = $('#subcategory').val();
            var category_id = $('#category').val();
            var tahunawal = $('#tahun-awal').val();
            var tahunakhir = $('#tahun-akhir').val();
            console.log(subcategory_id);
            console.log(this.value);
            var tahun = $(this).val();
            $('#table-prediksi').empty();
            $('#data-prediksi').empty();
            $('#data-prediksi').empty();
            if (tahun != '') {
                $.ajax({
                    url: "{{ route('prediksi-kinerja.get') }}",
                    type: "GET",
                    dataType: "json",
                    data: {
                        subcategory_id: subcategory_id,
                        category_id: category_id,
                        tahun: tahun,
                        tahunawal: tahunawal,
                        tahunakhir: tahunakhir
                    },
                    success: function (dataprediksi) {
                        var chart = Highcharts.chart('chart-prediksi', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Grafik Prediksi ' + dataprediksi.categories,
                            },
                            subtitle: {
                                text: 'Dengan Rumus Dasar B0 + B1 x: ' + dataprediksi.b0 + ' + ' + dataprediksi.b1 + ' x'
                            },
                            xAxis: {
                                categories: dataprediksi.categories,
                                crosshair: true
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Jumlah Kinerja'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
                                    '<td style="padding:0"><b> {point.y:1f} </b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                        });

                        $.each(dataprediksi.prediksi, function (key, value) {
                            chart.addSeries({
                                name: value.name,
                                data: value.data
                            });
                        });
                    }
                });
            } else {
                $('#form-group-tahun').hide();
                $('#chart').html('');
                $('#table').html('');
                $('#subcategory').html('');
                alert('Silahkan Pilih Kategori');
            }
        });
    })
</script>

@endsection