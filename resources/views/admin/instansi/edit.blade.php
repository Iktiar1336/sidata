@extends('layouts.dashboard')

@section('title')
    Update Instansi
@endsection

@section('css')
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Data instansi</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Update Data instansi</a></div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Update Data Instansi</h2>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Update Data Instansi</h4>
                            <div class="card-header-action">
                                <a href="{{ route('instansi.index') }}" class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <form id="form-edit-instansi" action="{{ route('instansi.update', Crypt::encrypt($instansi->id)) }}" method="post">
                                @csrf
                                @method('PUT')

                                <div class="form-group">
                                    <label for="code">Kode</label>
                                    <input type="text" name="code" id="code" value="{{ $instansi->code }}" class="form-control @error('code') is-invalid @enderror" placeholder="Kode" readonly>
                                    @error('code')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                @if ($instansi->parent_id != null)
                                    <div class="form-group" id="form-sub-instansi">
                                        <label for="instansi_id">Nama instansi</label>
                                        <input type="hidden" name="instansi_id" id="instansiid" value="{{ $instansi->parent_id }}">
                                        <select name="instansi_id" id="instansi_id" disabled class="form-control @error('instansi_id') is-invalid @enderror">
                                            <option value="">Pilih Instansi</option>
                                            @foreach ($datainstansi as $i)
                                                <option value="{{ $i->id }}" {{ $instansi->parent_id == $i->id ? 'selected' : '' }}>{{ $i->nama_instansi }}</option>
                                            @endforeach
                                        </select> 
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label for="nama-instansi" id="nama-label">Nama Instansi /
                                        Pemerintah</label>
                                    <input type="text" name="nama" id="nama-instansi" value="{{ $instansi->nama_instansi }}" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Instansi / Pemerintah">
                                    @error('nama')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="nomenklatur_unit">Nomenklatur Unit / Lembaga Kearsipan</label>
                                    <input type="text" name="nomenklatur_unit" value="{{ $instansi->nomenklatur_unit }}" id="nomenklatur_unit" class="form-control @error('nomenklatur_unit') is-invalid @enderror" placeholder="Nomenklatur Unit / Lembaga Kearsipan">
                                    @error('nomenklatur_unit')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea name="alamat" id="alamat" cols="30" class="form-control @error('alamat') is-invalid @enderror" rows="10" placeholder="Alamat Kantor">{{ $instansi->alamat }}</textarea>
                                    @error('alamat')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="telp">Telp / Fax</label>
                                    <input type="number" value="{{ $instansi->telp }}" name="telp" id="telp" class="form-control @error('telp') is-invalid @enderror" placeholder="Telp / Fax">
                                    @error('telp')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" value="{{ $instansi->email }}" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                                    @error('email')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block" id="btn-simpan">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')

@if (session('insert'))
    <script>
        swal("Instansi Berhasil Ditambahkan", {
            icon: 'success',
            title: 'Berhasil',
        })
    </script>
@endif

@if (session('update'))
    <script>
        swal("Instansi Berhasil Diupdate", {
            icon: 'success',
            title: 'Berhasil',
        })
    </script>
@endif

@if (session('delete'))
    <script>
        swal("Instansi Berhasil Dihapus", {
            icon: 'success',
            title: 'Berhasil',
        })
    </script>
@endif

@if (session('error'))
    <script>
        swal("Instansi Tidak Dapat Dihapus, Karena Sudah Terkait Dengan Data Lain", {
            icon: 'error',
            title: 'Gagal',
        })
    </script>
@endif


<script>
    $(document).ready(function () {

        $('.selectpicker').selectpicker();

        $('#pilihinstansi').on('change', function () {
            var id = $(this).val();
            if(id == ''){
                swal("Pilih Institusi Terlebih Dahulu", {
                    icon: 'error',
                    title: 'Gagal',
                })
            }

        });
    });
</script>

@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <script>
            swal("{{ $error }}", {
                title: "Failed",
                icon: "error",
            });
        </script>
    @endforeach
@endif
@endsection