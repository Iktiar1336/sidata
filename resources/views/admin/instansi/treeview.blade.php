<ul>
    @foreach ($subinstansi as $item)
        <li id="list-group-sub-item" data-id="{{ $item->id }}" data-name="{{ $item->nama_instansi }}" data-nomenlatur="{{ $item->nomenklatur_unit }}" data-alamat="{{ $item->alamat }}" data-telp="{{ $item->telp }}" data-email="{{ $item->email }}">
            {{ $item->nama_instansi }}
            @if (count($item->subinstansi) > 0)
                @include('admin.instansi.treeview', ['subinstansi' => $item->subinstansi])
            @endif
        </li>
    @endforeach
</ul>
