<form action="{{ route('instansi.destroy', Crypt::encrypt($row->id)) }}" method="POST">
    @csrf
    @method('DELETE')
    @can('Edit Instansi')
        <a class="btn btn-warning btn-sm" href="{{ route('instansi.edit', Crypt::encrypt($row->id)) }}"><i class="fas fa-edit"></i></a>
    @endcan
    @can('Hapus Instansi')
        <button type="submit" class="btn btn-danger btn-delete btn-sm"><i class="fas fa-trash"></i></button>
    @endcan
</form>

<script>
    $(document).ready(function() {
        $('.btn-delete').click(function(e) {
            e.preventDefault();
            var form = $(this).closest("form");
            swal({
                title: "Apakah Anda Yakin?",
                text: "Instansi ini akan dihapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    form.submit();
                } else {
                    swal("Instansi tidak jadi dihapus!");
                }
            });
        });
    });
</script>