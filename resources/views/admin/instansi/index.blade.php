@extends('layouts.dashboard')

@section('title')
List Instansi
@endsection

@section('css')
<style>
    .tree,
    .tree ul {
        margin: 0;
        padding: 0;
        list-style: none
    }

    .tree ul {
        margin-left: 1em;
        position: relative
    }

    .tree ul ul {
        margin-left: .7em
    }

    .tree ul:before {
        content: "";
        display: block;
        width: 0;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        border-left: 1px solid
    }

    .tree li {
        margin: 0;
        padding: 0 1em;
        line-height: 4em;
        font-weight: 700;
        position: relative;
        color: #6777ef;
    }

    .tree ul li:before {
        content: "";
        display: block;
        width: 10px;
        height: 0;
        border-top: 1px solid;
        margin-top: -1px;
        position: absolute;
        top: 1.9em;
        left: 0
    }

    .tree ul li:last-child:before {
        background: #fff;
        height: auto;
        top: 1.9em;
        bottom: 0
    }

    .indicator {
        margin-right: 5px;
        font-size: 17px
    }

    .tree li a {
        text-decoration: none;
        color: #369;
    }

    .tree li button,
    .tree li button:active,
    .tree li button:focus {
        text-decoration: none;
        color: #369;
        border: none;
        background: transparent;
        margin: 0px 0px 0px 0px;
        padding: 0px 0px 0px 0px;
        outline: 0;
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Data instansi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Data instansi</a></div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">List Data Instansi</h2>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        @can('Tambah Instansi')
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Tambah Instansi
                            </button>
                        @endcan
                        @can('Tambah Sub Instansi')
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#subinstansi">
                                Tambah Sub Instansi
                            </button>
                        @endcan
                    </div>
                    <div class="card-body">
                        <ul id="tree1" class="list-group">
                            @foreach ($instansi->whereNull('parent_id') as $i)
                            <li class="list-group-item" id="list-group-item" data-id="{{ $i->id }}" data-name="{{ $i->nama_instansi }}" data-nomenlatur="{{ $i->nomenklatur_unit }}" data-alamat="{{ $i->alamat }}" data-telp="{{ $i->telp }}" data-email="{{ $i->email }}">
                                {{ $i->nama_instansi }}
                                @if (count($i->subinstansi) > 0)
                                @include('admin.instansi.treeview', ['subinstansi' =>
                                $i->subinstansi])
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Data Instansi</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table datatable" id="tableinstansi">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        {{-- <th>Instansi</th> --}}
                                        <th>Nama Instansi</th>
                                        <th>Nomenklatur Unit</th>
                                        <th>Alamat</th>
                                        <th>Telp</th>
                                        <th>Email</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="subinstansi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Sub Instansi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('sub-instansi.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="code">Kode</label><span class="text-danger">*</span>
                        <input type="text" name="code" class="form-control @error('code') is-invalid @enderror" placeholder="Kode" required="" value="{{ $kode }}" readonly>
                        @error('code')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pilihinstansi">Pilih instansi</label><span class="text-danger">*</span>
                        <select name="instansi_id" id="pilihinstansi" class="selectpicker @error('instansi_id') is-invalid @enderror" data-show-subtext="true" data-live-search="true">
                            <option value="">Pilih Instansi</option>
                            @foreach ($instansi as $i)
                                <option value="{{ $i->id }}">{{ $i->nama_instansi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nama-instansi" id="nama-label">Nama Sub Instansi / Pemerintah</label><span class="text-danger">*</span>
                        <input type="text" name="nama" id="nama-instansi" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Instansi / Pemerintah" required>
                        @error('nama')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nomenklatur_unit">Nomenklatur Unit / Lembaga Kearsipan</label><span class="text-danger">*</span>
                        <input type="text" name="nomenklatur_unit" id="nomenklatur_unit" class="form-control @error('nomenklatur_unit') is-invalid @enderror" placeholder="Nomenklatur Unit / Lembaga Kearsipan" required>
                        @error('nomenklatur_unit')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label><span class="text-danger">*</span>
                        <textarea name="alamat" cols="30" class="form-control @error('alamat') is-invalid @enderror" rows="10" placeholder="Alamat Kantor" required></textarea>
                        @error('alamat')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="telp">Telp / Fax</label>
                        <input type="number" name="telp" class="form-control @error('telp') is-invalid @enderror" placeholder="Telp / Fax">
                        @error('telp')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                        @error('email')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-tambah">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Instansi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('instansi.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="code">Kode</label>
                        <input type="text" name="code" class="form-control @error('code') is-invalid @enderror" placeholder="Kode" required="" value="{{ $kode }}" readonly>
                        @error('code')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nama-instansi" id="nama-label">Nama Instansi / Pemerintah</label><span class="text-danger">*</span>
                        <input type="text" name="nama" id="nama-instansi" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Instansi / Pemerintah" required>
                        @error('nama')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nomenklatur_unit">Nomenklatur Unit / Lembaga Kearsipan</label><span class="text-danger">*</span>
                        <input type="text" name="nomenklatur_unit" id="nomenklatur_unit" class="form-control @error('nomenklatur_unit') is-invalid @enderror" placeholder="Nomenklatur Unit / Lembaga Kearsipan" required>
                        @error('nomenklatur_unit')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label><span class="text-danger">*</span>
                        <textarea name="alamat" cols="30" class="form-control @error('alamat') is-invalid @enderror" rows="10" placeholder="Alamat Kantor" required></textarea>
                        @error('alamat')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="telp">Telp / Fax</label>
                        <input type="number" name="telp" class="form-control @error('telp') is-invalid @enderror" placeholder="Telp / Fax">
                        @error('telp')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                        @error('email')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-tambah">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')

@if (session('insert'))
<script>
    swal("Instansi berhasil di tambahkan", {
        icon: 'success',
        title: 'Berhasil',
    })
</script>
@endif

@if (session('update'))
<script>
    swal("Instansi berhasil di update", {
        icon: 'success',
        title: 'Berhasil',
    })
</script>
@endif

@if (session('delete'))
<script>
    swal("Instansi berhasil di hapus", {
        icon: 'success',
        title: 'Berhasil',
    })
</script>
@endif

@if (session('error'))
<script>
    swal("Instansi tidak dapat dihapus, karena sudah terkait dengan data lain", {
        icon: 'error',
        title: 'Gagal',
    })
</script>
@endif


<script>
    $(document).ready(function () {

        $('.selectpicker').selectpicker();

        $('#tableinstansi').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/master-instansi/instansi",
            dataType: "json",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                // {data: 'nama_instansi', name: 'nama_instansi'},
                {
                    data: 'instansi',
                    name: 'instansi'
                },
                {
                    data: 'nomenklatur_unit',
                    name: 'nomenklatur_unit'
                },
                {
                    data: 'alamat',
                    name: 'alamat'
                },
                {
                    data: 'telp',
                    name: 'telp'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });

        $('#pilihinstansi').on('change', function () {
            var id = $(this).val();
            if (id == '') {
                swal("Pilih Institusi Terlebih Dahulu", {
                    icon: 'error',
                    title: 'Gagal',
                })
            }

        });
    });
</script>
<script>
    $.fn.extend({
        treed: function (o) {

            var openedClass = 'fa-folder-open';
            var closedClass = 'fa-folder';

            if (typeof o != 'undefined') {
                if (typeof o.openedClass != 'undefined') {
                    openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined') {
                    closedClass = o.closedClass;
                }
            };

            //initialize each of the top levels
            var tree = $(this);
            tree.addClass("tree");
            tree.find('li').has("ul").each(function () {
                var branch = $(this); //li with children ul
                branch.prepend("<i class='indicator far " + closedClass +
                    "'></i>");
                branch.addClass('branch');
                branch.on('click', function (e) {
                    if (this == e.target) {
                        var icon = $(this).children('i:first');
                        icon.toggleClass(openedClass + " " +
                            closedClass);
                        $(this).children().children().toggle();
                    }
                })
                branch.children().children().toggle();
            });
            //fire event from the dynamically added icon
            tree.find('.branch .indicator').each(function () {
                $(this).on('click', function () {
                    $(this).closest('li').click();
                });
            });
            //fire event to open branch if the li contains an anchor instead of text
            tree.find('.branch>a').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
            //fire event to open branch if the li contains a button instead of text
            tree.find('.branch>button').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });
    $('#tree1').treed({
        openedClass: 'fa-folder',
        closedClass: 'fa-folder-open'
    });
</script>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Failed",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection