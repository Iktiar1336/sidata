@extends('layouts.dashboard')

@section('title')
    Update Tugas & Fungsi
@endsection

@section('css')
    
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Update Tugas & Fungsi</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
      <div class="breadcrumb-item">Update Tugas & Fungsi</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Update Tugas dan Fungsi</h2>

    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4>Update Tugas dan Fungsi</h4>
          </div>
          <div class="card-body">
            <form action="{{ route('performance.tugas-fungsi.update', Crypt::encrypt($performance->id)) }}" method="POST">
              @csrf
              @method('PUT')
              <div class="form-group">
                  <label>Tugas :</label>
                  <textarea name="tugas" class="form-control @error('tugas') is-invalid @enderror" required id="tugas">
                    {!! $performance->tugas !!}
                  </textarea>
                  @error('tugas')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
              <div class="form-group">
                  <label>Fungsi :</label>
                  <textarea name="fungsi" class="form-control @error('fungsi') is-invalid @enderror" required id="fungsi">
                    {!! $performance->fungsi !!}
                  </textarea>
                  @error('fungsi')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-success btn-block">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js')
<script>
  var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
  };
  CKEDITOR.replace('tugas', options);
  CKEDITOR.replace('fungsi', options);
</script>
@endsection