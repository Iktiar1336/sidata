@extends('layouts.dashboard')

@section('title')
    Struktur Organisasi
@endsection

@section('css')
    
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Struktur Organisasi</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
      <div class="breadcrumb-item">Struktur Organisasi</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Struktur Organisasi</h2>
    <p class="section-lead">
    </p>

    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4>Struktur Organisasi</h4>
          </div>
          <div class="card-body">
            @forelse ($performance as $item)
              @if ($item->struktur_organisasi != null)
                  <img src="{{ asset('images/struktur-organisasi/'.$item->struktur_organisasi) }}" alt="" class="img-fluid">
                  <a href="{{ route('performance.tugas-fungsi.edit', Crypt::encrypt($item->id)) }}" class="btn btn-warning btn-block mt-3">Edit</a>
              @else
                <h5 class="text-danger">
                  Maaf Struktur Organisasi Saat Ini Belum Tersedia
                </h5>
                <p class="mt-2">
                  Silahkan Tambahkan Struktur Organisasi Terlebih Dahulu
                </p>
                <button class="btn btn-primary trigger--fire-modal-5 text-center" id="modal-8">Tambahkan</button>

                <form action="{{ route('performance.struktur-organisasi.store') }}" method="POST" class="modal-part" id="modal-struktur-organisasi-part" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                      <input type="hidden" name="id" value="{{ Crypt::encrypt($item->id) }}">
                      <label>Image :</label>
                      <input type="file" name="struktur_organisasi" class="form-control @error('struktur_organisasi') is-invalid @enderror" required>
                      @error('struktur_organisasi')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-success btn-block">Submit</button>
                  </div>
                </form>
              @endif
            @empty
              <h5 class="text-danger">
                Maaf Struktur Organisasi Saat Ini Belum Tersedia
              </h5>
              <p class="mt-2">
                Silahkan Tambahkan Visi dan Misi Terlebih Dahulu 
              </p>
              <a href="{{ route('performance.visi-misi') }}" class="btn btn-primary text-center">Tambahkan Visi & Misi</a>
            @endforelse
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js')
@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <script>
            swal("{{ $error }}", {
                title: "Failed",
                icon: "error",
            });
        </script>
    @endforeach
@endif
@endsection


