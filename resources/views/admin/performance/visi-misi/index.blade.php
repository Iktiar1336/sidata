@extends('layouts.dashboard')

@section('title')
    Visi & Misi
@endsection

@section('css')
    
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Visi & Misi</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">Data Kinerja</a></div>
      <div class="breadcrumb-item">Visi & Misi</div>
    </div>
  </div>

  <div class="section-body">
    <h2 class="section-title">Visi dan Misi</h2>
    <p class="section-lead">
    </p>

    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4>Visi dan Misi</h4>
          </div>
          <div class="card-body">
            @forelse ($performance as $item)
              @if ($item->visi != null && $item->misi != null)
                <div class="media">
                  <div class="media-body">
                    <h5 class="mt-0">Visi</h5>
                    {!! $item->visi !!}
                    <hr>
                    <h5 class="mt-0">Misi</h5>
                    {!! $item->misi !!}
                  </div>
                </div>
                <a href="{{ route('performance.visi-misi.edit', Crypt::encrypt($item->id)) }}" class="btn btn-warning btn-block">Edit</a>
                  
              @else
                <h5 class="text-danger">
                  Maaf Visi dan Misi saat ini belum tersedia
                </h5>
                <p class="mt-2">
                  Silahkan Tambahkan Visi dan Misi
                </p>
                <button class="btn btn-primary trigger--fire-modal-5 text-center" id="modal-7">Tambahkan</button>
              @endif
            @empty
              <h5 class="text-danger">
                Maaf Visi dan Misi saat ini belum tersedia
              </h5>
              <p class="mt-2">
                Silahkan Tambahkan Visi dan Misi
              </p>
              <button class="btn btn-primary trigger--fire-modal-5 text-center" id="modal-6">Tambahkan</button>
            @endforelse
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<form action="{{ route('performance.visi-misi.store') }}" method="POST" class="modal-part" id="modal-visi-misi-part">
  @csrf
  <div class="form-group">
      <label>Visi :</label>
      <textarea name="visi" class="form-control @error('visi') is-invalid @enderror" required id="visi"></textarea>
      @error('visi')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
  </div>
  <div class="form-group">
      <label>Misi :</label>
      <textarea name="misi" class="form-control @error('misi') is-invalid @enderror" required id="misi"></textarea>
      @error('misi')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
  </div>
  <div class="form-group">
      <button type="submit" class="btn btn-success btn-block">Submit</button>
  </div>
</form> 
@endsection

@section('js')
<script>
  var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
  };
  CKEDITOR.replace('visi', options);
  CKEDITOR.replace('misi', options);
</script>

@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <script>
            swal("{{ $error }}", {
                title: "Failed",
                icon: "error",
            });
        </script>
    @endforeach
@endif
@endsection

