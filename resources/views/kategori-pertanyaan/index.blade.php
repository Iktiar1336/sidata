@extends('layouts.dashboard')

@section('title')
Kategori Pertanyaan
@endsection

@section('css')

@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Kategori Pertanyaan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Master Data</div>
            <div class="breadcrumb-item">Kategori Pertanyaan</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Kategori Pertanyaan</h2>
        <p class="section-lead">Halaman untuk mengelola data Kategori Pertanyaan.</p>

        <div class="card">
            <div class="card-header">
                <h4>Daftar Kategori Pertanyaan</h4>
                <div class="card-header-action">
                    <button class="btn btn-primary trigger--fire-modal-5 text-center mr-3" id="modal-target-kinerja">Tambah Kategori Pertanyaan</button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-kategori-pertanyaan">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Survey</th>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="{{ route('kategori-pertanyaan.store') }}" method="POST" class="modal-part" id="modal-target-kinerja-part" tabindex="-1">
    @csrf
    <div class="form-group">
        <label for="survey">Pilih Survey</label>
        <select class="selecpicker" name="survey_id" id="survey">
            <option value="" disabled selected>Pilih Survey</option>
            @foreach ($surveyArsip as $item)
                <option value="{{ $item->id }}">{!! $item->nama !!} Tahun {{ $item->tahun }}</option>
            @endforeach
        </select>

    </div>
    <div class="form-group">
        <label for="judul">Kategori Pertanyaan</label>
        <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" min="0" placeholder="Kategori Pertanyaan">

        @error('judul')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="deskripsi">Deskripsi</label>
        <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" class="form-control @error('deskripsi') is-invalid @enderror"></textarea>

        @error('deskripsi')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block" id="btn-submit-indikator-kinerja">
            Simpan
        </button>
    </div>
</form>
@endsection

@section('js')

<script>
    $(document).ready(function() {
        $('.selecpicker').selectpicker();

        $('#table-kategori-pertanyaan').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/kategori-pertanyaan/",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'survey', name: 'survey'},
                {data: 'judul', name: 'judul'},
                {data: 'deskripsi', name: 'deskripsi'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>

@if (session('insert'))
<script>
    swal("Kategori Pertanyaan berhasil ditambahkan!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('update'))
<script>
    swal("Kategori Pertanyaan berhasil diubah!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif

@if (session('delete-failed'))
<script>
    swal("Kategori Pertanyaan gagal dihapus! Karena sudah terikat dengan data lain.", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('insert-failed'))
<script>
    swal("Kategori Pertanyaan sudah ada, silahkan pilih tahun yang lain!", {
        icon: "error",
        title: "Gagal",
    });
</script>
@endif

@if (session('delete-success'))
<script>
    swal("Kategori Pertanyaan berhasil dihapus!", {
        icon: "success",
        title: "Sukses",
    });
</script>
@endif


@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<script>
    swal("{{ $error }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endforeach
@endif
@endsection