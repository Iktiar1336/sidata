@extends('layouts.dashboard')

@section('title', 'Formatting & Pencarian Arsip')

@section('css')
    
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Formatting & Pencarian Arsip</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Formatting & Pencarian Arsip</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Formatting & Pencarian Arsip</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered" id="tablearsip" style="width: 150%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Instansi</th>
                                        <th>Uraian Item</th>
                                        <th>Nomor Surat</th>
                                        <th>Tanggal Surat</th>
                                        <th>Kode Klasifikasi</th>
                                        <th>
                                            Tingkat Perkembangan
                                        </th>
                                        <th>Media Arsip</th>
                                        <th>Kondisi</th>
                                        <th>Jumlah</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Instansi</th>
                                        <th>Uraian Item</th>
                                        <th>Nomor Surat</th>
                                        <th>Tanggal Surat</th>
                                        <th>Kode Klasifikasi</th>
                                        <th>
                                            Tingkat Perkembangan
                                        </th>
                                        <th>Media Arsip</th>
                                        <th>Kondisi</th>
                                        <th>Jumlah</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')

@if (session('error'))
    <script>
        swal("Data Arsip Sudah Ada", {
            title: "Failed",
            icon: "error",
        });
    </script>
@endif

<script>

    $(document).ready(function () {

        $('#tablearsip').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('metadata.index') }}",  
            dataType: "json",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'instansi', name: 'instansi'},
                {data: 'uraianitem', name: 'uraianitem'},
                {data: 'nomor_surat', name: 'nomor_surat'},
                {data: 'tanggal_surat', name: 'tanggal_surat'},
                {data: 'kode_klasifikasi', name: 'kode_klasifikasi'},
                {data: 'tingkat_perkembangan', name: 'tingkat_perkembangan'},
                {data: 'media_arsip', name: 'media_arsip'},
                {data: 'kondisi', name: 'kondisi'},
                {data: 'jumlah', name: 'jumlah'},
                {data: 'keterangan', name: 'keterangan'},
            ],

            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var select = $('<select class="selectpicker" data-show-subtext="true" data-live-search="true"><option value="">Pilih</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                        $('.selectpicker').selectpicker('refresh');
                    });
                });
            }
        });

        $('#instansi_id').on('change', function () {
            var instansi_id = $(this).val();
            if (instansi_id == '') {
                swal("Gagal", "Instansi tidak valid !", "error");
            }
        });

        $('.selectpicker').selectpicker();
    });
</script>

@if ($message = Session::get('success'))
<script>
    swal("{{ $message }}", {
        title: "Berhasil",
        icon: "success",
    });
</script>
@endif

@if ($message = Session::get('error'))
<script>
    swal("{{ $message }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endif
@endsection