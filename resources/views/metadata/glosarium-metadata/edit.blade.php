@extends('layouts.dashboard')

@section('title', 'Edit Glosarium Metadata')

@section('css')
    
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Edit Glosarium Metadata</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Edit Glosarium Metadata</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Glosarium Metadata</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('glosarium.index') }}" class="btn btn-primary">Kembali</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('glosarium.update', Crypt::encrypt($glometa->id)) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="metadata">Pilih Metadata</label>
                                <select class="form-control" id="metadata" name="metadata" required>
                                    <option value="" selected disabled>Pilih Metadata</option>
                                    <option value="Uraian Item" {{ $glometa->metadata == "Uraian Item" ? 'selected' : '' }}>Uraian Item</option>
                                    <option value="Nomor Surat" {{ $glometa->metadata == "Nomor Surat" ? 'selected' : '' }}>Nomor Surat</option>
                                    <option value="Tanggal Surat" {{ $glometa->metadata == "Tanggal Surat" ? 'selected' : '' }}>Tanggal Surat</option>
                                    <option value="Kode Klasifikasi" {{ $glometa->metadata == "Kode Klasifikasi" ? 'selected' : '' }}>Kode Klasifikasi</option>
                                    <option value="Tingkat Perkembangan" {{ $glometa->metadata == "Tingkat Perkembangan" ? 'selected' : '' }}>Tingkat Perkembangan</option>
                                    <option value="Media Arsip" {{ $glometa->metadata == "Media Arsip" ? 'selected' : '' }}>Media Arsip</option>
                                    <option value="Kondisi" {{ $glometa->metadata == "Kondisi" ? 'selected' : '' }}>Kondisi</option>
                                    <option value="Jumlah" {{ $glometa->metadata == "Jumlah" ? 'selected' : '' }}>Jumlah</option>
                                    <option value="Keterangan" {{ $glometa->metadata == "Keterangan" ? 'selected' : '' }}>Keterangan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="istilah">Istilah</label>
                                <input type="text" class="form-control" name="istilah" id="istilah" placeholder="Istilah" value="{{ $glometa->istilah }}" required>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" id="deskripsi" rows="3" required>{!! $glometa->definisi !!}</textarea>
                            </div>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            CKEDITOR.replace('deskripsi');
        });
    </script>
@endsection