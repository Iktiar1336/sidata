@extends('layouts.dashboard')

@section('title', 'Metadata Arsip')

@section('css')
<style>
    .accordion {
        display: block;
        width: 500px;
        height: auto;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
    }

    .accordion-collapse {
        border-width: 0;
        border: 0 solid #cccccc;
    }

    .accordion .collapsing {
        transition: height 0.4s ease;
    }

    .accordion-body {
        padding: 15px 10px;
        background: #fff;
    }

    /**
 * >> Accordion Item
 * ---------------------------------------------------------- */
    .accordion-item:last-of-type .accordion-button.collapsed {
        border-bottom-width: 1px;
    }

    .accordion-item:last-of-type .accordion-collapse {
        border-bottom-width: 1px;
    }

    /**
 * >> Accordion Header
 * ---------------------------------------------------------- */
    .accordion-header {
        margin-top: 0;
        margin-bottom: 0;
    }

    /** 
 * >> Button
 * ---------------------------------------------------------  */
    .accordion-button {
        display: flex;
        align-items: center;
        font-family: nexa xbold;
        font-size: 1.6rem;
        color: #58595b;
        width: 100%;
        padding: 10px 10px;
        background-color: #fff;
        border: 0;
        border-top: 1px solid #cccccc;
        border-bottom: 1px solid #cccccc;
        border-radius: 0;
        overflow-anchor: none;
        outline: none;
        position: relative;
        transition: all 0.4s ease;
    }

    .accordion-button.collapsed {
        border-bottom-width: 0;
    }

    .accordion-button.collapsed::after {
        background-image: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyNC4zLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAxOSAxOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTkgMTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsOiM1OTU5NUI7c3Ryb2tlOiM1ODU5NUI7c3Ryb2tlLW1pdGVybGltaXQ6MTA7fQ0KPC9zdHlsZT4NCjxsaW5lIGNsYXNzPSJzdDAiIHgxPSI5LjUiIHkxPSIwIiB4Mj0iOS41IiB5Mj0iMTkiLz4NCjxsaW5lIGNsYXNzPSJzdDAiIHgxPSIwIiB5MT0iOS41IiB4Mj0iMTkiIHkyPSI5LjUiLz4NCjwvc3ZnPg0K");
    }

    .accordion-button:not(.collapsed)::after {
        background-image: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyNC4zLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAxOSAxOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTkgMTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsOiM1OTU5NUI7c3Ryb2tlOiM1ODU5NUI7c3Ryb2tlLW1pdGVybGltaXQ6MTA7fQ0KPC9zdHlsZT4NCjxsaW5lIGNsYXNzPSJzdDAiIHgxPSIwIiB5MT0iOS41IiB4Mj0iMTkiIHkyPSI5LjUiLz4NCjwvc3ZnPg0K");
        transform: rotate(180deg);
    }

    .accordion-button::after {
        content: "";
        width: 19px;
        height: 19px;
        background-repeat: no-repeat;
        position: absolute;
        right: 10px;
        transition: all 0.4s ease;
    }
</style>
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Glosarium Metadata</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Glosarium Metadata</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Glosarium Metadata</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Tambah Glosarium
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered" id="tablearsip">
                                <thead>
                                    <tr>
                                        <th style="width:20px"></th>
                                        <th>Metadata</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Glosarium</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('glosarium.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="metadata">Pilih Metadata</label>
                        <select class="form-control" id="metadata" name="metadata" required>
                            <option value="" selected disabled>Pilih Metadata</option>
                            <option value="Uraian Item">Uraian Item</option>
                            <option value="Nomor Surat">Nomor Surat</option>
                            <option value="Tanggal Surat">Tanggal Surat</option>
                            <option value="Kode Klasifikasi">Kode Klasifikasi</option>
                            <option value="Tingkat Perkembangan">Tingkat Perkembangan</option>
                            <option value="Media Arsip">Media Arsip</option>
                            <option value="Kondisi">Kondisi</option>
                            <option value="Jumlah">Jumlah</option>
                            <option value="Keterangan">Keterangan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="istilah">Istilah</label>
                        <input type="text" class="form-control" name="istilah" id="istilah" placeholder="Istilah" required>
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" rows="3" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>

<script id="details-template" type="text/x-handlebars-template">
    <table class="table">
        <tr>
            <td>Istilah:</td>
            <td><b>@{{istilah}}</b></td>
        </tr>
        <tr>
            <td>Deskripsi:</td>
            <td>@{{definisi}}</td>
        </tr>
    </table>
</script>

<script>
    function format ( d ) {
        var html = '<table class="table">';
        html += `
            <thead>
                <tr>
                    <th>Istilah</th>
                    <th>Deskripsi</th>
                    <th>Action</th>
                </tr>
            </thead>
        `;
        html += '<tbody>';
        d.sort(function(a, b) {
            return a.istilah.localeCompare(b.istilah);
        });
        $.each(d, function (i, item) {
            console.log($.parseHTML(item.definisi));
            html += '<tr>' +
                '<td>' + item.istilah + '</td>' +
                '<td>' + $("<div/>").html(item.definisi).text() + '</td>' +
                '<td>' +
                    '<a href="/glosarium/' + item.id + '/edit" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>' +
                    '<form action="/glosarium/' + item.id + '" method="post" class="d-inline">' +
                        '@csrf' +
                        '@method("delete")' +
                        '<button type="submit" class="btn btn-danger ml-2 btn-sm"><i class="fas fa-trash"></i></button>' +
                    '</form>' +
                '</td>' +
            '</tr>';
        });
        html += '</tbody>';
        html += '</table>';

        return html;
    }
        
    $(document).ready(function () {

        CKEDITOR.replace('deskripsi');

        var template = Handlebars.compile($("#details-template").html());

        $('#tablearsip').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/glosarium",
            dataType: 'json',
            columns: [{
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: '<i class="fas fa-plus-square text-success" aria-hidden="true"></i>'
                },
                {
                    data: 'metadata',
                    name: 'metadata'
                },
            ],
        });

        $('#tablearsip tbody').on('click', 'tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var tdi = tr.find("i.fas");
            var row = $('#tablearsip').DataTable().row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
                tdi.first().removeClass('fa-minus-square');
                tdi.first().addClass('fa-plus-square');
            } else {
                row.child(format(row.data().children)).show();
                tr.addClass('shown');
                tdi.first().removeClass('fa-plus-square');
                tdi.first().addClass('fa-minus-square');
            }
        });
    });
</script>

@if ($message = Session::get('success'))
<script>
    swal("{{ $message }}", {
        title: "Berhasil",
        icon: "success",
    });
</script>
@endif

@if ($message = Session::get('error'))
<script>
    swal("{{ $message }}", {
        title: "Gagal",
        icon: "error",
    });
</script>
@endif
@endsection