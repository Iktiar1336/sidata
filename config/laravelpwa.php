<?php

return [
    'name' => 'LaravelPWA',
    'manifest' => [
        'name' => env('APP_NAME', 'My PWA App'),
        'short_name' => 'SIDATA',
        'start_url' => '/',
        'background_color' => '#099cf4',
        'theme_color' => '#ffffff',
        'display' => 'standalone',
        'orientation'=> 'any',
        'status_bar'=> 'black',
        'icons' => [
            '72x72' => [
                'path' => '/assets/img/faviconsidata.png',
                'purpose' => 'any'
            ],
            '96x96' => [
                'path' => '/assets/img/faviconsidata.png',
                'purpose' => 'any'
            ],
            '128x128' => [
                'path' => '/assets/img/faviconsidata.png',
                'purpose' => 'any'
            ],
            '144x144' => [
                'path' => '/assets/img/faviconsidata.png',
                'purpose' => 'any'
            ],
            '152x152' => [
                'path' => '/assets/img/faviconsidata.png',
                'purpose' => 'any'
            ],
            '192x192' => [
                'path' => '/assets/img/faviconsidata.png',
                'purpose' => 'any'
            ],
            '384x384' => [
                'path' => '/assets/img/faviconsidata.png',
                'purpose' => 'any'
            ],
            '512x512' => [
                'path' => '/assets/img/faviconsidata.png',
                'purpose' => 'any'
            ],
        ],
        'splash' => [
            '640x1136' => '/assets/img/faviconsidata.png',
            '750x1334' => '/assets/img/faviconsidata.png',
            '828x1792' => '/assets/img/faviconsidata.png',
            '1125x2436' => '/assets/img/faviconsidata.png',
            '1242x2208' => '/assets/img/faviconsidata.png',
            '1242x2688' => '/assets/img/faviconsidata.png',
            '1536x2048' => '/assets/img/faviconsidata.png',
            '1668x2224' => '/assets/img/faviconsidata.png',
            '1668x2388' => '/assets/img/faviconsidata.png',
            '2048x2732' => '/assets/img/faviconsidata.png',
        ],
        'shortcuts' => [
            [
                'name' => 'Shortcut Link 1',
                'description' => 'Shortcut Link 1 Description',
                'url' => '/shortcutlink1',
                'icons' => [
                    "src" => "/assets/img/faviconsidata.png",
                    "purpose" => "any"
                ]
            ],
            [
                'name' => 'Shortcut Link 2',
                'description' => 'Shortcut Link 2 Description',
                'url' => '/shortcutlink2'
            ]
        ],
        'custom' => []
    ]
];
