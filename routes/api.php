<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BotTelegramController;
use App\Http\Controllers\API\ArsipKolektor\ArsipKolektorController;
use App\Http\Controllers\API\Instansi\InstansiController;
use App\Http\Controllers\API\JenisArsip\JenisArsipController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('apihandler')->group(function () {

    // Instansi
    Route::resource('instansi', InstansiController::class)->only([
        'index'
    ]);

    // Jenis Arsip

    Route::resource('jenis-arsip', JenisArsipController::class)->only([
        'index'
    ]); 

    // Arsip Kolektor

    Route::resource('arsip-kolektor', ArsipKolektorController::class);

    Route::post('arsip-kolektor/upload-by-excel', [ArsipKolektorController::class, 'UploadByExcel']);
    
});

Route::get('/setWebhook', [BotTelegramController::class, 'setWebhook']);
Route::post('/sidataanri_bot/webhook', [BotTelegramController::class, 'commandHandler']);