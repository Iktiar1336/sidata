<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProdusenDataController;
use App\Http\Controllers\LandingPageController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\MasterDataKinerja\PerformanceController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\ActivityController;
use App\Http\Controllers\Admin\EmailSettingController;
use App\Http\Controllers\Admin\TelegramSettingController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\UnitKerja\CategoryController;
use App\Http\Controllers\UnitKerja\WorkUnitController;
use App\Http\Controllers\UnitKerja\KinerjaController;
use App\Http\Controllers\UnitKerja\KotakMasukController;
use App\Http\Controllers\UnitKerja\ChatController;
use App\Http\Controllers\Informasi\InfografisController;
use App\Http\Controllers\Informasi\NewsController;
use App\Http\Controllers\UnitKerja\ChartController;
use App\Http\Controllers\Instansi\InstansiController;
use App\Http\Controllers\Instansi\SubInstansiController;
use App\Http\Controllers\ArsipKolektor\ArsipKolektorController;
use App\Http\Controllers\ArsipKolektor\SurveyController;
use App\Http\Controllers\ArsipKolektor\JawabanSurveyController;
use App\Http\Controllers\ArsipKolektor\JenisArsipController;
use App\Http\Controllers\ArsipKolektor\InstrumenSurveyController;
use App\Http\Controllers\ArsipKolektor\ListArsipController;
use App\Http\Controllers\ArsipKolektor\ImportArsipController;
use App\Http\Controllers\ArsipIdentifikasi\ArsipIdentifikasiController;
use App\Http\Controllers\MasterDataKinerja\RealisasiAnggaranController;
use App\Http\Controllers\MasterDataKinerja\SasaranStrategisController;
use App\Http\Controllers\MasterDataKinerja\CapaianController;
use App\Http\Controllers\MasterDataKinerja\IndikatorKinerjaController;
use App\Http\Controllers\MasterDataKinerja\TargetKinerjaController;
use App\Http\Controllers\MasterDataKinerja\PeranStrategisController;
use App\Http\Controllers\MasterDataKinerja\TujuanController;
use App\Http\Controllers\MasterDataKinerja\RencanaStrategisController;
use App\Http\Controllers\MasterDataKinerja\PerjanjianKinerjaController;
use App\Http\Controllers\MasterDataKinerja\CapaianKinerjaController;
use App\Http\Controllers\MasterDataKinerja\VisiMisiController;
use App\Http\Controllers\MasterDataKinerja\TugasFungsiController;
use App\Http\Controllers\MasterDataKinerja\StrukturOrganisasiController;
use App\Http\Controllers\MasterDataKinerja\CapaianSPBEController;
use App\Http\Controllers\ModelKinerja\ModelKinerjaController;
use App\Http\Controllers\Metadata\MetaDataController;
use App\Http\Controllers\Metadata\GlosariumController;
use Illuminate\Support\Facades\Redirect;
use Laravel\Passport\ClientRepository;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Landing Page
Route::get('/', [LandingPageController::class, 'index'])->name('welcome');

// Protected XSS Route
Route::middleware(['XSS'])->group(function () {

    Route::get('/visi-misi', [LandingPageController::class, 'visimisi'])->name('visi-misi');
    
    Route::get('/tugas-fungsi', [LandingPageController::class, 'tugasfungsi'])->name('tugas-fungsi');
    
    Route::get('/struktur-organisasi', [LandingPageController::class, 'strukturorganisasi'])->name('struktur-organisasi');
    
    Route::get('/infografis', [LandingPageController::class, 'infografis'])->name('infografis');
    
    Route::get('/unit-kerja', [LandingPageController::class, 'unitkerja'])->name('unit-kerja');
    
    Route::get('/unit-kerja/detail/{id}', [LandingPageController::class, 'detailunitkerja'])->name('detail-unit-kerja');
    
    Route::get('/berita', [LandingPageController::class, 'news'])->name('news');
    
    Route::get('/berita/detail/{slug}', [LandingPageController::class, 'detailnews'])->name('detail-news');
    
    Route::get('/infografis/detail/{slug}', [LandingPageController::class, 'detailinfografis'])->name('detail-infografis');
    
    Route::post('getsubCategory', [LandingPageController::class, 'getsubCategory'])->name('getsubCategory');
    
    Route::post('getYear', [LandingPageController::class, 'getYear'])->name('getYear');
    
    Route::get('getKinerja', [LandingPageController::class, 'getKinerja'])->name('getKinerja');
    
    Route::post('search', [SearchController::class, 'search'])->name('search');

    Route::get('/capaian-spbe', [LandingPageController::class, 'capaianspbe'])->name('capaian-spbe');

    Route::get('/capaian-spbe/detail/{id}', [LandingPageController::class, 'detailcapaianspbe'])->name('detail-capaian-spbe');

    Route::get('/capaian-kinerja', [LandingPageController::class, 'capaianKinerja'])->name('capaian-kinerja');

    Route::get('/capaian-kinerja/detail/{id}', [LandingPageController::class, 'detailcapaianKinerja'])->name('detail-capaian-kinerja');

    Route::get('/capaian-lainnya', [LandingPageController::class, 'capaian'])->name('capaian-lainnya');

    Route::get('/capaian-lainnya/detail/{id}', [LandingPageController::class, 'detailcapaian'])->name('detail-capaian-lainnya');

    Route::get('/get-detail-arsip/{id}', [LandingPageController::class, 'getDetailArsip'])->name('get-detail-arsip');
});

// Authentication
Route::middleware(['revalidate','XSS'])->group(function () {
    Auth::routes([
        'register' => false,
        'verify' => true,
    ]); 
});

// Laravel File Manager
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::middleware(['auth', 'revalidate'])->group(function () {

    // Redirect to dashboard after login
    Route::get('/redirect', function () {

        //return redirect()->intended();
        if (auth()->user()->hasRole('admin') || auth()->user()->hasRole('super-admin')) {
            return Redirect::intended('Dashboard');
        } elseif (auth()->user()->hasRole('Eksekutif')) {
            return Redirect::intended('Informasi-Eksekutif');
        } else {
            return Redirect::intended('home');
        }

    });
    
    // Dashboard Admin 
    Route::prefix('Dashboard')->group(function () {

        // Dashboard Admin
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard.admin');
        
        Route::get('/chart-user', [DashboardController::class, 'handlechartuser'])->name('dashboard.chart-user');
    });

    // Dashboard Informasi Eksekutif
    Route::prefix('Informasi-Eksekutif')->group(function () {

        Route::get('/', [DashboardController::class, 'eksekutif'])->name('dashboard.eksekutif');
    
    });

    // Arsip Kolektor
    Route::prefix('arsip-kolektor')->group(function () {

        Route::get('instrumen-survey' , [InstrumenSurveyController::class, 'index'])->name('instrumen-survey.index');
        
        Route::get('instrumen-survey/isi-survey/{id}' , [InstrumenSurveyController::class, 'create'])->name('instrumen-survey.isisurvey');

        Route::post('instrumen-survey/{id}' , [InstrumenSurveyController::class, 'store'])->name('instrumen-survey.store');

        Route::get('instrumen-survey/edit/{id}' , [InstrumenSurveyController::class, 'edit'])->name('instrumen-survey.edit');

        Route::post('instrumen-survey/update/{id}' , [InstrumenSurveyController::class, 'update'])->name('instrumen-survey.update');
        
        Route::get('import-arsip' , [ImportArsipController::class, 'index'])->name('import-arsip.index');

        Route::post('import-arsip/store', [ImportArsipController::class, 'store'])->name('import-arsip.store');
    
        Route::get('instrumen-survey/responden/{id}', [InstrumenSurveyController::class, 'listResponden'])->name('instrumen-survey.listresponden');

        Route::resource('list-arsip', ListArsipController::class); 

        Route::post('list-arsip-digital/upload/{id}', [ListArsipController::class, 'upload'])->name('list-arsip-digital.upload');
        Route::get('list-arsip-digital/list/{id}', [ListArsipController::class, 'list'])->name('list-arsip-digital.list');
        Route::get('list-arsip-digital/download/{id}', [ListArsipController::class, 'download'])->name('list-arsip-digital.download');
        Route::delete('list-arsip-digital/delete/{id}', [ListArsipController::class, 'delete'])->name('list-arsip-digital.delete');
        Route::delete('list-arsip-digital/hapus/{id}', [ListArsipController::class, 'deleteFile'])->name('list-arsip-digital.hapus');
    });

    Route::prefix('arsip-identifikasi')->group(function () {
        Route::resource('taxonomy', \App\Http\Controllers\ArsipIdentifikasi\TaxonomyController::class); 
        Route::resource('list-arsip-digital', \App\Http\Controllers\ArsipIdentifikasi\ListArsipDigitalController::class);
        Route::get('list-arsip-digital/download/{id}', [ListArsipController::class, 'download'])->name('list-arsip-digital.download');
    });

    Route::prefix('arsip')->group(function () {
        Route::resource('survey', SurveyController::class);
        // Arsip Kolektor
        Route::resource('arsip-kolektor', ArsipKolektorController::class);
        Route::resource('jenis-arsip', JenisArsipController::class);
        Route::post('isi-survey/{id}', [JawabanSurveyController::class, 'store'])->name('arsip.jawab-survey');
        Route::post('jawab-survey/{id}', [JawabanSurveyController::class, 'update'])->name('arsip.jawab-survey.update');
        Route::get('arsipkolektor/download', [ArsipKolektorController::class, 'download'])->name('arsip.download');
       
        Route::post('detail-responden/{id}', [ArsipKolektorController::class, 'detail'])->name('arsip.detail');
        Route::get('import/{id}', [ArsipKolektorController::class, 'import'])->name('arsip.import');
        
        Route::get('file-arsip/download/{filename}', [ArsipKolektorController::class, 'downloadFile'])->name('download.file');
        Route::post('list-arsip-digital/upload/{id}', [ListArsipController::class, 'upload'])->name('list-arsip-digital.upload');
        Route::get('list-arsip-digital/list/{id}', [ListArsipController::class, 'list'])->name('list-arsip-digital.list');
        Route::get('list-arsip-digital/download/{id}', [ListArsipController::class, 'download'])->name('list-arsip-digital.download');
        Route::delete('list-arsip-digital/delete/{id}', [ListArsipController::class, 'delete'])->name('list-arsip-digital.delete');
        Route::delete('list-arsip-digital/hapus/{id}', [ListArsipController::class, 'deleteFile'])->name('list-arsip-digital.hapus');
        
    });

    // Modul Data Kinerja
    Route::prefix('unit-kerja')->group(function () {
        // Category
        Route::resource('categories', CategoryController::class);
        
        // Unit Kerja
        Route::resource('work-units', WorkUnitController::class);

        // Sub Category
        Route::get('/sub-category', [CategoryController::class, 'subCategory'])->name('categories.sub-category');
        Route::get('/sub-category/{id}', [CategoryController::class, 'edit_subCategory'])->name('categories.sub-category.edit');
        Route::put('/sub-category/{id}', [CategoryController::class, 'update_subCategory'])->name('categories.sub-category.update');
        Route::delete('/sub-category/{id}/delete', [CategoryController::class, 'destroy_subCategory'])->name('categories.sub-category.delete');
        Route::post('/categories/store-subcategory', [CategoryController::class, 'store_subCategory'])->name('categories.store-subcategory');

        // Kinerja
        Route::resource('kinerja', KinerjaController::class);
        Route::post('kinerja/{id}/send-kinerja', [KinerjaController::class, 'send'])->name('kinerja.send-kinerja');
        Route::get('/kotak-masuk', [KotakMasukController::class, 'index'])->name('kinerja.inbox');
        Route::post('/kotak-masuk/send-penugasan', [KotakMasukController::class, 'sendPenugasan'])->name('kinerja.send-penugasan');
        Route::post('getsubCategory', [KinerjaController::class, 'getsubCategory'])->name('kinerja.getsubCategory');
        Route::post('detail-kinerja', [KotakMasukController::class, 'getDetail'])->name('kinerja.detail-kinerja');
        Route::post('verifikasi-kinerja', [KotakMasukController::class, 'verifikasi'])->name('kinerja.verifikasi-kinerja');
        Route::post('openaccess-kinerja', [KotakMasukController::class, 'openaccessedit'])->name('kinerja.openaccessedit-kinerja');
        Route::post('update-kinerja', [KinerjaController::class, 'updateKinerja'])->name('kinerja.update-kinerja');
        Route::post('fetchsubcategory', [KinerjaController::class, 'fetchsubCategory'])->name('kinerja.fetchsubcategory');
        
        // Chart Data Kinerja
        Route::get('chart-kinerja', [ChartController::class, 'index'])->name('chart-kinerja');
        Route::get('chart-kinerja-eksekutif', [DashboardController::class, 'chart'])->name('chart-kinerja-eksekutif');
        Route::get('get-year', [DashboardController::class, 'getYear'])->name('get-year');
        Route::get('get-chart-kinerja', [ChartController::class, 'chart'])->name('chart-kinerja.get');
        
        // Prediksi Data Kinerja
        Route::get('get-prediksi-kinerja', [DashboardController::class, 'prediksi'])->name('prediksi-kinerja.get');

        // Chat
        Route::resource('chat', ChatController::class);
        Route::post('get-chat', [ChatController::class, 'getChat'])->name('chat.get-chat');
        Route::get('/download-file-pendukung/{id}', [ChartController::class, 'downloadFilePendukung'])->name('download.file-pendukung');
    });

    // Informasi Publik
    Route::prefix('informasi-publik')->group(function () {
        // Berita
        Route::resource('news', NewsController::class);

        // Infografis
        Route::resource('infografis', InfografisController::class);
    });

    // Modul Instansi
    Route::prefix('master-instansi')->group(function () {
        // Instansi
        Route::resource('instansi', InstansiController::class);

        // Sub Instansi
        Route::resource('sub-instansi', SubInstansiController::class);
    });

    // Users
    Route::resource('users', UserController::class);
    // Roles
    Route::resource('roles', RoleController::class);
    // Permissions
    Route::resource('permissions', PermissionController::class);
    // Activity Log
    Route::resource('activities', ActivityController::class);

    Route::get('activities/export/{id}', [ActivityController::class, 'export'])->name('activities.export');

    // Logout
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    // Home
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    // Read Notification
    Route::get('/read-notification', [App\Http\Controllers\HomeController::class, 'readNotification'])->name('read-notification');

    // Get Notification
    Route::get('/get-notification', [App\Http\Controllers\HomeController::class, 'getNotification'])->name('get-notification');


    // Setting
    Route::group(['prefix' => 'setting'], function () {
        Route::get('/', [SettingController::class, 'index'])->name('setting');

        Route::put('/update/app', [SettingController::class, 'updateApp'])->name('setting.update.app');
        Route::put('/update/socalmedia', [SettingController::class, 'updateSocalMedia'])->name('setting.update.socalmedia');

        Route::prefix('api-client')->group(function () {
            Route::get('/get', [ClientController::class, 'index'])->name('api-client');
            Route::post('/create', [ClientController::class, 'store'])->name('create-api-client');
            Route::delete('/delete/{id}', [ClientController::class, 'destroy'])->name('delete-api-client');      
        });
    });

    // Email Setting
    Route::resource('email-setting', EmailSettingController::class);

    // Email Test
    Route::post('/email-test', [EmailSettingController::class, 'emailTest'])->name('email-test');
    
    // Telegram Setting
    Route::resource('telegram-setting', TelegramSettingController::class);

    Route::prefix('user')->group(function () {

        // Dashboard User
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.user');

        // Profile User
        Route::get('/profile', [HomeController::class, 'profile'])->name('user.profile');

        // Update Profile User
        Route::post('/profile', [HomeController::class, 'updateProfile'])->name('profile.update');

        // Change Password User
        Route::post('/profile/password', [HomeController::class, 'updatePassword'])->name('change.password');
    });

    // Master Data Kinerja
    Route::prefix('master-data-kinerja')->group(function () {

        // Visi Misi
        Route::resource('visi-misi', VisiMisiController::class);

        // Tugas Fungsi
        Route::resource('tugas-fungsi', TugasFungsiController::class);

        // Struktur Organisasi
        Route::resource('struktur-organisasi', StrukturOrganisasiController::class);
        
        // Indikator Kinerja
        Route::resource('indikator-kinerja', IndikatorKinerjaController::class);

        // Sasaran Strategis
        Route::resource('sasaran-strategis', SasaranStrategisController::class); 

        // Target Kinerja
        Route::resource('target-kinerja', TargetKinerjaController::class);

        // Tujuan
        Route::resource('tujuan', TujuanController::class);

        // Peran Strategis
        Route::resource('peran-strategis', PeranStrategisController::class);

        // Rencana Strategis
        Route::resource('rencana-strategis', RencanaStrategisController::class);

        // Perjanjian Kinerja
        Route::resource('perjanjian-kinerja', PerjanjianKinerjaController::class);

        // Capaian Kinerja
        Route::resource('capaian-kinerja', CapaianKinerjaController::class);

        // Capaian SPBE
        Route::resource('capaian-spbe', CapaianSPBEController::class);

        // Realisasi Anggaran
        Route::resource('realisasi-anggaran', RealisasiAnggaranController::class);

        // Capaian
        Route::resource('capaian', CapaianController::class);
    });


    // Model Kinerja
    Route::resource('model-kinerja', ModelKinerjaController::class);

    Route::prefix('model-kinerja')->group(function () {
        Route::get('download/{id}', [ModelKinerjaController::class, 'download'])->name('download.file');
    });

    Route::resource('metadata', MetaDataController::class);

    Route::resource('glosarium', GlosariumController::class);

    Route::post('glosarium/getDetail', [GlosariumController::class, 'getDetail'])->name('glosarium.detail');
    

    Route::resource('kategori-pertanyaan', \App\Http\Controllers\KategoriPertanyaanController::class);

    Route::resource('kolom-pertanyaan', \App\Http\Controllers\KolomPertanyaanController::class);


    Route::post('/get-kolom-pertanyaan', [\App\Http\Controllers\PertanyaanController::class, 'getKolomPertanyaan'])->name('get-kolom-pertanyaan');

    Route::resource('pertanyaan', \App\Http\Controllers\PertanyaanController::class);

    Route::post('/get-jawaban-pertanyaan', [\App\Http\Controllers\PertanyaanController::class, 'getJawabanPertanyaan'])->name('get-jawaban-pertanyaan');
});


