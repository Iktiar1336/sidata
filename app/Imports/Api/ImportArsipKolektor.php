<?php

namespace App\Imports\Api;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection; 
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\Validator;
use App\Models\ArsipKolektor;

class ImportArsipKolektor implements ToCollection, WithHeadingRow
{
    public $user_id;
    public $filename;
    public $instansi_id;
    public $jenisarsip_id;
    public $jenis;

    public function __construct($user_id, $filename, $instansi_id, $jenis_arsip)
    {
        $this->user_id       = $user_id;
        $this->filename      = $filename;
        $this->instansi_id   = $instansi_id;
        $this->jenis         = $jenis_arsip->name;
        $this->jenisarsip_id = $jenis_arsip->id;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $arsip_kolektor = new ArsipKolektor([
                'user_id' => $this->user_id,
                'instansi_id' => $this->instansi_id,
                'jenisarsip_id' => $this->jenisarsip_id,
                'uraianitem' => $row['uraian_item'], 
                'nomor_surat' => $row['nomor_surat'],
                'tanggal_surat' =>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_surat']),
                'kode_klasifikasi' => $row['kode_klasifikasi'],
                'jumlah' => $row['jumlah'],
                'tingkat_perkembangan' => $row['tingkat_perkembangan'],
                'media_arsip' => $row['media_arsip'],
                'kondisi' => $row['kondisi'],
                'keterangan' => $row['keterangan'], 
            ]);
            

            $data = ArsipKolektor::where('nomor_surat', $row['nomor_surat'])->where('kode_klasifikasi', $row['kode_klasifikasi'])->get()->first();
            if ($data) {
                return [
                    'error' => true,
                    'code' => '422',
                    'message' => 'Data gagal diimport pada nomor ke - '.$row['no'] . " karena data sudah ada. silahkan cek kembali data anda.",
                    // 'data' => [
                    //     'uraianitem' => $row['uraian_item'], 
                    //     'nomor_surat' => $row['nomor_surat'],
                    //     'tanggal_surat' =>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_surat']),
                    //     'kode_klasifikasi' => $row['kode_klasifikasi'],
                    //     'jumlah' => $row['jumlah'],
                    //     'tingkat_perkembangan' => $row['tingkat_perkembangan'],
                    //     'media_arsip' => $row['media_arsip'],
                    //     'kondisi' => $row['kondisi'],
                    //     'keterangan' => $row['keterangan'],
                    // ]
                ];
            } else {
                try {
                    $arsip_kolektor->save();
                } catch (\Exception $e) {
                    //dd($e->getMessage());
                    return [
                        'error' => true,
                        'message' => 'Data gagal diimport pada nomor ke - '.$row['no'] . " karena data tidak sesuai dengan format yang ditentukan. silahkan cek kembali data anda."
                    ];
                }
            }
        }
    }
}
