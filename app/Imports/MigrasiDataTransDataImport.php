<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Category;
use Excel;
use App\Imports\MigrasiDataTransFileImport;

class MigrasiDataTransDataImport implements ToCollection, WithHeadingRow
{
    protected $datasubkategori;

    public function __construct($datasubkategori)
    {
        $this->datasubkategori = $datasubkategori;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $datatransdata = [];
        foreach ($collection as $row) 
        {
            foreach ($this->datasubkategori as $key => $value) 
            {
                if ($row['idsubkat'] == $value['last_idsub'] && $row['idkat'] == $value['last_idkat']) 
                {
                    if($row['status'] == 0)
                    {
                        $status = 1;
                        $masterdata_id = NULL;

                    } elseif($row['status'] == 1)
                    {
                        $status = 2;
                        $masterdata_id = NULL;

                    } elseif($row['status'] == 2)
                    {
                        $status = 3;
                        $masterdata_id = 12;

                    } elseif($row['status'] == 3)
                    {
                        $status = 4;
                        $masterdata_id = 12;
                    } 

                    if($row['verify'] != null && $row['verify'] != 'NULL' && $row['verify'] != '')
                    {
                        $verified_at = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['verify']));
                    } else {
                        $verified_at = null;
                    }

                    if($row['process'] == null || $row['process'] == 'NULL' || $row['process'] == '')
                    {
                        $proses_at = null;
                    } else {
                        $proses_at = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['process']));
                    }

                    $datatransdata[] = [
                        'last_idkat' => $row['idkat'],
                        'new_idkat' => $value['new_idkat'],
                        'last_idsub' => $row['idsubkat'],
                        'new_idsub' => $value['new_idsub'],
                        'last_idtrans' => $row['id'],
                        'new_idtrans' => $row['id'],
                        'unitkerjaprodusendata_id' => $row['unitid'],
                        'year' => $row['tahun'],
                        'jumlah' => $row['jumlah'],
                        'satuan' => $row['satuan'],
                        'save_at' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['save'])),
                        'status' => $status,
                        'keterangan' => $row['keterangan'],
                        'send_at' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['send'])),
                        'verified_at' => $verified_at,
                        'proses_at' => $proses_at,
                    ];


                }
            }
        }

        $filetransfile = public_path('/format/trans_files.csv');

        $import = Excel::import(new MigrasiDataTransFileImport($datatransdata), $filetransfile);

        \Log::info('Migrasi Data Selesai');
        return $datatransdata;
    }
}
