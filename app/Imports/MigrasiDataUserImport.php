<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Excel;
use App\Models\User;
use App\Imports\MigrasiDataKinerjaImport;

class MigrasiDataUserImport implements ToCollection, WithHeadingRow
{

    protected $datapengolahdata;

    public function __construct($datapengolahdata)
    {
        $this->datapengolahdata = $datapengolahdata;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $datakinerja = [];
        foreach ($collection as $row) 
        {
            foreach ($this->datapengolahdata as $key => $value) 
            {
                if ($row['id'] == $value['pengolahdata_id']) 
                {
                    $user = User::where('name', $row['name'])->where('username', $row['username'])->first();

                    $produsendata = User::where('workunit_id', $value['unitkerjaprodusendata_id'])->first();
                    $datakinerja[] = [
                        'last_idkat' => $value['last_idkat'],
                        'new_idkat' => $value['new_idkat'],
                        'last_idsub' => $value['last_idsub'],
                        'new_idsub' => $value['new_idsub'],
                        'year' => $value['tahun'],
                        'jumlah' => $value['jumlah'],
                        'satuan' => $value['satuan'],
                        'save_at' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value['save_at']),
                        'status' => $value['status'],
                        'keterangan' => $value['keterangan'],
                        'send_at' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value['send_at']),
                        'verified_at' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value['verified_at']),
                        'proses_at' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value['proses_at']),
                        'file' => $value['file'],
                        'masterdata_id' => 12,
                        'pengolahdata_id' => $user->id,
                        'produsendata_id' => $produsendata->id,
                    ];
                }
            }
        }
    }
}
