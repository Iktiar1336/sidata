<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Excel;
use App\Models\Category;
use App\Imports\MigrasiDataSubKategoriImport;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MigrasiDataKategoriImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $datakategori = [];
        foreach ($collection as $row) 
        {
            $category = Category::where('workunit_id', $row['unitkerjaid'])->where('name', $row['name'])->first();
            //\Log::info($category);
            $datakategori[] = [
                'last_id' => $row['id'],
                'new_id' => $category->id,
                'nama' => $row['name'],
            ];

        }

        $filesubkategori = public_path('format/master_subdata.csv');

        $import = Excel::import(new MigrasiDataSubKategoriImport($datakategori), $filesubkategori);

        \Log::info('Import Kategori Selesai');
        return $import;
    }
}
