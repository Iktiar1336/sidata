<?php

namespace App\Imports;

use App\Models\ArsipKolektor;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ArsipImport implements ToCollection, WithHeadingRow
{
    public $intansi_id;
    public $nama_file;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function __construct($instansi_id, $nama_file)
    {
        $this->instansi_id = $instansi_id;
        $this->nama_file = $nama_file; 
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            //dd($rows);
            $arsip_kolektor = new ArsipKolektor([
                'instansi_id' => $this->instansi_id,
                'uraianitem' => $row['uraianitem'], 
                'nomor_surat' => $row['nomorsurat'],
                'tanggal_surat' =>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggalsurat']),
                'kode_klasifikasi' => $row['kodeklasifikasi'],
                'jumlah' => $row['jumlah'],
                'tingkat_perkembangan' => $row['tingkatperkembangan'],
                'user_id' => auth()->user()->id,
                'media_arsip' => $row['mediaarsip'],
                'keterangan' => $row['keterangan'],
            ]);

            $data = ArsipKolektor::where('instansi_id', $this->instansi_id)->where('nomor_surat', $row['nomor_surat'])->where('kode_klasifikasi', $row['kode_klasifikasi'])->get();
            if ($data) {
                return redirect()->route('arsip-kolektor.index')->with('error', "Data gagal diimport pada nomor ke-".$row['no'] . " karena data sudah ada");
            } else {
                try {
                    $arsip_kolektor->save();
                    return redirect()->route('arsip-kolektor.index')->with('inport-success', "Data berhasil diimport");
                } catch (\Exception $e) {
                    return redirect()->route('arsip-kolektor.index')->with('import-failed', "Data gagal diimport pada nomor ke-".$row['no']);
                }
            }
        }
    }
}
