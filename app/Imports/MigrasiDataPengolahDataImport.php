<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Category;
use App\Models\User;
use Excel;
use App\Imports\MigrasiDataUserImport;
use App\Imports\MigrasiDataKinerjaImport;

class MigrasiDataPengolahDataImport implements ToCollection, WithHeadingRow
{

    protected $datatransfile;

    public function __construct($datatransfile)
    {
        $this->datatransfile = $datatransfile;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $datapengolahdata = [];
        foreach ($collection as $row) 
        {
            foreach ($this->datatransfile as $key => $value) 
            {
                if ($row['idkat'] == $value['idkat'] && $row['tahun'] == $value['year']) 
                {
                    $datapengolahdata[] = [
                        'last_idkat' => $row['idkat'],
                        'new_idkat' => $value['new_idkat'],
                        'last_idsub' => $value['idsub'],
                        'new_idsub' => $value['new_idsub'],
                        'year' => $value['year'],
                        'jumlah' => $value['jumlah'],
                        'satuan' => $value['satuan'],
                        'unitkerjaprodusendata_id' => $value['unitkerjaprodusendata_id'],
                        'save_at' => $value['save_at'],
                        'status' => $value['status'],
                        'keterangan' => $value['keterangan'],
                        'send_at' =>$value['send_at'],
                        'verified_at' =>$value['verified_at'],
                        'proses_at' =>$value['proses_at'],
                        'file' => $value['file'],
                        'pengolahdata_id' => $row['userid'],
                        'masterdata_id' => 12,
                    ];

                } else {
                    $datapengolahdata[] = [
                        'last_idkat' => $row['idkat'],
                        'new_idkat' => $value['new_idkat'],
                        'last_idsub' => $value['idsub'],
                        'new_idsub' => $value['new_idsub'],
                        'year' => $value['year'],
                        'jumlah' => $value['jumlah'],
                        'satuan' => $value['satuan'],
                        'unitkerjaprodusendata_id' => $value['unitkerjaprodusendata_id'],
                        'save_at' => $value['save_at'],
                        'status' => $value['status'],
                        'keterangan' => $value['keterangan'],
                        'send_at' =>$value['send_at'],
                        'verified_at' =>NULL,
                        'proses_at' =>$value['proses_at'],
                        'file' => $value['file'],
                        'pengolahdata_id' => NULL,
                        'masterdata_id' => 12,
                    ];
                }
            }
        }

        $fileuser = public_path('format/people.csv');

        $import = Excel::import(new MigrasiDataKinerjaImport($datapengolahdata), $fileuser);

        \Log::info('MigrasiDataPengolahDataImport: '.json_encode($datapengolahdata));
        return $import;
        
    }
}
