<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Category;
use Excel;
use App\Imports\MigrasiDataTransDataImport;

class MigrasiDataSubKategoriImport implements ToCollection, WithHeadingRow
{
    protected $datakategori;

    public function __construct($datakategori)
    {
        $this->datakategori = $datakategori;
    }


    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $datasubkategori = [];
        foreach ($collection as $row) 
        {
            foreach ($this->datakategori as $key => $value) 
            {
                if ($row['idkat'] == $value['last_id']) 
                {
                    $category = Category::whereNotNull('parent_id')->where('name', $row['name'])->first();
                    //\Log::info($row['name']);
                    $datasubkategori[] = [
                        'last_idkat' => $value['last_id'],
                        'new_idkat' => $value['new_id'],
                        'last_idsub' => $row['id'],
                        'new_idsub' => $category->id,
                        'nama' => $row['name'],
                    ];

                }
            }
        }

        $filetransdata = public_path('format/trans_data.xlsx');

        $import = Excel::import(new MigrasiDataTransDataImport($datasubkategori), $filetransdata);

        \Log::info('Import Sub Kategori Selesai');
        return $import;

    }
}
