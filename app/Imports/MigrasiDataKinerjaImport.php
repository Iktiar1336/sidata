<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Kinerja;
use App\Models\Chat;
use App\Models\Category;
use App\Models\FileUpload;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Excel; 
use App\Models\User;

class MigrasiDataKinerjaImport implements ToCollection, WithHeadingRow
{
    protected $datapengolahdata;

    public function __construct($datapengolahdata)
    {
        $this->datapengolahdata = $datapengolahdata;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $datakinerja = [];
        foreach ($collection as $row) 
        {
            foreach ($this->datapengolahdata as $key => $value) 
            {
                if ($row['id'] == $value['pengolahdata_id']) 
                {
                    $user = User::where('name', $row['name'])->where('username', $row['username'])->first();

                    $produsendata = User::where('workunit_id', $value['unitkerjaprodusendata_id'])->first();
                    
                    $category = Category::where('id',$value['new_idkat'])->first();

                    $chat = Chat::create([
                        'topic' => 'Kinerja '.$category->name.' Tahun '.$value['year'],
                    ]);

                    $fileupload = FileUpload::create([
                        'filename' => $value['file'],
                        'category_id' => $value['new_idkat'],
                        'year' => $value['year'],
                    ]);

                    $kinerja = Kinerja::create([
                        'category_id' => $value['new_idkat'],
                        'sub_category_id' => $value['new_idsub'],
                        'workunit_id' => $value['unitkerjaprodusendata_id'],
                        'year' => $value['year'],
                        'total' => $value['jumlah'],
                        'satuan' => $value['satuan'],
                        'chat_id' => $chat->id,
                        'status' => $value['status'],
                        'fileupload_id' => $fileupload->id,
                        'description' => $value['keterangan'],
                        'save_at' => $row['save_at'],
                        'send_at' => $row['send_at'],
                        'verified_at' => $row['verified_at'],
                        'process_at' => $row['proses_at'],
                        'masterdata_id' => 12,
                        'pengolahdata_id' => $user->id,
                        'produsendata_id' => $produsendata->id,
                    ]);
                } else {
                    $user = User::where('name', $row['name'])->where('username', $row['username'])->first();

                    $produsendata = User::where('workunit_id', $value['unitkerjaprodusendata_id'])->first();
                    
                    $category = Category::where('id',$value['new_idkat'])->first();

                    $chat = Chat::create([
                        'topic' => 'Kinerja '.$category->name.' Tahun '.$value['year'],
                    ]);

                    $fileupload = FileUpload::create([
                        'filename' => $value['file'],
                        'category_id' => $value['new_idkat'],
                        'year' => $value['year'],
                    ]);

                    $kinerja = Kinerja::create([
                        'category_id' => $value['new_idkat'],
                        'sub_category_id' => $value['new_idsub'],
                        'workunit_id' => $value['unitkerjaprodusendata_id'],
                        'year' => $value['year'],
                        'total' => $value['jumlah'],
                        'satuan' => $value['satuan'],
                        'chat_id' => $chat->id,
                        'status' => $value['status'],
                        'fileupload_id' => $fileupload->id,
                        'description' => $value['keterangan'],
                        'save_at' => $value['save_at'],
                        'send_at' => $value['send_at'],
                        'verified_at' => $value['verified_at'],
                        'process_at' => $value['proses_at'],
                        'masterdata_id' => 12,
                        'pengolahdata_id' => NULL,
                        'produsendata_id' => $produsendata->id,
                    ]);
                }
            }
        }
    }
}
