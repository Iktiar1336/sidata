<?php

namespace App\Imports;

use App\Models\ArsipKolektor;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection; 
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\Validator;

class ArsipKolektorImport implements ToCollection, WithHeadingRow
{
    public $rowCounter = 0;
    public $user_id;
    public $nama_file;
    public $instansi_id;
    public $jenisarsip_id;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function __construct($user_id, $nama_file, $jenis, $instansi_id, $jenisarsip_id)
    {
        $this->user_id       = $user_id;
        $this->nama_file     = $nama_file;
        $this->instansi_id   = $instansi_id;
        $this->jenis = $jenis;
        $this->jenisarsip_id = $jenisarsip_id;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $arsip_kolektor = new ArsipKolektor([
                'user_id' => $this->user_id,
                'instansi_id' => $this->instansi_id,
                'jenisarsip_id' => $this->jenisarsip_id,
                'uraianitem' => $row['uraian_item'], 
                'nomor_surat' => $row['nomor_surat'],
                'tanggal_surat' =>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_surat']),
                'kode_klasifikasi' => $row['kode_klasifikasi'],
                'jumlah' => $row['jumlah'],
                'tingkat_perkembangan' => $row['tingkat_perkembangan'],
                'media_arsip' => $row['media_arsip'],
                'kondisi' => $row['kondisi'],
                'keterangan' => $row['keterangan'], 
                'status' => $row['status'],
            ]);
            

            $data = ArsipKolektor::where('nomor_surat', $row['nomor_surat'])->where('kode_klasifikasi', $row['kode_klasifikasi'])->get()->first();
            if ($data) {
                return redirect()->route('import-arsip.index')->with('error', "Data gagal diimport pada nomor ke- ".$row['no'] . " karena data sudah ada. silahkan cek kembali data anda.");
            } else {
                try {
                    $arsip_kolektor->save();
                } catch (\Exception $e) {
                    //dd($e->getMessage());
                    return redirect()->route('import-arsip.index')->with('import-failed', "Data gagal diimport pada nomor ke - ".$row['no'] . " karena data tidak sesuai. silahkan cek kembali data anda.");
                }
            }
        }
    }
}
