<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Excel;
use App\Imports\MigrasiDataPengolahDataImport;

class MigrasiDataTransFileImport implements ToCollection, WithHeadingRow
{

    protected $datatransdata;

    public function __construct($datatransdata)
    {
        $this->datatransdata = $datatransdata;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $datatransfile = [];
        foreach ($collection as $row) 
        {
            foreach ($this->datatransdata as $key => $value) 
            {
                if ($row['idkat'] == $value['last_idkat'] && $row['tahun'] == $value['year']) 
                {
                    $datatransfile[] = [
                        'idkat' => $value['last_idkat'],
                        'new_idkat' => $value['new_idkat'],
                        'idsub' => $value['last_idsub'],
                        'new_idsub' => $value['new_idsub'],
                        'year' => $value['year'],
                        'jumlah' => $value['jumlah'],
                        'unitkerjaprodusendata_id' => $value['unitkerjaprodusendata_id'],
                        'satuan' => $value['satuan'],
                        'save_at' => $value['save_at'],
                        'status' => $value['status'],
                        'keterangan' => $value['keterangan'],
                        'send_at' => $value['send_at'],
                        'verified_at' => $value['verified_at'],
                        'proses_at' => $value['proses_at'],
                        'file' => $row['path'],
                    ];
                } 
            }
        }

        $filepengolahdata = public_path('format/pengelola_data.csv');

        $import = Excel::import(new MigrasiDataPengolahDataImport($datatransfile), $filepengolahdata);

        \Log::info('Migrasi Data Trans File Selesai');
        return $import;
    }
}
