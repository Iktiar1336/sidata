<?php 

namespace App\Interfaces;

use App\Http\Requests\ManualArsipKolektorRequest;

interface ArsipKolektorInterfaces
{
    public function GetAll();
    public function UploadByManual($request);
    public function UploadByExcel($data); 
}