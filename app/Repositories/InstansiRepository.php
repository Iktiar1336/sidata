<?php

namespace App\Repositories;

use App\Models\Instansi;
use App\Interfaces\InstansiInterfaces;
use App\Traits\ResponseAPI;

class InstansiRepository implements InstansiInterfaces
{
    use ResponseAPI;

    public function getAll()
    {
        try {
            $instansi = Instansi::orderBy('created_at', 'desc')->whereNull('parent_id')->with('subinstansi.subinstansi.subinstansi.subinstansi.subinstansi')->paginate(10);
            return $this->success('success', $instansi);
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), [], 500);
        }
    }
}