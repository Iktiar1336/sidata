<?php

namespace App\Repositories;

use App\Models\ArsipKolektor;
use App\Interfaces\ArsipKolektorInterfaces;
use App\Traits\ResponseAPI;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Imports\Api\ImportArsipKolektor;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\JenisArsip;

class ArsipKolektorRepository implements ArsipKolektorInterfaces
{
    use ResponseAPI;

    public function GetAll()
    {
        $arsip_kolektor = ArsipKolektor::orderBy('created_at', 'desc')->with('arsipdigital')->get();

        return $this->success('success', $arsip_kolektor); 
    }

    public function UploadByManual($data)
    {
        $validator = Validator::make($data, [
            'instansi_id' => 'required|numeric|exists:instansi,id',
            'jenis_arsip_id' => 'required|numeric|exists:jenis_arsip,id',
            'uraian_item' => 'required',
            'nomor_surat' => 'required',
            'tanggal_surat' => 'required', 
            'kode_klasifikasi' => 'required',
            'tingkat_perkembangan' => 'required',
            'media_arsip' => 'required',
            'kondisi' => 'required',
            'jumlah' => 'required',
            'keterangan' => 'required',
        ]);

        if ($validator->fails()) {
             return [
                'code' => 422,
                'error' => true,
                'message' => $validator->errors(),
             ];
        }

        $cek = ArsipKolektor::where('nomor_surat', $data['nomor_surat'])->where('kode_klasifikasi', $data['kode_klasifikasi'])->get()->first();
        
        if ($cek) {
            return [
                'code' => 422,
                'error' => true,
                'message' => 'Nomor Surat dan Kode Klasifikasi sudah ada, silahkan cek kembali data yang anda masukkan',
            ];
        }

        try {
            $arsip_kolektor = ArsipKolektor::create([
                'user_id' => 1,
                'instansi_id' => $data['instansi_id'],
                'jenisarsip_id' => $data['jenis_arsip_id'],
                'uraianitem' => $data['uraian_item'],
                'nomor_surat' => $data['nomor_surat'],
                'tanggal_surat' => $data['tanggal_surat'],
                'kode_klasifikasi' => $data['kode_klasifikasi'],
                'tingkat_perkembangan' => $data['tingkat_perkembangan'],
                'media_arsip' => $data['media_arsip'],
                'kondisi' => $data['kondisi'],
                'jumlah' => $data['jumlah'],
                'keterangan' => $data['keterangan'],
                'status' => $data['status'],
            ]);
            return $this->success('success', $arsip_kolektor);
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), [], 500);
        }
    }

    public function UploadByExcel($data)
    {
        return $this->success('success', $data);
        $validator = Validator::make($data, [
            'instansi_id' => 'required|numeric|exists:instansi,id',
            'jenis_arsip_id' => 'required|numeric|exists:jenis_arsip,id',
            'file' => 'required|mimes:xlsx,xls,csv',
        ]);

        if ($validator->fails()) {
             return [
                'code' => 422,
                'error' => true,
                'message' => $validator->errors(),
             ];
        }

        try {
            $file = $data->files['file'];
            return $this->success('success', $file);
            $filename = $file->getClientOriginalName();
            $user_id = 1;
            $instansi_id = $data['instansi_id'];
            $jenis_arsip = JenisArsip::find($data['jenis_arsip_id']);

            try {
                Excel::import(new ApiArsipKolektorImport($user_id, $filename, $instansi_id, $jenis_arsip), request()->file('file'));
            } catch (\Throwable $th) {
                return [
                    'code' => 422,
                    'error' => true,
                    'message' => 'Terjadi kesalahan pada file excel, silahkan cek kembali file excel anda',
                ];
            }

            return $this->success('success', []);
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), [], 500);
        }
    }

}