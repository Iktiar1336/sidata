<?php 

namespace App\Repositories;

use App\Models\JenisArsip;
use App\Interfaces\JenisArsipInterfaces;
use App\Traits\ResponseAPI;

class JenisArsipRepository implements JenisArsipInterfaces
{
    use ResponseAPI;

    public function getAll()
    {
        try {
            $jenis_arsip = JenisArsip::orderBy('created_at', 'desc')->paginate(10);
            return $this->success('success', $jenis_arsip);
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), [], 500);
        }
    }
}