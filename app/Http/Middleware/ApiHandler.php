<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ApiHandler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->is('api/*')) {
            try {
                $request->headers->set('client-id', Crypt::decrypt($request->headers->get('client-id')));
                $request->headers->set('client-secret', Crypt::decrypt($request->headers->get('client-secret')));
            } catch (\Exception $e) {
                return response()->json([
                    'code' => 401,
                    'status' => 'error',
                    'message' => 'Unauthorized',
                ], 401);
            }

            $response = \Http::asForm()->post('https://sidata.adhyamitra.com/oauth/token', [
                'grant_type' => 'client_credentials',
                'client_id' => $request->headers->get('client-id'),
                'client_secret' => $request->headers->get('client-secret'),
                'scope' => '',
            ]);

            if ($response->failed()) {
                return response()->json([
                    'code' => 401,
                    'status' => 'error',
                    'message' => 'Unauthorized',
                ], 401);
            } else {
                $request->headers->set('Authorization', 'Bearer ' . $response->json()['access_token']);
            }

            return $next($request);
        }
    }
}
