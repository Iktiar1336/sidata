<?php

namespace App\Http\Controllers\ModelKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ModelKinerja;
use App\Models\Capaian;
use App\Models\VisiMisi;
use App\Models\TugasFungsi;
use App\Models\StrukturOrganisasi;  
use App\Models\CapaianKinerja;
use App\Models\RealisasiAnggaran;
use App\Models\PeranStrategis;
use App\Models\RencanaStrategis;
use App\Models\CapaianSPBE;
use App\Models\PerjanjianKinerja;
use DataTables;
use PDF;
use Illuminate\Support\Facades\Crypt;

class ModelKinerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = ModelKinerja::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<form action="'.route('model-kinerja.destroy', Crypt::encrypt($row->id)).'" method="POST">
                        <a href="'.route('download.file', Crypt::encrypt($row->id)).'" class="btn btn-info btn-sm"><i class="fas fa-download"></i></a>
                        <a href="'.route('model-kinerja.show', Crypt::encrypt($row->id)).'" class="btn btn-primary btn-sm" target="_BLANK"><i class="fas fa-eye"></i></a>
                        <a href="'.route('model-kinerja.edit', Crypt::encrypt($row->id)).'" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                        '.csrf_field().'
                        '.method_field('DELETE').'
                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                        </form>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('model-kinerja.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $visi_misi = VisiMisi::orderBy('id', 'DESC')->take(1)->get();
        $tugas_fungsi = TugasFungsi::orderBy('id', 'DESC')->take(1)->get();
        $struktur_organisasi = StrukturOrganisasi::where('status', '1')->get();
        $peranstrategis = PeranStrategis::orderBy('id', 'DESC')->take(1)->get();
        $rencanastrategis = RencanaStrategis::orderBy('id', 'DESC')->get();
        $perjanjiankinerja = PerjanjianKinerja::orderBy('id', 'DESC')->get();
        $capaiankinerja = CapaianKinerja::orderBy('id', 'DESC')->get();
        $capaianspbe = CapaianSPBE::orderBy('id', 'DESC')->get();
        $realisasianggaran = RealisasiAnggaran::orderBy('id', 'DESC')->get();
        $capaian = Capaian::all();
        return view('model-kinerja.create', compact('visi_misi', 'tugas_fungsi', 'struktur_organisasi', 'capaian', 'peranstrategis', 'rencanastrategis', 'perjanjiankinerja', 'capaiankinerja', 'realisasianggaran', 'capaianspbe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'tahun' => 'required',
            'pengantar' => 'required',
            'rencanastrategis' => 'required',
            'perjanjiankinerja' => 'required',
            'capaiankinerjaorganisasi' => 'required',
            'realisasianggaran' => 'required',
            'capaianspbe' => 'required',
            'capaiankinerja' => 'required',
        ]);

        $visi_misi = VisiMisi::orderBy('id', 'DESC')->take(1)->get()->first();
        $tugas_fungsi = TugasFungsi::orderBy('id', 'DESC')->take(1)->get()->first();
        $struktur_organisasi = StrukturOrganisasi::where('status', '1')->get()->first();
        $peranstrategis = PeranStrategis::orderBy('id', 'DESC')->take(1)->get()->first();

        $model_kinerja = ModelKinerja::create([
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'pengantar' => $request->pengantar,
            'user_id' => auth()->user()->id,
            'visimisi_id' => $visi_misi->id,
            'core_values_asn' => $request->corevaluesasn,
            'nilai_nilai_anri' => $request->nilainilaianri,
            'latar_belakang' => $request->latarbelakang,
            'tugasfungsi_id' => $tugas_fungsi->id,
            'strukturorganisasi_id' => $struktur_organisasi->id,
            'peranstrategis_id' => $peranstrategis->id,
            'sistematika_penyajian' => $request->sistematikapenyajian,
            'rencanastrategis_id' => $request->rencanastrategis,
            'perjanjiankinerja_id' => $request->perjanjiankinerja,
            'capaianspbe_id' => $request->capaianspbe,
        ]);

        $model_kinerja->capaiankinerja()->attach($request->capaiankinerjaorganisasi);
        $model_kinerja->capaian()->attach($request->capaiankinerja);
        $model_kinerja->realisasianggaran()->attach($request->realisasianggaran);

        return redirect()->route('model-kinerja.index')->with('success', 'Model Kinerja Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model_kinerja = ModelKinerja::find(Crypt::decrypt($id));
        $pdf = PDF::loadView('model-kinerja.file', compact('model_kinerja'));
        return $pdf->stream();
    }

    public function download($id)
    {
        $model_kinerja = ModelKinerja::find(Crypt::decrypt($id));
        $filename = $model_kinerja->judul.'.docx';
        $headers = array(
            "Content-type"=>"text/html",
            "Content-Disposition"=>"attachment;Filename=". $model_kinerja->judul.".doc"
        );
        $html = view('model-kinerja.file', compact('model_kinerja'))->render();
        return response($html, 200, $headers);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model_kinerja = ModelKinerja::find(Crypt::decrypt($id));
        $visi_misi = VisiMisi::orderBy('id', 'DESC')->take(1)->get();
        $tugas_fungsi = TugasFungsi::orderBy('id', 'DESC')->take(1)->get();
        $struktur_organisasi = StrukturOrganisasi::where('status', '1')->get();
        $peranstrategis = PeranStrategis::orderBy('id', 'DESC')->take(1)->get();
        $rencanastrategis = RencanaStrategis::orderBy('id', 'DESC')->get();
        $perjanjiankinerja = PerjanjianKinerja::orderBy('id', 'DESC')->get();
        $capaiankinerja = CapaianKinerja::orderBy('id', 'DESC')->get();
        $capaianspbe = CapaianSPBE::orderBy('id', 'DESC')->get();
        $realisasianggaran = RealisasiAnggaran::orderBy('id', 'DESC')->get();
        $capaian = Capaian::all();
        $capaiankinerja_id = $model_kinerja->capaiankinerja->pluck('id')->toArray();
        $capaian_id = $model_kinerja->capaian->pluck('id')->toArray();
        $realisasianggaran_id = $model_kinerja->realisasianggaran->pluck('id')->toArray();
        
        return view('model-kinerja.edit', compact('model_kinerja', 'visi_misi', 'tugas_fungsi', 'struktur_organisasi', 'capaian', 'peranstrategis', 'rencanastrategis', 'perjanjiankinerja', 'capaiankinerja', 'realisasianggaran', 'capaianspbe', 'capaiankinerja_id', 'capaian_id', 'realisasianggaran_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model_kinerja = ModelKinerja::find(Crypt::decrypt($id));

        $this->validate($request, [
            'judul' => 'required',
            'tahun' => 'required',
            'pengantar' => 'required',
            'rencanastrategis' => 'required',
            'perjanjiankinerja' => 'required',
            'capaiankinerjaorganisasi' => 'required',
            'realisasianggaran' => 'required',
            'capaianspbe' => 'required',
            'capaiankinerja' => 'required',
            'user_id' => 'required|' . $model_kinerja->user_id,
        ]);

        $visi_misi = VisiMisi::orderBy('id', 'DESC')->take(1)->get()->first();
        $tugas_fungsi = TugasFungsi::orderBy('id', 'DESC')->take(1)->get()->first();
        $struktur_organisasi = StrukturOrganisasi::where('status', '1')->get()->first();
        $peranstrategis = PeranStrategis::orderBy('id', 'DESC')->take(1)->get()->first();

        $model_kinerja->update([
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'pengantar' => $request->pengantar,
            'visimisi_id' => $visi_misi->id,
            'core_values_asn' => $request->corevaluesasn,
            'nilai_nilai_anri' => $request->nilainilaianri,
            'latar_belakang' => $request->latarbelakang,
            'tugasfungsi_id' => $tugas_fungsi->id,
            'strukturorganisasi_id' => $struktur_organisasi->id,
            'peranstrategis_id' => $peranstrategis->id,
            'sistematika_penyajian' => $request->sistematikapenyajian,
            'rencanastrategis_id' => $request->rencanastrategis,
            'perjanjiankinerja_id' => $request->perjanjiankinerja,
            'capaianspbe_id' => $request->capaianspbe,
        ]);

        $model_kinerja->capaiankinerja()->attach($request->capaiankinerjaorganisasi);
        $model_kinerja->capaian()->attach($request->capaiankinerja);
        $model_kinerja->realisasianggaran()->attach($request->realisasianggaran);

        return redirect()->route('model-kinerja.index')->with('update', 'Model Kinerja Berhasil Diubah');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model_kinerja = ModelKinerja::find(Crypt::decrypt($id));
        $model_kinerja->capaiankinerja()->detach();
        $model_kinerja->capaian()->detach();
        $model_kinerja->realisasianggaran()->detach();
        $model_kinerja->delete();
        return redirect()->route('model-kinerja.index')->with('delete', 'Model Kinerja Berhasil Dihapus');
    }
}
