<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pertanyaan;
use App\Models\SurveyArsip;
use App\Models\KategoriPertanyaan;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class KategoriPertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = KategoriPertanyaan::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('survey', function($row){
   
                           $survey = $row->surveyArsip->nama;
     
                            return $survey;
                    })
                    ->addColumn('action', function($row){
   
                           $btn = '<form action="'.route('kategori-pertanyaan.destroy', $row->id).'" method="POST">'
                                    .csrf_field()
                                    .method_field('DELETE')
                                    .'<a href="'.route('kategori-pertanyaan.edit', $row->id).'" class="edit btn btn-primary btn-sm">Edit</a>'
                                    .'<button type="submit" class="delete btn btn-danger btn-sm">Delete</button>'
                                .'</form>';
     
                            return $btn;
                    })
                    ->rawColumns(['action', 'survey', 'deskripsi'])
                    ->make(true);
        }

        $surveyArsip = SurveyArsip::all();
      
        return view('kategori-pertanyaan.index', compact('surveyArsip'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'survey_id' => 'required',
        ]);

        $kategoriPertanyaan = KategoriPertanyaan::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'slug' => str_replace(' ', '-', strtolower($request->judul)),
            'survey_id' => $request->survey_id,
        ]);

        return redirect()->route('kategori-pertanyaan.index')->with('insert', 'Kategori Pertanyaan berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategoriPertanyaan = KategoriPertanyaan::find(Crypt::decrypt($id));
        $surveyArsip = SurveyArsip::all();
        return view('kategori-pertanyaan.edit', compact('kategoriPertanyaan', 'surveyArsip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'survey_id' => 'required',
        ]);

        $kategoriPertanyaan = KategoriPertanyaan::find($id);
        $kategoriPertanyaan->update([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'slug' => str_replace(' ', '-', strtolower($request->judul)),
            'survey_id' => $request->survey_id,
        ]);

        return redirect()->route('kategori-pertanyaan.index')->with('update', ' Kategori Pertanyaan berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
