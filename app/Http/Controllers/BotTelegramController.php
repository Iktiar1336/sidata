<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;

class BotTelegramController extends Controller
{
    public function setWebhook()
    {
        $response = Telegram::setWebhook(['url' => env('TELEGRAM_WEBHOOK_URL')]); 
        dd($response);
    }

    public function commandHandler(Request $request)
    {
        $updates = Telegram::commandsHandler(true);
        $chat_id = $updates->getChat()->getId();
        $username = $updates->getChat()->getUsername();

        if($updates->getMessage()->getText() == 'start' || $updates->getMessage()->getText() == 'Start'){
            $text = "Halo <a href='tg://user?id=$chat_id'><b>$username</b></a>, \n\n";
            $text .= "Selamat datang di \nBot Telegram Aplikasi SIDATA \n";
            $text .= "Berikut adalah ID Telegram Anda : \n\n";
            $text .= "<b>ID Telegram</b> : <a href='tg://user?id=$chat_id'>$chat_id</a> \n\n";
            $text .= "Silahkan kirimkan ID Telegram Anda ke Admin untuk di dibuatkan akun pada Aplikasi SIDATA \n\n";
            $text .= "Dimohon untuk tidak membalas pesan ini. \n\n";
            $text .= "Terima Kasih. \n";
            
            Telegram::sendMessage([
                'chat_id' => $chat_id,
                'parse_mode' => 'HTML',
                'text' => $text
            ]);
        }
    }
}
