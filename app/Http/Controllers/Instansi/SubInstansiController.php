<?php

namespace App\Http\Controllers\Instansi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Instansi;
use DataTables;

class SubInstansiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:List Sub Instansi|Tambah Sub Instansi|Edit Sub Instansi|Hapus Sub Instansi', ['only' => ['index','store']]);
        $this->middleware('permission:Tambah Sub Instansi', ['only' => ['create','store']]);
        $this->middleware('permission:Edit Sub Instansi', ['only' => ['edit','update']]);
        $this->middleware('permission:Hapus Sub Instansi', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $datasubinstansi = Instansi::whereNotNull('parent_id')->orderBy('created_at', 'DESC')->get();
            return Datatables::of($datasubinstansi)
                    ->addIndexColumn()
                    ->addColumn('nama_instansi', function($row){
   
                           $subinstansi = $row->subinstansi;
    
                          return $subinstansi;
                    })
                    ->addColumn('nama_sub_instansi', function($row){
   
                           $nama_subinstansi = $row->nama_instansi;
    
                        return $nama_subinstansi;
                    })
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editInstansi">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteInstansi">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        $instansi = Instansi::all();
        return view('admin.instansi.index', compact('instansi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'nomenklatur_unit' => 'required',
            'instansi_id' => 'required',
        ]);

        $subinstansi = Instansi::create([
            'code' => $request->code,
            'nama_instansi' => $request->nama,
            'alamat' => $request->alamat,
            'telp' => $request->telp,
            'email' => $request->email,
            'nomenklatur_unit' => $request->nomenklatur_unit,
            'parent_id' => $request->instansi_id,
        ]);

        return redirect()->route('instansi.index')->with('success', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
