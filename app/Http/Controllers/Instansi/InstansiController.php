<?php

namespace App\Http\Controllers\Instansi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Instansi;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class InstansiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:List Instansi|Tambah Instansi|Edit Instansi|Hapus Instansi', ['only' => ['index','store']]);
        $this->middleware('permission:Tambah Instansi', ['only' => ['create','store']]);
        $this->middleware('permission:Edit Instansi', ['only' => ['edit','update']]);
        $this->middleware('permission:Hapus Instansi', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $masterinstansi = Instansi::orderBy('created_at', 'DESC')->get()->first();
        if($masterinstansi == null){
            $kode = '001';
        } else {
            $kode = str_pad($masterinstansi->id + 1, 3, '0', STR_PAD_LEFT);
        }
        if ($request->ajax()) {
            $datainstansi = Instansi::orderBy('created_at', 'DESC')->get();
            return Datatables::of($datainstansi)
                    ->addIndexColumn()
                    ->addColumn('instansi', function($row){
    
                        if(count($row->subinstansi)){
                            foreach($row->subinstansi as $subinstansi){
                                $instansi = $row->nama_instansi;
                            }
                        }else{
                            $instansi = $row->nama_instansi;
                        }

                        return $instansi;

                    })
                    ->addColumn('nama_instansi', function($row){
    
                        if(count($row->subinstansi)){
                            foreach($row->subinstansi as $subinstansi){
                                $instansi = $row->nama_instansi;
                            }
                        }else{
                            $instansi = $row->nama_instansi;
                        }

                        return $instansi;

                    })
                    ->addColumn('action', function($row){

                        return view('admin.instansi.actionbtn', compact('row'));
 
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        $instansi = Instansi::all();
        return view('admin.instansi.index', compact('instansi', 'kode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'nomenklatur_unit' => 'required',
        ]);

        $instansi = Instansi::create([
            'code' => $request->code,
            'nama_instansi' => $request->nama,
            'alamat' => $request->alamat,
            'telp' => $request->telp,
            'email' => $request->email,
            'nomenklatur_unit' => $request->nomenklatur_unit,
        ]);

        activity("Menambahkan Data Instansi")
            ->causedBy(auth()->user())
            ->performedOn($instansi)
            ->log('Menambahkan data instansi dengan nama '.$request->nama);


        return redirect()->route('instansi.index')->with('insert', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instansi = Instansi::find(Crypt::decrypt($id));
        $datainstansi = Instansi::orderBy('created_at', 'DESC')->get();
        return view('admin.instansi.edit', compact('instansi', 'datainstansi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'nomenklatur_unit' => 'required',
        ]);

        $idinstansi = Crypt::decrypt($id);

        $instansi = Instansi::find($idinstansi);
        $instansi->update([
            'code' => $request->code,
            'nama_instansi' => $request->nama,
            'alamat' => $request->alamat,
            'parent_id' => $request->instansi_id == '' ? null : $request->instansi_id,
            'telp' => $request->telp,
            'email' => $request->email,
            'nomenklatur_unit' => $request->nomenklatur_unit,
        ]);

        activity("Mengubah Data Instansi")
            ->causedBy(auth()->user())
            ->performedOn($instansi)
            ->log('Mengubah data instansi dengan nama '.$request->nama);

        return redirect()->route('instansi.index')->with('update', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idinstansi = Crypt::decrypt($id);
        $instansi = Instansi::find($idinstansi);
        if($instansi->subinstansi->count() == 0 && $instansi->users->count() == 0){
            activity("Menghapus Data Instansi")
                ->causedBy(auth()->user())
                ->performedOn($instansi)
                ->log('Menghapus data instansi dengan nama '.$instansi->nama_instansi);
            $instansi->delete();
            return redirect()->route('instansi.index')->with('delete', 'Data berhasil dihapus');
        } else {
            return redirect()->route('instansi.index')->with('error', 'Data tidak dapat dihapus karena masih memiliki sub instansi');
        }
    }
}
