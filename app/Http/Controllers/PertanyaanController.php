<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pertanyaan;
use App\Models\KolomPertanyaan;
use Illuminate\Support\Facades\Crypt;
use App\Models\Jawaban;
use App\Models\KategoriPertanyaan;
use DataTables;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kolomPertanyaan = KolomPertanyaan::all();
        $kategoriPertanyaan = KategoriPertanyaan::all();
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', compact('kolomPertanyaan', 'pertanyaan', 'kategoriPertanyaan'));
    }

    public function getKolomPertanyaan(Request $request)
    {
        $kolomPertanyaan = KolomPertanyaan::where('kategoripertanyaan_id', $request->kategoripertanyaan_id)->get();
        $firstkolom = $kolomPertanyaan->first();
        $pertanyaan = Pertanyaan::orderBy('id', 'ASC')->get();
        return response()->json([
            'kolomPertanyaan' => $kolomPertanyaan,
            'firstkolom' => $firstkolom,
            'pertanyaan' => $pertanyaan
        ]);
    }

    public function getJawabanPertanyaan(Request $request)
    {
        $jawabanPertanyaan = Jawaban::where('surveyarsip_id', $request->id)->where('user_id', $request->user_id)->get();
        return response()->json([
            'jawabanPertanyaan' => $jawabanPertanyaan
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kolom_pertanyaan_id' => 'required|numeric',
            'kategori_pertanyaan_id' => 'required|numeric',
            'type' => 'required',
        ]);

        if($request->type == 'text'){
            $is_text = 1;
            $is_input = 0;
        } else {
            $is_text = 0;
            $is_input = 1;
        }

        $pertanyaan = Pertanyaan::create([
            'kolompertanyaan_id' => $request->kolom_pertanyaan_id,
            'kategoripertanyaan_id' => $request->kategori_pertanyaan_id,
            'text' => $request->text_pertanyaan,
            'is_rowspan' => $request->is_rowspan ? 1 : 0,
            'rowspan' => $request->jumlah_rowspan,
            'is_text' => $is_text,
            'is_input' => $is_input,
            'input_type' => $request->input_type,
        ]);

        return redirect()->route('pertanyaan.index')
                        ->with('insert','Pertanyaan created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
