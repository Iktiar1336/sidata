<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    //protected $redirectTo = '/redirect';

    protected function redirectTo()
    {
        return Redirect::session()->get('url.intended') ?? '/redirect';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:6',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        if(!session()->has('url.intended')){
            session(['url.intended' => url()->previous()]);
        }

        $remember_me  = ( !empty( $request->remember ) )? TRUE : FALSE;

        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if (auth()->attempt([$fieldType => $request->username, 'password' => $request->password], $remember_me)) {
            if (auth()->user()->hasRole('super-admin') || auth()->user()->hasRole('admin')) {
                activity('Login')
                        ->performedOn(auth()->user())
                        ->causedBy(auth()->user())
                        ->log(auth()->user()->name . ' Login Sebagai ' . auth()->user()->roles()->first()->description);    
                return Redirect::intended('Dashboard');
            } elseif(auth()->user()->hasRole('Eksekutif')){
                return Redirect::intended('Informasi-Eksekutif');
            } else {
                activity('Login')
                        ->performedOn(auth()->user())
                        ->causedBy(auth()->user())
                        ->log(auth()->user()->name . ' Login Sebagai ' . auth()->user()->roles()->first()->description);
                return Redirect::intended('home');
            }
        }
        return redirect()->back()->withErrors(['email' => 'These credentials do not match our records.']);
    }

    public function logout(Request $request)
    {
        activity('Logout')
                ->performedOn(auth()->user())
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Telah Logout');
        auth()->logout();
        return redirect('/');
    }
}
