<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\ClientRepository;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use DataTables;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = $clients = $request->user()->clients()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('id', function($row){
                        
                        return Crypt::encrypt($row->id);
                    })
                    ->addColumn('secret', function($row){
                        return Crypt::encrypt($row->secret);
                    })
                    ->addColumn('action', function($row){
                        return view('admin.settings.actionbtn', compact('row'));
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $clients = $request->user()->clients()->get();
        return view('admin.clients.index', compact('clients'));
    }

    public function store(Request $request, ClientRepository $clientRepository)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $client = $clientRepository->create(
            $request->user()->id,
            $request->name,
            env('APP_URL') . '/callback'
        );

        return redirect()->back()->with('create-apiclient-success', 'Client Created Successfully');

    }

    public function destroy(Request $request, $id, ClientRepository $clients)
    {
        $client = $clients->find(Crypt::decrypt($id));
        $client->delete();
        return redirect()->back()->with('delete-apiclient-success', 'Client Deleted Successfully');
    }
        
}
