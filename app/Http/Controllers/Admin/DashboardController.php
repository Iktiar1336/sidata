<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Http\Request;
use App\Models\Kinerja;
use App\Models\Category;
use App\Models\FileUpload;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Models\Instansi;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:Dashboard Admin|Dashboard Eksekutif');
    }
    
    public function index()
    {
        $year = date('Y');
        $users = User::all();
        $activities = Activity::select(\DB::raw("COUNT(*) as count"))->where('log_name', '==', 'login')->get()->pluck('count');
        $roles = Role::all();

        $datauser = [];
        $datatahun = [];
        foreach($roles as $role){
            $user = User::role($role->name)->get();
            foreach($user as $u){
                $datatahun[] = [
                    'name' => $u->name,
                    'data' => $u->getYearAttribute($u->created_at),
                ];
            }
            $datauser[] = [
                'name' => $role->description,
                'data' => $user->count(),
            ];
        }

        $collection = collect($datauser);
        $grouped = $collection->groupBy('name')->map(function ($item) {
                return $item->pluck('data');
        });

        $collectiontahun = collect($datatahun);
        foreach($grouped as $key => $value){
            $groupedbytahun = $collectiontahun->where('name', $key)->groupBy('data')->map(function ($item) {
                return $item->pluck('data');
            });
        }


        $categories = $groupedbytahun->keys()->toArray();
        $instansi = Instansi::paginate(5);
        return view('admin.dashboard.index', compact('activities','users', 'categories', 'grouped', 'year', 'instansi'));
    }

    public function eksekutif()
    {
        
        // return view('admin.dashboard.eksekutif', compact('years','dataterverifikasi', 'dataterproses', 'dataterkirim', 'datatersimpan'));
        $category = Category::all();
        $year = Kinerja::where('status', 4)->orderBy('year', 'asc')->get()->groupBy('year')->keys();
        return view('admin.dashboard.eksekutif', compact('category','year'));
    }

    public function getYear(Request $request)
    {
        $year = Kinerja::where('status', 4)->where('category_id', $request->category_id)->orderBy('year', 'asc')->get()->groupBy('year')->keys();
        return response()->json($year);
    }

    public function chart(Request $request)
    {
        $categoryid = $request->category_id;
        $category = Category::find($categoryid);
        $start_date = \Carbon\Carbon::createFromFormat('Y', $request->tahun_awal)->format('Y');
        $end_date = \Carbon\Carbon::createFromFormat('Y', $request->tahun_akhir)->format('Y');
        $year = Kinerja::where('status', 4)->where('category_id', $categoryid)->whereBetween('year', [$start_date, $end_date])->orderBy('year', 'ASC')->get()->groupBy('year');
        //dd($request->all());
        $data = array(); 
        $table = '<div class="table-responsive">'; 
        $table .= '<table class="table table-bordered table-striped table-hover">';
        $table .= '<thead>';
        $table .= '<tr>';
        $table .= '<th>Data Kinerja Unit</th>';
        $table .= '<th>Tahun</th>';
        $table .= '<th>Total</th>';
        // foreach($year as $key => $value){
        //     $table .= '<th>'.$key.'</th>';
        // }
        $table .= '<th>Keterangan</th>';
        $table .= '<th>File Pendukung</th>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        //dd($year);
        foreach ($year as $key => $value) {
            $kinerja = Kinerja::where('status', 4)->where('category_id', $categoryid)->where('year', $key)->get();
            foreach ($kinerja as $k) {
                $file = FileUpload::where('category_id', $categoryid)->where('year', $k->year)->get();
                if($file->count() > 0){
                    if($file->first()->filename == null){
                        $filename = '-';
                        $idfile = null;
                    } else {
                        $filename = $file->first()->filename;
                        $idfile = $file->first()->id;
                    }
                } else {
                    $filename = '-';
                    $idfile = null;
                }
                $data[] = [
                    'name'          => $k->subcategory->name,
                    'data'          => intval($k->total),
                    'satuan'        => $k->satuan,
                    'keterangan'    => $k->description,
                    'file'          => $filename,
                    'idfile'        => $idfile,
                    'year'          => $k->year,
                ];

                $subcategory[] = [
                    'id'   => $k->subcategory->id,
                    'name' => $k->subcategory->name,
                ];

                $tahun[] = [
                    'tahun' => $k->year,
                ];
            }
        }

        $collection = collect($data);
        $grouped = $collection->groupBy('name')->map(function ($item) {
                return $item->pluck('data');
        });

        //dd($grouped);

        $collectiontahun = collect($tahun);
        $groupedtahun = $collectiontahun->groupBy('tahun');

        $collection2 = collect($subcategory);
        $groupcategoryid = $collection2->groupBy('id')->map(function ($item) {
                return $item;
        });

        $groupcategoryname = $collection2->groupBy('name')->map(function ($item) {
                return $item;
        });

        $groupcategory = [
            'id' => $groupcategoryid,
            'name' => $groupcategoryname,
        ];

        foreach ($grouped as $key => $a) {
            $rowspan = $collection->where('name', $key)->count();
            $first = true;
            foreach ($collection->where('name', $key) as $b) {
                $table .= '<tr>';
                $totalrow = $collection->where('name', $key)->count();
                if($first && $totalrow > 1){
                    $table .= '<td rowspan='.$totalrow.'>'.$key.'</td>';
                    $first = false;
                } else {
                    if($totalrow == 1){
                        $table .= '<td>'.$key.'</td>';
                    }
                }
                $table .= '<td>'.$b['year'].'</td>';
                $table .= '<td>'.$b['data'].' '.$b['satuan'].'</td>';
                $table .= '<td>';
                $content = $b['keterangan'];
                $table .= '<button tabindex="0" type="button" class="btn btn-primary btn-sm" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom" data-html="true" data-content="'.$content.'"> <i class="fas fa-eye"></i> </button>';
                $table .= '</td>';
                if($b['file'] == '-'){
                    $table .= '<td>';
                    $table .= $b['file'];
                    $table .= '</td>';
                } else {
                    $table .= '<td>';
                    $table .= '<a href="'.route('download.file-pendukung', $b['idfile']).'">'.$b['file'].'</a>';
                    $table .= '</td>';
                }
                $table .= '</tr>';
            }
        }
        $table .= '</tbody>';
        $table .= '</table>';
        $table .= '</div>';

        $tableprediksi = '<table class="table table-bordered table-striped table-hover" id="table-kinerja">';
        $tableprediksi .= '<thead>';
        $tableprediksi .= '<tr>';
        $tableprediksi .= '<th>Data Kinerja Unit</th>';
        $tableprediksi .= '<th>Tahun</th>';
        $tableprediksi .= '<th>X</th>';
        $tableprediksi .= '<th>Y</th>';
        $tableprediksi .= '<th>XX</th>';
        $tableprediksi .= '<th>XY</th>';
        $tableprediksi .= '</tr>';
        $tableprediksi .= '</thead>';
        $tableprediksi .= '<tbody>';

        $selecttahun = '<select class="form-control" name="tahun" id="tahunprediksi">';
        $selecttahun .= '<option value="">Pilih Tahun</option>';
        for($i =1; $i<=10; $i++){
            $selecttahun .= '<option value="'.($i).'">'.($i).'</option>';
        }
        $selecttahun .= '</select>';
        
        return response()->json(
            [
                'category' => $value->first()->category->name,
                'sumber' => $value->first()->workunit->name,
                'satuan' => $value->first()->satuan,
                'series' => $grouped,
                'tahun' => $selecttahun,
                'groupcategoryid' => $groupcategoryid,
                'groupcategoryname' => $groupcategoryname,
                'table' => $table,
                'categories' => $year->keys()->toArray()
            ]
        );
    }

    public function prediksi(Request $request)
    {
        $data = Kinerja::where('status', 4)->where('category_id', $request->category_id)->where('sub_category_id', $request->subcategory_id)->whereBetween('year', [$request->tahunawal, $request->tahunakhir])->get();
        $x = 0;
        $jumlah_x = 0;
		$jumlah_y = 0;
		$jumlah_xx = 0;
		$jumlah_xy = 0;

        foreach($data as $d){
            $jumlah_x += $x;
            $jumlah_y += $d->total;
            $jumlah_xx += $x*$x;
            $jumlah_xy += $x*$d->total;
            $x++;

            $datasubcategory[] = [
                'name'          => $d->subcategory->name,
            ];
        }
        
        $rata2_x = $jumlah_x / $x;
        $rata2_y = $jumlah_y / $x;
        
        $b1 = ($jumlah_xy - (($jumlah_x * $jumlah_y) / $x)) / ($jumlah_xx - ($jumlah_x * $jumlah_x) / $x);
        
        $b0 = $rata2_y - ($b1 * $rata2_x);

        for($i = 1; $i <= $request->tahun; $i++){
            $tahun = $i;
            $thn = ($x - 1) + $i;
            $prediksi = $b0 + ($b1 * $thn);

            $dataprediksi[] = [
                'tahun'         => $request->tahunakhir + $tahun,
                'prediksi'      => $prediksi,
            ];
        }

        $collection = collect($datasubcategory)->groupBy('name')->map(function ($row) {
            return $row;
        });

        $collections = collect($dataprediksi);
        $series = $collections->map(function ($item) {
            return [
                'name' => $item['tahun'],
                'data' => [$item['prediksi']],
            ];
        });

        return response()->json(
            [
                'prediksi' => $series,
                'categories' => $collection->keys()->toArray(),
                'tahun' => $collections->toArray(),
                'b0' => $b0,
                'b1' => $b1,
            ]
        );

        //dd($dataprediksi);
    }

    public function handlechartuser()
    {
        $roles = Role::all();

        $datauser = [];
        $datatahun = [];
        foreach($roles as $role){
            $user = User::role($role->name)->get();
            foreach($user as $u){
                $datatahun[] = [
                    'name' => $u->name,
                    'data' => $u->getYearAttribute($u->created_at),
                ];
            }
            $datauser[] = [
                'name' => $role->description,
                'data' => $user->count(),
            ];
        }

        $collection = collect($datauser);
        $grouped = $collection->groupBy('name')->map(function ($item) {
                return $item->pluck('data');
        });

        $collectiontahun = collect($datatahun);
        foreach($grouped as $key => $value){
            $groupedbytahun = $collectiontahun->where('name', $key)->groupBy('data')->map(function ($item) {
                return $item->pluck('data');
            });
        }

        // dd($groupedbytahun);

        // print_r($user->toArray());
        $instansi = Instansi::with('arsipkolektor')->get();
        dd($instansi->toArray());
    }
}
