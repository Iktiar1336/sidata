<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EmailSetting;
use App\Mail\TestEmail;
use Illuminate\Support\Facades\Crypt;

class EmailSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emailSettings = EmailSetting::orderBy('created_at', 'desc')->get()->first();
        return view('admin.settings.index', compact('emailSettings'));
    }

    public function emailTest(Request $request)
    {
        $email = new TestEmail();
        try {
            \Mail::to($request->email)->send($email);
            return redirect()->back()->with('email-test-success', 'Email test sent successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('email-test-failed', 'Email test failed');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'driver' => 'required',
            'host' => 'required',
            'port' => 'required',
            'username' => 'required',
            'password' => 'required', 
            'encryption' => 'required',
            'from_address' => 'required',
        ]);

        config([
            'mail.mailers' => [
                $request->driver => [
                    'transport' => $request->driver,
                    'host' => $request->host,
                    'port' => $request->port,
                    'username' => $request->username,
                    'password' => $request->password,
                    'encryption' => $request->encryption,
                ]
            ],
            'mail.from' => [
                'address' => $request->from_address,
                'name' => 'Admin'
            ],
        ]);

        $emailSettings = EmailSetting::orderBy('created_at', 'desc')->get()->first();
        if ($emailSettings) {
            $emailSettings->update($request->all()); 
        } else {
            EmailSetting::create($request->all());
        }

        return redirect()->back()->with('setting-email-success', 'Email Setting berhasil diupdate');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'driver' => 'required',
            'host' => 'required',
            'port' => 'required',
            'username' => 'required',
            'password' => 'required',
            'encryption' => 'required',
            'email_pengirim' => 'required',
        ]);

        $path = base_path('.env');
        //dd($path);
        $test = file_get_contents($path);

        $driver = env('MAIL_MAILER');
        $host = env('MAIL_HOST');
        $port = env('MAIL_PORT');
        $username = env('MAIL_USERNAME');
        $password = env('MAIL_PASSWORD');
        $encryption = env('MAIL_ENCRYPTION');
        $email_pengirim = env('MAIL_FROM_ADDRESS');

        $test = str_replace("MAIL_MAILER=$driver", "MAIL_MAILER=$request->driver", $test);
        $test = str_replace("MAIL_HOST=$host", "MAIL_HOST=$request->host", $test);
        $test = str_replace("MAIL_PORT=$port", "MAIL_PORT=$request->port", $test);
        $test = str_replace("MAIL_USERNAME=$username", "MAIL_USERNAME=$request->username", $test);
        $test = str_replace("MAIL_PASSWORD=$password", "MAIL_PASSWORD=$request->password", $test);
        $test = str_replace("MAIL_ENCRYPTION=$encryption", "MAIL_ENCRYPTION=$request->encryption", $test);
        $test = str_replace("MAIL_FROM_ADDRESS=$email_pengirim", "MAIL_FROM_ADDRESS=$request->email_pengirim", $test);

        file_put_contents($path, $test);

        $emailSettings = EmailSetting::find($id);
        $emailSettings->update([
            'driver' => $request->driver,
            'host' => $request->host,
            'port' => $request->port,
            'username' => $request->username,
            'password' => Crypt::encryptString($request->password),
            'encryption' => $request->encryption,
            'from_address' => $request->email_pengirim,
        ]);

        return redirect()->back()->with('setting-email-success', 'Email Setting berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
