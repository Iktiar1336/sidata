<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ActivityExport;
use DataTables;
use App\Models\User;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:List Log Aktivitas|Export Log Aktivitas', ['only' => ['index','export']]);
        $this->middleware('role:super-admin|admin');
    }

    public function index(Request $request)
    {
        $roles = Role::all();
        $users = User::with('activity')->get();
        return view('admin.activity.index', compact('roles', 'users'));
    }

    public function export($id)
    {
        $role = Role::find($id);
        $users = User::role($role->name)->get();
        return Excel::download(new ActivityExport($users), 'Data Activity Log ' . $role->description . '.xlsx');
    }
}
