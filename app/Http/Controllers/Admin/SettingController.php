<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EmailSetting;
use App\Models\TelegramSetting;
use Illuminate\Support\Facades\Crypt;
use App\Models\AppSetting;
use DataTables;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        $emailSettings = EmailSetting::orderBy('created_at', 'desc')->get()->first();
        $telegramSettings = TelegramSetting::orderBy('created_at', 'desc')->get()->first();
        $clients = $request->user()->clients()->get();
        $settings = AppSetting::orderBy('created_at', 'desc')->get()->first();
        return view('admin.settings.index', compact('emailSettings', 'telegramSettings', 'clients', 'settings'));
    }

    public function updateApp(Request $request)
    {
        $request->validate([
            'nama_aplikasi' => 'required',
            'deskripsi_aplikasi' => 'required',
            'nama_instansi' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'fax' => 'required',
        ]);
        $settings = AppSetting::orderBy('created_at', 'desc')->get()->first();

        config(['app.name' => $request->nama_aplikasi]);
        
        $settings->update([
            'nama_aplikasi' => $request->nama_aplikasi,
            'deskripsi_aplikasi' => $request->deskripsi_aplikasi,
            'nama_instansi' => $request->nama_instansi,
            'alamat' => $request->alamat,
            'no_telp' => $request->no_telp,
            'email' => $request->email,
            'no_fax' => $request->fax,
        ]);

        return redirect()->back()->with('success', 'Pengaturan aplikasi berhasil diperbarui');
    }

    public function updateSocialMedia(Request $request)
    {
        $request->validate([
            'instagram' => 'required',
            'facebook' => 'required',
            'twitter' => 'required',
            'youtube' => 'required',
        ]);
        $settings = AppSetting::orderBy('created_at', 'desc')->get()->first();
        $settings->update($request->all());

        return redirect()->back()->with('success', 'Pengaturan media sosial berhasil diperbarui');
    }
}
