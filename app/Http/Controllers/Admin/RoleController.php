<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use DataTables;
use DB;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;


class RoleController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('permission:List Role|Tambah Role|Edit Role|Hapus Role', ['only' => ['index','store']]);
        $this->middleware('permission:Tambah Role', ['only' => ['create','store']]);
        $this->middleware('permission:Edit Role', ['only' => ['edit','update']]);
        $this->middleware('permission:Hapus Role', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Role::orderBy('created_at', 'DESC')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        if (auth()->user()->can('role-edit')) {
                            $btn = '<a href="'.route('roles.edit', Crypt::encrypt($row->id)).'" class="edit btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>';
                        }
                        if (auth()->user()->can('role-delete')) {
                            if ($row->name != 'super-admin' && $row->name != 'admin' && $row->name != 'Instansi' && $row->name != 'Eksekutif') {
                                $btn .= '<form action="'.route('roles.destroy', Crypt::encrypt($row->id)).'" method="POST" class="d-inline">
                                    '.csrf_field().'
                                    '.method_field('DELETE').
                                    '<button type="submit" class="btn btn-danger btn-sm delete"><i class="fas fa-trash"></i></button> ';

                                $btn .= '<script>
                                    $(".delete").on("click", function(e){
                                        event.preventDefault();
                                        const form = $(this).closest("form");
                                        swal({
                                            title: "Apakah anda yakin?",
                                            text: "Jenis User Ini Akan Dihapus Secara Permanen",
                                            icon: "warning",
                                            buttons: true,
                                            dangerMode: true,
                                        })
                                        .then((willDelete) => {
                                            if (willDelete) {
                                                form.submit();
                                            }
                                        });
                                    });';
                            }
                        }
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        $roles = Role::orderBy('created_at', 'DESC')->get();
        return view('admin.roles.index', compact('roles'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules = Permission::select('module')->distinct()->get()->toArray();
        return view('admin.roles.create', compact('modules'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'permissions' => 'required',
        ]);
        
        $role = Role::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        $role->syncPermissions($request->permissions);

        activity('Membuat Role Baru')
            ->performedOn($role)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Membuat satu role baru yaitu ' . $role->name);

        return redirect()->route('roles.index')->with('insert-role-success', 'Role created successfully');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roleid =  Crypt::decrypt($id);
        $role = Role::findOrFail($roleid);
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
            ->where("role_has_permissions.role_id", $roleid)
            ->get();

        return view('admin.roles.show', compact('role', 'rolePermissions'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleid =  Crypt::decrypt($id);
        $role = Role::findOrFail($roleid);
        $modules = Permission::select('module')->distinct()->orderBy('module')->get()->toArray();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $roleid)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();
        return view('admin.roles.edit', compact('role', 'modules', 'rolePermissions'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'permissions' => 'required',
        ]);
        
        $roleid =  Crypt::decrypt($id);
        $role = Role::findOrFail($roleid);
        $name = $role->description;
        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();

        activity('Mengupdate Role')
            ->performedOn(auth()->user())
            ->causedBy($role)
            ->log(auth()->user()->name . 'Mengupdate Jenis User ' . $name);

        $role->syncPermissions($request->permissions);
        
        return redirect()->route('roles.index')->with('update-role-success', 'Role updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $roleid =  Crypt::decrypt($id);
        $role = Role::findOrFail($roleid);
        if($role->users()->count() > 0){
            return redirect()->route('roles.index')->with('delete-role-failed', 'Role is associated with users');
        } else {
            activity('Menghapus Role')
                ->performedOn(auth()->user())
                ->causedBy($role)
                ->log(auth()->user()->name . ' Telah menghapus role ' . $role->description);
            $role->delete();
            return redirect()->route('roles.index')->with('delete-role-success', 'Role deleted successfully');
        }
    }
     
}
