<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use DB;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Crypt;
use App\Models\WorkUnit;
use Illuminate\Support\Str;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\Http;
use App\Mail\RegistrasiUser;
use Illuminate\Support\Facades\Mail;
use App\Models\Instansi;
use DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:List User|Tambah User|Edit User|Hapus User', ['only' => ['index','store']]);
        $this->middleware('permission:Tambah User', ['only' => ['create','store']]);
        $this->middleware('permission:Edit User', ['only' => ['edit','update']]);
        $this->middleware('permission:Hapus User', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::all();
        $role = Role::all();
        $workunit = WorkUnit::all();
        $instansi = Instansi::all();
        if($request->ajax()){
            $data = User::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('unitkerja', function($row){
                        if($row->workunit_id != null){
                            $workunit = $row->workunit->name;
                        }else if ($row->instansi_id != null){
                            $workunit = $row->instansi->nama_instansi;
                        } else {
                            $workunit = '-';
                        }

                        return $workunit;
                    })
                    ->addColumn('role', function($row){
                        $role = '<span class="badge">'.$row->roles->first()->description.'</span>';
                        return $role;
                    })
                    ->addColumn('action', function($row){
                        return view('admin.users.action', compact('row'));
                    })
                    ->rawColumns(['action', 'role'])
                    ->make(true);
        }
        return view('admin.users.index', compact('users', 'workunit', 'role', 'instansi'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        return view('admin.users.create', compact('roles'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required|string|max:255|unique:users',
            'name' => 'required|string|max:255',
            'workunit_id' => 'nullable|integer',
            'telegram_id' => 'nullable|integer',
            'email' => 'required|string|email|max:255',
            'role_id' => 'required|integer',
        ]);

        $password = Str::random(8);
        $email = $request->email;

        if($request->role_id != 2 && $request->role_id != 3 && $request->role_id != 4){
            $telegram = Http::get('https://api.telegram.org/bot'.env('TELEGRAM_BOT_TOKEN').'/getChatMember?chat_id='.$request->telegram_id.'&user_id='.$request->telegram_id);
            $cektelegram = json_decode($telegram);

            if ($cektelegram->ok == true) {
                $user = User::create([
                    'username' => $request->username,
                    'name' => $request->name,
                    'workunit_id' => $request->workunit_id,
                    'telegram_id' => $request->telegram_id,
                    'email' => $request->email,
                    'password' => Hash::make($password),
                ]);
        
                $role = Role::findById($request->role_id);
                $user->assignRole($role);
        
                $roleUser = $user->roles->first()->description;
                $workunitUser = $user->workunit->name;
                $chat_id = $user->telegram_id;
                $name = $user->name;
        
                $text = "Hai <a href='tg://user?id=$chat_id'><b>$name</b></a>,\n\n";
                $text .= "Selamat datang di aplikasi SIDATA ( Sistem Informasi Pengumpulan Data ). Kami telah mendaftarkan akun anda, berikut adalah detail akun anda : \n\n";
                
                $text .= "Nama Lengkap : $user->name \n";
                $text .= "Username     : $user->username \n";
                $text .= "Email     : $user->email \n";
                $text .= "Password  : $password \n";
                $text .= "Jenis User : $roleUser \n";
                $text .= "Unit Kerja : $workunitUser \n\n";
                // $text .= "Link      : http://localhost:8000/login \n\n";
                $text .= "Silahkan login dan lakukan verifikasi akun anda pada aplikasi SIDATA. \n";
                $text .= "<a href='".env('APP_URL')."'>Link Login</a> \n\n";
                $text .= "Terima kasih,\n\n";
                $text .= "Pusat Data & Informasi\n";
                $text .= "Arsip Nasional Republik Indonesia";
        
                Telegram::sendMessage([
                    'chat_id' => $chat_id,
                    'parse_mode' => 'HTML',
                    'text' => $text,
                ]);
        
                activity('Menambahkan User Baru')
                    ->performedOn($user)
                    ->causedBy(auth()->user())
                    ->log(auth()->user()->name .' Menambahkan User: '.$user->name . ' Dengan Role : '.$role->description);
                
                return redirect()->route('users.index')->with('insert-user-success', 'User created successfully');
            } else {
                return redirect()->back()
                    ->with('error', 'Telegram ID tidak valid');
            }
        } else if($request->role_id == 2){
            $user = User::create([
                'username' => $request->username,
                'name' => $request->name,
                'instansi_id' => $request->instansi_id,
                'email' => $request->email,
                'password' => Hash::make($password),
            ]);
        } else if($request->role_id == 3){
            $user = User::create([
                'username' => $request->username,
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($password),
            ]);
        } else {
            $user = User::create([
                'username' => $request->username,
                'name' => $request->name,
                'workunit_id' => $request->workunit_id,
                'telegram_id' => $request->telegram_id,
                'email' => $request->email,
                'password' => Hash::make($password),
            ]);
        }

        $role = Role::findById($request->role_id);
        $user->assignRole($role);
    
        Mail::to($email)->send(new RegistrasiUser($user, $password));

        activity('Menambahkan User Baru')
            ->performedOn($user)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name .' Menambahkan User: '.$user->name . ' Dengan Role : '.$role->description);

        return redirect()->route('users.index')->with('insert-user-success', 'User created successfully');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userid = Crypt::decrypt($id);
        $user = User::findOrFail($userid);
        $activity = Activity::all();
        return view('admin.users.show', compact('user', 'activity'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userid = Crypt::decrypt($id);
        $user = User::findOrFail($userid);
        $workunit = WorkUnit::all();
        $role = Role::all();
        $userRole = $user->roles->first();
        $instansi = Instansi::all();
        return view('admin.users.edit', compact('user', 'workunit', 'role', 'userRole', 'instansi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'nullable|string|max:255',
            'email' => 'nullable|string|email|max:255|',
            'password' => 'nullable|string|min:6|confirmed',
            'role_id' => 'required',
        ]);
        $unitkerja = WorkUnit::findOrFail($request->workunit_id);
        $role = Role::findById($request->role_id);

        $userid = Crypt::decrypt($id);
        $user = User::findOrFail($userid);
        $username = $user->name;
        $userchange = auth()->user()->name;
        $chat_id = $user->telegram_id;
        $tanggal = date('d-m-Y');
        $waktu = date('H:i:s');
        $user->workunit_id = $request->workunit_id;
        $user->instansi_id = $request->instansi_id;
        $user->save();
        if ($request->password) {
            $user->password = Hash::make($request->password);
            $user->save();
            $text = "Halo <a href='tg://user?id=$chat_id'><b>$username</b></a>, \n\n";
            $text .= "Data diri anda telah di update oleh $userchange pada aplikasi <b>SIDATA</b>,";
            $text .= "Berikut detailnya : \n\n";
            $text .= "<b>Tanggal</b>        : $tanggal \n";
            $text .= "<b>Waktu</b>      : $waktu \n";
            $text .= "<b>Unit Kerja</b>     : $unitkerja->name \n";
            $text .= "<b>Role</b>       : $role->description \n\n";
            $text .= "<b>Password</b>       : $request->password \n\n";
            $text .= "Silahkan login pada aplikasi SIDATA untuk melihat detailnya. \n\n";
            $text .= "Terima Kasih.";
            
            if($chat_id != null){
                Telegram::sendMessage([
                    'chat_id' => $chat_id,
                    'parse_mode' => 'HTML',
                    'text' => $text
                ]);
            }
        }

        $user->syncRoles($role);

        activity('Mengupdate User')
            ->performedOn($user)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name .' Mengupdate Profile User: '.$user->name);

        $text = "Halo <a href='tg://user?id=$chat_id'><b>$username</b></a>, \n\n";
        $text .= "Data diri anda telah di update oleh $userchange pada aplikasi <b>SIDATA</b>,";
        $text .= "Berikut detailnya : \n\n";
        $text .= "<b>Tanggal</b>        : $tanggal \n";
        $text .= "<b>Waktu</b>      : $waktu \n";
        $text .= "<b>Unit Kerja</b>     : $unitkerja->name \n";
        $text .= "<b>Role</b>       : $role->description \n\n";
        $text .= "Silahkan login pada aplikasi SIDATA untuk melihat detailnya. \n\n";
        $text .= "Terima Kasih.";

        if($chat_id != null){
            Telegram::sendMessage([
                'chat_id' => $chat_id,
                'parse_mode' => 'HTML',
                'text' => $text
            ]);
        }

        return redirect()->route('users.index')->with('update-user-success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $userid = Crypt::decrypt($id);
        $user = User::findOrFail($userid);
        if($user->produsendata()->count() > 0 || $user->masterdata()->count() > 0 || $user->masterdata()->count() > 0 || $user->messagesender()->count() > 0 || $user->messagereceiver()->count() > 0 || $user->berita()->count() > 0){
            return redirect()->route('users.index')->with('delete-user-failed', 'User tidak dapat dihapus karena masih memiliki data');
        }else{
            activity('Menghapus User')
                ->performedOn($user)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name .' Menghapus User: '.$user->name);
            $user->delete();
            return redirect()->route('users.index')->with('delete-user-success', 'User deleted successfully');
        }
    }
}
