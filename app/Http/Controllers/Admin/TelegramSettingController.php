<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\TelegramSetting;
use Illuminate\Support\Facades\Crypt;

class TelegramSettingController extends Controller
{
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'telegram_bot_token' => 'required',
        ]);

        try {
            $response = Telegram::getMe();
            $botId = $response->getId();
            $botUsername = $response->getUsername();
            config([
                'telegram.bots' => [
                    'mybot' => [
                        'username' => $botUsername,
                        'token' => $request->telegram_bot_token,
                    ],
                ],
            ]);  
            $telegram = TelegramSetting::find(Crypt::decrypt($id));
            $telegram->username_bot = $botUsername;
            $telegram->telegram_bot_token = Crypt::encryptString($request->telegram_bot_token);
            $telegram->save();

            return redirect()->back()->with('setting-telegram-success', 'Telegram bot token updated successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('setting-telegram-failed', 'Telegram bot token updated failed');
        }


        //dd(config('telegram.bots.mybot'));
    }
}
