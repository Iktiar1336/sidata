<?php

namespace App\Http\Controllers\ArsipKolektor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SurveyArsip;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = SurveyArsip::all();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                        if ($row->status == 1) {
                            $status = '<span class="badge badge-success">Aktif</span>';
                        } else {
                            $status = '<span class="badge badge-danger">Tidak Aktif</span>';
                        }
                        return $status;
                    })
                    ->addColumn('action', function($row){
                        $btn = '<form action="'.route('survey.destroy', Crypt::encrypt($row->id)).'" method="POST">
                                    '.csrf_field().'
                                    '.method_field('DELETE').'
                                    <a href="'.route('survey.edit', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                </form>';
                        return $btn;
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
        }
        return view('arsip-kolektor.survey.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'tahun' => 'required|numeric|digits:4|unique:survey_arsip,tahun',
            'status' => 'required',
        ]);

        $ceksurvey = SurveyArsip::where('status', 1)->get();
        if ($ceksurvey->count() > 0 && $request->status == 1) {
            return redirect()->back()->with('insert-failed', 'Data sudah ada');
        } else {
            $survey = SurveyArsip::create([
                'nama' => $request->nama,
                'tahun' => $request->tahun,
                'status' => $request->status,
            ]);
    
            return redirect()->back()->with('insert', 'Data berhasil ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        return view('arsip-kolektor.survey.edit', compact('survey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'tahun' => 'required|numeric|digits:4|unique:survey_arsip,tahun,'.Crypt::decrypt($id),
            'status' => 'required',
        ]);

        $ceksurvey = SurveyArsip::where('status', 1)->get();
        if ($ceksurvey->count() > 0 && $request->status == 1) {
            return redirect()->back()->with('update-failed', 'Data sudah ada');
        } else {
            $survey = SurveyArsip::find(Crypt::decrypt($id));
            $survey->nama = $request->nama;
            $survey->tahun = $request->tahun;
            $survey->status = $request->status;
            $survey->save();
    
            return redirect()->route('survey.index')->with('update', 'Data berhasil diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        $survey->delete();

        return redirect()->back()->with('delete', 'Data berhasil dihapus');
    }
}
