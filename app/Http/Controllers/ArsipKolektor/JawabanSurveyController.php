<?php

namespace App\Http\Controllers\ArsipKolektor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jawaban;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\SurveyArsip;
use Storage;
use File;

class JawabanSurveyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Isi Survey Arsip Kolektor|Detail Jawaban Survey Arsip Kolektor|Edit Jawaban Survey Arsip Kolektor');
        $this->middleware('permission:Isi Survey Arsip Kolektor', ['only' => ['store']]);
        $this->middleware('permission:Edit Jawaban Survey Arsip Kolektor', ['only' => ['edit', 'update']]);
        $this->middleware('permission:Detail Jawaban Survey Arsip Kolektor', ['only' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id) 
    {
        $this->validate($request, [
            'user_id' => 'required',
            'khasanaharsip_a1h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a2h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a3h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a4h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a5h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
        ]);

        //dd($request->all());

        $user = User::find($request->user_id);

        $survey = SurveyArsip::find(Crypt::decrypt($id));
        $input = $request->all();

        $jawaban = Jawaban::create($request->all());

        if($request->hasFile('khasanaharsip_a1h')){
            $file = $request->file('khasanaharsip_a1h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a1h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a2h')){
            $file = $request->file('khasanaharsip_a2h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a2h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a3h')){
            $file = $request->file('khasanaharsip_a3h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a3h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a4h')){
            $file = $request->file('khasanaharsip_a4h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a4h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a5h')){
            $file = $request->file('khasanaharsip_a5h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a5h'] = Crypt::encryptString($name);
        }

        activity("Mengisi Survey")
            ->causedBy($user)
            ->performedOn($jawaban)
            ->log($user->name . " Mengisi Survey " . $survey->judul . " Tahun " . $survey->tahun);

        
        return redirect()->route('arsip.import', Crypt::encrypt($survey->id))->with('success', 'Survey Berhasil Diisi');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'unitkerja' => 'required',
            'instansi' => 'required',
            'khasanaharsip_a1h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a2h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a3h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a4h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a5h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
        ]);

        // dd($request->all());

        $user = User::find($request->user_id);

        $survey = SurveyArsip::find(Crypt::decrypt($id));

        $jawaban = Jawaban::find(Crypt::decrypt($request->jawaban_id));
        $input = $request->all();
        $input['surveyarsip_id'] = $survey->id;

        if($request->hasFile('khasanaharsip_a1h')){
            $file = $request->file('khasanaharsip_a1h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a1h'] = Crypt::encryptString($name);
        }

        //dd(Storage::disk('public')->get('/arsip-kolektor/file-uploads/'));

        if($request->hasFile('khasanaharsip_a2h')){
            $file = $request->file('khasanaharsip_a2h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a2h'] = Crypt::encryptString($name);

        }

        if($request->hasFile('khasanaharsip_a3h')){
            $file = $request->file('khasanaharsip_a3h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a3h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a4h')){
            $file = $request->file('khasanaharsip_a4h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a4h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a5h')){
            $file = $request->file('khasanaharsip_a5h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a5h'] = Crypt::encryptString($name);
        }

        if ($jawaban->khasanaharsip_a1h != "") {
            //dd(Storage::disk('sftp')->getDriver()->getAdapter()->applyPathPrefix('arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a1h)));
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a1h)));
        } elseif($jawaban->khasanaharsip_a2h != "") {
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a2h)));
        } elseif($jawaban->khasanaharsip_a3h != "") {
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a3h)));
        } elseif($jawaban->khasanaharsip_a4h != "") {
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a4h)));
        } elseif($jawaban->khasanaharsip_a5h != "") {
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a5h)));
        }

        // $jawaban->update($input);

        try {
            $jawaban->update($input);

            //dd($jawaban);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        

        activity("Mengubah Jawaban Survey")
            ->causedBy($user)
            ->performedOn($jawaban)
            ->log($user->name . " Mengubah Jawaban Survey " . $survey->judul . " Tahun " . $survey->tahun);

        
        return redirect()->route('arsip-kolektor.index')->with('success', 'Jawaban Berhasil Diubah');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
