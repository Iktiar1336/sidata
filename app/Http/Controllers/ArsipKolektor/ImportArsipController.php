<?php

namespace App\Http\Controllers\ArsipKolektor;

use App\Models\JenisArsip;
use App\Models\SurveyArsip;
use Illuminate\Http\Request;
use App\Imports\ArsipKolektorImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Instansi;
use App\Http\Controllers\Controller;

class ImportArsipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Import Arsip Kolektor', ['only' => ['index', 'store']]);
    }

    public function index()
    {
        $jenisarsip = JenisArsip::all();
        $intansi = Instansi::all();
        return view('arsip-kolektor.import-arsip.index', compact('jenisarsip', 'intansi'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xlsx,xls,csv|max:2048',
            'jenis' => 'required',
            'jenisarsip_id' => 'required',
            'instansi_id' => 'required',
        ]);

        $file = $request->file('file');
        $nama_file = $file->getClientOriginalName();

        $user_id = auth()->user()->id;
        $instansi_id = $request->instansi_id; 

        try {
            $data = Excel::import(new ArsipKolektorImport($user_id, $nama_file, $request->jenis, $instansi_id, $request->jenisarsip_id), request()->file('file'));
            return redirect()->back()->with('import-success', $request->jenis . " berhasil diimport");
        } catch (\Exception $e) {
            //return redirect()->back()->with('import-failed', $e->getMessage() . $request->jenis . " gagal diimport , pastikan format file sesuai dengan template");
            return redirect()->back()->with('import-failed', $e->getMessage() . ' '. $request->jenis . " gagal diimport , pastikan format file sesuai dengan template");
        }
    }
}
