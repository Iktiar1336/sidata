<?php

namespace App\Http\Controllers\ArsipKolektor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JenisArsip;
use App\Models\ArsipKolektor;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class JenisArsipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = JenisArsip::all();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                        return view('arsip-kolektor.jenis-arsip.actionbtn', compact('row'));

                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('arsip-kolektor.jenis-arsip.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $jenis_arsip = JenisArsip::create($request->all());
        return redirect()->route('jenis-arsip.index')->with('insert', 'Jenis Arsip berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenisarsip = JenisArsip::find(Crypt::decrypt($id));
        return view('arsip-kolektor.jenis-arsip.edit', compact('jenisarsip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $jenisarsip = JenisArsip::find(Crypt::decrypt($id));
        $jenisarsip->update($request->all());
        return redirect()->route('jenis-arsip.index')->with('update', 'Jenis Arsip berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenisarsip = JenisArsip::find(Crypt::decrypt($id));
        if ($jenisarsip->ArsipKolektor->count() > 0) {
            return redirect()->route('jenis-arsip.index')->with('delete-failed', 'Jenis Arsip tidak dapat dihapus karena masih memiliki data');
        } else {
            $jenisarsip->delete();
            return redirect()->route('jenis-arsip.index')->with('delete-success', 'Jenis Arsip berhasil dihapus');
        }
    }
}
