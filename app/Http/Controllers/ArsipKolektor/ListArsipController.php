<?php

namespace App\Http\Controllers\ArsipKolektor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArsipDigital;
use App\Models\ArsipKolektor;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class ListArsipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:List Arsip Kolektor|Edit Arsip Kolektor|Delete Arsip Kolektor|Detail Arsip Kolektor', ['only' => ['index']]);
        $this->middleware('permission:Edit Arsip Kolektor', ['only' => ['edit']]);
        $this->middleware('permission:Delete Arsip Kolektor', ['only' => ['destroy']]);
        $this->middleware('permission:Detail Arsip Kolektor', ['only' => ['show']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(auth()->user()->roles()->first()->name == 'Instansi'){
            $data = ArsipKolektor::where('instansi_id', auth()->user()->instansi_id)->get();
        }else{
            $data = ArsipKolektor::orderBy('created_at', 'DESC')->get();
        }
        if ($request->ajax()) {
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('tanggal_surat', function($row){
                        $tanggal = \Carbon\Carbon::parse($row->tanggal_surat);
                        $time = $tanggal->format('H:i:s');

                        return $tanggal->isoFormat('dddd, D MMMM Y');
                    })
                    ->addColumn('action', function($row){
   
                            $btn = '<form action="'.route('list-arsip.destroy', Crypt::encrypt($row->id)).'" method="POST">';
                            $btn .= csrf_field();
                            $btn .= method_field('DELETE');
                            $btn .= '<a href="'.route('list-arsip.show', Crypt::encrypt($row->id)).'" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>';
                            $btn .= '<a href="'.route('list-arsip.edit', Crypt::encrypt($row->id)).'" class="btn btn-warning btn-sm mr-2 ml-2"><i class="fas fa-edit"></i></a>';
                            $btn .= '<button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('arsip-kolektor.list-arsip.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function upload(Request $request, $id)
    {
        if($request->hasFile('file')){
            $files = $request->file('file');
            
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $arsipkolektor = ArsipKolektor::find($id);
                $folder = uniqid('arsip-digital-', true);
                Storage::disk('public')->putFileAs('/arsip-digital/tmp/' . $folder, $file, $filename);
                $arsipdigital = ArsipDigital::create([
                    'arsipkolektor_id' => $id,
                    'user_id' => auth()->user()->id,
                    'folder' => $folder,
                    'type' => $file->getClientOriginalExtension(),
                    'file' => $filename,
                    'instansi_id' => $arsipkolektor->instansi_id,
                ]);

                return $folder;
            }

            return '';

        }
        
    }

    public function list($id)
    {
        $arsipdigital = ArsipDigital::where('arsipkolektor_id', Crypt::decrypt($id))->get();
        $data = [];
        foreach($arsipdigital as $arsip){
            $bytes = Storage::disk('public')->size('arsip-digital/tmp/'.$arsip->folder.'/'.$arsip->file);
            if ($bytes >= 1073741824)
            {
                $bytes = number_format($bytes / 1073741824, 2) . ' GB';
            } elseif ($bytes >= 1048576)
            {
                $bytes = number_format($bytes / 1048576, 2) . ' MB';
            } elseif ($bytes >= 1024)
            {
                $bytes = number_format($bytes / 1024, 2) . ' KB';
            } elseif ($bytes > 1)
            {
                $bytes = $bytes . ' bytes';
            } elseif ($bytes == 1)
            {
                $bytes = $bytes . ' byte';
            } else
            {
                $bytes = '0 bytes';
            }
            $img = $arsip->GetimageByType($arsip->type);
            $data[] = [
                'img' => $img,
                'name' => $arsip->file,
                'size' => $bytes,
                'url' => route('list-arsip-digital.download', $arsip->id),
                'type' => $arsip->type,
                'deleteUrl' => route('list-arsip-digital.hapus', Crypt::encrypt($arsip->id)),
                'deleteType' => 'DELETE',
            ];
        }

        return response()->json($data);

    }

    public function deleteFile(Request $request, $id)
    {
        $file = ArsipDigital::find(Crypt::decrypt($id));
        if($file){
            Storage::disk('public')->delete('arsip-digital/tmp/'.$file->folder.'/'.$file->file);
            Storage::disk('public')->deleteDirectory('arsip-digital/tmp/'.$file->folder);
            $file->delete();

            return response()->json(['success' => true]);
        }
    }

    public function download($id)
    {
        $file = ArsipDigital::find($id);
        $path = Storage::disk('public')->get('arsip-digital/tmp/'.$file->folder.'/'.$file->file);
        $filename = $file->file;
        $mime = Storage::disk('public')->mimeType('arsip-digital/tmp/'.$file->folder.'/'.$file->file);
        $headers = array(
            'Content-Type' => $mime,
        );
        return response($path, 200, $headers)->header('Content-Disposition', 'attachment; filename="'.$filename.'"');
    }

    public function delete(Request $request,$id)
    {
        $file = ArsipDigital::where('arsipkolektor_id', $id)->where('folder', request()->getContent())->first();
        if($file){
            Storage::disk('public')->delete('arsip-digital/tmp/'.$file->folder.'/'.$file->file);
            Storage::disk('public')->deleteDirectory('arsip-digital/tmp/'.$file->folder);
            $file->delete();

            return response()->json([
                'success' => true
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arsip = ArsipKolektor::find(Crypt::decrypt($id));
        $arsipdigital = ArsipDigital::where('arsipkolektor_id', $arsip->id)->get();
        return view('arsip-kolektor.list-arsip.detail', compact('arsip', 'arsipdigital'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arsip = ArsipKolektor::find(Crypt::decrypt($id));
        $arsipdigital = ArsipDigital::where('arsipkolektor_id', $arsip->id)->get();
        return view('arsip-kolektor.list-arsip.edit', compact('arsip', 'arsipdigital'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $arsip = ArsipKolektor::find(Crypt::decrypt($id));
        $arsip->update($request->all());
        return redirect()->route('list-arsip.index')->with('success', 'Data berhasil diubah');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arsip = ArsipKolektor::find(Crypt::decrypt($id));
        if($arsip->arsipdigital->count() > 0){
            return redirect()->route('list-arsip.index')->with('error', 'Arsip tidak bisa dihapus karena masih ada data arsip digital');
        } else {
            $arsip->delete();
            return redirect()->route('list-arsip.index')->with('success', 'Arsip berhasil dihapus');
        }
    }
}
