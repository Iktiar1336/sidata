<?php

namespace App\Http\Controllers\ArsipKolektor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArsipKolektor;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ArsipKolektorImport;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\Models\Instansi;
use App\Models\SurveyArsip;
use App\Models\Jawaban;
use Illuminate\Support\Facades\Auth;
use App\Models\KategoriPertanyaan;
use App\Models\Pertanyaan;
use App\Models\JenisArsip;
use App\Models\User;

class ArsipKolektorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:List Survey Arsip Kolektor|Isi Survey Arsip Kolektor|Detail Jawaban Survey Arsip Kolektor|Edit Jawaban Survey Arsip Kolektor');
        $this->middleware('permission:List Survey Arsip Kolektor', ['only' => ['index']]);
        $this->middleware('permission:Import Arsip Kolektor', ['only' => ['import', 'importStore']]);
        $this->middleware('permission:Detail Jawaban Survey Arsip Kolektor', ['only' => ['detail']]);
        $this->middleware('permission:Isi Survey Arsip Kolektor', ['only' => ['only', 'show']]);
        $this->middleware('permission:Edit Jawaban Survey Arsip Kolektor', ['only' => ['edit']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arsip_kolektor = ArsipKolektor::orderBy('created_at', 'DESC')->get();
        $instansi = Instansi::orderBy('created_at', 'DESC')->get();
        if($request->ajax()){
            $data = SurveyArsip::orderBy('created_at', 'DESC')->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('nama', function($row){
                        $nama = $row->nama . ' Tahun ' . $row->tahun;
                        return $nama;
                    })
                    ->addColumn('status', function($row){
                        $jawaban = Jawaban::where('surveyarsip_id', $row->id)->where('user_id', Auth::user()->id)->get()->first();
                        if($row->status == 1){
                            if (auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin') {
                                if ($jawaban) {
                                    $status = '<span class="badge badge-success">Sudah Mengisi</span>';
                                } else {
                                    $status = '<span class="badge badge-danger">Belum Mengisi</span>';
                                }
                            } else {
                                $status = '<span class="badge badge-success">Aktif</span>';
                            }
                            
                        } else {
                            $status = '<span class="badge badge-danger">Tidak Aktif</span>';
                        }
                        return $status;
                    })
                    ->addColumn('action', function($row){
                        $jawaban = Jawaban::where('surveyarsip_id', $row->id)->where('user_id', Auth::user()->id)->get()->first();
                        $btn = '';
                        if($row->status == 1){
                            if(auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin'){
                                if ($jawaban) {
                                    $btn .= '<div class = "btn-group">';
                                    $btn .= '<a href="'.route('arsip-kolektor.edit', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i> Edit Jawaban</a>';
                                    $btn .= '<a href="'.route('arsip.import', Crypt::encrypt($row->id)).'" class="btn btn-sm ml-2 mr-2 btn-primary"><i class="fas fa-file-import"></i> Import Excel</a>';
                                    $btn .= '<form action="'.route('arsip.detail', Crypt::encrypt($row->id)).'" method="POST">';
                                    $btn .= csrf_field();
                                    $btn .= '<input type="hidden" name="user_id" value="'.$jawaban->user_id.'">';
                                    $btn .= '<button type="submit" class="btn btn-sm btn-info"><i class="fas fa-eye"></i> Detail</button>';
                                    $btn .= '</form>';
                                    $btn .= '</div>';
                                } else {
                                    $btn .= '<a href="'.route('arsip-kolektor.show', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i> Isi Survey</a>';   
                                    $btn .= '<a href="'.route('arsip.import', Crypt::encrypt($row->id)).'" class="btn btn-sm ml-2 btn-primary"><i class="fas fa-file-import"></i> Import Excel</a>';
                                }
                            } else {
                                $btn .= '<a href="'.route('arsip.listresponden', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i> Detail</a>';
                            }
                        } else {
                            if(auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin'){
                                if ($jawaban) {
                                    $btn .= '<form action="'.route('arsip.detail', Crypt::encrypt($row->id)).'" method="POST">';
                                    $btn .= csrf_field();
                                    $btn .= '<input type="hidden" name="user_id" value="'.$jawaban->user_id.'">';
                                    $btn .= '<button type="submit" class="btn btn-sm btn-info"><i class="fas fa-eye"></i> Detail</button>';
                                    $btn .= '</form>';
                                } else {
                                    $btn .= '-';   
                                }
                            } else {
                                $btn .= '<a href="'.route('arsip.listresponden', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i> Detail</a>';
                            }
                                
                        }
                       
                        return $btn;
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
        }
        return view('arsip-kolektor.index', compact('arsip_kolektor', 'instansi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function downloadFile($filename)
    {
        $namafile = Crypt::decryptString($filename);
        $file = Storage::disk('public')->get('/arsip-kolektor/file-uploads/' .  Crypt::decryptString($filename));
        $mime = Storage::disk('public')->mimeType('/arsip-kolektor/file-uploads/' . $namafile);
        $headers = array(
            'Content-Type' => $mime,
        );

        //dd($headers);
        
        return response($file, 200, $headers);
    }

    public function listresponden(Request $request, $id)
    {
        if($request->ajax()){
            $data = Jawaban::where('surveyarsip_id', $id)->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('nama', function($row){
                        $nama = $row->user->name;
                        return $nama;
                    })
                    ->addColumn('instansi', function($row){
                        $instansi = $row->user->instansi->nama_instansi;
                        return $instansi;
                    })
                    ->addColumn('waktu_pengisian', function($row){
                        $tanggal = \Carbon\Carbon::parse($row->created_at);
                        $time = $tanggal->format('H:i:s');

                        return $tanggal->isoFormat('dddd, D MMMM Y') . ' ' . $time;
                        
                    })
                    ->addColumn('update_at', function($row){
                        $update_at = $row->updated_at->format('d-m-Y H:i:s');
                        return $update_at;
                    })
                    ->addColumn('action', function($row){
                        $btn = '<form action="'.route('arsip.detail', Crypt::encrypt($row->surveyarsip_id)).'" method="POST">';
                        $btn .= csrf_field();
                        $btn .= '<input type="hidden" name="user_id" value="'.$row->user_id.'">';
                        $btn .= '<button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Lihat Jawaban</button>';
                        $btn .= '</form>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        $survey = SurveyArsip::find(Crypt::decrypt($id));

        return view('arsip-kolektor.list-responden')->with(compact('survey'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'instansi_id' => 'required',
            'file' => 'required|mimes:xlsx,xls,csv|max:2048'
        ]);

        $file = $request->file('file');
        $nama_file = $file->getClientOriginalName();

        try {
            $data = Excel::import(new ArsipKolektorImport($request->instansi_id, $nama_file), request()->file('file'));
            return redirect()->route('arsip-kolektor.index')->with('success', 'Data berhasil diimport');
        } catch (\Exception $e) {
            return redirect()->route('arsip-kolektor.index')->with('import-failed', 'Data gagal diimport');
        }
         
    }

    public function download()
    {
        $file = public_path()."/format/Template-Format-Import.xlsx";
        $file_name = "Template-Format-Import.xlsx";
        $headers = array(
            'Content-Type: application/xlsx',
        );
        return response()->download($file, $file_name, $headers);
    }

    public function import($id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        $jenisarsip = JenisArsip::all();
        return view('arsip-kolektor.import-file', compact('survey', 'jenisarsip'));
    }

    public function importStore(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xlsx,xls,csv|max:2048',
            'jenis' => 'required',
            'jenisarsip_id' => 'required',
        ]);

        //dd($request->all());

        $file = $request->file('file');
        $nama_file = $file->getClientOriginalName();

        $user_id = auth()->user()->id;
        $instansi_id = auth()->user()->instansi_id; 

        try {
            $data = Excel::import(new ArsipKolektorImport($user_id, $nama_file, $request->jenis, $instansi_id, $request->jenisarsip_id), request()->file('file'));
            return redirect()->back()->with('import-success', $request->jenis . " berhasil diimport");
        } catch (\Exception $e) {
            //return redirect()->back()->with('import-failed', $e->getMessage() . $request->jenis . " gagal diimport , pastikan format file sesuai dengan template");
            return redirect()->back()->with('import-failed', $e->getMessage() . ' '. $request->jenis . " gagal diimport , pastikan format file sesuai dengan template");
        }
    }

    public function detail(Request $request, $id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        $jawaban = Jawaban::where('surveyarsip_id', $survey->id)->where('user_id', $request->user_id)->get()->first();
        $user = User::find($request->user_id);
        return view('arsip-kolektor.detail', compact('survey', 'jawaban', 'user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        //$pertanyaan = Pertanyaan::all();
        //$kategoriPertanyaan = KategoriPertanyaan::orderBy('created_at', 'ASC')->get();
        return view('arsip-kolektor.isi-survey', compact('survey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        $jawaban = Jawaban::where('surveyarsip_id', Crypt::decrypt($id))->where('user_id', Auth::user()->id)->get()->first();
        //$kategoriPertanyaan = KategoriPertanyaan::all();
        return view('arsip-kolektor.edit-survey', compact('jawaban','survey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'instansi_id' => 'required',
            'no_berkas' => 'required|unique:arsip_kolektor,no_berkas',
            'kode_klasifikasi' => 'required|unique:arsip_kolektor,kode_klasifikasi',
            'uraian_informasi_berkas' => 'required',
            'tanggal' => 'required',
            'jumlah' => 'required',
            'keterangan' => 'required',
            'status' => 'required',
            'user_id' => 'required',
        ]);

        $idarsip = Crypt::decrypt($id);
        $arsip_kolektor = ArsipKolektor::find($idarsip);
        if($request->user_id == auth()->user()->id){
            $arsip_kolektor->update($request->all());
            return redirect()->route('arsip-kolektor.index')->with('update', 'Data berhasil diupdate');
        }else{
            return redirect()->route('arsip-kolektor.index')->with('update-failed', 'Data gagal diupdate');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idarsip = Crypt::decrypt($id);
        $arsip_kolektor = ArsipKolektor::find($idarsip);
        $arsip_kolektor->delete();
        return redirect()->route('arsip-kolektor.index')->with('delete', 'Data berhasil dihapus');
    }
}
