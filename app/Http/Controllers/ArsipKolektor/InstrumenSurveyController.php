<?php

namespace App\Http\Controllers\ArsipKolektor;

use File;
use Auth;
use Storage;
use DataTables;
use App\Models\User;
use App\Models\Jawaban;
use App\Models\SurveyArsip;
use Illuminate\Http\Request;
use App\Models\ArsipKolektor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class InstrumenSurveyController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = SurveyArsip::orderBy('created_at', 'DESC')->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('nama', function($row){
                        $nama = $row->nama . ' Tahun ' . $row->tahun;
                        return $nama;
                    })
                    ->addColumn('status', function($row){
                        $jawaban = Jawaban::where('surveyarsip_id', $row->id)->where('user_id', Auth::user()->id)->get()->first();
                        if($row->status == 1){
                            if (auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin') {
                                if ($jawaban) {
                                    $status = '<span class="badge badge-success">Sudah Mengisi</span>';
                                } else {
                                    $status = '<span class="badge badge-danger">Belum Mengisi</span>';
                                }
                            } else {
                                $status = '<span class="badge badge-success">Aktif</span>';
                            }
                            
                        } else {
                            $status = '<span class="badge badge-danger">Tidak Aktif</span>';
                        }
                        return $status;
                    })
                    ->addColumn('action', function($row){
                        $jawaban = Jawaban::where('surveyarsip_id', $row->id)->where('user_id', Auth::user()->id)->get()->first();
                        $btn = '';
                        return view('arsip-kolektor.instrumen-survey.actionbtn', compact('row', 'jawaban'));
                        // if($row->status == 1){
                        //     if(auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin'){
                        //         if ($jawaban) {
                        //             $btn .= '<div class="btn-group">';
                        //             $btn .= '<a href="'.route('arsip-kolektor.edit', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i> Edit Jawaban</a>';
                        //             $btn .= '<a href="'.route('arsip.import', Crypt::encrypt($row->id)).'" class="btn btn-sm ml-2 mr-2 btn-primary"><i class="fas fa-file-import"></i> Import Excel</a>';
                        //             $btn .= '<form action="'.route('arsip.detail', Crypt::encrypt($row->id)).'" method="POST">';
                        //             $btn .= csrf_field();
                        //             $btn .= '<input type="hidden" name="user_id" value="'.$jawaban->user_id.'">';
                        //             $btn .= '<button type="submit" class="btn btn-sm btn-info"><i class="fas fa-eye"></i> Detail</button>';
                        //             $btn .= '</form>';
                        //             $btn .= '</div>';
                        //         } else {
                        //             $btn .= '<a href="'.route('arsip-kolektor.show', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i> Isi Survey</a>';   
                        //             $btn .= '<a href="'.route('arsip.import', Crypt::encrypt($row->id)).'" class="btn btn-sm ml-2 btn-primary"><i class="fas fa-file-import"></i> Import Excel</a>';
                        //         }
                        //     } else {
                        //         $btn .= '<a href="'.route('instrumen-survey.listresponden', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i> Detail</a>';
                        //     }
                        // } else {
                        //     if(auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin'){
                        //         if ($jawaban) {
                        //             $btn .= '<form action="'.route('arsip.detail', Crypt::encrypt($row->id)).'" method="POST">';
                        //             $btn .= csrf_field();
                        //             $btn .= '<input type="hidden" name="user_id" value="'.$jawaban->user_id.'">';
                        //             $btn .= '<button type="submit" class="btn btn-sm btn-info"><i class="fas fa-eye"></i> Detail</button>';
                        //             $btn .= '</form>';
                        //         } else {
                        //             $btn .= '-';   
                        //         }
                        //     } else {
                        //         $btn .= '<a href="'.route('arsip.listresponden', Crypt::encrypt($row->id)).'" class="btn btn-sm btn-warning"><i class="fas fa-eye"></i> Detail</a>';
                        //     }
                                
                        // }
                       
                        return $btn;
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
        }
        return view('arsip-kolektor.instrumen-survey.index');
    }

    public function create($id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        return view('arsip-kolektor.instrumen-survey.isi-survey', compact('survey'));
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'khasanaharsip_a1h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a2h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a3h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a4h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a5h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
        ]);

        $user = User::find($request->user_id);

        $survey = SurveyArsip::find(Crypt::decrypt($id));
        $input = $request->all();

        $jawaban = Jawaban::create($request->all());

        if($request->hasFile('khasanaharsip_a1h')){
            $file = $request->file('khasanaharsip_a1h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a1h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a2h')){
            $file = $request->file('khasanaharsip_a2h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a2h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a3h')){
            $file = $request->file('khasanaharsip_a3h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a3h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a4h')){
            $file = $request->file('khasanaharsip_a4h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a4h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a5h')){
            $file = $request->file('khasanaharsip_a5h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a5h'] = Crypt::encryptString($name);
        }

        activity("Mengisi Survey")
            ->causedBy($user)
            ->performedOn($jawaban)
            ->log($user->name . " Mengisi Survey " . $survey->judul . " Tahun " . $survey->tahun);

        return redirect(route('import-arsip.index'))->with('success', 'Survey Berhasil Diisi');
    }

    public function listResponden(Request $request, $id)
    {
        if($request->ajax()){
            $data = Jawaban::where('surveyarsip_id', $id)->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('nama', function($row){
                        $nama = $row->user->name;
                        return $nama;
                    })
                    ->addColumn('instansi', function($row){
                        $instansi = $row->user->instansi->nama_instansi;
                        return $instansi;
                    })
                    ->addColumn('waktu_pengisian', function($row){
                        $tanggal = \Carbon\Carbon::parse($row->created_at);
                        $time = $tanggal->format('H:i:s');

                        return $tanggal->isoFormat('dddd, D MMMM Y') . ' ' . $time;
                        
                    })
                    ->addColumn('update_at', function($row){
                        $update_at = $row->updated_at->format('d-m-Y H:i:s');
                        return $update_at;
                    })
                    ->addColumn('action', function($row){
                        $btn = '<form action="'.route('arsip.detail', Crypt::encrypt($row->surveyarsip_id)).'" method="POST">';
                        $btn .= csrf_field();
                        $btn .= '<input type="hidden" name="user_id" value="'.$row->user_id.'">';
                        $btn .= '<button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Lihat Jawaban</button>';
                        $btn .= '</form>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        $survey = SurveyArsip::find(Crypt::decrypt($id));

        return view('arsip-kolektor.instrumen-survey.list-responden', compact('survey'));
    }

    public function detail($id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        $jawaban = Jawaban::where('surveyarsip_id', $survey->id)->where('user_id', $request->user_id)->get()->first();
        $user = User::find($request->user_id);
        return view('arsip-kolektor.instrumen-survey.detail', compact('survey', 'jawaban'));
    }

    public function edit($id)
    {
        $survey = SurveyArsip::find(Crypt::decrypt($id));
        $jawaban = Jawaban::where('surveyarsip_id', Crypt::decrypt($id))->where('user_id', Auth::user()->id)->get()->first();
        return view('arsip-kolektor.instrumen-survey.edit-survey', compact('jawaban','survey'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'unitkerja' => 'required',
            'instansi' => 'required',
            'khasanaharsip_a1h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a2h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a3h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a4h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
            'khasanaharsip_a5h' => 'nullable|mimes:pdf,doc,docx,png,jpg,jpeg,xlsx,xls,csv|max:2048',
        ]);

        // dd($request->all());

        $user = User::find($request->user_id);

        $survey = SurveyArsip::find(Crypt::decrypt($id));

        $jawaban = Jawaban::find(Crypt::decrypt($request->jawaban_id));
        $input = $request->all();
        $input['surveyarsip_id'] = $survey->id;

        if($request->hasFile('khasanaharsip_a1h')){
            $file = $request->file('khasanaharsip_a1h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a1h'] = Crypt::encryptString($name);
        }

        //dd(Storage::disk('public')->get('/arsip-kolektor/file-uploads/'));

        if($request->hasFile('khasanaharsip_a2h')){
            $file = $request->file('khasanaharsip_a2h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a2h'] = Crypt::encryptString($name);

        }

        if($request->hasFile('khasanaharsip_a3h')){
            $file = $request->file('khasanaharsip_a3h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a3h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a4h')){
            $file = $request->file('khasanaharsip_a4h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a4h'] = Crypt::encryptString($name);
        }

        if($request->hasFile('khasanaharsip_a5h')){
            $file = $request->file('khasanaharsip_a5h');
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('arsip-kolektor/file-uploads/'.$name,  File::get($file));
            $input['khasanaharsip_a5h'] = Crypt::encryptString($name);
        }

        if ($jawaban->khasanaharsip_a1h != "") {
            //dd(Storage::disk('sftp')->getDriver()->getAdapter()->applyPathPrefix('arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a1h)));
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a1h)));
        } elseif($jawaban->khasanaharsip_a2h != "") {
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a2h)));
        } elseif($jawaban->khasanaharsip_a3h != "") {
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a3h)));
        } elseif($jawaban->khasanaharsip_a4h != "") {
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a4h)));
        } elseif($jawaban->khasanaharsip_a5h != "") {
            File::delete(Storage::disk('public')->get('/arsip-kolektor/file-uploads/' . Crypt::decryptString($jawaban->khasanaharsip_a5h)));
        }

        try {
            $jawaban->update($input);
        } catch (\Exception $e) {
            return redirect()->route('instrumen-survey.index')->with('error', $e->getMessage());
        }
        

        activity("Mengubah Jawaban Survey")
            ->causedBy($user)
            ->performedOn($jawaban)
            ->log($user->name . " Mengubah Jawaban Survey " . $survey->judul . " Tahun " . $survey->tahun);

        
        return redirect()->route('instrumen-survey.index')->with('success', "Jawaban Berhasil Diubah");
        
    }
}
