<?php

namespace App\Http\Controllers\Informasi;

use File;
use App\Models\News;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Spatie\Permission\Models\Permission;
use Spatie\ImageOptimizer\Image;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use DataTables;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:super-admin|admin|master-data|pengolah-data');
        $this->middleware('permission:Menu Informasi Publik');
        $this->middleware('permission:List Berita', ['only' => ['index']]);
        $this->middleware('permission:Tambah Berita', ['only' => ['create', 'store']]);
        $this->middleware('permission:Edit Berita', ['only' => ['edit', 'update']]);
        $this->middleware('permission:Hapus Berita', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news = News::orderBy('created_at', 'ASC')->get();
        if(auth()->user()->roles()->first()->name == 'super-admin' || auth()->user()->roles()->first()->name == 'admin'){
            $data = News::orderBy('created_at', 'DESC')->get();
        } else {
            $data = News::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        }

        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function($row){
                    $image = '<img src="'.asset('images/news/'.$row->image).'" alt="'.$row->title.'" width="100px">';
                    return $image;
                })
                ->addColumn('content', function($row){
                    $content = strip_tags($row->content);
                    $contents = Str::limit($content, 100, '...');

                    return $contents;
                })
                ->addColumn('status', function($row){
                    if($row->status == '1'){
                        $status = '<span class="badge badge-success">Publish</span>';
                    } else {
                        $status = '<span class="badge badge-danger">Draft</span>';
                    }
                    return $status;
                })
                ->addColumn('action', function($row){
                    return view('informasi.news.actionbtn', compact('row'));
                }
            )
            ->rawColumns(['action', 'image', 'status', 'content'])
            ->make(true);
        }
                    
        return view('informasi.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('informasi.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required',
        ]);
        
        $news = News::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'user_id' => auth()->user()->id,
            'content' => $request->content,
            'status' => $request->status,
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/news');
            $optimizerChain = OptimizerChainFactory::create();
            $optimizerChain->optimize($image->path(), $destinationPath . '/' . $name);
            $news->image = $name;
            $news->save();
        }

        activity('Menambahkan Berita Baru')
            ->causedBy(auth()->user())
            ->performedOn($news)
            ->log(auth()->user()->name.' Menambahkan Berita Baru Dengan Judul : ' . $news->title);

        return redirect()->route('news.index')->with('success', 'Berita berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);
        return view('informasi.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newsid = Crypt::decrypt($id);
        $news = News::find($newsid);
        return view('informasi.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
        ]);
        
        $newsid = Crypt::decrypt($id);
        $news = News::find($newsid);
        $news->title = $request->title;
        $news->slug = Str::slug($request->title);
        $news->user_id = auth()->user()->id;
        $news->content = $request->content;
        $news->status = $request->status;
        $news->save();
        
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            File::delete(public_path('/images/news/' . $news->image));
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/news');
            $image->move($destinationPath, $name);
            $news->image = $name;
            $news->save();
        }

        activity('Mengupdate Berita')
            ->causedBy(auth()->user())
            ->performedOn($news)
            ->log(auth()->user()->name.' Mengupdate Berita Yang Berjudul : ' . $news->title);
        
        return redirect()->route('news.index')->with('success', 'Berita berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newsid = Crypt::decrypt($id);
        $news = News::find($newsid);
        File::delete(public_path('/images/news/' . $news->image));
        activity('Menghapus Berita')
            ->causedBy(auth()->user())
            ->performedOn($news)
            ->log(auth()->user()->name.' Menghapus Berita Yang Berjudul : ' . $news->title);
        $news->delete();
        return redirect()->route('news.index')->with('success', 'Berita berhasil di hapus');
    }
}
