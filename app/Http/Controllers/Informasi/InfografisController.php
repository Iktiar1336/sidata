<?php

namespace App\Http\Controllers\Informasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\Infografis;
use File;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use DataTables;

class InfografisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:super-admin|admin|master-data|pengolah-data');
        $this->middleware('permission:Menu Informasi Publik');
        $this->middleware('permission:List Infografis', ['only' => ['index']]);
        $this->middleware('permission:Tambah Infografis', ['only' => ['create', 'store']]);
        $this->middleware('permission:Edit Infografis', ['only' => ['edit', 'update']]);
        $this->middleware('permission:Hapus Infografis', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(auth()->user()->roles()->first()->name == 'super-admin' || auth()->user()->roles()->first()->name == 'admin'){
            $data = Infografis::orderBy('created_at', 'DESC')->get();
        } else {
            $data = Infografis::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        }

        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function($row){
                    $image = '<img src="'.asset('images/infografis/'.$row->image).'" alt="'.$row->title.'" width="100px">';
                    return $image;
                })
                ->addColumn('content', function($row){
                    $content = strip_tags($row->content);
                    $contents = Str::limit($content, 100);

                    return $contents;
                })
                ->addColumn('status', function($row){
                    if($row->status == '1'){
                        $status = '<span class="badge badge-success">Publish</span>';
                    } else {
                        $status = '<span class="badge badge-danger">Draft</span>';
                    }
                    return $status;
                })
                ->addColumn('action', function($row){
                    return view('informasi.infografis.actionbtn', compact('row'));
                })
                ->rawColumns(['image', 'content', 'status', 'action'])
                ->make(true);
        }

        $infografis = Infografis::orderBy('created_at', 'desc')->get();
        return view('informasi.infografis.index', compact('infografis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('informasi.infografis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required',
        ]);

        $infografis = Infografis::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'user_id' => auth()->user()->id,
            'content' => $request->content,
            'status' => $request->status,
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/infografis');
            $image->move($destinationPath, $name);
            $infografis->image = $name;
            $infografis->save();
        }

        activity('Menambahkan Infografis')
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menambahkan Infografis Dengan Judul :' . $request->title);

        return redirect()->route('infografis.index')->with('success', 'Infografis Berhasil Ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idinfografis = Crypt::decrypt($id);
        $infografis = Infografis::find($idinfografis);
        return view('informasi.infografis.edit', compact('infografis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $idinfografis = Crypt::decrypt($id);

        $infografis = Infografis::find($idinfografis);
        $infografis->update([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'user_id' => auth()->user()->id,
            'content' => $request->content,
            'status' => $request->status,
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            File::delete(public_path('/images/infografis/' . $infografis->image));
            $destinationPath = public_path('/images/infografis');
            $image->move($destinationPath, $name);
            $infografis->image = $name;
            $infografis->save();
        }

        activity('Mengupdate Infografis')
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Mengupdate Infografis Yang Berjudul :' . $infografis->title);

        return redirect()->route('infografis.index')->with('success', 'Infografis berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idinfografis = Crypt::decrypt($id);
        $infografis = Infografis::find($idinfografis);
        File::delete(public_path('/images/infografis/' . $infografis->image));
        $infografis->delete();
        activity('Menghapus Infografis')
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menghapus Infografis Yang Berjudul :' . $infografis->title);
        return redirect()->route('infografis.index')->with('success', 'Infografis berhasil di hapus');
    }
}
