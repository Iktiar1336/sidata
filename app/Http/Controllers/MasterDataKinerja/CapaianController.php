<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Capaian;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class CapaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Capaian::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    if ($row->status == 1) {
                        $status = '<span class="badge badge-success">Publish</span>';
                    } else {
                        $status = '<span class="badge badge-danger">Draft</span>';
                    }
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $btn = '<form action="' . route('capaian.destroy', Crypt::encrypt($row->id)) . '" method="POST" class="btn-group">
                    <a href="' . route('capaian.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                    ' . csrf_field() . '
                    ' . method_field('DELETE') .
                    '<a href=' . route('capaian.show', Crypt::encrypt($row->id)) . ' class="btn btn-info btn-sm ml-2"><i class="fas fa-eye"></i></a>' .
                    '<button type="submit" class="btn btn-danger btn-sm ml-2"><i class="fas fa-trash"></i></button>'
                    . '</form>';
                    return $btn;
                    
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('master-data-kinerja.capaian.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'tahun' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
        ]);

        $capaian = Capaian::create([
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'deskripsi' => $request->deskripsi,
            'status' => $request->status,
        ]);

        return redirect()->route('capaian.index')->with('insert', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $capaian = Capaian::find(Crypt::decrypt($id));
        return view('master-data-kinerja.capaian.show', compact('capaian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $capaian = Capaian::find(Crypt::decrypt($id));
        return view('master-data-kinerja.capaian.edit', compact('capaian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'tahun' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
        ]);

        $capaian = Capaian::find(Crypt::decrypt($id));
        $capaian->update([
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'deskripsi' => $request->deskripsi,
            'status' => $request->status,
        ]);

        return redirect()->route('capaian.index')->with('update', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $capaian = Capaian::find(Crypt::decrypt($id));
        $capaian->delete();
        return redirect()->route('capaian.index')->with('delete', 'Data berhasil dihapus');
    }
}
