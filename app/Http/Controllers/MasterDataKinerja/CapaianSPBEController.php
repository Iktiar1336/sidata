<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CapaianSPBE;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class CapaianSPBEController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = CapaianSPBE::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('predikat', function($row){
                        if ($row->predikat == "Memuaskan") {
                            $predikat = '<span class="badge badge-success">Memuaskan</span>';
                        } elseif ($row->predikat == "Sangat Baik") {
                            $predikat = '<span class="badge badge-info">Sangat Baik</span>';
                        }elseif ($row->predikat == "Baik") {
                            $predikat = '<span class="badge badge-primary">Baik</span>';
                        }elseif ($row->predikat == "Cukup") {
                            $predikat = '<span class="badge badge-warning">Cukup</span>';
                        } else {
                            $predikat = '<span class="badge badge-danger">Kurang</span>';
                        }
                        return $predikat;
                    })
                    ->addColumn('action', function($row){
                        $btn = '<form action="'.route('capaian-spbe.destroy', Crypt::encrypt($row->id)).'" method="POST">
                                    <a href="'.route('capaian-spbe.edit', Crypt::encrypt($row->id)).'" class="edit btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                                    '.csrf_field().'
                                    '.method_field("DELETE").'
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                </form>';
                        return $btn;
                    })
                    ->rawColumns(['action', 'predikat'])
                    ->make(true);
        }
        return view('master-data-kinerja.capaianspbe.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'tahun' => 'required|unique:capaian_spbe',
            'indeks' => 'required',
            'deskripsi' => 'required',
        ]);

        $indeks = str_replace(',', '.', $request->indeks);

        if ($indeks >= 4.2 && $indeks <= 5) {
            $predikat = 'Memuaskan';
        } elseif ($indeks >= 3.5 && $indeks < 4.2) {
            $predikat = 'Sangat Baik';
        } elseif ($indeks >= 2.6 && $indeks < 3.5) {
            $predikat = 'Baik';
        } elseif ($indeks >= 1.8 && $indeks < 2.6) {
            $predikat = 'Cukup';
        } else {
            $predikat = 'Kurang';
        }

        $capaian_spbe = CapaianSPBE::create([
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'indeks' => $request->indeks,
            'predikat' => $predikat,
            'deskripsi' => $request->deskripsi,
        ]);


        return redirect()->route('capaian-spbe.index')->with('insert', 'Data berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $capaian_spbe = CapaianSPBE::find(Crypt::decrypt($id));
        return view('master-data-kinerja.capaianspbe.edit', compact('capaian_spbe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'tahun' => 'required|unique:capaian_spbe,tahun,'.Crypt::decrypt($id),
            'indeks' => 'required',
            'deskripsi' => 'required',
        ]);

        $indeks = str_replace(',', '.', $request->indeks);

        if ($indeks >= 4.2 && $indeks <= 5) {
            $predikat = 'Memuaskan';
        } elseif ($indeks >= 3.5 && $indeks < 4.2) {
            $predikat = 'Sangat Baik';
        } elseif ($indeks >= 2.6 && $indeks < 3.5) {
            $predikat = 'Baik';
        } elseif ($indeks >= 1.8 && $indeks < 2.6) {
            $predikat = 'Cukup';
        } else {
            $predikat = 'Kurang';
        }

        $capaian_spbe = CapaianSPBE::find(Crypt::decrypt($id));
        $capaian_spbe->judul = $request->judul;
        $capaian_spbe->tahun = $request->tahun;
        $capaian_spbe->indeks = $request->indeks;
        $capaian_spbe->predikat = $predikat;
        $capaian_spbe->deskripsi = $request->deskripsi;
        $capaian_spbe->save();

        return redirect()->route('capaian-spbe.index')->with('update', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $capaian_spbe = CapaianSPBE::find(Crypt::decrypt($id));
        $capaian_spbe->delete();

        return redirect()->route('capaian-spbe.index')->with('delete', 'Data berhasil dihapus');
    }
}
