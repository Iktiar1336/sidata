<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PerjanjianKinerja;
use App\Models\SasaranStrategis;
use App\Models\Tujuan;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class PerjanjianKinerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sasaranstrategis = SasaranStrategis::all();
        $tujuan = Tujuan::all();
        if ($request->ajax()) {
            $data = PerjanjianKinerja::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<form action="' . route('perjanjian-kinerja.destroy', Crypt::encrypt($row->id)) . '" method="POST">
                    <a href="' . route('perjanjian-kinerja.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-warning btn-sm">Edit</a>
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <a href="' . route('perjanjian-kinerja.show', Crypt::encrypt($row->id)) . '" class="edit btn btn-primary btn-sm">Show</a>
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>';
                    return view('master-data-kinerja.perjanjian-kinerja.actionbtn', compact('row'));
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('master-data-kinerja.perjanjian-kinerja.index', compact('sasaranstrategis', 'tujuan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required|unique:perjanjian_kinerja',
            'tujuan_id' => 'required',
            'sasaran_strategis_id' => 'required',
        ]);

        $perjanjiankinerja = PerjanjianKinerja::create([
            'tahun' => $request->tahun,
        ]);

        $perjanjiankinerja->tujuan()->attach($request->tujuan_id);
        $perjanjiankinerja->sasaranstrategis()->attach($request->sasaran_strategis_id);

        return redirect()->route('perjanjian-kinerja.index')->with('insert', 'Data Perjanjian Kinerja Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $perjanjiankinerja = PerjanjianKinerja::find(Crypt::decrypt($id));
        return view('master-data-kinerja.perjanjian-kinerja.show', compact('perjanjiankinerja'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perjanjiankinerja = PerjanjianKinerja::find(Crypt::decrypt($id));
        $sasaranstrategis = SasaranStrategis::all();
        $tujuan = Tujuan::all();
        $tujuan_id = $perjanjiankinerja->tujuan->pluck('id')->toArray();
        $sasaran_strategis_id = $perjanjiankinerja->sasaranstrategis->pluck('id')->toArray();
        return view('master-data-kinerja.perjanjian-kinerja.edit', compact('perjanjiankinerja', 'sasaranstrategis', 'tujuan', 'tujuan_id', 'sasaran_strategis_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'tujuan_id' => 'required',
            'sasaran_strategis_id' => 'required',
        ]);

        $perjanjiankinerja = PerjanjianKinerja::find(Crypt::decrypt($id));
        $perjanjiankinerja->update([
            'tahun' => $request->tahun,
        ]);

        $perjanjiankinerja->tujuan()->sync($request->tujuan_id);
        $perjanjiankinerja->sasaranstrategis()->sync($request->sasaran_strategis_id);

        return redirect()->route('perjanjian-kinerja.index')->with('update', 'Data Perjanjian Kinerja Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $perjanjiankinerja = PerjanjianKinerja::find(Crypt::decrypt($id));
        $perjanjiankinerja->tujuan()->detach();
        $perjanjiankinerja->sasaranstrategis()->detach();
        $perjanjiankinerja->delete();
        return redirect()->route('perjanjian-kinerja.index')->with('delete', 'Data Perjanjian Kinerja Berhasil Dihapus');
    }
}
