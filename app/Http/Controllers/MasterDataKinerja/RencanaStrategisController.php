<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RencanaStrategis;
use App\Models\SasaranStrategis;
use App\Models\Tujuan;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class RencanaStrategisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rencanastrategis = RencanaStrategis::all();
        $sasaranstrategis = SasaranStrategis::all();
        $tujuan = Tujuan::all();
        if ($request->ajax()) {
            $data = RencanaStrategis::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<form action="' . route('rencana-strategis.destroy', Crypt::encrypt($row->id)) . '" method="POST">
                    <a href="' . route('rencana-strategis.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-warning btn-sm">Edit</a>
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <a href="' . route('rencana-strategis.show', Crypt::encrypt($row->id)) . '" class="edit btn btn-primary btn-sm">Show</a>
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>';
                    return view('master-data-kinerja.rencana-strategis.actionbtn', compact('row'));
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('master-data-kinerja.rencana-strategis.index', compact('rencanastrategis', 'sasaranstrategis', 'tujuan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required|unique:rencana_strategis',
            'tujuan_id' => 'required',
            'sasaran_strategis_id' => 'required',
        ]);

        $rencanastrategis = RencanaStrategis::create([
            'tahun' => $request->tahun,
        ]);

        $rencanastrategis->tujuan()->attach($request->tujuan_id);

        $rencanastrategis->sasaranstrategis()->attach($request->sasaran_strategis_id);

        return redirect()->route('rencana-strategis.index')
            ->with('insert', 'Rencana Strategis created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rencanastrategis = RencanaStrategis::find(Crypt::decrypt($id));
        return view('master-data-kinerja.rencana-strategis.show', compact('rencanastrategis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rencanastrategis = RencanaStrategis::find(Crypt::decrypt($id));
        $sasaranstrategis = SasaranStrategis::all();
        $tujuan = Tujuan::all();
        $tujuan_id = $rencanastrategis->tujuan->pluck('id')->toArray(); 
        $sasaran_strategis_id = $rencanastrategis->sasaranstrategis->pluck('id')->toArray();
        return view('master-data-kinerja.rencana-strategis.edit', compact('rencanastrategis', 'sasaranstrategis', 'tujuan', 'tujuan_id', 'sasaran_strategis_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'tujuan_id' => 'required',
            'sasaran_strategis_id' => 'required',
        ]);

        $rencanastrategis = RencanaStrategis::find(Crypt::decrypt($id));
        $rencanastrategis->update([
            'tahun' => $request->tahun,
        ]);

        $rencanastrategis->tujuan()->sync($request->tujuan_id);

        $rencanastrategis->sasaranstrategis()->sync($request->sasaran_strategis_id);

        return redirect()->route('rencana-strategis.index')
            ->with('update', 'Rencana Strategis updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rencanastrategis = RencanaStrategis::find(Crypt::decrypt($id));
        if ($rencanastrategis->modelkinerja->count() > 0) {
            return redirect()->back()->with('delete-error', 'Rencana Strategis cannot be deleted because it is associated with Model Kinerja.');
        } else {
            $rencanastrategis->tujuan()->detach();
            $rencanastrategis->sasaranstrategis()->detach();
            $rencanastrategis->delete();

            return redirect()->route('rencana-strategis.index')
                ->with('delete', 'Rencana Strategis deleted successfully');
        }
        
    }
}
