<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VisiMisi;
use Illuminate\Support\Facades\Crypt;

class VisiMisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visimisi = VisiMisi::orderBy('created_at', 'ASC')->get()->take(1);
        return view('master-data-kinerja.visi-misi.index', compact('visimisi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'visi' => 'required',
            'misi' => 'required',
        ]);

        $visimisi = VisiMisi::create([
            'visi' => $request->visi,
            'misi' => $request->misi,
        ]);

        activity('Menambahkan Visi & Misi')
            ->performedOn($visimisi)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menambahkan Visi & Misi');

        return redirect()->route('visi-misi.index')->with('success', 'Visi dan Misi berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $visimisi = VisiMisi::find(Crypt::decrypt($id));
        return view('master-data-kinerja.visi-misi.edit', compact('visimisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'visi' => 'required',
            'misi' => 'required',
        ]);

        $visimisi = VisiMisi::find(Crypt::decrypt($id));
        $visi = $visimisi->visi;
        $misi = $visimisi->misi;
        $visimisi->visi = $request->visi;
        $visimisi->misi = $request->misi;
        $visimisi->save();

        activity('Mengubah Visi & Misi')
            ->performedOn($visimisi)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Mengubah Visi & Misi ' . $visi . ' menjadi ' . $request->visi . ' dan ' . $misi . ' menjadi ' . $request->misi);

        return redirect()->route('visi-misi.index')->with('success', 'Visi dan Misi berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visimisi = VisiMisi::find(Crypt::decrypt($id));
        $visimisi->delete();

        activity('Menghapus Visi & Misi')
            ->performedOn($visimisi)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menghapus Visi & Misi ' . $visimisi->visi . ' dan ' . $visimisi->misi);

        return redirect()->route('visi-misi.index')->with('success', 'Data berhasil dihapus');
    }
}
