<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PeranStrategis;
use App\Models\IndikatorKinerja;
use App\Models\TargetKinerja;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class PeranStrategisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $peran_strategis = PeranStrategis::latest()->get();
        if ($request->ajax()) {
            $data = PeranStrategis::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<form action="' . route('peran-strategis.destroy', Crypt::encrypt($row->id)) . '" method="POST">
                    <a href="' . route('peran-strategis.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-primary btn-sm">Edit</a>
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $indikatorkinerja = IndikatorKinerja::all();
        $targetkinerja = TargetKinerja::all();
        return view('master-data-kinerja.peran-strategis.index', compact('peran_strategis', 'indikatorkinerja', 'targetkinerja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'peran_strategis' => 'required',
        ]);

        $peran_strategis = PeranStrategis::create([
            'peran_strategis' => $request->peran_strategis,
        ]);

        return redirect()->route('peran-strategis.index')->with('insert', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peran_strategis = PeranStrategis::find(Crypt::decrypt($id));
        return view('master-data-kinerja.peran-strategis.edit', compact('peran_strategis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'peran_strategis' => 'required',
        ]);

        $peran_strategis = PeranStrategis::find(Crypt::decrypt($id));
        $peran_strategis->peran_strategis = $request->peran_strategis;
        $peran_strategis->save();

        return redirect()->route('peran-strategis.index')->with('update', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
