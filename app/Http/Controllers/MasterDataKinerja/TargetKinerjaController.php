<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TargetKinerja;
use App\Models\IndikatorKinerja;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class TargetKinerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $targetkinerja = TargetKinerja::all();
        $indikatorkinerja = IndikatorKinerja::all(); 
        if (request()->ajax()) {
            return DataTables::of($targetkinerja)
                ->addIndexColumn()
                ->addColumn('indikator_kinerja', function ($row) {
                    return $row->indikatorkinerja->content;
                })
                ->addColumn('target', function ($row) {
                    if($row->type == 'nilai'){
                        return $row->target;
                    }else{
                       if($row->target >= 90 && $row->target <= 100){
                            return '<span class="badge badge-success">AA</span>';
                        }elseif($row->target >= 80 && $row->target <= 89){
                            return '<span class="badge badge-success">A</span>';
                        }elseif($row->target >= 70 && $row->target <= 79){
                            return '<span class="badge badge-success">BB</span>';
                        }elseif($row->target >= 60 && $row->target <= 69){
                            return '<span class="badge badge-warning">B</span>';
                        }elseif($row->target >= 50 && $row->target <= 59){
                            return '<span class="badge badge-warning">CC</span>';
                        }elseif($row->target >= 30 && $row->target <= 49){
                            return '<span class="badge badge-danger">C</span>';
                        }elseif($row->target >= 0 && $row->target <= 29){
                            return '<span class="badge badge-danger">D</span>';
                        }

                    }
                })
                ->addColumn('action', function ($row) {
                    // $btn = '<form action="' . route('target-kinerja.destroy', Crypt::encrypt($row->id)) . '" method="POST"> 
                    // <a href="' . route('target-kinerja.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                    // ' . csrf_field() . '
                    // ' . method_field('DELETE') . '
                    // <button type="submit" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                    // </form>';
                    return view('master-data-kinerja.target-kinerja.actionbtn', compact('row'));
                })
                ->rawColumns(['action', 'indikator_kinerja', 'target'])
                ->make(true);
        }

        return view('master-data-kinerja.target-kinerja.index', compact('targetkinerja', 'indikatorkinerja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'indikatorkinerja_id' => 'required',
            'tahun' => 'required',
            'type' => 'required',
            'target_kinerja' => 'required',
        ]);

        $cektargetkinerja = TargetKinerja::where('indikatorkinerja_id', $request->indikatorkinerja_id)->where('tahun', $request->tahun)->get();

        if ($cektargetkinerja->count() > 0) {
            return redirect()->back()->with('insert-failed', 'Target Kinerja sudah ada');
        } else {
            $targetkinerja = TargetKinerja::updateOrCreate(['id' => $request->targetkinerja_id], [
                'indikatorkinerja_id' => $request->indikatorkinerja_id,
                'tahun' => $request->tahun,
                'type' => $request->type,
                'target' => $request->target_kinerja,
            ]);
    
            return redirect()->route('target-kinerja.index')->with('insert', 'Target Kinerja berhasil disimpan');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $targetkinerja = TargetKinerja::find(Crypt::decrypt($id));
        $indikatorkinerja = IndikatorKinerja::all();
        return view('master-data-kinerja.target-kinerja.edit', compact('targetkinerja', 'indikatorkinerja'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'indikatorkinerja_id' => 'required',
            'tahun' => 'required',
            'type' => 'required',
            'target_kinerja' => 'required',
        ]);

        $targetkinerja = TargetKinerja::find(Crypt::decrypt($id));
        $targetkinerja->indikatorkinerja_id = $request->indikatorkinerja_id;
        $targetkinerja->tahun = $request->tahun;
        $targetkinerja->type = $request->type;
        $targetkinerja->target = $request->target_kinerja;
        $targetkinerja->save();

        return redirect()->route('target-kinerja.index')->with('update', 'Target Kinerja berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $targetkinerja = TargetKinerja::find(Crypt::decrypt($id));
        if($targetkinerja->sasaranstrategis->count() > 0){
            return redirect()->route('target-kinerja.index')->with('delete-failed', 'Target Kinerja tidak dapat dihapus karena masih digunakan');
        }else{
            $targetkinerja->delete();
            return redirect()->route('target-kinerja.index')->with('delete-success', 'Target Kinerja berhasil dihapus');
        }

    }
}
