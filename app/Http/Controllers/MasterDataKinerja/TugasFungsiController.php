<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TugasFungsi;
use Illuminate\Support\Facades\Crypt;

class TugasFungsiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tugasfungsi = TugasFungsi::orderBy('created_at', 'ASC')->get()->take(1);
        return view('master-data-kinerja.tugas-fungsi.index', compact('tugasfungsi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tugas' => 'required',
            'fungsi' => 'required',
            'kewenangan' => 'required',
        ]);

        $tugasfungsi = TugasFungsi::create([
            'tugas' => $request->tugas,
            'fungsi' => $request->fungsi,
            'kewenangan' => $request->kewenangan,
        ]);

        activity('Menambahkan Tugas & Fungsi')
            ->performedOn($tugasfungsi)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menambahkan Tugas & Fungsi');

        return redirect()->route('tugas-fungsi.index')->with('insert', 'Data berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tugasfungsi = TugasFungsi::find(Crypt::decrypt($id));
        return view('master-data-kinerja.tugas-fungsi.edit', compact('tugasfungsi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tugas' => 'required',
            'fungsi' => 'required',
            'kewenangan' => 'required',
        ]);

        $tugasfungsi = TugasFungsi::find(Crypt::decrypt($id));
        $tugas = $tugasfungsi->tugas;
        $fungsi = $tugasfungsi->fungsi;
        $kewenangan = $tugasfungsi->kewenangan;
        $tugasfungsi->tugas = $request->tugas;
        $tugasfungsi->fungsi = $request->fungsi;
        $tugasfungsi->kewenangan = $request->kewenangan;
        $tugasfungsi->save();

        activity('Mengupdate Tugas & Fungsi')
            ->performedOn($tugasfungsi)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Mengupdate Tugas & Fungsi ');

        return redirect()->route('tugas-fungsi.index')->with('update', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
