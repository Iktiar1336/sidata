<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tujuan;
use App\Models\IndikatorKinerja;
use App\Models\TargetKinerja;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class TujuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tujuan = Tujuan::all();
        if ($request->ajax()) {
            $data = Tujuan::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('tujuan', function($row){
                        $tujuan = $row->tujuan;
                        return $tujuan;
                    })
                    ->addColumn('indikatorkinerja', function($row){
                        $indikatorkinerja = $row->indikatorkinerja->content;
                        return $indikatorkinerja;
                    })
                    ->addColumn('targetkinerja', function($row){
                        $targetkinerja = $row->targetkinerja->target;
                        return $targetkinerja;
                    })
                    ->addColumn('tahun', function($row){
                        $tahun = $row->targetkinerja->tahun;
                        return $tahun;
                    })
                    ->addColumn('action', function($row){
                        // $btn = '<form action="'.route('tujuan.destroy', Crypt::encrypt($row->id)).'" method="POST">';
                        // $btn .= csrf_field();
                        // $btn .= method_field('DELETE');
                        // $btn .= '<a href="'.route('tujuan.edit', Crypt::encrypt($row->id)).'" class="edit btn btn-primary btn-sm">Edit</a>';
                        // $btn .= '<button type="submit" class="btn btn-danger btn-sm">Delete</button>';
                        // $btn .= '</form>';
                        return view('master-data-kinerja.tujuan.actionbtn', compact('row'));
                    })
                    ->rawColumns(['action', 'indikatorkinerja', 'targetkinerja', 'tujuan'])
                    ->make(true);
        }
        $indikatorkinerja = IndikatorKinerja::all();
        $targetkinerja = TargetKinerja::all();
        return view('master-data-kinerja.tujuan.index', compact('tujuan', 'indikatorkinerja', 'targetkinerja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tujuan' => 'required',
            'indikatorkinerja_id' => 'required',
            'targetkinerja_id' => 'required',
        ]);

        $tujuan = Tujuan::create([
            'tujuan' => $request->tujuan,
            'indikatorkinerja_id' => $request->indikatorkinerja_id,
            'targetkinerja_id' => $request->targetkinerja_id,
        ]);

        return redirect()->route('tujuan.index')->with('insert', 'Data berhasil ditambahkan');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tujuan = Tujuan::find(Crypt::decrypt($id));
        $indikatorkinerja = IndikatorKinerja::all();
        $targetkinerja = TargetKinerja::all();
        return view('master-data-kinerja.tujuan.edit', compact('tujuan', 'indikatorkinerja', 'targetkinerja'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tujuan' => 'required',
            'indikatorkinerja_id' => 'required',
            'targetkinerja_id' => 'required',
        ]);

        $tujuan = Tujuan::find(Crypt::decrypt($id));
        $tujuan->update([
            'tujuan' => $request->tujuan,
            'indikatorkinerja_id' => $request->indikatorkinerja_id,
            'targetkinerja_id' => $request->targetkinerja_id,
        ]);

        return redirect()->route('tujuan.index')->with('update', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tujuan = Tujuan::find(Crypt::decrypt($id));
        if($tujuan->rencanastrategis->count() > 0 || $tujuan->perjanjiankinerja->count() > 0){
            return redirect()->route('tujuan.index')->with('delete', 'Data tidak bisa dihapus karena masih digunakan');
        }else{
            $tujuan->delete();
            return redirect()->route('tujuan.index')->with('delete', 'Data berhasil dihapus');
        }

    }
}
