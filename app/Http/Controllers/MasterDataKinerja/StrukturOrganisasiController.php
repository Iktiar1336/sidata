<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StrukturOrganisasi;
use Illuminate\Support\Facades\Crypt;

class StrukturOrganisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $strukturorganisasi = StrukturOrganisasi::orderBy('id', 'desc')->take(1)->get();
        
        return view('master-data-kinerja.struktur-organisasi.index', compact('strukturorganisasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'struktur-organisasi' => 'required|mimes:png,jpg,jpeg|max:2048',
        ]);

        $filename = $request->file('struktur-organisasi')->getClientOriginalName();
        $request->file('struktur-organisasi')->move('uploads/strukturorganisasi', $filename);

        $strukturorganisasi = StrukturOrganisasi::create([
            'file' => $filename,
            'status' => 1,
        ]);

        activity('Menambahkan Struktur Organisasi')
            ->performedOn($strukturorganisasi)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menambahkan Struktur Organisasi');

        return redirect()->route('struktur-organisasi.index')->with('success', 'Struktur Organisasi berhasil ditambahkan');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $strukturorganisasi = StrukturOrganisasi::find(Crypt::decrypt($id));
        return view('master-data-kinerja.struktur-organisasi.edit', compact('strukturorganisasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'struktur-organisasi' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        ]);

        $strukturorganisasi = StrukturOrganisasi::find(Crypt::decrypt($id));

        if ($request->hasFile('struktur-organisasi')) {
            $filename = $request->file('struktur-organisasi')->getClientOriginalName();
            $request->file('struktur-organisasi')->move('uploads/strukturorganisasi', $filename);
    
            $strukturorganisasi->update([
                'file' => $filename,
                'status' => 1,
            ]);
        } 

        activity('Mengubah Struktur Organisasi')
            ->performedOn($strukturorganisasi)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Mengubah Struktur Organisasi');

        return redirect()->route('struktur-organisasi.index')->with('success', 'Struktur Organisasi berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
