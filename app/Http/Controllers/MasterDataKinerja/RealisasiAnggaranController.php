<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RealisasiAnggaran;
use App\Models\SasaranStrategis;
use Illuminate\Support\Facades\Crypt;
use DataTables;


class RealisasiAnggaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = RealisasiAnggaran::with('sasaranstrategis')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('sasaranstrategis', function ($row) {
                    return $row->sasaranstrategis->sasaran_strategis;
                })
                ->addColumn('alokasi', function ($row) {
                    return 'Rp. ' . number_format($row->alokasi, 0, ',', '.');
                })
                ->addColumn('realisasi', function ($row) {
                    return 'Rp. ' . number_format($row->realisasi, 0, ',', '.');
                })
                ->addColumn('persentase', function ($row) {
                    if($row->alokasi >= $row->realisasi){
                        return '<span class="badge badge-danger"><i class="fas fa-arrow-down"></i> '.$row->persentase.'</span>';
                    }else{
                        return '<span class="badge badge-success"><i class="fas fa-arrow-up"></i> '.$row->persentase.'</span>';
                    }
                })
                ->addColumn('action', function ($row) {
                    $btn = '<form action="' . route('realisasi-anggaran.destroy', Crypt::encrypt($row->id)) . '" method="POST" class="btn-group">
                    <a href="' . route('realisasi-anggaran.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                    ' . csrf_field() . '
                    ' . method_field('DELETE') .
                    '<button type="submit" class="btn btn-danger btn-sm ml-2"><i class="fas fa-trash"></i></button>'
                    . '</form>';
                    return $btn;
                    
                })
                ->rawColumns(['action', 'sasaranstrategis', 'persentase'])
                ->make(true);
        }
        $sasaranstrategis = SasaranStrategis::all();
        return view('master-data-kinerja.realisasi-anggaran.index', compact('sasaranstrategis'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sasaran_strategis_id' => 'required',
            'realisasi' => 'required',
            'alokasi_anggaran' => 'required',
            'tahun' => 'required',
        ]);

        try {
            $sasaranstrategis = SasaranStrategis::where('id', $request->sasaran_strategis_id)->get()->first();

            $persen = number_format(($request->realisasi / $request->alokasi_anggaran) * 100, 2);

            $cekrealisasianggaran = RealisasiAnggaran::where('sasaranstrategis_id', $request->sasaran_strategis_id)->where('tahun', $request->tahun)->get();

            if (count($cekrealisasianggaran) > 0) {
                return redirect()->back()->with('insert-failed', 'Data Realisasi Anggaran Sudah Ada');
            } else {
                $realisasianggaran = new RealisasiAnggaran();
                $realisasianggaran->sasaranstrategis_id = $request->sasaran_strategis_id;
                $realisasianggaran->alokasi = $request->alokasi_anggaran;
                $realisasianggaran->realisasi = $request->realisasi;
                $realisasianggaran->tahun = $request->tahun;
                $realisasianggaran->persentase = $persen . '%';
                $realisasianggaran->save();
                return redirect()->back()->with('insert', 'Data Realisasi Anggaran Berhasil Ditambahkan');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with('error-insert', "Data Realisasi Anggaran Gagal Ditambahkan");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $realisasianggaran = RealisasiAnggaran::find(Crypt::decrypt($id));
        $sasaranstrategis = SasaranStrategis::all();
        return view('master-data-kinerja.realisasi-anggaran.edit', compact('realisasianggaran', 'sasaranstrategis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sasaran_strategis_id' => 'required',
            'realisasi' => 'required',
            'alokasi_anggaran' => 'required',
            'tahun' => 'required',
        ]);

        try {
            $sasaranstrategis = SasaranStrategis::where('id', $request->sasaran_strategis_id)->get()->first();

            $persen = number_format(($request->realisasi / $request->alokasi_anggaran) * 100, 2);

            $realisasianggaran = RealisasiAnggaran::find(Crypt::decrypt($id));
            $realisasianggaran->sasaranstrategis_id = $request->sasaran_strategis_id;
            $realisasianggaran->alokasi = $request->alokasi_anggaran;
            $realisasianggaran->realisasi = $request->realisasi;
            $realisasianggaran->tahun = $request->tahun;
            $realisasianggaran->persentase = $persen . '%';
            $realisasianggaran->update();
            return redirect(route('realisasi-anggaran.index'))->with('update', 'Data Realisasi Anggaran Berhasil Diubah');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error-insert', "Data Realisasi Anggaran Gagal Diubah");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $realisasianggaran = RealisasiAnggaran::find(Crypt::decrypt($id));
        $realisasianggaran->delete();
        return redirect()->back()->with('delete', 'Data Realisasi Anggaran Berhasil Dihapus');
    }
}
