<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Performance;
use Illuminate\Support\Facades\Crypt;
use File;

class PerformanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:super-admin|admin');
        $this->middleware('permission:menu-data-kinerja');
    }

    public function visimisi()
    {
        $performance = Performance::orderBy('created_at', 'ASC')->get()->take(1);
        return view('admin.performance.visi-misi.index', compact('performance')); 
    }

    public function visimisi_store(Request $request)
    {
        $performance = new Performance;
        $performance->visi = $request->visi;
        $performance->misi = $request->misi;
        $performance->save();
        activity('Menambahkan Visi & Misi')
            ->performedOn($performance)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menambahkan Visi & Misi');
        return redirect()->route('performance.visi-misi')->with('success', 'Data berhasil ditambahkan');
    }

    public function visimisi_edit($id)
    {
        $idvisimisi = Crypt::decrypt($id);
        $performance = Performance::find($idvisimisi);
        return view('admin.performance.visi-misi.edit', compact('performance'));
    }

    public function visimisi_update(Request $request, $id)
    {
        $idvisimisi = Crypt::decrypt($id);
        $performance = Performance::find($idvisimisi);
        $visi = $performance->visi;
        $misi = $performance->misi;
        $performance->visi = $request->visi;
        $performance->misi = $request->misi;
        $performance->save();
        activity('Mengupdate Visi & Misi')
            ->performedOn($performance)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Mengupdate Visi & Misi ' . $visi . ' menjadi ' . $request->visi . ' dan ' . $misi . ' menjadi ' . $request->misi);
        return redirect()->route('performance.visi-misi')->with('success', 'Data berhasil diubah');
    }

    public function tugasfungsi()
    {
        $performance = Performance::orderBy('created_at', 'ASC')->get()->take(1);
        return view('admin.performance.tugas-fungsi.index', compact('performance'));
    }

    public function tugasfungsi_store(Request $request)
    {
        $id = Crypt::decrypt($request->id);
        $performance = Performance::find($id);
        $performance->tugas = $request->tugas;
        $performance->fungsi = $request->fungsi;
        $performance->kewenangan = $request->kewenangan;
        $performance->update();
        activity('Menambahkan Tugas & Fungsi')
            ->performedOn($performance)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menambahkan Tugas & Fungsi');
        return redirect()->route('performance.tugas-fungsi')->with('success', 'Data berhasil ditambahkan');
    }

    public function tugasfungsi_edit($id)
    {
        $idtugasfungsi = Crypt::decrypt($id);
        $performance = Performance::find($idtugasfungsi);
        return view('admin.performance.tugas-fungsi.edit', compact('performance'));
    }

    public function tugasfungsi_update(Request $request, $id)
    {
        $this->validate($request, [
            'tugas' => 'required',
            'fungsi' => 'required',
        ]);

        $idtugasfungsi = Crypt::decrypt($id);
        $performance = Performance::find($idtugasfungsi);
        $performance->tugas = $request->tugas;
        $performance->fungsi = $request->fungsi;
        $performance->kewenangan = $request->kewenangan;
        $performance->save();
        activity('Mengupdate Tugas & Fungsi')
            ->performedOn($performance)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Mengupdate Tugas & Fungsi');
        return redirect()->route('performance.tugas-fungsi')->with('success', 'Data berhasil diubah');
    }

    public function strukturorganisasi()
    {
        $performance = Performance::orderBy('created_at', 'ASC')->get()->take(1);
        return view('admin.performance.struktur-organisasi.index', compact('performance'));
    }

    public function strukturorganisasi_store(Request $request)
    {
        $this->validate($request, [
            'struktur_organisasi' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $id = Crypt::decrypt($request->id);
        $performance = Performance::find($id);
        $file = $request->file('struktur_organisasi');
        $filename = $file->getClientOriginalName();
        $request->file('struktur_organisasi')->move('images/struktur-organisasi', $filename);
        $performance->struktur_organisasi = $filename;
        $performance->save();
        activity('Menambahkan Struktur Organisasi')
            ->performedOn($performance)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menambahkan Struktur Organisasi');
        return redirect()->route('performance.struktur-organisasi')->with('success', 'Data berhasil ditambahkan');
    }

    public function strukturorganisasi_edit($id)
    {
        $idstrukturorganisasi = Crypt::decrypt($id);
        $performance = Performance::find($idstrukturorganisasi);
        return view('admin.performance.struktur-organisasi.edit', compact('performance'));
    }

    public function strukturorganisasi_update(Request $request, $id)
    {
        $this->validate($request, [
            'struktur_organisasi' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $idstrukturorganisasi = Crypt::decrypt($id);
        $performance = Performance::find($idstrukturorganisasi);
        File::delete('images/struktur-organisasi/' . $performance->struktur_organisasi);
        $file = $request->file('struktur_organisasi');
        $filename = $file->getClientOriginalName();
        $request->file('struktur_organisasi')->move('images/struktur-organisasi', $filename);
        $performance->struktur_organisasi = $filename;
        $performance->save();
        activity('Mengupdate Struktur Organisasi')
            ->performedOn($performance)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Mengupdate Struktur Organisasi');
        return redirect()->route('performance.struktur-organisasi')->with('success', 'Data berhasil diubah');
    }
}
