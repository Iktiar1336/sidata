<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CapaianKinerja;
use DataTables;
use Illuminate\Support\Facades\Crypt;
use App\Models\SasaranStrategis;

class CapaianKinerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $capaian_kinerja = CapaianKinerja::all();
        $sasaranstrategis = SasaranStrategis::all();
        if ($request->ajax()) {
            $data = CapaianKinerja::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('sasaranstrategis', function ($row) {
                    return $row->sasaranstrategis->sasaran_strategis;
                })
                ->addColumn('target', function ($row) {
                    //return $row->sasaranstrategis->targetkinerja->target;
                    if($row->sasaranstrategis->targetkinerja->type == 'nilai'){
                        return $row->sasaranstrategis->targetkinerja->target;
                    }else{
                       if($row->sasaranstrategis->targetkinerja->target >= 90 && $row->sasaranstrategis->targetkinerja->target <= 100){
                            return '<span class="badge badge-success">AA</span>';
                        }elseif($row->sasaranstrategis->targetkinerja->target >= 80 && $row->sasaranstrategis->targetkinerja->target <= 89){
                            return '<span class="badge badge-success">A</span>';
                        }elseif($row->sasaranstrategis->targetkinerja->target >= 70 && $row->sasaranstrategis->targetkinerja->target <= 79){
                            return '<span class="badge badge-success">BB</span>';
                        }elseif($row->sasaranstrategis->targetkinerja->target >= 60 && $row->sasaranstrategis->targetkinerja->target <= 69){
                            return '<span class="badge badge-warning">B</span>';
                        }elseif($row->sasaranstrategis->targetkinerja->target >= 50 && $row->sasaranstrategis->targetkinerja->target <= 59){
                            return '<span class="badge badge-warning">CC</span>';
                        }elseif($row->sasaranstrategis->targetkinerja->target >= 30 && $row->sasaranstrategis->targetkinerja->target <= 49){
                            return '<span class="badge badge-danger">C</span>';
                        }elseif($row->sasaranstrategis->targetkinerja->target >= 0 && $row->sasaranstrategis->targetkinerja->target <= 29){
                            return '<span class="badge badge-danger">D</span>';
                        }
                    }
                })
                ->addColumn('realisasi', function ($row) {
                    if($row->sasaranstrategis->targetkinerja->type == 'nilai'){
                        return $row->realisasi;
                    }else{
                        if($row->realisasi >= 90 && $row->realisasi <= 100){
                            return '<span class="badge badge-success">AA</span>';
                        }elseif($row->realisasi >= 80 && $row->realisasi <= 89){
                            return '<span class="badge badge-success">A</span>';
                        }elseif($row->realisasi >= 70 && $row->realisasi <= 79){
                            return '<span class="badge badge-success">BB</span>';
                        }elseif($row->realisasi >= 60 && $row->realisasi <= 69){
                            return '<span class="badge badge-warning">B</span>';
                        }elseif($row->realisasi >= 50 && $row->realisasi <= 59){
                            return '<span class="badge badge-warning">CC</span>';
                        }elseif($row->realisasi >= 30 && $row->realisasi <= 49){
                            return '<span class="badge badge-danger">C</span>';
                        }elseif($row->realisasi >= 0 && $row->realisasi <= 29){
                            return '<span class="badge badge-danger">D</span>';
                        }
                    }
                })
                ->addColumn('persentase', function ($row) {
                    if($row->sasaranstrategis->targetkinerja->target >= $row->realisasi){
                        return '<span class="badge badge-danger"><i class="fas fa-arrow-down"></i> '.$row->persentase.'</span>';
                    }else{
                        return '<span class="badge badge-success"><i class="fas fa-arrow-up"></i> '.$row->persentase.'</span>';
                    }
                })
                ->addColumn('status', function ($row) {
                    if($row->status == '1'){
                        return '<span class="badge badge-success">Publish</span>';
                    }else{
                        return '<span class="badge badge-danger">Draft</span>';
                    }
                })
                ->addColumn('action', function ($row) {
                    $btn = '<form action="' . route('capaian-kinerja.destroy', Crypt::encrypt($row->id)) . '" method="POST" class="btn-group">
                    <a href="' . route('capaian-kinerja.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                    
                    <button type="submit" class="btn btn-danger btn-sm ml-2"><i class="fas fa-trash"></i></button>
                    </form>';
                    return $btn;
                })
                ->rawColumns(['action', 'persentase','sasaranstrategis', 'target', 'status', 'realisasi'])
                ->make(true);
        }
        return view('master-data-kinerja.capaian-kinerja.index', compact('capaian_kinerja', 'sasaranstrategis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sasaran_strategis_id' => 'required',
            'realisasi' => 'required',
            'tahun' => 'required',
            'status' => 'required',
        ]);

        $sasaranstrategis = SasaranStrategis::where('id', $request->sasaran_strategis_id)->get()->first();
        
        $target = $sasaranstrategis->targetkinerja->target;

        $realisasi = number_format($request->realisasi, 2, '.', '');

        $persen = number_format(($realisasi / $target) * 100, 2);

        $cek = CapaianKinerja::where('sasaranstrategis_id', $request->sasaran_strategis_id)->where('tahun', $request->tahun)->get();

        if (count($cek) > 0) {
            return redirect()->back()->with('insert-failed', 'Data Capaian Kinerja sudah ada');
        } else {
            $capaian_kinerja = new CapaianKinerja;
            $capaian_kinerja->sasaranstrategis_id = $request->sasaran_strategis_id;
            $capaian_kinerja->realisasi = $realisasi;
            $capaian_kinerja->tahun = $request->tahun;
            $capaian_kinerja->persentase = $persen . '%';
            $capaian_kinerja->status = $request->status;
            $capaian_kinerja->save();
    
            return redirect()->route('capaian-kinerja.index')->with('insert', 'Capaian Kinerja Berhasil Ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $capaiankinerja = CapaianKinerja::find(Crypt::decrypt($id));
        $sasaranstrategis = SasaranStrategis::all();
        return view('master-data-kinerja.capaian-kinerja.edit', compact('capaiankinerja', 'sasaranstrategis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sasaran_strategis_id' => 'required',
            'realisasi' => 'required',
            'tahun' => 'required',
            'status' => 'required',
        ]);

        $sasaranstrategis = SasaranStrategis::where('id', $request->sasaran_strategis_id)->get()->first();
        
        $target = $sasaranstrategis->targetkinerja->target;

        $realisasi = number_format($request->realisasi, 2, '.', '');

        $persen = number_format(($realisasi / $target) * 100, 2);

        $capaian_kinerja = CapaianKinerja::find(Crypt::decrypt($id));
        $capaian_kinerja->sasaranstrategis_id = $request->sasaran_strategis_id;
        $capaian_kinerja->realisasi = $realisasi;
        $capaian_kinerja->tahun = $request->tahun;
        $capaian_kinerja->persentase = $persen . '%';
        $capaian_kinerja->status = $request->status;
        $capaian_kinerja->save();

        return redirect()->route('capaian-kinerja.index')->with('update', 'Capaian Kinerja Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $capaian_kinerja = CapaianKinerja::find(Crypt::decrypt($id));
        $capaian_kinerja->delete();

        return redirect()->route('capaian-kinerja.index')->with('delete', 'Capaian Kinerja Berhasil Dihapus');
    }
}
