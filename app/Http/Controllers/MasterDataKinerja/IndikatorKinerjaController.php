<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IndikatorKinerja;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class IndikatorKinerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $indikator_kinerja = IndikatorKinerja::all();
        if (request()->ajax()) {
            return DataTables::of($indikator_kinerja)
                ->addIndexColumn()
                ->addColumn('indikator_kinerja', function ($row) {
                    return $row->content;
                })
                ->addColumn('action', function ($row) {
                    // $btn = '<form action="' . route('indikator-kinerja.destroy', Crypt::encrypt($row->id)) . '" method="POST"> 
                    // <a href="' . route('indikator-kinerja.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                    // ' . csrf_field() . '
                    // ' . method_field('DELETE') . '
                    // <button type="submit" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                    // </form>';

                    return view('master-data-kinerja.indikator-kinerja.actionbtn', compact('row'));
                })
                ->rawColumns(['action', 'indikator_kinerja'])
                ->make(true);
        }
        return view('master-data-kinerja.indikator-kinerja.index', compact('indikator_kinerja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'indikator_kinerja' => 'required',
        ]);

        $indikator_kinerja = IndikatorKinerja::create([
            'content' => $request->indikator_kinerja,
        ]);

        return redirect()->route('indikator-kinerja.index')->with('insert', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $indikator_kinerja = IndikatorKinerja::find(Crypt::decrypt($id));
        return view('master-data-kinerja.indikator-kinerja.edit', compact('indikator_kinerja'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'indikator_kinerja' => 'required',
        ]);

        $indikator_kinerja = IndikatorKinerja::find(Crypt::decrypt($id));
        $indikator_kinerja->update([
            'content' => $request->indikator_kinerja,
        ]);

        return redirect()->route('indikator-kinerja.index')->with('update', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $indikator_kinerja = IndikatorKinerja::find(Crypt::decrypt($id));

        if ($indikator_kinerja->targetkinerja->count() > 0 && $indikator_kinerja->sasaranstrategis->count() > 0) {
            return redirect()->route('indikator-kinerja.index')->with('delete-failed', 'Data tidak dapat dihapus karena masih digunakan');
        } else {
            $indikator_kinerja->delete();
            return redirect()->route('indikator-kinerja.index')->with('delete-success', 'Data berhasil dihapus');
        }
    }
}
