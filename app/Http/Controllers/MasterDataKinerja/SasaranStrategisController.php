<?php

namespace App\Http\Controllers\MasterDataKinerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SasaranStrategis;
use App\Models\IndikatorKinerja;
use App\Models\TargetKinerja;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class SasaranStrategisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = SasaranStrategis::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('sasaran_strategis', function($row){
                        $sasaran = $row->sasaran_strategis;
                        return $sasaran;
                    })
                    ->addColumn('indikatorkinerja', function($row){
                        $indikatorkinerja = $row->indikatorkinerja->content;
                        return $indikatorkinerja;
                    })
                    ->addColumn('targetkinerja', function($row){
                        $targetkinerja = $row->targetkinerja->target;
                        if($row->targetkinerja->type == 'nilai'){
                            return $targetkinerja;
                        }else{
                           if($row->targetkinerja->target >= 90 && $row->targetkinerja->target <= 100){
                                return '<span class="badge badge-success">AA</span>';
                            }elseif($row->targetkinerja->target >= 80 && $row->targetkinerja->target <= 89){
                                return '<span class="badge badge-success">A</span>';
                            }elseif($row->targetkinerja->target >= 70 && $row->targetkinerja->target <= 79){
                                return '<span class="badge badge-success">BB</span>';
                            }elseif($row->targetkinerja->target >= 60 && $row->targetkinerja->target <= 69){
                                return '<span class="badge badge-warning">B</span>';
                            }elseif($row->targetkinerja->target >= 50 && $row->targetkinerja->target <= 59){
                                return '<span class="badge badge-warning">CC</span>';
                            }elseif($row->targetkinerja->target >= 30 && $row->targetkinerja->target <= 49){
                                return '<span class="badge badge-danger">C</span>';
                            }elseif($row->targetkinerja->target >= 0 && $row->targetkinerja->target <= 29){
                                return '<span class="badge badge-danger">D</span>';
                            }
    
                        }
                        //return $targetkinerja;
                    })
                    ->addColumn('tahun', function($row){
                        $tahun = $row->targetkinerja->tahun;
                        return $tahun;
                    })
                    ->addColumn('action', function($row){
                        // $btn = '<div class="btn-group row">
                        // <form action="' . route('sasaran-strategis.destroy', Crypt::encrypt($row->id)) . '" method="POST">
                        // <a href="' . route('sasaran-strategis.edit', Crypt::encrypt($row->id)) . '" class="edit btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                        // ' . csrf_field() . '
                        // ' . method_field('DELETE') . '
                        // <button type="submit" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                        // </form>
                        // </div>';
                        return view('master-data-kinerja.sasaran-strategis.actionbtn', compact('row'));
                    })
                    ->rawColumns(['action', 'sasaran_strategis', 'indikatorkinerja', 'targetkinerja'])
                    ->make(true);
        }
        $sasaran_strategis = SasaranStrategis::all();
        $indikatorkinerja = IndikatorKinerja::all();
        $targetkinerja = TargetKinerja::all();
        return view('master-data-kinerja.sasaran-strategis.index', compact('sasaran_strategis', 'indikatorkinerja', 'targetkinerja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->ajax()){
            $targetkinerja = TargetKinerja::where('indikatorkinerja_id', $request->indikatorkinerja_id)->get();
            return response()->json($targetkinerja);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sasaran_strategis' => 'required',
            'indikatorkinerja_id' => 'required',
            'targetkinerja_id' => 'required',
        ]);

        $cek = SasaranStrategis::where('indikatorkinerja_id', $request->indikatorkinerja_id)->where('targetkinerja_id', $request->targetkinerja_id)->get();

        if (count($cek) > 0) {
            return redirect()->back()->with('insert-failed', 'Data sudah ada');
        } else {
            $sasaran_strategis = SasaranStrategis::create([
                'sasaran_strategis' => $request->sasaran_strategis,
                'indikatorkinerja_id' => $request->indikatorkinerja_id,
                'targetkinerja_id' => $request->targetkinerja_id,
            ]);
            
            return redirect()->route('sasaran-strategis.index')->with('insert', 'Sasaran Strategis berhasil ditambahkan');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sasaran_strategis = SasaranStrategis::find(Crypt::decrypt($id));
        $indikatorkinerja = IndikatorKinerja::all();
        $targetkinerja = TargetKinerja::all(); 
        return view('master-data-kinerja.sasaran-strategis.edit', compact('sasaran_strategis', 'indikatorkinerja', 'targetkinerja'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sasaran_strategis' => 'required',
            'indikatorkinerja_id' => 'required',
            'targetkinerja_id' => 'required',
        ]);

        $sasaran_strategis = SasaranStrategis::find(Crypt::decrypt($id));
        $sasaran_strategis->sasaran_strategis = $request->sasaran_strategis;
        $sasaran_strategis->indikatorkinerja_id = $request->indikatorkinerja_id;
        $sasaran_strategis->targetkinerja_id = $request->targetkinerja_id;
        $sasaran_strategis->save();
        
        return redirect()->route('sasaran-strategis.index')->with('update', 'Sasaran Strategis berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sasaran_strategis = SasaranStrategis::find(Crypt::decrypt($id));
        if($sasaran_strategis->rencanastrategis->count() > 0 && $sasaran_strategis->perjanjiankinerja->count() > 0){
            return redirect()->back()->with('delete-failed', 'Data tidak bisa dihapus karena masih ada data yang terkait');
        } else {
            $sasaran_strategis->delete();
            return redirect()->route('sasaran-strategis.index')->with('delete', 'Sasaran Strategis berhasil dihapus');
        }
    }
}
