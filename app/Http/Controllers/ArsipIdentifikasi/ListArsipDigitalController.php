<?php

namespace App\Http\Controllers\ArsipIdentifikasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArsipDigital;
use DataTables;
use Illuminate\Support\Facades\Storage;

class ListArsipDigitalController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if(auth()->user()->roles()->first()->name == 'admin' || auth()->user()->roles()->first()->name == 'super-admin'){
                $arsipDigital = ArsipDigital::all();
            }else{
                $arsipDigital = ArsipDigital::where('user_id', auth()->user()->id)->where('instansi_id', auth()->user()->instansi_id)->get();
            }
            return Datatables::of($arsipDigital)
                ->addIndexColumn()
                ->addColumn('instansi', function ($row) {
                    $instansi = $row->instansi->nama_instansi;
                    return $instansi;
                })
                ->addColumn('file', function ($row) {
                   return $row->file;
                })
                ->addColumn('size', function ($row) {
                    $bytes = Storage::disk('public')->size('arsip-digital/tmp/'.$row->folder.'/'.$row->file);
                    if ($bytes >= 1073741824)
                    {
                        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
                    } elseif ($bytes >= 1048576)
                    {
                        $bytes = number_format($bytes / 1048576, 2) . ' MB';
                    } elseif ($bytes >= 1024)
                    {
                        $bytes = number_format($bytes / 1024, 2) . ' KB';
                    } elseif ($bytes > 1)
                    {
                        $bytes = $bytes . ' bytes';
                    } elseif ($bytes == 1)
                    {
                        $bytes = $bytes . ' byte';
                    } else
                    {
                        $bytes = '0 bytes';
                    }

                    return $bytes;
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm editArsipDigital">Edit</a>';
                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-danger btn-sm deleteArsipDigital">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action', 'file'])
                ->make(true);
        }

        return view('arsip-identifikasi.list-arsip-digital.index');
    }
}
