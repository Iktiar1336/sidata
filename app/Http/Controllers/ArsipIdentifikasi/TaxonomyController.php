<?php

namespace App\Http\Controllers\ArsipIdentifikasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArsipKolektor;
use App\Models\ArsipDigital;
use DataTables;
use App\Models\Instansi;
use Illuminate\Support\Facades\Crypt;

class TaxonomyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $alldata = ArsipDigital::orderBy('created_at', 'ASC')->get()->groupBy('arsipkolektor_id');

            $grup = [];

            foreach ($alldata as $key => $collect) {
                
                foreach ($collect as $keys => $val) {

                    if(auth()->user()->roles()->first()->name == 'Instansi' && auth()->user()->instansi_id == $val->arsipkolektor->instansi_id){
                        if(count($val->arsipkolektor->instansi->subinstansi) > 0){
                            $subinstansi = Instansi::where('parent_id', $val->arsipkolektor->instansi_id)->get();
                        } else {
                            $subinstansi = [];
                        }
                        $grup[] = [
                            'id' => $val->id,
                            'instansi' => $val->arsipkolektor->instansi->nama_instansi,
                            'instansi_id' => $val->arsipkolektor->instansi_id,
                            'jenis_arsip' => $val->arsipkolektor->jenisarsip->nama,
                            'media_arsip' => $val->arsipkolektor->media_arsip,
                            'keterangan' => $val->arsipkolektor->keterangan,
                            'kondisi' => $val->arsipkolektor->kondisi,
                            'tingkat_perkembangan' => $val->arsipkolektor->tingkat_perkembangan,
                            'type' => $val->type,
                            'uraian_item' => $val->arsipkolektor->uraianitem,
                            'kode_klasifikasi' => $val->arsipkolektor->kode_klasifikasi,
                            'nomor_surat' => $val->arsipkolektor->nomor_surat,
                            'tanggal_surat' => $val->arsipkolektor->tanggal_surat,
                            'jumlah_berkas' => $val->arsipkolektor->jumlah,
                            'folder' => $val->folder,
                            'nama_file' => $val->file,
                        ];
                    } else if(auth()->user()->roles()->first()->name == 'admin' || auth()->user()->roles()->first()->name == 'super-admin'){
                        $grup[] = [
                            'id' => $val->id,
                            'instansi' => $val->arsipkolektor->instansi->nama_instansi,
                            'instansi_id' => $val->arsipkolektor->instansi_id,
                            'jenis_arsip' => $val->arsipkolektor->jenisarsip->nama,
                            'media_arsip' => $val->arsipkolektor->media_arsip,
                            'keterangan' => $val->arsipkolektor->keterangan,
                            'kondisi' => $val->arsipkolektor->kondisi,
                            'tingkat_perkembangan' => $val->arsipkolektor->tingkat_perkembangan,
                            'type' => $val->type,
                            'uraian_item' => $val->arsipkolektor->uraianitem,
                            'kode_klasifikasi' => $val->arsipkolektor->kode_klasifikasi,
                            'nomor_surat' => $val->arsipkolektor->nomor_surat,
                            'tanggal_surat' => $val->arsipkolektor->tanggal_surat,
                            'jumlah_berkas' => $val->arsipkolektor->jumlah,
                            'folder' => $val->folder,
                            'nama_file' => $val->file,
                        ];
                    }
                }
            }

            $collect = collect($grup);
            $datafilter6 = [];

            if($request->filter_6 == 'jenis_arsip'){
                $grupbyjenisarsip = $collect->groupBy('jenis_arsip');

                foreach ($grupbyjenisarsip as $keyjenisarsip => $jenis) {
                    $datafilter6[] = [
                        'key' => $keyjenisarsip,
                        'data' => $jenis
                    ];
                }
            }elseif($request->filter_6 == 'media_arsip'){

                $grupbymediaarsip = $collect->groupBy('media_arsip');

                foreach ($grupbymediaarsip as $keymediaarsip => $media) {
                    $datafilter6[] = [
                        'key' => $keymediaarsip,
                        'data' => $media
                    ];
                }
            }elseif($request->filter_6 == 'keterangan'){
                $grupbyketerangan = $collect->groupBy('keterangan');

                foreach ($grupbyketerangan as $keyketerangan => $ket) {
                    $datafilter6[] = [
                        'key' => $keyketerangan,
                        'data' => $ket
                    ];
                }
            }else if($request->filter_6 == 'kode_klasifikasi'){
                $grupbykodeklasifikasi = $collect->groupBy('kode_klasifikasi');

                foreach ($grupbykodeklasifikasi as $keykodeklasifikasi => $kode) {
                    $datafilter6[] = [
                        'key' => $keykodeklasifikasi,
                        'data' => $kode
                    ];
                }
            }else if($request->filter_6 == 'tingkat_perkembangan'){
                $grupbytingkatperkembangan = $collect->groupBy('tingkat_perkembangan');

                foreach ($grupbytingkatperkembangan as $keytingkatperkembangan => $tingkat) {
                    $datafilter6[] = [
                        'key' => $keytingkatperkembangan,                    
                        'data' => $tingkat
                    ];
                }
            } elseif($request->filter_6 == 'kondisi') {
                $grupbykondisi = $collect->groupBy('kondisi');

                foreach ($grupbykondisi as $keykondisi => $kon) {
                    $datafilter6[] = [
                        'key' => $keykondisi,       
                        'data' => $kon
                    ];
                }
            }

            if($request->filter_5 == 'jenis_arsip'){
                $grupbyjenisarsip = $collect->groupBy('jenis_arsip');

                foreach ($grupbyjenisarsip as $keyjenisarsip => $jenis) {
                    $datafilter5[] = [
                        'key' => $keyjenisarsip,
                        'data' => $datafilter6
                    ];
                }
            }else if($request->filter_5 == 'media_arsip'){
                $grupbymediaarsip = $collect->groupBy('media_arsip');

                foreach ($grupbymediaarsip as $keymediaarsip => $media) {
                    $datafilter5[] = [
                        'key' => $keymediaarsip,
                        'data' => $datafilter6
                    ];
                }
            }else if($request->filter_5 == 'keterangan'){
                $grupbyketerangan = $collect->groupBy('keterangan');
                foreach ($grupbyketerangan as $keyketerangan => $ket) {
                    $datafilter5[] = [
                        'key' => $keyketerangan,
                        'data' => $datafilter6
                    ];
                }
            }else if($request->filter_5 == 'kode_klasifikasi'){
                $grupbykodeklasifikasi = $collect->groupBy('kode_klasifikasi');

                foreach ($grupbykodeklasifikasi as $keykodeklasifikasi => $kode) {
                    $datafilter5[] = [
                        'key' => $keykodeklasifikasi,
                        'data' => $datafilter6
                    ];
                }
            }else if($request->filter_5 == 'tingkat_perkembangan'){
                $grupbytingkatperkembangan = $collect->groupBy('tingkat_perkembangan');

                foreach ($grupbytingkatperkembangan as $keytingkatperkembangan => $tingkat) {
                    $datafilter5[] = [
                        'key' => $keytingkatperkembangan,   
                        'data' => $datafilter6                 
                    ];
                }
            } else {
                $grupbykondisi = $collect->groupBy('kondisi');

                foreach ($grupbykondisi as $keykondisi => $kon) {
                    $datafilter5[] = [
                        'key' => $keykondisi,       
                        'data' => $datafilter6
                    ];
                }
            }

            if($request->filter_4 == 'jenis_arsip'){
                $grupbyjenisarsip = $collect->groupBy('jenis_arsip');

                foreach ($grupbyjenisarsip as $keyjenisarsip => $jenis) {
                    $datafilter4[] = [
                        'key' => $keyjenisarsip,
                        'data' => $datafilter5,
                    ];
                }
            }else if($request->filter_4 == 'media_arsip'){
                $grupbymediaarsip = $collect->groupBy('media_arsip');

                foreach ($grupbymediaarsip as $keymediaarsip => $media) {
                    $datafilter4[] = [
                        'key' => $keymediaarsip,
                        'data' => $datafilter5,
                    ];
                }
            }else if($request->filter_4 == 'keterangan'){
                $grupbyketerangan = $collect->groupBy('keterangan');
                foreach ($grupbyketerangan as $keyketerangan => $ket) {
                    $datafilter4[] = [
                        'key' => $keyketerangan,
                        'data' => $datafilter5,
                    
                    ];
                }
            }else if($request->filter_4 == 'kode_klasifikasi'){
                $grupbykodeklasifikasi = $collect->groupBy('kode_klasifikasi');

                foreach ($grupbykodeklasifikasi as $keykodeklasifikasi => $kode) {
                    $datafilter4[] = [
                        'key' => $keykodeklasifikasi,
                        'data' => $datafilter5,
                    ];
                }
            }else if($request->filter_4 == 'tingkat_perkembangan'){
                $grupbytingkatperkembangan = $collect->groupBy('tingkat_perkembangan');

                foreach ($grupbytingkatperkembangan as $keytingkatperkembangan => $tingkat) {
                    $datafilter4[] = [
                        'key' => $keytingkatperkembangan,
                        'data' => $datafilter5,                    
                    ];
                }
            } else {
                $grupbykondisi = $collect->groupBy('kondisi');

                foreach ($grupbykondisi as $keykondisi => $kon) {
                    $datafilter4[] = [
                        'key' => $keykondisi,
                        'data' => $datafilter5,
                    ];
                }
            }

            if($request->filter_3 == 'jenis_arsip'){
                $grupbyjenisarsip = $collect->groupBy('jenis_arsip');

                foreach ($grupbyjenisarsip as $keyjenisarsip => $jenis) {
                    $datafilter3[] = [
                        'key' => $keyjenisarsip,
                        'data' => $datafilter4,
                    ];
                }
            }else if($request->filter_3 == 'media_arsip'){
                $grupbymediaarsip = $collect->groupBy('media_arsip');

                foreach ($grupbymediaarsip as $keymediaarsip => $media) {
                    $datafilter3[] = [
                        'key' => $keymediaarsip,
                        'data' => $datafilter4,
                    ];
                }
            }else if($request->filter_3 == 'keterangan'){
                $grupbyketerangan = $collect->groupBy('keterangan');
                foreach ($grupbyketerangan as $keyketerangan => $ket) {
                    $datafilter3[] = [
                        'key' => $keyketerangan,
                        'data' => $datafilter4,
                    ];
                }
            }else if($request->filter_3 == 'kode_klasifikasi'){
                $grupbykodeklasifikasi = $collect->groupBy('kode_klasifikasi');

                foreach ($grupbykodeklasifikasi as $keykodeklasifikasi => $kode) {
                    $datafilter3[] = [
                        'key' => $keykodeklasifikasi,
                        'data' => $datafilter4,
                    ];
                }
            }else if($request->filter_3 == 'tingkat_perkembangan'){
                $grupbytingkatperkembangan = $collect->groupBy('tingkat_perkembangan');

                foreach ($grupbytingkatperkembangan as $keytingkatperkembangan => $tingkat) {
                    $datafilter3[] = [
                        'key' => $keytingkatperkembangan,
                        'data' => $datafilter4,                    
                    ];
                }
            } else {
                $grupbykondisi = $collect->groupBy('kondisi');

                foreach ($grupbykondisi as $keykondisi => $kon) {
                    $datafilter3[] = [
                        'key' => $keykondisi,
                        'data' => $datafilter4,
                    ];
                }
            }

            if($request->filter_2 == 'jenis_arsip'){
                $grupbyjenisarsip = $collect->groupBy('jenis_arsip');

                foreach ($grupbyjenisarsip as $keyjenisarsip => $jenis) {
                    $datafilter2[] = [
                        'key' => $keyjenisarsip,
                        'data' => $datafilter3,
                    ];
                }
            }else if($request->filter_2 == 'media_arsip'){
                $grupbymediaarsip = $collect->groupBy('media_arsip');

                foreach ($grupbymediaarsip as $keymediaarsip => $media) {
                    $datafilter2[] = [
                        'key' => $keymediaarsip,
                        'data' => $datafilter3,
                    ];
                }
            }else if($request->filter_2 == 'keterangan'){
                $grupbyketerangan = $collect->groupBy('keterangan');
                foreach ($grupbyketerangan as $keyketerangan => $ket) {
                    $datafilter2[] = [
                        'key' => $keyketerangan,
                        'data' => $datafilter3,
                    
                    ];
                }
            }else if($request->filter_2 == 'kode_klasifikasi'){
                $grupbykodeklasifikasi = $collect->groupBy('kode_klasifikasi');

                foreach ($grupbykodeklasifikasi as $keykodeklasifikasi => $kode) {
                    $datafilter2[] = [
                        'key' => $keykodeklasifikasi,
                        'data' => $datafilter3,
                    ];
                }
            }else if($request->filter_2 == 'tingkat_perkembangan'){
                $grupbytingkatperkembangan = $collect->groupBy('tingkat_perkembangan');

                foreach ($grupbytingkatperkembangan as $keytingkatperkembangan => $tingkat) {
                    $datafilter2[] = [
                        'key' => $keytingkatperkembangan, 
                        'data' => $datafilter3,                   
                    ];
                }
            } else {
                $grupbykondisi = $collect->groupBy('kondisi');

                foreach ($grupbykondisi as $keykondisi => $kon) {
                    $datafilter2[] = [
                        'key' => $keykondisi,
                        'data' => $datafilter3,
                    ];
                }
            }

            $datafilter1 = [];
            
            if($request->filter_1 == 'jenis_arsip'){
                $grupbyjenisarsip = $collect->groupBy('jenis_arsip');

                foreach ($grupbyjenisarsip as $keyjenisarsip => $jenis) {
                    $datafilter1[] = [
                        'key' => $keyjenisarsip,
                        'data' => $datafilter2,
                    ];
                }

            }else if($request->filter_1 == 'media_arsip'){
                $grupbymediaarsip = $collect->groupBy('media_arsip');

                foreach ($grupbymediaarsip as $keymediaarsip => $media) {
                    $datafilter1[] = [
                        'key' => $keymediaarsip,
                        'data' => $datafilter2,
                    ];
                }
            }else if($request->filter_1 == 'keterangan'){
                $grupbyketerangan = $collect->groupBy('keterangan');
                foreach ($grupbyketerangan as $keyketerangan => $ket) {
                    $datafilter1[] = [
                        'key' => $keyketerangan,
                        'data' => $datafilter2,
                    
                    ];
                }
            }else if($request->filter_1 == 'kode_klasifikasi'){
                $grupbykodeklasifikasi = $collect->groupBy('kode_klasifikasi');

                foreach ($grupbykodeklasifikasi as $keykodeklasifikasi => $kode) {
                    $datafilter1[] = [
                        'key' => $keykodeklasifikasi,
                        'data' => $datafilter2,
                    ];
                }

            }else if($request->filter_1 == 'tingkat_perkembangan'){
                $grupbytingkatperkembangan = $collect->groupBy('tingkat_perkembangan');

                foreach ($grupbytingkatperkembangan as $keytingkatperkembangan => $tingkat) {
                    $datafilter1[] = [
                        'key' => $keytingkatperkembangan,
                        'data' => $datafilter2,                   
                    ];
                }

            } else {
                $grupbykondisi = $collect->groupBy('kondisi');

                foreach ($grupbykondisi as $keykondisi => $kon) {
                    $datafilter1[] = [
                        'key' => $keykondisi,
                        'data' => $datafilter2,
                    ];
                }
            }

            $filterinstansi = [];
            $alldatainstansi = [];

            if($request->instansi_id == 'ALL'){
                $grupbyinstansi = $collect->groupBy('instansi');
                foreach ($grupbyinstansi as $keyinstansi => $inst) {
                    $filterinstansi[] = [
                        'key' => $keyinstansi,
                        'data' => $datafilter1,
                    ];
                }
            } else {
                $grupbyinstansi = $collect->groupBy('instansi');
                $datainstansi = Instansi::where('id', $request->instansi_id)->first();
                foreach ($grupbyinstansi as $keyinstansi => $inst) {
                    if($keyinstansi == $datainstansi->nama_instansi){
                        $filterinstansi[] = [
                            'key' => $keyinstansi,
                            'data' => $datafilter1,
                        ];
                    }
                }
            }

            $dataresponse = [
                'data' => $filterinstansi,
            ];

            return response()->json($dataresponse);
        }

        $instansi = Instansi::all();

        return view('arsip-identifikasi.index', compact('instansi'));

    }

    public function subinstansi($data)
    {
        $alldatainstansi = [];

        foreach ($data as $key => $value) {
            if(count($value->subinstansi) > 0){
                $this->subinstansi($value->subinstansi);
            } else {
                $alldatainstansi[] = [
                    'key' => $value->nama_subinstansi,
                    'data' => [],
                ];
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
