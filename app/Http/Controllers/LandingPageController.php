<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Infografis;
use App\Models\News;
use App\Models\Performance;
use App\Models\VisiMisi;
use App\Models\StrukturOrganisasi;
use App\Models\TugasFungsi;
use App\Models\Kinerja;
use Illuminate\Support\Facades\Crypt;
use App\Models\WorkUnit;
use App\Models\Category;
use App\Models\FileUpload;
use App\Models\CapaianSPBE;
use App\Models\User;
use App\Models\Chat;
use App\Models\Capaian;
use App\Models\CapaianKinerja;
use Illuminate\Support\Facades\Storage;
use App\Models\ArsipDigital;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class LandingPageController extends Controller
{

    public function index()
    {

        // $datatransdata = DB::table('trans_data')->get();
        // $datamasterkatdata = DB::table('master_katdata')->get();
        // $datamastersubdata = DB::table('master_subdata')->get();
        // $datatransfile = DB::table('trans_files')->get();
        // $datapengeloladata = DB::table('pengelola_data')->get();
        // $datapeople = DB::table('people')->get();

        // foreach ($datatransdata as $value) {

        //     foreach ($datamasterkatdata as $kat) {
        //         if ($kat->idkatdata == $value->idkatdata) {
        //             $kategori = Category::where('name', $kat->dataclass)->first();
        //         }
        //     }

        //     foreach ($datamastersubdata as $sub) {
        //         if ($sub->subdataid == $value->subdataid) {
        //             $subkategori = Category::where('name', $sub->subclassdata)->first();
        //         }
        //     }

        //     $datafile = DB::table('trans_files')->where('idkatdata', $value->idkatdata)->where('tahun', $value->tahun)->first();
        //     if($datafile != null) {
        //         $path = $datafile->path;
        //         $filependukung = Str::remove('./fileupload/', $path);
        //         $storefileupload = FileUpload::create([
        //             'filename' => $filependukung,
        //             'category_id' => $kategori->id,
        //             'year' => $value->tahun,
        //         ]);

        //         $fileuploadid = $storefileupload->id;
        //     } else {
        //         $filependukung = null;
        //         $fileuploadid = null;
        //     }
        //     // foreach ($datatransfile as $file) {
        //     //     if ($file->idkatdata == $value->idkatdata && $file->tahun == $value->tahun) {
        //     //         if($file->path != null || $file->path != '' || $file->path != '-') {
        //     //             $path = $file->path;
        //     //             $filependukung = Str::remove('./fileupload/', $path);
        //     //             $storefileupload = FileUpload::create([
        //     //                 'filename' => $filependukung,
        //     //                 'category_id' => $kategori->id,
        //     //                 'year' => $value->tahun,
        //     //             ]);

        //     //             $fileuploadid = $storefileupload->id;
        //     //         } else {
        //     //             $filependukung = null;
        //     //             $fileuploadid = null;
        //     //         }
        //     //     } else {
        //     //         $filependukung = null;
        //     //         $fileuploadid = null;
        //     //     }
        //     // }

        //     if ($value->verifyed == 0) {

        //         $status = 1;
        //         $pengelolaid = null;
        //         $masterdataid = null;

        //     } elseif($value->verifyed == 1) {
        //         $status = 2;
        //         $pengelolaid = null;
        //         $masterdataid = null;
        //     } elseif($value->verifyed == 2) {
        //         $status = 3;
        //         $datapengolahdata = DB::table('pengelola_data')->where('idkatdata', $value->idkatdata)->first();
        //         $people = DB::table('people')->where('peopleid', $datapengolahdata->peopleid)->first();
        //         $user = User::where('name', $people->peoplename)->where('workunit_id', $people->roleid)->where('username', $people->username)->first();
        //         $pengelolaid = $datapengolahdata->peopleid;
        //         $masterdataid = 12;
        //     } elseif($value->verifyed == 3) {
        //         $status = 4;
        //         $datapengolahdata = DB::table('pengelola_data')->where('idkatdata', $value->idkatdata)->first();
        //         $people = DB::table('people')->where('peopleid', $datapengolahdata->peopleid)->first();
        //         $user = User::where('name', $people->peoplename)->where('workunit_id', $people->roleid)->where('username', $people->username)->first();
        //         $pengelolaid = $datapengolahdata->peopleid;
        //         $masterdataid = 12;
        //     } elseif($value->verifyed == 4) {
        //         $status = 5;
        //         $pengelolaid = null;
        //         $masterdataid = null;
        //     }

        //     //dd($pengelolaid);

        //     $storechat = Chat::create([
        //         'topic' => 'Kinerja' . $kategori->name . 'Tahun' . $value->tahun,
        //     ]);

        //     $produsendata = User::where('workunit_id', $value->roleid)->first();

        //     $storekinerja = Kinerja::create([
        //         'category_id' => $kategori->id,
        //         'sub_category_id' => $subkategori->id,
        //         'workunit_id' => $value->roleid,
        //         'fileupload_id' => $fileuploadid,
        //         'pengolahdata_id' => $pengelolaid,
        //         'year' => $value->tahun,
        //         'total' => $value->jumlah,
        //         'description' => $value->keterangan,
        //         'status' => $status,
        //         'satuan' => $value->satuan,
        //         'masterdata_id' => $masterdataid,
        //         'save_at' => $value->dt_save,
        //         'send_at' => $value->dt_send,
        //         'chat_id' => $storechat->id,
        //         'process_at' => $value->dt_proses,
        //         'verified_at' => $value->dt_verifyed,
        //         'produsendata_id' => $produsendata->id,
        //     ]);

        //     activity('Menambahkan Data Kinerja Baru')
        //         ->performedOn($storekinerja)
        //         ->causedBy($produsendata)
        //         ->log('Menambahkan Data Kinerja');
        // }
        
        $infografis = Infografis::orderBy('created_at', 'desc')->where('status', 1)->take(6)->get();
        $news = News::orderBy('created_at', 'desc')->where('status', 1)->take(6)->get();
        $arsipdigital = ArsipDigital::orderBy('created_at', 'desc')->with('arsipkolektor')->take(6)->get();
        return view('welcome', compact('infografis', 'news', 'arsipdigital'));
    }
    
    public function visimisi()
    {
        $visimisi = VisiMisi::orderBy('created_at', 'asc')->get()->take(1);
        return view('landing-page.visi-misi', compact('visimisi'));
    }

    public function tugasfungsi()
    {
        $tugasfungsi = TugasFungsi::orderBy('created_at', 'asc')->get()->take(1);
        return view('landing-page.tugas-fungsi', compact('tugasfungsi'));
    }

    public function strukturorganisasi()
    {
        $strukturorganisasi = StrukturOrganisasi::orderBy('created_at', 'desc')->get()->take(1);
        return view('landing-page.struktur-organisasi', compact('strukturorganisasi'));
    }

    public function infografis(Request $request)
    {
        if($request->search){
            $infografis = Infografis::where('title', 'ILIKE', '%'.$request->search.'%')
            ->orWhere('content', 'ILIKE', '%'.$request->search.'%')
            ->where('status', 1)
            ->paginate(9);
        }else{
            $infografis = Infografis::orderBy('created_at', 'desc')->paginate(9);
        }
        return view('landing-page.infografis.index', compact('infografis'));
    }

    public function news(Request $request)
    {
        if($request->search){
            $berita = News::where('status', 1)
            ->where('title', 'ILIKE', "%".$request->search."%")
            ->orWhere('content', 'ILIKE', "%".$request->search."%")
            ->paginate(9);
        } else{
            $berita = News::where('status', 1)->paginate(9); 
        }

        return view('landing-page.berita.index', compact('berita'));
    }

    public function capaianspbe(Request $request)
    {
        if($request->search){
            $capaianspbe = CapaianSPBE::where('judul', 'ILIKE', '%'.$request->search.'%')
            ->orWhere('deskripsi', 'ILIKE', '%'.$request->search.'%')
            ->where('status', 1)
            ->paginate(9);
        }else{
            $capaianspbe = CapaianSPBE::orderBy('created_at', 'desc')->paginate(9);
        }
        return view('landing-page.capaianspbe.index', compact('capaianspbe'));
    }

    public function detailcapaianspbe($id)
    {
        $capaianspbe = CapaianSPBE::find(Crypt::decrypt($id));
        views($capaianspbe)->record();
        return view('landing-page.capaianspbe.detail', compact('capaianspbe'));
    } 

    public function capaian(Request $request)
    {
        if($request->search){
            $capaian = Capaian::where('judul', 'ILIKE', '%'.$request->search.'%')
            ->orWhere('deskripsi', 'ILIKE', '%'.$request->search.'%')
            ->where('status', 1)
            ->paginate(9);
        }else{
            $capaian = Capaian::orderBy('created_at', 'desc')->where('status', 1)->paginate(9);
        }
        return view('landing-page.capaian.index', compact('capaian'));
    }

    public function detailcapaian($id)
    {
        $capaian = Capaian::find(Crypt::decrypt($id));
        views($capaian)->record();
        return view('landing-page.capaian.detail', compact('capaian'));
    }

    public function capaiankinerja()
    {
        $capaiankinerja = CapaianKinerja::orderBy('created_at', 'desc')->where('status', 1)->get();
        return view('landing-page.capaian-kinerja.index', compact('capaiankinerja'));
    }

    public function detailcapaiankinerja($id)
    {
        $capaiankinerja = CapaianKinerja::find(Crypt::decrypt($id));
        views($capaiankinerja)->record();
        return view('landing-page.capaian-kinerja.detail', compact('capaiankinerja'));
    }

    public function detailnews($slug)
    {
        $berita = News::where('slug', $slug)->first();
        views($berita)->record();
        return view('landing-page.berita.detail', compact('berita'));
    }

    public function detailinfografis($slug)
    {
        $infografis = Infografis::where('slug', $slug)->first();
        views($infografis)->record();
        return view('landing-page.infografis.detail', compact('infografis'));
    }

    public function unitkerja()
    {
        $unitkerja = WorkUnit::where('code', '!=', '001')->with('category')->get();
        return view('landing-page.unit-kerja.index', compact('unitkerja'));
    }

    public function detailunitkerja($id)
    {
        $idunitkinerja = Crypt::decrypt($id);
        $category = Category::where('workunit_id', $idunitkinerja)->whereNull('parent_id')->orderBy('created_at', 'desc')->get();
        $unitkerja = WorkUnit::where('id', $idunitkinerja)->with('category')->get()->first();
        $workunit = WorkUnit::find($idunitkinerja);
        $kinerja = Kinerja::where('workunit_id', $idunitkinerja)->where('status', 4)->get()->groupBy('category_id');
        $datacategory = [];
        foreach($kinerja as $k){
            foreach($k as $key => $value){
                $datacategory[] = [
                    'category_name' => $value->category->name,
                    'category_id' => $value->category_id,
                ];
            }
        }

        $collection = collect($datacategory);
        $grouped = $collection->groupBy('category_id')->map(function ($row) {
            foreach($row as $key => $value){
                return $value['category_name'];
            }
        });
        $collectioncategory = collect($grouped);
        
        views($workunit)->record();
        return view('landing-page.unit-kerja.detail', compact('category', 'unitkerja', 'kinerja', 'collectioncategory'));
    }

    public function detailkinerja($id)
    {
        $idkinerja = Crypt::decrypt($id);
        $kinerja = Kinerja::find($idkinerja);
        return view('landing-page.detail-kinerja', compact('kinerja'));
    }

    public function getsubCategory(Request $request)
    {
        // $category_id = Crypt::decrypt($request->categoryid);
        $category = Category::where('parent_id', $request->category_id)->where('status', 1)->with(['kinerja','subkinerja'])->get();
        $kinerja = Kinerja::where('category_id', $request->category_id)->where('status', 4)->get()->groupBy('sub_category_id');
        $datasubcategory = [];
        foreach($kinerja as $k){
            foreach($k as $key => $value){
                $datasubcategory[] = [
                    'sub_category_name' => $value->subcategory->name,
                    'sub_category_id' => $value->sub_category_id,
                ];
            }
        }

        $collection = collect($datasubcategory);
        $grouped = $collection->groupBy('sub_category_id')->map(function ($row) {
            foreach($row as $key => $value){
                return $value['sub_category_name'];
            }
        });

        $collectionsubcategory = collect($grouped);

        //$categories = $category->kinerja->where('status', 4)->get();
        return response()->json([ 
            'subcategories' => $collectionsubcategory,
        ]);
    }

    public function getYear(Request $request)
    {
        
        if($request->subcategory_id == 'all')
        {
            $kinerja = Kinerja::where('category_id', $request->category_id)->where('status', 4)->get();
            $year = Kinerja::where('category_id', $request->category_id)->where('status', 4)->select('year')->get();

            $grouped = $kinerja->groupBy('year');
        } else {
            $kinerja = Kinerja::where('sub_category_id', $request->subcategory_id)->where('status', 4)->get();
            $year = Kinerja::where('sub_category_id', $request->subcategory_id)->where('status', 4)->select('year')->get();

            $grouped = $kinerja->groupBy('year');
        }
        return response()->json([
            'kinerja' => $kinerja,
            'year' => $grouped,
        ]);
    }

    public function getKinerja(Request $request)
    {
        if($request->ajax()){
            $start_date = \Carbon\Carbon::createFromFormat('Y', $request->start_date)->format('Y');
            $end_date = \Carbon\Carbon::createFromFormat('Y', $request->end_date)->format('Y');

            if($request->subcategory == 'all'){
                $kinerja = Kinerja::where('category_id', $request->category)
                                ->where('status', 4)
                                ->whereBetween('year', [$start_date, $end_date])->get()->groupBy('year');
            } else {
                $kinerja = Kinerja::where('category_id', $request->category)
                                ->where('sub_category_id', $request->subcategory)
                                ->where('status', 4)
                                ->whereBetween('year', [$start_date, $end_date])->get()->groupBy('year');
            }

            //dd($request->category);

            $data = array();

            $table = '<div class="table-responsive">'; 
            $table .= '<table class="table table-bordered table-hovered" style="width:120%">';
            $table .= '<thead>';
            $table .= '<tr>';
            $table .= '<th>Data Kinerja Unit</th>';
            $table .= '<th>Tahun</th>';
            $table .= '<th>Total</th>';
            // foreach($year as $key => $value){
            //     $table .= '<th>'.$key.'</th>';
            // }
            $table .= '<th>Keterangan</th>';
            $table .= '<th>File Pendukung</th>';
            $table .= '<th>Verifikator</th>';
            $table .= '</tr>';
            $table .= '</thead>';
            $table .= '<tbody>';

            $sumber = '';
            $subcategoryname = '';
            $categoryname = '';

            foreach ($kinerja as $key => $value) {
                if($request->subcategory == 'all'){
                    $datakinerja = Kinerja::where('category_id', $request->category)
                                    ->where('status', 4)
                                    ->where('year', $key)->get();
                } else {
                    $datakinerja = Kinerja::where('category_id', $request->category)
                                    ->where('sub_category_id', $request->subcategory)
                                    ->where('status', 4)
                                    ->where('year', $key)->get();
                }

                $sumber = $value->first()->workunit->name;
                $subcategoryname = $value->first()->subcategory->name;
                $categoryname = $value->first()->category->name;

                foreach ($datakinerja as $key => $item) {
                    $file = FileUpload::where('category_id', $request->category)->get();
                    if($file->count() > 0){
                        if($file->first()->filename == null || $file->first()->filename == ''){
                            $idfile = '-';
                            $filename = '-';
                        } else {
                            $idfile = $file->first()->id;
                            $filename = $file->first()->filename;
                        }
                    } else {
                        $filename = '-';
                    }

                    $data[] = array(
                        'id'            => $item->id,
                        'nama'          => $item->subcategory->name,
                        'tahun'         => $item->year,
                        'total'         => intval($item->total),
                        'satuan'        => $item->satuan,
                        'keterangan'    => $item->description,
                        'file'          => $filename,
                        'idfile'        => $idfile,
                        'year'          => $item->year,
                        'verified'      => $item->pengolahdata->name,
                    );
                }
            }

            $collection = collect($data);
            $grouped = $collection->groupBy('nama')->map(function ($item) {
                    return $item->pluck('total');
            });

            foreach ($grouped as $key => $a) {
                $rowspan = $collection->where('nama', $key)->count();
                $first = true;
                foreach ($collection->where('nama', $key) as $b) {
                    $table .= '<tr>';
                    $totalrow = $collection->where('nama', $key)->count();
                    if($first && $totalrow > 1){
                        $table .= '<td rowspan='.$totalrow.'>'.$key.'</td>';
                        $first = false;
                    } else {
                        if($totalrow == 1){
                            $table .= '<td>'.$key.'</td>';
                        }
                    }
                    $table .= '<td>'.$b['year'].'</td>';
                    $table .= '<td>'.$b['total'].' '.$b['satuan'].'</td>';
                    $table .= '<td>';
                    $content = $b['keterangan'];
                    if($content == null || $content == '' || $content == '-'){
                        $table .= '-';
                    } else {
                        $table .= '<button type="button" class="btn btn-sm btn-primary" data-html="true" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="Keterangan" data-content="'.$content.'"><i class="fas fa-eye"></i></button>';
                    }
                    $table .= '</td>';
                    if($b['file'] == '-'){
                        $table .= '<td>';
                        $table .= $b['file'];
                        $table .= '</td>';
                    } else {
                        $table .= '<td>';
                        $table .= '<a href="'.route('download.file-pendukung', $b['idfile']).'">'.$b['file'].'</a>';
                        $table .= '</td>';
                    }
                    $table .= '<td>';
                    $table .= $b['verified'];
                    $table .= '</td>';
                    $table .= '</tr>';
                }
            }
            $table .= '</tbody>';
            $table .= '</table>';
            $table .= '</div>';


            return response()->json([
                'series' => $grouped,
                'sumber' => $sumber, 
                'table' => $table,
                'category' => $categoryname,
                'subcategory' => $subcategoryname,
                'categories' => $kinerja->keys()->toArray(),
            ]);
        }
    }

    public function getDetailArsip($id)
    {
        $arsipdigital = ArsipDigital::find(Crypt::decrypt($id));
        $bytes = Storage::disk('public')->size('arsip-digital/tmp/'.$arsipdigital->folder.'/'.$arsipdigital->file);
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        } else
        {
            $bytes = '0 bytes';
        }
        $img = $arsipdigital->GetimageByType($arsipdigital->type);
        return response()->json([
            'arsipdigital' => $arsipdigital,
            'instansi' => $arsipdigital->instansi->nama_instansi,
            'arsipkolektor' => $arsipdigital->arsipkolektor,
            'size' => $bytes,
            'link_download' => route('list-arsip-digital.download', $arsipdigital->id),
            'img' => $img,
        ]);

    }
}
