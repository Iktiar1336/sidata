<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Infografis;
use App\Models\News;
use App\Models\ArsipKolektor;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        if($request->category == 'infografis'){
            $category = 'Infografis';
            $search = $request->search;
            $infografis = Infografis::where('title', 'ILIKE', '%'.$request->search.'%')
                                    ->orWhere('content', 'ILIKE', '%'.$request->search.'%')
                                    ->where('status', 1)
                                    ->paginate(9);
            return view('search', compact('infografis', 'category', 'search'));
        }elseif($request->category == 'news'){
            $category = 'Berita';
            $search = $request->search;
            $berita = News::where('status', 1)
                          ->where('title', 'ILIKE', "%".$request->search."%")
                          ->orWhere('content', 'ILIKE', "%".$request->search."%")
                          ->paginate(9);
            return view('search', compact('berita', 'category', 'search'));
        }elseif($request->category == 'arsip'){
            $category = 'Arsip Publik';
            $search = $request->search;
            $arsip = ArsipKolektor::where('uraianitem', 'like', '%'.$request->search.'%')->where('kode_klasifikasi', 'like', '%'. $request->search.'%')->get();
            return view('search', compact('arsip', 'category', 'search'));
        }else{

            return view('search');
        }
    }
}
