<?php

namespace App\Http\Controllers\Metadata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GlosariumMetadata;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class GlosariumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $glosarium = GlosariumMetadata::orderBy('id','asc')->get()->groupBy('metadata');
        $data = [];
        foreach ($glosarium as $key => $value) {

            foreach ($value as $keys => $value) {
                $children[] = [
                    'id' => Crypt::encrypt($value->id),
                    'metadata' => $value->metadata,
                    'istilah' => $value->istilah,
                    'definisi' => $value->definisi,
                ];
            }

            $data[] = [
                'metadata' => $key,
                'children' => $children,
            ];
        }
        $collection = collect($data);
        //dd($collection);
        if ($request->ajax()) {
            return Datatables::of($collection)
                    ->addIndexColumn()
                    ->make(true);
        }

        return view('metadata.glosarium-metadata.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'metadata' => 'required',
            'istilah' => 'required',
            'deskripsi' => 'required',
        ]);

        $glosarium = GlosariumMetadata::create([
            'metadata' => $request->metadata,
            'istilah' => $request->istilah,
            'definisi' => $request->deskripsi,
        ]);

        activity("Menambahkan Glosarium")
            ->causedBy(auth()->user())
            ->performedOn($glosarium)
            ->log("Menambahkan Glosarium untuk metadata {$request->metadata}");

        return redirect()->route('glosarium.index')->with('success', 'Glosarium berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function getDetail(Request $request)
    {
        $name = $request->get('name');
        $glosarium = GlosariumMetadata::where('metadata', $name)->get();
        $data = [];
        foreach ($glosarium as $key => $value) {
            $data[] = [
                'id' => Crypt::encrypt($value->id),
                'metadata' => $value->metadata,
                'istilah' => $value->istilah,
                'definisi' => $value->definisi,
            ];
        }

        $collection = collect($data);
        return response()->json($collection);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $glometa = GlosariumMetadata::find(Crypt::decrypt($id));
        return view('metadata.glosarium-metadata.edit', compact('glometa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'metadata' => 'required',
            'istilah' => 'required',
            'deskripsi' => 'required',
        ]);

        $glosarium = GlosariumMetadata::find(Crypt::decrypt($id));
        $glosarium->update([
            'metadata' => $request->metadata,
            'istilah' => $request->istilah,
            'definisi' => $request->deskripsi,
        ]);

        activity("Mengubah Glosarium")
            ->causedBy(auth()->user())
            ->performedOn($glosarium)
            ->log("Mengubah Glosarium untuk metadata {$request->metadata}");

        return redirect()->route('glosarium.index')->with('success', 'Glosarium berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $glosarium = GlosariumMetadata::find($id);
        $glosarium->delete();

        activity("Menghapus Glosarium")
            ->causedBy(auth()->user())
            ->performedOn($glosarium)
            ->log("Menghapus Glosarium untuk metadata {$glosarium->metadata}");

        return redirect()->route('glosarium.index')->with('success', 'Glosarium berhasil dihapus');
    }
}
