<?php

namespace App\Http\Controllers\Metadata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArsipDigital;
use App\Models\ArsipKolektor;
use DataTables;

class MetaDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = ArsipKolektor::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('instansi', function($row){
                        $instansi = $row->instansi->nama_instansi;
                        return $instansi;
                    })
                    ->addColumn('tanggal_surat', function($row){
                        $tanggal_surat = \Carbon\Carbon::parse($row->tanggal_surat)->format('d-m-Y');
                        $hari = \Carbon\Carbon::parse($row->tanggal_surat)->format('l');
                        $bulan = \Carbon\Carbon::parse($row->tanggal_surat)->format('F');
                        $tahun = \Carbon\Carbon::parse($row->tanggal_surat)->format('Y');
                        $tgl = \Carbon\Carbon::parse($row->tanggal_surat)->format('d');
                        $tgl_surat = $tgl.' '.$bulan.' '.$tahun;
                        return $tgl_surat;
                    })
                    ->make(true);
        }
      
        return view('metadata.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
