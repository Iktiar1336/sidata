<?php

namespace App\Http\Controllers;

use File;
use Storage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\Models\Notification;;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\Http;
use App\Models\SurveyArsip;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'verified']);
        $this->middleware('permission:User Profile|Dashboard');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $survey = SurveyArsip::where('status', '1')->get();
        return view('home', compact('survey'));
    }

    public function readNotification()
    {
        $notification = Notification::where('user_id', auth()->user()->id)->where('read_at', null)->get();
        foreach ($notification as $notif) {
            $notif->read_at = now();
            $notif->save();
        }
        return response()->json(['success' => 'Notification read']);
    }

    public function getNotification()
    {
        $notification = Notification::where('user_id', auth()->user()->id)->where('read_at', null)->count();
        return response()->json(['success' => $notification]);
    }

    public function profile()
    {
        $activity = \Spatie\Activitylog\Models\Activity::where('causer_id', auth()->user()->id)->get();
        return view('users.profile', compact('activity'));
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if(auth()->user()->roles()->first()->name != 'Instansi' && auth()->user()->roles()->first()->name != 'Eksekutif' && auth()->user()->roles()->first()->name != 'admin' && auth()->user()->roles()->first()->name != 'super-admin') {
            $telegram = Http::get('https://api.telegram.org/bot'.env('TELEGRAM_BOT_TOKEN').'/getChatMember?chat_id='.$request->telegram_id.'&user_id='.$request->telegram_id);
            $cektelegram = json_decode($telegram);

            if ($cektelegram->ok == true) {
                $user = User::find(auth()->user()->id);
                $name = auth()->user()->name;
                $email = auth()->user()->email;
                $telegramid = auth()->user()->telegram_id;
                $user->name = $request->name;
                $user->telegram_id = $request->telegram_id;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->username = $request->username;
                $user->save();
                if($request->hasfile(['avatar'])){
                    $request->file(['avatar'])->move('asset/images/avatar/',$request->file(['avatar'])->getClientOriginalName());
                    $user->avatar = $request->file(['avatar'])->getClientOriginalName();
                    $user->save();
                }
                if ($name != $request->name) {
                    activity('Update Profile')
                    ->performedOn($user)
                    ->causedBy(auth()->user())
                    ->log($name. ' Mengubah Nama menjadi '.$request->name);
                } 

                if ($telegramid != $request->telegram_id) {
                    activity('Update Profile')
                    ->performedOn($user)
                    ->causedBy(auth()->user())
                    ->log($name. ' Mengubah Telegram ID menjadi '.$request->telegram_id);
                } 
                
                if ($email != $request->email) {
                    activity('Update Profile')
                    ->performedOn($user)
                    ->causedBy(auth()->user())
                    ->log($name. ' Mengubah Email menjadi '.$request->email);
                }

                // activity('Update Profile')
                //     ->performedOn($user)
                //     ->causedBy(auth()->user())
                //     ->log($user->name . ' Mengupdate profile');
                return redirect()->back()->with('update-profile-success', 'Profile updated successfully');
            } else {
                return redirect()->back()->with('error', 'Telegram ID tidak valid');
            }
        }
        elseif(auth()->user()->roles()->first()->name == 'Instansi'){
            $user = User::find(auth()->user()->id);
            $name = auth()->user()->name;
            $email = auth()->user()->email;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->jabatan = $request->jabatan;
            $user->username = $request->username;
            $user->save();
            if($request->hasfile(['avatar'])){
                $request->file(['avatar'])->move('asset/images/avatar/',$request->file(['avatar'])->getClientOriginalName());
                $user->avatar = $request->file(['avatar'])->getClientOriginalName();
                $user->save();
            }
            if ($name != $request->name) {
                activity('Update Profile')
                ->performedOn($user)
                ->causedBy(auth()->user())
                ->log($name. ' Mengubah Nama menjadi '.$request->name);
            } 
            
            if ($email != $request->email) {
                activity('Update Profile')
                ->performedOn($user)
                ->causedBy(auth()->user())
                ->log($name. ' Mengubah Email menjadi '.$request->email);
            }

            return redirect()->back()->with('update-profile-success', 'Profile updated successfully');
        } else {
            $user = User::find(auth()->user()->id);
            $name = auth()->user()->name;
            $email = auth()->user()->email;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->username = $request->username;
            $user->save();
            if($request->hasfile(['avatar'])){
                $request->file(['avatar'])->move('asset/images/avatar/',$request->file(['avatar'])->getClientOriginalName());
                $user->avatar = $request->file(['avatar'])->getClientOriginalName();
                $user->save();
            }
            if ($name != $request->name) {
                activity('Update Profile')
                ->performedOn($user)
                ->causedBy(auth()->user())
                ->log($name. ' Mengubah Nama menjadi '.$request->name);
            } 
            
            if ($email != $request->email) {
                activity('Update Profile')
                ->performedOn($user)
                ->causedBy(auth()->user())
                ->log($name. ' Mengubah Email menjadi '.$request->email);
            }

            return redirect()->back()->with('update-profile-success', 'Profile updated successfully');
        }
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'password' => ['required', new MatchOldPassword, 'min:8', 'regex:/^(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.* [AZ])(?=.*[az]).*$'],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        activity('Update Password')
            ->performedOn(auth()->user())
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Telah Mengupdate Password');

        $chat_id = auth()->user()->telegram_id;
        $username = auth()->user()->name;
        $tanggal = date('d-m-Y');
        $waktu = date('H:i:s');
        $password = $request->new_password;
        $text = "Halo <a href='tg://user?id=$chat_id'><b>$username</b></a>, \n\n";
        $text .= "Anda telah mengupdate password pada aplikasi <b>SIDATA</b>,";
        $text .= "Berikut detailnya : \n\n";
        $text .= "<b>Tanggal</b>        : $tanggal \n";
        $text .= "<b>Waktu</b>      : $waktu \n";
        $text .= "<b>Password Baru</b>      : $password \n\n";
        $text .= "Silahkan login dengan password baru anda. \n\n";
        $text .= "Jangan berikan password anda kepada siapapun. \n\n";
        $text .= "Terima Kasih.";

        Telegram::sendMessage([
            'chat_id' => $chat_id,
            'parse_mode' => 'HTML',
            'text' => $text
        ]);

        return redirect()->back()->with('update-password-success', 'Password updated successfully.');
    }

}
