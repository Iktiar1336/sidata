<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KolomPertanyaan;
use App\Models\KategoriPertanyaan;
use Illuminate\Support\Facades\Crypt;
use DataTables;


class KolomPertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = KolomPertanyaan::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('kategori', function($row){

                           $kategori = $row->kategoriPertanyaan->judul;

                            return $kategori;
                    })
                    ->addColumn('action', function($row){

                           $btn = '<form action="'.route('kolom-pertanyaan.destroy', $row->id).'" method="POST">'
                                    .csrf_field()
                                    .method_field('DELETE')
                                    .'<a href="'.route('kolom-pertanyaan.edit', $row->id).'" class="edit btn btn-primary btn-sm">Edit</a>'
                                    .'<button type="submit" class="delete btn btn-danger btn-sm">Delete</button>'
                                .'</form>';

                            return $btn;
                    })
                    ->rawColumns(['action', 'kategori', 'deskripsi'])
                    ->make(true);
        }

        $kategoriPertanyaan = KategoriPertanyaan::all();
      
        return view('kolom-pertanyaan.index', compact('kategoriPertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kategori_pertanyaan_id' => 'required',
            'nama_kolom' => 'required',
        ]);

        $kolomPertanyaan = KolomPertanyaan::create([
            'kategoripertanyaan_id' => $request->kategori_pertanyaan_id,
            'nama_kolom' => $request->nama_kolom,
        ]);

        return redirect()->route('kolom-pertanyaan.index')
                        ->with('success','Kolom Pertanyaan created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
