<?php

namespace App\Http\Controllers\API\ArsipKolektor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArsipKolektor;
use App\Http\Requests\ArsipKolektorRequest;
use App\Imports\ArsipKolektorImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\JenisArsip;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Interfaces\ArsipKolektorInterfaces;
use App\Traits\ResponseAPI;

class ArsipKolektorController extends Controller
{
    protected $arsipKolektorInterfaces;

    public function __construct(ArsipKolektorInterfaces $arsipKolektorInterfaces)
    {
        $this->middleware('api');
        $this->arsipKolektorInterfaces = $arsipKolektorInterfaces;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->arsipKolektorInterfaces->GetAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        return $this->arsipKolektorInterfaces->UploadByManual($data);
    }

    public function UploadByExcel(Request $request)
    {
        $data = $request->all();
        $file = $request->file('file');
        return response()->json($file);
        return $this->arsipKolektorInterfaces->UploadByExcel($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
