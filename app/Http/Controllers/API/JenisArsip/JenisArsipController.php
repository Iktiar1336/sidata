<?php

namespace App\Http\Controllers\API\JenisArsip;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JenisArsip;
use App\Http\Resources\JenisArsip\JenisArsipResource;
use App\Interfaces\JenisArsipInterfaces;
use App\Traits\ResponseAPI;

class JenisArsipController extends Controller
{
    protected $jenisArsipInterfaces;

    public function __construct(JenisArsipInterfaces $jenisArsipInterfaces)
    {
        $this->jenisArsipInterfaces = $jenisArsipInterfaces;
    }

    public function index()
    {
        return $this->jenisArsipInterfaces->getAll();
    }
}
