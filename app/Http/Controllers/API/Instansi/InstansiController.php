<?php

namespace App\Http\Controllers\API\Instansi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Instansi;
use App\Interfaces\InstansiInterfaces;
use App\Traits\ResponseAPI;


class InstansiController extends Controller
{
    protected $instansiInterfaces;

    public function __construct(InstansiInterfaces $instansiInterfaces)
    {
        $this->middleware('api');
        $this->instansiInterfaces = $instansiInterfaces;
    }

    public function index()
    {
        return $this->instansiInterfaces->getAll();
    }
}
