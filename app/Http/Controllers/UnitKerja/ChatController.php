<?php

namespace App\Http\Controllers\UnitKerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Notification;
use App\Models\Chat;
use App\Models\Message;
use Telegram\Bot\Laravel\Facades\Telegram;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $chatid = $request->chatid;
            $chat = Chat::find($chatid);
            $messages = Message::where('chat_id', $chatid)->get();

            return response()->json([
                'success' => 'List of messages', 
                'messages' => $messages,
                'profile' => auth()->user()->getAvatar(),
                'user' => auth()->user(),
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    public function getChat(Request $request)
    {
        $chatid = $request->chatid;
        $chat = Chat::find($chatid);
        $messages = Message::where('chat_id', $chatid)->with('sender')->get();
        foreach ($messages as $message) {
            if($message->sender_id == auth()->user()->id){
                $message->sender->avatar = auth()->user()->getAvatar();
                if($message->receiver->avatar != null){
                    $message->receiver->avatar = asset('asset/images/avatar/'.$message->receiver->avatar);
                } else {
                    $message->receiver->avatar = \Avatar::create($message->receiver->name)->toBase64();
                }
            } else {

                if($message->receiver->avatar != null){
                    $message->receiver->avatar = asset('asset/images/avatar/'.$message->receiver->avatar);
                } else {
                    $message->receiver->avatar = \Avatar::create($message->receiver->name)->toBase64();
                }

                if($message->sender->avatar != null){
                    $message->sender->avatar = asset('asset/images/avatar/'.$message->sender->avatar);
                } else {
                    $message->sender->avatar = \Avatar::create($message->sender->name)->toBase64();
                }
            }
        }

        return response()->json([
            'success' => 'List of messages', 
            'messages' => $messages,
            'picture' => auth()->user()->getAvatar(),
            'user' => auth()->user(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chatid = $request->chatid;

        $receiver = User::find($request->receiver_id);

        $sender = User::find(auth()->user()->id);

        $message = Message::create([
            'chat_id' => $chatid,
            'sender_id' => auth()->user()->id,
            'receiver_id' => $request->receiver_id,
            'message' => $request->message,
            'sent_at' => now(),
        ]);

        if ($message->sender_id == auth()->user()->id) {
            $position = 'right';
        } else {
            $position = 'left';
        }

        $chat = Chat::find($chatid);

        $textreceiver = "Hai ".$receiver->name.",\n\n";
        $textreceiver .= "Ada pesan baru dari <b>".$message->sender->name."</b>\n di chat <b>".$chat->topic."</b>.\n\n";
        $textreceiver .= "Untuk data kinerja : <b>" . $chat->topic . "</b>\n\n";
        $textreceiver .= "Silahkan buka aplikasi SIDATA untuk melihat pesan tersebut.\n\n";
        $textreceiver .= "Terima kasih.";

        Telegram::sendMessage([
            'chat_id' => $receiver->telegram_id, 
            'text' => $textreceiver,
            'parse_mode' => 'HTML',
        ]);

        Notification::create([
            'user_id' => $receiver->id,
            'title' => 'Anda mendapatkan pesan baru',
            'message' => 'Anda mendapatkan pesan baru dari '.$sender->name,
        ]);

        $textsender = "Hai ".$sender->name.",\n\n";
        $textsender .= "Pesan anda telah terkirim ke <b>".$message->receiver->name."</b>\n di chat <b>".$chat->topic."</b>.\n\n";
        $textsender .= "Untuk data kinerja : <b>" . $chat->topic . "</b>\n\n";
        $textsender .= "Silahkan buka aplikasi SIDATA untuk melihat pesan tersebut.\n\n";
        $textsender .= "Terima kasih.";

        Telegram::sendMessage([
            'chat_id' => $sender->telegram_id, 
            'text' => $textsender,
            'parse_mode' => 'HTML',
        ]);

        Notification::create([
            'user_id' => $sender->id,
            'title' => 'Pesan anda telah terkirim',
            'message' => 'Pesan anda telah terkirim ke '.$receiver->name,
        ]);

        return response()->json([
            'success' => 'Message sent', 
            'message' => $request->message,
            'picture' => auth()->user()->getAvatar(),
            'position' => $position,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
