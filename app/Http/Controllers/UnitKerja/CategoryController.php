<?php

namespace App\Http\Controllers\UnitKerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\WorkUnit;
use Illuminate\Support\Str;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:List Kategori|Tambah Kategori|Edit Kategori|Hapus Kategori', ['only' => ['index','store']]);
        $this->middleware('permission:Tambah Kategori', ['only' => ['create','store']]);
        $this->middleware('permission:Edit Kategori', ['only' => ['edit','update']]);
        $this->middleware('permission:Hapus Kategori', ['only' => ['destroy']]);

        $this->middleware('permission:List Sub Kategori|Tambah Sub Kategori|Edit Sub Kategori|Hapus Sub Kategori', ['only' => ['index','store']]);
        $this->middleware('permission:Tambah Sub Kategori', ['only' => ['create','store']]);
        $this->middleware('permission:Edit Sub Kategori', ['only' => ['edit','update']]);
        $this->middleware('permission:Hapus Sub Kategori', ['only' => ['destroy']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category = Category::whereNull('parent_id')->get();
        if ($request->ajax()) {
            if (auth()->user()->roles->first()->name == 'super-admin') {
                $data = Category::whereNull('parent_id')->get();
            } else {
                $data = Category::whereNull('parent_id')->where('workunit_id', auth()->user()->workunit_id)->get();
            }
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('workunit', function($row){
                        return $row->workunit->name;
                    })
                    ->addColumn('status', function($row){
                        if ($row->status == 1) {
                            return '<span class="badge badge-success">Aktif</span>';
                        } else {
                            return '<span class="badge badge-danger">Tidak Aktif</span>';
                        }
                        
                    })
                    ->addColumn('access', function($row){
                        if ($row->access == 'Public') {
                            return '<span class="badge badge-success">Publik</span>';
                        } else {
                            return '<span class="badge badge-danger">Tidak Publik</span>';
                        }
                        
                    })
                    ->addColumn('action', function($row){
                        return view('unit-kerja.category.actionbtncategory', compact('row'));
                    })
                    ->rawColumns(['action', 'status', 'access'])
                    ->make(true);
        }
        $workunit = WorkUnit::all();
        return view('unit-kerja.category.index', compact('category', 'workunit'));
    }

    public function subCategory()
    {
        $categories = Category::where('status', 1)->get();
        $allCategories = Category::pluck('name','id')->all();
        $category = Category::whereNull('parent_id')->get()->groupBy('workunit_id');
        $workunit = WorkUnit::all(); 
        //dd($category);
        return view('unit-kerja.category.subcategory', compact('categories','allCategories','category', 'workunit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'workunit_id' => 'required|integer',
            'access' => 'required|string|max:255',
            'status' => 'required|integer',
        ]);

        $category = Category::create([
            'name' => $request->name,
            'workunit_id' => $request->workunit_id,
            'description' => $request->description,
            'access' => $request->access,
            'status' => $request->status,
        ]);

        activity('Membuat Kategori Baru')
                ->performedOn($category)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Menambahkan Kategori Baru : ' . $category->name);

        return redirect()->route('categories.index')->with(['insert-category-success' => 'Kategori berhasil ditambahkan']);
    }

    public function store_subCategory(Request $request)
    {
        $this->validate($request, [
            'parent_id' => 'required|integer',
            'name' => 'required|string|max:100',
        ]);

        $kategori = Category::where('id', $request->parent_id)->first();

        $category = Category::create([
            'name' => $request->name,
            'parent_id' => $request->parent_id,
            'status' => $kategori->status,
            'workunit_id' => $request->workunit_id,
            'description' => $request->name,
        ]);
        activity('Membuat Sub Kategori Baru')
                ->performedOn($category)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Menambahkan Sub Kategori Baru : ' . $category->name);
        return redirect()->route('categories.sub-category')->with(['insert-sub-category-success' => 'Kategori berhasil ditambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idcategory = Crypt::decrypt($id);
        $category = Category::find($idcategory);
        $workunit = WorkUnit::orderBy('created_at', 'DESC')->get();
        return view('unit-kerja.category.edit', compact('category', 'workunit'));
    }

    public function edit_subCategory($id)
    {
        $idcategory = Crypt::decrypt($id);
        $category = Category::orderBy('created_at', 'DESC')->get();
        $subcategory = Category::find($idcategory);
        return view('unit-kerja.category.edit_subcategory', compact('subcategory','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'workunit_id' => 'required|integer',
            'access' => 'required|string|max:255',
            'status' => 'required|integer',
        ]);

        $idcategory = Crypt::decrypt($id);
        $category = Category::find($idcategory);
        $name = $category->name;
        $category->update([
            'name' => $request->name,
            'workunit_id' => $request->workunit_id,
            'description' => $request->description,
            'access' => $request->access,
            'status' => $request->status,
        ]);

        activity('Mengupdate Kategori')
                ->performedOn($category)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Mengupdate Kategori : ' . $name . ' menjadi ' . $request->name);
        return redirect()->route('categories.index')->with(['update-category-success' => 'Kategori berhasil diubah']);
    }

    public function update_subCategory(Request $request, $id)
    {
        $this->validate($request, [
            'parent_id' => 'required|integer',
            'name' => 'required|string|max:100',
        ]);
        $idcategory = Crypt::decrypt($id);
        $category = Category::find($idcategory);
        $name = $category->name;
        $kategoridata = Category::where('parent_id', $category->parent_id)->get()->first();
        $kategoribaru = Category::find($request->parent_id);
        $category->update([
            'name' => $request->name,
            'parent_id' => $request->parent_id,
            'workunit_id' => $request->workunit_id,
            'status' => $kategoribaru->status,
            'description' => $request->name,
        ]);

        activity('Mengupdate Sub Kategori')
                ->performedOn($category)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Telah Mengupdate Sub Kategori' . $name . ' menjadi ' . $request->name . ' Dan ' . $kategoridata->name . ' menjadi ' . $kategoribaru->name);
        return redirect()->route('categories.sub-category')->with(['update-sub-category-success' => 'Kategori berhasil diubah']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idcategory = Crypt::decrypt($id);
        $category = Category::find($idcategory);
        //dd($category->subcategory()->count());
        if($category->subcategory()->count() == 0 && $category->kinerja()->count() == 0){
            activity('Menghapus Kategori')
            ->performedOn($category)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name . ' Menghapus Kategori : ' . $category->name);
            $category->delete();
            return redirect()->route('categories.index')->with(['delete-category-success' => 'Kategori berhasil dihapus']);
        }
        return redirect()->route('categories.index')->with(['delete-category-failed' => 'Kategori tidak bisa dihapus karena masih memiliki sub kategori']);
    }

    public function destroy_subCategory($id)
    {
        $idcategory = Crypt::decrypt($id);
        $category = Category::find($idcategory);
        if($category->subcategory()->count() == 0 && $category->subkinerja()->count() == 0){
            activity('Menghapus Sub Kategori')
                    ->performedOn($category)
                    ->causedBy(auth()->user())
                    ->log($category->name . ' Dihapus Oleh ' . auth()->user()->name);
            $category->delete();
            return redirect()->route('categories.sub-category')->with(['delete-sub-category-success' => 'Kategori berhasil dihapus']);
        }
        return redirect()->route('categories.sub-category')->with(['delete-sub-category-failed' => 'Kategori tidak bisa dihapus karena masih memiliki sub kategori']);
    }
}
