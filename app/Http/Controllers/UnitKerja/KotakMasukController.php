<?php

namespace App\Http\Controllers\UnitKerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kinerja;
use App\Models\Workunit;
use App\Models\Category;
use App\Models\FileUpload;
use App\Models\PengolahData;
use App\Models\Notification;
use App\Models\User;
use App\Models\Chat;
use DB;
use File;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Telegram\Bot\Laravel\Facades\Telegram;

class KotakMasukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Kotak Data Masuk Pengolah Data|Kotak Data Masuk Master Data|Detail Data Kinerja');
    }

    public function index()
    {
        $kinerja = Kinerja::where('status' , '!=' , '1')->orderBy('updated_at', 'DESC')->get()->groupBy(['category_id', 'year']);
        //dd($kinerja);
        $users = User::all();
        return view('unit-kerja.kotak-masuk.index', compact('kinerja','users'));
    }

    public function getDetail(Request $request)
    {
        $kinerja = Kinerja::where('category_id', $request->category_id)
            ->where('year', $request->year)
            ->with('category')
            ->with('workunit')
            ->with('subcategory')
            ->with('fileupload')
            ->with('produsendata')
            ->with('pengolahdata.workunit')
            ->with('masterdata')
            ->get();
        
        $fileupload = FileUpload::where('category_id', $request->category_id)
            ->where('year', $request->year)
            ->get()->first();
        return response()->json([
            'kinerja' => $kinerja,
            'fileupload' => $fileupload
        ]);
    }

    public function sendPenugasan(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'year' => 'required',
            'produsendata_id' => 'required',
            'pengolahdata_id' => 'required',
            'masterdata_id' => 'required',
        ]);

        $category_id = Crypt::decrypt($request->category_id);
        $year = Crypt::decrypt($request->year);
        $produsendataid = Crypt::decrypt($request->produsendata_id);
        $pengolahdataid = Crypt::decrypt($request->pengolahdata_id);
        $masterdataid = Crypt::decrypt($request->masterdata_id);
        $category = Category::find($category_id);
        $kinerja = Kinerja::where('category_id',$category_id)->where('year', $year)->get();
        foreach ($kinerja as $key => $value) {
            $datakinerja = Kinerja::find($value->id);
            $datakinerja->status = 3;
            $datakinerja->process_at = Carbon::now();
            $datakinerja->masterdata_id = $masterdataid;
            $datakinerja->pengolahdata_id = $pengolahdataid;
            $datakinerja->save();
        }

        $produsendata = User::where('id', $produsendataid)->get()->first();
        $message = "Hai, $produsendata->name, \n";
        $message .= "Data Kinerja Dengan Kategori $category->name Untuk Tahun $year Telah Dikirimkan ke pengolah data dan akan segera dilakukan verifikasi. \n";
        $message .= "Terima Kasih";
        
        Telegram::sendMessage([
            'chat_id' => $produsendata->telegram_id,
            'parse_mode' => 'HTML',
            'text' => $message
        ]);

        Notification::create([
            'user_id' => $produsendataid,
            'title' => 'Data Kinerja Anda Akan Diverifikasi',
            'message' => "Data Kinerja Dengan Kategori $category->name Untuk Tahun $year Telah Dikirimkan ke pengolah data dan akan segera dilakukan verifikasi.",
        ]);

        $pengolahdata = User::where('id', $pengolahdataid)->get()->first();
        $text = "Hai, $pengolahdata->name, \n\n";
        $text .= "Anda mendapatkan penugasan untuk memverifikasi data kinerja dengan kategori data $category->name untuk tahun $year. \n\n";
        $text .= "Silahkan login ke sistem untuk memverifikasi data kinerja tersebut. \n\n";
        $text .= "Terima Kasih. \n\n";

        Telegram::sendMessage([
            'chat_id' => $pengolahdata->telegram_id,
            'parse_mode' => 'HTML',
            'text' => $text,
        ]);

        Notification::create([
            'user_id' => $pengolahdataid,
            'title' => 'Anda Mendapatkan Penugasan',
            'message' => "Anda mendapatkan penugasan untuk memverifikasi data kinerja dengan kategori data $category->name untuk tahun $year.",
        ]);

        $masterdata = User::where('id', $masterdataid)->get()->first();

        activity('Mengirim Penugasan Kinerja')
            ->performedOn($datakinerja)
            ->causedBy(auth()->user())
            ->log($masterdata->name . ' Mengirim Penugasan Kinerja Dengan Kategori Data :' . $category->name . ' Untuk Tahun : ' . $year . ' Ke Pengolah Data : ' . $pengolahdata->name);

        return redirect()->route('kinerja.inbox')->with('success', 'Data Kinerja Berhasil Dikirim');

    }

    public function verifikasi(Request $request)
    {
        $category = Category::find($request->category_id);
        $kinerja = Kinerja::where('category_id', $request->category_id)
            ->where('year', $request->year)
            ->where('pengolahdata_id', $request->pengolahdata_id)
            ->get();
        $penerima = '';
        $verifikator = '';
        $telegram_id = '';
        
        foreach ($kinerja as $key => $value) {
            $datakinerja = Kinerja::find($value->id);
            $datakinerja->status = 4;
            $datakinerja->verified_at = Carbon::now();
            $datakinerja->save();
            $penerima = $value->produsendata->name;
            $verifikator = $value->pengolahdata->name;
            $telegram_id = $value->produsendata->telegram_id;
        }

        $message = "Hai, $penerima, \n\n";
        $message .= "Data kinerja $category->name untuk tahun $request->year telah diverifikasi oleh $verifikator. \n\n";
        $message .= "Silahkan login ke aplikasi untuk melihat detailnya. \n\n";
        $message .= "Terima kasih. \n\n";

        Telegram::sendMessage([
            'chat_id' => $telegram_id,
            'parse_mode' => 'HTML',
            'text' => $message,
        ]);

        activity('Memverifikasi Data Kinerja')
                ->performedOn($datakinerja)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Memverifikasi Data Kinerja Untuk Kategori Data : ' . $category->name . ' Untuk Tahun : ' . $request->year);
        
        return redirect()->back()->with('verifikasi-success', 'Data berhasil diverifikasi');

    }

    public function openaccessedit(Request $request)
    {
        $category = Category::find($request->category_id);
        $kinerja = Kinerja::where('category_id', $request->category_id)
            ->where('year', $request->year)
            ->where('pengolahdata_id', $request->pengolahdata_id)
            ->get();
        $penerima = '';
        $verifikator = '';
        $telegram_id = '';
        
        foreach ($kinerja as $key => $value) {
            $datakinerja = Kinerja::find($value->id);
            $datakinerja->status = 5;
            $datakinerja->save();
            $penerima = $value->produsendata->name;
            $verifikator = $value->pengolahdata->name;
            $telegram_id = $value->produsendata->telegram_id;
        }

        $message = "Hai, $penerima, \n\n";
        $message .= "Data kinerja $category->name untuk tahun $request->year telah dibuka kan akses editnya oleh $verifikator. \n\n";
        $message .= "Silahkan lakukan perubahan untuk data kinerja tersebut. \n\n";
        $message .= "Terima kasih. \n\n";

        Telegram::sendMessage([
            'chat_id' => $telegram_id,
            'parse_mode' => 'HTML',
            'text' => $message,
        ]);

        activity('Membuka Akses Data Kinerja')
                ->performedOn($datakinerja)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Membuka Akses Data Kinerja Untuk Kategori Data : ' . $category->name . ' Untuk Tahun : ' . $request->year);
        
        return redirect()->back()->with('open-access-success', 'Data berhasil dibuka aksesnya');
    }
}
