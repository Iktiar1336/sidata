<?php

namespace App\Http\Controllers\UnitKerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kinerja;
use App\Models\Category;
use App\Models\FileUpload;
use DB;

class ChartController extends Controller
{
    public function index()
    {
        $category = Category::all();
        $year = Kinerja::where('status', 4)->orderBy('year', 'asc')->get()->groupBy('year')->keys();
        return view('unit-kerja.chart.index', compact('category','year'));
    }

    public function chart(Request $request)
    {
        $categoryid = $request->category_id;
        $category = Category::find($categoryid);
        $year = Kinerja::where('status', 4)->where('category_id', $categoryid)->orderBy('year', 'ASC')->get()->groupBy('year');
        $data = array(); 
        $table = '<table class="table table-bordered table-striped table-hover">';
        $table .= '<thead>';
        $table .= '<tr>';
        $table .= '<th>Data Kinerja Unit</th>';
        $table .= '<th>Tahun</th>';
        $table .= '<th>Total</th>';
        $table .= '<th>Keterangan</th>';
        $table .= '<th>File Pendukung</th>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $totaldata = $year->count();
        foreach ($year as $key => $values) {
            //$kinerja = Kinerja::where('status', 4)->where('category_id', $categoryid)->where('year', $key)->get();
            
            foreach ($values as $value) {
                $file = FileUpload::where('category_id', $categoryid)->where('year', $value->year)->get();

                if($file->count() > 0){
                    $filename = $file->first()->filename;
                    $fileid = $file->first()->id;
                }else{
                    $filename = '-';
                    $fileid = 0;
                }

                if($value->year == $key){
                    $data[] = [
                                'name'          => $value->subcategory->name,
                                'data'          => intval($value->total),
                                'satuan'        => $value->satuan,
                                'keterangan'    => $value->description,
                                'year'          => $value->year,
                                'file'          => $filename,
                                'idfile'        => $fileid,
                            ];
                } else {
                    $data[] = [
                                'name'          => $value->subcategory->name,
                                'data'          => 0,
                                'satuan'        => $value->satuan,
                                'keterangan'    => $value->description,
                                'year'          => $value->year,
                                'file'          => $filename,
                                'idfile'        => $fileid,
                            ];
                }

                    $tahun[] = [
                        'tahun' => $value->year,
                    ];

                    $subcategory[] = [
                        'id'   => $value->subcategory->id,
                        'name' => $value->subcategory->name,
                    ];
            }

            // foreach ($kinerja as $k) {
            //     $file = FileUpload::where('category_id', $categoryid)->where('year', $k->year)->get();
            //     if($file->count() > 0){
            //         $filename = $file->first()->filename;
            //     } else {
            //         $filename = '-';
            //     }

            //     if($k->year == $key){
            //         $data[] = [
            //             'name'          => $k->subcategory->name,
            //             'data'          => [$k->total],
            //             'satuan'        => $k->satuan,
            //             'keterangan'    => $k->description,
            //             'year'          => $k->year,
            //             'file'          => $filename,
            //         ];
            //     } else {
            //         $data[] = [
            //             'name'          => $k->subcategory->name,
            //             'data'          => [0],
            //             'satuan'        => $k->satuan,
            //             'keterangan'    => $k->description,
            //             'year'          => $k->year,
            //             'file'          => $filename,
            //         ];
            //     }
                
            //     // $data[] = [
            //     //     'name'          => $k->subcategory->name,
            //     //     'data'          => [$k->total],
            //     //     'satuan'        => $k->satuan,
            //     //     'keterangan'    => $k->description,
            //     //     'year'          => $k->year,
            //     //     'file'          => $filename,
            //     // ];

            //     $tahun[] = [
            //         'tahun' => $k->year,
            //     ];

            //     $subcategory[] = [
            //         'id'   => $k->subcategory->id,
            //         'name' => $k->subcategory->name,
            //     ];
            // }
        }

        $collection = collect($data);
        
        $grouped = $collection->groupBy('name')->map(function ($item) {
            return $item->pluck('data');
        });

        $collectiontahun = collect($tahun);
        $groupedtahun = $collectiontahun->groupBy('tahun');


        $collection2 = collect($subcategory);

        $groupcategoryid = $collection2->groupBy('id')->map(function ($item) {
                return $item;
        });

        $groupcategoryname = $collection2->groupBy('name')->map(function ($item) {
                return $item;
        });

        $groupcategory = [
            'id' => $groupcategoryid,
            'name' => $groupcategoryname,
        ];


        foreach ($grouped as $key => $a) {
            $rowspan = $collection->where('name', $key)->count();
            $first = true;
            foreach ($collection->where('name', $key) as $b) {
                $table .= '<tr>';
                $totalrow = $collection->where('name', $key)->count();
                if($first){
                    $table .= '<td rowspan='.$totalrow.'>'.$key.'</td>';
                    $first = false;
                } else {
                    $table .= '<td>'.$b['year'].'</td>';
                    $table .= '<td>'.$b['data'].' '.$b['satuan'].'</td>';
                    $table .= '<td>';
                    $content = $b['keterangan'];
                    $table .= '<button tabindex="0" type="button" class="btn btn-primary btn-sm" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom" data-html="true" data-content="'.$content.'"> <i class="fas fa-eye"></i> </button>';
                    $table .= '</td>';
                    if($b['file'] == '-'){
                        $table .= '<td>';
                        $table .= $b['file'];
                        $table .= '</td>';
                    } else {
                        $table .= '<td>';
                        $table .= '<a href="'.route('download.file-pendukung', $b['idfile']).'">'.$b['file'].'</a>';
                        $table .= '</td>';
                    }
                }
                $table .= '</tr>';
            }
            // $table .= '<tr>';
            // $table .= '<td rowspan='.$totaldata.'>'.$key.'</td>';
            // $table .= '<td>';
            // $table .= '<ul class="list-group list-group-flush">';
            // foreach($collection->where('name', $key) as $b){
            //     $table .= '<li class="list-group-item bg-transparent">'.$b['year'].'</li>';
            // }
            // $table .= '</ul>';
            // $table .= '</td>';
            // $table .= '<td>';
            // $table .= '<ul class="list-group list-group-flush">';
            // foreach ($collection->where('name', $key) as $b) {
            //     foreach ($year as $y => $v) {
            //         if($b['year'] == $y){
            //             $table .= '<li class="list-group-item bg-transparent">'.$b['data'][0].' '.$b['satuan'].'</li>';
            //         }
            //     }
            // }
            // $table .= '</ul>';
            // $table .= '</td>';
            // $table .= '<td>';
            // $table .= '<ul class="list-group list-group-flush">';
            // foreach ($collection->where('name', $key) as $b) {
            //     $filepath = public_path('uploads/kinerja/'.$b['file']);
            //     $table .= '<li class="list-group-item bg-transparent">';
            //     $content = $b['keterangan'];
            //     $table .= '<button tabindex="0" type="button" class="btn btn-primary btn-sm" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom" data-html="true" data-content="'.$content.'"> <i class="fas fa-eye"></i> </button>';
            //     // $table .= $b['keterangan'];
            //     $table .= '</li>';
            // }
            // $table .= '</ul>';
            // $table .= '</td>';
            // $table .= '<td>';
            // $table .= '<ul class="list-group list-group-flush">';
            // foreach ($collection->where('name', $key) as $b) {
            //     $filepath = public_path('uploads/kinerja/'.$b['file']);
            //     if($b['file'] == '-'){
            //         $table .= '<li class="list-group-item bg-transparent">';
            //         $table .= $b['file'];
            //         $table .= '</li>';
            //     } else {
            //         $table .= '<li class="list-group-item bg-transparent">';
            //         $table .= '<a href="'.route('download.file-pendukung', $b['idfile']).'">'.$b['file'].'</a>';
            //         $table .= '</li>';
            //     }
            // }
            // $table .= '</ul>';
            // $table .= '</td>';
            // $table .= '</tr>';
        }
        $table .= '</tbody>';
        $table .= '</table>';

        $tableprediksi = '<table class="table table-bordered table-striped table-hover" id="table-kinerja">';
        $tableprediksi .= '<thead>';
        $tableprediksi .= '<tr>';
        $tableprediksi .= '<th>Data Kinerja Unit</th>';
        $tableprediksi .= '<th>Tahun</th>';
        $tableprediksi .= '<th>X</th>';
        $tableprediksi .= '<th>Y</th>';
        $tableprediksi .= '<th>XX</th>';
        $tableprediksi .= '<th>XY</th>';
        $tableprediksi .= '</tr>';
        $tableprediksi .= '</thead>';
        $tableprediksi .= '<tbody>';


        $selecttahun = '<select class="form-control" name="tahun" id="tahunprediksi">';
        $selecttahun .= '<option value="">Pilih Tahun</option>';
        for($i =1; $i<=10; $i++){
            $selecttahun .= '<option value="'.($i).'">'.($i).'</option>';
        }
        $selecttahun .= '</select>';
        
        return response()->json(
            [
                'category' => $value->first()->category->name,
                'sumber' => $value->first()->workunit->name,
                'satuan' => $value->first()->satuan,
                'series' => $grouped,
                'tahun' => $selecttahun,
                'groupcategoryid' => $groupcategoryid,
                'groupcategoryname' => $groupcategoryname,
                'table' => $table,
                'categories' => $groupedtahun->keys(),
            ]
        );
    }

    public function downloadFilePendukung($id)
    {
        $file = FileUpload::find($id);
        $filepath = public_path() . '/uploads/kinerja/' . $file->filename;
        return response()->download($filepath);
    }
}
