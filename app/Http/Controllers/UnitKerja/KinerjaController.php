<?php

namespace App\Http\Controllers\UnitKerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kinerja;
use App\Models\Workunit;
use App\Models\Category;
use App\Models\FileUpload;
use App\Models\PengolahData;
use App\Models\Notification;
use App\Models\User;
use App\Models\Chat;
use DB;
use File;
use DataTables;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Telegram\Bot\Laravel\Facades\Telegram;

class KinerjaController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:List Data Kinerja|Tambah Data Kinerja|Edit Data Kinerja|Hapus Data Kinerja|Detail Data Kinerja', ['only' => ['index','store','detail']]);
        $this->middleware('permission:Tambah Data Kinerja', ['only' => ['create','store']]);
        $this->middleware('permission:Edit Data Kinerja', ['only' => ['edit','update']]);
        $this->middleware('permission:Hapus Data Kinerja', ['only' => ['destroy']]);
        // $this->middleware('permission:Grafik Data Kinerja', ['only' => ['grafik']]);
        $this->middleware('permission:Detail Data Kinerja', ['only' => ['show']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $datakinerja = Kinerja::orderBy('created_at', 'DESC')->get()->groupBy(['year']);
            $data = array(); 
            foreach ($datakinerja as $key => $values) {
                //$kinerja = Kinerja::where('status', 4)->where('category_id', $categoryid)->where('year', $key)->get();
                
                foreach ($values as $value) {
                    $file = FileUpload::where('category_id', $categoryid)->where('year', $value->year)->get();
    
                    if($file->count() > 0){
                        $filename = $file->first()->filename;
                        $fileid = $file->first()->id;
                    }else{
                        $filename = '-';
                        $fileid = 0;
                    }
    
                    if($value->year == $key){
                        $data[] = [
                                    'name'          => $value->subcategory->name,
                                    'data'          => [$value->total],
                                    'satuan'        => $value->satuan,
                                    'keterangan'    => $value->description,
                                    'year'          => $value->year,
                                    'file'          => $filename,
                                    'idfile'        => $fileid,
                                ];
                    } else {
                        $data[] = [
                                    'name'          => $value->subcategory->name,
                                    'data'          => [0],
                                    'satuan'        => $value->satuan,
                                    'keterangan'    => $value->description,
                                    'year'          => $value->year,
                                    'file'          => $filename,
                                    'idfile'        => $fileid,
                                ];
                    }
    
                        $tahun[] = [
                            'tahun' => $value->year,
                        ];
    
                        $subcategory[] = [
                            'id'   => $value->subcategory->id,
                            'name' => $value->subcategory->name,
                        ];
                }
            }

            $collection = collect($data);
            $grouped = $collection->groupBy('name')->map(function ($item) {
                return $item->pluck('data');
            });

            return Datatables::of($grouped)
                    ->addColumn('category', function($row) use ($key){
                        $rowspan = $collection->where('name', $key)->count();
                        $first = true;
                        
                    })
                    ->addColumn('subcategory', function($row){
                        foreach($row as $data){
                            $listgroup = '<ul class="list-group list-group-flush">';
                            foreach($data as $d){
                                $subcategoryname = $d->subcategory->name;
                                $listgroup .= '<li class="list-group-item">'.$subcategoryname.'</li>';
                            }
                            $listgroup .= '</ul>';
                            return $listgroup;
                            
                        }
                    })
                    ->addColumn('year', function($row){
                        foreach($row as $data){
                            $listgroup = '<ul class="list-group list-group-flush">';
                            foreach($data as $d){
                                $year = $d->year;
                                $listgroup .= '<li class="list-group-item">'.$year.'</li>';
                            }
                            $listgroup .= '</ul>';
                            return $listgroup;
                        }
                    })
                    ->addColumn('jumlah', function($row){
                        foreach($row as $data){
                            $listgroup = '<ul class="list-group list-group-flush">';
                            foreach($data as $d){
                                $jumlah = $d->total;
                                $listgroup .= '<li class="list-group-item d-inline">'.$jumlah. ' ' .$d->satuan . '</li>';
                            }
                            $listgroup .= '</ul>';
                            return $listgroup;
                        }
                    })
                    ->addColumn('keterangan', function($row){
                        foreach($row as $data){
                            $listgroup = '<ul class="list-group list-group-flush">';
                            foreach($data as $d){
                                if($d->keterangan == null){
                                    $keterangan = '-';
                                }else{
                                    $keterangan = $d->description;
                                }
                                $listgroup .= '<li class="list-group-item">'.$keterangan.'</li>';
                            }
                            $listgroup .= '</ul>';
                            return $listgroup;
                        }
                    })
                    ->addColumn('status', function($row){
                        foreach($row as $data){
                            foreach($data as $d){
                                if($d->status == 1){
                                    $status = '<span class="badge badge-primary">Tersimpan</span>';
                                }elseif($d->status == 2){
                                    $status = '<span class="badge badge-info">Terkirim</span>';
                                }elseif($d->status == 3){
                                    $status = '<span class="badge badge-warning">Sedang Di Verifikasi</span>';
                                }elseif($d->status == 4){
                                    $status = '<span class="badge badge-success">Terverifikasi</span>';
                                } elseif($d->status == 5){
                                    $status = '<span class="badge badge-warning">Perlu Di Tinjau</span>';
                                } elseif ($d->status == 6){
                                    $status = '<span class="badge badge-info">Tinjauan Terkirim</span>';
                                }
                            }
                            return $status;
                        }
                    })
                    ->addColumn('action', function($row){
                        foreach($row as $data){
                            foreach($data as $d){
                                if($d->produsendata_id == auth()->user()->id){
                                    $actionBtn = 
                                    '
                                    <form action="'.route('kinerja.destroy', $d->id).'" method="POST">
                                        <input type="hidden" name="year" value="'.$d->year.'">
                                        <input type="hidden" name="kinerja_id" value="'.$d->id.'">
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(\'Apakah Anda Yakin Ingin Menghapus Data Ini?\')"><i class="fas fa-trash"></i></button>
                                    ';

                                    if($d->status == 1){
                                        $actionBtn .= '
                                        <a href="'.route('kinerja.edit', $d->id).'" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                                        <button type="submit" class="btn btn-success btn-sm" onclick="return confirm(\'Apakah Anda Yakin Ingin Mengirim Data Ini?\')"><i class="fas fa-paper-plane"></i></button>
                                        ';
                                    }elseif($d->status == 2){
                                        $actionBtn .= '
                                        <button type="submit" class="btn btn-warning btn-sm" onclick="return confirm(\'Apakah Anda Yakin Ingin Membatalkan Pengiriman Data Ini?\')"><i class="fas fa-times"></i></button>
                                        ';
                                    }elseif($d->status == 3){
                                        $actionBtn .= '
                                        <button type="submit" class="btn btn-warning btn-sm" onclick="return confirm(\'Apakah Anda Yakin Ingin Membatalkan Pengiriman Data Ini?\')"><i class="fas fa-times"></i></button>
                                        ';
                                    }
                                }
                            }
                            return $actionBtn;
                        }
                    })
                    ->rawColumns(['action', 'status', 'year', 'keterangan', 'jumlah', 'subcategory'])
                    ->make(true);
        }
        $kinerja = Kinerja::orderBy('created_at', 'DESC')->get()->groupBy(['category_id', 'year']);
        //dd($kinerja);
        $category = Category::orderBy('created_at', 'DESC')->get();
        return view('unit-kerja.kinerja.index', compact('kinerja', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::where('status', 1)->whereNull('parent_id')->get();
        return view('unit-kerja.kinerja.create', compact('category'));
    }

    public function getsubCategory(Request $request)
    {
        // $category_id = Crypt::decrypt($request->categoryid);
        $category = Category::where('id', $request->category_id)->where('status', 1)->with(['subcategory','subcategory.subcategory'])->get();

        return response()->json([
            'subcategories' => $category,
        ]);
    }

    public function fetchsubCategory(Request $request)
    {
        // $category_id = Crypt::decrypt($request->category_id);
        $data['subcategories'] = Category::where('id', $request->category_id)->where('status', 1)->with(['subcategory','subcategory.subcategory'])->get();
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'user_id' => 'required',
            'workunit_id' => 'required',
            'total' => 'required',
            'year' => 'required',
            'file' => 'mimes:xls,xlsx,csv|max:2048',
        ]);
        
        $datakinerja = Kinerja::select('*')
            ->where('category_id', last($request->category_id))
            ->where('sub_category_id', $request->subcategory_id)
            ->where('year', $request->year)
            ->get();

        $category = Category::where('id', last($request->category_id))->first();
        
        if (count($datakinerja) > 0) {
            return redirect()->back()->with('error', 'Data Kinerja Sudah Ada, Silahkan Masukkan Data Kinerja Yang Lain');
        } else {
            $chat = Chat::create([
                'topic' => 'Kinerja '.$category->name.' Tahun '.$request->year,
            ]);

            $workunit = Workunit::find($request->workunit_id);

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $file->move(public_path('uploads/kinerja'), 'Data Kinerja '.$workunit->name.' '.$request->year.' '.$filename);
                $fileupload = new FileUpload();
                $fileupload->filename = $filename;
                $fileupload->category_id = last($request->category_id);
                $fileupload->year = $request->year;
                $fileupload->save();
            }

            for($i = 0; $i < count($request->subcategory_id); $i++){
                $kinerja = new Kinerja();
                $kinerja->category_id = last($request->category_id);
                $kinerja->sub_category_id = $request->subcategory_id[$i];
                $kinerja->workunit_id = $request->workunit_id;
                $kinerja->total = $request->total[$i];
                $kinerja->year = $request->year;
                $kinerja->status = 1;
                $kinerja->produsendata_id = $request->user_id;
                $kinerja->chat_id = $chat->id;
                $kinerja->satuan = $request->satuan[$i];
                if($request->description[$i] == null){
                    $kinerja->description = '-';
                }else{
                    $kinerja->description = $request->description[$i];
                }
                $kinerja->save_at = date('Y-m-d H:i:s');
                if ($request->hasFile('file')) {
                    $kinerja->fileupload_id = $fileupload->id;
                }
                $kinerja->save();
            }

            activity('Menambahkan Data Kinerja Baru')
                ->performedOn($kinerja)
                ->causedBy(auth()->user())
                ->log('Menambahkan Data Kinerja');

            return redirect()->route('kinerja.index')->with('insert', 'Data Kinerja Berhasil Ditambahkan');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $kinerja = Kinerja::where('category_id', $request->category_id)
            ->where('year', $request->year)
            ->with('category')
            ->with('workunit')
            ->with('subcategory')
            ->with('fileupload')
            ->with('produsendata')
            ->with('pengolahdata.workunit')
            ->with('masterdata')
            ->get();
        
        $fileupload = FileUpload::where('category_id', $request->category_id)
            ->where('year', $request->year)
            ->get()->first();
        return response()->json([
            'kinerja' => $kinerja,
            'fileupload' => $fileupload
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $datakinerja = Kinerja::find($id);
        $kinerja = Kinerja::where('category_id', $datakinerja->category_id)->where('year', $datakinerja->year)->where('produsendata_id', $datakinerja->produsendata_id)->get();
        $category = Category::with('subcategory')->get();
        return view('unit-kerja.kinerja.edit', compact('kinerja', 'category', 'datakinerja'));
    }

    public function updateKinerja(Request $request)
    {
        $datakinerja = Kinerja::find($request->id);
        $datakinerja->total = $request->total;
        $datakinerja->save();

        activity('Mengubah Data Kinerja')
            ->performedOn($datakinerja)
            ->causedBy(auth()->user())
            ->log('Mengubah Data Kinerja');

        return response()->json([
            'success' => 'Data Kinerja Berhasil Diubah',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'subcategory_id' => 'required',
            'workunit_id' => 'required',
            'total' => 'required',
            'year' => 'required',
            'file' => 'mimes:xls,xlsx|max:2048',
        ]);

        $category_id = Crypt::decrypt($id);
        $kinerja = Kinerja::where('category_id', $category_id)
            ->where('workunit_id', $request->workunit_id)
            ->where('year', $request->year)
            ->where('produsendata_id', $request->user_id)
            ->get();
        
        if(count($kinerja) > 0){
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $file->move(public_path('uploads/kinerja'), $filename);
                $fileupload = FileUpload::where('category_id', $category_id)
                    ->where('year', $request->year)
                    ->get()->first();
                File::delete(public_path('uploads/kinerja') . $fileupload->filename);
                $fileupload->filename = $filename;
                $fileupload->update();
            }
            foreach ($kinerja as $key => $value) {
                $datakinerja = Kinerja::find($value->id);
                $datakinerja->total = $request->total[$key];
                $datakinerja->year = $request->year;
                $datakinerja->satuan = $request->satuan[$key];
                if($datakinerja->status == 5){
                    $datakinerja->status = 3;
                }
                if($request->description[$key] == null){
                    $datakinerja->description = '-';
                }else{
                    $datakinerja->description = $request->description[$key];
                }
                if ($request->hasFile('file')) {
                    $datakinerja->fileupload_id = $fileupload->id;
                }
                $datakinerja->save();
            }
            
            activity('Mengubah Data Kinerja')
                ->performedOn($datakinerja)
                ->causedBy(auth()->user())
                ->log('Mengubah Data Kinerja');

            return redirect()->route('kinerja.index')->with('update', 'Data Kinerja Berhasil Diubah');
        }

        // activity('Mengubah Data Kinerja')
        //         ->performedOn($kinerja)
        //         ->causedBy(auth()->user())
        //         ->log(auth()->user()->name . ' Mengubah Data Kinerja Dengan Kategori Data :' . $category->name . ' Untuk Tahun : ' . $request->year);

        // return redirect()->route('kinerja.index')->with('update-work-unit-success', 'Data berhasil diubah');
    }

    public function kotakmasuk()
    {
        $kinerja = Kinerja::where('status' , '!=' , '1')->orderBy('updated_at', 'DESC')->get()->groupBy(['category_id', 'year']);
        //dd($kinerja);
        $users = User::all();
        return view('unit-kerja.kotak-masuk.index', compact('kinerja','users'));
    }

    public function detail(Request $request)
    {
        
    }

    public function send(Request $request, $id)
    {
        $idcategory = Crypt::decrypt($id);
        $category = Category::find($idcategory);
        $kinerja = Kinerja::where('category_id', $idcategory)->where('year', $request->year)->get();
        $pengirim = '';
        $unitkerja = '';
        $telegram_id = auth()->user()->telegram_id; 
        foreach ($kinerja as $key => $value) {
            $datakinerja = Kinerja::find($value->id);
            $datakinerja->status = 2;
            $datakinerja->send_at = Carbon::now();
            $datakinerja->save();
            $pengirim = $value->produsendata->name;
            $unitkerja = $value->workunit->name;
        }

        $message = "Hai, $pengirim, \n\n";
        $message .= "Terima kasih telah mengirimkan data kinerja $category->name untuk tahun $request->year. \n\n";
        $message .= "Data kinerja $category->name untuk tahun $request->year telah dikirim ke master data. \n\n";
        $message .= "Master data akan segara memproses data kinerja anda, mohon menunggu. \n\n";
        $message .= "Anda akan menerima notifikasi jika data kinerja yang anda kirimkan telah diproses oleh master data. \n\n";
        $message .= "Terima kasih. \n\n";

        Notification::create([
            'user_id' => auth()->user()->id,
            'title' => 'Data Kinerja Berhasil Dikirim',
            'message' => "Data Kinerja Dengan Kategori $category->name Untuk Tahun $request->year Telah Dikirim Ke Master Data",
        ]);

        Telegram::sendMessage([
            'chat_id' => $telegram_id,
            'parse_mode' => 'HTML',
            'text' => $message,
        ]);

        $users = User::all();
        foreach ($users as $key => $user) {
            if($user->hasRole('master-data')){
                $notification = Notification::create([
                    'user_id' => $user->id,
                    'title' => 'Kotak Data Masuk Baru',
                    'message' => "Anda memiliki data kotak masuk kinerja dari $pengirim dari unit kerja $unitkerja",
                ]);
                $chat_id = $user->telegram_id;
                $text = "Hai, $user->name, \n\n";
                $text .= "Anda memiliki kotak data masuk baru dari unit kerja $unitkerja, berikut detailnya : \n\n";
                $text .= "Kategori Data : $category->name \n";
                $text .= "Tahun         : $request->year \n";
                $text .= "Pengirim      : $pengirim \n\n";
                $text .= "Silahkan login ke aplikasi untuk melihat detailnya. \n\n";
                $text .= "Terima Kasih \n";

                Telegram::sendMessage([
                    'chat_id' => $chat_id,
                    'text' => $text,
                    'parse_mode' => 'HTML'
                ]);
            }
        }

        activity('Mengirim Data Kinerja')
                ->performedOn($datakinerja)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Mengirimkan Data Kinerja Untuk Kategori Data : ' . $category->name . ' Untuk Tahun : ' . $request->year);
        return redirect()->route('kinerja.index')->with('send-work-unit-success', 'Data berhasil dikirim');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $idcategory = Crypt::decrypt($id);
        $category = Category::find($idcategory);
        $kinerja = Kinerja::where('category_id', $idcategory)->where('year', $request->year)->get();
        $fileupload = FileUpload::where('category_id', $idcategory)->where('year', $request->year)->get()->first();
        foreach($kinerja as $k){
            if($k->status == 1){
                if($k->fileupload != null){
                    File::delete(public_path('uploads/kinerja').$k->fileupload->filename);
                    $fileupload->delete();
                }
                $k->delete();
            } else {
                return redirect()->route('kinerja.index')->with('delete-work-unit-failed', 'Data tidak dapat dihapus');
            }
        }
        activity('Menghapus Kinerja')
                ->performedOn($k)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name . ' Menghapus Kinerja Untuk Kategori Data ' . $category->name . ' Untuk Tahun : ' . $request->year);
        return redirect()->route('kinerja.index')->with('delete-work-unit-success', 'Data berhasil dihapus');

        // $category = Category::where('id', $kinerja->category_id)->with('subcategory')->get()->first();

        // dd($category);
        // $subcategory = $category->subcategory;   
        // for($i=0; $i<count($subcategory); $i++){
        //     $fileupload = FileUpload::where('category_id', $kinerja->category_id)->where('year', $kinerja->year)->get();
        //     dd($fileupload);
        //     $kinerjas = Kinerja::where('category_id', $kinerja->category_id)->where('sub_category_id', $subcategory[$i]->id)->where('year', $kinerja->year)->get()->first();
        //     if ($kinerjas->status == 1) {
        //         activity('Menghapus Kinerja')
        //         ->performedOn($kinerja)
        //         ->causedBy(auth()->user())
        //         ->log(auth()->user()->name . ' Menghapus Kinerja Untuk Kategori Data ' . $subcategory[$i]->name);
        //         $kinerjas->delete();
        //         if (count($fileupload) > 0) {
        //             File::delete(public_path('uploads/kinerja').$fileupload->filename);
        //             $fileupload->delete();
        //         }
        //     } else {
        //         return redirect()->route('kinerja.index')->with('delete-work-unit-failed', 'Data tidak dapat dihapus karena sudah diverifikasi');
        //     }
        // }
        // return redirect()->route('kinerja.index')->with('delete-work-unit-success', 'Data berhasil dihapus');

        
    }
}
