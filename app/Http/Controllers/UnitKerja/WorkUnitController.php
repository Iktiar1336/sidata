<?php

namespace App\Http\Controllers\UnitKerja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\WorkUnit;
use Illuminate\Support\Facades\Crypt;

class WorkUnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:workunit-list|workunit-create|workunit-edit|workunit-delete', ['only' => ['index','store']]);
        $this->middleware('permission:workunit-create', ['only' => ['create','store']]);
        $this->middleware('permission:workunit-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:workunit-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workunit = WorkUnit::orderBy('created_at', 'DESC')->get();
        $unitkerja = WorkUnit::orderBy('id', 'DESC')->get()->first();
        if($unitkerja == null){
            $kode = '001';
        }else{
            $kode = str_pad($unitkerja->id + 1, 3, '0', STR_PAD_LEFT);
        }
        return view('unit-kerja.index', compact('workunit', 'kode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:work_units',
            'name' => 'required|string|max:100',
            'description' => 'required|string|max:255',
            'status' => 'required|integer|max:5',
        ]);

        $workunit = WorkUnit::create([
            'code' => $request->code,
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
        ]);

        activity('Membuat Unit Kerja Baru')
            ->performedOn($workunit)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name.' Membuat Unit Kerja Baru: '.$workunit->name);

        return redirect()->route('work-units.index')->with(['insert-work-unit-success' => 'Unit Kerja berhasil ditambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workunitid = Crypt::decrypt($id);
        $workunit = WorkUnit::find($workunitid);
        return view('unit-kerja.edit', compact('workunit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required|string|max:100',
            'name' => 'required|string|max:100',
            'description' => 'required|string|max:255',
            'status' => 'required|integer|max:5',
        ]);

        $idworkunit = Crypt::decrypt($id);
        $workunit = WorkUnit::find($idworkunit);
        $workunit->update([
            'code' => $request->code,
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
        ]);

        activity('Mengupdate Unit Kerja')
            ->performedOn($workunit)
            ->causedBy(auth()->user())
            ->log(auth()->user()->name.' Mengupdate Unit Kerja: '.$workunit->name);
        
        return redirect()->route('work-units.index')->with(['update-work-unit-success' => 'Unit Kerja berhasil diubah']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idworkunit = Crypt::decrypt($id);
        $workunit = WorkUnit::findOrFail($idworkunit);
        if($workunit->category->count() == 0 && $workunit->user->count() == 0){
            activity('Menghapus Unit Kerja')
                ->performedOn($workunit)
                ->causedBy(auth()->user())
                ->log(auth()->user()->name.' Menghapus Unit Kerja: '.$workunit->name);
            $workunit->delete();
            return redirect()->route('work-units.index')->with(['delete-work-unit-success' => 'Kategori berhasil dihapus']);
        }

        return redirect()->route('work-units.index')->with(['delete-work-unit-failed' => 'Kategori tidak bisa dihapus karena masih memiliki sub kategori']);
        
    }
}
