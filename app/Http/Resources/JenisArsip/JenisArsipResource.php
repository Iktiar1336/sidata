<?php

namespace App\Http\Resources\JenisArsip;

use Illuminate\Http\Resources\Json\JsonResource;

class JenisArsipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    
    public function toArray($request)
    {
        return [
            'status' => 'success',
            'code' => 200,
            'data' => [
                'id' => $this->id,
                'nama' => $this->nama,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
        ];
    }
}
