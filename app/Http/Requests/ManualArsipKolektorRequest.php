<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManualArsipKolektorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'instansi_id' => 'required|numeric|exists:instansi,id',
            'jenis_arsip_id' => 'required|numeric|exists:jenis_arsip,id',
            'uraian_item' => 'required',
            'nomor_surat' => 'required',
            'tanggal_surat' => 'required', 
            'kode_klasifikasi' => 'required',
            'tingkat_perkembangan' => 'required',
            'media_arsip' => 'required',
            'kondisi' => 'required',
            'jumlah' => 'required|numeric',
            'keterangan' => 'required',
        ];
        
    }
}
