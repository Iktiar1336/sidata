<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisArsip extends Model
{
    use HasFactory;

    protected $table = 'jenis_arsip';

    protected $fillable = [
        'nama',
    ];

    public function ArsipKolektor()
    {
        return $this->hasMany(ArsipKolektor::class, 'jenisarsip_id', 'id');
    }
}
