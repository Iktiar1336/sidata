<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;

    protected $table = 'pertanyaan';

    protected $fillable = [
        'kolompertanyaan_id',
        'is_rowspan',
        'is_colspan',
        'rowspan',
        'colspan',
        'is_required',
        'is_text',
        'text',
        'is_input',
        'input_type',
        'is_select',
        'kategoripertanyaan_id'
    ];

    public function kolomPertanyaan()
    {
        return $this->belongsTo(KolomPertanyaan::class, 'kolompertanyaan_id');
    }

    public function Optionselect()
    {
        return $this->hasMany(OptionSelect::class, 'pertanyaan_id');
    }
}
