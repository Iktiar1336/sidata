<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileUpload extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id', 'filename', 'year'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
