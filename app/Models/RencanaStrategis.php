<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RencanaStrategis extends Model
{
    use HasFactory;

    protected $table = 'rencana_strategis';

    protected $fillable = [
        'tahun',
    ];

    public function sasaranstrategis()
    {
        return $this->belongsToMany(SasaranStrategis::class, 'rencanastrategis_sasaranstrategis', 'rencanastrategis_id', 'sasaranstrategis_id');
    }

    public function tujuan()
    {
        return $this->belongsToMany(Tujuan::class, 'rencanastrategis_tujuan', 'rencanastrategis_id', 'tujuan_id');
    }

    public function modelkinerja()
    {
        return $this->belongsToMany(ModelKinerja::class);
    }
}
