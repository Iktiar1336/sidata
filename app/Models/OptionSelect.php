<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionSelect extends Model
{
    use HasFactory;

    protected $table = 'option_select';

    protected $fillable = [
        'pertanyaan_id',
        'text'
    ];

    public function pertanyaan()
    {
        return $this->belongsTo(Pertanyaan::class, 'pertanyaan_id');
    }
}
