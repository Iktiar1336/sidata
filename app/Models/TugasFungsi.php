<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TugasFungsi extends Model
{
    use HasFactory;

    protected $table = 'tugas_fungsi';

    protected $fillable = [
        'tugas',
        'fungsi',
        'kewenangan',
    ];

}
