<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_aplikasi',
        'deskripsi_aplikasi',
        'nama_instansi',
        'alamat',
        'no_telp',
        'email',
        'no_fax',
        'instagram',
        'facebook',
        'twitter',
        'youtube',
    ];
}
