<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeranStrategis extends Model
{
    use HasFactory;

    protected $table = 'peran_strategis';

    protected $fillable = [
        'peran_strategis',
    ];
}
