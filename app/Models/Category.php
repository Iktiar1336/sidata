<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description' , 'parent_id', 'access', 'status', 'workunit_id'];

    public function subcategory()
    {
        return $this->hasMany(\App\Models\Category::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(\App\Models\Category::class, 'parent_id');
    }

    public function workunit()
    {
        return $this->belongsTo(\App\Models\WorkUnit::class);
    }

    public function fileupload()
    {
        return $this->hasMany(\App\Models\FileUpload::class);
    }

    public function kinerja()
    {
        return $this->hasMany(\App\Models\Kinerja::class, 'category_id');
    }

    public function subkinerja()
    {
        return $this->hasMany(\App\Models\Kinerja::class, 'sub_category_id');
    }


}
