<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArsipDigital extends Model
{
    use HasFactory;

    protected $table = 'arsip_digital';

    protected $fillable = [
        'id',
        'arsipkolektor_id',
        'instansi_id',
        'user_id',
        'folder',
        'type',
        'file',
        'created_at',
        'updated_at',
    ];

    public function arsipkolektor()
    {
        return $this->belongsTo(ArsipKolektor::class, 'arsipkolektor_id');
    }

    public function GetimageByType($type)
    {
        if ($type == 'pdf') {
            return asset('format/images/pdf.png');
        } elseif ($type == 'docx' || $type == 'doc') {
            return asset('format/images/word.png');
        } elseif ($type == 'xlsx' || $type == 'xls') {
            return asset('format/images/excel.png');
        } elseif ($type == 'pptx' || $type == 'ppt') {
            return asset('format/images/ppt.png');
        } elseif ($type == 'jpg' || $type == 'jpeg') {
            return 'jpeg.png';
        } elseif ($type == 'png') {
            return asset('format/images/png.png');
        } elseif ($type == 'svg') {
            return asset('format/images/svg.png');
        } elseif ($type == 'zip' || $type == 'rar') {
            return 'zip.png';asset('format/images/zip.png');
        } elseif ($type == 'mp4' || $type == 'avi' || $type == 'mkv') {
            return asset('format/images/mp4.png');
        } elseif ($type == 'mp3' || $type == 'wav') {
            return asset('format/images/Music.png');
        } else {
            return asset('format/images/file.png');
        }
    }

    public function instansi()
    {
        return $this->belongsTo(Instansi::class, 'instansi_id');
    }


}
