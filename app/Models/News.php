<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class News extends Model implements Viewable
{
    use HasFactory;
    use InteractsWithViews;

    protected $table = 'news';

    protected $fillable = [
        'user_id',
        'title',
        'slug',
        'content',
        'image',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
