<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instansi extends Model
{
    use HasFactory;

    protected $table = 'instansi';

    protected $fillable = [
        'code',
        'name',
        'parent_id',
        'nama_instansi',
        'nomenklatur_unit',
        'alamat',
        'telp',
        'email',
    ];

    public function subinstansi()
    {
        return $this->hasMany(Instansi::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Instansi::class, 'parent_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'instansi_id');
    }

    public function arsipkolektor()
    {
        return $this->hasMany(ArsipKolektor::class, 'instansi_id');
    }

    public function arsipdigital()
    {
        return $this->hasMany(ArsipDigital::class, 'instansi_id');
    }
}
