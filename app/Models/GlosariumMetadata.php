<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GlosariumMetadata extends Model
{
    use HasFactory;

    protected $table = 'glosarium_metadata';

    protected $fillable = [
        'metadata',
        'istilah',
        'definisi',
    ];
}
