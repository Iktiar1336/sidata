<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapaianKinerja extends Model
{
    use HasFactory;

    protected $table = 'capaian_kinerja';

    protected $fillable = [
        'sasaranstrategis_id',
        'realisasi',
        'tahun',
        'persentase',
        'status',
    ];

    public function sasaranstrategis()
    {
        return $this->belongsTo(SasaranStrategis::class);
    }

    public function modelkinerja()
    {
        return $this->belongsToMany(ModelKinerja::class);
    }
}
