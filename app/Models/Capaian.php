<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class Capaian extends Model implements Viewable
{
    use HasFactory, InteractsWithViews;

    protected $table = 'capaian';

    protected $fillable = [
        'judul',
        'tahun',
        'deskripsi',
        'status',
    ];

    public function modelkinerja()
    {
        return $this->belongsTo(ModelKinerja::class);
    }
}
