<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetKinerja extends Model
{
    use HasFactory;

    protected $table = 'target_kinerja';

    protected $fillable = [
        'indikatorkinerja_id',
        'tahun',
        'type',
        'target',
    ];

    public function indikatorkinerja()
    {
        return $this->belongsTo(IndikatorKinerja::class);
    }

    public function sasaranstrategis()
    {
        return $this->hasMany(SasaranStrategis::class, 'targetkinerja_id');
    }
}
