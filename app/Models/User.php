<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Models\Activity;
use Auth;
use ESolution\DBEncryption\Traits\EncryptedAttribute;
use Carbon\Carbon;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'workunit_id',
        'telegram_id',
        'instansi_id',
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be encrypted.
     *
     * @var array<int, string>
     */
    protected $encryptable = [
        'username',
        'workunit_id',
        'telegram_id',
        'instansi_id',
        'name',
        'phone',
        'jabatan',
    ];


    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])
        ->format('l, d F Y H:i');
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
        ->format('l, d F Y H:i');
    }

    public function getAvatar()
    {
        if (!$this->avatar) {
            return \Avatar::create(Auth::user()->name)->toBase64();
        }

        return asset('asset/images/avatar/' .$this->avatar);
    }

    public function workunit()
    {
        return $this->belongsTo(WorkUnit::class);
    }

    public function activity()
    {
        return $this->hasMany(Activity::class, 'causer_id');
    }

    public function produsendata()
    {
        return $this->hasMany(Kinerja::class, 'produsendata_id');
    }

    public function pengolahdata()
    {
        return $this->hasMany(Kinerja::class, 'pengolahdata_id');
    }

    public function masterdata()
    {
        return $this->hasMany(Kinerja::class, 'masterdata_id');
    }

    public function messagesender()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function messagereceiver()
    {
        return $this->hasMany(Message::class, 'receiver_id');
    }

    public function berita()
    {
        return $this->hasMany(News::class, 'user_id');
    }

    public function instansi()
    {
        return $this->belongsTo(Instansi::class, 'instansi_id');
    }

    public function getYearAttribute($value)
    {
        return Carbon::parse($value)->format('Y');
    }
}
