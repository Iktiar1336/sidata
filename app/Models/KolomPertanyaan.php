<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KolomPertanyaan extends Model
{
    use HasFactory;

    protected $table = 'kolom_pertanyaan';

    protected $fillable = [
        'nama_kolom',
        'kategoripertanyaan_id',
    ];

    public function kategoriPertanyaan()
    {
        return $this->belongsTo(KategoriPertanyaan::class, 'kategoripertanyaan_id');
    }
    
    public function pertanyaan()
    {
        return $this->hasMany(Pertanyaan::class, 'kolompertanyaan_id');
    }
}
