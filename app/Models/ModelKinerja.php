<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelKinerja extends Model
{
    use HasFactory;

    protected $table = 'model_kinerja';

    protected $fillable = [
        'judul',
        'tahun',
        'pengantar',
        'user_id',
        'visimisi_id',
        'core_values_asn',
        'nilai_nilai_anri',
        'latar_belakang',
        'tugasfungsi_id',
        'strukturorganisasi_id',
        'peranstrategis_id',
        'sistematika_penyajian',
        'rencanastrategis_id',
        'perjanjiankinerja_id',
        'capaianspbe_id',
    ];

    public function visimisi()
    {
        return $this->belongsTo(VisiMisi::class, 'visimisi_id');
    }

    public function tugasfungsi()
    {
        return $this->belongsTo(TugasFungsi::class, 'tugasfungsi_id');
    }

    public function strukturorganisasi()
    {
        return $this->belongsTo(StrukturOrganisasi::class, 'strukturorganisasi_id');
    }

    public function peranstrategis()
    {
        return $this->belongsTo(PeranStrategis::class, 'peranstrategis_id');
    }

    public function rencanastrategis()
    {
        return $this->belongsTo(RencanaStrategis::class, 'rencanastrategis_id');
    }

    public function perjanjiankinerja()
    {
        return $this->belongsTo(PerjanjianKinerja::class, 'perjanjiankinerja_id');
    }

    public function capaian()
    {
        return $this->belongsToMany(Capaian::class, 'capaian_modelkinerja', 'modelkinerja_id', 'capaian_id');
    }

    public function capaianspbe()
    {
        return $this->belongsTo(CapaianSPBE::class, 'capaianspbe_id');
    }

    public function capaiankinerja()
    {
        return $this->belongsToMany(CapaianKinerja::class, 'capaiankinerja_modelkinerja', 'modelkinerja_id', 'capaiankinerja_id');
    }

    public function realisasianggaran()
    {
        return $this->belongsToMany(RealisasiAnggaran::class, 'modelkinerja_realisasianggaran', 'modelkinerja_id', 'realisasianggaran_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
