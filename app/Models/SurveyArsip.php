<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyArsip extends Model
{
    use HasFactory;

    protected $table = 'survey_arsip';

    protected $fillable = [
        'nama',
        'tahun',
        'status',
    ];
    
}
