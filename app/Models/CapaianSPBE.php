<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class CapaianSPBE extends Model implements Viewable
{
    use HasFactory, InteractsWithViews;

    protected $table = 'capaian_spbe';

    protected $fillable = [
        'judul',
        'tahun',
        'indeks',
        'predikat',
        'deskripsi',
    ];

    public function modelkinerja()
    {
        return $this->belongsTo(ModelKinerja::class);
    }
}
