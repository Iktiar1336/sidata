<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class Kinerja extends Model implements Viewable
{
    use HasFactory, InteractsWithViews;

    protected $table = 'kinerja';

    protected $fillable = [
        'workunit_id',
        'category_id',
        'sub_category_id',
        'year',
        'total',
        'description',
        'satuan',
        'status',
        'save_at',
        'process_at',
        'send_at',
        'verified_at',
        'produsendata_id',
        'pengolahdata_id',
        'masterdata_id',
        'chat_id',
        'fileupload_id',
    ];

    public function workunit()
    {
        return $this->belongsTo(WorkUnit::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Category::class, 'sub_category_id');
    }

    public function produsendata()
    {
        return $this->belongsTo(User::class, 'produsendata_id');
    }

    public function pengolahdata()
    {
        return $this->belongsTo(User::class, 'pengolahdata_id');
    }

    public function masterdata()
    {
        return $this->belongsTo(User::class, 'masterdata_id');
    }

    public function fileupload()
    {
        return $this->belongsTo(FileUpload::class);
    }
}
