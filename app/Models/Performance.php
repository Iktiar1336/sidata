<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    use HasFactory;

    protected $table = 'master_kinerja';

    protected $fillable = [
        'visi',
        'misi',
        'tugas',
        'fungsi',
        'kewenangan',
        'struktur_organisasi',
    ];

}
