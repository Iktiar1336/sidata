<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ESolution\DBEncryption\Traits\EncryptedAttribute;

class ArsipKolektor extends Model
{
    use HasFactory;

    protected $table = 'arsip_kolektor';

    protected $fillable = [
        'user_id',
        'instansi_id',
        'jenisarsip_id',
        'kode_klasifikasi',
        'nomor_berkas',
        'type',
        'uraian_informasi',
        'kurun_waktu',
        'jumlah',
        'media',
        'satuan',
        'kondisi',
        'tingkat_perkembangan',
        'folder',
        'boks',
        'keterangan',
        'status',
        // 'uraianitem',
        // 'nomor_surat',
        // 'tanggal_surat',
        // 'kode_klasifikasi',
        // 'tingkat_perkembangan',
        // 'media_arsip',
        // 'kondisi',
        // 'jumlah',
        // 'keterangan', 
        // 'status',
    ];

    public function instansi()
    {
        return $this->belongsTo(Instansi::class);
    }

    public function jenisarsip()
    {
        return $this->belongsTo(JenisArsip::class);
    }

    public function surveyarsip()
    {
        return $this->belongsTo(SurveyArsip::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function arsipdigital()
    {
        return $this->hasMany(ArsipDigital::class, 'arsipkolektor_id');
    }
}
