<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class WorkUnit extends Model implements Viewable
{
    use HasFactory, InteractsWithViews;

    protected $fillable = [
        'code', 'name', 'description', 'status',
    ];

    /**
     * Get all of the category for the WorkUnit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category()
    {
        return $this->hasMany(Category::class, 'workunit_id');
    }

    /**
     * Get all of the user for the WorkUnit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Get all of the kinerja for the WorkUnit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function kinerja()
    {
        return $this->hasMany(Kinerja::class, 'workunit_id');
    }
    
}
