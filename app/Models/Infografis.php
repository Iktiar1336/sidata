<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class Infografis extends Model implements Viewable
{
    use HasFactory;
    use InteractsWithViews;

    protected $table = 'infografis';

    protected $fillable = [
        'title',
        'slug',
        'content',
        'user_id',
        'image',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
