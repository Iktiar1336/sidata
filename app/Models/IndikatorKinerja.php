<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorKinerja extends Model
{
    use HasFactory;

    protected $table = 'indikator_kinerja';

    protected $fillable = [
        'content',
    ];

    public function targetkinerja()
    {
        return $this->hasMany(TargetKinerja::class, 'indikatorkinerja_id');
    }

    public function sasaranstrategis()
    {
        return $this->hasMany(SasaranStrategis::class, 'indikatorkinerja_id');
    }

    
}
