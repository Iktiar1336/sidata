<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RealisasiAnggaran extends Model
{
    use HasFactory;

    protected $table = 'realisasi_anggaran'; 

    protected $fillable = [
        'tahun',
        'sasaranstrategis_id',
        'alokasi',
        'realisasi',
        'persentase',
    ];

    public function sasaranstrategis()
    {
        return $this->belongsTo(SasaranStrategis::class);
    }

    public function modelkinerja()
    {
        return $this->belongsToMany(ModelKinerja::class);
    }
}
