<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tujuan extends Model
{
    use HasFactory;

    protected $table = 'tujuan';

    protected $fillable = [
        'tujuan',
        'indikatorkinerja_id',
        'targetkinerja_id',
    ];

    public function indikatorkinerja()
    {
        return $this->belongsTo(IndikatorKinerja::class, 'indikatorkinerja_id');
    }

    public function targetkinerja()
    {
        return $this->belongsTo(TargetKinerja::class, 'targetkinerja_id');
    }

    public function rencanastrategis()
    {
        return $this->belongsToMany(RencanaStrategis::class);
    }

    public function perjanjiankinerja()
    {
        return $this->belongsToMany(PerjanjianKinerja::class);
    }
}
