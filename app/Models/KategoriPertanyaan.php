<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriPertanyaan extends Model
{
    use HasFactory;

    protected $table = 'kategori_pertanyaan';

    protected $fillable = [
        'judul',
        'deskripsi',
        'slug',
        'survey_id',
    ];

    public function surveyArsip()
    {
        return $this->belongsTo(SurveyArsip::class, 'survey_id');
    }

    public function kolomPertanyaan()
    {
        return $this->hasMany(KolomPertanyaan::class, 'kategoripertanyaan_id');
    }
}
