<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PerjanjianKinerja extends Model
{
    use HasFactory;

    protected $table = 'perjanjian_kinerja';

    protected $fillable = [
        'tahun',
    ];

    public function sasaranstrategis()
    {
        return $this->belongsToMany(SasaranStrategis::class, 'perjanjiankinerja_sasaranstrategis', 'perjanjiankinerja_id', 'sasaranstrategis_id');
    }

    public function tujuan()
    {
        return $this->belongsToMany(Tujuan::class, 'perjanjiankinerja_tujuan', 'perjanjiankinerja_id', 'tujuan_id');
    }

    
}
