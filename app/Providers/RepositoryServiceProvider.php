<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Interfaces\JenisArsipInterfaces',
            'App\Repositories\JenisArsipRepository',
        );

        $this->app->bind(
            'App\Interfaces\InstansiInterfaces',
            'App\Repositories\InstansiRepository',
        );

        $this->app->bind(
            'App\Interfaces\ArsipKolektorInterfaces',
            'App\Repositories\ArsipKolektorRepository',
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
