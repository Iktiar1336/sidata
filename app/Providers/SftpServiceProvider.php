<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;

class SftpServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Storage::extend('sftp', function ($app, $config) {
            $sftpAdapter = new SftpAdapter(
                $config['host'],
                $config['port'],
                $config['username'],
                $config['password'],
                $config['root']
            );
            return new Filesystem($sftpAdapter);
        });
    }
}
